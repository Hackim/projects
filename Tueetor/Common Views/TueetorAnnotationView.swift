//
//  TueetorAnnotationView.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 21/06/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit
import MapKit

class TueetorAnnotationView: MKAnnotationView {
    
    override var annotation: MKAnnotation? {
        willSet {
            guard let artwork = newValue as? TueetorLocation else {return}
            canShowCallout = true
            calloutOffset = CGPoint(x: -5, y: 5)
            let mapsButton = UIButton(frame: CGRect(origin: CGPoint.zero,
                                                    size: CGSize(width: 30, height: 30)))
            mapsButton.setBackgroundImage(UIImage(named: "Maps-icon"), for: UIControlState())
            rightCalloutAccessoryView = mapsButton
            
            let detailLabel = UILabel()
            detailLabel.numberOfLines = 0
            detailLabel.font = detailLabel.font.withSize(12)
            detailLabel.text = artwork.subtitle
            detailCalloutAccessoryView = detailLabel
            image = UIImage(named: "map_pin")
            
        }
    }
    
}
