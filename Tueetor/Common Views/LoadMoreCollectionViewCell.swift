//
//  LoadMoreCollectionViewCell.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 28/05/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit

class LoadMoreCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    class func cellIdentifier() -> String {
        return "LoadMoreCollectionViewCell"
    }
    
}
