//
//  Constants.swift
//  Tueetor
//
//  Created by Phaninder on 15/03/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//
import Foundation
import UIKit

let headers = ["Accept" : "application/json",
               "Os-Type": "iOS",
               "timezone": Utilities.getTimeZone(),
               "locale": UserStore.shared.selectedLanguageCode]

let googlePlacesAPIKey = "AIzaSyDS40t7mNFKDa4XHQRcvMrBQC48j1Xi_80"

//MARK: - UserDefault Keys -

let IsLoggedIn = "IsLoggedIn"
let DeviceToken = "DeviceToken"
let WalkThrough = "WalkThrough"
let LanguageSelected = "LanguageSelected"
let Skip = "Skip"
let NotificationUnAuthorized = "NotificationUnAuthorized"
let HasLoginKey = "hasLoginKey"
let UserEmail = "UserEmail"
let UserID = "UserID"
let TouchIDEmail = "TouchIDEmail"
let SelectedLanguage = "SelectedLanguage"
let SelectedLanguageCode = "SelectedLanguageCode"
let BioMetricEnabledByUser = "BioMetricEnabledByUser"
let IsNotificationOn = "IsNotificationOn"
let IsNotificationSet = "IsNotificationSet"
let NotificationPermissionsAsked = "NotificationPermissionsAsked"

let InvalidAuthTokenErrorCode = 403
let ForceAppUpdateErrorCode = 412

let AppName = "Tueetor".localized

typealias ResponseObject = (response: HTTPURLResponse, data: Data?)
typealias CountObject = (totalCups: Int, todayCups: Int)
typealias UpdateAppObject = (isUpdatedAvailable: Bool, link: String)
typealias UserCalendar = (calendar: [String: [Schedule]], userEvent: [String: [UserEvent]])
typealias AgencyIDInfo = (agencyID: String, assetID: String)
//MARK: - Dimension Constants -

let ScreenWidth = UIScreen.main.bounds.width
let ScreenHeight = UIScreen.main.bounds.height

//MARK: - Validation Constants -

let PhoneNumberMaxLimit = 15
let PhoneNumberMinLimit = 5
let PostalCodeMinLimit = 0
let PostalCodeMaxLimit = 10
let USAPostalCodeLimit = 5
let UserNameMinLimit = 2
let UserNameMaxLimit = 32
let PasswordMinLimit = 8
let PasswordMaxLimit = 15
let EmailMaxLimit = 254
let NameMinLimit = 1
let NameMaxLimit = 50
let DisplayNameMinLimit = 1
let DisplayNameMaxLimit = 100
let AddressMinLimit = 12
let AddressMaxLimit = 360
let CityMinLimit = 0
let CityMaxLimit = 50
let CompanyNameMaxLimit = 164
let MessageMinLimit = 16
let MessageMaxLimit = 360
let PhoneNumberCharacterSet = "0123456789*#+()-"
let NumbersCharacterSet = "0123456789"
let CardNumberMinLimit = 13
let CardNumberMaxLimit = 16
let CardCVVMinDigits = 3
let CardCVVMaxDigits = 4
let CardExpirationLimit = 4


//MARK: - Error Messages -

let AuthenticationErrorMessage = "New Log-in on other device, your session expired and will be redirected to login screen.".localized

//MARK: - Alert Button Titles -

let OKButtonTitle = "OK".localized
let SubmitButtonTitle = "Submit".localized
let CancelButtonTitle = "Cancel".localized
let SettingsButtonTitle = "Settings".localized
let SendButtonTitle = "Send".localized

//MARK: - API related

let baseUrl =  "http://socket-chat.tueetor.com" //"http://tueetoradmin.hipster-inc.com/public"


let successCode = 200

let termsAndConditionsURL = "https://www.getsip.sg/termsconditions"

// MARK: - Default fonts
let textFieldDefaultFont = UIFont(name: "Montserrat-Regular", size: 15.0)!
let textFieldLightFont = UIFont(name: "Montserrat-Light", size: 15.0)!




let titleOptions = ["Mr.".localized, "Mrs.".localized, "Ms.".localized, "Dr.".localized, "Prof.".localized]
let genderOptions = ["Male".localized, "Female".localized]
let daysArray = ["MON", "TUE", "WED", "THU", "FRI", "SAT", "SUN"]
let imageExtensions = ["jpeg", "gif", "png", "jpg"]
let videoExtensions = ["mp4", "mov", "m4v"]

let themeColor = UIColor(red: 0.2235, green: 0.6392, blue: 0.7058, alpha: 1)
let themeBlueColor = UIColor(red: 0.098, green: 0.6431, blue: 0.6941, alpha: 1)
let themeBlackColor = UIColor(red: 0.2823, green: 0.2823, blue: 0.2823, alpha: 1)
let themeLightGrayColor = UIColor(red: 0.8398, green: 0.8398, blue: 0.8398, alpha: 1)
let themeOrangeColor = UIColor(red: 0.9372, green: 0.6509, blue: 0.3176, alpha: 1)
let themeRedColor = UIColor(red: 0.996, green: 0.300, blue: 0.300, alpha: 1)
let themeGreenColor = UIColor.init(red: 0.3843, green: 0.7882, blue: 0.4156, alpha: 1)

// MARK: - URLs

let aboutURL = URL(string: "https://tueetor.com/static-page/aboutus")
let tueetorialURL = URL(string: "https://tueetor.com/static-page/tueetorial")
let tueetorExperienceURL = URL(string: "https://tueetor.com/static-page/experience")
let tueetorAdvantageURL = URL(string: "https://tueetor.com/static-page/advantage")
let faqURL = URL(string: "https://tueetor.com/static-page/faq")
let contactURL = URL(string: "https://tueetor.com/static-page/contactus")
let blogURL = URL(string: "https://tueetor.com/blog/")
let partnerURL = URL(string: "https://tueetor.com/static-page/partner")
let termsURL = URL(string: "https://tueetor.com/static-page/terms")
let privacyURL = URL(string: "https://tueetor.com/static-page/privacy")


// MARK: - Helper texts
let tutorSubjectTexts = ["Select a subject you would like to teach. To suggest a new subject click \"+\".".localized,"",
                  "Select the level of this subject you would like to teach.".localized,
                  "Select the highest qualification you have attained or like to be recognized for this subject. A well-qualified tutor may be more sought after.".localized,
                  "Select the year you first taught this subject. An experienced tutor may be more sought after.".localized,
                  "Enter how much you wish to charge for this subject, per session.".localized,
                  "Enter how much you wish to charge for this subject, per month.".localized,
                  "Enter the location you would like to teach at:\"Any Location\", if you do not mind traveling, \"Set Location\", if you prefer particular venue (eg. Home) and enter the maximum distance you are able to travel from this venue(\"0\" = unable to). Usually, the more flexible you are, the sooner you will find that student, For privacy reasons, Tueetor never discloses your exact address.".localized,
                  "",
                  "",
                  "Select the day and time you are available for this subject. More time slots = more students may respond.".localized,
                  "Once checked, this subject and related information shall be published on the map and searchable.".localized]

let studentSubjectTexts = ["Select a subject you would like to be taught. To suggest a new subject click \"+\".".localized,
                         "Select the level of this subject you would like to be taught.".localized,
                         "Select the minimum qualification of the tutor for this subject. A well-qualified tutor may charge more.".localized,
                         "Select the experience year-range of the tutor for this subject. An Experienced tutor may charge more.".localized,
                         "Enter the maximum amount per session you are able to accept for this subject.".localized,
                         "Enter the maximum amount per month you are able to accept for this subject.".localized,
                         "Enter the location you would like to be taught at:\"Any Location\", if you do not mind traveling, \"Set Location\", if you prefer particular venue (eg. Home) and enter the maximum distance you are able to travel from this venue(\"0\" = unable to). Usually, the more flexible you are, the sooner you will find that tutor, For privacy reasons, Tueetor never discloses your exact address.".localized,
                         "",
                         "",
                         "Select the day and time you are available for this subject. More time slots = more tutors may respond.".localized,
                         "Uncheck to disable this subject profile.".localized]
