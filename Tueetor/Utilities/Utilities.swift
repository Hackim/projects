//
//  Utilities.swift
//  Tueetor
//
//  Created by Phaninder on 15/03/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import Foundation
import Reachability
import Toaster
import CoreLocation
import RxSwift
import Unbox
import EventKit

class Utilities: NSObject {
    
    static let shared = Utilities()
    var reachability: Reachability?

    fileprivate override init() {
        super.init()
    }
    
    class func showHUD(forView view: UIView!, excludeViews: [UIView?]) {
        DispatchQueue.main.async {
            let hudView = MBProgressHUD.showAdded(to: view, animated: true)
            view.bringSubview(toFront: hudView)
            if !excludeViews.isEmpty {
                for subView in excludeViews {
                    view.bringSubview(toFront: subView!)
                }
            }
        }
    }
    
    class func showToast(withString string: String) {
        DispatchQueue.main.async {
            guard string != AuthenticationErrorMessage else {
                return
            }
            ToastView.appearance().backgroundColor = .red
            Toast(text: string, duration: Delay.short).show()
        }
    }
    
    class func hideHUD(forView view: UIView!) {
        DispatchQueue.main.async {
            MBProgressHUD.hide(for: view, animated: true);
        }
    }
    

    class func logoutUser() {
        DispatchQueue.main.async(execute: {
            UserStore.shared.isLoggedIn = false
            UserStore.shared.userID = nil
            UserStore.shared.userEmail = nil
            UserStore.shared.isNotificationsStatusSet = false

        })
    }
    

    class func randomString(length: Int) -> String {
        
        let letters : NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        let len = UInt32(letters.length)
        
        var randomString = ""
        
        for _ in 0 ..< length {
            let rand = arc4random_uniform(len)
            var nextChar = letters.character(at: Int(rand))
            randomString += NSString(characters: &nextChar, length: 1) as String
        }
        
        return randomString
    }
    
    class func isLocationPermissionDenied() -> Bool {
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                return true
            case .authorizedAlways, .authorizedWhenInUse:
                return false
            }
        } else {
            return true
        }
    }
    fileprivate func initializeReachability() {
        let reachability = Reachability()
        self.reachability = reachability
        
        do {
            try self.reachability!.startNotifier()
        } catch {
            print("Unable to start notifier")
        }
    }
    
    func setupReachability() {
        self.initializeReachability()
    }
    
    func isNetworkReachable() -> Bool {
        if let netReachability = reachability {
            return netReachability.connection != .none
        } else {
            self.setupReachability()
            return false
        }
    }
        
    class func isValidString(_ string: String?) -> Bool {
        if let string_ = string, !string_.isEmpty {
            return true
        }
        return false
    }
    
    class func colorFromHex(_ rgbValue:UInt32, alpha:Double=1.0) -> UIColor {
        let red = CGFloat((rgbValue & 0xFF0000) >> 16)/256.0
        let green = CGFloat((rgbValue & 0xFF00) >> 8)/256.0
        let blue = CGFloat(rgbValue & 0xFF)/256.0
        
        return UIColor(red:red, green:green, blue:blue, alpha:CGFloat(alpha))
    }

    class func getFormattedDate(_ date: Date, dateFormat: String = "dd/MM/yyyy") -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateFormat
        return dateFormatter.string(from: date)
    }
    
    class func hasPassword() -> Bool {
        let email = UserStore.shared.touchIDEmail ?? ""
        if email.isEmpty {
            return false
        }

        do {
        let passwordItem = KeychainPasswordItem(service: KeychainConfiguration.serviceName,
                                                account: email,
                                                accessGroup: KeychainConfiguration.accessGroup)
        let keychainPassword = try passwordItem.readPassword()
            if !keychainPassword.isEmpty {
                return true
            } else {
                return false
            }
        } catch {
            return false
        }
    }

    class func getTimeZone() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "Z"
        var timeZone = dateFormatter.string(from: Date())
        timeZone.insert(":", at: timeZone.index(timeZone.startIndex, offsetBy: 3))
        return timeZone
    }
    
    class func sendDeviceToken(_ deviceToken: String?) {
        guard Utilities.shared.isNetworkReachable() else {
            return
        }
        guard UserStore.shared.isNotificationOn == false else {
            return
        }
        guard UserStore.shared.isNotificationsStatusSet == false else {
            return
        }
        
        guard let userID = UserStore.shared.userID else {
            return
        }
        
        let deviceName = UIDevice.current.name
        guard !UserStore.shared.deviceToken.isEmpty || deviceToken != nil else {
            return
        }
        
        guard let uniqueIdentifier = UIDevice.current.identifierForVendor?.uuidString else {
            return
        }
        UIPasteboard.general.string = UserStore.shared.deviceToken
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        let notificationObserver = ApiManager.shared.apiService.toggleNotifications(true, deviceName: deviceName, userId: String(userID), deviceId: uniqueIdentifier, deviceToken: UserStore.shared.deviceToken)
        let _ = notificationObserver.subscribe(onNext: {(message) in
            
            UserStore.shared.isNotificationsStatusSet = true
            DispatchQueue.main.async {
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
            }
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
            })
        })
    }
    
    class func isUserTypeStudent() -> Bool {
        if let user = (UIApplication.shared.delegate as! AppDelegate).user,
            let userType = user.userType {
            return userType == "S"
        }
        return true
    }
    
    class func convertTime(timeString : String , outputFormat: String, inputFormat: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = inputFormat
        
        dateFormatter.timeZone = NSTimeZone(forSecondsFromGMT: 0) as TimeZone?
        
        let date = dateFormatter.date(from: timeString)
        dateFormatter.dateFormat = outputFormat
        dateFormatter.timeZone = TimeZone.current
        
        let datestr = dateFormatter.string(from: date!)
        return datestr
    }
    
    class func getCountries() -> [Country]? {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        if let countries = appDelegate.countries {
            return countries
        } else {
            if let path = Bundle.main.path(forResource: "Country", ofType: "json") {
                do {
                    let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                    let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
                    if let jsonResult = jsonResult as? Dictionary<String, AnyObject>, let dataArray = jsonResult["data"] as? [Dictionary<String, Any>] {
                        var countries = [Country]()
                        for country in dataArray {
                            do {
                                let countryObject: Country = try unbox(dictionary: country)
                                countries.append(countryObject)
                            } catch {
                                //Skip
                            }
                        }
                        appDelegate.countries = countries
                        return countries
                    }
                } catch {
                    return []
                }
            }
            return []
        }
    }
    
    class func getSchedulesDictFromData(_ completeData: [String: AnyObject]) -> UserCalendar {
        var schedules = [String: [Schedule]]()
        var userEvents = [String: [UserEvent]]()
        let scheduleData = completeData["calender"] as! [String: AnyObject]
        let commentsData = completeData["comments"] as! [String: AnyObject]

        for day in daysArray {
            var daySchedule = [Schedule]()
            if let scheduleArray = scheduleData[day] as? [[String: AnyObject]] {
                for individualSchedule in scheduleArray {
                    do {
                        let schedule: Schedule = try unbox(dictionary: individualSchedule)
                        if let _ = schedule.subject {
                            daySchedule.append(schedule)
                        }
                    } catch {
                        print("Couldn't parse schedule")
                    }
                }
            }
            daySchedule.sort { (schedule1, schedule2) -> Bool in
                schedule1.hours < schedule2.hours
            }
            schedules[day] = daySchedule
            
            var dayEvent = [UserEvent]()
            if let eventArray = commentsData[day] as? [[String: AnyObject]] {
                for individualEvent in eventArray {
                    do {
                        let userEvent: UserEvent = try unbox(dictionary: individualEvent)
                        dayEvent.append(userEvent)
                    } catch {
                        print("Couldn't parse schedule")
                    }
                }
            }
            dayEvent.sort { (event1, event2) -> Bool in
                event1.hours < event2.hours
            }
            userEvents[day] = dayEvent
        }
        return (calendar: schedules, userEvent: userEvents)
    }
    
    class func getCurrentCountryCode() -> String {
        let countriesArray = Utilities.getCountries()
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        if let user = appDelegate.user, let countryID = user.countryID {
            return countryID
        }
        guard let countryCode = (Locale.current as NSLocale).object(forKey: .countryCode) as? String else {
            return "178"
        }
        guard let countries = countriesArray else {
            return "178"
        }
        
        let index = countries.index { (country) -> Bool in
            return String(country.code) == countryCode
        }
        if let countryIndex = index, countryIndex > -1 {
            return String(countries[countryIndex].id)
        }
        return "178"
    }
    
    class func getCurrentCountryCurrency(_ selectedCountryCode: String?) -> String {
        var countryCode = selectedCountryCode
        if countryCode == nil || countryCode == "" {
            countryCode = Utilities.getCurrentCountryCode()
        }
        
        let countriesArray = Utilities.getCountries()
        guard let countries = countriesArray else {
            return "SGD"
        }
        let index = countries.index { (country) -> Bool in
            return String(country.id) == countryCode
        }
        if let countryIndex = index, countryIndex > -1 {
            return String(countries[countryIndex].currencyCode)
        }
        return "SGD"
    }
    
    class func getCountryCurrencySymbol(_ selectedCountryCode: String?) -> String {
        var countryCode = selectedCountryCode
        if countryCode == nil || countryCode == "" {
            countryCode = Utilities.getCurrentCountryCode()
        }
        
        let countriesArray = Utilities.getCountries()
        guard let countries = countriesArray else {
            return "SGD($)"
        }
        let index = countries.index { (country) -> Bool in
            return String(country.id) == countryCode
        }
        if let countryIndex = index, countryIndex > -1 {
            return "\(countries[countryIndex].currencyCode!)(\(countries[countryIndex].currencySymbol!))"
        }
        return "SGD($)"
    }

    class func getCountryNameForID(_ selectedCountryCode: String?) -> String {
        var countryCode = selectedCountryCode
        if countryCode == nil {
            countryCode = Utilities.getCurrentCountryCode()
        }
        
        let countriesArray = Utilities.getCountries()
        guard let countries = countriesArray else {
            return "Singapore"
        }
        let index = countries.index { (country) -> Bool in
            return String(country.id) == countryCode
        }
        if let countryIndex = index, countryIndex > -1 {
            return String(countries[countryIndex].name)
        }
        return "Singapore"
    }
    
    class func getEKWeekDay(_ selectedDay: Int) -> EKWeekday {
        switch selectedDay {
        case 0:
            return EKWeekday.monday
        case 1:
            return EKWeekday.tuesday
        case 2:
            return EKWeekday.wednesday
        case 3:
            return EKWeekday.thursday
        case 4:
            return EKWeekday.friday
        case 5:
            return EKWeekday.saturday
        default:
            return EKWeekday.sunday
        }
    }
    
    class  func getRepeatValue (_ option : Int) -> EKRecurrenceRule?{
        switch option {
        case 0:
            //on the same week day for 50 years
            let rule = EKRecurrenceRule(recurrenceWith: EKRecurrenceFrequency.weekly, interval: 1, end: nil)
            return rule
        case 1:
            //on the same date of every month
            let rule = EKRecurrenceRule(recurrenceWith: EKRecurrenceFrequency.monthly, interval: 1, end: nil)
            return rule
        case 2:
            //on the same date and month of the year
            let rule = EKRecurrenceRule(recurrenceWith: EKRecurrenceFrequency.yearly, interval: 1, end: nil)
            return rule
        default:
            return nil
        }
    }
    
    class func getDateBasedOnWeekday(_ weekDayInt: Int, andHours hours: Int) -> Date {
        // Weekday units are the numbers 1 through n, where n is the number of days in the week.
        // For example, in the Gregorian calendar, n is 7 and Sunday is represented by 1.
        
        var newWeekDay = 1
        switch weekDayInt {
        case 0:
            newWeekDay = 2
        case 1:
            newWeekDay = 3
        case 2:
            newWeekDay = 4
        case 3:
            newWeekDay = 5
        case 4:
            newWeekDay = 6
        case 5:
            newWeekDay = 7
        case 6:
            newWeekDay = 1
        default:
            newWeekDay = 1
        }
        let accurateDate = Calendar.current.date(bySettingHour: hours, minute: 0, second: 0, of: Date())!
        let weekDayComponents = DateComponents(calendar: Calendar.current, hour: hours, weekday: newWeekDay)
        let nextDay = Calendar.current.nextDate(after: accurateDate, matching: weekDayComponents, matchingPolicy: .nextTimePreservingSmallerComponents)!

        return nextDay
    }
    
    class func getSortStringForType(_ sortBy: SortByFilter) -> String? {
        switch sortBy {
        case .noSort:
            return nil
        case .qualifi_asc:
            return "qualification_a"
        case .qualifi_desc:
            return "qualification_d"
        case .exp_asc:
            return "experience_a"
        case .exp_desc:
            return "experience_d"
        case .session_asc:
            return "persession_a"
        case .session_desc:
            return "persession_d"
        case .month_asc:
            return "permonth_a"
        case .month_desc:
            return "permonth_d"
        case .dist_asc:
            return "distance_a"
        case .dist_desc:
            return "distance_d"
        case .rating_asc:
            return "rating_a"
        case .rating_desc:
            return "rating_d"
        default:
            return nil
        }
    }

}
extension String {
    func hexStringToUIColor () -> UIColor {
        var cString:String = self.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
}

