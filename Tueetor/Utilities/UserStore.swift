//
//  UserStore.swift
//  Tueetor
//
//  Created by Phaninder on 15/03/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import Foundation

class UserStore: NSObject {
    
    static let shared = UserStore()
    let userDefaults = UserDefaults.standard
    
    fileprivate override init() {
        super.init()
    }
    
    var isLoggedIn: Bool {
        set {
            userDefaults.setValue(newValue, forKey: IsLoggedIn)
            userDefaults.synchronize()
        }
        get {
            return userDefaults.value(forKey: IsLoggedIn) as? Bool ?? false
        }
    }

    var deviceToken: String {
        set {
            userDefaults.setValue(newValue, forKey: DeviceToken)
            userDefaults.synchronize()
        }
        get {
            return userDefaults.value(forKey: DeviceToken) as? String ?? ""
        }
    }

    var isWalkthroughCompleted: Bool {
        set {
            userDefaults.setValue(newValue, forKey: WalkThrough)
            userDefaults.synchronize()
        }
        get {
            return userDefaults.value(forKey: WalkThrough) as? Bool ?? false
        }
    }
    
    var isLanguageSelected: Bool {
        set {
            userDefaults.setValue(newValue, forKey: LanguageSelected)
            userDefaults.synchronize()
        }
        get {
            return userDefaults.value(forKey: LanguageSelected) as? Bool ?? false
        }
    }

    var isSkipSelected: Bool {
        set {
            userDefaults.setValue(newValue, forKey: Skip)
            userDefaults.synchronize()
        }
        get {
            return userDefaults.value(forKey: Skip) as? Bool ?? false
        }
    }

    var isNotificationUnAuthorized: Bool {
        set {
            userDefaults.setValue(newValue, forKey: NotificationUnAuthorized)
            userDefaults.synchronize()
        }
        get {
            return userDefaults.value(forKey: NotificationUnAuthorized) as? Bool ?? false
        }

    }
    
    var hasLoginKey: Bool {
        set {
            userDefaults.setValue(newValue, forKey: HasLoginKey)
            userDefaults.synchronize()
        }
        get {
            return userDefaults.value(forKey: HasLoginKey) as? Bool ?? false
        }
        
    }

    var userEmail: String? {
        set {
            userDefaults.setValue(newValue, forKey: UserEmail)
            userDefaults.synchronize()
        }
        get {
            return userDefaults.value(forKey: UserEmail) as? String
        }
    }
    
    var touchIDEmail: String? {
        set {
            userDefaults.setValue(newValue, forKey: TouchIDEmail)
            userDefaults.synchronize()
        }
        get {
            return userDefaults.value(forKey: TouchIDEmail) as? String
        }
    }
    
    var userID: Int? {
        set {
            userDefaults.setValue(newValue, forKey: UserID)
            userDefaults.synchronize()
        }
        get {
            return userDefaults.value(forKey: UserID) as? Int
        }
    }
    
    var isBioMetricEnabledByUser: Bool {
        set {
            userDefaults.setValue(newValue, forKey: BioMetricEnabledByUser)
            userDefaults.synchronize()
        }
        get {
            return userDefaults.value(forKey: BioMetricEnabledByUser) as? Bool ?? false
        }
    }

    var selectedLanguageName: String {
        set {
            userDefaults.setValue(newValue, forKey: SelectedLanguage)
            userDefaults.synchronize()
        }
        get {
            return userDefaults.value(forKey: SelectedLanguage) as? String ?? "English"
        }
    }
    
    var selectedLanguageCode: String {
        set {
            userDefaults.setValue(newValue, forKey: SelectedLanguageCode)
            userDefaults.synchronize()
        }
        get {
            return userDefaults.value(forKey: SelectedLanguageCode) as? String ?? "en"
        }
    }
    
    var isNotificationsStatusSet: Bool {
        set {
            userDefaults.setValue(newValue, forKey: IsNotificationSet)
            userDefaults.synchronize()
        }
        get {
            return userDefaults.value(forKey: IsNotificationSet) as? Bool ?? false
        }
    }
    
    var isNotificationOn: Bool {
        set {
            userDefaults.setValue(newValue, forKey: IsNotificationOn)
            userDefaults.synchronize()
        }
        get {
            return userDefaults.value(forKey: IsNotificationOn) as? Bool ?? false
        }
    }
    
    var notificationPermissionsAsked: Bool {
        set {
            userDefaults.setValue(newValue, forKey: NotificationPermissionsAsked)
            userDefaults.synchronize()
        }
        get {
            return userDefaults.value(forKey: NotificationPermissionsAsked) as? Bool ?? false
        }
    }
}

