//
//  Extensions.swift
//  Tueetor
//
//  Created by Phaninder on 15/03/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import Foundation

extension UIViewController {
    
    class var storyboardID : String {
        return "\(self)"
    }
    
    static func instantiateFromAppStoryboard(appStoryboard: AppStoryboard) -> Self {
        return appStoryboard.viewController(viewControllerClass: self)
    }
    
}

extension UIButton {
    // MARK: - UIButton+Aligment
    
    func alignContentVerticallyByCenter(offset:CGFloat = 10.0) {
        let buttonSize = frame.size
        
        if let titleLabel = titleLabel,
            let imageView = imageView {
            
            if let buttonTitle = titleLabel.text,
                let image = imageView.image {
                let titleString:NSString = NSString(string: buttonTitle)
                let titleSize = titleString.size(withAttributes: [
                    NSAttributedStringKey.font : titleLabel.font
                    ])
                let buttonImageSize = image.size
                
                let topImageOffset = (buttonSize.height - (titleSize.height + buttonImageSize.height + offset)) / 2
                let leftImageOffset = (buttonSize.width - buttonImageSize.width) / 2
                imageEdgeInsets = UIEdgeInsetsMake(topImageOffset,
                                                   leftImageOffset,
                                                   0,0)
                
                let titleTopOffset = topImageOffset + offset + buttonImageSize.height
                let leftTitleOffset = (buttonSize.width - titleSize.width) / 2 - image.size.width
                
                titleEdgeInsets = UIEdgeInsetsMake(titleTopOffset,
                                                   leftTitleOffset,
                                                   0,0)
            }
        }
    }
    
    func hasImage(named imageName: String, for state: UIControlState) -> Bool {
        guard let buttonImage = image(for: state), let namedImage = UIImage(named: imageName) else {
            return false
        }
        
        return UIImagePNGRepresentation(buttonImage) == UIImagePNGRepresentation(namedImage)
    }

}

extension String {
    
    func widthWithConstrainedHeight(height: CGFloat) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, context: nil)
        
        return boundingBox.width
    }
    
    
    func isValidEmailID() -> Bool {
        if self.isEmpty {
            return false
        }
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self)
    }
    
    func isValidUserName() -> Bool {
        let userNameRegex =  "^(?=.{2,32}$)(?![_.0-9])(?!.*[_.]{2})[a-zA-Z0-9._]+(?<![_.])$"
        
        let userNameTest = NSPredicate(format:"SELF MATCHES %@", userNameRegex)
        
        let dotArray = self.components(separatedBy: ".")
        let underScoreArray = self.components(separatedBy: "_")
        return userNameTest.evaluate(with: self) && dotArray.count - 1 <= 1 && underScoreArray.count - 1 <= 1
    }
    
    
    func isEmptyString() -> Bool {
        let trimmedString = self.components(separatedBy: .whitespacesAndNewlines).joined(separator: "")
        return trimmedString.isEmpty
    }
    
    
    func isValidPassword() -> Bool {
        if self.isEmptyString() || self.count < PasswordMinLimit {
            return false
        }
        return true
    }
    
    func isValidPostalCode() -> Bool {
        if self.count < PostalCodeMinLimit {
            return false
        }
        return true
    }
    
    func isValidPhoneNumber() -> Bool {
        if self.isEmptyString() || self.count < PhoneNumberMinLimit {
            return false
        }
        return true
    }
    
    func isValidName() -> Bool {
        if self.isEmptyString() || self.count < NameMinLimit {
            return false
        }
        return true
    }
    
    func isValidAddress() -> Bool {
        if self.isEmptyString() || self.count < AddressMinLimit {
            return false
        }
        return true
    }
    
    func isValidCity() -> Bool {
        if self.isEmptyString() || self.count < CityMinLimit {
            return false
        }
        return true
    }
    
    func isFeedBackMessage() -> Bool {
        if self.isEmptyString() || self.count < MessageMinLimit {
            return false
        }
        return true
    }
    
    func isValidCardNumber() -> Bool {
        if self.isEmptyString() || self.count < CardNumberMinLimit {
            return false
        }
        return true
    }
    
    func isValidCardExpirationDate() -> Bool {
        if self.isEmptyString() || self.count < CardExpirationLimit {
            return false
        }
        return true
    }
    
    func isValidCardCVV() -> Bool {
        if self.isEmptyString()
            || self.count < CardCVVMinDigits
            || self.count > CardCVVMaxDigits {
            return false
        }
        
        return true
    }
    
    func containsOnlyCharactersIn(matchCharacters: String) -> Bool {
        let disallowedCharacterSet = NSCharacterSet(charactersIn: matchCharacters).inverted
        return (self.rangeOfCharacter(from: disallowedCharacterSet) == nil)
    }
    
    mutating func convertHtml(fontFamilyName: String, fontSize: Int) -> NSAttributedString{
        let aux = "<span style=\"font-family: \(fontFamilyName); font-size: \(fontSize)\">\(self)</span>"

        
        guard let data = aux.data(using: .utf8) else { return NSAttributedString() }
        do{
            let htmlConvertedString = try NSMutableAttributedString(data: data,                                           options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
            return htmlConvertedString
        }catch{
            return NSAttributedString()
        }
    }
    
    
}

extension NSAttributedString {
    
    func height(withConstrainedWidth width: CGFloat) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, context: nil)
        
        return ceil(boundingBox.height)
    }
    
    func width(withConstrainedHeight height: CGFloat) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, context: nil)
        
        return ceil(boundingBox.width)
    }
}

extension UILabel {
    
    func heightForText() -> CGFloat {
        let constraintRect = CGSize(width: self.frame.width, height: .greatestFiniteMagnitude)
        let boundingBox = self.text?.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [.font : self.font], context: nil)
        
        return ceil(boundingBox!.height)
    }

}

extension UIDevice {
    static var isIphoneX: Bool {
        var modelIdentifier = ""
        if isSimulator {
            modelIdentifier = ProcessInfo.processInfo.environment["SIMULATOR_MODEL_IDENTIFIER"] ?? ""
        } else {
            var size = 0
            sysctlbyname("hw.machine", nil, &size, nil, 0)
            var machine = [CChar](repeating: 0, count: size)
            sysctlbyname("hw.machine", &machine, &size, nil, 0)
            modelIdentifier = String(cString: machine)
        }
        
        return modelIdentifier == "iPhone10,3" || modelIdentifier == "iPhone10,6"
    }
    
    static var isSimulator: Bool {
        return TARGET_OS_SIMULATOR != 0
    }
}

extension Notification.Name {
    static let openURLInSafari = Notification.Name("openURLInSafari")
    static let deviceTokenReceived = Notification.Name("deviceTokenReceived")
    static let subjectsUpdated = Notification.Name("subjectsUpdated")
    static let refreshDashboardData = Notification.Name("refreshDashboardData")
    static let filterAvailabilitySelected = Notification.Name("filterAvailabilitySelected")

}

extension Collection {
    
    /// Returns the element at the specified index iff it is within bounds, otherwise nil.
    subscript (safe index: Index) -> Element? {
        return indices.contains(index) ? self[index] : nil
    }
}

extension Data {
    private static let mimeTypeSignatures: [UInt8 : String] = [
        0xFF : "image/jpeg",
        0x89 : "image/png",
        0x47 : "image/gif",
        0x49 : "image/tiff",
        0x4D : "image/tiff",
        0x25 : "application/pdf",
        0xD0 : "application/vnd",
        0x46 : "text/plain",
        ]
    
    var mimeType: String {
        var c: UInt8 = 0
        copyBytes(to: &c, count: 1)
        return Data.mimeTypeSignatures[c] ?? "application/octet-stream"
    }
    
    
}

extension UIView {
        
    func fadeOut(_ duration: TimeInterval) {
        UIView.animate(withDuration: duration, animations: {
            self.alpha = 0.0
        })
    }
    
    func fadeIn(_ duration: TimeInterval) {
        UIView.animate(withDuration: duration, animations: {
            self.alpha = 1.0
        })
    }
    
    class func viewFromNibName(_ name: String) -> UIView? {
        let views = Bundle.main.loadNibNamed(name, owner: nil, options: nil)
        return views?.first as? UIView
    }
}


