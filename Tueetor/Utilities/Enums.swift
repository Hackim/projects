//
//  Enums.swift
//  Tueetor
//
//  Created by Phaninder on 15/03/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import Foundation

extension String {
    var localized: String {
        
        var languageCode = ""
        if UserStore.shared.selectedLanguageCode == "ph"{
            languageCode = "fil-PH"
        }else{
            languageCode = UserStore.shared.selectedLanguageCode
        }
        
        let path = Bundle.main.path(forResource: languageCode, ofType: "lproj")
        let bundle = Bundle(path: path!)
        
        return NSLocalizedString(self, tableName: nil, bundle: bundle!, value: "", comment: "")
    }
}


enum URI: String {
    case login = "/post/login"
    case countries = "/country"
    case emailAvailability = "/post/check"
    case checkName = "/check_name"
    case signup = "/post/register"
    case confirmOTP = "/post/check_otp"
    case resendOTP = "/sendotp/"
    case forgotPassword = "/post/forgot"
    case resetPassword = "/post/password"
    case dashboard = "/dashboard/"
    case topSubjects = "/category"
    case categorySubjects = "/category/"
    case featuredTutors = "/top_tueetor"
    case userDetails = "/user/"
    case subjects = "/get_subject"
    case levels = "/get_level"
    case qualifications = "/qualification"
    case experiences = "/experience"
    case changePassword = "/post/change_password"
    case changeEmail = "/post/change_email"
    case changeMobile = "/post/change_contact"
    case fetchLanguages = "/language"
    case notifications = "/post/device_registration"
    case changeNumber = "/post/override_contact"
    case fetchTutor = "/tutor_profile"
    case fetchAgency = "/agency_profile"
    case featuredPartners = "/top_partner"
    case mySubjectListing = "/user_subject/"
    case addSubject = "/post/addSubject/"
    case changeSubjectStatus = "/subject/changeStatus/"
    case deleteSubject = "/delete_user_subject"
    case editSubject = "/post/editSubject/"
    case fetchDocuments = "/doc_list"
    case uploadDocument = "/post/upload_doc"
    case renameDocument = "/post/rename_doc"
    case deleteDocument = "/doc_delete"
    case editProfile = "/post/profile/"
    case fetchShortlist = "/shortlist/"
    case addShortlist = "/set_shortlist"
    case editShortlist = "/edit_shortlist"
    case deleteShortlist = "/delete_shortlist"
    case filter = "/filter"
    case countryCode = "/country_by_code"
    case fetchStudent = "/student_profile"
    case reportUser = "/post/report_user"
    case enquire = "/post/enquiry"
    case postShoutout = "/post/shout_out"
    case fetchNotifications = "/notifications"
    case fetchReviews = "/my_ratings/"
    case requestReview = "/post/add_rating"
    case plans = "/plans/"
    case fetchEphemeralKey = "/get_ephemeral_key"
    case subscribe = "/post/subcribe"
    case filterTutor = "/filter_tutor"
    case filterStudent = "/filter_student"
    case filterTutorCount = "/tutor_filtered"
    case filterStudentCount = "/student_filtered"
    case unsubscribe = "/unsubcribe"
    case savedCards = "/cards"
    case deleteCard = "/delete_card"
    case addCard = "/post/add_card"
    case userCalendar = "/get_calender/"
    case addEvent = "/post/avilabilty_comments/"
    case editEvent = "/post/avilabilty_comments/update/"
    case deleteEvent = "/avilabilty_comments/delete/"
    case requestSubject = "/post/request_subject"
    case chatBox = "/user_chatbox/"
    case userMessages = "/user_messages"
    case sendMessage = "/post/sendMessage"
    case enquiryCount = "/post/enquiry_count"
    case limits  = "/message_details/"
}

enum AppStoryboard : String {
    
    case Main, Profile, Message, Dashboard, More, Find, Login
    
    var instance : UIStoryboard {
        return UIStoryboard(name: self.rawValue, bundle: Bundle.main)
    }
    
    func viewController<T : UIViewController>(viewControllerClass : T.Type) -> T {
        let storyboardID = (viewControllerClass as UIViewController.Type).storyboardID
        return instance.instantiateViewController(withIdentifier: storyboardID) as! T
    }
    
}

enum CustomButtonType: Int {
    case clear = 0, white, dual
}

enum PaginationType: Int {
    case new, old, reload
}


enum APIServiceType {
    case apiService
}

enum ResponseError: Error {
    case notFoundError
    case badRequestError
    case timeoutError
    case internalServerError
    case parseError
    case unboxParseError
    case apiFailureError(message:String)
    case unknownError
    
    func description() -> String {
        switch self {
        case .notFoundError:
            return "URL not found!!!".localized
        case .badRequestError:
            return "Tueetor.API.BadRequest".localized
        case .timeoutError:
            return "Tueetor.API.RequestTimeout".localized
        case .internalServerError:
            return "Tueetor.API.InternalServerError".localized
        case .parseError:
            return "Tueetor.API.ParseError".localized
        default:
            return "Tueetor.API.UnknownError".localized
        }
    }
}

enum ErrorMessage {
    case apiFailureError(message:String)
}

enum ResponseStatusCode:Int {
    case notFound = 404
    case badRequest = 400
    case timeout = 408
    case internalServer = 500
}

enum ScreenID: String {
    case RegistrationViewController = "RegistrationViewController"
}

// MARK: - TextFields
enum SignupAccountTextFieldType: Int {
    case email = 1, confirmEmail, password, confirmPassword
}

enum SignupProfileTextFieldType: Int {
    case dual = 1, firstName = 2, lastName = 3, displayName = 4, birthday = 6
}

enum SignupAddressTextFieldType: Int {
    case address1 = 1, address2, dual, country, phone
}

enum ChangePasswordTextFieldType: Int {
    case oldPassword = 0, newPassword, confirmPassword
}

enum ChangeEmailTextFieldType: Int {
    case oldEmail = 0, newEmail, confirmEmail
}

enum ChangeMobileTextFieldType: Int {
    case oldMobile = 0, newMobile
}

enum EditProfileScreenRowType: Int {
    case profileImage = 0, email, title, firstName, lastName, display, profileURL, birthday, address1, address2, city, country, contact, about
}

enum SubjectDetailRowType: Int {
    case header = 0, qualification, teachingSince, sessionRate, monthlyRate, location, maxDistance, status, options
}

enum AddSubjectRowType: Int {
    case subject = 0, suggestSubject, level, qualification, teachingSince, sessionRate, monthlyRate, location, address, distance, preferredTime, active
}

enum ValidationErrorMessage: String {
    case emailEmpty
    case emailInvalid
    case emailMismatch
    case passwordEmpty
    case passwordInvalid
    case passwordMismatch
    case title
    case gender
    case firstName
    case lastName
    case displayName
    case birthday
    case address
    case postalCode
    case country
    case phoneEmpty
    case phoneInvalid
    case phoneMismatch
    case subject
    case level
    case qualification
    case teachingSince
    case sessionRate
    case monthRate
    case subjectAddress
    case distance
    case schedule
    case experience
    case studentName
    
    func description() -> String {
        switch self {
        case .emailEmpty:
            return "Please enter an email address.".localized
        case .emailInvalid:
            return "Please enter a valid email address.".localized
        case .emailMismatch:
            return "Both emails did not match. Please try again.".localized
        case .passwordEmpty:
            return "Please enter password.".localized
        case .passwordInvalid:
            return "Password length should be \(PasswordMinLimit) characters or more.".localized
        case .passwordMismatch:
            return "Both passwords did not match. Please try again.".localized
        case .title:
            return "Please select a title.".localized
        case .gender:
            return "Please select a gender.".localized
        case .firstName:
            return "Please enter your first name.".localized
        case .lastName:
            return "Please enter your last name.".localized
        case .displayName:
            return "Please enter a preferred display name.".localized
        case .birthday:
            return "Please select birthday.".localized
        case .address:
            return "Please enter Address Line 1.".localized
        case .postalCode:
            return "Must be \(PostalCodeMinLimit) characters or more.".localized
        case .country:
            return "Please select a country.".localized
        case .phoneEmpty:
            return "Please enter telephone no.".localized
        case .phoneInvalid:
            return "Please enter valid telephone no.".localized
        case .phoneMismatch:
            return "Both contact numbers did not match. Please try again.".localized
        case .subject:
            return "Please select subject.".localized
        case .level:
            return "Please select level.".localized
        case .qualification:
            return "Please select qualification.".localized
        case .teachingSince:
            return "Please select the year you first taught this subject.".localized
        case .sessionRate:
            return "Please enter your rate per session.".localized
        case .monthRate:
            return "Please enter your rate per month.".localized
        case .subjectAddress:
            return "Please provide an address.".localized
        case .distance:
            return "Please enter distance".localized
        case .schedule:
            return "Please provide your availability.".localized
        case .experience:
            return "Please select the preferred experience".localized
        case .studentName:
            return "Please enter student name.".localized
        }
    }
}

//enum ToastMessage: String {
//    case invalidInput
//
//    func description() -> String {
//        switch self {
//        case .invalidInput:
//            return "Please enter valid inputs.".localized
//        }
//    }
//}

enum AlertMessage: String {
    case removeFieldData
    case resetPasswordSuccess
    case exitSignupProcess
    case invalidInput
    case noInternetConnection
    case changePasswordSuccess
    case changeEmailSuccess
    case changeMobileSuccess
    case fbAccountDoesntExist
    case changePhoneNumber
    case docDesc
    case docDescRename
    case addSubjectSuccess
    case deleteSubject
    case editShortlistComment
    case delete
    case newShortlistComment
    case removeShortlist
    case tPakRenewal
    case suggestSubject
    case subjectLimit
    
    func description() -> String {
        switch self {
        case .removeFieldData:
            return "Leaving this page will clear the data you've entered. Do you want to proceed?".localized
        case .resetPasswordSuccess:
            return "Password has been successfully changed. Please login to continue.".localized
        case .exitSignupProcess:
            return "Do you want to exit the Sign up process?"
        case .invalidInput:
            return "Please enter valid inputs.".localized
        case .noInternetConnection:
            return "Please check your internet connection.".localized
        case .changePasswordSuccess:
            return "Password has been successfully changed".localized
        case .changeEmailSuccess:
            return "Email has been successfully changed".localized
        case .changeMobileSuccess:
            return "Contact number has been successfully changed".localized
        case .fbAccountDoesntExist:
            return "There is no account associated with this Facebook account. Do you want to Sign Up using this account?".localized
        case .changePhoneNumber:
            return "Please enter the new phone number in the text field.".localized
        case .docDesc:
            return "Please enter name for the selected document.".localized
        case .docDescRename:
            return "Please enter new name for the document.".localized
        case .addSubjectSuccess:
            return "Subject added successfully. You will receive an instant notification from us shortly when there's a match. You may also widen your search in Find Now.".localized
        case .deleteSubject:
            return "Are you sure you want to delete the subject?".localized
        case .editShortlistComment:
            return "Edit your comments here".localized
        case .delete:
            return "Are you sure, you want to delete?".localized
        case .newShortlistComment:
            return "Save to My Shortlist".localized
        case .removeShortlist:
            return "Are you sure, you want to remove this user from shortlist?".localized
        case .tPakRenewal:
            return "Your T.Pak subscription has expired. An active subscription is required for subject, message and review management.".localized
        case .suggestSubject:
            return "Name of the New Subject".localized
        case .subjectLimit:
            return "Cannot select more than 4 subjects".localized
        }
    }
}

enum AlertButton: String {
    case yes
    case no
    case ok
    case later
    case confirm
    case cancel
    case findNow
    case submit
    case renewNow
    case proceed
    case settings
    case close
    case enquire
    case sendMessage
    
    func description() -> String {
        switch self {
        case .yes:
            return "Yes".localized
        case .no:
            return "No".localized
        case .ok:
            return "OK".localized
        case .confirm:
            return "Confirm".localized
        case .later:
            return "Later".localized
        case .cancel:
            return "Cancel".localized
        case .findNow:
            return "Find Now".localized
        case .submit:
            return "Submit".localized
        case .renewNow:
            return "Renew Now".localized
        case .proceed:
            return "Proceed".localized
        case .settings:
            return "Settings".localized
        case .close:
            return "Close".localized
        case .enquire:
             return "Enquire".localized
        case .sendMessage :
            return "SEND MESSAGE".localized
            
        }        
    }
}

enum EmptyMessageText: String {
    case about
    case subject
    case document
    case review
    case media
    case location
    case course
    
    func description() -> String {
        switch self {
        case .about:
            return "About has not been provided".localized
        case .subject:
            return "No subjects have been declared.".localized
        case .document:
            return "No documents have been uploaded.".localized
        case .review:
            return "No reviews have been given.".localized
        case .media:
            return "No media have been uploaded".localized
        case .location:
            return "No locations have been provided".localized
        case .course:
            return "No courses have been provided".localized

        }
    }
}

enum LegendType: Int {
    case studentSubject = 0, tutorSubject, multiMatch, subjectMatch, agency
}

enum FilterTableViewSectionType: Int {
    case subjects = 0, levels, qualifications, experience, budget, rating, location, userSearch, distance, availability
}

enum SlotDay: Int {
    case mon = 1, tue, wed, thu, fri, sat, sun
}

enum FilterScreenCategoryType: Int {
    case subject = 0, level, qualification, experience, teachingSince
}

enum MoreTableViewRow: Int {
    case about = 1, tueetorial, experience, advantage, faq, contact, notifications, blog, partnership, terms, privacy, settings
}

enum ProfilePageType: Int {
    case tutor = 0, agency, student
}

enum SortByFilter: Int {
    case noSort = 0 , qualifi_asc, qualifi_desc, exp_asc, exp_desc, session_asc, session_desc, month_asc, month_desc, dist_asc, dist_desc, rating_asc, rating_desc
}

//enum ValidationErrorMessage: String {
//    case emailEmpty = "Please enter an email address.".localized
//    case countries = "/country"
//}

