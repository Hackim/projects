//
//  DataService.swift
//  Tueetor
//
//  Created by Phaninder on 15/03/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import Unbox

// MARK: - APIClient (Convenient Resource Methods)

class DataService: TueetorService {
    func fetchMessagesForReceiverID(_ receiverID: String, senderID: String, page: Int) -> Observable<ChatPagination> {
        let params = ["user_id": receiverID,
                      "sender_id": senderID,
                      "page":String(page)] as [String: AnyObject]
        return Observable.create{ observer in
            let messagesObserver = ApiManager.get(path: .userMessages, params: params)
            _ = messagesObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .userMessages, successCompletionHandler: { (success, json, error) in
                            if success == true, let userObject = json/*, let dataDict = userObject["data"] as? [String: AnyObject], let messageArray = dataDict["messages"] as? [[String: AnyObject]]*/ {
//                                var messages = [Message]()
//                                for messageDict in messageArray {
//                                    let message = Message(responseDict: messageDict as NSDictionary)
//                                    messages.append(message)
//                                }
//                                observer.on(.next(messages))
                                do{
                                    let chatItem: ChatPagination = try unbox(dictionary: userObject)
                                    observer.on(.next(chatItem))
                                }catch{
                                    observer.onError(ResponseError.unboxParseError)
                                }
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        }
    }
    
    
    func fetchUserChatBox(_ page: String, _ userID: Int) -> Observable<MessagePagination> {
        let params = ["id": userID] as [String: AnyObject]
        return Observable.create{ observer in
            let chatObserver = ApiManager.get(path: .chatBox, params: params)
            _ = chatObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .chatBox, successCompletionHandler: { (success, json, error) in
                            if success == true, let userObject = json /*,let dataDict = userObject["data"] as? [String: AnyObject], let chatArray = dataDict["detail"] as? [[String: AnyObject]]*/ {
                                
                                //                                var chats = [Chat]()
                                //                                for chatDict in chatArray {
                                do {
                                    
                                    //                                        let chat: Chat = try unbox(dictionary: chatDict)
                                    //                                        chats.append(chat)
                                    let messageItem: MessagePagination = try unbox(dictionary: userObject)
                                    observer.on(.next(messageItem))
                                } catch {
                                    observer.onError(ResponseError.unboxParseError)
                                }
                                //                                }
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        }
        
        /*        let urlPath = isTutor ? URI.filterTutor : URI.filterStudent
         return Observable.create{ observer in
         let filterObserver = ApiManager.get(path: urlPath, params: filterDict)
         _ = filterObserver.retry(2)
         .subscribe(
         onNext: {(responseObject) in
         ApiManager.validateResponse(forResponse: responseObject, path: .filter, successCompletionHandler: { (success, json, error) in
         if success == true, let userObject = json {
         do {
         let mapItem: MapItemPagination = try unbox(dictionary: userObject)
         observer.on(.next(mapItem))
         } catch {
         observer.onError(ResponseError.unboxParseError)
         }
         } else if success == false, let error_ = error {
         observer.onError(error_)
         } else {
         observer.onError(ResponseError.unknownError as NSError)
         }
         })
         }, onError: {(error) in
         observer.onError(error)
         })
         return Disposables.create {}
         }*/
    }

    func loginUserWithEmail(_ email: String, password: String) -> Observable<User> {
        let dict = ["email": email, "password": password] as Dictionary<String, AnyObject>
        return Observable.create{ observer in
            let loginObserver = ApiManager.post(path: .login, params: dict)
            _ = loginObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .login, successCompletionHandler: { (success, json, error) in
                            if success == true, let userObject = json, let dataDict = userObject["data"] as? [String: AnyObject], let userDict = dataDict["user"] as? [String: AnyObject] {
                                do {
                                    let user: User = try unbox(dictionary: userDict)
                                    if let verifyURL = dataDict["redirectToVerify"] as? String {
                                        user.redirectToVerify = verifyURL
                                    }
                                    observer.on(.next(user))
                                } catch {
                                    observer.onError(ResponseError.unboxParseError)
                                }
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        }
    }
    
    func fetchCountries() -> Observable<[Country]> {
        return Observable.create{ observer in
            let loginObserver = ApiManager.get(path: .countries, params: nil)
            _ = loginObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .login, successCompletionHandler: { (success, json, error) in
                            if success == true, let userObject = json, let countriesArray = userObject["data"] as? [[String: AnyObject]] {
                                var countries = [Country]()
                                for country in countriesArray {
                                    do {
                                        let countryObject: Country = try unbox(dictionary: country)
                                        countries.append(countryObject)
                                    } catch {
                                        observer.onError(ResponseError.unboxParseError)
                                    }
                                }
                                observer.on(.next(countries))
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        }
    }
    
    func checkAccountAvailability(_ email: String, phoneNumber: String) -> Observable<Bool> {
        var dict: [String: AnyObject]!
        if !email.isEmpty {
            dict = ["email": email] as [String : AnyObject]
        }
        
        if !phoneNumber.isEmpty {
            dict = ["contact": phoneNumber] as [String : AnyObject]
        }
        return Observable.create{ observer in
            let loginObserver = ApiManager.post(path: .emailAvailability, params: dict)
            _ = loginObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .emailAvailability, successCompletionHandler: { (success, json, error) in
                            if success == true  {
                                observer.on(.next(true))
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        }
    }
    
    func profileLinkForName(_ name: String) -> Observable<String> {
        let dict = ["name": name]
        
        return Observable.create{ observer in
            let checkNameObserver = ApiManager.get(path: .checkName, params: dict as Dictionary<String, AnyObject>)
            _ = checkNameObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .checkName, successCompletionHandler: { (success, json, error) in
                            if success == true {
                                observer.on(.next(name))
                            } else if success == false, let data = json, let userData = data["data"] as? [String: AnyObject], let suggestion = userData["suggestion"] as? String {
                                observer.on(.next(suggestion))
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        }
    }
    
    func createAccountWithDetails(_ details: [String: AnyObject]) -> Observable<Int> {
        return Observable.create{ observer in
            let loginObserver = ApiManager.post(path: .signup, params: details)
            _ = loginObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .signup, successCompletionHandler: { (success, json, error) in
                            if success == true, let userObject = json, let data = userObject["data"] as? [String: AnyObject], let id = data["id"] as? Int {
                                observer.on(.next(id))
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        }
    }
    
    func verifyOTP(_ otp: String, forUserID userID: Int) -> Observable<User> {
        let dict = ["id": userID,
                    "otp": otp] as Dictionary<String, AnyObject>

        return Observable.create{ observer in
            let loginObserver = ApiManager.post(path: .confirmOTP, params: dict)
            _ = loginObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .confirmOTP, successCompletionHandler: { (success, json, error) in
                            if success == true, let userObject = json, let dataDict = userObject["data"] as? [String: AnyObject], let userDict = dataDict["user"] as? [String: AnyObject] {
                                do {
                                    let user: User = try unbox(dictionary: userDict)
//                                    print(user)
                                    observer.on(.next(user))
                                } catch {
                                    observer.onError(ResponseError.unboxParseError)
                                }
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        }
    }
    
    func resendOTPForID(_ userID: Int) -> Observable<[String: AnyObject]> {
        let params = ["id": userID] as [String: AnyObject]
        return Observable.create{ observer in
            let loginObserver = ApiManager.get(path: .resendOTP, params: params)
            _ = loginObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .resendOTP, successCompletionHandler: { (success, json, error) in
                            if success == true, let userObject = json {
                                observer.on(.next(userObject))
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        }
    }

    func sentOTPToMobileNumber(_ mobile: String) -> Observable<Int> {
        let params = ["contact": mobile] as [String: AnyObject]
        return Observable.create{ observer in
            let loginObserver = ApiManager.post(path: .forgotPassword, params: params)
            _ = loginObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .forgotPassword, successCompletionHandler: { (success, json, error) in
                            if success == true, let userObject = json, let data = userObject["data"] as? [String: AnyObject], let id = data["id"] as? Int {
                                observer.on(.next(id))
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        }

    }
    
    func resetPasswordForID(_ userID: Int, withPassword password: String) -> Observable<User> {
        let dict = ["id": userID,
                    "password": password] as Dictionary<String, AnyObject>
        return Observable.create{ observer in
            let loginObserver = ApiManager.post(path: .resetPassword, params: dict)
            _ = loginObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .resetPassword, successCompletionHandler: { (success, json, error) in
                            if success == true, let userObject = json, let dataDict = userObject["data"] as? [String: AnyObject], let userDict = dataDict["user"] as? [String: AnyObject] {
                                do {
                                    let user: User = try unbox(dictionary: userDict)
//                                    print(user)
                                    observer.on(.next(user))
                                } catch {
                                    observer.onError(ResponseError.unboxParseError)
                                }
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        }
    }

    func facebookLoginWithDict(_ dict: [String: AnyObject]) -> Observable<User> {

        return Observable.create{ observer in
            let loginObserver = ApiManager.post(path: .login, params: dict)
            _ = loginObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .login, successCompletionHandler: { (success, json, error) in
                            if success == true, let userObject = json, let userDict = userObject["data"] as? [String: AnyObject],
                            let dataDict = userDict["user"] as? [String: AnyObject] {
                                do {
                                    let user: User = try unbox(dictionary: dataDict)
                                    observer.on(.next(user))
                                } catch {
                                    observer.onError(ResponseError.unboxParseError)
                                }
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        }
    }

    func fetchDashboardDetails() -> Observable<DashboardInfo> {
        var dict = ["sample": 1]
        if UserStore.shared.isLoggedIn, let userID = UserStore.shared.userID {
            dict["id"] = userID
        }
        
        return Observable.create{ observer in
            let dashboardObserver = ApiManager.get(path: .dashboard, params: dict as Dictionary<String, AnyObject>)
            _ = dashboardObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .dashboard, successCompletionHandler: { (success, json, error) in
                            if success == true, let userObject = json, let dataDict = userObject["data"] as? [String: AnyObject] {
                                do {
                                    let dashboardInfo: DashboardInfo = try unbox(dictionary: dataDict)
                                    observer.on(.next(dashboardInfo))
                                } catch {
                                    observer.onError(ResponseError.unboxParseError)
                                }
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        }
    }
    
    func fetchTopSubjects() -> Observable<[SubjectCategory]> {
        return Observable.create{ observer in
            let topSubObserver = ApiManager.get(path: .topSubjects, params: nil)
            _ = topSubObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .topSubjects, successCompletionHandler: { (success, json, error) in
                            if success == true, let userObject = json, let dataArray = userObject["data"] as? [[String: AnyObject]] {
                                var topSubjects = [SubjectCategory]()
                                for subjectDict in dataArray {
                                    do {
                                        let topSubject : SubjectCategory = try unbox(dictionary: subjectDict)
                                        topSubjects.append(topSubject)
                                    } catch {
                                        //Error
                                    }
                                }
                                observer.onNext(topSubjects)
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        }
    }
    
    func fetchSubjectsWithInCategory(_ categoryId: String, page: Int, query: String) -> Observable<SubjectPagination> {
        let dict = ["id": Int(categoryId)!,
                    "page": String(page),
                    "search": query] as [String : AnyObject]
        
        return Observable.create{ observer in
            let subjectObserver = ApiManager.get(path: .categorySubjects, params: dict)
            _ = subjectObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .categorySubjects, successCompletionHandler: { (success, json, error) in
                            if success == true, let userObject = json {
                                do {
                                    let subjectPagination: SubjectPagination = try unbox(dictionary: userObject)
                                    observer.on(.next(subjectPagination))
                                } catch {
                                    observer.onError(ResponseError.unboxParseError)
                                }
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        }
    }
    
    func fetchFeaturedTrainers(_ page: Int) -> Observable<TutorPagination> {
        var dict = ["page": String(page)]
        
        if UserStore.shared.isLoggedIn, let userID = UserStore.shared.userID {
            dict["id"] = String(userID)
        }
        return Observable.create{ observer in
            let featuredTutorsObserver = ApiManager.get(path: .featuredTutors, params: dict as Dictionary<String, AnyObject>)
            _ = featuredTutorsObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .featuredTutors, successCompletionHandler: { (success, json, error) in
                            if success == true, let userObject = json {
                                do {
                                    let tutorPagination: TutorPagination = try unbox(dictionary: userObject)
                                    observer.on(.next(tutorPagination))
                                } catch {
                                    observer.onError(ResponseError.unboxParseError)
                                }
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        }
    }
    
    func fetchFeaturedPartners(_ page: Int) -> Observable<PartnerPagination> {
        var dict = ["page": String(page)]
        
        if UserStore.shared.isLoggedIn, let userID = UserStore.shared.userID {
            dict["id"] = String(userID)
        }
        return Observable.create{ observer in
            let featuredPartnerObserver = ApiManager.get(path: .featuredPartners, params: dict as Dictionary<String, AnyObject>)
            _ = featuredPartnerObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .featuredPartners, successCompletionHandler: { (success, json, error) in
                            if success == true, let userObject = json {
                                do {
                                    let partnerPagination: PartnerPagination = try unbox(dictionary: userObject)
                                    observer.on(.next(partnerPagination))
                                } catch {
                                    observer.onError(ResponseError.unboxParseError)
                                }
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        }
    }

    func fetchUserDetails() -> Observable<User> {
        let dict = ["id": UserStore.shared.userID ?? 0]
        return Observable.create{ observer in
            let userObserver = ApiManager.get(path: .userDetails, params: dict as Dictionary<String, AnyObject>)
            _ = userObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .userDetails, successCompletionHandler: { (success, json, error) in
                            if success == true, let userObject = json, let dataDict = userObject["data"] as? [String: AnyObject], let userDict = dataDict["user"] as? [String: AnyObject] {
                                do {
                                    let user: User = try unbox(dictionary: userDict)
                                    observer.onNext(user)
                                } catch {
                                    observer.onError(ResponseError.unboxParseError)
                                }
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        }
    }
    
    func fetchSubjects(_ page: Int, _ query: String) -> Observable<SubjectPagination> {
        let dict = ["page": String(page),
                    "search": query]
        
        return Observable.create{ observer in
            let subjectObserver = ApiManager.get(path: .subjects, params: dict as Dictionary<String, AnyObject>)
            _ = subjectObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .subjects, successCompletionHandler: { (success, json, error) in
                            if success == true, let userObject = json {
                                do {
                                    let subjectPagination: SubjectPagination = try unbox(dictionary: userObject)
                                    observer.on(.next(subjectPagination))
                                } catch {
                                    observer.onError(ResponseError.unboxParseError)
                                }
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        }
    }
    
    func fetchLevels(_ page: Int, _ countryID: String, _ query: String) -> Observable<LevelPagination> {
        let dict = ["page": String(page),
                    "country_id": countryID,
                    "search": query]
        
        return Observable.create{ observer in
            let levelObserver = ApiManager.get(path: .levels, params: dict as Dictionary<String, AnyObject>)
            _ = levelObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .levels, successCompletionHandler: { (success, json, error) in
                            if success == true, let userObject = json, let dataDict = userObject["data"] as? [String: AnyObject] {
                                do {
                                    let levelPagination: LevelPagination = try unbox(dictionary: dataDict)
                                    observer.on(.next(levelPagination))
                                } catch {
                                    observer.onError(ResponseError.unboxParseError)
                                }
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        }
    }
    
    func fetchQualifications() -> Observable<[Qualification]> {
        return Observable.create{ observer in
            let qualificationObserver = ApiManager.get(path: .qualifications, params: nil)
            _ = qualificationObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .levels, successCompletionHandler: { (success, json, error) in
                            if success == true, let userObject = json, let dataDict = userObject["data"] as? [String: AnyObject], let qualificationsArray = dataDict["qualification"] as? [[String: AnyObject]] {
                                var qualifications = [Qualification]()
                                for qualificationDict in qualificationsArray {
                                    do {
                                        let qualification: Qualification = try unbox(dictionary: qualificationDict)
                                        qualifications.append(qualification)
                                    } catch {
                                        //Error while parsing
                                    }
                                }
                                observer.on(.next(qualifications))
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        }
    }

    func fetchExperiences() -> Observable<[Experience]> {
        return Observable.create{ observer in
            let experienceObserver = ApiManager.get(path: .experiences, params: nil)
            _ = experienceObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .experiences, successCompletionHandler: { (success, json, error) in
                            if success == true, let userObject = json, let dataDict = userObject["data"] as? [String: AnyObject], let experienceArray = dataDict["experience"] as? [[String: AnyObject]] {
                                var experiences = [Experience]()
                                for experienceDict in experienceArray {
                                    do {
                                        let experience: Experience = try unbox(dictionary: experienceDict)
                                        experiences.append(experience)
                                    } catch {
                                        //Error while parsing
                                    }
                                }
                                observer.on(.next(experiences))
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        }
    }
    
    func changePasswordForID(_ id: String, oldPassword: String, newPassword: String) -> Observable<User> {
        let dict = ["id": id,
                    "old_password": oldPassword,
                    "new_password": newPassword]
        
        return Observable.create{ observer in
            let changePasswordObserver = ApiManager.post(path: .changePassword, params: dict as Dictionary<String, AnyObject>)
            _ = changePasswordObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .changePassword, successCompletionHandler: { (success, json, error) in
                            if success == true, let userObject = json, let dataDict = userObject["data"] as? [String: AnyObject], let userDict = dataDict["user"] as? [String: AnyObject] {
                                do {
                                    let user: User = try unbox(dictionary: userDict)
                                    observer.on(.next(user))
                                } catch {
                                    observer.onError(ResponseError.unboxParseError)
                                }
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        }
    }

    func changeEmailForID(_ id: String, oldEmail: String, newEmail: String) -> Observable<User> {
        let dict = ["id": id,
                    "old_email": oldEmail,
                    "new_email": newEmail]
        
        return Observable.create{ observer in
            let changeEmailObserver = ApiManager.post(path: .changeEmail, params: dict as Dictionary<String, AnyObject>)
            _ = changeEmailObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .changeEmail, successCompletionHandler: { (success, json, error) in
                            if success == true, let userObject = json, let dataDict = userObject["data"] as? [String: AnyObject], let userDict = dataDict["user"] as? [String: AnyObject] {
                                do {
                                    let user: User = try unbox(dictionary: userDict)
                                    observer.on(.next(user))
                                } catch {
                                    observer.onError(ResponseError.unboxParseError)
                                }
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        }
    }
    
    func changePhoneNumberForID(_ id: String, oldNumber: String, newNumber: String) -> Observable<Int> {
        let dict = ["id": id,
                    "old_contact": oldNumber,
                    "new_contact": newNumber]
        
        return Observable.create{ observer in
            let changeMobileObserver = ApiManager.post(path: .changeMobile, params: dict as Dictionary<String, AnyObject>)
            _ = changeMobileObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .changeMobile, successCompletionHandler: { (success, json, error) in
                            if success == true, let userObject = json, let data = userObject["data"] as? [String: AnyObject], let id = data["id"] as? String {
                                observer.on(.next(Int(id)!))
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        }
    }
    
    func fetchLanguages(_ page: Int, query: String) -> Observable<LanguagePagination> {
        let dict = ["page": String(page),
                    "search": query]
        
        return Observable.create{ observer in
            let levelObserver = ApiManager.get(path: .fetchLanguages, params: dict as Dictionary<String, AnyObject>)
            _ = levelObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .fetchLanguages, successCompletionHandler: { (success, json, error) in
                            if success == true, let userObject = json, let dataDict = userObject["data"] as? [String: AnyObject] {
                                do {
                                    let languagePagination: LanguagePagination = try unbox(dictionary: dataDict)
                                    observer.on(.next(languagePagination))
                                } catch {
                                    observer.onError(ResponseError.unboxParseError)
                                }
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        }
    }
    
    func verifyOTPForNewMobile(_ otp: String, forUserID userID: Int, tempContact: String) -> Observable<User> {
        let dict = ["id": userID,
                    "otp": otp,
                    "temp_contact": tempContact] as Dictionary<String, AnyObject>
        
        return Observable.create{ observer in
            let verifyOTPObserver = ApiManager.post(path: .confirmOTP, params: dict)
            _ = verifyOTPObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .confirmOTP, successCompletionHandler: { (success, json, error) in
                            if success == true, let userObject = json, let dataDict = userObject["data"] as? [String: AnyObject], let userDict = dataDict["user"] as? [String: AnyObject] {
                                do {
                                    let user: User = try unbox(dictionary: userDict)
//                                    print(user)
                                    observer.on(.next(user))
                                } catch {
                                    observer.onError(ResponseError.unboxParseError)
                                }
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        }
    }
    
    func toggleNotifications(_ isOn: Bool, deviceName: String, userId: String, deviceId: String, deviceToken: String) -> Observable<String> {
        let dict = ["user_id": userId,
                    "notification_status": isOn ? "1" : "0",
                    "device_id": deviceId,
                    "device_token": deviceToken,
                    "device_type": "iPhone",
                    "device_name": deviceName] as Dictionary<String, AnyObject>
        print(dict)
        return Observable.create{ observer in
            let notificationObserver = ApiManager.post(path: .notifications, params: dict)
            _ = notificationObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .notifications, successCompletionHandler: { (success, json, error) in
                            if success == true, let userObject = json, let message = userObject["message"] as? String {
                                observer.onNext(message)
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        }
    }
    
    
    func overridePhoneNumber(phoneNumber: String, userid: String) -> Observable<Int> {
        let dict = ["id": userid,
                    "contact": phoneNumber]
        return Observable.create{ observer in
            let changeNumberObserver = ApiManager.post(path: .changeNumber, params: dict as Dictionary<String, AnyObject>)
            _ = changeNumberObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .changeNumber, successCompletionHandler: { (success, json, error) in
                            if success == true, let userObject = json, let data = userObject["data"] as? [String: AnyObject], let id = data["id"] as? Int {
                                observer.on(.next(id))
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        }
    }

    func fetchTutorDetailsForID(_ id: String) -> Observable<TutorDetailedInfo> {
        var dict = ["id": id]
        if UserStore.shared.isLoggedIn == true, let userID = UserStore.shared.userID {
            dict["user_id"] = String(userID)
        }

        return Observable.create{ observer in
            let changeNumberObserver = ApiManager.get(path: .fetchTutor, params: dict as Dictionary<String, AnyObject>)
            _ = changeNumberObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .fetchTutor, successCompletionHandler: { (success, json, error) in
                            if success == true, let userObject = json, let data = userObject["data"] as? [String: AnyObject] {
                                do {
                                    let tutor: TutorDetailedInfo = try unbox(dictionary: data)
                                    observer.on(.next(tutor))
                                } catch {
                                    observer.onError(ResponseError.unboxParseError)
                                }
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        }
    }
    
    func fetchgAgencyDetailsForID(_ id: String, andAssetID assetID: String) -> Observable<AgencyDetailedInfo> {
        let dict = ["id": id,
                    "asset_id": assetID]
                
        return Observable.create{ observer in
            let changeNumberObserver = ApiManager.get(path: .fetchAgency, params: dict as Dictionary<String, AnyObject>)
            _ = changeNumberObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .fetchAgency, successCompletionHandler: { (success, json, error) in
                            if success == true, let userObject = json, let data = userObject["data"] as? [String: AnyObject] {
                                do {
                                    let agency: AgencyDetailedInfo = try unbox(dictionary: data)
                                    observer.on(.next(agency))
                                } catch {
                                    observer.onError(ResponseError.unboxParseError)
                                }
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        }
    }
    
    func fetchMySubjectListingAtPage(_ page: Int, forUserID userID: Int) -> Observable<MySubjectPagination> {
        let dict = ["page": String(page),
                    "id": userID] as [String : Any]
        
        return Observable.create{ observer in
            let mySubjectObserver = ApiManager.get(path: .mySubjectListing, params: dict as Dictionary<String, AnyObject>)
            _ = mySubjectObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .mySubjectListing, successCompletionHandler: { (success, json, error) in
                            if success == true, let userObject = json {
                                do {
                                    let mySubjecyPagination: MySubjectPagination = try unbox(dictionary: userObject)
                                    observer.on(.next(mySubjecyPagination))
                                } catch {
                                    observer.onError(ResponseError.unboxParseError)
                                }
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        }
    }

    func addSubject(_ subjectDict: [String: AnyObject]) -> Observable<String> {

        return Observable.create{ observer in
            let addSubjectObserver = ApiManager.post(path: .addSubject, params: subjectDict)
            _ = addSubjectObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .addSubject, successCompletionHandler: { (success, json, error) in
                            if success == true, let userObject = json, let message = userObject["message"] as? String {
                                observer.on(.next(message))
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        }
    }
    
    func changeStatusOfSubject(_ subjectID: String, forUser userID: String) -> Observable<String> {
        let dict = ["subject_id": subjectID,
                    "id": userID] as [String : AnyObject]
        
        return Observable.create{ observer in
            let changeStatusObserver = ApiManager.get(path: .changeSubjectStatus, params: dict)
            _ = changeStatusObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .changeSubjectStatus, successCompletionHandler: { (success, json, error) in
                            if success == true, let userObject = json, let message = userObject["message"] as? String {
                                observer.on(.next(message))
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        }
    }
    
    func deleteSubjectWithID(_ subjectID: String, forUser userID: String) -> Observable<String> {
        let dict = ["id": subjectID,
                    "user_id": userID] as [String : AnyObject]
        
        return Observable.create{ observer in
            let deleteSubjectObserver = ApiManager.get(path: .deleteSubject, params: dict)
            _ = deleteSubjectObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .deleteSubject, successCompletionHandler: { (success, json, error) in
                            if success == true, let userObject = json, let message = userObject["message"] as? String {
                                observer.on(.next(message))
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        }

    }
    
    func editSubject(_ subjectDict: [String: AnyObject]) -> Observable<String> {
        
        return Observable.create{ observer in
            let editSubjectObserver = ApiManager.post(path: .editSubject, params: subjectDict)
            _ = editSubjectObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .changeNumber, successCompletionHandler: { (success, json, error) in
                            if success == true, let userObject = json, let message = userObject["message"] as? String {
                                observer.on(.next(message))
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        }
    }

    func fetchDocumentsForUserID(_ userID: Int) -> Observable<[Document]> {
        let dict = ["user_id": userID] as [String : AnyObject]
        
        return Observable.create{ observer in
            let fetchDocumentsObserver = ApiManager.get(path: .fetchDocuments, params: dict)
            _ = fetchDocumentsObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .fetchDocuments, successCompletionHandler: { (success, json, error) in
                            if success == true, let userObject = json, let documentsArray = userObject["data"] as? [[String: AnyObject]] {
                                var documents = [Document]()
                                for documentDict in documentsArray {
                                    do {
                                        let document: Document = try unbox(dictionary: documentDict)
                                        documents.append(document)
                                    } catch {
                                        observer.onError(ResponseError.unboxParseError)
                                    }
                                }
                                observer.on(.next(documents))
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        }
    }
    
    func renameDocumentForUserID(_ userID: Int, documentID: String, desc: String) -> Observable<String> {
        let dict = ["user_id": String(userID),
                    "doc_id": documentID,
                    "doc_desc": desc]
        return Observable.create{ observer in
            let renameDocumentObserver = ApiManager.post(path: .renameDocument, params: dict as Dictionary<String, AnyObject>)
            _ = renameDocumentObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .renameDocument, successCompletionHandler: { (success, json, error) in
                            if success == true, let userObject = json, let message = userObject["message"] as? String {
                                observer.on(.next(message))
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        }
    }
    
    func deleteDocumentForUserID(_ userID: Int, documentID: String) -> Observable<String> {
        let dict = ["doc_id": documentID,
                    "user_id": userID] as [String : AnyObject]
        
        return Observable.create{ observer in
            let deleteDocumentObserver = ApiManager.get(path: .deleteDocument, params: dict)
            _ = deleteDocumentObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .deleteDocument, successCompletionHandler: { (success, json, error) in
                            if success == true, let userObject = json, let message = userObject["message"] as? String {
                                observer.on(.next(message))
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        }
    }

    func editProfileWithDict(_ dict: [String: AnyObject]) -> Observable<User> {

        return Observable.create{ observer in
            let loginObserver = ApiManager.post(path: .editProfile, params: dict)
            _ = loginObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .editProfile, successCompletionHandler: { (success, json, error) in
                            if success == true, let userObject = json, let dataDict = userObject["data"] as? [String: AnyObject], let userDict = dataDict["user"] as? [String: AnyObject] {
                                do {
                                    let user: User = try unbox(dictionary: userDict)
//                                    print(user)
                                    observer.on(.next(user))
                                } catch {
                                    observer.onError(ResponseError.unboxParseError)
                                }
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        }
    }
    
    func fetchShortlistUsersForUserID(_ id: Int, page: Int, query: String) -> Observable<ShortlistedUserPagination> {
        let dict = ["page": page,
                    "id": id,
                    "search": query] as [String : AnyObject]
        return Observable.create{ observer in
            let myShortlistObserver = ApiManager.get(path: .fetchShortlist, params: dict)
            _ = myShortlistObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .fetchShortlist, successCompletionHandler: { (success, json, error) in
                            if success == true, let userObject = json, let dataDict = userObject["data"] as? [String: AnyObject] {
                                do {
                                    let shortlistedUserPagination: ShortlistedUserPagination = try unbox(dictionary: dataDict)
                                    observer.on(.next(shortlistedUserPagination))
                                } catch {
                                    observer.onError(ResponseError.unboxParseError)
                                }
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        }
    }
    
    func addToShortlistForUser(_ id: String, shortlistUserID: String, reason: String) -> Observable<String> {
        let dict = ["user_id": id,
                    "favorite_id": shortlistUserID,
                    "reason_favorite": reason] as [String : AnyObject]
        
        return Observable.create{ observer in
            let deleteDocumentObserver = ApiManager.get(path: .addShortlist, params: dict)
            _ = deleteDocumentObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .deleteDocument, successCompletionHandler: { (success, json, error) in
                            if success == true, let userObject = json, let message = userObject["message"] as? String {
                                observer.on(.next(message))
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        }

    }
    
    func editShortlistForUser(_ id: String, shortlistUserID: String, reason: String) -> Observable<String> {
        let dict = ["user_id": id,
                    "favorite_id": shortlistUserID,
                    "reason_favorite": reason] as [String : AnyObject]

        return Observable.create{ observer in
            let deleteDocumentObserver = ApiManager.get(path: .editShortlist, params: dict)
            _ = deleteDocumentObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .deleteDocument, successCompletionHandler: { (success, json, error) in
                            if success == true, let userObject = json, let message = userObject["message"] as? String {
                                observer.on(.next(message))
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        }

    }
    
    func deleteShortlistForUser(_ id: String, shortlistUserID: String) -> Observable<String> {
        let dict = ["user_id": id,
                    "favorite_id": shortlistUserID] as [String : AnyObject]

        return Observable.create{ observer in
            let deleteDocumentObserver = ApiManager.get(path: .deleteShortlist, params: dict)
            _ = deleteDocumentObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .deleteDocument, successCompletionHandler: { (success, json, error) in
                            if success == true, let userObject = json, let message = userObject["message"] as? String {
                                observer.on(.next(message))
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        }
    }
    
    func findUsers(_ filterDict: [String: AnyObject], isTutor: Bool) -> Observable<MapItemPagination> {
        let urlPath = isTutor ? URI.filterTutor : URI.filterStudent
        return Observable.create{ observer in
            let filterObserver = ApiManager.get(path: urlPath, params: filterDict)
            _ = filterObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .filter, successCompletionHandler: { (success, json, error) in
                            if success == true, let userObject = json {
                                do {
                                    let mapItem: MapItemPagination = try unbox(dictionary: userObject)
                                    observer.on(.next(mapItem))
                                } catch {
                                    observer.onError(ResponseError.unboxParseError)
                                }
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        }
    }
    
    func fetchFilterCount(_ filterDict: [String: AnyObject], isTutor: Bool) -> Observable<Int> {
        let urlPath = isTutor ? URI.filterTutorCount : URI.filterStudentCount

        return Observable.create{ observer in
            let filterObserver = ApiManager.get(path: urlPath, params: filterDict)
            _ = filterObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .filter, successCompletionHandler: { (success, json, error) in
                            if success == true, let userObject = json, let count = userObject["data"] as? Int {
                                observer.on(.next(count))
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        }
    }

    
    func getCountryID(_ country: String) -> Observable<String> {
        let dict = ["code": country]
        return Observable.create{ observer in
            let filterObserver = ApiManager.get(path: .countryCode, params: dict as Dictionary<String, AnyObject>)
            _ = filterObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .countryCode, successCompletionHandler: { (success, json, error) in
                            if success == true, let userObject = json, let countryDict = userObject["data"] as? [String: AnyObject] {
                                if let countryID = countryDict["id"] as? Int {
                                    observer.on(.next(String(countryID)))
                                } else {
                                    observer.on(.next("178"))
                                }
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        }
    }
    
    func fetchStudentDetailsForID(_ id: String) -> Observable<StudentDetailedInfo> {
        var dict = ["id": id]
        if UserStore.shared.isLoggedIn == true, let userID = UserStore.shared.userID {
            dict["user_id"] = String(userID)
        }
        
        return Observable.create{ observer in
            let changeNumberObserver = ApiManager.get(path: .fetchStudent, params: dict as Dictionary<String, AnyObject>)
            _ = changeNumberObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .fetchStudent, successCompletionHandler: { (success, json, error) in
                            if success == true, let userObject = json, let data = userObject["data"] as? [String: AnyObject] {
                                do {
                                    let student: StudentDetailedInfo = try unbox(dictionary: data)
                                    observer.on(.next(student))
                                } catch {
                                    observer.onError(ResponseError.unboxParseError)
                                }
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        }
    }
    
    func reportUserWithUserID(_ reportedUserID: String, comment: String, nature: String, userID: String) -> Observable<String> {
        let dict = ["sender_id": userID,
                    "inappropriate_user_id": reportedUserID,
                    "comment": comment,
                    "report_nature": nature]

        return Observable.create{ observer in
            let reportObserver = ApiManager.post(path: .reportUser, params: dict as Dictionary<String, AnyObject>)
            _ = reportObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .reportUser, successCompletionHandler: { (success, json, error) in
                            if success == true, let userObject = json, let message = userObject["message"] as? String {
                                observer.on(.next(message))
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        }
    }
    
    func enquireAgencyWithID(_ agencyID: String, assetID: String, name: String, email: String, phone: String, preferredContact: String, message: String, userID: String, courseID: String?) -> Observable<String> {
        var dict = ["asset_id": assetID,
                    "agency_id": assetID,
                    "email": email,
                    "full_name": name,
                    "contact": phone,
                    "message": message,
                    "preferred_contact": preferredContact]
        if let course = courseID {
            dict["course_id"] = course
        }
        if !userID.isEmpty {
            dict["user_id"] = userID
        }
        
        return Observable.create{ observer in
            let reportObserver = ApiManager.post(path: .enquire, params: dict as Dictionary<String, AnyObject>)
            _ = reportObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .enquire, successCompletionHandler: { (success, json, error) in
                            if success == true, let userObject = json, let message = userObject["message"] as? String {
                                observer.on(.next(message))
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        }
    }

    func fetchNotificationsForUserID(_ userID: String, deviceID: String, page: Int) -> Observable<NotificationPagination> {
        let dict = ["page": page,
                    "user_id": userID,
                    "device_id": deviceID] as [String : AnyObject]
        return Observable.create{ observer in
            let myShortlistObserver = ApiManager.get(path: .fetchNotifications, params: dict)
            _ = myShortlistObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .fetchNotifications, successCompletionHandler: { (success, json, error) in
                            if success == true, let userObject = json {
                                do {
                                    let notiticationPagination: NotificationPagination = try unbox(dictionary: userObject)
                                    observer.onNext(notiticationPagination)
                                } catch {
                                    observer.onError(ResponseError.unboxParseError)
                                }
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        }
    }
    
    func fetchReviewsForUserID(_ userID: Int, page: Int, search: String) -> Observable<MyReviewPagination> {
        let dict = ["page": page,
                    "id": userID,
                    "search": search] as [String : AnyObject]
        return Observable.create{ observer in
            let myShortlistObserver = ApiManager.get(path: .fetchReviews, params: dict)
            _ = myShortlistObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .fetchReviews, successCompletionHandler: { (success, json, error) in
                            if success == true, let userObject = json {
                                do {
                                    let myReviewPagination: MyReviewPagination = try unbox(dictionary: userObject)
                                    observer.onNext(myReviewPagination)
                                } catch {
                                    observer.onError(ResponseError.unboxParseError)
                                }
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        }
    }
    
    func requestReviewForUserID(_ userID: Int, studentName: String, studentEmail: String) -> Observable<String> {
        let dict = ["student_email": studentEmail,
                    "user_id": userID,
                    "student_name": studentEmail] as [String : AnyObject]
        return Observable.create{ observer in
            let myShortlistObserver = ApiManager.post(path: .requestReview, params: dict)
            _ = myShortlistObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .requestReview, successCompletionHandler: { (success, json, error) in
                            if success == true, let _ = json {
                                observer.onNext("Success")
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        }
    }
    
    func fetchPlansForCountryID(_ countryID: String) -> Observable<[Plan]> {
        let dict = ["id": Int(countryID)!] as [String : AnyObject]
        return Observable.create{ observer in
            let myShortlistObserver = ApiManager.get(path: .plans, params: dict)
            _ = myShortlistObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .plans, successCompletionHandler: { (success, json, error) in
                            if success == true, let userObject = json, let dataDict = userObject["data"] as? [String: AnyObject], let plansArray = dataDict["plans"] as? [[String: AnyObject]] {
                                var planArray = [Plan]()
                                for data in plansArray {
                                    do {
                                        let plan: Plan = try unbox(dictionary: data)
                                        planArray.append(plan)
                                    } catch {
                                        //Do nothing
                                    }
                                }
                                observer.onNext(planArray)
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        }
    }
    
    func fetchEphemeralKeyForUserID(_ userID: String) -> Observable<String> {
        let dict = ["user_id": Int(userID)!,
                    "api_version": "2018-07-27"] as [String : AnyObject]
        return Observable.create{ observer in
            let myShortlistObserver = ApiManager.get(path: .fetchEphemeralKey, params: dict)
            _ = myShortlistObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .fetchEphemeralKey, successCompletionHandler: { (success, json, error) in
                            if success == true, let userObject = json, let dataDict = userObject["data"] as? [String: AnyObject], let ephemeralKey = dataDict["id"] as? String {
                                observer.onNext(ephemeralKey)
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        }
    }
    
    func subscribeToPlanWithStripeToken(_ stripeToken: String, userID: String, planID: String, couponCode: String?) -> Observable<String> {
        var dict = ["plan_id": planID,
                    "user_id": userID,
                    "stripe_token": stripeToken]
        if let coupon = couponCode, !coupon.isEmpty {
            dict["coupon_id"] = coupon
        }
        print(dict)
        return Observable.create{ observer in
            let subscribeObserver = ApiManager.post(path: .subscribe, params: dict as [String: AnyObject])
            _ = subscribeObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .requestReview, successCompletionHandler: { (success, json, error) in
                            if success == true, let userObject = json {
                                if let dataDict = userObject["data"] as? [String: AnyObject], let userDict = dataDict["user"] as? [String: AnyObject] {
                                    do {
                                        let user: User = try unbox(dictionary: userDict)
                                        let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                        appDelegate.user = user
                                    } catch {
                                        //Do nothing
                                    }
                                }
                                if let message = userObject["message"] as? String {
                                    observer.onNext(message)
                                }
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        }
    }

    func subscribeToPlanWithCardID(_ cardID: String, userID: String, planID: String, couponCode: String?) -> Observable<String> {
        var dict = ["plan_id": planID,
                    "user_id": userID,
                    "card_id": cardID]
        if let coupon = couponCode, !coupon.isEmpty {
            dict["coupon_id"] = coupon
        }
        print(dict)
        return Observable.create{ observer in
            let subscribeObserver = ApiManager.post(path: .subscribe, params: dict as [String: AnyObject])
            _ = subscribeObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .requestReview, successCompletionHandler: { (success, json, error) in
                            if success == true, let userObject = json {
                                if let dataDict = userObject["data"] as? [String: AnyObject], let userDict = dataDict["user"] as? [String: AnyObject] {
                                    do {
                                        let user: User = try unbox(dictionary: userDict)
                                        let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                        appDelegate.user = user
                                    } catch {
                                        //Do nothing
                                    }
                                }
                                if let message = userObject["message"] as? String {
                                    observer.onNext(message)
                                }
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        }
    }
    
    func applyCouponCode(_ couponCode: String, toPlanID planID: String, countryID: String) -> Observable<[String: String]> {
        let dict = ["id": Int(countryID)!,
                    "coupon": couponCode,
                    "plan_id": planID] as [String : AnyObject]

        print(dict)
        return Observable.create{ observer in
            let couponObserver = ApiManager.get(path: .plans, params: dict)
            _ = couponObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .plans, successCompletionHandler: { (success, json, error) in
                            if success == true, let userObject = json, let dataDict = userObject["data"] as? [String: AnyObject], let couponDict = dataDict["coupon"] as? [String: AnyObject] {
                                var resp = ["coupon": couponCode]
                                if let discount = couponDict["discount"] as? String {
                                    resp["discount"] = discount
                                }
                                if let extraMonth = couponDict["trial_month"] as? String {
                                    resp["extraMonth"] = extraMonth
                                }
                                observer.onNext(resp)
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        }
    }
    
    func unsubscribePlanForUserID(_ userID: String) -> Observable<String> {
        let dict = ["user_id": userID]
        return Observable.create{ observer in
            let subscribeObserver = ApiManager.get(path: .unsubscribe, params: dict as [String: AnyObject])
            _ = subscribeObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .unsubscribe, successCompletionHandler: { (success, json, error) in
                            if success == true, let userObject = json {
                                if let dataDict = userObject["data"] as? [String: AnyObject], let userDict = dataDict["user"] as? [String: AnyObject] {
                                    do {
                                        let user: User = try unbox(dictionary: userDict)
                                        let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                        appDelegate.user = user
                                    } catch {
                                        //Do nothing
                                    }
                                }
                                if let message = userObject["message"] as? String {
                                    observer.onNext(message)
                                }
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        }
    }
    
    func fetchSavedCardsForUserID(_ userID: String) -> Observable<[SavedCard]> {
        let dict = ["user_id": userID]
        return Observable.create{ observer in
            let subscribeObserver = ApiManager.get(path: .savedCards, params: dict as [String: AnyObject])
            _ = subscribeObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .savedCards, successCompletionHandler: { (success, json, error) in
                            if success == true, let userObject = json, let dataArray = userObject["data"] as? [[String: AnyObject]] {
                                var cards = [SavedCard]()
                                for cardDict in dataArray {
                                    do {
                                        let card: SavedCard = try unbox(dictionary: cardDict)
                                        cards.append(card)
                                    } catch {
                                        //Do nothing
                                    }

                                }
                                observer.onNext(cards)
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        }
    }
    
    
    
    func deleteCardForUserID(_ userID: String, cardID: String) -> Observable<[SavedCard]> {
        let dict = ["user_id": userID,
                    "card_id": cardID]
        return Observable.create{ observer in
            let subscribeObserver = ApiManager.get(path: .deleteCard, params: dict as [String: AnyObject])
            _ = subscribeObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .deleteCard, successCompletionHandler: { (success, json, error) in
                            if success == true, let userObject = json, let dataArray = userObject["data"] as? [[String: AnyObject]] {
                                var cards = [SavedCard]()
                                for cardDict in dataArray {
                                    do {
                                        let card: SavedCard = try unbox(dictionary: cardDict)
                                        cards.append(card)
                                    } catch {
                                        //Do nothing
                                    }
                                }
                                observer.onNext(cards)
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        }
    }
    
    func addCardToUserID(_ userID: String, stripeToken: String) -> Observable<[SavedCard]> {
        let dict = ["user_id": userID,
                    "stripe_token": stripeToken]
        return Observable.create{ observer in
            let subscribeObserver = ApiManager.post(path: .addCard, params: dict as [String: AnyObject])
            _ = subscribeObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .addCard, successCompletionHandler: { (success, json, error) in
                            if success == true, let userObject = json, let dataArray = userObject["data"] as? [[String: AnyObject]] {
                                var cards = [SavedCard]()
                                for cardDict in dataArray {
                                    do {
                                        let card: SavedCard = try unbox(dictionary: cardDict)
                                        cards.append(card)
                                    } catch {
                                        //Do nothing
                                    }
                                }
                                observer.onNext(cards)
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        }
    }
    
    func fetchUserSchedules(_ userID: Int) -> Observable<UserCalendar> {
        let dict = ["id": userID]
        
        return Observable.create{ observer in
            let calendarObserver = ApiManager.get(path: .userCalendar, params: dict as [String: AnyObject])
            _ = calendarObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .userCalendar, successCompletionHandler: { (success, json, error) in
                            if success == true, let userObject = json, let dataObject = userObject["data"] as? [String: AnyObject] {
                                observer.onNext(Utilities.getSchedulesDictFromData(dataObject))
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        }
    }
    
    func addEventWithComment(_ comment: String, dayName: String, hours: String, eventIDs: [String], userID: Int) -> Observable<String> {
        let dict = ["user_id": String(userID),
                    "dayname": dayName,
                    "hours": hours,
                    "comments": comment,
                    "calender_events": eventIDs.joined(separator: ",")] as [String: AnyObject]
        
        return Observable.create{ observer in
            let calendarObserver = ApiManager.post(path: .addEvent, params: dict)
            _ = calendarObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .addEvent, successCompletionHandler: { (success, json, error) in
                            if success == true, let userObject = json, let message = userObject["message"] as? String {
                                observer.onNext(message)
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        }
    }
    
    func editEventWithID(_ id: String, comment: String, eventIDs: [String]) -> Observable<String> {
        let dict = ["id": id,
                    "comments": comment,
                    "calender_events": eventIDs.joined(separator: ",")] as [String: AnyObject]
        
        return Observable.create{ observer in
            let calendarObserver = ApiManager.post(path: .editEvent, params: dict)
            _ = calendarObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .editEvent, successCompletionHandler: { (success, json, error) in
                            if success == true, let userObject = json, let message = userObject["message"] as? String {
                                observer.onNext(message)
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        }
    }

    func deleteEventWithID(_ eventID: String) -> Observable<String> {
        let dict = ["id": eventID] as [String: AnyObject]
        
        return Observable.create{ observer in
            let calendarObserver = ApiManager.get(path: .deleteEvent, params: dict)
            _ = calendarObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .deleteEvent, successCompletionHandler: { (success, json, error) in
                            if success == true, let userObject = json, let message = userObject["message"] as? String {
                                observer.onNext(message)
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        }
    }

    func suggestSubject(name: String, userID: String, countryID: String) -> Observable<String> {
        let dict = ["name": name,
                    "user_id": userID,
                    "country_id": countryID] as [String: AnyObject]
        
        return Observable.create{ observer in
            let suggestSubObserver = ApiManager.post(path: .requestSubject, params: dict)
            _ = suggestSubObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .requestSubject, successCompletionHandler: { (success, json, error) in
                            if success == true, let userObject = json, let message = userObject["message"] as? String {
                                observer.onNext(message)
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        }
    }
    
    func fetchSubjectDetailWithID(_ subjectID: String, userID: String) -> Observable<MySubject> {
        let dict = ["id": userID,
                    "subject_id": subjectID] as [String: AnyObject]
        
        return Observable.create{ observer in
            let calendarObserver = ApiManager.get(path: .mySubjectListing, params: dict)
            _ = calendarObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .mySubjectListing, successCompletionHandler: { (success, json, error) in
                            if success == true, let userObject = json, let dataDict = userObject["data"] as? [String: AnyObject], let subjectDict = dataDict["subject"] as? [String: AnyObject] {
                                do {
                                    let mySubject: MySubject = try unbox(dictionary: subjectDict)
                                    observer.on(.next(mySubject))
                                } catch {
                                    observer.onError(ResponseError.unboxParseError)
                                }
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        }
    }
    
    func fetchShoutoutUsers(_ filterDict: [String: AnyObject]) -> Observable<Shoutout> {
        return Observable.create{ observer in
            let filterObserver = ApiManager.get(path: .filterTutor, params: filterDict)
            _ = filterObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .filterTutor, successCompletionHandler: { (success, json, error) in
                            if success == true, let userObject = json, let dataDict = userObject["data"] as? [String: AnyObject] {
                                do {
                                    let shoutoutModal: Shoutout = try unbox(dictionary: dataDict)
                                    observer.on(.next(shoutoutModal))
                                } catch {
                                    observer.onError(ResponseError.unboxParseError)
                                }
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        }
    }
    
    func fetchUserChatBox(_ userID: Int) -> Observable<[Chat]> {
        let params = ["id": userID] as [String: AnyObject]
        return Observable.create{ observer in
            let chatObserver = ApiManager.get(path: .chatBox, params: params)
            _ = chatObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .chatBox, successCompletionHandler: { (success, json, error) in
                            if success == true, let userObject = json, let dataDict = userObject["data"] as? [String: AnyObject], let chatArray = dataDict["detail"] as? [[String: AnyObject]] {
                                var chats = [Chat]()
                                for chatDict in chatArray {
                                    do {
                                        let chat: Chat = try unbox(dictionary: chatDict)
                                        chats.append(chat)
                                    } catch {
                                        observer.onError(ResponseError.unboxParseError)
                                    }
                                }
                                observer.on(.next(chats))

                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        }
    }
    
    func fetchMessagesForReceiverID(_ receiverID: String, senderID: String) -> Observable<[Message]> {
        let params = ["user_id": receiverID,
                      "sender_id": senderID] as [String: AnyObject]
        return Observable.create{ observer in
            let messagesObserver = ApiManager.get(path: .userMessages, params: params)
            _ = messagesObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .userMessages, successCompletionHandler: { (success, json, error) in
                            if success == true, let userObject = json, let dataDict = userObject["data"] as? [String: AnyObject], let messageArray = dataDict["messages"] as? [[String: AnyObject]] {
                                var messages = [Message]()
                                for messageDict in messageArray {
                                    let message = Message(responseDict: messageDict as NSDictionary)
                                    messages.append(message)
                                }
                                observer.on(.next(messages))
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        }
    }
    
    func sendMessageToUserID(_ receiverID: String, senderID: String, message: String) -> Observable<Message> {
        let dict = ["receiver_id": receiverID,
                    "sender_id": senderID,
                    "message": message] as [String: AnyObject]
        
        return Observable.create{ observer in
            let suggestSubObserver = ApiManager.post(path: .sendMessage, params: dict)
            _ = suggestSubObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .sendMessage, successCompletionHandler: { (success, json, error) in
                            if success == true, let userObject = json, let messge = userObject["message"] as? String {
                                let message = Message(responseDict: userObject["data"] as! NSDictionary)
                                message.msg = (userObject["message"] as? String)!
                                observer.onNext(message)
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        }
    }
    
    func updateEnquiryCount(agencyID: String, assetID: String, userID: String, courseID: String?) -> Observable<String> {
        var dict = ["agency_id": agencyID,
                    "asset_id": assetID,
                    "user_id": userID]
        if let course = courseID {
            dict["course_id"] = course
        }
        
        return Observable.create{ observer in
            let suggestSubObserver = ApiManager.post(path: .enquiryCount, params: dict as [String: AnyObject])
            _ = suggestSubObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .enquiryCount, successCompletionHandler: { (success, json, error) in
                            if success == true, let userObject = json, let message = userObject["message"] as? String {
                                observer.onNext(message)
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        }
    }
    
    //DEEPAK
    
    func sendShoutoutMessage(params:[String:AnyObject]) -> Observable<String> {
        return Observable.create{ observer in
            let reportObserver = ApiManager.post(path: .postShoutout, params: params as Dictionary<String, AnyObject>)
            _ = reportObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .postShoutout, successCompletionHandler: { (success, json, error) in
                            if success == true, let userObject = json, let message = userObject["message"] as? String {
                                observer.on(.next(message))
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        }
    }
    
    func fetchMessageLimits(_ userID: Int) -> Observable<Limits>{
        let params = ["id": userID] as [String: AnyObject]
        return Observable.create{ observer in
            let chatObserver = ApiManager.get(path: .limits, params: params)
            _ = chatObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .limits, successCompletionHandler: { (success, json, error) in
                            if success == true, let userObject = json, let dataDict = userObject["data"] as? [String: AnyObject] {
                                do {
                                    let lmt: Limits = try unbox(dictionary: dataDict)
                                    observer.on(.next(lmt))
                                } catch {
                                    observer.onError(ResponseError.unboxParseError)
                                }
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        }
    }

    
}
