//
//  TueetorService.swift
//  Tueetor
//
//  Created by Phaninder on 15/03/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import Foundation
import RxSwift

protocol TueetorService {

    func loginUserWithEmail(_ email: String, password: String) -> Observable<User>
 
    func fetchCountries() -> Observable<[Country]>
    
    func checkAccountAvailability(_ email: String, phoneNumber: String) -> Observable<Bool>

    func profileLinkForName(_ name: String) -> Observable<String>
    
    func createAccountWithDetails(_ details: [String: AnyObject]) -> Observable<Int>
    
    func verifyOTP(_ otp: String, forUserID userID: Int) -> Observable<User>

    func resendOTPForID(_ userID: Int) -> Observable<[String: AnyObject]>
    
    func sentOTPToMobileNumber(_ mobile: String) -> Observable<Int>
    
    func resetPasswordForID(_ userID: Int, withPassword password: String) -> Observable<User>
    
    func facebookLoginWithDict(_ dict: [String: AnyObject]) -> Observable<User>

    func fetchDashboardDetails() -> Observable<DashboardInfo>
    
    func fetchTopSubjects() -> Observable<[SubjectCategory]>
    
    func fetchSubjectsWithInCategory(_ categoryId: String, page: Int, query: String) -> Observable<SubjectPagination>
    
    func fetchFeaturedTrainers(_ page: Int) -> Observable<TutorPagination>
    
    func fetchFeaturedPartners(_ page: Int) -> Observable<PartnerPagination>

    func fetchUserDetails() -> Observable<User>
    
    func fetchSubjects(_ page: Int, _ query: String) -> Observable<SubjectPagination>
    
    func fetchLevels(_ page: Int, _ countryID: String, _ query: String) -> Observable<LevelPagination>
    
    func fetchQualifications() -> Observable<[Qualification]>

    func fetchExperiences() -> Observable<[Experience]>

    func changePasswordForID(_ id: String, oldPassword: String, newPassword: String) -> Observable<User>
 
    func changeEmailForID(_ id: String, oldEmail: String, newEmail: String) -> Observable<User>

    func changePhoneNumberForID(_ id: String, oldNumber: String, newNumber: String) -> Observable<Int>

    func fetchLanguages(_ page: Int, query: String) -> Observable<LanguagePagination>

    func verifyOTPForNewMobile(_ otp: String, forUserID userID: Int, tempContact: String) -> Observable<User>
    
    func toggleNotifications(_ isOn: Bool, deviceName: String, userId: String, deviceId: String, deviceToken: String) -> Observable<String>
    
    func overridePhoneNumber(phoneNumber: String, userid: String) -> Observable<Int>
    
    func fetchTutorDetailsForID(_ id: String) -> Observable<TutorDetailedInfo>
    
    func fetchgAgencyDetailsForID(_ id: String, andAssetID assetID: String) -> Observable<AgencyDetailedInfo>
    
    func fetchMySubjectListingAtPage(_ page: Int, forUserID userID: Int) -> Observable<MySubjectPagination>
    
    func addSubject(_ subjectDict: [String: AnyObject]) -> Observable<String>
 
    func changeStatusOfSubject(_ subjectID: String, forUser userID: String) -> Observable<String>
    
    func deleteSubjectWithID(_ subjectID: String, forUser userID: String) -> Observable<String>
    
    func editSubject(_ subjectDict: [String: AnyObject]) -> Observable<String>

    func fetchDocumentsForUserID(_ userID: Int) -> Observable<[Document]>
    
    func renameDocumentForUserID(_ userID: Int, documentID: String, desc: String) -> Observable<String>
    
    func deleteDocumentForUserID(_ userID: Int, documentID: String) -> Observable<String>
    
    func editProfileWithDict(_ dict: [String: AnyObject]) -> Observable<User>
    
    func fetchShortlistUsersForUserID(_ id: Int, page: Int, query: String) -> Observable<ShortlistedUserPagination>
    
    func addToShortlistForUser(_ id: String, shortlistUserID: String, reason: String) -> Observable<String>
    
    func editShortlistForUser(_ id: String, shortlistUserID: String, reason: String) -> Observable<String>
    
    func deleteShortlistForUser(_ id: String, shortlistUserID: String) -> Observable<String>
    
    func findUsers(_ filterDict: [String: AnyObject], isTutor: Bool) -> Observable<MapItemPagination>
    
    func fetchFilterCount(_ filterDict: [String: AnyObject], isTutor: Bool) -> Observable<Int>
    
    func getCountryID(_ country: String) -> Observable<String>
    
    func fetchStudentDetailsForID(_ id: String) -> Observable<StudentDetailedInfo>

    func reportUserWithUserID(_ reportedUserID: String, comment: String, nature: String, userID: String) -> Observable<String>
    
    func enquireAgencyWithID(_ agencyID: String, assetID: String, name: String, email: String, phone: String, preferredContact: String, message: String, userID: String, courseID: String?) -> Observable<String>
    
    func sendShoutoutMessage(params:[String : AnyObject]) -> Observable<String> 

    func fetchNotificationsForUserID(_ userID: String, deviceID: String, page: Int) -> Observable<NotificationPagination>
    
    func fetchReviewsForUserID(_ userID: Int, page: Int, search: String) -> Observable<MyReviewPagination>

    func requestReviewForUserID(_ userID: Int, studentName: String, studentEmail: String) -> Observable<String>
    
    func fetchPlansForCountryID(_ countryID: String) -> Observable<[Plan]>
    
    func fetchEphemeralKeyForUserID(_ userID: String) -> Observable<String>
    
    func subscribeToPlanWithStripeToken(_ stripeToken: String, userID: String, planID: String, couponCode: String?) -> Observable<String>
    
    func subscribeToPlanWithCardID(_ cardID: String, userID: String, planID: String, couponCode: String?) -> Observable<String>
    
    func applyCouponCode(_ couponCode: String, toPlanID planID: String, countryID: String) -> Observable<[String: String]>
    
    func unsubscribePlanForUserID(_ userID: String) -> Observable<String>
    
    func fetchSavedCardsForUserID(_ userID: String) -> Observable<[SavedCard]>
    
    func deleteCardForUserID(_ userID: String, cardID: String) -> Observable<[SavedCard]>
    
    func addCardToUserID(_ userID: String, stripeToken: String) -> Observable<[SavedCard]>
    
    func fetchUserSchedules(_ userID: Int) -> Observable<UserCalendar>
    
    func addEventWithComment(_ comment: String, dayName: String, hours: String, eventIDs: [String], userID: Int) -> Observable<String>
    
    func editEventWithID(_ id: String, comment: String, eventIDs: [String]) -> Observable<String>
    
    func deleteEventWithID(_ eventID: String) -> Observable<String>
    
    func suggestSubject(name: String, userID: String, countryID: String) -> Observable<String>
    
    func fetchSubjectDetailWithID(_ subjectID: String, userID: String) -> Observable<MySubject>
    
    func fetchShoutoutUsers(_ filterDict: [String: AnyObject]) -> Observable<Shoutout>
    
   // func fetchUserChatBox(_ userID: Int) -> Observable<[Chat]>
    func fetchUserChatBox(_ page: String,_ userID: Int) -> Observable<MessagePagination>
    
    //func fetchMessagesForReceiverID(_ receiverID: String, senderID: String) -> Observable<[Message]>
    func fetchMessagesForReceiverID(_ receiverID: String, senderID: String,page: Int) -> Observable<ChatPagination>
    
    func sendMessageToUserID(_ receiverID: String, senderID: String, message: String) -> Observable<Message>
    
    func updateEnquiryCount(agencyID: String, assetID: String, userID: String, courseID: String?) -> Observable<String>
    func fetchMessageLimits(_ userID: Int) -> Observable<Limits>

}

