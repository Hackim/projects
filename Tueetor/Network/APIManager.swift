//
//  APIManager.swift
//  Tueetor
//
//  Created by Phaninder on 15/03/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import Foundation
import Unbox
import RxSwift
import Alamofire

class ApiManager {
    
    static let shared = ApiManager()
    var apiService: TueetorService = DataService()
    
    fileprivate init() {
        //super.init()
    }
    
    class func getResponseErrorWithStatusCode(_ statusCode:Int) -> ResponseError {
        switch statusCode {
        case ResponseStatusCode.notFound.rawValue:
            return ResponseError.notFoundError
        case ResponseStatusCode.badRequest.rawValue:
            return ResponseError.badRequestError
        case ResponseStatusCode.timeout.rawValue:
            return ResponseError.timeoutError
        case ResponseStatusCode.internalServer.rawValue:
            return ResponseError.internalServerError
        default:
            return ResponseError.unknownError
        }
    }
    
    class func dataTask(request: NSMutableURLRequest) -> Observable<ResponseObject> {
        
        return Observable.create { observer in
            
            let urlconfig = URLSessionConfiguration.default
            urlconfig.timeoutIntervalForRequest = 90
            urlconfig.timeoutIntervalForResource = 90
            
            let session = URLSession(configuration: urlconfig)
            let dataTask = session.dataTask(with: request as URLRequest) { (data, response, error) -> Void in
                if let error_ = error {
                    observer.on(.error(error_))
                } else if let response = response as? HTTPURLResponse {
                    let responseObject = (response, data)
                    observer.on(.next(responseObject))
                }
            }
            dataTask.resume()
            return Disposables.create {
                dataTask.cancel()
            }
        }
    }
    
    class func post(path: URI, params: Dictionary<String, AnyObject>? = nil) -> Observable<ResponseObject> {
        var parameters = params
        var pathString = path.rawValue
        
        if path == .addSubject ||
            path == .editSubject ||
            path == .editProfile ||
            path == .addEvent {
            if let paramDict = parameters, let id = paramDict["user_id"] as? String {
                pathString += String(id)
                parameters!["user_id"] = nil
            }
        } else if path == .editEvent {
            if let paramDict = parameters, let id = paramDict["id"] as? String {
                pathString += String(id)
                parameters!["id"] = nil
            }
        }
        return ApiManager.dataTask(request: clientURLRequest(path: pathString, params: parameters, method: "POST"))
    }
    
    class func put(path: URI, params: Dictionary<String, AnyObject>? = nil) -> Observable<ResponseObject> {
        return ApiManager.dataTask(request: clientURLRequest(path: path.rawValue, params: params, method: "PUT"))
    }
    
    class func get(path: URI, params: Dictionary<String, AnyObject>? = nil)  -> Observable<ResponseObject> {
        var parameters = params
        var pathString = path.rawValue
        
        if path == .resendOTP ||
            path == .userDetails ||
            path == .categorySubjects ||
            path == .dashboard ||
            path == .mySubjectListing ||
            path == .fetchShortlist ||
            path == .fetchReviews ||
            path == .plans ||
            path == .userCalendar ||
            path == .chatBox ||
            path == .limits{
            if let paramDict = parameters, let id = paramDict["id"] as? Int {
                pathString += String(id)
                parameters!["id"] = nil
            } else if let paramDict = parameters, let id = paramDict["id"] as? String, let subjectID = paramDict["subject_id"] {
                pathString += id
                pathString += "/\(subjectID)"
                parameters!["id"] = nil
                parameters!["subject_id"] = nil
            }
        } else if path == .changeSubjectStatus {
            if let paramDict = parameters, let id = paramDict["id"] as? String {
                pathString += id
                parameters!["id"] = nil
            }
            if let paramDict = parameters, let subjectID = paramDict["subject_id"] as? String {
                pathString += "/" + subjectID
                parameters!["subject_id"] = nil
            }
        } else if path == .deleteEvent {
            if let paramDict = parameters, let id = paramDict["id"] as? String {
                pathString += id
                parameters!["id"] = nil
            }
        }
        
        return ApiManager.dataTask(request: clientURLRequest(path: pathString, params: parameters, method: "GET"))
    }
        
    class func clientURLRequest(path: String, params: Dictionary<String, AnyObject>? = nil, method: String) -> NSMutableURLRequest {
        
        let baseURLString = "\(baseUrl)\(path)"
        let request = NSMutableURLRequest(url: NSURL(string: baseURLString)! as URL)
        request.httpMethod = method
        if let _params = params {
            switch method {
            case "POST":
                request.httpMethod = "POST"
                var queryString = ""
                
                if path.contains("addSubject") || path.contains("editSubject") || path.contains("shout_out"){
                    let jsonData = try? JSONSerialization.data(withJSONObject: params!)
                    request.httpBody = jsonData
                    request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                } else {
                    for (key, value) in _params {
                        queryString += "\(key)=\(value)&"
                    }
                    let postData = NSMutableData(data: queryString.data(using: String.Encoding.utf8)!)
                    request.httpBody = postData as Data;
                    request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
                }
                
                break
            case "GET":
                request.url = formURL(base: baseURLString, paramDict: _params)!
//                print("GET URL : \(request.url!)")
                request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
                break
            default:
                break
            }
        }
        
        //add headers
        for (key, value) in headers {
            request.addValue(value, forHTTPHeaderField: key)
        }
        if let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
            request.addValue(version, forHTTPHeaderField: "App-Version")
        }

        return request
    }
    
    fileprivate class func formURL(base: String, paramDict: Dictionary<String, AnyObject>) -> URL? {
        
        var urlComponents = URLComponents(string: base)!
        var queryItems = [URLQueryItem]()
        for (key, value) in paramDict {
            queryItems.append(URLQueryItem(name: key, value: String(describing: value)))
        }
        urlComponents.queryItems = queryItems as [URLQueryItem]?
        return urlComponents.url
    }
    
    fileprivate class func jsonToString(json: AnyObject) ->  String {
        do {
            let data1 =  try JSONSerialization.data(withJSONObject: json, options: JSONSerialization.WritingOptions.prettyPrinted) // first of all convert json to the data
            let convertedString = String(data: data1, encoding: String.Encoding.utf8) // the data will be converted to the string
            return convertedString ?? ""
            
        } catch let myJSONError {
//            print(myJSONError)
            return ""
        }
    }
    
    class func validateResponse(forResponse responseObject: ResponseObject, path: URI, successCompletionHandler:((_ success: Bool, _ jsonObject: [String: AnyObject]?, _ error: NSError?) -> Void)) {

        let response = responseObject.response
        let data = responseObject.data
        if let data = data {
            #if DEBUG
                let string1 = String(data: data, encoding: String.Encoding.utf8) ?? "Data could not be printed"
                print("REQUEST: \(String(describing: responseObject.response.url!))")
                print("RESPONSE:\(string1)")
            #endif
            do  {
                let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String : AnyObject]
                
                //TODO: Consider the status as a boolean, once all APIs return boolean value.
                guard response.statusCode == successCode else {
                    let error = ApiManager.handleError(forResponse: response, forJSONObject: json, path: path)
                    return successCompletionHandler(false, json, error)
                }
                
                if let status = json?["status"] as? String, status == "success" {
                    return successCompletionHandler(true, json, nil)
                } else if let status = json?["success"] as? String, status == "fail" {
                    return successCompletionHandler(false, json, nil)
                } else {
                    let error = ApiManager.handleError(forResponse: response, forJSONObject: json, path: path)
                    return successCompletionHandler(false, json, error)
                }
            } catch {
                return successCompletionHandler(false, nil, ResponseError.parseError as NSError)
            }
        }
        return successCompletionHandler(false, nil, ResponseError.parseError as NSError)
    }
    
    
    fileprivate class func handleError(forResponse response: HTTPURLResponse, forJSONObject json: [String: AnyObject]?, path: URI) -> NSError? {
        guard json != nil else {
            return ResponseError.parseError as NSError
        }
        switch response.statusCode {
        case 400:
            var msg = ""
            if let message = json?["message"] as? String {
                msg = message
            }
            let error = NSError(domain: "", code: -1, userInfo: [NSLocalizedDescriptionKey: msg])
            return error
        case 403:
            var urlString = ""
            if let data = json?["data"] as? [String: AnyObject], let url = data["app_url"] as? String {
                urlString = url
            }
            let userInfo = ["url": urlString];
//            NotificationCenter.default.post(name: .forceUpdate, object: nil, userInfo:userInfo)
            let error = NSError(domain: "", code: -1, userInfo: [NSLocalizedDescriptionKey: ""])
            return error
        case 503:
//            UserStore.shared.isServerError = true
//            NotificationCenter.default.post(name: .serverError, object: nil, userInfo:nil)
            let error = NSError(domain: "", code: -1, userInfo: [NSLocalizedDescriptionKey: ""])
            return error
        case InvalidAuthTokenErrorCode:
            //TODO: Special condition to ignore 403 response code for Logout API. Should be ignored by backend later.
            let error = NSError(domain: "", code: -1, userInfo: [NSLocalizedDescriptionKey: AuthenticationErrorMessage])
            return error
        default:
            let errorType =  ApiManager.getResponseErrorWithStatusCode((response.statusCode))
            return errorType as NSError
        }
    }
    
    
}

