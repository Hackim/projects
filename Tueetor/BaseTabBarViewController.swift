//
//  BaseTabBarViewController.swift
//  Tueetor
//
//  Created by Phaninder on 08/04/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit

protocol BaseTabBarViewControllerDelegate {
    func tabBarViewControllerCenterButtonTapped(tabBarController:BaseTabBarViewController, button:UIButton, buttonState:Bool);
}

class BaseTabBarViewController: UITabBarController, UITabBarControllerDelegate {

    var baseTabBarControllerDelegate:BaseTabBarViewControllerDelegate?
    var centerButton:UIButton!;
    private var centerButtonTappedOnce:Bool = false;
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        tabBar.invalidateIntrinsicContentSize()
        self.bringcenterButtonToFront()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        UIApplication.shared.statusBarStyle = .default

        self.delegate = self;
        self.tabBar.barTintColor = UIColor.white
        self.tabBar.tintColor = UIColor.init(red: 0.0509, green: 0.3725, blue: 0.4078, alpha: 1.0)

        let appearance = UITabBarItem.appearance()
        appearance.setTitleTextAttributes([.font: UIFont.init(name: "Montserrat-SemiBold", size: 10.0)!], for: [.normal])

        self.hidesBottomBarWhenPushed = true
        
        let findViewController = FindViewController.instantiateFromAppStoryboard(appStoryboard: .Find)
        findViewController.tabBarItem = UITabBarItem.init(title: "FIND NOW", image: UIImage(named: "find"), selectedImage: UIImage(named: "find_selected"))
        let nav1 = BaseNavigationController(rootViewController: findViewController)
        
        let messageViewController = MessageViewController.instantiateFromAppStoryboard(appStoryboard: .Message)
        messageViewController.tabBarItem = UITabBarItem.init(title: "MESSAGE", image: UIImage(named: "message"), selectedImage: UIImage(named: "message_selected"))
        let nav2 = BaseNavigationController(rootViewController: messageViewController)

        let dashboardViewController = DashboardViewController.instantiateFromAppStoryboard(appStoryboard: .Dashboard)
        dashboardViewController.tabBarItem = UITabBarItem(tabBarSystemItem: .more, tag: 1)
        dashboardViewController.tabBarItem.title = ""
        let nav3 = BaseNavigationController(rootViewController: dashboardViewController)

        let profileViewController = ProfileViewController.instantiateFromAppStoryboard(appStoryboard: .Profile)
        profileViewController.tabBarItem = UITabBarItem.init(title: "PROFILE", image: UIImage(named: "profile"), selectedImage: UIImage(named: "profile_selected"))
        let nav4 = BaseNavigationController(rootViewController: profileViewController)
        
        let moreViewController = MoreViewController.instantiateFromAppStoryboard(appStoryboard: .More)
        moreViewController.tabBarItem = UITabBarItem.init(title: "MORE", image: UIImage(named: "more"), selectedImage: UIImage(named: "more_selected"))
        let nav5 = BaseNavigationController(rootViewController: moreViewController)

        viewControllers = [nav1, nav2, nav3, nav4, nav5]
        for item in self.tabBar.items! {
//            if UIDevice.isIphoneX {
//                item.imageInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
//                item.titlePositionAdjustment = UIOffset(horizontal: 0, vertical: 6)
//            } else {
                item.imageInsets = UIEdgeInsets(top: -4, left: 0, bottom: 4, right: 0)
                item.titlePositionAdjustment = UIOffset(horizontal: 0, vertical: -3)
//            }
        }
        self.setupMiddleButton()
    }
    
    override func viewWillLayoutSubviews() {
        var tabFrame = self.tabBar.frame
        let height: CGFloat = UIDevice.isIphoneX ? 95 : 63

        tabFrame.size.height = height
        tabFrame.origin.y = ScreenHeight - height
        self.tabBar.frame = tabFrame
        let iphoneDevice = UIDevice.current
        print(iphoneDevice)
        tabBar.layer.shadowOffset = CGSize.init(width: 0, height: 1)
        tabBar.layer.shadowRadius = 3
        tabBar.layer.shadowColor = UIColor.black.cgColor
        tabBar.layer.shadowOpacity = 0.3
    }

    // MARK: - TabbarDelegate Methods
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        switch viewController.childViewControllers[0] {
        case is FindViewController:
            self.showCenterButton()
        case is MoreViewController:
            self.showCenterButton()
        case is MessageViewController:
            guard UserStore.shared.isLoggedIn else {
                navigateLoginPage()
                return
            }
            self.showCenterButton()
        case is ProfileViewController:
            guard UserStore.shared.isLoggedIn else {
                navigateLoginPage()
                return
            }
            self.showCenterButton()
        default:
            self.showCenterButton()
        }
    }
    
    func navigateLoginPage() {
        UserStore.shared.isSkipSelected = false

        guard let window = UIApplication.shared.keyWindow else {
            return
        }
        
        guard let rootViewController = window.rootViewController else {
            return
        }
        
        let landingViewController = LandingViewController.instantiateFromAppStoryboard(appStoryboard: .Login)
        landingViewController.view.frame = rootViewController.view.frame
        landingViewController.view.layoutIfNeeded()
        let navController = BaseNavigationController(rootViewController: landingViewController)
        UIView.transition(with: window, duration: 0.3, options: .transitionCrossDissolve, animations: {
            window.rootViewController = navController
        }, completion: nil)

    }
    // MARK: - Internal Methods
    
    @objc private func centerButtonAction(sender: UIButton) {
        guard selectedIndex != 2 else {
            return
        }
        
        self.selectedIndex = 2

        if self.selectedIndex != 2 {
            self.selectedIndex = 2
        }
        baseTabBarControllerDelegate?.tabBarViewControllerCenterButtonTapped(tabBarController: self,
                                                                                          button: centerButton,
                                                                                        buttonState: centerButtonTappedOnce)
    }
    
    func hideCenterButton() {
        centerButton.isHidden = true;
    }
    
    func showCenterButton() {
        centerButton.isHidden = false;
        self.bringcenterButtonToFront();
    }
    
    // MARK: - Private methods
    
    private func setupMiddleButton() {
        centerButton = UIButton(frame: CGRect(x: 0, y: 0, width: 81, height: 81))
        
        var centerButtonFrame = centerButton.frame
        if UIDevice.isIphoneX {
            centerButtonFrame.origin.y = ScreenHeight - centerButtonFrame.height - 22
        } else {
            centerButtonFrame.origin.y = ScreenHeight - centerButtonFrame.height + 10
        }
        centerButtonFrame.origin.x = ScreenWidth/2 - centerButtonFrame.size.width/2
        centerButton.frame = centerButtonFrame
        
        centerButton.layer.cornerRadius = centerButtonFrame.height/2
        view.addSubview(centerButton)
        centerButton.setImage(UIImage(named: "dashboard"), for: .normal)
        centerButton.setImage(UIImage(named: "dashboard"), for: .highlighted)
        centerButton.addTarget(self, action: #selector(centerButtonAction(sender:)), for: .touchUpInside)
        
        view.layoutIfNeeded()
    }
    
    private func bringcenterButtonToFront() {
        self.view.bringSubview(toFront: self.centerButton);
    }
    
}
