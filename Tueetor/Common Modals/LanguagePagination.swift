//
//  LanguagePagination.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 30/05/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import Foundation
import Unbox

class LanguagePagination: Unboxable {
    
    var languages: [Language] = []
    private let limit = 30
    var currentPageNumber = 1
    var paginationType: PaginationType = .new
    var hasMoreToLoad: Bool
    
    init() {
        languages = []
        currentPageNumber = 1
        paginationType = .new
        hasMoreToLoad = false
    }
    
    required init(unboxer: Unboxer) throws {
        if let languagesArray = unboxer.dictionary["language"] as? [[String: AnyObject]] {
            for languageDict in languagesArray {
                let language: Language = try unbox(dictionary: languageDict)
                self.languages.append(language)
            }
        }
        hasMoreToLoad = self.languages.count == limit
    }
    
    func appendDataFromObject(_ newPaginationObject: LanguagePagination) {
        switch self.paginationType {
        case .new, .reload:
            self.languages = [];
            self.languages = newPaginationObject.languages
            self.paginationType = .old
        case .old:
            self.languages += newPaginationObject.languages
            self.paginationType = .old
        }
        self.hasMoreToLoad = newPaginationObject.hasMoreToLoad
        self.currentPageNumber += 1
    }
    
}
