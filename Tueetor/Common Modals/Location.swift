//
//  Location.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 11/06/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import Foundation
import Unbox

class Location: Unboxable {
    
    var assetID: String!
    var nickName: String!
    var dispName: String!
    var address1: String!
    var address2: String!
    var latitude: String!
    var longitude: String!
    var postalCode: String!
    var phone: String!
    var completeAddress = ""
    var imagePath: String!
    
    required init(unboxer: Unboxer) throws {
        self.assetID = unboxer.unbox(key: "id_asset") ?? ""
        self.nickName = unboxer.unbox(key: "nick_name") ?? ""
        self.dispName = unboxer.unbox(key: "display_name") ?? ""
        self.address1 = unboxer.unbox(key: "address_1") ?? ""
        self.address2 = unboxer.unbox(key: "address_2") ?? ""
        self.latitude = unboxer.unbox(key: "lattitude") ?? ""
        self.longitude = unboxer.unbox(key: "longitude") ?? ""
        self.postalCode = unboxer.unbox(key: "postcode") ?? ""
        
        self.completeAddress = self.address1
        if !self.address2.isEmpty {
            completeAddress += ", " + self.address2
        }
        
        if !self.postalCode.isEmpty {
            completeAddress += ", " + self.postalCode
        }
        
        self.phone = unboxer.unbox(key: "telephone") ?? ""
        self.imagePath = unboxer.unbox(key: "image") ?? ""

    }
    
}
