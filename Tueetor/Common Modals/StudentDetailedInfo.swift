//
//  StudentDetailedInfo.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 06/07/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import Foundation
import Unbox

class StudentDetailedInfo: Unboxable {
    
    var student: Student!
    var subjects = [SubjectDetailedInfo]()
    var about: String!
    var aboutHTML: NSAttributedString
    var schedules: [String: [Schedule]]!
    var scheduleInfoArray = [Int: [String]]()
    var weekIndexes = [String: Int]()
    
    required init(unboxer: Unboxer) throws {
        self.student = unboxer.unbox(key: "user")
        self.about = unboxer.unbox(key: "about") ?? ""
        self.aboutHTML = self.about.convertHtml(fontFamilyName: "Montserrat-Light", fontSize: 16)
        var subjectsArray = [SubjectDetailedInfo]()
        if let subjectObjArray = unboxer.dictionary["subject"] as? [[String: AnyObject]],
            subjectObjArray.count > 0 {
            for subjectDict in subjectObjArray {
                do {
                    let subject: SubjectDetailedInfo = try unbox(dictionary: subjectDict)
                    subjectsArray.append(subject)
                } catch {
                    print("Couldn't parse Subject")
                }
            }
        }
        
        self.subjects = subjectsArray
        

        schedules = [String: [Schedule]]()
        var dataIndex = 0
        if let scheduleDict = unboxer.dictionary["schedule"] as? [String: AnyObject],
            let calendarDict = scheduleDict["calender"] as? [String: AnyObject] {

            for day in daysArray {
                scheduleInfoArray[dataIndex] = ["day", day]
                weekIndexes[day] = dataIndex
                dataIndex += 1
                var daySchedule = [Schedule]()
                if let scheduleArray = calendarDict[day] as? [[String: AnyObject]] {
                    for individualSchedule in scheduleArray {
                        do {
                            let schedule: Schedule = try unbox(dictionary: individualSchedule)
                            if let _ = schedule.subject {
                                daySchedule.append(schedule)
                            }
                        } catch {
                            print("Couldn't parse schedule")
                        }
                    }
                }
                daySchedule.sort { (schedule1, schedule2) -> Bool in
                    schedule1.hours < schedule2.hours
                }
                for i in 0...23 {
                    let filteredSchedules = daySchedule.filter({ (schedule) -> Bool in
                        return schedule.hours == String(i)
                    })
                    if filteredSchedules.count > 0 {
                        for j in 0..<filteredSchedules.count {
                            if j == 0 {
                                scheduleInfoArray[dataIndex] = [filteredSchedules[0].hours, "\(filteredSchedules[0].subject.name!)\n\(filteredSchedules[0].subject.level!)"]
                                
                            } else {
                                scheduleInfoArray[dataIndex] = ["\(filteredSchedules[j].subject.name!)\n\(filteredSchedules[j].subject.level!)"]
                            }
                            dataIndex += 1
                        }
                    }
                }
                
                schedules[day] = daySchedule
            }
        }
    }
    
}
