//
//  SubjectPagination.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 24/05/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import Foundation
import Unbox

class SubjectPagination: Unboxable {
    
    var subjects: [Subject] = []
    private let limit = 30
    var currentPageNumber = 1
    var paginationType: PaginationType = .new
    var hasMoreToLoad: Bool
    
    init() {
        subjects = []
        currentPageNumber = 1
        paginationType = .new
        hasMoreToLoad = false
    }
    
    required init(unboxer: Unboxer) throws {
        if let subjectsArray = unboxer.dictionary["data"] as? [[String: Any]] {
            for subjectDict in subjectsArray {
                let subject: Subject = try unbox(dictionary: subjectDict)
                self.subjects.append(subject)
            }
        }
        hasMoreToLoad = self.subjects.count == limit
    }
    
    func appendDataFromObject(_ newPaginationObject: SubjectPagination) {
        switch self.paginationType {
        case .new, .reload:
            self.subjects = [];
            self.subjects = newPaginationObject.subjects
            self.paginationType = .old
        case .old:
            self.subjects += newPaginationObject.subjects
            self.paginationType = .old
        }
        self.hasMoreToLoad = newPaginationObject.hasMoreToLoad
        self.currentPageNumber += 1
    }
    
}
