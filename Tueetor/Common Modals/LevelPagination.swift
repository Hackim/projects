//
//  LevelPagination.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 25/05/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import Foundation
import Unbox

class LevelPagination: Unboxable {
    
    var levels: [Level] = []
    private let limit = 10
    var currentPageNumber = 1
    var paginationType: PaginationType = .new
    var hasMoreToLoad: Bool
    
    init() {
        levels = []
        currentPageNumber = 1
        paginationType = .new
        hasMoreToLoad = false
    }
    
    required init(unboxer: Unboxer) throws {
        if let levelsArray = unboxer.dictionary["level"] as? [[String: Any]] {
            for levelDict in levelsArray {
                let level: Level = try unbox(dictionary: levelDict)
                self.levels.append(level)
            }
        }
        hasMoreToLoad = self.levels.count == limit
    }
    
    func appendDataFromObject(_ newPaginationObject: LevelPagination) {
        switch self.paginationType {
        case .new, .reload:
            self.levels = [];
            self.levels = newPaginationObject.levels
            self.paginationType = .old
        case .old:
            self.levels += newPaginationObject.levels
            self.paginationType = .old
        }
        self.hasMoreToLoad = newPaginationObject.hasMoreToLoad
        self.currentPageNumber += 1
    }
    
}
