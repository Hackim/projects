
//
//  Media.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 11/06/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import Foundation
import Unbox

class Media: Unboxable {
    
    var name: String!
    var desc: String!
    var size: String!
    var type: String!
    var thumbNail: String!
    
    required init(unboxer: Unboxer) throws {
        self.name = unboxer.unbox(key: "file_name") ?? ""
        self.desc = unboxer.unbox(key: "file_desc") ?? ""
        self.size = unboxer.unbox(key: "file_size") ?? ""
        self.type = unboxer.unbox(key: "file_type") ?? "jpg"
        self.thumbNail = unboxer.unbox(key: "thumb_file_name") ?? ""
    }
    
}
