//
//  SubjectDetailedInfo.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 11/06/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import Foundation
import Unbox

class SubjectDetailedInfo: Unboxable {
    
    var id: Int!
    var name: String!
    var subjectID: String!
    var level: String!
    var levelID: String!
    var qualification: String!
    var qualificationID: String!
    var pricePerSession: String!
    var pricePerMonth: String!
    var status: String!
    var teachingSince: String!
    var experience: String!
    var abilityToTravel: String!
    var latitude: String!
    var logitude: String!
    var address: String!
    var radius: String!
    
    required init(unboxer: Unboxer) throws {
        self.id = unboxer.unbox(key: "id") ?? 0
        self.name = unboxer.unbox(key: "name") ?? ""
        self.subjectID = unboxer.unbox(key: "subject_id") ?? "0"
        self.level = unboxer.unbox(key: "level") ?? ""
        self.levelID = unboxer.unbox(key: "level_id") ?? "0"
        self.qualification = unboxer.unbox(key: "qualification") ?? ""
        self.qualificationID = unboxer.unbox(key: "qualification_id") ?? ""
        self.experience = unboxer.unbox(key: "experience") ?? ""
        self.pricePerSession = unboxer.unbox(key: "pricepersession") ?? "0"
        self.pricePerMonth = unboxer.unbox(key: "pricepermonth") ?? "0"
        self.status = unboxer.unbox(key: "status") ?? "0"
        self.teachingSince = unboxer.unbox(key: "teachingsince") ?? "0"
        self.abilityToTravel = unboxer.unbox(key: "ability_to_travel") ?? ""
        self.latitude = unboxer.unbox(keyPath: "map.lattitude") ?? ""
        self.logitude = unboxer.unbox(keyPath: "map.longitude") ?? ""
        self.address = unboxer.unbox(keyPath: "map.address") ?? ""
        self.radius = unboxer.unbox(keyPath: "map.radius") ?? "0"
    }
    
    init() {
        self.id = 0
        self.name = ""
        self.subjectID = "0"
        self.level = ""
        self.levelID = "0"
        self.qualification = ""
        self.qualificationID = "0"
        self.experience = ""
        self.pricePerSession = ""
        self.pricePerMonth = ""
        self.status = "1"
        self.teachingSince = ""
        self.abilityToTravel = ""
        self.latitude = ""
        self.logitude = ""
        self.address = ""
        self.radius = ""
    }
    
}
