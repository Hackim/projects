//
//  Document.swift
//  Tueetor1
//
//  Created by Phaninder Kumar on 11/06/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import Foundation
import Unbox

class Document: Unboxable {
    
    var url: String!
    var name: String!
    var id: String!
    var type: String!
    var thumbNail: String!
    
    required init(unboxer: Unboxer) throws {
        self.url = unboxer.unbox(key: "file_name") ?? ""
        self.name = unboxer.unbox(key: "file_desc") ?? ""
        self.id = unboxer.unbox(key: "id") ?? ""
        self.type = unboxer.unbox(key: "file_type") ?? ""
        self.thumbNail = unboxer.unbox(key: "thumb_file_name") ?? ""

    }
    
}
