//
//  AgencyDetailedInfo.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 11/06/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import Foundation
import Unbox

class AgencyDetailedInfo: Unboxable {
    
    var courses = [Course]()
    var medias = [Media]()
    var locations = [Location]()
    var about: String!
    var aboutHTML: NSAttributedString
    var attributedInfo: NSAttributedString!
    var nickName: String!
    var displayName1: String!
    var displayName2: String!
    var address1: String!
    var address2: String!
    var latitude: String!
    var longitude: String!
    var postalCode: String!
    var telephone: String!
    var contactPerson: String!
    var url: String!
    var email: String!
    var note: String!
    var noteHTML: NSAttributedString
    var imagePath: String!
    
    
    required init(unboxer: Unboxer) throws {
        self.about = unboxer.unbox(key: "about") ?? ""
        self.aboutHTML = self.about.convertHtml(fontFamilyName: "Montserrat-Light", fontSize: 16)
        self.nickName = unboxer.unbox(keyPath: "agency.nick_name") ?? ""
        self.displayName1 = unboxer.unbox(keyPath: "agency.display_name1") ?? ""
        self.displayName2 = unboxer.unbox(keyPath: "agency.display_name2") ?? ""
        self.address1 = unboxer.unbox(keyPath: "agency.address_1") ?? ""
        self.address2 = unboxer.unbox(keyPath: "agency.address_2") ?? ""
        self.latitude = unboxer.unbox(keyPath: "agency.lattitude") ?? ""
        self.longitude = unboxer.unbox(keyPath: "agency.longitude") ?? ""
        self.postalCode = unboxer.unbox(keyPath: "agency.postcode") ?? ""
        self.telephone = unboxer.unbox(keyPath: "agency.telephone") ?? ""
        self.contactPerson = unboxer.unbox(keyPath: "agency.contact_person") ?? ""
        self.url = unboxer.unbox(keyPath: "agency.url") ?? ""
        self.email = unboxer.unbox(keyPath: "agency.email") ?? ""
        self.note = unboxer.unbox(keyPath: "agency.note") ?? ""
        self.noteHTML = self.note.convertHtml(fontFamilyName: "Montserrat-Light", fontSize: 16)
        self.imagePath = unboxer.unbox(keyPath: "agency.image") ?? ""

        var coursesArray = [Course]()
        if let courseObjArray = unboxer.dictionary["courses"] as? [[String: AnyObject]],
            courseObjArray.count > 0 {
            for courseDict in courseObjArray {
                do {
                    let course: Course = try unbox(dictionary: courseDict)
                    coursesArray.append(course)
                } catch {
                    print("Couldn't parse Course")
                }
            }
        }
        self.courses = coursesArray
        
        var mediaArray = [Media]()
        if let mediaObjArray = unboxer.dictionary["media"] as? [[String: AnyObject]],
            mediaObjArray.count > 0 {
            for mediaDict in mediaObjArray {
                do {
                    let media: Media = try unbox(dictionary: mediaDict)
                    mediaArray.append(media)
                } catch {
                    print("Couldn't parse Media")
                }
            }
        }
        self.medias = mediaArray
        
        var locationArray = [Location]()
        if let locationObjArray = unboxer.dictionary["location"] as? [[String: AnyObject]],
            locationObjArray.count > 0 {
            for locationDict in locationObjArray {
                do {
                    let location: Location = try unbox(dictionary: locationDict)
                    locationArray.append(location)
                } catch {
                    print("Couldn't parse Location")
                }
            }
        }
        self.locations = locationArray
        
        
        let attributedString = NSMutableAttributedString()
        attributedString.append(NSAttributedString(string: self.displayName1,
                                                   attributes: [.font: UIFont(name: "Montserrat-SemiBold", size: 25.0)!,
                                                                .foregroundColor : themeBlackColor]))
        var infoString = self.displayName2!
        infoString += "\n" + self.address1
        if !self.address2.isEmpty {
            infoString += ", " + self.address2
        }
        if !self.postalCode.isEmpty {
            infoString += ", " + self.postalCode
        }
        infoString += "\n" + self.telephone
        infoString += "\n" + self.url
        infoString += "\nContact: " + self.contactPerson
        
        attributedString.append(NSAttributedString(string: "\n\(infoString)",
            attributes: [.font: UIFont(name: "Montserrat-Light", size: 13.0)!,
                         .foregroundColor : themeBlackColor]))
        attributedInfo = attributedString
        
    }
    
}
