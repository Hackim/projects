//
//  Student.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 06/07/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import Foundation
import Unbox

class Student: Unboxable {
    
    var ID: Int!
    var title: String!
    var name: String!
    var firstName: String!
    var lastName: String!
    var gender: String!
    var image: String!
    var about: String!
    var aboutHTML: NSAttributedString
    var status: String!
    var imageURL: String!
    var isFavorite = false
    var subjects = [TutorSubject]()
    var age: String!
    
    required init(unboxer: Unboxer) throws {
        self.ID = unboxer.unbox(key: "id") ?? 0
        self.title = unboxer.unbox(key: "title") ?? ""
        self.name = unboxer.unbox(key: "name") ?? ""
        self.firstName = unboxer.unbox(key: "first_name") ?? ""
        self.lastName = unboxer.unbox(key: "last_name") ?? ""
        self.image = unboxer.unbox(key: "image") ?? ""
        self.status = unboxer.unbox(key: "status") ?? ""
        self.isFavorite = unboxer.unbox(key: "is_favorite") ?? false
        self.imageURL = self.image
        self.about = unboxer.unbox(key: "about") ?? ""
        self.aboutHTML = self.about.convertHtml(fontFamilyName: "Montserrat-Light", fontSize: 16)
        self.gender = unboxer.unbox(key: "gender") ?? "M"
        self.gender = self.gender == "M" ? "Male" : "Female"
        self.age = unboxer.unbox(key: "age") ?? "0"
        var subjectsArray = [TutorSubject]()
        if let subjectsObjectsArray = unboxer.dictionary["subject_prices"] as? [[String: AnyObject]],
            subjectsObjectsArray.count > 0 {
            for subjectDict in subjectsObjectsArray {
                do {
                    let subject: TutorSubject = try unbox(dictionary: subjectDict)
                    subjectsArray.append(subject)
                } catch {
                    print("Couldn't parse Subject")
                }
            }
        }
        subjects = subjectsArray
    }
    
}
