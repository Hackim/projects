//
//  Language.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 30/05/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import Foundation
import Unbox

class Language: Unboxable {
    
    var id: String!
    var name: String!
    var transalationName: String!
    var code: String!
    var country: String!
    var status: String!
    
    required init(unboxer: Unboxer) throws {
        self.id = unboxer.unbox(key: "id") ?? "0"
        self.name = unboxer.unbox(key: "name") ?? ""
        self.transalationName = unboxer.unbox(key: "translation_name") ?? ""
        self.code = unboxer.unbox(key: "code") ?? ""
        self.country = unboxer.unbox(key: "country_name") ?? ""
        self.status = unboxer.unbox(key: "status") ?? ""
    }
    
    init(id: String, name: String, transalationName: String, code: String, country: String, status: String) {
        self.id = id
        self.name = name
        self.transalationName = transalationName
        self.code = code
        self.country = country
        self.status = status
    }
    
}
