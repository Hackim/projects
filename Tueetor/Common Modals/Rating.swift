//
//  Rating.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 11/06/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import Foundation
import Unbox

class Rating: Unboxable {
    
    var rate: String!
    var subject: String!
    var comment: String!
    var studentName: String!
    var studentProfileImage: String!
    var studentEmail: String!
    
    required init(unboxer: Unboxer) throws {
        self.rate = unboxer.unbox(key: "rate") ?? "0"
        self.subject = unboxer.unbox(key: "subject") ?? ""
        self.comment = unboxer.unbox(key: "comment") ?? ""
        self.studentName = unboxer.unbox(key: "student_name") ?? "Tueetor®"
        self.studentProfileImage = unboxer.unbox(key: "student_image") ?? ""
        self.studentEmail = unboxer.unbox(key: "student_email") ?? ""
    }
    
}
