//
//  Course.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 11/06/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import Foundation
import Unbox

class Course: Unboxable {
    
    var id: String!
    var name: String!
    var nameAttributedString: NSAttributedString!
    var teachingExp: String!
    var url: String!
    var level: String!
    var subject: String!
    var pricePerSession: String!
    var pricePerMonth: String!
    var pricePerTerm: String!
    var schedule: Int!
    var doc: Int!
    var documents = [Document]()
    var schedules = ["SUN": [String](),
                     "MON": [String](),
                     "TUE": [String](),
                     "WED": [String](),
                     "THU": [String](),
                     "FRI": [String](),
                     "SAT": [String]()]
    
    required init(unboxer: Unboxer) throws {
        self.id = unboxer.unbox(key: "id") ?? ""
        self.name = unboxer.unbox(key: "course_name") ?? ""
        self.nameAttributedString = self.name.convertHtml(fontFamilyName: "Montserrat-Regular", fontSize: 17)
        self.subject = unboxer.unbox(key: "subject") ?? ""
        self.teachingExp = unboxer.unbox(key: "teaching_Exp") ?? ""
        self.level = unboxer.unbox(key: "level") ?? "Not Applicable"
        self.url = unboxer.unbox(key: "url") ?? ""
        self.pricePerSession = unboxer.unbox(key: "pricepersession") ?? ""
        self.pricePerMonth = unboxer.unbox(key: "pricepermonth") ?? ""
        self.pricePerTerm = unboxer.unbox(key: "priceperterm") ?? ""
        self.schedule = unboxer.unbox(key: "schedule") ?? 0
        self.doc = unboxer.unbox(key: "doc") ?? 0
        if let docsArray = unboxer.dictionary["doc_data"] as? [[String: AnyObject]] {
            for documentDict in docsArray {
                do {
                    let document: Document = try unbox(dictionary: documentDict)
                    documents.append(document)
                } catch {
                    print("Couldn't parse Document")
                }

            }
        }
        
        if let schedulesDict = unboxer.dictionary["schedule_data"] as? [String: AnyObject] {
            for day in daysArray {
                if let daysSchedule = schedulesDict[day] as? [[String: String]] {
                    for daySchedule in daysSchedule {
                        let startTime = daySchedule["start_time"]
                        let endTime = daySchedule["end_time"]
                        var currentArray = schedules[day]
                        let timeRange = Utilities.convertTime(timeString: startTime!, outputFormat: "h:mm a", inputFormat: "HH:mm:ss") + " - " + Utilities.convertTime(timeString: endTime!, outputFormat: "h:mm a", inputFormat: "HH:mm:ss")
                        currentArray?.append(timeRange)
                        schedules[day] = currentArray
                    }
                }
            }
        }
    }
    
}
