//
//  Experience.swift
//  Tueetor
//
//  Created by Phaninder on 17/05/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import Foundation
import Unbox

class Experience: Unboxable {
    
    var name: String!
    var isSelected = false
    
    required init(unboxer: Unboxer) throws {
        self.name = unboxer.unbox(key: "teaching_Exp") ?? ""
    }
    
    init(name: String) {
        self.name = name
        self.isSelected = false
    }
    
}
