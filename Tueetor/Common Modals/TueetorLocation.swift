//
//  TueetorLocation.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 21/06/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import Foundation
import MapKit
import Contacts

class TueetorLocation: NSObject, MKAnnotation {
    
    let title: String?
    let locationName: String
    let coordinate: CLLocationCoordinate2D
    
    var subtitle: String? {
        return locationName
    }
    
    init(name: String?, lat: String?, long: String?) {
        self.title = name ?? "No Title".localized
        self.locationName = name ?? ""
        
        if let latitude = Double(lat ?? "0"),
            let longitude = Double(long ?? "0") {
            self.coordinate = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        } else {
            self.coordinate = CLLocationCoordinate2D()
        }
    }
    
    func mapItem() -> MKMapItem {
        let addressDict = [CNPostalAddressStreetKey: subtitle!]
        let placemark = MKPlacemark(coordinate: coordinate, addressDictionary: addressDict)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = title
        return mapItem
    }
    
}
