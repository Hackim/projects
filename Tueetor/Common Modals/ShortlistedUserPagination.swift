//
//  ShortlistedUserPagination.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 23/06/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import Foundation
import Unbox

class ShortlistedUserPagination: Unboxable {
    
    var shortlistedUsers: [ShortlistedUser] = []
    private let limit = 10
    var currentPageNumber = 1
    var paginationType: PaginationType = .new
    var hasMoreToLoad: Bool
    
    init() {
        shortlistedUsers = []
        currentPageNumber = 1
        paginationType = .new
        hasMoreToLoad = false
    }
    
    required init(unboxer: Unboxer) throws {
        if let shortlistedUsersArray = unboxer.dictionary["shortlist_user"] as? [[String: Any]] {
            for shortlistedUserDict in shortlistedUsersArray {
                let shortlistedUser: ShortlistedUser = try unbox(dictionary: shortlistedUserDict)
                self.shortlistedUsers.append(shortlistedUser)
            }
        }
        hasMoreToLoad = self.shortlistedUsers.count == limit
    }
    
    func appendDataFromObject(_ newPaginationObject: ShortlistedUserPagination) {
        switch self.paginationType {
        case .new, .reload:
            self.shortlistedUsers = [];
            self.shortlistedUsers = newPaginationObject.shortlistedUsers
            self.paginationType = .old
        case .old:
            self.shortlistedUsers += newPaginationObject.shortlistedUsers
            self.paginationType = .old
        }
        self.hasMoreToLoad = newPaginationObject.hasMoreToLoad
        self.currentPageNumber += 1
    }
    
}
