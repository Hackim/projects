//
//  Level.swift
//  Tueetor
//
//  Created by Phaninder on 17/05/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import Foundation
import Unbox

class Level: Unboxable {
    
    var id: Int!
    var name: String!
    var isSelected = false
    
    required init(unboxer: Unboxer) throws {
        self.id = unboxer.unbox(key: "id") ?? 0
        self.name = unboxer.unbox(key: "name") ?? ""
    }
    
    init(id: Int, name: String) {
        self.id = id
        self.name = name
        self.isSelected = false
    }
}
