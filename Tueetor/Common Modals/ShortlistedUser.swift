//
//  ShortlistedUser.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 23/06/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import Foundation
import Unbox

class ShortlistedUser: Unboxable {
    
    var id: String!
    var userID: String!
    var reason: String!
    var name: String!
    var image: String!
    var userType: String!
    var createdDate: String!
    var subjects = [Subject]()
    
    required init(unboxer: Unboxer) throws {
        self.id = unboxer.unbox(key: "id") ?? "0"
        self.userID = unboxer.unbox(key: "favorite_id") ?? "0"
        self.reason = unboxer.unbox(key: "reason_favorite") ?? ""
        self.name = unboxer.unbox(key: "name") ?? ""
        self.image = unboxer.unbox(key: "image") ?? ""
        self.userType = unboxer.unbox(key: "user_type") ?? ""
        self.createdDate = unboxer.unbox(key: "favoritedon") ?? ""
        if let subjectsArray = unboxer.dictionary["subjects"] as? [[String: Any]] {
            for subjectDict in subjectsArray {
                let subject: Subject = try unbox(dictionary: subjectDict)
                self.subjects.append(subject)
            }
        }
    }
    
}
