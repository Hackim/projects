//
//  SampleMessage.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 13/06/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import Foundation

class SampleMessage {
    
    let imageURL: String
    let name: String
    let time: String
    let date: String
    let place: String
    let indicatorColorBlue: Bool
    let message: String
    
    init(message: String, imageURL: String, name: String, time: String, date: String, place: String, indicatorColorBlue: Bool) {
        self.message = message
        self.imageURL = imageURL
        self.name = name
        self.time = time
        self.date = date
        self.place = place
        self.indicatorColorBlue = indicatorColorBlue
    }
    
    
}
