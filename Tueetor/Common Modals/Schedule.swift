//
//  Schedule.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 11/06/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import Foundation
import Unbox

class Schedule: Unboxable {
    
    var subject: SubjectDetailedInfo!
    var hours: String!
    var comments: String!
    var isAvailable: String!
    
    required init(unboxer: Unboxer) throws {
        self.subject = unboxer.unbox(key: "profile")
        if self.subject == nil {
            
        }
        self.hours = unboxer.unbox(key: "hours") ?? "0"
        self.comments = unboxer.unbox(key: "comments") ?? ""
        self.isAvailable = unboxer.unbox(key: "isavailable") ?? "N"
    }

}

