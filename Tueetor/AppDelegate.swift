//
//  AppDelegate.swift
//  Tueetor
//
//  Created by Phaninder on 15/03/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import Fabric
import Crashlytics
import FBSDKCoreKit
import FBSDKLoginKit
import UserNotifications
import GooglePlaces
import GooglePlacePicker
import GoogleMaps
import Stripe
import EventKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate {

    var window: UIWindow?
    var user: User?
    var ignoreRegisteringNotification = false
    var countries: [Country]!
    var eventStore : EKEventStore!

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        //Initialize rechability and crashlytics
        
        Utilities.shared.setupReachability()
        IQKeyboardManager.sharedManager().enable = true
        IQKeyboardManager.sharedManager().disabledToolbarClasses = [ReportUserViewController.self, ChatViewController.self]
        Fabric.with([STPAPIClient.self, Crashlytics.self])
     
        GMSPlacesClient.provideAPIKey(googlePlacesAPIKey)
        GMSServices.provideAPIKey(googlePlacesAPIKey)
        STPPaymentConfiguration.shared().publishableKey = "pk_test_225FhHoDeJVwsPnd9IpS97hC"
        eventStore = EKEventStore()

        checkForNotificationsPermissions()
        if UserStore.shared.notificationPermissionsAsked == true {
            registerForPushNotifications()
        }
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().delegate = self
        }
        
        var preferences = EasyTipView.Preferences()
        preferences.drawing.font = UIFont(name: "Montserrat-Light", size: 13.0)!
        preferences.drawing.foregroundColor = UIColor.white
        preferences.drawing.backgroundColor = themeBlueColor
        preferences.drawing.arrowPosition = EasyTipView.ArrowPosition.top
        
        /*
         * Optionally you can make these preferences global for all future EasyTipViews
         */
        EasyTipView.globalPreferences = preferences
        
        if let countryCode = (Locale.current as NSLocale).object(forKey: .countryCode) as? String {
            print(countryCode)
        }
        
        let splashAnimationViewController = SplashAnimationViewController.instantiateFromAppStoryboard(appStoryboard: .Main)
        let navController = BaseNavigationController(rootViewController: splashAnimationViewController)
        self.window = UIWindow(frame: UIScreen.main.bounds)
        self.window?.rootViewController = navController

        self.window?.makeKeyAndVisible()
        return true
    }

    func showAppropriateViewController() {
        if UserStore.shared.isSkipSelected || UserStore.shared.isLoggedIn {

            let tabBarController = BaseTabBarViewController()
            
            tabBarController.selectedIndex = 2
            self.window = UIWindow(frame: UIScreen.main.bounds)
            self.window?.rootViewController = tabBarController
        } else {
            let splashAnimationViewController = SplashAnimationViewController.instantiateFromAppStoryboard(appStoryboard: .Main)
            let navController = BaseNavigationController(rootViewController: splashAnimationViewController)
            self.window = UIWindow(frame: UIScreen.main.bounds)
            self.window?.rootViewController = navController
        }
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        UIApplication.shared.applicationIconBadgeNumber = 0
        if UserStore.shared.notificationPermissionsAsked == true, ignoreRegisteringNotification == false {
            ignoreRegisteringNotification = true
            registerForPushNotifications()
        } else {
            ignoreRegisteringNotification = false
        }
        
    }
    
    public func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        
        let facebookDidHandle = FBSDKApplicationDelegate.sharedInstance().application(
            app,
            open: url,
            sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as! String,
            annotation: options[UIApplicationOpenURLOptionsKey.annotation])
        
        return facebookDidHandle
    }
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {

        let facebookDidHandle = FBSDKApplicationDelegate.sharedInstance().application(
            application,
            open: url,
            sourceApplication: sourceApplication,
            annotation: annotation)
        
        return facebookDidHandle
    }


    func registerForPushNotifications() {
        ignoreRegisteringNotification = true
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) { (granted, error) in
                guard granted else {
                    UserStore.shared.isNotificationUnAuthorized = true
                    return
                }
                self.ignoreRegisteringNotification = false
                self.getNotificationSettings()
            }
        } else {
            let setting = UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            UIApplication.shared.registerUserNotificationSettings(setting)
            UIApplication.shared.registerForRemoteNotifications()
            getNotificationSettings()
        }
    }
    
    func getNotificationSettings() {
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().getNotificationSettings { (settings) in

                guard settings.authorizationStatus == .authorized else {
                    UserStore.shared.isNotificationUnAuthorized = true
                    return
                }
                DispatchQueue.main.async {
                    UIApplication.shared.registerForRemoteNotifications()
                }
            }
        } else {
            if UIApplication.shared.isRegisteredForRemoteNotifications == false {
                UIApplication.shared.registerForRemoteNotifications()
            }
        }
    }
    
    
    func application(_ application: UIApplication,
                     didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        let tokenParts = deviceToken.map { data -> String in
            return String(format: "%02.2hhx", data)
        }
        
        let token = tokenParts.joined()
        UserStore.shared.deviceToken = token
        UserStore.shared.isNotificationUnAuthorized = false
        Utilities.sendDeviceToken(token)
        NotificationCenter.default.post(name: .deviceTokenReceived, object: nil, userInfo:nil)
        print("Device Token: \(token)")
    }
    
    func application(_ application: UIApplication,
                     didFailToRegisterForRemoteNotificationsWithError error: Error) {
        UserStore.shared.deviceToken = ""
        print("Failed to register: \(error)")
    }
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {

        completionHandler([.alert, .badge, .sound])
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        
        switch application.applicationState {
        case .active:
            //app is currently active, can update badges count here
            print("active")
        case .inactive:
            //app is transitioning from background to foreground (user taps notification), do what you need when user taps here
            print("inactive")
        case .background:
            //app is in background, if content-available key of your notification is set to 1, poll to your backend to retrieve data and update your interface here
            print("background")
        }
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {

    }

    
    func checkForNotificationsPermissions() {
        if #available(iOS 10.0, *) {
            let current = UNUserNotificationCenter.current()
            current.getNotificationSettings(completionHandler: { settings in
                
                switch settings.authorizationStatus {
                    
                case .notDetermined:
                    UserStore.shared.isNotificationUnAuthorized = true
                // Authorization request has not been made yet
                case .denied:
                    UserStore.shared.isNotificationUnAuthorized = true
                    // User has denied authorization.
                // You could tell them to change this in Settings
                case .authorized:
                    UserStore.shared.isNotificationUnAuthorized = false
                    // User has given authorization.
                }
            })
        } else {
            // Fallback on earlier versions
            if UIApplication.shared.isRegisteredForRemoteNotifications {
                UserStore.shared.isNotificationUnAuthorized = false
            } else {
                UserStore.shared.isNotificationUnAuthorized = true
            }
        }
    }

}

