//
//  ReportUserViewController.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 15/07/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit

class ReportUserViewController: BaseViewController {

    @IBOutlet weak var commentsTextField: TueetorTextField!
    @IBOutlet weak var commentsTextView: UITextView!
    @IBOutlet weak var spamButtonView: UIButton!
    @IBOutlet weak var replyButtonView: UIButton!
    @IBOutlet weak var conductButtonView: UIButton!
    
    var selectedOption = 1
    var mapItem: MapItem!

    override func viewDidLoad() {
        super.viewDidLoad()
        commentsTextField.placeholder = "GIVE YOUR COMMENTS HERE".localized
        commentsTextField.title = "GIVE YOUR COMMENTS HERE".localized
        commentsTextField.textFont = textFieldLightFont
        commentsTextField.titleFont = textFieldDefaultFont
        commentsTextField.placeholderFont = textFieldDefaultFont

        configureButtons()
    }

    func configureButtons() {
        spamButtonView.setImage(UIImage(named: selectedOption == 1 ? "radioChecked" : "checkbox_empty"), for: .normal)
        replyButtonView.setImage(UIImage(named: selectedOption == 2 ? "radioChecked" : "checkbox_empty"), for: .normal)
        conductButtonView.setImage(UIImage(named: selectedOption == 3 ? "radioChecked" : "checkbox_empty"), for: .normal)
    }
    
    @IBAction func spamButtonTapped(_ sender: UIButton) {
        if selectedOption != 1 {
            selectedOption = 1
            configureButtons()
        }
    }
    
    @IBAction func noReplyButtonTapped(_ sender: UIButton) {
        if selectedOption != 2 {
            selectedOption = 2
            configureButtons()
        }

    }
    
    @IBAction func improperButtonTapped(_ sender: UIButton) {
        if selectedOption != 3 {
            selectedOption = 3
            configureButtons()
        }
    }

}

extension ReportUserViewController: UITextViewDelegate {
    
    func textViewDidChange(_ textView: UITextView) {
        commentsTextField.text = textView.text
    }
}
