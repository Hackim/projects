//
//  EnquireAgencyViewController.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 15/07/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit
import RxSwift

class EnquireAgencyViewController: BaseViewController {

    @IBOutlet weak var nameTextField: TueetorTextField!
    @IBOutlet weak var emailTextField: TueetorTextField!
    @IBOutlet weak var phoneTextField: TueetorTextField!
    
    @IBOutlet weak var emailChecklistButton: UIButton!
    @IBOutlet weak var phoneChecklistButton: UIButton!
    
    @IBOutlet weak var commentTextView: UITextView!
    @IBOutlet weak var commentTextField: TueetorTextField!
    @IBOutlet weak var titleLabel: UILabel!
    
    var selectedOption = 1
    var mapItem: MapItem!
    var course: Course!
    var disposableBag = DisposeBag()
    var agencyID: String!
    var assetID: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureTextFields()
        if let _ = course {
            titleLabel.text = "Course Enquiry".localized
        } else {
            titleLabel.text = "General Enquiry".localized
        }
        if agencyID == nil {
            agencyID = mapItem.ID
            assetID = mapItem.assetID
        }
        updateEnquireCount()
        configureButtons()
    }
    
    func configureTextFields() {
        nameTextField.placeholder = "FULL NAME".localized
        nameTextField.title = "FULL NAME".localized
        nameTextField.textFont = textFieldLightFont
        nameTextField.titleFont = textFieldDefaultFont
        nameTextField.placeholderFont = textFieldDefaultFont

        emailTextField.placeholder = "EMAIL ADDRESS".localized
        emailTextField.title = "EMAIL ADDRESS".localized
        emailTextField.textFont = textFieldLightFont
        emailTextField.titleFont = textFieldDefaultFont
        emailTextField.placeholderFont = textFieldDefaultFont

        phoneTextField.placeholder = "CONTACT NUMBER".localized
        phoneTextField.title = "CONTACT NUMBER".localized
        phoneTextField.textFont = textFieldLightFont
        phoneTextField.titleFont = textFieldDefaultFont
        phoneTextField.placeholderFont = textFieldDefaultFont

        if let user = (UIApplication.shared.delegate as! AppDelegate).user {
            nameTextField.text = user.firstName + " " + user.lastName
            emailTextField.text = user.email ?? ""
            phoneTextField.text = user.phone ?? ""
        }
        
        
        commentTextField.placeholder = "GIVE YOUR COMMENTS HERE".localized
        commentTextField.title = "GIVE YOUR COMMENTS HERE".localized
        commentTextField.textFont = textFieldLightFont
        commentTextField.titleFont = textFieldDefaultFont
        commentTextField.placeholderFont = textFieldDefaultFont

    }
    
    func configureButtons() {
        emailChecklistButton.setImage(UIImage(named: selectedOption == 1 ? "radioChecked" : "checkbox_empty"), for: .normal)
        phoneChecklistButton.setImage(UIImage(named: selectedOption == 2 ? "radioChecked" : "checkbox_empty"), for: .normal)
    }

    
    @IBAction func emailButtonTapped(_ sender: UIButton) {
        if selectedOption != 1 {
            selectedOption = 1
            configureButtons()
        }
    }
    
    @IBAction func phoneButtonTapped(_ sender: UIButton) {
        if selectedOption != 2 {
            selectedOption = 2
            configureButtons()
        }

    }
}

extension EnquireAgencyViewController: UITextViewDelegate {
    
    func textViewDidChange(_ textView: UITextView) {
        commentTextField.text = textView.text
    }
}

extension EnquireAgencyViewController {
    
    func updateEnquireCount() {
        guard Utilities.shared.isNetworkReachable() else {
            return
        }
        
        guard let userID = UserStore.shared.userID else {
            return
        }
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        var courseID = ""
        if let agencyCourse = course {
            courseID = agencyCourse.id
        }
        
        let enquireObserver = ApiManager.shared.apiService.updateEnquiryCount(agencyID: agencyID, assetID: assetID, userID: String(userID), courseID: courseID.isEmpty ? nil : courseID)
        let enquireDisposable = enquireObserver.subscribe(onNext: {(message) in
            DispatchQueue.main.async {
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
            }
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
            })
        })
        enquireDisposable.disposed(by: disposableBag)
    }

}

