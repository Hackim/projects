//
//  EventsPopupViewController.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 07/08/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit
import EventKit

protocol EventsPopupViewControllerDelegate {
    func cancelButtonTapped()
    func okButtonTapped(desc: String, recurring: Int, alarm: [Int])
    func editButtonTapped(event: EKEvent?, desc: String, recurring: Int, alarm: [Int], userEvent: UserEvent)
    func deleteButtonTapped(calendarEvent: EKEvent?, userEvent: UserEvent)
}

class EventsPopupViewController: BaseViewController {

    @IBOutlet weak var descriptionTextField: TueetorTextField!
    @IBOutlet weak var weeklyCheckBox: UIButton!
    @IBOutlet weak var monthlyCheckBox: UIButton!
    @IBOutlet weak var yearlyCheckBox: UIButton!
    @IBOutlet weak var fifteenCheckBox: UIButton!
    @IBOutlet weak var thirtyCheckBox: UIButton!
    @IBOutlet weak var oneHourCheckBox: UIButton!
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var leftButton: UIButton!
    @IBOutlet weak var rightButton: UIButton!
    
    var selectedRecurringOption = -1
    var selectedAlarmOptions = [Int]()
    var eventDescription: String = ""
    var userEvent: UserEvent!
    var delegate: EventsPopupViewControllerDelegate?
    var canAccessCalendar = false
    var eventStore : EKEventStore!
    var reminders: [EKReminder]!
    var currentEvent: EKEvent?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        descriptionTextField.placeholder = "DESCRIPTION".localized
        descriptionTextField.title = "DESCRIPTION".localized
        descriptionTextField.text = eventDescription
        descriptionTextField.textFont = textFieldLightFont
        descriptionTextField.titleFont = textFieldDefaultFont
        descriptionTextField.placeholderFont = textFieldDefaultFont
        errorLabel.text = ""
        if let _ = userEvent {
            leftButton.setTitle("Delete".localized, for: .normal)
            rightButton.setTitle("Edit".localized, for: .normal)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let event = userEvent, canAccessCalendar {
            if let calendarEvent = self.fetchEvent(event) {
                self.currentEvent = calendarEvent
                //Recurrence Rules
                if let recurrenceRules = calendarEvent.recurrenceRules, recurrenceRules.count > 0 {
                    let recurrentRule = recurrenceRules[0]
                    switch recurrentRule.frequency {
                    case .weekly:
                        selectedRecurringOption = 0
                    case .monthly:
                        selectedRecurringOption = 1
                    case .yearly:
                        selectedRecurringOption = 2
                    default:
                        selectedRecurringOption = -1
                    }
                }
                //Alarms
                if let alarms = calendarEvent.alarms, alarms.count > 0 {
                    for alarm in alarms {
                        let alarmDate = alarm.absoluteDate
                        let offset = alarmDate?.timeIntervalSince(calendarEvent.startDate)
                        switch offset {
                        case -15*60:
                            selectedAlarmOptions.append(0)
                        case -30*60:
                            selectedAlarmOptions.append(1)
                        case -60*60:
                            selectedAlarmOptions.append(2)
                        default:
                            break
                        }
                        
                    }
                }
                setupViews()
            }
        }
    }
    func setupViews() {
        setupViewForRecurringOptions()
        setupViewForAlarmOptions()
    }
    
    func fetchEvent(_ userEvent: UserEvent) -> EKEvent? {
        for eventId in userEvent.eventIds {
            if let fetchedEvent = self.eventStore.event(withIdentifier: eventId) {
                return fetchedEvent
            }
        }
        return nil
    }
    
    func setupViewForRecurringOptions() {
        weeklyCheckBox.setImage(UIImage(named: selectedRecurringOption == 0 ? "radioChecked" : "checkbox_empty"), for: .normal)
        monthlyCheckBox.setImage(UIImage(named: selectedRecurringOption == 1 ? "radioChecked" : "checkbox_empty"), for: .normal)
        yearlyCheckBox.setImage(UIImage(named: selectedRecurringOption == 2 ? "radioChecked" : "checkbox_empty"), for: .normal)
    }
    
    func setupViewForAlarmOptions() {
        fifteenCheckBox.setImage(UIImage(named: selectedAlarmOptions.contains(0) ? "checkbox" : "checkbox_empty"), for: .normal)
        thirtyCheckBox.setImage(UIImage(named: selectedAlarmOptions.contains(1) ? "checkbox" : "checkbox_empty"), for: .normal)
        oneHourCheckBox.setImage(UIImage(named: selectedAlarmOptions.contains(2) ? "checkbox" : "checkbox_empty"), for: .normal)
    }

    @IBAction func recurringOptionButtonTapped(_ sender: UIButton) {
        selectedRecurringOption = selectedRecurringOption == sender.tag ? -1 : sender.tag
        setupViewForRecurringOptions()
    }
    
    @IBAction func alarmOptionButtonTapped(_ sender: UIButton) {
        let index = selectedAlarmOptions.index { (i) -> Bool in
            return sender.tag == i
        }
        if let optionIndex = index, optionIndex != -1 {
            selectedAlarmOptions.remove(at: optionIndex)
        } else {
            selectedAlarmOptions.append(sender.tag)
        }
        setupViewForAlarmOptions()
    }
    
    @IBAction func okButtonTapped(_ sender: UIButton) {
        self.view.endEditing(true)
        eventDescription = descriptionTextField.text ?? ""
        guard !eventDescription.isEmpty else {
            errorLabel.text = "Please enter event description".localized
            return
        }
        errorLabel.text = ""
        if let _ = userEvent {
            delegate?.editButtonTapped(event: currentEvent, desc: eventDescription, recurring: selectedRecurringOption, alarm: selectedAlarmOptions, userEvent: userEvent)
        } else {
            delegate?.okButtonTapped(desc: eventDescription, recurring: selectedRecurringOption, alarm: selectedAlarmOptions)
        }
    }
    
    @IBAction func cancelButtonTapped(_ sender: UIButton) {
        if let _ = userEvent {
            delegate?.deleteButtonTapped(calendarEvent: currentEvent, userEvent: userEvent)
        } else {
            delegate?.cancelButtonTapped()
        }
    }
}
