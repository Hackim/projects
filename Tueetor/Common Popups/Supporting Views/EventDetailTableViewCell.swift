//
//  EventDetailTableViewCell.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 09/08/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit

class EventDetailTableViewCell: UITableViewCell {
    
    @IBOutlet weak var eventLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    class func cellIdentifier() -> String {
        return "EventDetailTableViewCell"
    }
    
    class func cellHeight() -> CGFloat {
        return 44.0
    }
}
