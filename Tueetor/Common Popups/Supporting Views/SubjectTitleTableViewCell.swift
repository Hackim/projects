//
//  SubjectTitleTableViewCell.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 07/08/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit

class SubjectTitleTableViewCell: UITableViewCell {

    @IBOutlet weak var subjectLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    class func cellIdentifier() -> String {
        return "SubjectTitleTableViewCell"
    }
    
    class func cellHeight() -> CGFloat {
        return 44.0
    }
}
