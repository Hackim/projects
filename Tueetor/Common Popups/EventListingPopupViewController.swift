//
//  EventListingPopupViewController.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 09/08/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit

class EventListingPopupViewController: BaseViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableViewHeightConstraint: NSLayoutConstraint!
    
    var userEvents: [UserEvent]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let nib = UINib(nibName: "EventDetailTableViewCell", bundle: nil)
        if userEvents.count < 8 {
            tableView.isScrollEnabled = false
            tableViewHeightConstraint.constant = CGFloat(userEvents.count) * EventDetailTableViewCell.cellHeight() + 20.0
        }
        tableView.register(nib, forCellReuseIdentifier: EventDetailTableViewCell.cellIdentifier())
        tableView.tableFooterView = UIView()
        tableView.delegate = self
        tableView.dataSource = self
    }
    
}

extension EventListingPopupViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return userEvents.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: EventDetailTableViewCell.cellIdentifier(), for: indexPath) as! EventDetailTableViewCell
        cell.eventLabel.text = userEvents[indexPath.row].comments
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return EventDetailTableViewCell.cellHeight()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}
