//
//  SubjectsPopupViewController.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 07/08/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit

protocol SubjectsPopupViewControllerDelegate {
    func showSubjectDetailsForSubject(_ subject: SubjectDetailedInfo)
}
class SubjectsPopupViewController: BaseViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var titleLabel: UILabel!
    
    var subjects: [SubjectDetailedInfo]!
    var delegate: SubjectsPopupViewControllerDelegate?
    var subjectNames: [String]!
    var titleString: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let nib = UINib(nibName: "SubjectTitleTableViewCell", bundle: nil)
        if subjectNames == nil {
            subjectNames = [String]()
            for subject in subjects {
                subjectNames.append(subject.name)
            }
        }
        if let _titleString = titleString {
            titleLabel.text = _titleString
        }
        if subjectNames.count < 8 {
            tableViewHeightConstraint.constant = CGFloat(subjectNames.count) * SubjectTitleTableViewCell.cellHeight() + 20.0
        }
        tableView.register(nib, forCellReuseIdentifier: SubjectTitleTableViewCell.cellIdentifier())
        tableView.tableFooterView = UIView()
        tableView.delegate = self
        tableView.dataSource = self
    }

}

extension SubjectsPopupViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return subjectNames.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: SubjectTitleTableViewCell.cellIdentifier(), for: indexPath) as! SubjectTitleTableViewCell
        cell.subjectLabel.text = subjectNames[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return SubjectTitleTableViewCell.cellHeight()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if subjects != nil {
            delegate?.showSubjectDetailsForSubject(subjects[indexPath.row])
        }
    }
}


