//
//  TueetorTextFieldTableViewCell.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 10/04/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit

protocol TueetorTextFieldTableViewCellDelegate {
    func valueChangedForTextField(_ textField: TueetorTextField)
}

class TueetorTextFieldTableViewCell: UITableViewCell {

    @IBOutlet weak var customTextField: TueetorTextField!
    @IBOutlet weak var errorLabel: UILabel!
 
    var delegate: TueetorTextFieldTableViewCellDelegate?
    
    class func cellIdentifier() -> String {
        return "TueetorTextFieldTableViewCell"
    }
 
    func configureCellForRowAtIndex(_ index: Int, withText text: String) {
        customTextField.tag = index
        customTextField.placeholder = text
        customTextField.title = text
        customTextField.textFont = textFieldLightFont
        customTextField.placeholderFont = textFieldDefaultFont
        customTextField.titleFont = textFieldDefaultFont
    }
    
    @IBAction func valueChangedInTextField(_ sender: TueetorTextField) {
        delegate?.valueChangedForTextField(sender)
    }
}

// MARK: - Validations

extension TueetorTextFieldTableViewCell {
    
    // MARK: Email
    func validateEmail(text: String) {
        if text.isEmptyString() {
            customTextField.hideImage()
            errorLabel.text = ValidationErrorMessage.emailEmpty.description()
        } else if !text.isValidEmailID() {
            customTextField.hideImage()
            errorLabel.text = ValidationErrorMessage.emailInvalid.description()
        } else {
            customTextField.showImage()
            errorLabel.text = ""
        }
    }
    
    func validateConfirmEmail(text: String, email: String) {
        if text.isEmptyString() {
            customTextField.hideImage()
            errorLabel.text = ValidationErrorMessage.emailEmpty.description()
        } else if !text.isValidEmailID() {
            customTextField.hideImage()
            errorLabel.text = ValidationErrorMessage.emailInvalid.description()
        } else if text != email {
            customTextField.hideImage()
            errorLabel.text = ValidationErrorMessage.emailMismatch.description()
        } else {
            customTextField.showImage()
            errorLabel.text = ""
        }
    }

    
    // MARK: Password
    func validatePassword(text: String) {
        if text.isEmptyString() {
            customTextField.hideImage()
            errorLabel.text = ValidationErrorMessage.passwordEmpty.description()
        } else if !text.isValidPassword() {
            customTextField.hideImage()
            errorLabel.text = ValidationErrorMessage.passwordInvalid.description()
        } else {
            customTextField.showImage()
            errorLabel.text = ""
        }
    }

    // MARK: Confirm Password

    func validateConfirmPassword(text: String, password: String) {
        if text.isEmptyString() {
            customTextField.hideImage()
            errorLabel.text = ValidationErrorMessage.passwordEmpty.description()
        } else if !text.isValidPassword() {
            customTextField.hideImage()
            errorLabel.text = ValidationErrorMessage.passwordInvalid.description()
        } else if text != password {
            customTextField.hideImage()
            errorLabel.text = ValidationErrorMessage.passwordMismatch.description()
        } else {
            customTextField.showImage()
            errorLabel.text = ""
        }
    }
    
    // MARK: First Name
    func validateFirstName(text: String) {
        if text.isEmptyString() {
            customTextField.hideImage()
            errorLabel.text = ValidationErrorMessage.firstName.description()
        } else {
            customTextField.showImage()
            errorLabel.text = ""
        }
    }

    // MARK: Last Name
    func validateLastName(text: String) {
        if text.isEmptyString() {
            customTextField.hideImage()
            errorLabel.text = ValidationErrorMessage.lastName.description()
        } else {
            customTextField.showImage()
            errorLabel.text = ""
        }
    }

    // MARK: Display Name
    func validateDisplayName(text: String) {
        if text.isEmptyString() {
            customTextField.hideImage()
            errorLabel.text = ValidationErrorMessage.displayName.description()
        } else {
            customTextField.showImage()
            errorLabel.text = ""
        }
    }

    // MARK: Birthday
    func validateBirthday(text: String) {
        
        let inputDateFormatter = DateFormatter()
        inputDateFormatter.dateFormat = "dd-MMM-yyyy"
        let date = inputDateFormatter.date(from: text)
        if text.isEmptyString() || date == nil {
            errorLabel.text = ValidationErrorMessage.birthday.description()
        } else {
            errorLabel.text = ""
        }
    }
    

    // MARK: Country
    func validateCountry(text: String) {
        if text.isEmptyString() {
            errorLabel.text = ValidationErrorMessage.country.description()
        } else {
            errorLabel.text = ""
        }
    }

    // MARK: Contact Number
    func validatePhone(text: String) {
        if text.isEmptyString() {
            customTextField.hideImage()
            errorLabel.text = ValidationErrorMessage.phoneEmpty.description()
        } else if !text.isValidPhoneNumber() {
            customTextField.hideImage()
            errorLabel.text = ValidationErrorMessage.phoneInvalid.description()
        } else {
            customTextField.showImage()
            errorLabel.text = ""
        }
    }
    
    func validateConfirmPhone(text: String, phone: String) {
        if text.isEmptyString() {
            customTextField.hideImage()
            errorLabel.text = ValidationErrorMessage.phoneEmpty.description()
        } else if !text.isValidPhoneNumber() {
            customTextField.hideImage()
            errorLabel.text = ValidationErrorMessage.phoneInvalid.description()
        } else if text != phone {
            customTextField.hideImage()
            errorLabel.text = ValidationErrorMessage.phoneMismatch.description()
        } else {
            customTextField.showImage()
            errorLabel.text = ""
        }
    }

    // MARK: Student Name
    func validateStudentName(text: String) {
        if text.isEmptyString() {
            customTextField.hideImage()
            errorLabel.text = ValidationErrorMessage.studentName.description()
        } else {
            customTextField.showImage()
            errorLabel.text = ""
        }
    }


}
