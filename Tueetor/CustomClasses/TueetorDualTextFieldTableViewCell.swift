//
//  TueetorDualTextFieldTableViewCell.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 11/04/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit

class TueetorDualTextFieldTableViewCell: UITableViewCell {


    @IBOutlet weak var textFieldOne: TueetorTextField!
    @IBOutlet weak var textFieldTwo: TueetorTextField!
    @IBOutlet weak var errorMessageLabelOne: UILabel!
    @IBOutlet weak var errorMessageLabelTwo: UILabel!
    
    class func cellIdentifier() -> String {
        return "TueetorDualTextFieldTableViewCell"
    }
    
    func configureCellWithTitles(title: [String], index: Int) {
        textFieldOne.tag = index
        textFieldOne.placeholder = title[0]
        textFieldOne.title = title[0]
        textFieldOne.textFont = textFieldLightFont
        textFieldOne.placeholderFont = textFieldDefaultFont
        textFieldOne.titleFont = textFieldDefaultFont

        textFieldTwo.tag = index
        textFieldTwo.placeholder = title[1]
        textFieldTwo.title = title[1]
        textFieldTwo.textFont = textFieldLightFont
        textFieldTwo.placeholderFont = textFieldDefaultFont
        textFieldTwo.titleFont = textFieldDefaultFont

    }
    
}

// MARK: - Validations

extension TueetorDualTextFieldTableViewCell {
    
    // MARK: Title
    func validateTitle(text: String) {
        if text.isEmptyString() {
            errorMessageLabelOne.text = ValidationErrorMessage.title.description()
        } else {
            errorMessageLabelOne.text = ""
        }
    }
    
    // MARK: Gender
    func validateGender(text: String) {
        if text.isEmptyString() {
            errorMessageLabelTwo.text = ValidationErrorMessage.gender.description()
        } else {
            errorMessageLabelTwo.text = ""
        }
    }
    
    // MARK: City
    func validateCity(text: String) {
//        if text.isEmptyString() {
//            textFieldOne.hideImage()
//            errorMessageLabelOne.text = "Please enter City."
//        } else {
//            textFieldOne.showImage()
//            errorMessageLabelOne.text = ""
//        }
    }
    
    // MARK: Zip Code
    func validateZip(text: String) {
//        if !text.isValidPostalCode() {
//            textFieldTwo.hideImage()
//            errorMessageLabelTwo.text = ValidationErrorMessage.postalCode.description()
//        } else {
//            if text.isEmpty {
//                textFieldTwo.hideImage()
//            } else {
//                textFieldTwo.showImage()
//            }
//            errorMessageLabelTwo.text = ""
//        }
    }
}
