//
//  TueetorButtonTextField.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 09/04/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

protocol TueetorButtonTextFieldDelegate {
    func actionButtonTapped()
}

//@IBDesignable
class TueetorButtonTextField: SkyFloatingLabelTextField {

    
    public var actionButton: UIButton!
    public var buttonWidth: CGFloat = 60.0
    
    var textFieldButtonDelegate: TueetorButtonTextFieldDelegate?
    
    @IBInspectable
    public var buttonName: String? {
        didSet {
            actionButton?.setTitle(buttonName, for: .normal)
        }
    }
    
    public var textFont: UIFont? {
        didSet {
            self.titleFont = textFont ?? textFieldDefaultFont
            self.placeholderFont = textFont ?? textFieldDefaultFont
            self.font = textFont ?? textFieldDefaultFont
        }
    }

    // MARK: Initializers
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        createActionButton()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        createActionButton()
    }
    
    // MARK: Creating the icon label
    
    func createActionButton() {
        actionButton = UIButton(type: .custom)
        actionButton.addTarget(self, action: #selector(actionButtonTapped), for: .touchUpInside)
        actionButton.alpha = 0.6
        addSubview(actionButton)
    }
    
    // MARK: Handling the icon color
    
    override public func updateColors() {
        super.updateColors()
    }
    
    
    // MARK: Custom layout overrides
    
    override public func textRect(forBounds bounds: CGRect) -> CGRect {
        var rect = super.textRect(forBounds: bounds)
        rect.origin.x = 0
        rect.size.width -= buttonWidth
        return rect
    }
    
    override public func editingRect(forBounds bounds: CGRect) -> CGRect {
        var rect = super.textRect(forBounds: bounds)
        rect.origin.x = 0
        rect.size.width -= buttonWidth
        return rect
    }
    
    override public func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        var rect = super.placeholderRect(forBounds: bounds)
        rect.origin.x = 0
        rect.size.width -= buttonWidth
        return rect
    }
    
    override public func layoutSubviews() {
        super.layoutSubviews()
        let textWidth: CGFloat = bounds.size.width
        
        actionButton.frame = CGRect(
            x: textWidth - buttonWidth,
            y: bounds.size.height - textHeight(),
            width: buttonWidth,
            height: textHeight()
        )
    }
    
    // MARK: Actions
    
    @objc func actionButtonTapped() {
        textFieldButtonDelegate?.actionButtonTapped()
    }
    
    func setButtonTitle(title: String, font: UIFont = textFieldDefaultFont) {
        actionButton.titleLabel?.font = font
        actionButton.setTitle(title, for: .normal)
    }
    
    

}
