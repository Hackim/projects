//
//  WebViewController.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 03/07/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit
import WebKit

class WebViewController: BaseViewController {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var holderView: UIView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var webView: WKWebView!
    var link: URL!
    var titleString: String!

    override func loadView() {
        super.loadView()
        let webConfiguration = WKWebViewConfiguration()
        webView = WKWebView(frame: CGRect.init(x: 0, y: 0, width: ScreenWidth, height: ScreenHeight - 66.5), configuration: webConfiguration)
        webView.uiDelegate = self
        webView.navigationDelegate = self
        holderView.backgroundColor = UIColor.orange
        holderView.addSubview(webView)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        (self.tabBarController as! BaseTabBarViewController).tabBar.isHidden = true
        (self.tabBarController as! BaseTabBarViewController).hideCenterButton()

        titleLabel.text = titleString ?? ""
        let myRequest = URLRequest(url: link)
        webView.load(myRequest)
    }

}

extension WebViewController: WKUIDelegate, WKNavigationDelegate {
    
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        activityIndicator.stopAnimating()
    }
    
}
