//
//  DashboardViewController.swift
//  Tueetor
//
//  Created by Phaninder on 08/04/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit
import RxSwift

class DashboardViewController: BaseViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var dashboardDetails: DashboardInfo!
    var disposableBag = DisposeBag()
    var permissionsScreenShownOne = false
    var showHUD = true
    var refreshData = false
    private let refreshControl = UIRefreshControl()

    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 10.0, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.addSubview(refreshControl)
        }
        refreshControl.addTarget(self, action: #selector(refreshData(_:)), for: .valueChanged)

        tableView.tableFooterView = UIView()
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(openURL(_:)), name: .openURLInSafari,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(refreshDashboardData), name: .refreshDashboardData,
                                               object: nil)

        if UserStore.shared.isLoggedIn == true, appDelegate.user == nil {
            fetchUserDetails()
        } else {
            fetchDashboard()
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        (self.tabBarController as! BaseTabBarViewController).tabBar.isHidden = false
        (self.tabBarController as! BaseTabBarViewController).showCenterButton()
        if refreshData {
            refreshData = false
            fetchDashboard()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if permissionsScreenShownOne == false {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            let user = appDelegate.user
            let userLoggedIn = UserStore.shared.isLoggedIn || user != nil
            
            if userLoggedIn,
                UserStore.shared.userEmail != UserStore.shared.touchIDEmail,
                UserStore.shared.isBioMetricEnabledByUser {
                UserStore.shared.isBioMetricEnabledByUser = false
                let alert = UIAlertController(title: AppName, message: "We have noticed a change in user login. Touch ID has been disabled. Renable it from the Settings Page.".localized, preferredStyle: .alert)
                let yesAction = UIAlertAction(title: AlertButton.ok.description(), style: .default) { (action) in
                    DispatchQueue.main.async {
                        self.checkForPermissions()
                    }
                }
                alert.addAction(yesAction)
                self.present(alert, animated: true, completion: nil)
            } else {
                checkForPermissions()
            }
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: .openURLInSafari, object: nil)
    }
    
    @objc func refreshData(_ sender: Any) {
        if UserStore.shared.isLoggedIn == true {
            fetchUserDetails()
        } else {
            fetchDashboard()
        }
        if let refreshControl = sender as? UIRefreshControl {
            refreshControl.endRefreshing()
        }
    }
    
    @objc func refreshDashboardData() {
        refreshData = true
    }

    @objc func openURL(_ notification: NSNotification) {
        if let urlString = notification.userInfo?["url"] as? String,
            let url = URL(string: urlString),
            let title = notification.userInfo?["title"] as? String {
            self.pushToWebViewWithURL(url, title: title)
        }
    }


    func checkForPermissions() {
        if UserStore.shared.isNotificationUnAuthorized == true || Utilities.isLocationPermissionDenied() {
            showPermissionsScreen()
        }
    }
    
    func showPermissionsScreen() {
        permissionsScreenShownOne = true
        let permissionsViewController = PermissionsViewController.instantiateFromAppStoryboard(appStoryboard: .Main)
        self.navigationController?.pushViewController(permissionsViewController, animated: true)
    }
    
}

//MARK: - TableView Delegate and Datasource

extension DashboardViewController: UITableViewDataSource, UITableViewDelegate {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let _ = dashboardDetails else {
            return 0
        }
        return 6
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: DashboardStatsTableViewCell.cellIdentifier(), for: indexPath) as! DashboardStatsTableViewCell
            cell.delegate = self
            cell.configureCell(dashboardDetails!)
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: DashboardSubjectsTableViewCell.cellIdentifier(), for: indexPath) as! DashboardSubjectsTableViewCell
            cell.delegate = self
            cell.configureCellWithSubjects(dashboardDetails.topSubjects)
            var title = "Show Top Subjects ".localized
            title += "(\(dashboardDetails.topSubjectsCount))"
            cell.moreSubjectsButton.setTitle(title, for: .normal)

            return cell
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: DashboardPartnersTableViewCell.cellIdentifier(), for: indexPath) as! DashboardPartnersTableViewCell
            var title = "Show All Partners ".localized
            title += "(\(dashboardDetails.featuredPartnersCount))"
            cell.showMoreButton.setTitle(title, for: .normal)
            cell.partners = dashboardDetails.partners
            cell.reloadCollectionViewData()
            cell.showMoreButton.isHidden = false
            cell.delegate = self
            return cell
        case 3:
            let cell = tableView.dequeueReusableCell(withIdentifier: DashboardTutorsTableViewCell.cellIdentifier(), for: indexPath) as! DashboardTutorsTableViewCell
            cell.delegate = self
            cell.tutors = dashboardDetails.topTutors
            var title = "Show All Trainers ".localized
            cell.reloadCollectionViewData()
            title += "(\(dashboardDetails.featuredTutorsCount))"
            cell.moreTutorsButton.setTitle(title, for: .normal)
            return cell
        case 4:
            let cell = tableView.dequeueReusableCell(withIdentifier: DashboardAnnouncementsTableViewCell.cellIdentifier(), for: indexPath) as! DashboardAnnouncementsTableViewCell
            cell.announcements = dashboardDetails.announcements
            cell.reloadCollectionViewData()
            return cell
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: DashboardBannerTableViewCell.cellIdentifier(), for: indexPath) as! DashboardBannerTableViewCell
            cell.delegate = self
            cell.banners = dashboardDetails.banners
            cell.configureCell()
            cell.reloadCollectionViewData()
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == 5 {
            if let bannerCell = tableView.cellForRow(at: indexPath) as? DashboardBannerTableViewCell {
                bannerCell.timer.invalidate()
                bannerCell.timer = nil
            }
            print("removed")
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0:
            return DashboardStatsTableViewCell.cellHeight()
        case 1:
            return DashboardSubjectsTableViewCell.cellHeight()
        case 2:
//            if dashboardDetails.featuredPartnersCount > 4 {
                return DashboardPartnersTableViewCell.cellHeight()
//            }
//            return 250.0
        case 3:
//            if dashboardDetails.featuredTutorsCount > 10 {
                return DashboardTutorsTableViewCell.cellHeight()
//            }
//            return 195.0
        case 4:
            return DashboardAnnouncementsTableViewCell.cellHeight()
        default:
            return DashboardBannerTableViewCell.cellHeight()
        }
    }
    
}

extension DashboardViewController {
    
    func fetchDashboard() {
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }
        
        if showHUD == true {
            Utilities.showHUD(forView: self.view, excludeViews: [])
        }
        let dashboardObserver = ApiManager.shared.apiService.fetchDashboardDetails()
        let dashboardDisposable = dashboardObserver.subscribe(onNext: {(dashboardDetails) in
            Utilities.hideHUD(forView: self.view)
            self.dashboardDetails = dashboardDetails
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.showSimpleAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        })
        dashboardDisposable.disposed(by: disposableBag)
    }
    
    func fetchUserDetails() {
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }
        
        Utilities.showHUD(forView: self.view, excludeViews: [])
        let userObserver = ApiManager.shared.apiService.fetchUserDetails()
        let userDisposable = userObserver.subscribe(onNext: {(user) in
            DispatchQueue.main.async {
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.user = user
                self.showHUD = false
                self.fetchDashboard()
            }
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.showSimpleAlertWithMessage(error.localizedDescription)
                    }
                }
                self.fetchDashboard()
            })
        })
        userDisposable.disposed(by: disposableBag)

    }
}

extension DashboardViewController: DashboardSubjectsTableViewCellDelegate {
    
    func showAllCategoriesScreen() {
        let topSubjectsVC = TopSubjectsViewController.instantiateFromAppStoryboard(appStoryboard: .Dashboard)
        //        topSubjectsVC.categories = dashboardDetails.topSubjects
        navigationController?.pushViewController(topSubjectsVC, animated: true)
    }
    
    func showCategorySubjects(_ button: UIButton) {
        let category = dashboardDetails.topSubjects[button.tag]
        //        guard category.subjects.count > 3 else {
        //            return
        //        }
        //
        let subListingVC = SubjectListingViewController.instantiateFromAppStoryboard(appStoryboard: .Dashboard)
        subListingVC.categoryID = category.id
        subListingVC.headerText = category.name
        navigationController?.pushViewController(subListingVC, animated: true)
    }
    
}

extension DashboardViewController: DashboardBannerTableViewCellDelegate {
    
    func openURLInWebViewForBanner(_ banner: Banner) {
        if let urlString = banner.url,
            let url = URL(string: urlString),
            let title = banner.title {
            self.pushToWebViewWithURL(url, title: title)
        }

    }
    
    func navigateToAgencyProfileWithID(_ agencyID: String) {
        //Do here something later
    }
}

extension DashboardViewController: DashboardTutorsTableViewCellDelegate {
    
    func showFeaturedTutors() {
        let findViewController = FindViewController.instantiateFromAppStoryboard(appStoryboard: .Find)
        findViewController.isPartOfTabbar = false
        let filterParameters = FilterParams.init()
        filterParameters.isFeatured = true
        filterParameters.showAgencyPin = false
        findViewController.filterParams = filterParameters
        self.navigationController?.pushViewController(findViewController, animated: true)
    }
    
    func tutorSelected(_ tutor: Tutor) {
        let tutorProfile = TutorProfileViewController.instantiateFromAppStoryboard(appStoryboard: .Dashboard)
        tutorProfile.tutorID = String(tutor.ID)
        navigationController?.pushViewController(tutorProfile, animated: true)
    }
}

extension DashboardViewController: DashboardStatsTableViewCellDelegate {
    
    func showTrainers() {
        self.navigateToFindScreen()
    }
    
    func showLeads() {
        self.navigateToFindScreen()
    }
    
    func showLocations() {
        self.navigateToFindScreen()
    }
    
    func showSubjects() {
        self.navigateToFindScreen()
    }
    
    func showAssignments() {
        self.navigateToFindScreen()
    }
}

extension DashboardViewController: DashboardPartnersTableViewCellDelegate {
    
    func showFeaturedAgents() {
        let findViewController = FindViewController.instantiateFromAppStoryboard(appStoryboard: .Find)
        findViewController.isPartOfTabbar = false
        let filterParameters = FilterParams.init()
        filterParameters.isFeatured = true
        filterParameters.showAgencyPin = true
        filterParameters.showTutorSubPin = false
        filterParameters.showMatchinSubPin = false
        filterParameters.showStudentSubPin = false
        filterParameters.showMatchingSubLevelPin = false
        findViewController.filterParams = filterParameters
        self.navigationController?.pushViewController(findViewController, animated: true)
    }
    
    func partnerSelected(_ partner: Partner) {
        let agencyProfile = PartnerProfileViewController.instantiateFromAppStoryboard(appStoryboard: .Dashboard)
        agencyProfile.agencyID = partner.ID
        agencyProfile.assetID = partner.assetId
        navigationController?.pushViewController(agencyProfile, animated: true)
    }
}
