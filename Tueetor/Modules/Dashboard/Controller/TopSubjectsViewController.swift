//
//  TopSubjectsViewController.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 27/05/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit
import RxSwift

class TopSubjectsViewController: BaseViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    
    var categories: [SubjectCategory]!
    var disposableBag = DisposeBag()

    private let refreshControl = UIRefreshControl()

    override func viewDidLoad() {
        super.viewDidLoad()        
        if #available(iOS 10.0, *) {
            collectionView.refreshControl = refreshControl
        } else {
            collectionView.addSubview(refreshControl)
        }
        refreshControl.addTarget(self, action: #selector(refreshData(_:)), for: .valueChanged)
        fetchTopSubjects()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        (self.tabBarController as! BaseTabBarViewController).tabBar.isHidden = true
        (self.tabBarController as! BaseTabBarViewController).hideCenterButton()
    }

    @objc func refreshData(_ sender: Any) {
        fetchTopSubjects()
        if let refreshControl = sender as? UIRefreshControl {
            refreshControl.endRefreshing()
        }
    }


}

extension TopSubjectsViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let categoriesArray = categories {
            return categoriesArray.count
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: TopSubjectsCollectionViewCell.cellIdentifier(), for: indexPath) as! TopSubjectsCollectionViewCell
        cell.configureCellWithTopSubject(categories[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (ScreenWidth/2) - 31, height: 185)
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let category = categories[indexPath.item]
        
        let subListingVC = SubjectListingViewController.instantiateFromAppStoryboard(appStoryboard: .Dashboard)
        subListingVC.categoryID = category.id
        subListingVC.headerText = category.name
        navigationController?.pushViewController(subListingVC, animated: true)
    }
}

extension TopSubjectsViewController {
    
    func fetchTopSubjects() {
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }
        Utilities.showHUD(forView: self.view, excludeViews: [])
        let subjectsObserver = ApiManager.shared.apiService.fetchTopSubjects()
        let subjectsDisposable = subjectsObserver.subscribe(onNext: {(topSubjects) in
            self.categories = topSubjects
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                self.collectionView.reloadData()
            })
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.showSimpleAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        })
        subjectsDisposable.disposed(by: disposableBag)
    }

}

