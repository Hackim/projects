//
//  FeaturedTueetorsViewController.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 28/05/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit
import RxSwift

class FeaturedTueetorsViewController: BaseViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    
    var tutorPagination = TutorPagination.init()
    var disposableBag = DisposeBag()
    var tutorObserver: Observable<TutorPagination>!
    var isFetchingData = false
    var isFirstTime = true
    var cellHeights: [Int: CGFloat] = [:]
    private let refreshControl = UIRefreshControl()

    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 10.0, *) {
            collectionView.refreshControl = refreshControl
        } else {
            collectionView.addSubview(refreshControl)
        }
        refreshControl.addTarget(self, action: #selector(refreshData(_:)), for: .valueChanged)
        fetchFeaturedTutors()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        (self.tabBarController as! BaseTabBarViewController).tabBar.isHidden = true
        (self.tabBarController as! BaseTabBarViewController).hideCenterButton()
    }
    
    @objc func refreshData(_ sender: Any) {
        tutorPagination.paginationType = .new
        fetchFeaturedTutors()
        if let refreshControl = sender as? UIRefreshControl {
            refreshControl.endRefreshing()
        }
    }
    
    @IBAction func filterButtonTapped(_ sender: UIButton) {
        Utilities.showToast(withString: "Under development")
    }
    
}

extension FeaturedTueetorsViewController: UICollectionViewDelegateFlowLayout, UICollectionViewDelegate, UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return tutorPagination.tutors.count + 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard indexPath.row != tutorPagination.tutors.count else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: LoadMoreCollectionViewCell.cellIdentifier(), for: indexPath) as! LoadMoreCollectionViewCell
            cell.activityIndicator.startAnimating()
            return cell
        }
        
        let tutor = tutorPagination.tutors[indexPath.item]
        if tutor.subjects.count > 3 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: TopTutorFourSubjectsCollectionViewCell.cellIdentifier(), for: indexPath) as! TopTutorFourSubjectsCollectionViewCell
            
            cell.configureCellWithTutor(tutorPagination.tutors[indexPath.item])
            return cell

        } else if tutor.subjects.count == 3 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: TopTutorThreeSubjectsCollectionViewCell.cellIdentifier(), for: indexPath) as! TopTutorThreeSubjectsCollectionViewCell
            
            cell.configureCellWithTutor(tutorPagination.tutors[indexPath.item])
            return cell

        } else if tutor.subjects.count == 2 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: TopTutorTwoSubjectsCollectionViewCell.cellIdentifier(), for: indexPath) as! TopTutorTwoSubjectsCollectionViewCell
            
            cell.configureCellWithTutor(tutorPagination.tutors[indexPath.item])
            return cell

        } else if tutor.subjects.count == 1 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: TopTutorOneSubjectCollectionViewCell.cellIdentifier(), for: indexPath) as! TopTutorOneSubjectCollectionViewCell
            
            cell.configureCellWithTutor(tutorPagination.tutors[indexPath.item])
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: TopTutorNoSubjectsCollectionViewCell.cellIdentifier(), for: indexPath) as! TopTutorNoSubjectsCollectionViewCell
            
            cell.configureCellWithTutor(tutorPagination.tutors[indexPath.item])
            return cell

        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        guard indexPath.row != tutorPagination.tutors.count else {
            return tutorPagination.hasMoreToLoad ? CGSize(width: ScreenWidth - 56 , height: 50) : CGSize(width: ScreenWidth - 56 , height: 0)
        }
        let tutor = tutorPagination.tutors[indexPath.item]
        let height: CGFloat!
        if tutor.subjects.count > 3 {
            height = TopTutorFourSubjectsCollectionViewCell.cellHeight()
        } else if tutor.subjects.count == 3 {
            height = TopTutorThreeSubjectsCollectionViewCell.cellHeight()
        } else if tutor.subjects.count == 2 {
            height = TopTutorTwoSubjectsCollectionViewCell.cellHeight()
        } else if tutor.subjects.count == 1 {
            height = TopTutorOneSubjectCollectionViewCell.cellHeight()
        } else {
            height = TopTutorNoSubjectsCollectionViewCell.cellHeight()
        }
        return CGSize(width: ScreenWidth - 56 , height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if indexPath.row == tutorPagination.tutors.count && self.tutorPagination.hasMoreToLoad && !isFetchingData {
            fetchFeaturedTutors()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard indexPath.row != tutorPagination.tutors.count else {
            return
        }
        let tutorProfile = TutorProfileViewController.instantiateFromAppStoryboard(appStoryboard: .Dashboard)
        tutorProfile.tutorID = String(self.tutorPagination.tutors[indexPath.item].ID)
        navigationController?.pushViewController(tutorProfile, animated: true)
    }
}

extension FeaturedTueetorsViewController {
    
    func fetchFeaturedTutors() {
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }
        
        if tutorPagination.paginationType != .old {
            self.tutorPagination.currentPageNumber = 1
            if isFirstTime == true {
                Utilities.showHUD(forView: self.view, excludeViews: [])
            } else {
                UIApplication.shared.isNetworkActivityIndicatorVisible = true
            }
            
        } else {
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
        }
        
        tutorObserver = ApiManager.shared.apiService.fetchFeaturedTrainers(tutorPagination.currentPageNumber)
        let tutorDisposable = tutorObserver.subscribe(onNext: {(paginationObject) in
            self.isFetchingData = false
            
            self.tutorPagination.appendDataFromObject(paginationObject)
            for index in 0..<self.tutorPagination.tutors.count {
                let tutor = self.tutorPagination.tutors[index]
                if tutor.subjects.count > 2 {
                    self.cellHeights[index] = 169.0
                } else if tutor.subjects.count > 0 {
                    self.cellHeights[index] = 130.0
                } else {
                    self.cellHeights[index] = 91.0
                }
            }
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if self.tutorPagination.paginationType != .old && self.isFirstTime == true {
                    self.isFirstTime = false
                } else {
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                }
                self.collectionView.reloadData()
            })
        }, onError: {(error) in
            self.isFetchingData = false
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
            })
        })
        tutorDisposable.disposed(by: disposableBag)

    }
    
}

