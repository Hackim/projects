//
//  DocumentsViewController.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 06/07/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit

class DocumentsViewController: BaseViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var documents: [Document]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView()
    }

    @IBAction func closeButtonTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
}

extension DocumentsViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return documents.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: MyDocumentTableViewCell.cellIdentifier(), for: indexPath) as! MyDocumentTableViewCell
        cell.configureCellWithDocument(documents[indexPath.row], index: indexPath.row)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        tableView.estimatedRowHeight = 98.0
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let documentsPage = DocumentPageViewController.instantiateFromAppStoryboard(appStoryboard: .Profile)
        var names = [String]()
        var links = [String]()
        for document in documents {
            names.append(document.name)
            links.append(document.url)
        }
        documentsPage.titles = names
        documentsPage.links = links
        documentsPage.startIndex = indexPath.row
        documentsPage.isPushed = false
        self.present(documentsPage, animated: true, completion: nil)
    }

    
}


