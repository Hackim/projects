//
//  StudentProfileViewController.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 06/07/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit
import RxSwift
import SafariServices
import SpreadsheetView

protocol StudentProfileViewControllerDelegate {
    func refreshShortlistDataForStudent()
    func refreshDataForStudentWithID(_ ID: String)
}
class StudentProfileViewController: BaseViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var disposableBag = DisposeBag()
    var studentDetail: StudentDetailedInfo!
    var studentID: String!
    var currentSelectedSection: Int = 0
    var documentCellHeight: CGFloat = 300.0
    var currentWeekDaySelection = 0
    var weekSelectionHeader: CalendarHeaderTableViewCell!
    var delegate: StudentProfileViewControllerDelegate?
    var currentSelectedSubject = -1
    private let refreshControl = UIRefreshControl()
    var filteredSchedules: [String: [Schedule]]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 10.0, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.addSubview(refreshControl)
        }
        refreshControl.addTarget(self, action: #selector(refreshData(_:)), for: .valueChanged)
        fetchStudentDetails()
        tableView.tableFooterView = UIView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        (self.tabBarController as! BaseTabBarViewController).tabBar.isHidden = true
        (self.tabBarController as! BaseTabBarViewController).hideCenterButton()
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }
    
    @objc func refreshData(_ sender: Any) {
        if let refreshControl = sender as? UIRefreshControl {
            refreshControl.endRefreshing()
        }
        fetchStudentDetails()
    }
    
    
    @IBAction func gotoMessage(btn:UIButton){
        
    }
    
    
    func showCommentAlert() {
        let alertController = UIAlertController(title: AppName,
                                                message: AlertMessage.newShortlistComment.description(),
                                                preferredStyle: .alert)
        
        
        let submitAction = UIAlertAction(title: AlertButton.submit.description(),
                                         style: .default) { [weak alertController] _ in
                                            guard let alertController = alertController, let textField = alertController.textFields?.first else { return }
                                            DispatchQueue.main.async {
                                                print(textField.text!)
                                                self.addTutorToShortlist(comment: textField.text!)
                                            }
        }
        submitAction.isEnabled = false
        alertController.addAction(submitAction)
        
        alertController.addTextField { textField in
            textField.placeholder = "New comment".localized
            
            NotificationCenter.default.addObserver(forName: NSNotification.Name.UITextFieldTextDidChange, object: textField, queue: OperationQueue.main, using: { (notification) in
                submitAction.isEnabled = !textField.text!.isEmpty
            })
        }
        
        
        let cancelAction = UIAlertAction(title: AlertButton.cancel.description(), style: .cancel, handler: nil)
        alertController.addAction(cancelAction)
        present(alertController, animated: true, completion: nil)
    }
    
    func showDeleteAlert() {
        let alert = UIAlertController(title: AppName, message: AlertMessage.removeShortlist.description(), preferredStyle: .alert)
        
        let confirmAction = UIAlertAction(title: AlertButton.confirm.description(), style: .default, handler: { (action) in
            DispatchQueue.main.async {
                self.removeUserFromShortlist()
            }
        })
        alert.addAction(confirmAction)
        
        let cancelAction = UIAlertAction(title: AlertButton.cancel.description(), style: .default, handler: nil)
        alert.addAction(cancelAction)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    
    
}

extension StudentProfileViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let _ = studentDetail {
            if currentSelectedSection == 1 {
                if studentDetail.subjects.count > 0 {
                    return 2 + studentDetail.subjects.count
                } else {
                    return 3
                }
            } else {
                return 3
            }
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: StudentInfoTableViewCell.cellIdentifier(), for: indexPath) as! StudentInfoTableViewCell
            cell.delegate = self
            cell.configureCellWithStudent(studentDetail)
            return cell
        } else if indexPath.row == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: TutorMoreInfoTableViewCell.cellIdentifier(), for: indexPath) as! TutorMoreInfoTableViewCell
            cell.page = .student
            cell.delegate = self
            cell.selectedIndex = currentSelectedSection
            cell.collectionView.reloadData()
            return cell
        } else {
            switch currentSelectedSection {
            case 0:
                if studentDetail.about.isEmpty {
                    let cell = tableView.dequeueReusableCell(withIdentifier: EmptyMessageTableViewCell.cellIdentifier(), for: indexPath) as! EmptyMessageTableViewCell
                    cell.messageLabel.text = EmptyMessageText.about.description()
                    return cell
                } else {
                    let cell = tableView.dequeueReusableCell(withIdentifier: TutorAboutTableViewCell.cellIdentifier(), for: indexPath) as! TutorAboutTableViewCell
                    cell.configureCellWithStudent(studentDetail)
                    return cell
                }
            case 1:
                if studentDetail.subjects.count > 0 {
                    let cell = tableView.dequeueReusableCell(withIdentifier: SubjectListingTableViewCell.cellIdentifier(), for: indexPath) as! SubjectListingTableViewCell
                    cell.delegate =  self
                    cell.configureCellWithSubject(studentDetail.subjects[indexPath.row - 2], indexPath.row - 2, isStudent: true)
                    return cell
                } else {
                    let cell = tableView.dequeueReusableCell(withIdentifier: EmptyMessageTableViewCell.cellIdentifier(), for: indexPath) as! EmptyMessageTableViewCell
                    cell.messageLabel.text = EmptyMessageText.subject.description()
                    return cell
                }
            default:
                let cell = tableView.dequeueReusableCell(withIdentifier: ScheduleTableViewCell.cellIdentifier(), for: indexPath) as! ScheduleTableViewCell
                cell.delegate = self
                if let schedules = filteredSchedules {
                    cell.configureCellWithSchedules(schedules)
                } else {
                    cell.configureCellWithSchedules(studentDetail.schedules)
                }
                return cell
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return StudentInfoTableViewCell.cellHeight()
        } else if indexPath.row == 1 {
            return TutorMoreInfoTableViewCell.cellHeight()
        } else {
            switch currentSelectedSection {
            case 0:
                if studentDetail.about.isEmpty {
                    tableView.estimatedRowHeight = 44.0
                    return UITableViewAutomaticDimension
                } else {
                    tableView.estimatedRowHeight = 55
                    return UITableViewAutomaticDimension
                }
            case 1:
                if studentDetail.subjects.count > 0 {
                    return indexPath.row == currentSelectedSubject ? 185.0 : 94.0
                } else {
                    tableView.estimatedRowHeight = 44.0
                    return UITableViewAutomaticDimension
                }
            default:
                return ScheduleTableViewCell.cellHeight()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return StudentInfoTableViewCell.cellHeight()
        } else if indexPath.row == 1 {
            return TutorMoreInfoTableViewCell.cellHeight()
        } else {
            switch currentSelectedSection {
            case 0:
                if studentDetail.about.isEmpty {
                    return 44
                } else {
                    return 55
                }
            case 1:
                if studentDetail.subjects.count > 0 {
                    return indexPath.row == currentSelectedSubject ? 185.0 : 94.0
                } else {
                    return 44
                }
            default:
                return ScheduleTableViewCell.cellHeight()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section != 0 {
            let sectionHeader = tableView.dequeueReusableCell(withIdentifier: CalendarHeaderTableViewCell.cellIdentifier()) as! CalendarHeaderTableViewCell
            weekSelectionHeader = sectionHeader
            sectionHeader.selectedIndex = currentWeekDaySelection
            sectionHeader.delegate = self
            return sectionHeader
        } else {
            let header = UIView()
            header.backgroundColor = UIColor.white
            return header
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section != 0 {
            return CalendarHeaderTableViewCell.sectionHeight()
        } else {
            return 0.5
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.5
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footer = UIView()
        footer.backgroundColor = UIColor.white
        return footer
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row > 1, currentSelectedSection == 1, studentDetail.subjects.count > 0 {
            currentSelectedSubject = indexPath.row == currentSelectedSubject ? -1 : indexPath.row
            tableView.beginUpdates()
            tableView.endUpdates()
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let weekIndexesValues = Array(studentDetail.weekIndexes.values)
        if weekIndexesValues.contains(indexPath.row),
            currentSelectedSection == 3,
            let header = weekSelectionHeader,
            let dayData = studentDetail.scheduleInfoArray[indexPath.row]{
            let rowIndex = daysArray.index(of: dayData[1])
            if let index = rowIndex, index >= 0 {
                header.selectedIndex = index
                currentWeekDaySelection = index
                header.reloadTheCalendarView()
            }
        }
    }
}

extension StudentProfileViewController: TutorMoreInfoTableViewCellDelegate {
    
    func indexSelected(_ index: Int) {
        currentSelectedSection = index
        filteredSchedules = nil
        tableView.reloadData()
    }
}

extension StudentProfileViewController: CalendarHeaderTableViewCellDelegate {
    
    func indexSelectedForCalendarHeader(_ index: Int) {
        let day = daysArray[index]
        currentWeekDaySelection = index
        self.tableView.scrollToRow(at: IndexPath.init(row: studentDetail.weekIndexes[day]!, section: 1), at: .top, animated: true)
    }
    
}

extension StudentProfileViewController: StudentInfoTableViewCellDelegate {
    
    func shortlistStudentButtonTapped() {
        guard UserStore.shared.isLoggedIn else {
            showLoginAlert()
            return
        }
        

        if studentDetail.student.isFavorite {
            showDeleteAlert()
        } else {
            showCommentAlert()
        }

    }
    
    func messageStudentButtonTapped() {
        guard UserStore.shared.isLoggedIn else {
            showLoginAlert()
            return
        }
        let chatVC = ChatViewController.instantiateFromAppStoryboard(appStoryboard: .Message)
        chatVC.userTwoImageURL = ""//chats[indexPath.row - 1].image
        chatVC.senderID = "\(self.studentDetail.student.ID!)" //chats[indexPath.row - 1].senderID
        chatVC.userTwoName = self.studentDetail.student.name//chats[indexPath.row - 1].displayName
        chatVC.chatID = "" // chats[indexPath.row - 1].pID
        self.navigationController?.pushViewController(chatVC, animated: true)
    }
    
    func reportStudentButtonTapped() {
        guard UserStore.shared.isLoggedIn else {
            showLoginAlert()
            return
        }

        let reportVC = ReportUserViewController(nibName: "ReportUserViewController", bundle: nil)
        
        // Create the dialog
        let popup = PopupDialog(viewController: reportVC,
                                buttonAlignment: .horizontal,
                                transitionStyle: .bounceDown,
                                tapGestureDismissal: true,
                                panGestureDismissal: true)
        
        let buttonOne = CancelButton(title: AlertButton.cancel.description(), height: 60) {
            //Do nothing
        }
        
        
        let buttonTwo = DefaultButton(title: AlertButton.submit.description(), height: 60) {
            self.reportUserWithComment(reportVC.commentsTextView.text ?? "", nature: reportVC.selectedOption)
        }
        
        popup.addButtons([buttonOne, buttonTwo])
        present(popup, animated: true, completion: nil)
        

    }
    
    
}

extension StudentProfileViewController: ScheduleTableViewCellDelegate, UIScrollViewDelegate {
    func showSubjectListing(_ subjects: String, title: String) {
        let subPopupVC = SubjectsPopupViewController(nibName: "SubjectsPopupViewController", bundle: nil)
        subPopupVC.subjectNames = subjects.components(separatedBy: ",")
        subPopupVC.titleString = title
        
        let subjectListingDialogue = PopupDialog(viewController: subPopupVC,
                                                 buttonAlignment: .horizontal,
                                                 transitionStyle: .bounceDown,
                                                 tapGestureDismissal: true,
                                                 panGestureDismissal: true)
        let buttonOne = CancelButton(title: AlertButton.close.description(), height: 60) {
            //Do nothing
        }
        
        subjectListingDialogue.addButtons([buttonOne])
        
        present(subjectListingDialogue, animated: true, completion: nil)
    }
    
    
    func showSubjectsPopup(_ subjects: String, subView: UIView) {
        
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
    }
}

extension StudentProfileViewController: SubjectListingTableViewCellDelegate {
    
    func showScheduleForSubjectAtIndex(_ index: Int) {
        
        let subject = studentDetail.subjects[index]
        filteredSchedules = [String:[Schedule]]()
        let weeksArray = ["MON", "TUE", "WED", "THU", "FRI", "SAT", "SUN"]
        for day in weeksArray {
            let dayTotalSchedule = studentDetail.schedules[day]
            let filteredSchedule = dayTotalSchedule?.filter({ (schedule) -> Bool in
                return schedule.subject.id == subject.id
            })
            filteredSchedules[day] = filteredSchedule
        }
        currentSelectedSection = 3
        tableView.scrollToRow(at: IndexPath.init(row: 1, section: 0), at: .middle, animated: true)
        tableView.reloadData()
    }
    
    func showLocationForSubjectAtIndex(_ index: Int) {
        let subject = studentDetail.subjects[index]
        let locationViewController = LocationViewController.instantiateFromAppStoryboard(appStoryboard: .Profile)
        var latitude = ""
        var longitude = ""
        if let lat = subject.latitude, !lat.isEmpty {
            latitude = lat
            longitude = subject.logitude
        } else {
            let user = (UIApplication.shared.delegate as! AppDelegate).user
            latitude = user?.latitude ?? "0"
            longitude = user?.longitude ?? "0"
        }
        locationViewController.locationToShow = TueetorLocation.init(name: subject.name ?? "", lat: latitude, long: longitude)
        navigationController?.pushViewController(locationViewController, animated: true)
        
    }

}

extension StudentProfileViewController {
    
    func fetchStudentDetails() {
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }
        
        Utilities.showHUD(forView: self.view, excludeViews: [])
        
        let tutorObserver = ApiManager.shared.apiService.fetchStudentDetailsForID(studentID)
        let tutorDisposable = tutorObserver.subscribe(onNext: {(studentDetails) in
            Utilities.hideHUD(forView: self.view)
            self.studentDetail = studentDetails
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.showSimpleAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        })
        tutorDisposable.disposed(by: disposableBag)
    }
    
    func addTutorToShortlist(comment: String) {
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }
        
        guard let userID = UserStore.shared.userID else {
            return
        }
        
        Utilities.showHUD(forView: self.view, excludeViews: [])
        
        let addToShortlistObserver = ApiManager.shared.apiService.addToShortlistForUser(String(userID), shortlistUserID: studentID, reason: comment)
        let addToShortlistDisposable = addToShortlistObserver.subscribe(onNext: {(message) in
            Utilities.hideHUD(forView: self.view)
            
            DispatchQueue.main.async {
                NotificationCenter.default.post(name: .refreshDashboardData, object: nil, userInfo:nil)
                self.delegate?.refreshShortlistDataForStudent()
                self.delegate?.refreshDataForStudentWithID(self.studentID)
                self.showSimpleAlertWithMessage(message)
                self.studentDetail.student.isFavorite = true
                self.tableView.reloadRows(at: [IndexPath.init(row: 0, section: 0)], with: .none)
            }
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.showSimpleAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        })
        addToShortlistDisposable.disposed(by: disposableBag)
    }
    
    func removeUserFromShortlist() {
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }
        
        guard let userID = UserStore.shared.userID else {
            return
        }
        
        Utilities.showHUD(forView: self.view, excludeViews: [])
        
        let deleteShortlistObserver = ApiManager.shared.apiService.deleteShortlistForUser(String(userID), shortlistUserID: studentID)
        let deleteShortlistDisposable = deleteShortlistObserver.subscribe(onNext: {(message) in
            Utilities.hideHUD(forView: self.view)
            
            DispatchQueue.main.async {
                NotificationCenter.default.post(name: .refreshDashboardData, object: nil, userInfo:nil)
                self.delegate?.refreshShortlistDataForStudent()
                self.delegate?.refreshDataForStudentWithID(self.studentID)
                self.showSimpleAlertWithMessage(message)
                self.studentDetail.student.isFavorite = false
                self.tableView.reloadRows(at: [IndexPath.init(row: 0, section: 0)], with: .none)
            }
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.showSimpleAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        })
        deleteShortlistDisposable.disposed(by: disposableBag)
    }
    
    func reportUserWithComment(_ comment: String, nature: Int) {
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }
        
        guard let userID = UserStore.shared.userID else {
            return
        }
        
        var nat = ""
        switch nature {
        case 1:
            nat = "Spam"
        case 2:
            nat = "No Reply"
        default:
            nat = "Improper Conduct"
        }
        
        Utilities.showHUD(forView: self.view, excludeViews: [])
        
        let reportUserObserver = ApiManager.shared.apiService.reportUserWithUserID(studentID, comment: comment, nature: nat, userID: String(userID))
        let reportUserDisposable = reportUserObserver.subscribe(onNext: {(message) in
            Utilities.hideHUD(forView: self.view)
            
            DispatchQueue.main.async {
                self.showSimpleAlertWithMessage(message)
            }
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.showSimpleAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        })
        reportUserDisposable.disposed(by: disposableBag)
    }

}
