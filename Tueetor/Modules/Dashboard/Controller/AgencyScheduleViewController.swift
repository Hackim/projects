//
//  AgencyScheduleViewController.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 06/07/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit

class AgencyScheduleViewController: BaseViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var schedules: [String: [String]]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView()
    }
    
    @IBAction func closeButtonTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
}

extension AgencyScheduleViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CourseScheduleTableViewCell.cellIdentifier(), for: indexPath) as! CourseScheduleTableViewCell
        cell.configureCellWithSchedules(schedules)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return ScreenHeight - 66.0
    }
    
}
