//
//  SubjectListingViewController.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 27/05/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit
import RxSwift

class SubjectListingViewController: BaseViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var searchTextFieldWidthConstraint: NSLayoutConstraint!

    var disposableBag = DisposeBag()
    var isFetchingData = false
    var isSearchActive = false
    var isFirstTime = true
    var searchString = ""
    var headerText: String!
    var categoryID: String!
    
    var subjectPagination = SubjectPagination.init()
    var selectedSubject: Subject!
    var subjectDisposable: Disposable!

    private let refreshControl = UIRefreshControl()

    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 10.0, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.addSubview(refreshControl)
        }

        searchTextFieldWidthConstraint.constant = 0.0
        refreshControl.addTarget(self, action: #selector(refreshData(_:)), for: .valueChanged)
        fetchSubjects()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        (self.tabBarController as! BaseTabBarViewController).tabBar.isHidden = true
        (self.tabBarController as! BaseTabBarViewController).hideCenterButton()
    }

    @objc func refreshData(_ sender: Any) {
        self.subjectPagination.paginationType = .new
        fetchSubjects()
        if let refreshControl = sender as? UIRefreshControl {
            refreshControl.endRefreshing()
        }
    }

    @IBAction func searchButtonTapped(_ sender: UIButton) {
        isSearchActive = !isSearchActive
        if isSearchActive {
            searchTextField.becomeFirstResponder()
        } else {
            searchTextField.resignFirstResponder()
        }
        self.view.layoutIfNeeded()
        UIView.animate(withDuration: 0.7, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 1, options: .curveEaseIn, animations: {
            self.searchTextFieldWidthConstraint.constant = self.isSearchActive ? ScreenWidth - 132.0 : 0
            let imageName = self.isSearchActive ? "search_active" : "search_inactive"
            self.searchButton.setImage(UIImage(named: imageName), for: .normal)
            self.searchButton.backgroundColor = self.isSearchActive ? themeBlueColor : UIColor.clear
            self.view.layoutIfNeeded()
        }) { (completed) in
            
        }
    }

    @IBAction func valueChangedInSearchField(_ sender: Any) {
        if (subjectDisposable != nil) {
            Utilities.hideHUD(forView: view)
            subjectDisposable.dispose()
        }
        subjectPagination.paginationType = .new
        fetchSubjects()
    }

}


extension SubjectListingViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return subjectPagination.subjects.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard indexPath.row != subjectPagination.subjects.count else {
            let cell = tableView.dequeueReusableCell(withIdentifier: LoadMoreTableViewCell.cellIdentifier(), for: indexPath) as! LoadMoreTableViewCell
            cell.activityIndicator.startAnimating()
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: SubjectListTableViewCell.cellIdentifier(), for: indexPath) as! SubjectListTableViewCell
            let subject = subjectPagination.subjects[indexPath.row]
            cell.configureCellWithSubject(subject)
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let sectionHeader = tableView.dequeueReusableCell(withIdentifier: FilterOptionSectionHeaderTableViewCell.cellIdentifier()) as! FilterOptionSectionHeaderTableViewCell
        sectionHeader.titleLabel.text = headerText
        return sectionHeader
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        tableView.estimatedSectionHeaderHeight = 81.0
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.5
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footer = UIView()
        footer.backgroundColor = UIColor.white
        return footer
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == subjectPagination.subjects.count {
            return subjectPagination.hasMoreToLoad ? 50.0 : 0
        }
        tableView.estimatedRowHeight = 100.0
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == subjectPagination.subjects.count && self.subjectPagination.hasMoreToLoad && !isFetchingData {
            fetchSubjects()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let findViewController = FindViewController.instantiateFromAppStoryboard(appStoryboard: .Find)
        findViewController.isPartOfTabbar = false
        let filterParameters = FilterParams.init()
        filterParameters.selectedSubjects.append(subjectPagination.subjects[indexPath.row])
        findViewController.filterParams = filterParameters
        self.navigationController?.pushViewController(findViewController, animated: true)

    }
    
    
    
}

extension SubjectListingViewController: UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if isFetchingData == false, searchString != textField.text {
            subjectPagination.paginationType = .new
            fetchSubjects()
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (textField.text?.isEmpty)! && string == " " {
            return false
        }
        let char = string.cString(using: String.Encoding.utf8)!
        let isBackSpace = strcmp(char, "\\b")
        if (isBackSpace == -92) {
            return true
        }
        return true
    }
}

extension SubjectListingViewController {
    
    func fetchSubjects() {
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }
        searchString = searchTextField.text ?? ""
        
        if subjectPagination.paginationType != .old {
            self.subjectPagination.currentPageNumber = 1
            if isFirstTime == true {
                Utilities.showHUD(forView: self.view, excludeViews: [])
            } else {
                UIApplication.shared.isNetworkActivityIndicatorVisible = true
            }
            
        } else {
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
        }
        
        let subjectObserver = ApiManager.shared.apiService.fetchSubjectsWithInCategory(categoryID, page: subjectPagination.currentPageNumber, query: searchString)
        subjectDisposable = subjectObserver.subscribe(onNext: {(paginationObject) in
            self.isFetchingData = false
            
            self.subjectPagination.appendDataFromObject(paginationObject)
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if self.subjectPagination.paginationType != .old && self.isFirstTime == true {
                    self.isFirstTime = false
                } else {
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                }
                self.tableView.reloadData()
            })
        }, onError: {(error) in
            self.isFetchingData = false
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
            })
        })
        subjectDisposable.disposed(by: disposableBag)
    }
    
}
