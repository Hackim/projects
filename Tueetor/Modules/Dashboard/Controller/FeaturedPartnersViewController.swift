//
//  FeaturedPartnersViewController.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 05/06/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit
import RxSwift

class FeaturedPartnersViewController: BaseViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    
    var partnerPagination = PartnerPagination.init()
    var disposableBag = DisposeBag()
    var partnerObserver: Observable<PartnerPagination>!
    var isFetchingData = false
    var isFirstTime = true
    var cellHeights: [Int: CGFloat] = [:]
    private let refreshControl = UIRefreshControl()

    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 10.0, *) {
            collectionView.refreshControl = refreshControl
        } else {
            collectionView.addSubview(refreshControl)
        }
        refreshControl.addTarget(self, action: #selector(refreshData(_:)), for: .valueChanged)
        fetchFeaturedPartners()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        (self.tabBarController as! BaseTabBarViewController).tabBar.isHidden = true
        (self.tabBarController as! BaseTabBarViewController).hideCenterButton()
    }
    
    @objc func refreshData(_ sender: Any) {
        partnerPagination.paginationType = .new
        fetchFeaturedPartners()
        if let refreshControl = sender as? UIRefreshControl {
            refreshControl.endRefreshing()
        }
    }
}

extension FeaturedPartnersViewController: UICollectionViewDelegateFlowLayout, UICollectionViewDelegate, UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return partnerPagination.partners.count + 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard indexPath.item != partnerPagination.partners.count else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: LoadMoreCollectionViewCell.cellIdentifier(), for: indexPath) as! LoadMoreCollectionViewCell
            cell.activityIndicator.startAnimating()
            return cell
        }
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: DashboardPartnerCollectionViewCell.cellIdentifier(), for: indexPath) as! DashboardPartnerCollectionViewCell
        cell.configureCellWithPartner(partnerPagination.partners[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        guard indexPath.row != partnerPagination.partners.count else {
            return partnerPagination.hasMoreToLoad ? CGSize(width: ScreenWidth - 56 , height: 50) : CGSize(width: ScreenWidth - 56 , height: 0)
        }
        return CGSize(width: (ScreenWidth/2) - 28, height: 170)
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if indexPath.row == partnerPagination.partners.count && self.partnerPagination.hasMoreToLoad && !isFetchingData {
            fetchFeaturedPartners()
        }
    }

//    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        guard indexPath.row != tutorPagination.tutors.count else {
//            return
//        }
//        let tutorProfile = TutorProfileViewController.instantiateFromAppStoryboard(appStoryboard: .Dashboard)
//        tutorProfile.tutor = self.tutorPagination.tutors[indexPath.item]
//        navigationController?.pushViewController(tutorProfile, animated: true)
//    }
}

extension FeaturedPartnersViewController {
    
    func fetchFeaturedPartners() {
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }
        
        if partnerPagination.paginationType != .old {
            self.partnerPagination.currentPageNumber = 1
            if isFirstTime == true {
                Utilities.showHUD(forView: self.view, excludeViews: [])
            } else {
                UIApplication.shared.isNetworkActivityIndicatorVisible = true
            }
            
        } else {
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
        }
        
        partnerObserver = ApiManager.shared.apiService.fetchFeaturedPartners(partnerPagination.currentPageNumber)
        let partnerDisposable = partnerObserver.subscribe(onNext: {(paginationObject) in
            self.isFetchingData = false
            
            self.partnerPagination.appendDataFromObject(paginationObject)
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if self.partnerPagination.paginationType != .old && self.isFirstTime == true {
                    self.isFirstTime = false
                } else {
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                }
                self.collectionView.reloadData()
            })
        }, onError: {(error) in
            self.isFetchingData = false
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
            })
        })
        partnerDisposable.disposed(by: disposableBag)
        
    }
    
}

