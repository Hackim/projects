//
//  PartnerProfileViewController.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 11/06/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit
import RxSwift
import SKPhotoBrowser

class PartnerProfileViewController: BaseViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var disposableBag = DisposeBag()
    var agencyDetailedInfo: AgencyDetailedInfo!
    var agencyID: String!
    var assetID: String!
    var currentSelectedSection: Int = 0
    var documentCellHeight: CGFloat = 300.0
    var selectedCourseIndex = -1
    
    private let refreshControl = UIRefreshControl()

    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 10.0, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.addSubview(refreshControl)
        }
        refreshControl.addTarget(self, action: #selector(refreshData(_:)), for: .valueChanged)
        fetchAgencyDetails()
        tableView.tableFooterView = UIView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        (self.tabBarController as! BaseTabBarViewController).tabBar.isHidden = true
        (self.tabBarController as! BaseTabBarViewController).hideCenterButton()
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }
    
    @objc func refreshData(_ sender: Any) {
        fetchAgencyDetails()
        if let refreshControl = sender as? UIRefreshControl {
            refreshControl.endRefreshing()
        }
    }


}

extension PartnerProfileViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let _ = agencyDetailedInfo {
            if currentSelectedSection == 3 {
                if agencyDetailedInfo.locations.count > 0 {
                    return 2 + agencyDetailedInfo.locations.count
                } else {
                    return 3
                }
            } else if currentSelectedSection == 1 {
                if agencyDetailedInfo.courses.count > 0 {
                    return 2 + agencyDetailedInfo.courses.count
                } else {
                    return 3
                }
            } else {
                return 3
            }
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: AgencyInfoTableViewCell.cellIdentifier(), for: indexPath) as! AgencyInfoTableViewCell
            cell.delegate = self
            cell.configureCellWithAgency(agencyDetailedInfo)
            cell.infoLabel.delegate = self
            return cell
        } else if indexPath.row == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: TutorMoreInfoTableViewCell.cellIdentifier(), for: indexPath) as! TutorMoreInfoTableViewCell
            cell.page = .agency
            cell.delegate = self
            cell.selectedIndex = currentSelectedSection
            cell.collectionView.reloadData()
            return cell
        } else {
            switch currentSelectedSection {
            case 0:
                if agencyDetailedInfo.about.isEmpty {
                    let cell = tableView.dequeueReusableCell(withIdentifier: EmptyMessageTableViewCell.cellIdentifier(), for: indexPath) as! EmptyMessageTableViewCell
                    cell.messageLabel.text = EmptyMessageText.about.description()
                    return cell
                } else {
                    let cell = tableView.dequeueReusableCell(withIdentifier: AgencyAboutTableViewCell.cellIdentifier(), for: indexPath) as! AgencyAboutTableViewCell
                        cell.textView.delegate = self
                    cell.textView.attributedText = agencyDetailedInfo.noteHTML
                    return cell
                }
            case 2:
                if agencyDetailedInfo.medias.count > 0 {
                    let cell = tableView.dequeueReusableCell(withIdentifier: DocumentListingTableViewCell.cellIdentifier(), for: indexPath) as! DocumentListingTableViewCell
                    cell.mediaDocuments = agencyDetailedInfo.medias
                    cell.isDocumentType = false
                    cell.configureCellForHeightListener()
                    cell.delegate = self
                    return cell
                } else {
                    let cell = tableView.dequeueReusableCell(withIdentifier: EmptyMessageTableViewCell.cellIdentifier(), for: indexPath) as! EmptyMessageTableViewCell
                    cell.messageLabel.text = EmptyMessageText.media.description()
                    return cell
                }
            case 3:
                if agencyDetailedInfo.locations.count > 0 {
                    let cell = tableView.dequeueReusableCell(withIdentifier: LocationTableViewCell.cellIdentifier(), for: indexPath) as! LocationTableViewCell
                    cell.configureCellWithLocation(agencyDetailedInfo.locations[indexPath.row - 2])
                    return cell
                } else {
                    let cell = tableView.dequeueReusableCell(withIdentifier: EmptyMessageTableViewCell.cellIdentifier(), for: indexPath) as! EmptyMessageTableViewCell
                    cell.messageLabel.text = EmptyMessageText.location.description()
                    return cell
                }
            default:
                if agencyDetailedInfo.courses.count > 0 {
                    let cell = tableView.dequeueReusableCell(withIdentifier: CoursesTableViewCell.cellIdentifier(), for: indexPath) as! CoursesTableViewCell
                    cell.configureCellWithCourse(agencyDetailedInfo.courses[indexPath.row - 2], tag: indexPath.row - 2)
                    cell.delegate = self
                    return cell
                } else {
                    let cell = tableView.dequeueReusableCell(withIdentifier: EmptyMessageTableViewCell.cellIdentifier(), for: indexPath) as! EmptyMessageTableViewCell
                    cell.messageLabel.text = EmptyMessageText.course.description()
                    return cell
                }
            }

        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return AgencyInfoTableViewCell.cellHeightForAgency(agencyDetailedInfo)
        } else if indexPath.row == 1 {
            return TutorMoreInfoTableViewCell.cellHeight()
        } else {
            switch currentSelectedSection {
            case 0:
                if agencyDetailedInfo.about.isEmpty {
                    return 44.0
                } else {
                    return AgencyAboutTableViewCell.cellHeightForAbout(self.agencyDetailedInfo)
                }
            case 2:
                if agencyDetailedInfo.medias.isEmpty {
                    return 44.0
                } else {
                    return documentCellHeight
                }
            case 3:
                if agencyDetailedInfo.locations.isEmpty {
                    return 44.0
                } else {
                    return LocationTableViewCell.cellHeightForLocation(agencyDetailedInfo.locations[indexPath.row - 2])
                }
            default:
                if agencyDetailedInfo.courses.isEmpty {
                    return 44.0
                } else {
                    return CoursesTableViewCell.cellHeightForCourse(agencyDetailedInfo.courses[indexPath.row - 2], isSelected: indexPath.row == selectedCourseIndex)
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return AgencyInfoTableViewCell.cellHeightForAgency(agencyDetailedInfo)
        } else if indexPath.row == 1 {
            return TutorMoreInfoTableViewCell.cellHeight()
        } else {
            switch currentSelectedSection {
            case 0:
                if agencyDetailedInfo.about.isEmpty {
                    return 44.0
                } else {
                    return AgencyAboutTableViewCell.cellHeightForAbout(self.agencyDetailedInfo)
                }
            case 2:
                if agencyDetailedInfo.medias.isEmpty {
                    return 44.0
                } else {
                    return documentCellHeight
                }
            case 3:
                if agencyDetailedInfo.locations.isEmpty {
                    return 44.0
                } else {
                    return LocationTableViewCell.cellHeightForLocation(agencyDetailedInfo.locations[indexPath.row - 2])
                }
            default:
                if agencyDetailedInfo.courses.isEmpty {
                    return 44.0
                } else {
                    return CoursesTableViewCell.cellHeightForCourse(agencyDetailedInfo.courses[indexPath.row - 2], isSelected: indexPath.row == selectedCourseIndex)
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row > 1, currentSelectedSection == 1, agencyDetailedInfo.courses.count > 0 {
            selectedCourseIndex = indexPath.row == selectedCourseIndex ? -1 : indexPath.row
            tableView.beginUpdates()
            tableView.endUpdates()
        } else if indexPath.row > 1, currentSelectedSection == 3, agencyDetailedInfo.locations.count > 0 {
            assetID = agencyDetailedInfo.locations[indexPath.row - 2].assetID
            fetchAgencyDetails(showCourses: true)
        }
    }

}

extension PartnerProfileViewController: TutorMoreInfoTableViewCellDelegate {
    
    func indexSelected(_ index: Int) {
        currentSelectedSection = index
        tableView.reloadData()
    }
}

extension PartnerProfileViewController: AgencyInfoTableViewCellDelegate {
    
    func enquireAgency() {
        let enquireVC = EnquireAgencyViewController(nibName: "EnquireAgencyViewController", bundle: nil)
        enquireVC.agencyID = self.agencyID
        enquireVC.assetID = self.assetID
        // Create the dialog
        let popup = PopupDialog(viewController: enquireVC,
                                buttonAlignment: .horizontal,
                                transitionStyle: .bounceDown,
                                tapGestureDismissal: true,
                                panGestureDismissal: true)
        
        let buttonOne = CancelButton(title: AlertButton.cancel.description(), height: 60) {
            //Do nothing
        }
        
        
        let buttonTwo = DefaultButton(title: AlertButton.submit.description(), height: 60) {
            self.enquireAgencyWithMessage(enquireVC.commentTextView.text ?? "", fullName: enquireVC.nameTextField.text ?? "", email: enquireVC.emailTextField.text ?? "", phone: enquireVC.phoneTextField.text ?? "", preferred: enquireVC.selectedOption, courseID: nil)
        }
        
        popup.addButtons([buttonOne, buttonTwo])
        present(popup, animated: true, completion: nil)
    }
    
    func reportAgency() {
        guard UserStore.shared.isLoggedIn else {
            showLoginAlert()
            return
        }

        let reportVC = ReportUserViewController(nibName: "ReportUserViewController", bundle: nil)
        
        // Create the dialog
        let popup = PopupDialog(viewController: reportVC,
                                buttonAlignment: .horizontal,
                                transitionStyle: .bounceDown,
                                tapGestureDismissal: true,
                                panGestureDismissal: true)
        
        let buttonOne = CancelButton(title: AlertButton.cancel.description(), height: 60) {
            //Do nothing
        }
        
        
        let buttonTwo = DefaultButton(title: AlertButton.submit.description(), height: 60) {
            self.reportUserWithComment(reportVC.commentsTextView.text ?? "", nature: reportVC.selectedOption)
        }
        
        popup.addButtons([buttonOne, buttonTwo])
        present(popup, animated: true, completion: nil)
    }
    
}
extension PartnerProfileViewController: DocumentListingTableViewCellDelegate {
    
    func updateHeightForTheTableView(_ newHeight: CGFloat) {
        documentCellHeight = newHeight
        self.tableView.reloadRows(at: [IndexPath.init(row: 2, section: 0)], with: .none)
    }
    
    func documentTappedAtIndex(_ index: Int) {
        
    }
    
    func mediaTappedAtIndex(_ index: Int) {
        let documentsPage = DocumentPageViewController.instantiateFromAppStoryboard(appStoryboard: .Profile)
        var names = [String]()
        var links = [String]()
        for media in agencyDetailedInfo.medias {
            names.append(media.desc)
            links.append(media.name)
        }
        documentsPage.titles = names
        documentsPage.links = links
        documentsPage.startIndex = index
        self.navigationController?.pushViewController(documentsPage, animated: true)
    }
    
    func showMediaAtIndex(_ index: Int, fromMedias medias: [Media]) {
        var images = [SKPhoto]()
        for media in medias {
            let photo = SKPhoto.photoWithImageURL(media.name)
            photo.caption = media.desc
            photo.shouldCachePhotoURLImage = true // you can use image cache by true(NSCache)
            images.append(photo)
        }
        
        // 2. create PhotoBrowser Instance, and present.
        let browser = SKPhotoBrowser(photos: images)
        browser.initializePageIndex(index)
        present(browser, animated: true, completion: {})
    }
}

extension PartnerProfileViewController: UITextViewDelegate {
    
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange) -> Bool {
        if URL.absoluteString.hasPrefix("tel:") {
            return true
        } else {
            pushToWebViewWithURL(URL, title: agencyDetailedInfo.nickName)
            return false
        }
    }
}

extension PartnerProfileViewController: CoursesTableViewCellDelegate {
    
    func showWebViewForCourseAtIndex(_ index: Int) {
        let course = agencyDetailedInfo.courses[index]
        if let url = URL(string: course.url), let title = course.name {
            pushToWebViewWithURL(url, title: title)
        }
    }
    
    func showDocumentsForCourseAtIndex(_ index: Int) {
        let documentVC = DocumentsViewController.instantiateFromAppStoryboard(appStoryboard: .Dashboard)
        documentVC.documents = agencyDetailedInfo.courses[index].documents
        self.present(documentVC, animated: true, completion: nil)
    }
    
    func showSchedulesForCourseAtIndex(_ index: Int) {
        let schedulesVC = AgencyScheduleViewController.instantiateFromAppStoryboard(appStoryboard: .Dashboard)
        schedulesVC.schedules = agencyDetailedInfo.courses[index].schedules
        self.present(schedulesVC, animated: true, completion: nil)
    }
    
    func showCourseEnquiryAtIndex(_ index: Int) {
        
        let enquireVC = EnquireAgencyViewController(nibName: "EnquireAgencyViewController", bundle: nil)
        let course = self.agencyDetailedInfo.courses[index]
        enquireVC.course = course
        enquireVC.agencyID = self.agencyID
        enquireVC.assetID = self.assetID

        // Create the dialog
        let popup = PopupDialog(viewController: enquireVC,
                                buttonAlignment: .horizontal,
                                transitionStyle: .bounceDown,
                                tapGestureDismissal: true,
                                panGestureDismissal: true)
        
        let buttonOne = CancelButton(title: AlertButton.cancel.description(), height: 60) {
            //Do nothing
        }
        
        
        let buttonTwo = DefaultButton(title: AlertButton.submit.description(), height: 60) {
            self.enquireAgencyWithMessage(enquireVC.commentTextView.text ?? "", fullName: enquireVC.nameTextField.text ?? "", email: enquireVC.emailTextField.text ?? "", phone: enquireVC.phoneTextField.text ?? "", preferred: enquireVC.selectedOption, courseID: enquireVC.course.id)
        }
        
        popup.addButtons([buttonOne, buttonTwo])
        present(popup, animated: true, completion: nil)
    }
}

extension PartnerProfileViewController {
    
    func fetchAgencyDetails(showCourses: Bool = false) {
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }
        
        Utilities.showHUD(forView: self.view, excludeViews: [])
        
        let agencyObserver = ApiManager.shared.apiService.fetchgAgencyDetailsForID(agencyID, andAssetID: assetID)
        let agencyDisposable = agencyObserver.subscribe(onNext: {(agencyDetails) in
            Utilities.hideHUD(forView: self.view)
            self.agencyDetailedInfo = agencyDetails
            DispatchQueue.main.async {
                if showCourses == true {
                    self.currentSelectedSection = 1
                    self.selectedCourseIndex = -1
                    self.tableView.scrollToRow(at: IndexPath.init(row: 0, section: 0), at: .top, animated: true)
                } else {
                    self.enquireAgency()
                }

                self.tableView.reloadData()
            }
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.showSimpleAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        })
        agencyDisposable.disposed(by: disposableBag)
        
    }
    
    func reportUserWithComment(_ comment: String, nature: Int) {
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }
        
        guard let userID = UserStore.shared.userID else {
            return
        }
        
        var nat = ""
        switch nature {
        case 1:
            nat = "Spam"
        case 2:
            nat = "No Reply"
        default:
            nat = "Improper Conduct"
        }
        
        Utilities.showHUD(forView: self.view, excludeViews: [])
        
        let reportUserObserver = ApiManager.shared.apiService.reportUserWithUserID(agencyID, comment: comment, nature: nat, userID: String(userID))
        let reportUserDisposable = reportUserObserver.subscribe(onNext: {(message) in
            Utilities.hideHUD(forView: self.view)
            
            DispatchQueue.main.async {
                self.showSimpleAlertWithMessage(message)
            }
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.showSimpleAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        })
        reportUserDisposable.disposed(by: disposableBag)
    }

    func enquireAgencyWithMessage(_ message: String, fullName: String, email: String, phone: String, preferred: Int, courseID: String?) {
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }
        
        var userID = ""
        
        if let userIDInt = UserStore.shared.userID {
            userID = String(userIDInt)
        }
        
        var preference = ""
        switch preferred {
        case 1:
            preference = "Email"
        default:
            preference = "Phone"
        }
        
        Utilities.showHUD(forView: self.view, excludeViews: [])
        
        let enquireObserver = ApiManager.shared.apiService.enquireAgencyWithID(agencyID, assetID: assetID, name: fullName, email: email, phone: phone, preferredContact: preference, message: message, userID: userID, courseID: courseID)
        let enquireDisposable = enquireObserver.subscribe(onNext: {(message) in
            Utilities.hideHUD(forView: self.view)
            
            DispatchQueue.main.async {
                self.showSimpleAlertWithMessage(message)
            }
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.showSimpleAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        })
        enquireDisposable.disposed(by: disposableBag)
    }

}

