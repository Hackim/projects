//
//  SubjectCategory.swift
//  Tueetor
//
//  Created by Phaninder on 14/05/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import Foundation
import Unbox

class SubjectCategory: Unboxable {
    
    var id: String!
    var name: String!
    var image: String!
    var count = 0
    var subjects = [Subject]()
    
    required init(unboxer: Unboxer) throws {
        self.id = unboxer.unbox(key: "category_id") ?? ""
        self.name = unboxer.unbox(key: "category_name") ?? ""
        self.image = unboxer.unbox(key: "category_image") ?? ""
        self.count = unboxer.unbox(key: "number_of_subjects") ?? 0
        var subjectsArray = [Subject]()
        if let subjectsObjectsArray = unboxer.dictionary["subjects"] as? [[String: AnyObject]],
            subjectsObjectsArray.count > 0 {
            for subjectDict in subjectsObjectsArray {
                do {
                    let subject: Subject = try unbox(dictionary: subjectDict)
                    subjectsArray.append(subject)
                } catch {
                    print("Couldn't parse Subject")
                }
            }
        }
        subjects = subjectsArray
    }
    
}
