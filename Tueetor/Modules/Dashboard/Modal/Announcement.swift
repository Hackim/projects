//
//  Announcement.swift
//  Tueetor
//
//  Created by Phaninder on 14/05/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import Foundation
import Unbox

class Announcement: Unboxable {
    
    var countryID: String!
    var title: String!
    var url: String!
    var image: String!
    var newsDescription: String!
    var order: String!
    var status: String!
    var imageURL: String!

    required init(unboxer: Unboxer) throws {
        self.countryID = unboxer.unbox(key: "country_id") ?? ""
        self.title = unboxer.unbox(key: "title") ?? ""
        self.url = unboxer.unbox(key: "url") ?? ""
        self.image = unboxer.unbox(key: "image") ?? ""
        self.newsDescription = unboxer.unbox(key: "description") ?? ""
        self.order = unboxer.unbox(key: "news_order") ?? ""
        self.status = unboxer.unbox(key: "status") ?? ""
        self.imageURL = self.image!

    }
    
}
