//
//  Partner.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 28/05/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import Foundation
import Unbox

class Partner: Unboxable {
    
    var ID: String!
    var name: String!
    var image: String!
    var assetId: String!
    var id_asset:String!
    required init(unboxer: Unboxer) throws {
        self.ID = unboxer.unbox(key: "id") ?? ""
        self.name = unboxer.unbox(key: "name") ?? ""
        self.image = unboxer.unbox(key: "image") ?? ""
        self.assetId = unboxer.unbox(keyPath: "id_asset") ?? "0"
        self.id_asset = unboxer.unbox(keyPath: "id_asset") ?? "0"

    }
    
}
