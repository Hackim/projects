//
//  PartnerPagination.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 12/06/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import Foundation
import Unbox

class PartnerPagination: Unboxable {
    
    var partners: [Partner] = []
    private let limit = 10
    var currentPageNumber = 1
    var paginationType: PaginationType = .new
    var hasMoreToLoad: Bool
    
    init() {
        partners = []
        currentPageNumber = 1
        paginationType = .new
        hasMoreToLoad = false
    }
    
    required init(unboxer: Unboxer) throws {
        if let dataDict = unboxer.dictionary["data"] as? [String: Any],
        let partnersArray =  dataDict["featured_partner"] as? [[String: AnyObject]] {
            for partnerDict in partnersArray {
                let partner: Partner = try unbox(dictionary: partnerDict)
                self.partners.append(partner)
            }
        }
        hasMoreToLoad = self.partners.count == limit
    }
    
    func appendDataFromObject(_ newPaginationObject: PartnerPagination) {
        switch self.paginationType {
        case .new, .reload:
            self.partners = [];
            self.partners = newPaginationObject.partners
            self.paginationType = .old
        case .old:
            self.partners += newPaginationObject.partners
            self.paginationType = .old
        }
        self.hasMoreToLoad = newPaginationObject.hasMoreToLoad
        self.currentPageNumber += 1
    }
    
}
