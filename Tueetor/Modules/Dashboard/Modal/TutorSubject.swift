//
//  TutorSubject.swift
//  Tueetor
//
//  Created by Phaninder on 14/05/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import Foundation
import Unbox

class TutorSubject: Unboxable {
    
    var name: String!
    var price: String!

    required init(unboxer: Unboxer) throws {
        self.name = unboxer.unbox(key: "name") ?? ""
        self.price = unboxer.unbox(key: "pricepersession") ?? ""
    }
    
}
