//
//  TutorPagination.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 28/05/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import Foundation
import Unbox

class TutorPagination: Unboxable {
    
    var tutors: [Tutor] = []
    private let limit = 10
    var currentPageNumber = 1
    var paginationType: PaginationType = .new
    var hasMoreToLoad: Bool
    
    init() {
        tutors = []
        currentPageNumber = 1
        paginationType = .new
        hasMoreToLoad = false
    }
    
    required init(unboxer: Unboxer) throws {
        if let tutorsArray = unboxer.dictionary["data"] as? [[String: Any]] {
            for tutorDict in tutorsArray {
                if let tutorUserDict = tutorDict["user"] as? [String: AnyObject] {
                    let tutor: Tutor = try unbox(dictionary: tutorUserDict)
                    self.tutors.append(tutor)
                }
            }
        }
        hasMoreToLoad = self.tutors.count == limit
    }
    
    func appendDataFromObject(_ newPaginationObject: TutorPagination) {
        switch self.paginationType {
        case .new, .reload:
            self.tutors = [];
            self.tutors = newPaginationObject.tutors
            self.paginationType = .old
        case .old:
            self.tutors += newPaginationObject.tutors
            self.paginationType = .old
        }
        self.hasMoreToLoad = newPaginationObject.hasMoreToLoad
        self.currentPageNumber += 1
    }
    
}
