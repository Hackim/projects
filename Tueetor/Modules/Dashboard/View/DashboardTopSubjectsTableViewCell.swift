//
//  DashboardTopSubjectsTableViewCell.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 21/08/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit

import SDWebImage

protocol DashboardTopSubjectsTableViewCellDelegate {
    func showAllCategoriesScreen()
    func showCategorySubjects(_ button: UIButton)
}

class DashboardTopSubjectsTableViewCell: UITableViewCell {

    @IBOutlet weak var subjectViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var category1ImageView: UIImageView!
    @IBOutlet weak var category1NameLabel: UILabel!
    @IBOutlet weak var category1SubjectsLabel: UILabel!
    @IBOutlet weak var category1MoreLabel: UILabel!
    
    @IBOutlet weak var category2ImageView: UIImageView!
    @IBOutlet weak var category2NameLabel: UILabel!
    @IBOutlet weak var category2SubjectsLabel: UILabel!
    @IBOutlet weak var category2MoreLabel: UILabel!

    @IBOutlet weak var category3ImageView: UIImageView!
    @IBOutlet weak var category3NameLabel: UILabel!
    @IBOutlet weak var category3SubjectsLabel: UILabel!
    @IBOutlet weak var category3MoreLabel: UILabel!

    @IBOutlet weak var category4ImageView: UIImageView!
    @IBOutlet weak var category4NameLabel: UILabel!
    @IBOutlet weak var category4SubjectsLabel: UILabel!
    @IBOutlet weak var category4MoreLabel: UILabel!

    @IBOutlet weak var showTopSubjectButton: UIButton!
    
    var delegate: DashboardTopSubjectsTableViewCellDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
        self.showTopSubjectButton.layer.shadowOffset = CGSize.init(width: 0, height: 5)
        self.showTopSubjectButton.layer.shadowRadius = 3
        self.showTopSubjectButton.layer.shadowColor = UIColor(red: 0.098, green: 0.6431, blue: 0.6941, alpha: 1).cgColor
        self.showTopSubjectButton.layer.shadowOpacity = 0.75
    }

    class func cellIdentifier() -> String {
        return "DashboardTopSubjectsTableViewCell"
    }
    
//    class func cellHeight(_ categories: [SubjectCategory]) -> CGFloat {
//        return (DashboardTopSubjectsTableViewCell.getMaxHeightForCategoryTiles(categories) * CGFloat(2)) + 193.5
//    }
    
    @IBAction func category1ButtonTapped(_ sender: UIButton) {
        delegate?.showCategorySubjects(sender)
    }
    
    @IBAction func category2ButtonTapped(_ sender: UIButton) {
        delegate?.showCategorySubjects(sender)
    }
    
    @IBAction func category3ButtonTapped(_ sender: UIButton) {
        delegate?.showCategorySubjects(sender)
    }
    
    @IBAction func category4ButtonTapped(_ sender: UIButton) {
        delegate?.showCategorySubjects(sender)
    }

    @IBAction func showTopSubjectsButtonTapped(_ sender: UIButton) {
        delegate?.showAllCategoriesScreen()
    }
    
    func configureCellWithSubjects(_ categories: [SubjectCategory]) {
        subjectViewHeightConstraint.constant = self.getMaxHeightForCategoryTiles(categories)
        
        let category1 = categories[0]
        category1NameLabel.text = category1.name
        category1SubjectsLabel.text = getSubjectsString(category1)
        category1ImageView.sd_setShowActivityIndicatorView(true)
        category1ImageView.sd_setIndicatorStyle(.gray)
        category1ImageView.sd_setImage(with: URL(string: category1.image), completed: nil)
        category1MoreLabel.isHidden = category1.subjects.count < 4
        
        let category2 = categories[1]
        category2NameLabel.text = category2.name
        category2SubjectsLabel.text = getSubjectsString(category2)
        category2ImageView.sd_setShowActivityIndicatorView(true)
        category2ImageView.sd_setIndicatorStyle(.gray)
        category2ImageView.sd_setImage(with: URL(string: category2.image), completed: nil)
        category2MoreLabel.isHidden = category2.subjects.count < 4
        
        let category3 = categories[2]
        category3NameLabel.text = category3.name
        category3SubjectsLabel.text = getSubjectsString(category3)
        category3ImageView.sd_setShowActivityIndicatorView(true)
        category3ImageView.sd_setIndicatorStyle(.gray)
        category3ImageView.sd_setImage(with: URL(string: category3.image), completed: nil)
        category3MoreLabel.isHidden = category3.subjects.count < 4
        
        let category4 = categories[3]
        category4NameLabel.text = category4.name
        category4SubjectsLabel.text = getSubjectsString(category4)
        category4ImageView.sd_setShowActivityIndicatorView(true)
        category4ImageView.sd_setIndicatorStyle(.gray)
        category4ImageView.sd_setImage(with: URL(string: category4.image), completed: nil)
        category4MoreLabel.isHidden = category4.subjects.count < 4
    }
    
    func getSubjectsString(_ category: SubjectCategory) -> String {
        var subsString = ""
        var i = 0
        for subject in category.subjects {
            guard i < 2 else {
                subsString += "\(subject.name ?? "")"
                return subsString
            }
            subsString += "\(subject.name ?? "")\n"
            i += 1
        }
        return subsString
    }
    
    func getMaxHeightForCategoryTiles(_ categories: [SubjectCategory]) -> CGFloat {
        var maxHeight: CGFloat = 185.0
        for i in 0..<4  {
            var nameHeight: CGFloat = 24.0
            let category = categories[i]
            switch i {
            case 0:
                category1NameLabel.text = category.name
                nameHeight = category1NameLabel.heightForText()
            case 1:
                category2NameLabel.text = category.name
                nameHeight = category2NameLabel.heightForText()
            case 2:
                category3NameLabel.text = category.name
                nameHeight = category3NameLabel.heightForText()
            case 3:
                category4NameLabel.text = category.name
                nameHeight = category4NameLabel.heightForText()
            default:
                break
            }
            let finalHeight = nameHeight + 147.5
            if finalHeight > maxHeight {
                maxHeight = finalHeight
            }
        }
        print(maxHeight)
        return maxHeight
    }

    
}
