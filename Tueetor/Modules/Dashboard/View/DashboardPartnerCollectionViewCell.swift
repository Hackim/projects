//
//  DashboardPartnerCollectionViewCell.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 28/05/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit
import SDWebImage

class DashboardPartnerCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var partnerImageView: UIImageView!
    @IBOutlet weak var partnerLabel: UILabel!
    @IBOutlet weak var partnerShadowView: UIView!
    
    class func cellIdentifier() -> String {
        return "DashboardPartnerCollectionViewCell"
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.partnerShadowView.layer.shadowOffset = CGSize(width: 0, height: 1)
        self.partnerShadowView.layer.shadowColor = UIColor.black.cgColor
        self.partnerShadowView.layer.shadowRadius = 4
        self.partnerShadowView.layer.shadowOpacity = 0.3
        self.partnerShadowView.layer.masksToBounds = false
        self.partnerShadowView.layer.rasterizationScale = UIScreen.main.scale
        self.partnerShadowView.clipsToBounds = false
        
        self.partnerImageView.layer.cornerRadius = 5
        self.partnerImageView.clipsToBounds = true
    }

    func configureCellWithPartner(_ partner: Partner) {
        partnerImageView.sd_setShowActivityIndicatorView(true)
        partnerImageView.sd_setIndicatorStyle(.gray)
        partnerImageView.sd_setImage(with: URL(string: partner.image), completed: nil)
        partnerLabel.text = partner.name
    }
    
}
