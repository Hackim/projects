//
//  DashboardBannerTableViewCell.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 28/05/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit

protocol DashboardBannerTableViewCellDelegate {
    func openURLInWebViewForBanner(_ banner: Banner)
    func navigateToAgencyProfileWithID(_ agencyID: String)
}

class DashboardBannerTableViewCell: UITableViewCell, UIScrollViewDelegate, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate, UICollectionViewDataSource {
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    var banners: [Banner]!
    let dummyCount = 3
    var timer: Timer!
    var delegate: DashboardBannerTableViewCellDelegate?
    
    class func cellIdentifier() -> String {
        return "DashboardBannerTableViewCell"
    }
    
    class func cellHeight() -> CGFloat {
        return 95.0
    }
    
    func reloadCollectionViewData() {
        self.collectionView.reloadData()
    }
    
    func configureCell() {
        if timer == nil {
            startTimer()
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.centerIfNeeded()
    }
    
    func centerIfNeeded() {
        let currentOffset = collectionView.contentOffset
        let contentWidth = self.totalContentWidth
        let width = contentWidth / CGFloat(dummyCount)
        
        if 0 > currentOffset.x {
            //left scrolling
            collectionView.contentOffset = CGPoint(x: width - currentOffset.x, y: currentOffset.y)
        } else if (currentOffset.x + cellWidth) > contentWidth {
            //right scrolling
            let difference = (currentOffset.x + cellWidth) - contentWidth
            collectionView.contentOffset = CGPoint(x: width - (cellWidth - difference), y: currentOffset.y)
        }
    }
    

    var totalContentWidth: CGFloat {
        return CGFloat(banners.count * dummyCount) * ScreenWidth
    }
    
    func startTimer() {
        if banners.count > 1 && timer == nil {
            timer = Timer.scheduledTimer(timeInterval: TimeInterval(Int(banners[0].slideTime) ?? 2), target: self, selector: #selector(rotate), userInfo: nil, repeats: true)
        }
    }
    
    func stopTimer() {
        timer?.invalidate()
        timer = nil
    }
    
    @objc func rotate() {
        let offset = CGPoint(x: collectionView.contentOffset.x + cellWidth, y: collectionView.contentOffset.y)
        collectionView.setContentOffset(offset, animated: true)
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.stopTimer()
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        self.startTimer()
    }

    var cellWidth: CGFloat {
        return ScreenWidth
    }

    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dummyCount * banners.count

    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: DashboardBannerCollectionViewCell.cellIdentifier(), for: indexPath) as! DashboardBannerCollectionViewCell
        let itemIndex = indexPath.item % self.banners.count

        cell.configureCellWithBanner(banners[itemIndex])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: ScreenWidth, height: 45)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let itemIndex = indexPath.item % self.banners.count
        let banner = banners[itemIndex]
        if !banner.agencyID.isEmpty, banner.agencyID != "0" {
            delegate?.navigateToAgencyProfileWithID(banner.agencyID)
        } else {
            delegate?.openURLInWebViewForBanner(banner)
        }
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(0, 0, 0, 0)
    }

}
