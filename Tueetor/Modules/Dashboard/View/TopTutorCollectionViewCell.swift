//
//  TopTutorCollectionViewCell.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 05/06/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit
import SwiftyStarRatingView
import SDWebImage

class TopTutorCollectionViewCell: UICollectionViewCell {
 
    @IBOutlet weak var holderView: UIView!
    @IBOutlet weak var tutorImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var favIconButton: UIButton!
    @IBOutlet weak var ratingView: SwiftyStarRatingView!
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var responseTimeLabel: UILabel!
    @IBOutlet weak var subjectLabel1: UILabel!
    @IBOutlet weak var subjectLabel2: UILabel!
    @IBOutlet weak var subjectLabel3: UILabel!
    @IBOutlet weak var subjectLabel4: UILabel!
    @IBOutlet weak var subjectsHolderViewHeightConstraint: NSLayoutConstraint!
    
    class func cellIdentifier() -> String {
        return "TopTutorCollectionViewCell"
    }
    
    func configureCellWithTutor(_ tutor: Tutor) {
        nameLabel.text = tutor.name
        ratingLabel.text = tutor.ratingCount
        if let n = NumberFormatter().number(from: tutor.averageRating) {
            
            ratingView.value = CGFloat(truncating: n)
        }
        subjectLabel1.isHidden = true
        subjectLabel2.isHidden = true
        subjectLabel3.isHidden = true
        subjectLabel4.isHidden = true
    
        tutorImageView.sd_setShowActivityIndicatorView(true)
        tutorImageView.sd_setIndicatorStyle(.gray)
        tutorImageView.sd_setImage(with: URL(string: tutor.imageURL), placeholderImage: UIImage(named: "profile_placeholder"), options: .retryFailed, completed: nil)
        let favImage = UIImage(named: tutor.isFavorite ? "heartActive" : "heartIconInactive")
        favIconButton.setImage(favImage, for: .normal)
        var heightOfSubjects: CGFloat = 0
        if let subject1 = tutor.subjects[safe: 0] {
            heightOfSubjects = 39.0
            subjectLabel1.text = subject1.name
            subjectLabel1.isHidden = false
        }
        
        if let subject2 = tutor.subjects[safe: 1] {
            subjectLabel2.text = subject2.name
            subjectLabel2.isHidden = false
        }

        if let subject3 = tutor.subjects[safe: 2] {
            heightOfSubjects += 39.0
            subjectLabel3.text = subject3.name
            subjectLabel3.isHidden = false
        }

        if let subject4 = tutor.subjects[safe: 3] {
            subjectLabel4.text = subject4.name
            subjectLabel4.isHidden = false
        }
        subjectsHolderViewHeightConstraint.constant = heightOfSubjects        
    }
    
    
}
