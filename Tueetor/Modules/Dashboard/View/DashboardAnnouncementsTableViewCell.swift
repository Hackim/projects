//
//  DashboardAnnouncementsTableViewCell.swift
//  Tueetor
//
//  Created by Phaninder on 14/05/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit

class DashboardAnnouncementsTableViewCell: UITableViewCell {

    @IBOutlet weak var collectionView: UICollectionView!
    var announcements: [Announcement]!
    
    class func cellIdentifier() -> String {
        return "DashboardAnnouncementsTableViewCell"
    }
    
    class func cellHeight() -> CGFloat {
        return 211.0
    }
    
    func reloadCollectionViewData() {
        self.collectionView.reloadData()
    }


}

extension DashboardAnnouncementsTableViewCell: UICollectionViewDelegateFlowLayout, UICollectionViewDelegate, UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return announcements.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: DashboardAnnouncementCollectionViewCell.cellIdentifier(), for: indexPath) as! DashboardAnnouncementCollectionViewCell
        cell.configureCellWithAnnouncement(announcements[indexPath.item])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 295, height: 130)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let userInfo: [String: String] = ["url": announcements[indexPath.item].url,
                                          "title": announcements[indexPath.item].title];
        NotificationCenter.default.post(name: .openURLInSafari, object: nil, userInfo:userInfo)
    }
    
}
