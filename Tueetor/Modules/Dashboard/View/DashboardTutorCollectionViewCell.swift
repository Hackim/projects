//
//  DashboardTutorCollectionViewCell.swift
//  Tueetor
//
//  Created by Phaninder on 14/05/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit
import SwiftyStarRatingView

class DashboardTutorCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var tutorImageView: UIImageView!
    @IBOutlet weak var tutorNameLabel: UILabel!
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var ratingView: SwiftyStarRatingView!
    
    @IBOutlet weak var favIcon: UIImageView!
    @IBOutlet weak var holderView: UIView!
    
    
    class func cellIdentifier() -> String {
        return "DashboardTutorCollectionViewCell"
    }
    
    func configureCellWithTutor(_ tutor: Tutor) {
        tutorNameLabel.text = tutor.name
        ratingLabel.text = tutor.ratingCount
        if let n = NumberFormatter().number(from: tutor.averageRating) {
            
            ratingView.value = CGFloat(truncating: n)
        }
        if !Utilities.isUserTypeStudent() {
            favIcon.isHidden = true
        }
        tutorImageView.sd_setShowActivityIndicatorView(true)
        tutorImageView.sd_setIndicatorStyle(.gray)
        tutorImageView.sd_setImage(with: URL(string: tutor.imageURL), placeholderImage: UIImage(named: "profile_placeholder"), options: .retryFailed, completed: nil)
        favIcon.image = UIImage(named: tutor.isFavorite ? "heart" : "heart_empty")

    }
}
