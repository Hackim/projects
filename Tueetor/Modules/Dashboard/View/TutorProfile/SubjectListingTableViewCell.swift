//
//  SubjectListingTableViewCell.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 17/06/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit

protocol SubjectListingTableViewCellDelegate {
    func showScheduleForSubjectAtIndex(_ index: Int)
    func showLocationForSubjectAtIndex(_ index: Int)
}
class SubjectListingTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var levelLabel: UILabel!
    @IBOutlet weak var qualificationLabel: UILabel!
    @IBOutlet weak var costLabel: UILabel!
    @IBOutlet weak var experienceLabel: UILabel!
    @IBOutlet weak var abilityToTravelLabel: UILabel!
    @IBOutlet weak var scheduleButton: UIButton!
    @IBOutlet weak var expansionIndicator: UIImageView!
    
    var delegate: SubjectListingTableViewCellDelegate?
    
    class func cellIdentifier() -> String {
        return "SubjectListingTableViewCell"
    }
 
    func configureCellWithSubject(_ subject: SubjectDetailedInfo, _ tag: Int, isStudent: Bool = false) {
        nameLabel.text = subject.name
        levelLabel.text = subject.level
        scheduleButton.tag = tag
        qualificationLabel.text = subject.qualification
        var costString = ""
        if let pricePerSession = subject.pricePerSession, !pricePerSession.isEmpty {
            costString = pricePerSession + "/session".localized
        } else {
            costString = "NA/session".localized
        }
        costString += "   "
        if let pricePerMonth = subject.pricePerMonth, !pricePerMonth.isEmpty {
            costString += pricePerMonth + "/month".localized
        } else {
            costString += "NA/month".localized
        }
        costLabel.text = costString
        if isStudent == false {
            if let teachingSince = subject.teachingSince, !teachingSince.isEmpty {
                experienceLabel.text = "Teaching since ".localized + teachingSince
            } else {
                experienceLabel.text = "Teaching since : N/A".localized
            }
        } else {
            if let experience = subject.experience, !experience.isEmpty {
                if experience == "0" {
                    experienceLabel.text = "No Experience Required".localized
                } else {
                    experienceLabel.text = "Required Experience ".localized + experience
                }
                
            } else {
                experienceLabel.text = "Required Experience : N/A".localized
            }
        }

        if let abilityToTravel = subject.abilityToTravel, !abilityToTravel.isEmpty {
            abilityToTravelLabel.text = "Can travel ".localized + abilityToTravel
        } else {
            abilityToTravelLabel.text = "Ability to travel: N/A".localized
        }

    }
    
    @IBAction func schedulesButtonTapped(_ sender: UIButton) {
        delegate?.showScheduleForSubjectAtIndex(sender.tag)
    }
    
    @IBAction func locationButtonTapped(_ sender: UIButton) {
        delegate?.showLocationForSubjectAtIndex(sender.tag)
    }
}
