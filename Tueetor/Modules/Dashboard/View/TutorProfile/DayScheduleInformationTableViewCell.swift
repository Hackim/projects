//
//  DayScheduleInformationTableViewCell.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 16/06/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit

class DayScheduleInformationTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var shadowView: UIView!
    
    class func cellIdentifier() -> String {
        return "DayScheduleInformationTableViewCell"
    }
    
    class func cellHeight() -> CGFloat {
        return 85.0
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.shadowView.layer.borderColor = themeBlueColor.cgColor
        self.shadowView.layer.borderWidth = 1.5
    }

    func configureCellWithInformation(_ data: [String]) {
        var displayString = ""
        if data.count == 1 {
            timeLabel.isHidden = true
            displayString = data[0]
        } else {
            timeLabel.isHidden = false
            let time = data[0]
            if time.count == 1 {
                timeLabel.text = "0\(time):00"
            } else {
                timeLabel.text = "\(time):00"
            }
            displayString = data[1]
        }
        
        let stringArray = displayString.components(separatedBy: "\n")
        if stringArray.count > 1 {
            let attributedString = NSMutableAttributedString()
            attributedString.append(NSAttributedString(string: stringArray[0],
                                                       attributes: [.font: UIFont(name: "Montserrat-SemiBold", size: 16.0)!,
                                                                    .foregroundColor : themeBlackColor]))
            attributedString.append(NSAttributedString(string: "\n" + stringArray[1],
                                                       attributes: [.font: UIFont(name: "Montserrat-Light", size: 14.0)!,
                                                                    .foregroundColor : themeBlackColor]))
            nameLabel.attributedText = attributedString

        } else {
            nameLabel.text  = displayString
        }
    }
    
}
