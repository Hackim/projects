//
//  CalendarHeaderTableViewCell.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 16/06/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit

protocol CalendarHeaderTableViewCellDelegate {
    func indexSelectedForCalendarHeader(_ index: Int)
}

class CalendarHeaderTableViewCell: UITableViewCell {
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    var selectedIndex = 0
    var titles = ["MON".localized, "TUE".localized, "WED".localized, "THU".localized, "FRI".localized, "SAT".localized, "SUN".localized]
    
    var delegate: CalendarHeaderTableViewCellDelegate?
    
    class func cellIdentifier() -> String {
        return "CalendarHeaderTableViewCell"
    }
    
    class func sectionHeight() -> CGFloat {
        return 76.0
    }
    
    func reloadTheCalendarView() {
        self.collectionView.reloadData()
    }
    
}

extension CalendarHeaderTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return titles.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: WeekDaySelectionCollectionViewCell.cellIdentifier(), for: indexPath) as! WeekDaySelectionCollectionViewCell
        if selectedIndex == indexPath.item {
            cell.bgImageView.isHidden = false
            cell.titleLabel.textColor = UIColor.white
        } else {
            cell.bgImageView.isHidden = true
            cell.titleLabel.textColor = themeBlackColor
        }
        cell.bgImageView.isHidden = selectedIndex != indexPath.item
        let title = titles[indexPath.item]
        cell.titleLabel.text = title
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (ScreenWidth-40)/7, height: 62)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedIndex = indexPath.item
        delegate?.indexSelectedForCalendarHeader(selectedIndex)
        collectionView.scrollToItem(at: indexPath, at: .left, animated: true)
        collectionView.reloadData()
    }
    
}
