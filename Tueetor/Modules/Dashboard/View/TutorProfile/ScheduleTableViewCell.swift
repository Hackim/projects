//
//  ScheduleTableViewCell.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 11/06/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit
import SpreadsheetView

protocol ScheduleTableViewCellDelegate {
    func showSubjectsPopup(_ subjects: String, subView: UIView)
    func showSubjectListing(_ subjects: String, title: String)
}
class ScheduleTableViewCell: UITableViewCell, SpreadsheetViewDataSource, SpreadsheetViewDelegate {
    
    @IBOutlet weak var spreadSheet: SpreadsheetView!
    
    var headers = ["".localized, "MON".localized, "TUE".localized, "WED".localized, "THU".localized, "FRI".localized, "SAT".localized, "SUN".localized]
    var data = [[String]]()
    var delegate: ScheduleTableViewCellDelegate?
    
    class func cellIdentifier() -> String {
        return "ScheduleTableViewCell"
    }
    
    class func cellHeight() -> CGFloat {
        return 30.0 + 24 * 27.0
    }

    
    func configureCellWithSchedules(_ schedules: [String : [Schedule]]) {
        
        data = getDataArrayFromSchedules(schedules)
        spreadSheet.delegate = self
        spreadSheet.dataSource = self
        spreadSheet.isScrollEnabled = false
        spreadSheet.register(HeaderCell.self, forCellWithReuseIdentifier: String(describing: HeaderCell.self))
        spreadSheet.register(TextCell.self, forCellWithReuseIdentifier: String(describing: TextCell.self))
        spreadSheet.register(ScheduleCell.self, forCellWithReuseIdentifier: String(describing: ScheduleCell.self))
    }
    
    func getDataArrayFromSchedules(_ schedules: [String: [Schedule]]) -> [[String]] {
        var completeData = [[String]]()
        let weeksArray = ["MON", "TUE", "WED", "THU", "FRI", "SAT", "SUN"]
        for i in 0...23 {
            var hourSchedule = [String]()
            if i > 9 {
                hourSchedule = ["\(i):00"]
            } else {
                hourSchedule = ["0\(i):00"]
            }
            for day in weeksArray {
                let daysSchedule = schedules[day]
                let filteredSchedules = daysSchedule?.filter({ (schedule) -> Bool in
                    return schedule.hours == String(i)
                })
                if let schedulesArray = filteredSchedules, schedulesArray.count > 0 {
                    var nameString = ""
                    
                    for filteredSchedule in schedulesArray {
//                        nameString += filteredSchedule.subject.name + " - " + filteredSchedule.subject.level + ",\n"
                        nameString += filteredSchedule.subject.name + ","

                    }
                    nameString.removeLast()
                    hourSchedule.append(nameString)
                } else {
                    hourSchedule.append("")
                }
            }
            completeData.append(hourSchedule)
        }
        return completeData
    }
    
    // MARK: DataSource
    
    func numberOfColumns(in spreadsheetView: SpreadsheetView) -> Int {
        return headers.count
    }
    
    func numberOfRows(in spreadsheetView: SpreadsheetView) -> Int {
        return 1 + data.count
    }
    
    func spreadsheetView(_ spreadsheetView: SpreadsheetView, widthForColumn column: Int) -> CGFloat {
        if column == 0 {
            return 50
        } else {
            return (ScreenWidth - 60)/7
        }
    }
    
    func spreadsheetView(_ spreadsheetView: SpreadsheetView, heightForRow row: Int) -> CGFloat {
        if case 0 = row {
            return 30
        } else {
            return 26
        }
    }
    
    func frozenRows(in spreadsheetView: SpreadsheetView) -> Int {
        return 25
    }
    
    func spreadsheetView(_ spreadsheetView: SpreadsheetView, cellForItemAt indexPath: IndexPath) -> Cell? {
        if case 0 = indexPath.row {
            let cell = spreadSheet.dequeueReusableCell(withReuseIdentifier: String(describing: HeaderCell.self), for: indexPath) as! HeaderCell
            if indexPath.column == 0 {
                cell.contentView.backgroundColor = UIColor.clear
                cell.gridlines.left = .none
                cell.gridlines.top = .none
            } else {
                cell.contentView.backgroundColor = themeLightGrayColor
                cell.gridlines.left = .solid(width: 1, color: themeLightGrayColor)
                cell.gridlines.top = .solid(width: 1, color: themeLightGrayColor)
            }
            
            cell.gridlines.right = .solid(width: 1, color: themeLightGrayColor)
            cell.gridlines.bottom = .none
            cell.label.text = headers[indexPath.column]
            
            return cell
        } else {
            let subjectName = data[indexPath.row - 1][indexPath.column]

            if indexPath.column == 0 {
                let cell = spreadSheet.dequeueReusableCell(withReuseIdentifier: String(describing: TextCell.self), for: indexPath) as! TextCell
                cell.label.text = subjectName
                cell.gridlines.bottom = .solid(width: 1, color: themeLightGrayColor)
                cell.gridlines.left = .none
                cell.gridlines.right = .none
                cell.gridlines.top = .none

                cell.label.backgroundColor = UIColor.clear
                return cell
            } else {
                let cell = spreadSheet.dequeueReusableCell(withReuseIdentifier: String(describing: ScheduleCell.self), for: indexPath) as! ScheduleCell
                if let colorView = cell.colorView {
                    colorView.backgroundColor = subjectName.isEmpty ? UIColor.clear : themeBlueColor
                } else {
                    let colorView = UIView.init(frame: CGRect.init(x: 4, y: 6, width: (ScreenWidth - 60)/7 - 8, height: 14))
                    colorView.backgroundColor = subjectName.isEmpty ? UIColor.clear : themeBlueColor
                    cell.contentView.addSubview(colorView)
                    cell.colorView = colorView
                }
                cell.gridlines.bottom = .solid(width: 1, color: themeLightGrayColor)
                cell.gridlines.left = .none
                cell.gridlines.right = .none
                cell.gridlines.top = .none
                return cell
            }
        }
    }
    
    func spreadsheetView(_ spreadsheetView: SpreadsheetView, didSelectItemAt indexPath: IndexPath) {
        guard indexPath.column != 0 else {
            return
        }
        guard indexPath.row != 0 else {
            return
        }
        let subjectName = data[indexPath.row - 1][indexPath.column]
        guard !subjectName.isEmpty else {
            return
        }
        if let cell = spreadSheet.cellForItem(at: indexPath) as? ScheduleCell {
            delegate?.showSubjectsPopup(subjectName, subView: cell.colorView!)
            var day = "MON"
            switch indexPath.column {
            case 1:
                day = "Monday".localized
            case 2:
                day = "Tuesday".localized
            case 3:
                day = "Wednesday".localized
            case 4:
                day = "Thursday".localized
            case 5:
                day = "Friday".localized
            case 6:
                day = "Saturday".localized
            case 7:
                day = "Sunday".localized
            default:
                break
            }
            var time = "0:00 AM"
            if indexPath.row == 1 {
                time = "12:00 AM"
            } else if indexPath.row < 13 {
                time = "\(indexPath.row - 1):00 AM"
            } else if indexPath.row == 13 {
                time = "12:00 PM"
            } else {
                time = "\(indexPath.row - 13):00 PM"
            }
            delegate?.showSubjectListing(subjectName, title: "\(day)\n\(time)")
        }
    }
    
}
