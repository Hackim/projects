//
//  TutorInfoTableViewCell.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 30/05/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit
import SwiftyStarRatingView
import SDWebImage

protocol TutorInfoTableViewCellDelegate {
    func shortlistUserButtonTapped()
    func messageUserButtonTapped()
    func reportUserButtonTapped()
}

class TutorInfoTableViewCell: UITableViewCell {

    @IBOutlet weak var tutorImageView: UIImageView!
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var durationLabel: UILabel!
    @IBOutlet weak var reviewsLabel: UILabel!
    @IBOutlet weak var ratingView: SwiftyStarRatingView!
    @IBOutlet weak var messageView: UIView!
    
    @IBOutlet weak var shortlistView: UIView!
    @IBOutlet weak var shortlistLabel: UILabel!
    
    @IBOutlet weak var reportView: UIView!
    @IBOutlet weak var anotherReportView: UIView!
    
    @IBOutlet weak var viewWithShortlist: UIView!
    @IBOutlet weak var viewWithOutShortlist: UIView!

    var shortlistTapGesture: UITapGestureRecognizer!
    var reportTapGesture: UITapGestureRecognizer!
    var messageTapGesture: UITapGestureRecognizer!
    var anotherReportTapGesture: UITapGestureRecognizer!

    var delegate: TutorInfoTableViewCellDelegate?
    
    class func cellIdentifier() -> String {
        return "TutorInfoTableViewCell"
    }

    class func cellHeight() -> CGFloat {
        return 261.0
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        shortlistTapGesture = UITapGestureRecognizer(target: self, action: #selector(shortlistLabelTapped))
        shortlistView.addGestureRecognizer(shortlistTapGesture)
        
        reportTapGesture = UITapGestureRecognizer(target: self, action: #selector(reportLabelTapped))
        reportView.addGestureRecognizer(reportTapGesture)
        anotherReportTapGesture = UITapGestureRecognizer(target: self, action: #selector(reportLabelTapped))
        anotherReportView.addGestureRecognizer(anotherReportTapGesture)
        
        messageTapGesture = UITapGestureRecognizer(target: self, action: #selector(messageLabelTapped))
        messageView.addGestureRecognizer(messageTapGesture)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        if Utilities.isUserTypeStudent() {
            viewWithShortlist.isHidden = false
            viewWithOutShortlist.isHidden = true
            messageView.layer.shadowOffset = CGSize.init(width: 0, height: 5)
            messageView.layer.shadowRadius = 3
            messageView.layer.shadowColor = themeBlackColor.cgColor
            messageView.layer.shadowOpacity = 0.4
            
            shortlistView.layer.shadowOffset = CGSize.init(width: 0, height: 5)
            shortlistView.layer.shadowRadius = 3
            shortlistView.layer.shadowColor = themeBlackColor.cgColor
            shortlistView.layer.shadowOpacity = 0.4
            shortlistView.isHidden = !Utilities.isUserTypeStudent()
            
            reportView.layer.shadowOffset = CGSize.init(width: 0, height: 5)
            reportView.layer.shadowRadius = 3
            reportView.layer.shadowColor = themeBlackColor.cgColor
            reportView.layer.shadowOpacity = 0.4
        } else {
            viewWithShortlist.isHidden = true
            viewWithOutShortlist.isHidden = false
            
            anotherReportView.layer.shadowOffset = CGSize.init(width: 0, height: 5)
            anotherReportView.layer.shadowRadius = 3
            anotherReportView.layer.shadowColor = themeBlackColor.cgColor
            anotherReportView.layer.shadowOpacity = 0.4

        }

    }
    
    @objc func shortlistLabelTapped(sender: UITapGestureRecognizer) {
        delegate?.shortlistUserButtonTapped()
    }
    
    @objc func reportLabelTapped(sender: UITapGestureRecognizer) {
        delegate?.reportUserButtonTapped()
    }

    @objc func messageLabelTapped(sender: UITapGestureRecognizer) {
        delegate?.messageUserButtonTapped()
    }

    func configureCellWithTutor(_ tutorInfo: TutorDetailedInfo) {
        let tutor = tutorInfo.tutor!
        tutorImageView.sd_setShowActivityIndicatorView(true)
        tutorImageView.sd_setIndicatorStyle(.gray)
        tutorImageView.sd_setImage(with: URL(string: tutor.imageURL), placeholderImage: UIImage(named: "profile_placeholder"), options: .retryFailed) { (image, error, cacheType, url) in
            if error == nil {
                //Do nothing
            }
        }
        
        let attributedString = NSMutableAttributedString()
        attributedString.append(NSAttributedString(string: tutor.name,
                                                   attributes: [.font: UIFont(name: "Montserrat-SemiBold", size: 25.0)!,
                                                                .foregroundColor : themeBlackColor]))
        attributedString.append(NSAttributedString(string: "\n\(tutor.gender!)",
                                                   attributes: [.font: UIFont(name: "Montserrat-Light", size: 16.0)!,
                                                                .foregroundColor : themeBlackColor]))
        infoLabel.attributedText = attributedString

        if tutor.ratingCount == "1" {
            reviewsLabel.text = "\(tutor.ratingCount!) Review"
        } else {
            reviewsLabel.text = "\(tutor.ratingCount!) Reviews"
        }
        
        if let n = NumberFormatter().number(from: tutor.averageRating) {
            ratingView.value = CGFloat(truncating: n)
        }
        
        shortlistLabel.text = tutor.isFavorite ? "SHORTLISTED".localized : "SHORTLIST".localized
//        shortlistLabel.isUserInteractionEnabled = !tutor.isFavorite
        
        durationLabel.text = tutorInfo.responseTime
        
    }
}
