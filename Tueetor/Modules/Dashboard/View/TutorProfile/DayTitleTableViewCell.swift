
//
//  DayTitleTableViewCell.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 16/06/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit

class DayTitleTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    
    class func cellIdentifier() -> String {
        return "DayTitleTableViewCell"
    }
    
    class func cellHeight() -> CGFloat {
        return 96.0
    }
}
