//
//  EmptyMessageTableViewCell.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 11/06/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit

class EmptyMessageTableViewCell: UITableViewCell {

    @IBOutlet weak var messageLabel: UILabel!
    
    class func cellIdentifier() -> String {
        return "EmptyMessageTableViewCell"
    }

}
