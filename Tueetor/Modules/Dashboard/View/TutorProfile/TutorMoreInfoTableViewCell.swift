//
//  TutorMoreInfoTableViewCell.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 30/05/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit

protocol TutorMoreInfoTableViewCellDelegate {
    func indexSelected(_ index: Int)
}

class TutorMoreInfoTableViewCell: UITableViewCell {
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    var selectedIndex = 0
    var titlesForTutor = ["ABOUT".localized, "DOCUMENTS".localized, "SUBJECTS".localized, "SCHEDULE".localized, "REVIEW".localized]
    var titlesForAgency = ["ABOUT".localized, "COURSES".localized, "MEDIA".localized, "LOCATIONS".localized]
    var titlesForStudent = ["ABOUT".localized, "SUBJECTS".localized, "SCHEDULE".localized]

    var page: ProfilePageType = .tutor
    
    var delegate: TutorMoreInfoTableViewCellDelegate?
    
    class func cellIdentifier() -> String {
        return "TutorMoreInfoTableViewCell"
    }
    
    class func cellHeight() -> CGFloat {
        return 61.0
    }
    
    
}

extension TutorMoreInfoTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch page {
        case .tutor:
            return titlesForTutor.count
        case .student:
            return titlesForStudent.count
        case .agency:
            return titlesForAgency.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: TutorMoreInfoCollectionViewCell.cellIdentifier(), for: indexPath) as! TutorMoreInfoCollectionViewCell
        if selectedIndex == indexPath.item {
            cell.bgImageView.isHidden = false
            cell.titleLabel.textColor = UIColor.white
        } else {
            cell.bgImageView.isHidden = true
            cell.titleLabel.textColor = themeBlackColor
        }
        cell.bgImageView.isHidden = selectedIndex != indexPath.item
        var title = ""
        switch page {
        case .tutor:
            title = titlesForTutor[indexPath.item]
        case .student:
            title = titlesForStudent[indexPath.item]
        case .agency:
            title = titlesForAgency[indexPath.item]
        }

        cell.titleLabel.text = title
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 111, height: 62)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedIndex = indexPath.item
        delegate?.indexSelected(selectedIndex)
        collectionView.scrollToItem(at: indexPath, at: .left, animated: true)
        collectionView.reloadData()
    }
    
}

