//
//  TutorAboutTableViewCell.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 30/05/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit

class TutorAboutTableViewCell: UITableViewCell {

    @IBOutlet weak var aboutLabel: UILabel!
    
    class func cellIdentifier() -> String {
        return "TutorAboutTableViewCell"
    }
    
    func configureCellWithTutor(_ tutorDetail: TutorDetailedInfo) {
        aboutLabel.text = tutorDetail.about
     }
    
    func configureCellWithAgency(_ agency: AgencyDetailedInfo) {
        aboutLabel.attributedText = agency.noteHTML
    }
    
    func configureCellWithStudent(_ student: StudentDetailedInfo) {
        aboutLabel.text = student.about
    }


}
