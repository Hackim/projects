//
//  ReviewTableViewCell.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 11/06/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit
import SwiftyStarRatingView
import SDWebImage

class ReviewTableViewCell: UITableViewCell {
    
    @IBOutlet weak var ratingView: SwiftyStarRatingView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var contentLabel: UILabel!
    
    class func cellIdentifier() -> String {
        return "ReviewTableViewCell"
    }

    func configureCellWithRating(_ rating: Rating) {
        if let n = NumberFormatter().number(from: rating.rate) {
            ratingView.value = CGFloat(truncating: n)
        }
        ratingView.isUserInteractionEnabled = false
        titleLabel.text = rating.subject.uppercased()
        contentLabel.text = rating.comment
    }
}
