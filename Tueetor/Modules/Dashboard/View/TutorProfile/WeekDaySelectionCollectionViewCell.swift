//
//  WeekDaySelectionCollectionViewCell.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 16/06/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit

class WeekDaySelectionCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var bgImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!

    class func cellIdentifier() -> String {
        return "WeekDaySelectionCollectionViewCell"
    }
    
}
