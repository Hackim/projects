//
//  TutorMoreInfoCollectionViewCell.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 30/05/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit

class TutorMoreInfoCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var bgImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
    class func cellIdentifier() -> String {
        return "TutorMoreInfoCollectionViewCell"
    }

}
