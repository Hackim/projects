//
//  DocumentItemCollectionViewCell.swift
//  Tueetor1
//
//  Created by Phaninder Kumar on 11/06/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit

class DocumentItemCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var mediaImageView: UIImageView!
    @IBOutlet weak var playIconImageView: UIImageView!
    
    class func cellIdentifier() -> String {
        return "DocumentItemCollectionViewCell"
    }
    
    func configureCellWithDocument(_ document: Document) {
        playIconImageView.isHidden = !videoExtensions.contains(document.type)

        if document.type == "pdf" {
            mediaImageView.image = UIImage.init(named: "pdf")
        } else if imageExtensions.contains(document.type)
            || videoExtensions.contains(document.type) {
            mediaImageView.sd_setImage(with: URL(string: document.thumbNail.isEmpty ? document.name : document.thumbNail), placeholderImage: UIImage(named: ""), options: .retryFailed) { (image, error, cacheType, url) in
                if error == nil {
                    //Do nothing
                }
            }
        } else if document.type == "doc"
            || document.type == "msword"
            || document.type == "ms-office" {
            mediaImageView.image = UIImage.init(named: "doc")
        } else if document.type == "xls" {
            mediaImageView.image = UIImage.init(named: "xls")
        } else {
            mediaImageView.image = UIImage.init(named: "txt")
        }

        nameLabel.text = document.name
        mediaImageView.layer.cornerRadius = 5.0
        mediaImageView.clipsToBounds = true
        mediaImageView.contentMode = .scaleAspectFill

    }
    
    func configureCellWithMedia(_ media: Media) {
        nameLabel.text = media.desc
        mediaImageView.sd_setShowActivityIndicatorView(true)
        mediaImageView.sd_setIndicatorStyle(.gray)
        playIconImageView.isHidden = !videoExtensions.contains(media.type)

        if media.type == "pdf" {
            mediaImageView.image = UIImage.init(named: "pdf")
        } else if imageExtensions.contains(media.type)
            || videoExtensions.contains(media.type) {
            mediaImageView.sd_setImage(with: URL(string: media.thumbNail.isEmpty ? media.name : media.thumbNail), placeholderImage: UIImage(named: ""), options: .retryFailed) { (image, error, cacheType, url) in
                if error == nil {
                    //Do nothing
                }
            }
        } else if media.type == "doc"
            || media.type == "msword"
            || media.type == "ms-office" {
            mediaImageView.image = UIImage.init(named: "doc")
        } else if media.type == "xls" {
            mediaImageView.image = UIImage.init(named: "xls")
        } else {
            mediaImageView.image = UIImage.init(named: "txt")
        }
        mediaImageView.layer.cornerRadius = 5.0
        mediaImageView.clipsToBounds = true
        mediaImageView.contentMode = .scaleAspectFill
    }
        

}
