//
//  DocumentListingTableViewCell.swift
//  Tueetor1
//
//  Created by Phaninder Kumar on 11/06/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit

protocol DocumentListingTableViewCellDelegate {
    func updateHeightForTheTableView(_ newHeight: CGFloat)
    func documentTappedAtIndex(_ index: Int)
    func mediaTappedAtIndex(_ index: Int)
}

class DocumentListingTableViewCell: UITableViewCell {

    @IBOutlet weak var collectionView: UICollectionView!
    
    var documents: [Document]!
    var isDocumentType = true
    var mediaDocuments: [Media]!
    var isDelegateSet = false
    var delegate: DocumentListingTableViewCellDelegate?
    
    class func cellIdentifier() -> String {
        return "DocumentListingTableViewCell"
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configureCellForHeightListener() {
        if isDelegateSet == false {
            self.collectionView.addObserver(self, forKeyPath: "contentSize", options: .old, context: nil)
            isDelegateSet = true
        }
    }
    
    func removeObserverFromCell() {
        self.collectionView.removeObserver(self, forKeyPath: "contentSize", context: nil)
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        let newHeight = self.collectionView.collectionViewLayout.collectionViewContentSize.height
        delegate?.updateHeightForTheTableView(newHeight)
    }
    
}

extension DocumentListingTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let _ = documents {
            return documents.count
        } else if let _ = mediaDocuments {
            return mediaDocuments.count
        } else {
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if isDocumentType {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: DocumentItemCollectionViewCell.cellIdentifier(), for: indexPath) as! DocumentItemCollectionViewCell
            cell.configureCellWithDocument(documents[indexPath.item])
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: DocumentItemCollectionViewCell.cellIdentifier(), for: indexPath) as! DocumentItemCollectionViewCell
            cell.configureCellWithMedia(mediaDocuments[indexPath.item])
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if isDocumentType {
            return CGSize(width: 91, height: 132)
        } else {
            return CGSize(width: 91, height: 146)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if isDocumentType {
            delegate?.documentTappedAtIndex(indexPath.item)
        } else {
            delegate?.mediaTappedAtIndex(indexPath.item)
        }
    }
    
}
