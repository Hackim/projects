//
//  DashboardPartnersTableViewCell.swift
//  Tueetor
//
//  Created by Phaninder on 14/05/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit
import SDWebImage

protocol DashboardPartnersTableViewCellDelegate {
    func showFeaturedAgents()
    func partnerSelected(_ partner: Partner)
}

class DashboardPartnersTableViewCell: UITableViewCell {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var showMoreButton: UIButton!
    
    var partners: [Partner]!
    var delegate: DashboardPartnersTableViewCellDelegate?

    class func cellIdentifier() -> String {
        return "DashboardPartnersTableViewCell"
    }
    
    class func cellHeight() -> CGFloat {
        return 350.0
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.showMoreButton.layer.shadowOffset = CGSize.init(width: 0, height: 5)
        self.showMoreButton.layer.shadowRadius = 3
        self.showMoreButton.layer.shadowColor = UIColor(red: 0.098, green: 0.6431, blue: 0.6941, alpha: 1).cgColor
        self.showMoreButton.layer.shadowOpacity = 0.75
    }

    func reloadCollectionViewData() {
        self.collectionView.reloadData()
    }

    @IBAction func showMoreButtonTapped(_ sender: UIButton) {
        delegate?.showFeaturedAgents()
    }
    
}

extension DashboardPartnersTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return partners.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: DashboardPartnerCollectionViewCell.cellIdentifier(), for: indexPath) as! DashboardPartnerCollectionViewCell
        cell.configureCellWithPartner(partners[indexPath.row])
        print(partners[indexPath.row].assetId)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 130, height: 170)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        delegate?.partnerSelected(partners[indexPath.item])
    }
    
}


