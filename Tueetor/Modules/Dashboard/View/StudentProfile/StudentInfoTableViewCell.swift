//
//  StudentInfoTableViewCell.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 09/07/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit
import SwiftyStarRatingView
import SDWebImage

protocol StudentInfoTableViewCellDelegate {
    func shortlistStudentButtonTapped()
    func messageStudentButtonTapped()
    func reportStudentButtonTapped()
}

class StudentInfoTableViewCell: UITableViewCell {
    
    @IBOutlet weak var studentImageView: UIImageView!
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var messageView: UIView!
    
    @IBOutlet weak var shortlistView: UIView!
    @IBOutlet weak var shortlistLabel: UILabel!
    
    @IBOutlet weak var reportView: UIView!
    @IBOutlet weak var anotherReportView: UIView!
    
    @IBOutlet weak var viewWithShortlist: UIView!
    @IBOutlet weak var viewWithOutShortlist: UIView!
    
    var shortlistTapGesture: UITapGestureRecognizer!
    var reportTapGesture: UITapGestureRecognizer!
    var messageTapGesture: UITapGestureRecognizer!
    
    var delegate: StudentInfoTableViewCellDelegate?
    
    class func cellIdentifier() -> String {
        return "StudentInfoTableViewCell"
    }
    
    class func cellHeight() -> CGFloat {
        return 209.0
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        shortlistTapGesture = UITapGestureRecognizer(target: self, action: #selector(shortlistLabelTapped))
        shortlistView.addGestureRecognizer(shortlistTapGesture)
        
        reportTapGesture = UITapGestureRecognizer(target: self, action: #selector(reportLabelTapped))
        reportView.addGestureRecognizer(reportTapGesture)
        anotherReportView.addGestureRecognizer(reportTapGesture)
        
        messageTapGesture = UITapGestureRecognizer(target: self, action: #selector(messageLabelTapped))
        messageView.addGestureRecognizer(messageTapGesture)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        if !Utilities.isUserTypeStudent() {
            viewWithShortlist.isHidden = false
            viewWithOutShortlist.isHidden = true
            messageView.layer.shadowOffset = CGSize.init(width: 0, height: 5)
            messageView.layer.shadowRadius = 3
            messageView.layer.shadowColor = themeBlackColor.cgColor
            messageView.layer.shadowOpacity = 0.4
            
            shortlistView.layer.shadowOffset = CGSize.init(width: 0, height: 5)
            shortlistView.layer.shadowRadius = 3
            shortlistView.layer.shadowColor = themeBlackColor.cgColor
            shortlistView.layer.shadowOpacity = 0.4
            shortlistView.isHidden = Utilities.isUserTypeStudent()
            
            reportView.layer.shadowOffset = CGSize.init(width: 0, height: 5)
            reportView.layer.shadowRadius = 3
            reportView.layer.shadowColor = themeBlackColor.cgColor
            reportView.layer.shadowOpacity = 0.4
        } else {
            viewWithShortlist.isHidden = true
            viewWithOutShortlist.isHidden = false
            
            anotherReportView.layer.shadowOffset = CGSize.init(width: 0, height: 5)
            anotherReportView.layer.shadowRadius = 3
            anotherReportView.layer.shadowColor = themeBlackColor.cgColor
            anotherReportView.layer.shadowOpacity = 0.4
            
        }
        
    }
    
    @objc func shortlistLabelTapped(sender: UITapGestureRecognizer) {
        delegate?.shortlistStudentButtonTapped()
    }
    
    @objc func reportLabelTapped(sender: UITapGestureRecognizer) {
        delegate?.reportStudentButtonTapped()
    }
    
    @objc func messageLabelTapped(sender: UITapGestureRecognizer) {
        delegate?.messageStudentButtonTapped()
    }
    
    func configureCellWithStudent(_ studentInfo: StudentDetailedInfo) {
        let student = studentInfo.student!
        studentImageView.sd_setShowActivityIndicatorView(true)
        studentImageView.sd_setIndicatorStyle(.gray)
        studentImageView.sd_setImage(with: URL(string: student.imageURL), placeholderImage: UIImage(named: "profile_placeholder"), options: .retryFailed) { (image, error, cacheType, url) in
            if error == nil {
                //Do nothing
            }
        }
        
        let attributedString = NSMutableAttributedString()
        attributedString.append(NSAttributedString(string: student.name,
                                                   attributes: [.font: UIFont(name: "Montserrat-SemiBold", size: 25.0)!,
                                                                .foregroundColor : themeBlackColor]))
        attributedString.append(NSAttributedString(string: "\n\(student.gender!)",
            attributes: [.font: UIFont(name: "Montserrat-Light", size: 16.0)!,
                         .foregroundColor : themeBlackColor]))
        infoLabel.attributedText = attributedString
        
        
        shortlistLabel.text = student.isFavorite ? "SHORTLISTED".localized : "SHORTLIST".localized
        shortlistLabel.isUserInteractionEnabled = !student.isFavorite
        
    }
}
