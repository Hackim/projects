//
//  AgencyAboutTableViewCell.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 03/07/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit

class AgencyAboutTableViewCell: UITableViewCell {

    @IBOutlet weak var textView: UITextView!
    
    class func cellIdentifier() -> String {
        return "AgencyAboutTableViewCell"
    }
 
    class func cellHeightForAbout(_ agency: AgencyDetailedInfo) -> CGFloat {
        return 50.0 + agency.aboutHTML.height(withConstrainedWidth: ScreenWidth - 56.0)
    }

}
