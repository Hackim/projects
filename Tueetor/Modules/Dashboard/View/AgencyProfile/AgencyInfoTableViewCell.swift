//
//  AgencyInfoTableViewCell.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 12/06/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit
import SDWebImage

protocol AgencyInfoTableViewCellDelegate {
    func reportAgency()
    func enquireAgency()
}

class AgencyInfoTableViewCell: UITableViewCell {

    @IBOutlet weak var agencyImageView: UIImageView!
    @IBOutlet weak var infoLabel: UITextView!
    @IBOutlet weak var enquiryView: UIView!
    @IBOutlet weak var reportView: UIView!
    
    var delegate: AgencyInfoTableViewCellDelegate?
    
    class func cellIdentifier() -> String {
        return "AgencyInfoTableViewCell"
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        enquiryView.layer.shadowOffset = CGSize.init(width: 0, height: 5)
        enquiryView.layer.shadowRadius = 3
        enquiryView.layer.shadowColor = themeBlackColor.cgColor
        enquiryView.layer.shadowOpacity = 0.4
        
        reportView.layer.shadowOffset = CGSize.init(width: 0, height: 5)
        reportView.layer.shadowRadius = 3
        reportView.layer.shadowColor = themeBlackColor.cgColor
        reportView.layer.shadowOpacity = 0.4
        
    }

    class func cellHeightForAgency(_ agency: AgencyDetailedInfo) -> CGFloat {
        return 92.0 + agency.attributedInfo.height(withConstrainedWidth: ScreenWidth - 164.0)
    }
    
    func configureCellWithAgency(_ agency: AgencyDetailedInfo) {
        agencyImageView.sd_setShowActivityIndicatorView(true)
        agencyImageView.sd_setIndicatorStyle(.gray)
        agencyImageView.sd_setImage(with: URL(string: agency.imagePath), placeholderImage: UIImage(named:
            "profile_placeholder"), options: .retryFailed) { (image, error, cacheType, url) in
            if error == nil {
                //Do nothing
            }
        }
        infoLabel.attributedText = agency.attributedInfo
    }
    @IBAction func enquireButtonTapped(_ sender: UIButton) {
        delegate?.enquireAgency()
    }
    
    @IBAction func reportButtonTapped(_ sender: UIButton) {
        delegate?.reportAgency()
    }
}
