//
//  LocationTableViewCell.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 12/06/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit

class LocationTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var phoneTextView: UITextView!
    @IBOutlet weak var phoneImageView: UIImageView!
    
    class func cellIdentifier() -> String {
        return "LocationTableViewCell"
    }

    func configureCellWithLocation(_ location: Location) {
        titleLabel.text = location.nickName
        
        addressLabel.text = location.completeAddress
        phoneTextView.text = location.phone
        let imageName = "phonecode"
        let originalImage = UIImage(named: imageName)
        let templateImage = originalImage!.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
        phoneImageView.image = templateImage
        phoneImageView.tintColor = themeBlueColor
    }
    
    class func cellHeightForLocation(_ location: Location) -> CGFloat {
        let nameAttriString = NSAttributedString.init(string: location.nickName,
                                                      attributes: [.font: UIFont(name: "Montserrat-Regular", size: 17.0)!,
                                                                   .foregroundColor : themeBlackColor])
        let addressAttriString = NSAttributedString.init(string: location.completeAddress,
                                                         attributes: [.font: UIFont(name: "Montserrat-Regular", size: 13.0)!,
                                                                      .foregroundColor : themeBlackColor])
        let nameStringHeight = nameAttriString.height(withConstrainedWidth: ScreenWidth - 44.0)
        let addressStringHeight = addressAttriString.height(withConstrainedWidth: ScreenWidth - 72.0)
        
        return addressStringHeight + nameStringHeight + 60.0
        
    }
}
