//
//  CourseScheduleTableViewCell.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 06/07/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit
import SpreadsheetView

class CourseScheduleTableViewCell: UITableViewCell, SpreadsheetViewDataSource, SpreadsheetViewDelegate {
    
    @IBOutlet weak var spreadSheet: SpreadsheetView!
    
    var headers = ["MON".localized, "TUE".localized, "WED".localized, "THU".localized, "FRI".localized, "SAT".localized, "SUN".localized]
    var data = [[String]]()
    var delegate: ScheduleTableViewCellDelegate?
    var rowsCount = 1
    
    class func cellIdentifier() -> String {
        return "CourseScheduleTableViewCell"
    }
    
    class func cellHeight(_ schedules: [String: [String]]) -> CGFloat {
        var count = 1
        for day in daysArray {
            let daysSchedule = schedules[day] ?? [""]
            if count < daysSchedule.count {
                count = daysSchedule.count
            }
        }

        return 40.0 + CGFloat(count * 35)
    }
    
    func configureCellWithSchedules(_ schedules: [String : [String]]) {
        
        data = getDataArrayFromSchedules(schedules)
        spreadSheet.delegate = self
        spreadSheet.dataSource = self
        spreadSheet.register(HeaderCell.self, forCellWithReuseIdentifier: String(describing: HeaderCell.self))
        spreadSheet.register(TextCell.self, forCellWithReuseIdentifier: String(describing: TextCell.self))
        
    }
    
    func getDataArrayFromSchedules(_ schedules: [String: [String]]) -> [[String]] {
        var completeData = [[String]]()
        for day in daysArray {
            let daysSchedule = schedules[day] ?? []
            let count = daysSchedule.count
            if rowsCount < count {
                rowsCount = count
            }
            if daysSchedule.count > 0 {
                var finalArray = [day]
                finalArray.append(contentsOf: daysSchedule)
                completeData.append(finalArray)
            }
        }
        return completeData
    }
    
    // MARK: DataSource
    
    func numberOfColumns(in spreadsheetView: SpreadsheetView) -> Int {
        return data.count
    }
    
    func numberOfRows(in spreadsheetView: SpreadsheetView) -> Int {
        return 1 + rowsCount
    }
    
    func spreadsheetView(_ spreadsheetView: SpreadsheetView, widthForColumn column: Int) -> CGFloat {
        return 160
    }
    
    func spreadsheetView(_ spreadsheetView: SpreadsheetView, heightForRow row: Int) -> CGFloat {
        if case 0 = row {
            return 40
        } else {
            return 35
        }
    }
    
    func frozenRows(in spreadsheetView: SpreadsheetView) -> Int {
        return rowsCount + 1
    }
    
    func spreadsheetView(_ spreadsheetView: SpreadsheetView, cellForItemAt indexPath: IndexPath) -> Cell? {
        if case 0 = indexPath.row {
            let cell = spreadSheet.dequeueReusableCell(withReuseIdentifier: String(describing: HeaderCell.self), for: indexPath) as! HeaderCell
            cell.contentView.backgroundColor = themeLightGrayColor
            cell.gridlines.left = .solid(width: 1, color: themeLightGrayColor)
            cell.gridlines.top = .solid(width: 1, color: themeLightGrayColor)
            cell.gridlines.right = .solid(width: 1, color: themeLightGrayColor)
            cell.gridlines.bottom = .none
            cell.label.text = data[indexPath.column][0]
            
            return cell
        } else {
            let scheduleArray = data[indexPath.column]
            let timing = scheduleArray[safe: indexPath.row] ?? ""
            let cell = spreadSheet.dequeueReusableCell(withReuseIdentifier: String(describing: TextCell.self), for: indexPath) as! TextCell
            cell.label.text = timing
            cell.gridlines.bottom = timing.isEmpty ? .none : .solid(width: 1, color: themeLightGrayColor)
            cell.gridlines.left = .none
            cell.gridlines.right = .none
            cell.gridlines.top = .none
            cell.backgroundColor = timing.isEmpty ? UIColor.clear : themeBlueColor
            return cell
        }
    }
    
}
