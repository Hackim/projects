//
//  CoursesTableViewCell.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 12/06/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit

protocol CoursesTableViewCellDelegate {
    func showWebViewForCourseAtIndex(_ index: Int)
    func showDocumentsForCourseAtIndex(_ index: Int)
    func showSchedulesForCourseAtIndex(_ index: Int)
    func showCourseEnquiryAtIndex(_ index: Int)
}

class CoursesTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var levelLabel: UILabel!
    @IBOutlet weak var subjectLabel: UILabel!
    @IBOutlet weak var costLabel: UILabel!
    @IBOutlet weak var schedulesLabel: UILabel!
    @IBOutlet weak var documentsLabel: UILabel!
    @IBOutlet weak var infoButton: UIButton!
    @IBOutlet weak var documentsButton: UIButton!
    @IBOutlet weak var schedulesButton: UIButton!
    @IBOutlet weak var enquireButton: UIButton!
    
    var delegate: CoursesTableViewCellDelegate?
    
    class func cellIdentifier() -> String {
        return "CoursesTableViewCell"
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        enquireButton.layer.borderColor = themeBlueColor.cgColor
        enquireButton.layer.borderWidth = 1.0
    }
    
    func configureCellWithCourse(_ course: Course, tag: Int) {
        nameLabel.attributedText = course.nameAttributedString
        subjectLabel.text = course.subject
        levelLabel.text = course.level
        infoButton.tag = tag
        schedulesButton.tag = tag
        documentsButton.tag = tag
        enquireButton.tag = tag
        var costString = ""
        
        if let pricePerSession = course.pricePerSession, !pricePerSession.isEmpty {
            costString = pricePerSession + "/session".localized
        } else {
            costString = "NA/session".localized
        }
        costString += "   "
        if let pricePerMonth = course.pricePerMonth, !pricePerMonth.isEmpty {
            costString += pricePerMonth + "/month".localized
        } else {
            costString += "NA/month".localized
        }

        costString += "   "
        if let pricePerTerm = course.pricePerTerm, !pricePerTerm.isEmpty {
            costString += pricePerTerm + "/term".localized
        } else {
            costString += "NA/month".localized
        }

        costLabel.text = costString
        
        if let schedules = course.schedule, schedules > 0 {
            schedulesLabel.text = "\(schedules) \(schedules == 1 ? "schedule" : "schedules") available".localized
        } else {
            schedulesLabel.text = "No schedules available".localized
        }
        
        if let documents = course.doc, documents > 0 {
            documentsLabel.text = "\(documents) \(documents == 1 ? "document" : "documents") available".localized
        } else {
            documentsLabel.text = "No documents available".localized
        }
    }
    
    @IBAction func infoButtonTapped(_ sender: UIButton) {
        delegate?.showWebViewForCourseAtIndex(sender.tag)
    }

    @IBAction func documentsButtonTapped(_ sender: UIButton) {
        delegate?.showDocumentsForCourseAtIndex(sender.tag)
    }
    
    @IBAction func schedulesButtontTapped(_ sender: UIButton) {
        delegate?.showSchedulesForCourseAtIndex(sender.tag)
    }

    @IBAction func enquireButtonTapped(_ sender: UIButton) {
        delegate?.showCourseEnquiryAtIndex(sender.tag)
    }
    
    class func cellHeightForCourse(_ course: Course, isSelected: Bool) -> CGFloat {
        return course.nameAttributedString.height(withConstrainedWidth: ScreenWidth - 86.0) + (isSelected ? 162.0 : 52.0)
    }

}

