//
//  SubjectListTableViewCell.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 27/05/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit

class SubjectListTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var countLabel: UILabel!
    
    class func cellIdentifier() -> String {
        return "SubjectListTableViewCell"
    }
    
    func configureCellWithSubject(_ subject: Subject) {
        nameLabel.text = subject.name
        countLabel.text = subject.searchCount
    }
}
