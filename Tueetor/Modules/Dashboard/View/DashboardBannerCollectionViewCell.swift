//
//  DashboardBannerCollectionViewCell.swift
//  Tueetor
//
//  Created by Phaninder on 14/05/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit
import SDWebImage

class DashboardBannerCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var bannerImageView: UIImageView!
    
    class func cellIdentifier() -> String {
        return "DashboardBannerCollectionViewCell"
    }
    
    func configureCellWithBanner(_ banner: Banner) {
        bannerImageView.sd_setShowActivityIndicatorView(true)
        bannerImageView.sd_setIndicatorStyle(.gray)
        bannerImageView.sd_setImage(with: URL(string: banner.imageURL), completed: nil)
    }
    
}
