//
//  DashboardSubjectsTableViewCell.swift
//  Tueetor
//
//  Created by Phaninder on 14/05/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit
import SDWebImage

protocol DashboardSubjectsTableViewCellDelegate {
    func showAllCategoriesScreen()
    func showCategorySubjects(_ button: UIButton)
}

class DashboardSubjectsTableViewCell: UITableViewCell {

    @IBOutlet weak var moreSubjectsButton: UIButton!

    @IBOutlet weak var categoryImageView1: UIImageView!
    @IBOutlet weak var categoryImageView2: UIImageView!
    @IBOutlet weak var categoryImageView3: UIImageView!
    @IBOutlet weak var categoryImageView4: UIImageView!
    
    @IBOutlet weak var categoryTitleLabel1: UILabel!
    @IBOutlet weak var categoryTitleLabel2: UILabel!
    @IBOutlet weak var categoryTitleLabel3: UILabel!
    @IBOutlet weak var categoryTitleLabel4: UILabel!
    
    @IBOutlet weak var categorySubjectsLabel1: UILabel!
    @IBOutlet weak var categorySubjectsLabel2: UILabel!
    @IBOutlet weak var categorySubjectsLabel3: UILabel!
    @IBOutlet weak var categorySubjectsLabel4: UILabel!
    
    @IBOutlet weak var moreLabel1: UILabel!
    @IBOutlet weak var moreLabel2: UILabel!
    @IBOutlet weak var moreLabel3: UILabel!
    @IBOutlet weak var moreLabel4: UILabel!
    
    var delegate: DashboardSubjectsTableViewCellDelegate?
    
    class func cellIdentifier() -> String {
        return "DashboardSubjectsTableViewCell"
    }
    
    class func cellHeight() -> CGFloat {
        return 583.0
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        self.moreSubjectsButton.layer.shadowOffset = CGSize.init(width: 0, height: 5)
        self.moreSubjectsButton.layer.shadowRadius = 3
        self.moreSubjectsButton.layer.shadowColor = UIColor(red: 0.098, green: 0.6431, blue: 0.6941, alpha: 1).cgColor
        self.moreSubjectsButton.layer.shadowOpacity = 0.75
    }
    
    
    func configureCellWithSubjects(_ categories: [SubjectCategory]) {
        let category1 = categories[0]
        categoryTitleLabel1.text = category1.name
        categorySubjectsLabel1.text = getSubjectsString(category1)
        categoryImageView1.sd_setShowActivityIndicatorView(true)
        categoryImageView1.sd_setIndicatorStyle(.gray)
        categoryImageView1.sd_setImage(with: URL(string: category1.image), completed: nil)
        moreLabel1.isHidden = category1.subjects.count < 4
        
        let category2 = categories[1]
        categoryTitleLabel2.text = category2.name
        categorySubjectsLabel2.text = getSubjectsString(category2)
        categoryImageView2.sd_setShowActivityIndicatorView(true)
        categoryImageView2.sd_setIndicatorStyle(.gray)
        categoryImageView2.sd_setImage(with: URL(string: category2.image), completed: nil)
        moreLabel2.isHidden = category2.subjects.count < 4

        let category3 = categories[2]
        categoryTitleLabel3.text = category3.name
        categorySubjectsLabel3.text = getSubjectsString(category3)
        categoryImageView3.sd_setShowActivityIndicatorView(true)
        categoryImageView3.sd_setIndicatorStyle(.gray)
        categoryImageView3.sd_setImage(with: URL(string: category3.image), completed: nil)
        moreLabel3.isHidden = category3.subjects.count < 4

        let category4 = categories[3]
        categoryTitleLabel4.text = category4.name
        categorySubjectsLabel4.text = getSubjectsString(category4)
        categoryImageView4.sd_setShowActivityIndicatorView(true)
        categoryImageView4.sd_setIndicatorStyle(.gray)
        categoryImageView4.sd_setImage(with: URL(string: category4.image), completed: nil)
        moreLabel4.isHidden = category4.subjects.count < 4


    }
    
    func getSubjectsString(_ category: SubjectCategory) -> String {
        var subsString = ""
        var i = 0
        for subject in category.subjects {
            guard i < 2 else {
                subsString += "\(subject.name ?? "")"
                return subsString
            }
            subsString += "\(subject.name ?? "")\n"
            i += 1
        }
        return subsString
    }
    
    @IBAction func moreButtonTapped(_ sender: UIButton) {
        delegate?.showAllCategoriesScreen()
    }
    
    @IBAction func moreSubjectsButtonTapped(_ sender: UIButton) {
        delegate?.showCategorySubjects(sender)
    }
    
    
}
