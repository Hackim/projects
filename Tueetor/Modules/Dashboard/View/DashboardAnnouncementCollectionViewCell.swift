//
//  DashboardAnnouncementCollectionViewCell.swift
//  Tueetor
//
//  Created by Phaninder on 14/05/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit

class DashboardAnnouncementCollectionViewCell: UICollectionViewCell {
 
    @IBOutlet weak var cellImageView: UIImageView!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var costLabel: UILabel!
    @IBOutlet weak var holderView: UIView!
    
    class func cellIdentifier() -> String {
        return "DashboardAnnouncementCollectionViewCell"
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.holderView.layer.shadowOffset = CGSize(width: 4, height: 4)
        self.holderView.layer.shadowColor = UIColor.black.cgColor
        self.holderView.layer.shadowRadius = 1
        self.holderView.layer.cornerRadius = 8
        self.holderView.layer.borderColor = UIColor.lightGray.cgColor
        self.holderView.layer.borderWidth = 0.5
        self.holderView.layer.shadowOpacity = 0.1
        self.holderView.layer.masksToBounds = false
        self.holderView.layer.rasterizationScale = UIScreen.main.scale
        self.holderView.clipsToBounds = false

    }
    
    func configureCellWithAnnouncement(_ announcement: Announcement) {
        titleLabel.text = announcement.title
        cellImageView.sd_setShowActivityIndicatorView(true)
        cellImageView.sd_setIndicatorStyle(.gray)
        cellImageView.sd_setImage(with: URL(string: announcement.imageURL), completed: nil)

    }

}
