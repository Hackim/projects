//
//  DashboardStatsTableViewCell.swift
//  Tueetor
//
//  Created by Phaninder on 12/05/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit

protocol DashboardStatsTableViewCellDelegate {
    func showTrainers()
    func showLeads()
    func showLocations()
    func showSubjects()
    func showAssignments()
}

class DashboardStatsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var trainersLabel: UILabel!
    @IBOutlet weak var leadsLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var subjectsLabel: UILabel!
    @IBOutlet weak var assignmentsLabel: UILabel!
    
    var delegate: DashboardStatsTableViewCellDelegate?
    
    class func cellIdentifier() -> String {
        return "DashboardStatsTableViewCell"
    }
    
    class func cellHeight() -> CGFloat {
        return 447.0
    }
    
    func configureCell(_ dashboard: DashboardInfo) {
        trainersLabel.text = String(dashboard.trainersOnline)
        leadsLabel.text = String(dashboard.leadsGenerated)
        locationLabel.text = String(dashboard.locations)
        subjectsLabel.text = String(dashboard.subjectsAvailable)
        assignmentsLabel.text = String(dashboard.activeSubjects)
    }
    
    @IBAction func trainersButtonTapped(_ sender: UIButton) {
        delegate?.showTrainers()
    }
    
    
    @IBAction func leadsButtonTapped(_ sender: UIButton) {
        delegate?.showLeads()
    }
    
    @IBAction func locationButtonTapped(_ sender: UIButton) {
        delegate?.showLocations()
    }
    
    @IBAction func subjectsButtonTapped(_ sender: UIButton) {
        delegate?.showSubjects()
    }
    
    @IBAction func assignmentsButtonTapped(_ sender: UIButton) {
        delegate?.showAssignments()
    }
    
}
