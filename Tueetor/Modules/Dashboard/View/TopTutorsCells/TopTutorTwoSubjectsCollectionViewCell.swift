//
//  TopTutorTwoSubjectsCollectionViewCell.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 05/06/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit
import SwiftyStarRatingView
import SDWebImage

class TopTutorTwoSubjectsCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var holderView: UIView!
    @IBOutlet weak var tutorImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var favIconButton: UIButton!
    @IBOutlet weak var ratingView: SwiftyStarRatingView!
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var responseTimeLabel: UILabel!
    @IBOutlet weak var subjectLabel1: UILabel!
    @IBOutlet weak var subjectLabel2: UILabel!
    
    class func cellIdentifier() -> String {
        return "TopTutorTwoSubjectsCollectionViewCell"
    }
    
    class func cellHeight() -> CGFloat {
        return 176.0
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.holderView.layer.cornerRadius = 5.0
        self.holderView.layer.borderColor = themeLightGrayColor.cgColor
        self.holderView.layer.borderWidth = 1.4
        self.holderView.clipsToBounds = true
    }

    func configureCellWithTutor(_ tutor: Tutor) {
        nameLabel.text = tutor.name
        ratingLabel.text = tutor.ratingCount
        if let n = NumberFormatter().number(from: tutor.averageRating) {
            
            ratingView.value = CGFloat(truncating: n)
        }
        subjectLabel1.isHidden = true
        subjectLabel2.isHidden = true
        if !Utilities.isUserTypeStudent() {
            favIconButton.isHidden = true
        }

        tutorImageView.sd_setShowActivityIndicatorView(true)
        tutorImageView.sd_setIndicatorStyle(.gray)
        tutorImageView.sd_setImage(with: URL(string: tutor.imageURL), placeholderImage: UIImage(named: "profile_placeholder"), options: .retryFailed, completed: nil)
        let favImage = UIImage(named: tutor.isFavorite ? "heartActive" : "heartIconInactive")
        favIconButton.setImage(favImage, for: .normal)
        if let subject1 = tutor.subjects[safe: 0] {
            subjectLabel1.text = subject1.name
            subjectLabel1.isHidden = false
        }
        
        if let subject2 = tutor.subjects[safe: 1] {
            subjectLabel2.text = subject2.name
            subjectLabel2.isHidden = false
        }
        
    }
    

}
