//
//  DashboardTutorsTableViewCell.swift
//  Tueetor
//
//  Created by Phaninder on 14/05/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit

protocol DashboardTutorsTableViewCellDelegate {
    func showFeaturedTutors()
    func tutorSelected(_ tutor: Tutor)
}

class DashboardTutorsTableViewCell: UITableViewCell {

    @IBOutlet weak var moreTutorsButton: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    
    var tutors: [Tutor]!
    
    var delegate: DashboardTutorsTableViewCellDelegate?
    
    class func cellIdentifier() -> String {
        return "DashboardTutorsTableViewCell"
    }
    
    class func cellHeight() -> CGFloat {
        return 290.0
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.moreTutorsButton.layer.shadowOffset = CGSize.init(width: 0, height: 5)
        self.moreTutorsButton.layer.shadowRadius = 3
        self.moreTutorsButton.layer.shadowColor = UIColor(red: 0.098, green: 0.6431, blue: 0.6941, alpha: 1).cgColor
        self.moreTutorsButton.layer.shadowOpacity = 0.75
    }
    
    func reloadCollectionViewData() {
        self.collectionView.reloadData()
    }
    
    @IBAction func tutorsButtonTapped(_ sender: UIButton) {
        delegate?.showFeaturedTutors()
    }
    
}

extension DashboardTutorsTableViewCell: UICollectionViewDelegateFlowLayout, UICollectionViewDelegate, UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return tutors.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: DashboardTutorCollectionViewCell.cellIdentifier(), for: indexPath) as! DashboardTutorCollectionViewCell
        cell.configureCellWithTutor(tutors[indexPath.item])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 145, height: 125)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        delegate?.tutorSelected(tutors[indexPath.item])
    }
}
