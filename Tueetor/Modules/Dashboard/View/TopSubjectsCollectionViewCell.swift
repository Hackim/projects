//
//  TopSubjectsCollectionViewCell.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 27/05/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit

class TopSubjectsCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var categoryImageView: UIImageView!
    @IBOutlet weak var categoryTitleLabel: UILabel!
    @IBOutlet weak var categorySubjectsLabel: UILabel!

    class func cellIdentifier() -> String {
        return "TopSubjectsCollectionViewCell"
    }
    
    func configureCellWithTopSubject(_ category: SubjectCategory) {
        categoryTitleLabel.text = category.name
        categorySubjectsLabel.text = getSubjectsString(category)
        categoryImageView.sd_setShowActivityIndicatorView(true)
        categoryImageView.sd_setIndicatorStyle(.gray)
        categoryImageView.sd_setImage(with: URL(string: category.image), completed: nil)

    }
 
    func getSubjectsString(_ category: SubjectCategory) -> String {
        var subsString = ""
        var i = 0
        for subject in category.subjects {
            guard i < 2 else {
                subsString += "\(subject.name ?? "")"
                return subsString
            }
            subsString += "\(subject.name ?? "")\n"
            i += 1
        }
        return subsString
    }

}
