//
//  ShoutoutListingViewController.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 30/08/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit
import RxSwift

protocol ShoutoutListingViewControllerDelegate {
    func finishedSelectingTutors(tutors: [MapItem], selectedIDs: [String])
    func finishedSelectingAgencies(agencies: [MapItem], selectedIDs: [AgencyIDInfo])
}

class ShoutoutListingViewController: BaseViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var isTutor = true
    var mapItemPagination = MapItemPagination()
    var disposableBag = DisposeBag()
    var findDisposable: Disposable!
    var isFirstTime = true
    var filterParams: FilterParams!
    var isFetchingData = false
    var currentLocation: CLLocationCoordinate2D!
    var selectedTutorIDs: [String]!
    var selectedAgencyIDs: [AgencyIDInfo]!
    var delegate: ShoutoutListingViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        tableView.tableFooterView = UIView()
        isFirstTime = true
        filterParams.setLegendsForTutorSelection()
        filterParams.showAgencyPin = !isTutor
        filterParams.showTutorSubPin = isTutor
        filterParams.isShoutout = false
        mapItemPagination.paginationType = .new
        filterUsers()
    }

    @IBAction func backButtonTapped(_ sender: UIButton) {
        let selectedItems = mapItemPagination.mapItems.filter { (mapItem) -> Bool in
            return mapItem.isSelectedForShoutout
        }
        if isTutor {
            delegate?.finishedSelectingTutors(tutors: selectedItems, selectedIDs: selectedTutorIDs)
        } else {
            delegate?.finishedSelectingAgencies(agencies: selectedItems, selectedIDs: selectedAgencyIDs)
        }
        self.navigateBack(sender: sender)
    }
    
    func configureObjectsWithSelectionState(_ mapItemPagination: MapItemPagination) {
        if isTutor {
            for id in selectedTutorIDs {
                let index = mapItemPagination.mapItems.index { (mapItem) -> Bool in
                    return mapItem.ID == id
                }
                if let foundIndex = index, foundIndex != -1 {
                    mapItemPagination.mapItems[foundIndex].isSelectedForShoutout = true
                }
            }
        } else {
            for agencyID in selectedAgencyIDs {
                let index = mapItemPagination.mapItems.index { (mapItem) -> Bool in
                    return agencyID.agencyID == mapItem.ID && agencyID.assetID == mapItem.assetID
                }
                if let foundIndex = index, foundIndex != -1 {
                    mapItemPagination.mapItems[foundIndex].isSelectedForShoutout = true
                }
            }
        }
    }
 
    func showCommentAlertForIndex(_ index: Int) {
        let alertController = UIAlertController(title: AppName,
                                                message: AlertMessage.newShortlistComment.description(),
                                                preferredStyle: .alert)
        
        
        let submitAction = UIAlertAction(title: AlertButton.submit.description(),
                                         style: .default) { [weak alertController] _ in
                                            guard let alertController = alertController, let textField = alertController.textFields?.first else { return }
                                            DispatchQueue.main.async {
                                                print(textField.text!)
                                                self.addUserToShortlist(comment: textField.text!, itemIndex: index)
                                            }
        }
        submitAction.isEnabled = false
        alertController.addAction(submitAction)
        
        alertController.addTextField { textField in
            textField.placeholder = "New comment".localized
            
            NotificationCenter.default.addObserver(forName: NSNotification.Name.UITextFieldTextDidChange, object: textField, queue: OperationQueue.main, using: { (notification) in
                submitAction.isEnabled = !textField.text!.isEmpty
            })
        }
        
        let cancelAction = UIAlertAction(title: AlertButton.cancel.description(), style: .cancel, handler: nil)
        alertController.addAction(cancelAction)
        present(alertController, animated: true, completion: nil)
    }
    
    func showDeleteAlertForIndex(_ index: Int) {
        let alert = UIAlertController(title: AppName, message: AlertMessage.removeShortlist.description(), preferredStyle: .alert)
        
        let confirmAction = UIAlertAction(title: AlertButton.confirm.description(), style: .default, handler: { (action) in
            DispatchQueue.main.async {
                self.removeUserFromShortlist(itemIndex: index)
            }
        })
        alert.addAction(confirmAction)
        
        let cancelAction = UIAlertAction(title: AlertButton.cancel.description(), style: .default, handler: nil)
        alert.addAction(cancelAction)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func canAddItemToSelection() -> Bool {
        return selectedTutorIDs.count + selectedAgencyIDs.count < 10
    }

}

extension ShoutoutListingViewController: UITableViewDelegate, UITableViewDataSource {
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.tableView.isHidden ? 0 : mapItemPagination.mapItems.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard indexPath.row != mapItemPagination.mapItems.count else {
            let cell = tableView.dequeueReusableCell(withIdentifier: LoadMoreTableViewCell.cellIdentifier(), for: indexPath) as! LoadMoreTableViewCell
            cell.activityIndicator.startAnimating()
            return cell
        }
        if isTutor {
            let cell = tableView.dequeueReusableCell(withIdentifier: FindTutorProfileTableViewCell.cellIdentifier()) as! FindTutorProfileTableViewCell
            let mapItem = mapItemPagination.mapItems[indexPath.row]
            cell.delegate = self
            cell.configureWithMapItemForShoutoutScreen(mapItem, atIndex: indexPath.row, isSelected: mapItem.isSelectedForShoutout)
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: FindAgencyProfileTableViewCell.cellIdentifier()) as! FindAgencyProfileTableViewCell
            let mapItem = mapItemPagination.mapItems[indexPath.row]
            cell.delegate = self
            cell.configureWithMapItemForShoutoutScreen(mapItem, atIndex: indexPath.row, isSelected: mapItem.isSelectedForShoutout)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        guard indexPath.row != mapItemPagination.mapItems.count else {
            return mapItemPagination.hasMoreToLoad ? 50.0 : 0
        }
        return isTutor ? FindTutorProfileTableViewCell.cellHeight() : FindAgencyProfileTableViewCell.cellHeight()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard indexPath.row != mapItemPagination.mapItems.count else {
            return
        }
        if isTutor {
            let tutor = mapItemPagination.mapItems[indexPath.row]
            let index = selectedTutorIDs.index(of: tutor.ID)
            if let itemIndex = index, itemIndex > -1 {
                selectedTutorIDs.remove(at: itemIndex)
            } else {
                guard canAddItemToSelection() else {
                    self.showSimpleAlertWithMessage("Cannot send message to more than 10 profiles at a time".localized)
                    return
                }
                selectedTutorIDs.append(tutor.ID)
            }
            tutor.isSelectedForShoutout = !tutor.isSelectedForShoutout
            self.tableView.reloadRows(at: [indexPath], with: .none)
        } else {
            let agency = mapItemPagination.mapItems[indexPath.row]
            let index = selectedAgencyIDs.index { (agencyIDInfo) -> Bool in
                return agency.ID == agencyIDInfo.agencyID && agency.assetID == agencyIDInfo.assetID
            }
            if let itemIndex = index, itemIndex > -1 {
                selectedAgencyIDs.remove(at: itemIndex)
            } else {
                guard canAddItemToSelection() else {
                    self.showSimpleAlertWithMessage("Cannot send message to more than 10 profiles at a time".localized)
                    return
                }
                selectedAgencyIDs.append((agencyID: agency.ID, assetID: agency.assetID))
            }
            agency.isSelectedForShoutout = !agency.isSelectedForShoutout
            self.tableView.reloadRows(at: [indexPath], with: .none)
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == mapItemPagination.mapItems.count && self.mapItemPagination.hasMoreToLoad && !isFetchingData {
            filterUsers()
        }
    }
    
}

extension ShoutoutListingViewController: FindTutorProfileTableViewCellDelegate {
    
    func showProfileAtIndex(_ index: Int) {
        let mapItem = mapItemPagination.mapItems[index]
        if mapItem.type == "tutor" {
            let tutorProfile = TutorProfileViewController.instantiateFromAppStoryboard(appStoryboard: .Dashboard)
            tutorProfile.tutorID = mapItem.ID
            tutorProfile.delegate = self
            navigationController?.pushViewController(tutorProfile, animated: true)
        } else if mapItem.type == "agency" {
            let agencyProfile = PartnerProfileViewController.instantiateFromAppStoryboard(appStoryboard: .Dashboard)
            agencyProfile.agencyID = mapItem.ID
            agencyProfile.assetID = mapItem.assetID
            navigationController?.pushViewController(agencyProfile, animated: true)
        }
        
    }
    
    func messageProfileAtIndex(_ index: Int) {
        self.showSimpleAlertWithMessage("Yet to be developed")
    }
    
    func shortlistProfileAtIndex(_ index: Int) {
        guard UserStore.shared.isLoggedIn else {
            
            showLoginAlert()
            return
        }
        let mapItem = mapItemPagination.mapItems[index]
        if mapItem.isFavorite {
            showDeleteAlertForIndex(index)
        } else {
            showCommentAlertForIndex(index)
        }
    }
    
    func reportProfileAtIndex(_ index: Int) {
        guard UserStore.shared.isLoggedIn else {
            showLoginAlert()
            return
        }
        
        let reportVC = ReportUserViewController(nibName: "ReportUserViewController", bundle: nil)
        reportVC.mapItem = mapItemPagination.mapItems[index]
        // Create the dialog
        let popup = PopupDialog(viewController: reportVC,
                                buttonAlignment: .horizontal,
                                transitionStyle: .bounceDown,
                                tapGestureDismissal: true,
                                panGestureDismissal: true)
        
        let buttonOne = CancelButton(title: AlertButton.cancel.description(), height: 60) {
            //Do nothing
        }
        
        
        let buttonTwo = DefaultButton(title: AlertButton.submit.description(), height: 60) {
            self.reportUserWithComment(reportVC.commentsTextView.text ?? "", nature: reportVC.selectedOption, mapItem: reportVC.mapItem)
        }
        
        popup.addButtons([buttonOne, buttonTwo])
        present(popup, animated: true, completion: nil)
    }
    
    
    func selectMarkerForProfileAtIndex(_ index: Int) {
        //Doesnt apply here
    }
    
}

extension ShoutoutListingViewController: FindAgencyProfileTableViewCellDelegate {
    
    func selectMarkerForAgencyAtIndex(_ index: Int) {
        //Doesnt apply here
    }
    
    func showAgencyAtIndex(_ index: Int) {
        
        let mapItem = mapItemPagination.mapItems[index]
        let agencyProfile = PartnerProfileViewController.instantiateFromAppStoryboard(appStoryboard: .Dashboard)
        agencyProfile.agencyID = mapItem.ID
        agencyProfile.assetID = mapItem.assetID
        navigationController?.pushViewController(agencyProfile, animated: true)
    }
    
    func enquireAgencyAtIndex(_ index: Int) {
        guard UserStore.shared.isLoggedIn else {
            showLoginAlert()
            return
        }
        
        let enquireVC = EnquireAgencyViewController(nibName: "EnquireAgencyViewController", bundle: nil)
        let mapItem = mapItemPagination.mapItems[index]
        enquireVC.mapItem = mapItem
        // Create the dialog
        let popup = PopupDialog(viewController: enquireVC,
                                buttonAlignment: .horizontal,
                                transitionStyle: .bounceDown,
                                tapGestureDismissal: true,
                                panGestureDismissal: true)
        
        let buttonOne = CancelButton(title: AlertButton.cancel.description(), height: 60) {
            //Do nothing
        }
        
        
        let buttonTwo = DefaultButton(title: AlertButton.submit.description(), height: 60) {
            self.enquireAgencyWithMessage(enquireVC.commentTextView.text ?? "", fullName: enquireVC.nameTextField.text ?? "", email: enquireVC.emailTextField.text ?? "", phone: enquireVC.phoneTextField.text ?? "", preferred: enquireVC.selectedOption,
                                          mapItem: enquireVC.mapItem)
            
        }
        
        popup.addButtons([buttonOne, buttonTwo])
        present(popup, animated: true, completion: nil)
    }
    
    func reportAgencyAtIndex(_ index: Int) {
        guard UserStore.shared.isLoggedIn else {
            showLoginAlert()
            return
        }
        
        let reportVC = ReportUserViewController(nibName: "ReportUserViewController", bundle: nil)
        let mapItem = mapItemPagination.mapItems[index]
        reportVC.mapItem = mapItem
        
        // Create the dialog
        let popup = PopupDialog(viewController: reportVC,
                                buttonAlignment: .horizontal,
                                transitionStyle: .bounceDown,
                                tapGestureDismissal: true,
                                panGestureDismissal: true)
        
        let buttonOne = CancelButton(title: AlertButton.cancel.description(), height: 60) {
            //Do nothing
        }
        
        
        let buttonTwo = DefaultButton(title: AlertButton.submit.description(), height: 60) {
            self.reportUserWithComment(reportVC.commentsTextView.text ?? "", nature: reportVC.selectedOption, mapItem: reportVC.mapItem)
        }
        
        popup.addButtons([buttonOne, buttonTwo])
        present(popup, animated: true, completion: nil)
    }
    
}

extension ShoutoutListingViewController: TutorProfileViewControllerDelegate {
    func refreshShortlistDataForTutor() {
        
    }
    
    func refreshDataForTutorWithID(_ ID: String) {
        let mapIndex = mapItemPagination.mapItems.index { (item) -> Bool in
            return item.ID == ID
        }
        if let index = mapIndex, index != -1 {
            let currentState = mapItemPagination.mapItems[index].isFavorite
            mapItemPagination.mapItems[index].isFavorite = !currentState
            tableView.reloadData()
        }
    }
    
}

extension ShoutoutListingViewController {
    
    func filterUsers() {
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }
        
        if mapItemPagination.paginationType != .old {
            self.mapItemPagination.currentPageNumber = 1
            if isFirstTime == true {
                Utilities.showHUD(forView: self.view, excludeViews: [])
            } else {
                UIApplication.shared.isNetworkActivityIndicatorVisible = true
            }
        } else {
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
        }
        let filterDict = filterParams.getFilterDictForCurrentLocation(currentLocation, currentCountryID: Utilities.getCurrentCountryCode(), page: String(mapItemPagination.currentPageNumber))
        
        isFetchingData = true
        
        let filterObserver = ApiManager.shared.apiService.findUsers(filterDict as [String: AnyObject], isTutor: filterParams.isTutorButtonSelected)
        findDisposable = filterObserver.subscribe(onNext: {(paginationObject) in
            
            self.isFetchingData = false

            self.configureObjectsWithSelectionState(paginationObject)
            if self.mapItemPagination.paginationType == .old && self.isFirstTime == false {
                DispatchQueue.main.async(execute: {
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                })
            }
            
            self.mapItemPagination.appendDataFromObject(paginationObject)
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                self.tableView.isHidden = false
                self.isFirstTime = false
                self.tableView.reloadData()
            })
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
            })
        })
        findDisposable.disposed(by: disposableBag)
    }
    
    func addUserToShortlist(comment: String, itemIndex: Int) {
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }
        
        guard let userID = UserStore.shared.userID else {
            return
        }
        let mapItem = mapItemPagination.mapItems[itemIndex]
        Utilities.showHUD(forView: self.view, excludeViews: [])
        
        let addToShortlistObserver = ApiManager.shared.apiService.addToShortlistForUser(String(userID), shortlistUserID: mapItem.ID, reason: comment)
        let addToShortlistDisposable = addToShortlistObserver.subscribe(onNext: {(message) in
            Utilities.hideHUD(forView: self.view)
            
            DispatchQueue.main.async {
                NotificationCenter.default.post(name: .refreshDashboardData, object: nil, userInfo:nil)
                self.showSimpleAlertWithMessage(message)
                self.mapItemPagination.mapItems[itemIndex].isFavorite = true
                self.tableView.reloadRows(at: [IndexPath.init(row: itemIndex + 1, section: 0)], with: .none)
            }
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.showSimpleAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        })
        addToShortlistDisposable.disposed(by: disposableBag)
    }
    
    func removeUserFromShortlist(itemIndex: Int) {
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }
        
        guard let userID = UserStore.shared.userID else {
            return
        }
        let mapItem = mapItemPagination.mapItems[itemIndex]
        Utilities.showHUD(forView: self.view, excludeViews: [])
        
        let deleteShortlistObserver = ApiManager.shared.apiService.deleteShortlistForUser(String(userID), shortlistUserID: mapItem.ID)
        let deleteShortlistDisposable = deleteShortlistObserver.subscribe(onNext: {(message) in
            Utilities.hideHUD(forView: self.view)
            
            DispatchQueue.main.async {
                NotificationCenter.default.post(name: .refreshDashboardData, object: nil, userInfo:nil)
                self.showSimpleAlertWithMessage(message)
                self.mapItemPagination.mapItems[itemIndex].isFavorite = false
                self.tableView.reloadRows(at: [IndexPath.init(row: itemIndex + 1, section: 0)], with: .none)
            }
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.showSimpleAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        })
        deleteShortlistDisposable.disposed(by: disposableBag)
    }
    
    func reportUserWithComment(_ comment: String, nature: Int, mapItem: MapItem) {
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }
        
        guard let userID = UserStore.shared.userID else {
            return
        }
        
        var nat = ""
        switch nature {
        case 1:
            nat = "Spam"
        case 2:
            nat = "No Reply"
        default:
            nat = "Improper Conduct"
        }
        
        Utilities.showHUD(forView: self.view, excludeViews: [])
        
        let reportUserObserver = ApiManager.shared.apiService.reportUserWithUserID(mapItem.ID, comment: comment, nature: nat, userID: String(userID))
        let reportUserDisposable = reportUserObserver.subscribe(onNext: {(message) in
            Utilities.hideHUD(forView: self.view)
            
            DispatchQueue.main.async {
                self.showSimpleAlertWithMessage(message)
            }
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.showSimpleAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        })
        reportUserDisposable.disposed(by: disposableBag)
    }
    
    func enquireAgencyWithMessage(_ message: String, fullName: String, email: String, phone: String, preferred: Int, mapItem: MapItem) {
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }
        
        guard let userID = UserStore.shared.userID else {
            return
        }
        
        var preference = ""
        switch preferred {
        case 1:
            preference = "Email"
        default:
            preference = "Phone"
        }
        
        Utilities.showHUD(forView: self.view, excludeViews: [])
        
        let enquireObserver = ApiManager.shared.apiService.enquireAgencyWithID(mapItem.ID, assetID: mapItem.assetID, name: fullName, email: email, phone: phone, preferredContact: preference, message: message, userID: String(userID), courseID: nil)
        let enquireDisposable = enquireObserver.subscribe(onNext: {(message) in
            Utilities.hideHUD(forView: self.view)
            
            DispatchQueue.main.async {
                self.showSimpleAlertWithMessage(message)
            }
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.showSimpleAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        })
        enquireDisposable.disposed(by: disposableBag)
    }
}
