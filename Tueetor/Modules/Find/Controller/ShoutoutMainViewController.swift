//
//  ShoutoutMainViewController.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 22/08/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit
import RxSwift

class ShoutoutMainViewController: BaseViewController {

    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var sendButtonHolderView: UIView!
    
    var currentCountryID: String!
    var disposableBag = DisposeBag()
    var messageCell:ShoutoutMessageTableViewCell!

    var shoutoutModal: Shoutout!
    var filterParams: FilterParams!
    var currentLocation: CLLocationCoordinate2D!
    var message = ""
    var shouldCallAPI = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        (self.tabBarController as! BaseTabBarViewController).tabBar.isHidden = true
        (self.tabBarController as! BaseTabBarViewController).hideCenterButton()
        tableView.tableFooterView = UIView()
        fetchShoutoutUsers()
        sendButtonHolderView.layer.shadowPath = UIBezierPath(roundedRect:
            sendButtonHolderView.bounds, cornerRadius: self.sendButtonHolderView.layer.cornerRadius).cgPath
        sendButtonHolderView.layer.shadowColor = themeBlackColor.cgColor
        sendButtonHolderView.layer.shadowOffset = CGSize(width: 0, height: -2)
        sendButtonHolderView.layer.shadowRadius = 5
        sendButtonHolderView.layer.masksToBounds = false
        sendButtonHolderView.layer.shadowOpacity = 0.4
        
        sendButton.layer.shadowPath = UIBezierPath(roundedRect:
            sendButton.bounds, cornerRadius: self.sendButton.layer.cornerRadius).cgPath
        sendButton.layer.shadowColor = themeBlueColor.cgColor
        sendButton.layer.shadowOffset = CGSize(width: 0, height: 0)
        sendButton.layer.shadowRadius = 5
        sendButton.layer.masksToBounds = false
        sendButton.layer.shadowOpacity = 0.4

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if shouldCallAPI {
            fetchShoutoutUsers()
            shouldCallAPI = false
        }
    }
    
    func filterSelectedTutorIDs() -> [String] {
        let selectedTutors = shoutoutModal.tutors.filter { (mapItem) -> Bool in
            return mapItem.isSelectedForShoutout
        }
        var selectedIDs = [String]()
        for tutor in selectedTutors {
            selectedIDs.append(tutor.ID)
        }
        return selectedIDs
    }
    
    func filterSelectedAgencyIDs() -> [AgencyIDInfo] {
        let selectedAgencies = shoutoutModal.agencies.filter { (mapItem) -> Bool in
            return mapItem.isSelectedForShoutout
        }
        var selectedIDs = [AgencyIDInfo]()
        for agency in selectedAgencies {
            selectedIDs.append((agencyID: agency.ID, assetID: agency.assetID))
        }
        return selectedIDs
    }
    
    func showCommentAlertForIndex(_ index: Int) {
        let alertController = UIAlertController(title: AppName,
                                                message: AlertMessage.newShortlistComment.description(),
                                                preferredStyle: .alert)
        
        
        let submitAction = UIAlertAction(title: AlertButton.submit.description(),
                                         style: .default) { [weak alertController] _ in
                                            guard let alertController = alertController, let textField = alertController.textFields?.first else { return }
                                            DispatchQueue.main.async {
                                                print(textField.text!)
                                                self.addUserToShortlist(comment: textField.text!, itemIndex: index)
                                            }
        }
        submitAction.isEnabled = false
        alertController.addAction(submitAction)
        
        alertController.addTextField { textField in
            textField.placeholder = "New comment".localized
            
            NotificationCenter.default.addObserver(forName: NSNotification.Name.UITextFieldTextDidChange, object: textField, queue: OperationQueue.main, using: { (notification) in
                submitAction.isEnabled = !textField.text!.isEmpty
            })
        }
        
        let cancelAction = UIAlertAction(title: AlertButton.cancel.description(), style: .cancel, handler: nil)
        alertController.addAction(cancelAction)
        present(alertController, animated: true, completion: nil)
    }
    
    func showDeleteAlertForIndex(_ index: Int) {
        let alert = UIAlertController(title: AppName, message: AlertMessage.removeShortlist.description(), preferredStyle: .alert)
        
        let confirmAction = UIAlertAction(title: AlertButton.confirm.description(), style: .default, handler: { (action) in
            DispatchQueue.main.async {
                self.removeUserFromShortlist(itemIndex: index)
            }
        })
        alert.addAction(confirmAction)
        
        let cancelAction = UIAlertAction(title: AlertButton.cancel.description(), style: .default, handler: nil)
        alert.addAction(cancelAction)
        
        self.present(alert, animated: true, completion: nil)
    }

    
    var filteredTutors:[MapItem]?
    var filteredAgencies:[MapItem]?
    func canAddItemToSelection() -> Bool {
         filteredTutors = shoutoutModal.tutors.filter { $0.isSelectedForShoutout }
         filteredAgencies = shoutoutModal.agencies.filter { $0.isSelectedForShoutout }
        return filteredTutors!.count + filteredAgencies!.count < 10
    }
    
//    override func showLoginAlert() {
//        let alert = UIAlertController(title: AppName, message: "You need to login in order to perform this action.".localized, preferredStyle: .alert)
//        let okAction = UIAlertAction(title: AlertButton.ok.description(), style: .default, handler: { (action) in
//            UserStore.shared.isSkipSelected = false
//            self.navigateToLandingScreen()
//        })
//        let laterAction = UIAlertAction(title: AlertButton.later.description(), style: .default, handler: nil)
//        alert.addAction(laterAction)
//        alert.addAction(okAction)
//        self.present(alert, animated: true, completion: nil)
//    }

    
    //DEEPAK
    @IBAction func sendBtnTapped(_ sender: UIButton) {
        
        guard UserStore.shared.isLoggedIn else {
            showLoginAlert()
            return
        }
        
        
        if filteredAgencies != nil ||  filteredTutors != nil  {
            if messageCell != nil && validate(textView: messageCell.messageTextView){
               sendShoutOutMessageToAllSelectedUsers()
            }else{
                self.showSimpleAlertWithMessage("Message box is empty.")
            }
        }else{
            self.showSimpleAlertWithMessage("Select Users to send message")
        }
    }
    
    func validate(textView textView: UITextView) -> Bool {
        guard let text = textView.text,
            !text.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).isEmpty else {
                // this will be reached if the text is nil (unlikely)
                // or if the text only contains white spaces
                // or no text at all
                return false
        }
        
        return true
    }
    
}

extension ShoutoutMainViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return shoutoutModal == nil ? 0 : 4
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 1
        case 1:
            return (shoutoutModal.tutors.count > 0) ? 3 + shoutoutModal.tutors.count : 0
        case 2:
            return (shoutoutModal.agencies.count > 0) ? 3 + shoutoutModal.agencies.count : 0
        default:
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: FilterOptionSectionHeaderTableViewCell.cellIdentifier(), for: indexPath) as! FilterOptionSectionHeaderTableViewCell
            cell.titleLabel.text = "Shout Out".localized
            return cell
        case 1:
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: SectionDescriptionTableViewCell.cellIdentifier(), for: indexPath) as! SectionDescriptionTableViewCell
                cell.messageLabel.text = "We will send your message to up to 10 trainers and tutors nearest to you.\nUnclick to deselect".localized
                return cell
            } else if indexPath.row == 1 {
                let cell = tableView.dequeueReusableCell(withIdentifier: ShoutoutFilterOptionsTableViewCell.cellIdentifier(), for: indexPath) as! ShoutoutFilterOptionsTableViewCell
                cell.delegate = self
                cell.refineButton.tag = 1
                cell.sortButton.tag = 1
                cell.configureCellWithCount(shoutoutModal.tutorCount)
                return cell
            } else if indexPath.row == 2 + shoutoutModal.tutors.count {
                let cell = tableView.dequeueReusableCell(withIdentifier: ShoutoutShowAllButtonTableViewCell.cellIdentifier(), for: indexPath) as! ShoutoutShowAllButtonTableViewCell
                cell.showAllButton.tag = 1
                cell.showAllButton.isHidden = !(shoutoutModal.tutorCount > shoutoutModal.tutors.count)
                cell.delegate = self
                return cell
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: FindTutorProfileTableViewCell.cellIdentifier()) as! FindTutorProfileTableViewCell
                let mapItem = shoutoutModal.tutors[indexPath.row - 2]
                cell.configureWithMapItemForShoutoutScreen(mapItem, atIndex: indexPath.row - 2, isSelected: mapItem.isSelectedForShoutout)
                cell.delegate = self
                return cell
            }
        case 2:
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: SectionDescriptionTableViewCell.cellIdentifier(), for: indexPath) as! SectionDescriptionTableViewCell
                var message = "We also found some Agencies matching your criteria. You may send them the same message too.".localized
                if shoutoutModal.tutors.count == 0 {
                    message = "We couldn't find any tutors matching the criteria. But we have found some Agencies matching your criteria. You may send them the same message too.".localized
                }
                cell.messageLabel.text = message
                return cell
            } else if indexPath.row == 1 {
                let cell = tableView.dequeueReusableCell(withIdentifier: ShoutoutFilterOptionsTableViewCell.cellIdentifier(), for: indexPath) as! ShoutoutFilterOptionsTableViewCell
                cell.delegate = self
                cell.refineButton.tag = 2
                cell.sortButton.tag = 2
                cell.configureCellWithCount(shoutoutModal.agencyCount)
                return cell
            } else if indexPath.row == 2 + shoutoutModal.agencies.count {
                let cell = tableView.dequeueReusableCell(withIdentifier: ShoutoutShowAllButtonTableViewCell.cellIdentifier(), for: indexPath) as! ShoutoutShowAllButtonTableViewCell
                cell.showAllButton.tag = 2
                cell.showAllButton.isHidden = !(shoutoutModal.agencyCount > shoutoutModal.agencies.count)
                cell.delegate = self
                return cell
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: FindAgencyProfileTableViewCell.cellIdentifier()) as! FindAgencyProfileTableViewCell
                let mapItem = shoutoutModal.agencies[indexPath.row - 2]
                cell.configureWithMapItemForShoutoutScreen(mapItem, atIndex: indexPath.row - 2, isSelected: mapItem.isSelectedForShoutout)
                cell.delegate = self
                return cell
            }
        default:
            messageCell = tableView.dequeueReusableCell(withIdentifier: ShoutoutMessageTableViewCell.cellIdentifier(), for: indexPath) as! ShoutoutMessageTableViewCell
            messageCell.messageTextView.text = message
            return messageCell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            tableView.estimatedRowHeight = 81
            return UITableViewAutomaticDimension
        case 1:
            if indexPath.row == 0 {
                tableView.estimatedRowHeight = 81
                return UITableViewAutomaticDimension
            } else if indexPath.row == 1 {
                return ShoutoutFilterOptionsTableViewCell.cellHeight()
            } else if indexPath.row == 2 + shoutoutModal.tutors.count {
                return shoutoutModal.tutorCount > shoutoutModal.tutors.count ? 50.0 : 10.0
            } else {
                return FindTutorProfileTableViewCell.cellHeight()
            }
        case 2:
            if indexPath.row == 0 {
                tableView.estimatedRowHeight = 81
                return UITableViewAutomaticDimension
            } else if indexPath.row == 1 {
                return ShoutoutFilterOptionsTableViewCell.cellHeight()
            } else if indexPath.row == 2 + shoutoutModal.agencies.count {
                return shoutoutModal.agencyCount > shoutoutModal.agencies.count ? 50.0 : 10.0
            } else {
                return FindTutorProfileTableViewCell.cellHeight()
            }
        default:
            return ShoutoutMessageTableViewCell.cellHeight()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 1, indexPath.row > 1 {
            if indexPath.row < shoutoutModal.tutors.count + 2 {
                let tutor = shoutoutModal.tutors[indexPath.row - 2]
                if tutor.isSelectedForShoutout {
                    tutor.isSelectedForShoutout = false
                } else {
                    tutor.isSelectedForShoutout = true
                    guard canAddItemToSelection() else {
                        self.showSimpleAlertWithMessage("Cannot send message to more than 10 profiles at a time".localized)
                        return
                    }
                }
                self.tableView.reloadRows(at: [indexPath], with: .none)
            }
        } else if indexPath.section == 2, indexPath.row > 1 {
            if indexPath.row < shoutoutModal.agencies.count + 2 {
                let agency = shoutoutModal.agencies[indexPath.row - 2]
                if agency.isSelectedForShoutout {
                    agency.isSelectedForShoutout = false
                } else {
                    agency.isSelectedForShoutout = true
                    guard canAddItemToSelection() else {
                        self.showSimpleAlertWithMessage("Cannot send message to more than 10 profiles at a time".localized)
                        return
                    }
                }
                self.tableView.reloadRows(at: [indexPath], with: .none)
            }
        }
    }
    
}

extension ShoutoutMainViewController: ShoutoutShowAllButtonTableViewCellDelegate {

    func showAllButtonTapped(_ button: UIButton) {
        let shoutoutListingVC = ShoutoutListingViewController.instantiateFromAppStoryboard(appStoryboard: .Find)
        let filter = FilterParams.init()
        filter.copyValuesFromObject(filterParams)
        filter.isShoutout = true
        shoutoutListingVC.filterParams = filter
        shoutoutListingVC.currentLocation = self.currentLocation
        shoutoutListingVC.isTutor = button.tag == 1
        shoutoutListingVC.selectedTutorIDs = self.filterSelectedTutorIDs()
        shoutoutListingVC.selectedAgencyIDs = self.filterSelectedAgencyIDs()
        shoutoutListingVC.delegate = self
        self.navigationController?.pushViewController(shoutoutListingVC, animated: true)
    }

}

extension ShoutoutMainViewController: ShoutoutListingViewControllerDelegate {

    func finishedSelectingTutors(tutors: [MapItem], selectedIDs: [String]) {
        for tutor in tutors {
            let index = shoutoutModal.tutors.index { (mapItem) -> Bool in
                return mapItem.ID == tutor.ID
            }
            if let foundIndex = index, foundIndex != -1 {
                // Do nothing
            } else {
                shoutoutModal.tutors.append(tutor)
            }
        }
        for tutor in shoutoutModal.tutors {
            tutor.isSelectedForShoutout = false
        }
        for id in selectedIDs {
            let index = shoutoutModal.tutors.index { (mapItem) -> Bool in
                return mapItem.ID == id
            }
            if let foundIndex = index, foundIndex != -1 {
                shoutoutModal.tutors[foundIndex].isSelectedForShoutout = true
            }
        }
        self.shoutoutModal.tutors.sort { $0.isSelectedForShoutout && !$1.isSelectedForShoutout }
        self.tableView.reloadData()
    }
    
    func finishedSelectingAgencies(agencies: [MapItem], selectedIDs: [AgencyIDInfo]) {
        
        for agency in agencies {
            let index = shoutoutModal.agencies.index { (mapItem) -> Bool in
                return mapItem.ID == agency.ID && mapItem.assetID == agency.assetID
            }
            if let foundIndex = index, foundIndex != -1 {
                // Do nothing
            } else {
                shoutoutModal.agencies.append(agency)
            }
        }
        for agency in shoutoutModal.agencies {
            agency.isSelectedForShoutout = false
        }

        for id in selectedIDs {
            let index = shoutoutModal.agencies.index { (mapItem) -> Bool in
                return mapItem.ID == id.agencyID && mapItem.assetID == id.assetID
            }
            if let foundIndex = index, foundIndex != -1 {
                shoutoutModal.agencies[foundIndex].isSelectedForShoutout = true
            }
        }
        self.shoutoutModal.agencies.sort { $0.isSelectedForShoutout && !$1.isSelectedForShoutout }
        self.tableView.reloadData()
    }
    
}

extension ShoutoutMainViewController: ShoutoutFilterOptionsTableViewCellDelegate {
    
    func filterOptionTappedForSection(_ section: Int) {
        let filterViewController = FilterViewController.instantiateFromAppStoryboard(appStoryboard: .Find)
        let filter = FilterParams.init()
        filter.copyValuesFromObject(filterParams)
        filter.isShoutout = false
        filterViewController.filterParameters = filter
        filterViewController.currentLocation = self.currentLocation
        filterViewController.delegate = self
        self.navigationController?.pushViewController(filterViewController, animated: true)
    }
    
    func sortOptionTappedForSection(_ section: Int) {
        let sortByViewController = SortViewController.instantiateFromAppStoryboard(appStoryboard: .Find)
        let filter = FilterParams.init()
        filter.copyValuesFromObject(filterParams)
        sortByViewController.filterParams = filter
        sortByViewController.delegate = self
        self.navigationController?.pushViewController(sortByViewController, animated: true)
    }
}

extension ShoutoutMainViewController: FilterViewControllerDelegate {
    
    func applyButtonTapped(_ newFilterParams: FilterParams) {
        shouldCallAPI = true
        filterParams.copyValuesFromObject(newFilterParams)
        filterParams.isShoutout = true
    }
    
}

extension ShoutoutMainViewController: SortViewControllerDelegate {
    
    func applySortingWithParams(_ filterParams: FilterParams) {
        self.filterParams.copyValuesFromObject(filterParams)
        shouldCallAPI = true
    }
    
    func resetSortingButtonTapped() {
        self.filterParams.sortBy = .noSort
        shouldCallAPI = true
    }
}

extension ShoutoutMainViewController: FindTutorProfileTableViewCellDelegate {
    
    
    
    func showProfileAtIndex(_ index: Int) {
        let mapItem = shoutoutModal.tutors[index]
        if mapItem.type == "tutor" {
            let tutorProfile = TutorProfileViewController.instantiateFromAppStoryboard(appStoryboard: .Dashboard)
            tutorProfile.tutorID = mapItem.ID
            tutorProfile.delegate = self
            navigationController?.pushViewController(tutorProfile, animated: true)
        } else if mapItem.type == "agency" {
            let agencyProfile = PartnerProfileViewController.instantiateFromAppStoryboard(appStoryboard: .Dashboard)
            agencyProfile.agencyID = mapItem.ID
            agencyProfile.assetID = mapItem.assetID
            navigationController?.pushViewController(agencyProfile, animated: true)
        }
        
    }
    
    func messageProfileAtIndex(_ index: Int) {
        //self.showSimpleAlertWithMessage("Yet to be developed")
        guard UserStore.shared.isLoggedIn else {
            showLoginAlert()
            return
        }
        let mapItem = shoutoutModal.tutors[index]
        let msgPop = self.storyboard?.instantiateViewController(withIdentifier: "MessagePopupVC") as! MessagePopupVC
        let indexPath = IndexPath(row: index + 2, section: 1)
        let cell  = self.tableView.cellForRow(at: indexPath) as? FindTutorProfileTableViewCell
        msgPop.image = cell?.profileImageView.image
        msgPop.name =  "SEND MESSAGE TO " + mapItem.firstName.uppercased()
        msgPop.senderID = mapItem.ID
        var nameArr = [String]()
        for subs in filterParams.selectedSubjects{
           nameArr.append(subs.name)
        }
        msgPop.messages = nameArr.count > 0 ? nameArr : [""]
        // self.view.addSubview(msgPop.view)
        msgPop.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        self.present(msgPop, animated: true, completion: nil)
    }
    
    func shortlistProfileAtIndex(_ index: Int) {
        guard UserStore.shared.isLoggedIn else {
            
            showLoginAlert()
            return
        }
        let mapItem = shoutoutModal.tutors[index]
        if mapItem.isFavorite {
            showDeleteAlertForIndex(index)
        } else {
            showCommentAlertForIndex(index)
        }
    }
    
    func reportProfileAtIndex(_ index: Int) {
        guard UserStore.shared.isLoggedIn else {
            showLoginAlert()
            return
        }
        
        let reportVC = ReportUserViewController(nibName: "ReportUserViewController", bundle: nil)
        reportVC.mapItem = shoutoutModal.tutors[index]
        // Create the dialog
        let popup = PopupDialog(viewController: reportVC,
                                buttonAlignment: .horizontal,
                                transitionStyle: .bounceDown,
                                tapGestureDismissal: true,
                                panGestureDismissal: true)
        
        let buttonOne = CancelButton(title: AlertButton.cancel.description(), height: 60) {
            //Do nothing
        }
        
        
        let buttonTwo = DefaultButton(title: AlertButton.submit.description(), height: 60) {
            self.reportUserWithComment(reportVC.commentsTextView.text ?? "", nature: reportVC.selectedOption, mapItem: reportVC.mapItem)
        }
        
        popup.addButtons([buttonOne, buttonTwo])
        present(popup, animated: true, completion: nil)
    }
    
    
    func selectMarkerForProfileAtIndex(_ index: Int) {
        //Doesnt apply here
    }
    
}

extension ShoutoutMainViewController: FindAgencyProfileTableViewCellDelegate {
    
    func selectMarkerForAgencyAtIndex(_ index: Int) {
        //Doesnt apply here
    }
    
    func showAgencyAtIndex(_ index: Int) {
        
        let mapItem = shoutoutModal.agencies[index]
        let agencyProfile = PartnerProfileViewController.instantiateFromAppStoryboard(appStoryboard: .Dashboard)
        agencyProfile.agencyID = mapItem.ID
        agencyProfile.assetID = mapItem.assetID
        navigationController?.pushViewController(agencyProfile, animated: true)
    }
    
    func enquireAgencyAtIndex(_ index: Int) {
        guard UserStore.shared.isLoggedIn else {
            showLoginAlert()
            return
        }
        
        let enquireVC = EnquireAgencyViewController(nibName: "EnquireAgencyViewController", bundle: nil)
        let mapItem = shoutoutModal.agencies[index]
        enquireVC.mapItem = mapItem

        // Create the dialog
        let popup = PopupDialog(viewController: enquireVC,
                                buttonAlignment: .horizontal,
                                transitionStyle: .bounceDown,
                                tapGestureDismissal: true,
                                panGestureDismissal: true)
        
        let buttonOne = CancelButton(title: AlertButton.cancel.description(), height: 60) {
            //Do nothing
        }
        
        
        let buttonTwo = DefaultButton(title: AlertButton.submit.description(), height: 60) {
            self.enquireAgencyWithMessage(enquireVC.commentTextView.text ?? "", fullName: enquireVC.nameTextField.text ?? "", email: enquireVC.emailTextField.text ?? "", phone: enquireVC.phoneTextField.text ?? "", preferred: enquireVC.selectedOption,
                                          mapItem: enquireVC.mapItem)
            
        }
        
        popup.addButtons([buttonOne, buttonTwo])
        present(popup, animated: true, completion: nil)
    }
    
    func reportAgencyAtIndex(_ index: Int) {
        guard UserStore.shared.isLoggedIn else {
            showLoginAlert()
            return
        }
        
        let reportVC = ReportUserViewController(nibName: "ReportUserViewController", bundle: nil)
        let mapItem = shoutoutModal.agencies[index]
        reportVC.mapItem = mapItem

        // Create the dialog
        let popup = PopupDialog(viewController: reportVC,
                                buttonAlignment: .horizontal,
                                transitionStyle: .bounceDown,
                                tapGestureDismissal: true,
                                panGestureDismissal: true)
        
        let buttonOne = CancelButton(title: AlertButton.cancel.description(), height: 60) {
            //Do nothing
        }
        
        
        let buttonTwo = DefaultButton(title: AlertButton.submit.description(), height: 60) {
            self.reportUserWithComment(reportVC.commentsTextView.text ?? "", nature: reportVC.selectedOption, mapItem: reportVC.mapItem)
        }
        
        popup.addButtons([buttonOne, buttonTwo])
        present(popup, animated: true, completion: nil)
    }
    
}

extension ShoutoutMainViewController: TutorProfileViewControllerDelegate {
    func refreshShortlistDataForTutor() {
        
    }
    
    func refreshDataForTutorWithID(_ ID: String) {
        let mapIndex = shoutoutModal.tutors.index { (item) -> Bool in
            return item.ID == ID
        }
        if let index = mapIndex, index != -1 {
            let currentState = shoutoutModal.tutors[index].isFavorite
            shoutoutModal.tutors[index].isFavorite = !currentState
            tableView.reloadData()
        }
    }
    
}

extension ShoutoutMainViewController {
    
    func addUserToShortlist(comment: String, itemIndex: Int) {
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }
        
        guard let userID = UserStore.shared.userID else {
            return
        }
        let mapItem = shoutoutModal.tutors[itemIndex]
        Utilities.showHUD(forView: self.view, excludeViews: [])
        
        let addToShortlistObserver = ApiManager.shared.apiService.addToShortlistForUser(String(userID), shortlistUserID: mapItem.ID, reason: comment)
        let addToShortlistDisposable = addToShortlistObserver.subscribe(onNext: {(message) in
            Utilities.hideHUD(forView: self.view)
            
            DispatchQueue.main.async {
                NotificationCenter.default.post(name: .refreshDashboardData, object: nil, userInfo:nil)
                self.showSimpleAlertWithMessage(message)
                self.shoutoutModal.tutors[itemIndex].isFavorite = true
                self.tableView.reloadRows(at: [IndexPath.init(row: itemIndex + 1, section: 0)], with: .none)
            }
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.showSimpleAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        })
        addToShortlistDisposable.disposed(by: disposableBag)
    }
    
    func removeUserFromShortlist(itemIndex: Int) {
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }
        
        guard let userID = UserStore.shared.userID else {
            return
        }
        let mapItem = shoutoutModal.tutors[itemIndex]
        Utilities.showHUD(forView: self.view, excludeViews: [])
        
        let deleteShortlistObserver = ApiManager.shared.apiService.deleteShortlistForUser(String(userID), shortlistUserID: mapItem.ID)
        let deleteShortlistDisposable = deleteShortlistObserver.subscribe(onNext: {(message) in
            Utilities.hideHUD(forView: self.view)
            
            DispatchQueue.main.async {
                NotificationCenter.default.post(name: .refreshDashboardData, object: nil, userInfo:nil)
                self.showSimpleAlertWithMessage(message)
                self.shoutoutModal.tutors[itemIndex].isFavorite = false
                self.tableView.reloadRows(at: [IndexPath.init(row: itemIndex + 1, section: 0)], with: .none)
            }
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.showSimpleAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        })
        deleteShortlistDisposable.disposed(by: disposableBag)
    }
    
    func reportUserWithComment(_ comment: String, nature: Int, mapItem: MapItem) {
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }
        
        guard let userID = UserStore.shared.userID else {
            return
        }
        
        var nat = ""
        switch nature {
        case 1:
            nat = "Spam"
        case 2:
            nat = "No Reply"
        default:
            nat = "Improper Conduct"
        }
        
        Utilities.showHUD(forView: self.view, excludeViews: [])
        
        let reportUserObserver = ApiManager.shared.apiService.reportUserWithUserID(mapItem.ID, comment: comment, nature: nat, userID: String(userID))
        let reportUserDisposable = reportUserObserver.subscribe(onNext: {(message) in
            Utilities.hideHUD(forView: self.view)
            
            DispatchQueue.main.async {
                self.showSimpleAlertWithMessage(message)
            }
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.showSimpleAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        })
        reportUserDisposable.disposed(by: disposableBag)
    }
    
    func enquireAgencyWithMessage(_ message: String, fullName: String, email: String, phone: String, preferred: Int, mapItem: MapItem) {
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }
        
        guard let userID = UserStore.shared.userID else {
            return
        }
        
        var preference = ""
        switch preferred {
        case 1:
            preference = "Email"
        default:
            preference = "Phone"
        }
        
        Utilities.showHUD(forView: self.view, excludeViews: [])
        
        let enquireObserver = ApiManager.shared.apiService.enquireAgencyWithID(mapItem.ID, assetID: mapItem.assetID, name: fullName, email: email, phone: phone, preferredContact: preference, message: message, userID: String(userID), courseID: nil)
        let enquireDisposable = enquireObserver.subscribe(onNext: {(message) in
            Utilities.hideHUD(forView: self.view)
            
            DispatchQueue.main.async {
                self.showSimpleAlertWithMessage(message)
            }
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.showSimpleAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        })
        enquireDisposable.disposed(by: disposableBag)
    }
    
    func fetchShoutoutUsers() {
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }

        let filterDict = filterParams.getFilterDictForCurrentLocation(currentLocation, currentCountryID: Utilities.getCurrentCountryCode(), page: nil)
        Utilities.showHUD(forView: self.view, excludeViews: [])

        let shoutoutObserver = ApiManager.shared.apiService.fetchShoutoutUsers(filterDict as [String : AnyObject])
        let shoutoutDisposable = shoutoutObserver.subscribe(onNext: {(shoutoutResponse) in
            Utilities.hideHUD(forView: self.view)

            self.shoutoutModal = shoutoutResponse
            DispatchQueue.main.async(execute: {
                if self.shoutoutModal.tutors.count > 0 || self.shoutoutModal.agencies.count > 0 {
                    self.sendButtonHolderView.isHidden = false
                }
                self.tableView.reloadData()
            })
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.showSimpleAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        })
        shoutoutDisposable.disposed(by: disposableBag)
    }
    
    func sendShoutOutMessageToAllSelectedUsers(){
        var selectedAgenciesDictArr = [[String :AnyObject]]()
        var selecteProfileIDArr = [String]()
        for item in filteredAgencies!{
            let dict = ["id":item.ID!, "asset_id":item.assetID!]
            selectedAgenciesDictArr.append(dict as [String : AnyObject])
        }
        
        for tutors in filteredTutors! {
            selecteProfileIDArr.append(tutors.ID)
        }
        let completeDict = ["sender_id":UserStore.shared.userID!, "receivers":selecteProfileIDArr, "agency":selectedAgenciesDictArr, "message":self.messageCell.messageTextView.text] as [String : AnyObject]
        
        print(completeDict)
        
        
       
        
        let postObserber = ApiManager.shared.apiService.sendShoutoutMessage(params: completeDict)
        let enquireDisposable = postObserber.subscribe(onNext: {(message) in
            Utilities.hideHUD(forView: self.view)
            DispatchQueue.main.async {
                self.showSimpleAlertWithMessage(message)
                let alert = UIAlertController(title: AppName, message: message, preferredStyle: .alert)
                let okAction = UIAlertAction(title: AlertButton.ok.description(), style: .default, handler: { (action) in
                    self.navigationController?.popViewController(animated: true)
                })
                alert.addAction(okAction)
                self.present(alert, animated: true, completion: nil)
                
            }
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.showSimpleAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        })
        enquireDisposable.disposed(by: disposableBag)
    }
    
}
