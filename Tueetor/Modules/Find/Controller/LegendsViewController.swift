
//
//  LegendsViewController.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 28/06/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit

protocol LegendsViewControllerDelegate {
    func legendsSettingApplied(_ newFilterParameters: FilterParams)
}

class LegendsViewController: BaseViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var resetButton: UIButton!
    @IBOutlet weak var applyButton: UIButton!
    @IBOutlet weak var bottomView: UIView!
    
    var filterParameters: FilterParams!
    var delegate: LegendsViewControllerDelegate?
    var isTutorFilter = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        isTutorFilter = filterParameters.isTutorButtonSelected
        resetButton.layer.shadowOffset = CGSize.init(width: 0, height: 5)
        resetButton.layer.shadowRadius = 3
        resetButton.layer.shadowColor = themeBlueColor.cgColor
        resetButton.layer.shadowOpacity = 0.75
        
        applyButton.layer.shadowOffset = CGSize.init(width: 0, height: 5)
        applyButton.layer.shadowRadius = 3
        applyButton.layer.shadowColor = themeBlueColor.cgColor
        applyButton.layer.shadowOpacity = 0.75
        
        bottomView.layer.shadowOffset = CGSize.init(width: 0, height: -5)
        bottomView.layer.shadowRadius = 3
        bottomView.layer.shadowColor = themeBlackColor.cgColor
        bottomView.layer.shadowOpacity = 0.4
        tableView.tableFooterView = UIView()
    }
    
    @IBAction func resetButtonTapped(_ sender: UIButton) {
        filterParameters.showAgencyPin = isTutorFilter
        filterParameters.showStudentSubPin = !isTutorFilter
        filterParameters.showTutorSubPin = isTutorFilter
        filterParameters.showMatchingSubLevelPin = false
        filterParameters.showMatchinSubPin = false
        self.tableView.reloadData()
    }
    
    @IBAction func applyButtonTapped(_ sender: UIButton) {
        if !filterParameters.showAgencyPin,
            !filterParameters.showStudentSubPin,
            !filterParameters.showTutorSubPin,
            !filterParameters.showMatchingSubLevelPin,
            !filterParameters.showMatchinSubPin {
            showSimpleAlertWithMessage("Please select at least one")
            return
        }
        delegate?.legendsSettingApplied(filterParameters)
        navigationController?.popViewController(animated: true)
    }
    
}

extension LegendsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard indexPath.row != 0 else {
            let cell = tableView.dequeueReusableCell(withIdentifier: FilterOptionSectionHeaderTableViewCell.cellIdentifier(), for: indexPath) as! FilterOptionSectionHeaderTableViewCell
            return cell
        }
        
        guard indexPath.row != 5 else {
            let cell = tableView.dequeueReusableCell(withIdentifier: LegendAgencyPinTableViewCell.cellIdentifier(), for: indexPath) as! LegendAgencyPinTableViewCell
            cell.configureCellWithType(filter: filterParameters)
            cell.backgroundColor = UIColor.white
            if !isTutorFilter {
                cell.checkBoxButton.setImage(UIImage.init(named: "greycheckbox"), for: .normal)
            }
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: LegendUserPinTableViewCell.cellIdentifier(), for: indexPath) as! LegendUserPinTableViewCell
        cell.configureCellWithType(indexPath.row - 1, filter: filterParameters)
        cell.backgroundColor = UIColor.white
        if indexPath.row == 1, isTutorFilter {
            cell.checkBoxButton.setImage(UIImage.init(named: "greycheckbox"), for: .normal)
        } else if indexPath.row == 2, !isTutorFilter {
            cell.checkBoxButton.setImage(UIImage.init(named: "greycheckbox"), for: .normal)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        guard indexPath.row != 0 else {
            tableView.estimatedRowHeight = 100.0
            return UITableViewAutomaticDimension
        }
        
        guard indexPath.row != 5 else {
            return LegendAgencyPinTableViewCell.cellHeight()
        }
        return LegendUserPinTableViewCell.cellHeight()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard indexPath.row != 0 else {
            return
        }
        switch indexPath.row - 1 {
        case LegendType.studentSubject.rawValue:
            if isTutorFilter {
                return
            }
            filterParameters.showStudentSubPin = !filterParameters.showStudentSubPin
        case LegendType.tutorSubject.rawValue:
            if !isTutorFilter {
                return
            }
            filterParameters.showTutorSubPin = !filterParameters.showTutorSubPin
        case LegendType.multiMatch.rawValue:
            filterParameters.showMatchingSubLevelPin = !filterParameters.showMatchingSubLevelPin
        case LegendType.subjectMatch.rawValue:
            filterParameters.showMatchinSubPin = !filterParameters.showMatchinSubPin
        case LegendType.agency.rawValue:
            if !isTutorFilter {
                return
            }
            filterParameters.showAgencyPin = !filterParameters.showAgencyPin
        default:
            break
        }
        self.tableView.reloadData()
    }
    
    
}
