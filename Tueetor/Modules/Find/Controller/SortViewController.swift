//
//  SortViewController.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 31/08/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit

protocol SortViewControllerDelegate {
    func resetSortingButtonTapped()
    func applySortingWithParams(_ filterParams: FilterParams)
}


class SortViewController: BaseViewController {

    @IBOutlet weak var resetButton: UIButton!
    @IBOutlet weak var applyButton: UIButton!
    @IBOutlet weak var buttonHolderView: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    var filterParams: FilterParams!
    var delegate: SortViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        buttonHolderView.layer.shadowPath = UIBezierPath(roundedRect:
            buttonHolderView.bounds, cornerRadius: self.buttonHolderView.layer.cornerRadius).cgPath
        buttonHolderView.layer.shadowColor = themeBlackColor.cgColor
        buttonHolderView.layer.shadowOffset = CGSize(width: 0, height: -2)
        buttonHolderView.layer.shadowRadius = 5
        buttonHolderView.layer.masksToBounds = false
        buttonHolderView.layer.shadowOpacity = 0.4
        
        resetButton.layer.shadowPath = UIBezierPath(roundedRect:
            resetButton.bounds, cornerRadius: self.resetButton.layer.cornerRadius).cgPath
        resetButton.layer.shadowColor = themeBlueColor.cgColor
        resetButton.layer.shadowOffset = CGSize(width: 0, height: 0)
        resetButton.layer.shadowRadius = 5
        resetButton.layer.masksToBounds = false
        resetButton.layer.shadowOpacity = 0.4

        applyButton.layer.shadowPath = UIBezierPath(roundedRect:
            applyButton.bounds, cornerRadius: self.applyButton.layer.cornerRadius).cgPath
        applyButton.layer.shadowColor = themeBlueColor.cgColor
        applyButton.layer.shadowOffset = CGSize(width: 0, height: 0)
        applyButton.layer.shadowRadius = 5
        applyButton.layer.masksToBounds = false
        applyButton.layer.shadowOpacity = 0.4

        tableView.tableFooterView = UIView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func resetButtonTapped(_ sender: UIButton) {
        filterParams.sortBy = .noSort
        self.tableView.reloadData()
        delegate?.resetSortingButtonTapped()
    }
    
    @IBAction func applyButtonTapped(_ sender: UIButton) {
        delegate?.applySortingWithParams(filterParams)
        navigateBack(sender: sender)
    }
}

extension SortViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 7
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: FilterOptionSectionHeaderTableViewCell.cellIdentifier(), for: indexPath) as! FilterOptionSectionHeaderTableViewCell
            cell.titleLabel.text = "Sort Trainers".localized
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: SortTableViewCell.cellIdentifier(), for: indexPath) as! SortTableViewCell
            cell.configureCellForIndexPath(indexPath, filterParams: filterParams)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            tableView.estimatedRowHeight = 81
            return UITableViewAutomaticDimension
        } else {
            return SortTableViewCell.cellHeight()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard indexPath.row != 0 else {
            return
        }
        
        switch indexPath.row {
        case 1:
            if filterParams.sortBy == .qualifi_asc {
                filterParams.sortBy = .qualifi_desc
            } else if filterParams.sortBy == .qualifi_desc {
                filterParams.sortBy = .noSort
            } else {
                filterParams.sortBy = .qualifi_asc
            }
            break
        case 2:
            if filterParams.sortBy == .exp_asc {
                filterParams.sortBy = .exp_desc
            } else if filterParams.sortBy == .exp_desc {
                filterParams.sortBy = .noSort
            } else {
                filterParams.sortBy = .exp_asc
            }
            break
        case 3:
            if filterParams.sortBy == .session_asc {
                filterParams.sortBy = .session_desc
            } else if filterParams.sortBy == .session_desc {
                filterParams.sortBy = .noSort
            } else {
                filterParams.sortBy = .session_asc
            }
            break
        case 4:
            if filterParams.sortBy == .month_asc {
                filterParams.sortBy = .month_desc
            } else if filterParams.sortBy == .month_desc {
                filterParams.sortBy = .noSort
            } else {
                filterParams.sortBy = .month_asc
            }
            break
        case 5:
            if filterParams.sortBy == .dist_asc {
                filterParams.sortBy = .dist_desc
            } else if filterParams.sortBy == .dist_desc {
                filterParams.sortBy = .noSort
            } else {
                filterParams.sortBy = .dist_asc
            }
            break
        case 6:
            if filterParams.sortBy == .rating_asc {
                filterParams.sortBy = .rating_desc
            } else if filterParams.sortBy == .rating_desc {
                filterParams.sortBy = .noSort
            } else {
                filterParams.sortBy = .rating_asc
            }
            break
        default:
            break
        }
        tableView.reloadData()
    }
    
}
