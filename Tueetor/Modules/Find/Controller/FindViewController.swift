//
//  FindViewController.swift
//  Tueetor
//
//  Created by Phaninder on 08/04/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit
import GooglePlaces
import GoogleMaps
import RxSwift

class FindViewController: BaseViewController, BaseTabBarViewControllerDelegate {
    
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var slidingViewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var mapViewBottomConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var shoutoutImageView: UIImageView!
    @IBOutlet weak var findNowImageView: UIImageView!
    @IBOutlet weak var legendImageView: UIImageView!
    @IBOutlet weak var findNowButton: UIButton!
    @IBOutlet weak var legendButton: UIButton!
    @IBOutlet weak var shoutoutButton: UIButton!
    @IBOutlet weak var showAgenciesCheckBox: UIButton!
    
    @IBOutlet weak var backButtonHolderView: UIView!
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var backButtonWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var searchBarHorizontalSpacing: NSLayoutConstraint!
    @IBOutlet weak var searchTextField: UITextField!
    
    
    //    var mapView: GMSMapView!
    private var clusterManager: GMUClusterManager!
    var mapItemPagination = MapItemPagination()
    var disposableBag = DisposeBag()
    var findDisposable: Disposable!
    var isFirstTime = true
    var currentLocation: CLLocationCoordinate2D!
    var isLocationAllowed = false
    var isFetchingData = false
    private let locationManager = CLLocationManager()
    var filterParams: FilterParams!
    var isSearchActive = false
    var shouldCallAPI = false
    var currentCountryID = ""
    var mapPadding: CGFloat = 60.0
    
    var shouldAddItems = false
    var shouldReloadMap = false
    var mapItemIndex = -1
    var isPartOfTabbar = true
    var ignoreScroll = false
    var showOnlyAgencies = false
    var receiverID: String?
    
    @IBOutlet var panGestureRecognizer: UIPanGestureRecognizer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if filterParams == nil {
            filterParams = FilterParams.init()
        }
        
        (self.tabBarController as! BaseTabBarViewController).baseTabBarControllerDelegate = self
        locationManager.delegate = self
        slidingViewTopConstraint.constant = ScreenHeight/2 - 50
        let height: CGFloat = UIDevice.isIphoneX ? 95 : 63
        configureButtons()
        mapViewBottomConstraint.constant = isPartOfTabbar ? (ScreenHeight/2 + 44 - height) : (ScreenHeight/2 + 30)
        if isPartOfTabbar {
            backButtonWidthConstraint.constant = 0
            searchBarHorizontalSpacing.constant = 0
        }
        
        let iconGenerator = GMUDefaultClusterIconGenerator()
        let algorithm = GMUNonHierarchicalDistanceBasedAlgorithm()
        let renderer = GMUDefaultClusterRenderer(mapView: mapView,
                                                 clusterIconGenerator: iconGenerator)
        renderer.delegate = self
        clusterManager = GMUClusterManager(map: mapView, algorithm: algorithm,
                                           renderer: renderer)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.shared.statusBarStyle = .default
        (self.tabBarController as! BaseTabBarViewController).tabBar.isHidden = false
        (self.tabBarController as! BaseTabBarViewController).showCenterButton()
        if isPartOfTabbar == false {
            (self.tabBarController as! BaseTabBarViewController).tabBar.isHidden = true
            (self.tabBarController as! BaseTabBarViewController).hideCenterButton()
            self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        } else {
            (self.tabBarController as! BaseTabBarViewController).tabBar.isHidden = false
            (self.tabBarController as! BaseTabBarViewController).showCenterButton()
        }
        if currentLocation == nil {
            checkLocationAuthorizationStatus()
        }
        configureSearchBar()
        shouldShowOnlyAgencies()
        if shouldCallAPI {
            isFirstTime = true
            mapItemPagination.paginationType = .new
            filterUsers()
            shouldCallAPI = false
        }
    }
    
    func configureSearchBar() {
        var subjectNames = [String]()
        for subject in filterParams.selectedSubjects {
            subjectNames.append(subject.name)
        }
        searchTextField.text = subjectNames.joined(separator: ", ")
    }
    
    func shouldShowOnlyAgencies() {
        showOnlyAgencies = self.filterParams.showAgencyPin && !self.filterParams.showTutorSubPin && !self.filterParams.showMatchinSubPin && !self.filterParams.showStudentSubPin && !self.filterParams.showMatchingSubLevelPin
        showAgenciesCheckBox.setImage(UIImage(named: showOnlyAgencies ? "radioChecked" : "checkbox_empty"), for: .normal)
    }
   
    @IBAction func selectSubjectButtonTapped(_ sender: UIButton) {
        let filterSelectionViewController = FilterCateogrySelectionViewController.instantiateFromAppStoryboard(appStoryboard: .Find)
        (self.tabBarController as! BaseTabBarViewController).tabBar.isHidden = true
        (self.tabBarController as! BaseTabBarViewController).hideCenterButton()

        filterSelectionViewController.delegate = self
        filterSelectionViewController.screenType = .subject
        if filterParams.selectedSubjects.count > 0 {
            filterSelectionViewController.selectedSubjectsArray = filterParams.selectedSubjects
        }
        filterSelectionViewController.isMultiSelect = true
        navigationController?.pushViewController(filterSelectionViewController, animated: true)
    }
    
    
    
    @IBAction func handlePanGesture(_ sender: UIPanGestureRecognizer) {
        let translation = sender.location(in: self.view)
        print(translation)
        
        if translation.y < 0 || translation.y > (ScreenHeight/2 - 50) {
            return
        }
        print("panned")
        
        let vel = sender.velocity(in: self.view)
        let upwardDirection = vel.y < 0
        print("state: ", sender.state.rawValue)
        if sender.state == .changed || sender.state == .began {
                self.slidingViewTopConstraint.constant = translation.y
                self.view.layoutIfNeeded()
        } else if sender.state == .ended {
            
            if upwardDirection {
                UIView.animate(withDuration: 0.1) {
                    self.slidingViewTopConstraint.constant = 0
                    self.view.layoutIfNeeded()
                }
            } else {
                UIView.animate(withDuration: 0.1) {
                    self.slidingViewTopConstraint.constant = ScreenHeight/2 - 50
                    self.view.layoutIfNeeded()
                }
            }
        }
    }
    
    @IBAction func shoutoutButtonTapped(_ sender: UIButton) {
        let shoutoutMainVC = ShoutoutMainViewController.instantiateFromAppStoryboard(appStoryboard: .Find)
        let filter = FilterParams.init()
        filter.copyValuesFromObject(filterParams)
        filter.isShoutout = true
        shoutoutMainVC.filterParams = filter
        shoutoutMainVC.currentLocation = self.currentLocation
        self.navigationController?.pushViewController(shoutoutMainVC, animated: true)
    }
    
    @IBAction func findNowButtonTapped(_ sender: UIButton) {
        self.view.endEditing(true)
        let filterViewController = FilterViewController.instantiateFromAppStoryboard(appStoryboard: .Find)
        (self.tabBarController as! BaseTabBarViewController).tabBar.isHidden = true
        (self.tabBarController as! BaseTabBarViewController).hideCenterButton()
        let filter = FilterParams.init()
        filter.copyValuesFromObject(filterParams)
        filterViewController.filterParameters = filter
        filterViewController.currentLocation = self.currentLocation
        filterViewController.delegate = self
        self.navigationController?.pushViewController(filterViewController, animated: true)
    }
    
    @IBAction func legendsButtonTapped(_ sender: UIButton) {
        self.view.endEditing(true)
        let legendsViewController = LegendsViewController.instantiateFromAppStoryboard(appStoryboard: .Find)
        (self.tabBarController as! BaseTabBarViewController).tabBar.isHidden = true
        (self.tabBarController as! BaseTabBarViewController).hideCenterButton()
        let filter = FilterParams.init()
        filter.copyValuesFromObject(filterParams)
        legendsViewController.filterParameters = filter
        legendsViewController.delegate = self
        self.navigationController?.pushViewController(legendsViewController, animated: true)
    }
    
    @IBAction func showAgenciesButtonTapped(_ sender: UIButton) {
        guard self.filterParams.isTutorButtonSelected else {
            return
        }
        
        guard !self.filterParams.isFeatured else {
            return
        }
        
        if showOnlyAgencies {
            self.filterParams.showTutorSubPin = true
        } else {
            self.filterParams.showTutorSubPin = false
            self.filterParams.showAgencyPin = true
            self.filterParams.showMatchinSubPin = false
            self.filterParams.showMatchingSubLevelPin = false
        }
        shouldShowOnlyAgencies()
        mapItemPagination.paginationType = .new
        isFirstTime = true
        filterUsers()
    }
    
    func callAPIAccordingly() {
        currentCountryID = Utilities.getCurrentCountryCode()
        filterUsers()
    }
    
    func checkLocationAuthorizationStatus() {
        
        if CLLocationManager.authorizationStatus() == .denied {
            let alert = UIAlertController(title: "Location Permission".localized, message: "Please grant location permission to find resources nearby.".localized, preferredStyle: .alert)
            let okAction = UIAlertAction(title: AlertButton.ok.description(), style: .default) { (action) in
                self.navigateToSettingsPage()
            }
            let laterAction = UIAlertAction(title: AlertButton.later.description(), style: .cancel, handler: nil)
            alert.addAction(okAction)
            alert.addAction(laterAction)
            self.present(alert, animated: true, completion: nil)
        } else {
            locationManager.requestWhenInUseAuthorization()
        }
    }

    func populateMapWithData() {
        if isLocationAllowed {
            mapView.isMyLocationEnabled = true
        }
        if shouldReloadMap {
            shouldReloadMap = false
            if mapView == nil {
                mapView.clear()
            }
            self.clusterManager.clearItems()
            self.clusterManager.add(self.mapItemPagination.mapItems)
            self.clusterManager.cluster()
            
            clusterManager.setDelegate(self, mapDelegate: self)
            
            let path = GMSMutablePath()
            for item in mapItemPagination.mapItems {
                path.add(item.coordinate)
            }
            
            if mapView.selectedMarker != nil {
                //Do nothing
            } else if let lat = filterParams.latitude, let long = filterParams.longitude {
                path.add(CLLocationCoordinate2D(latitude: lat, longitude: long))
                
                mapView.camera = GMSCameraPosition(target: CLLocationCoordinate2D(latitude: lat, longitude: long), zoom: 15, bearing: 0, viewingAngle: 0)
            } else if let coordinate = currentLocation, let _ = mapView {
                path.add(coordinate)
                mapView.camera = GMSCameraPosition(target: coordinate, zoom: 15, bearing: 0, viewingAngle: 0)
            }
            
            
            let bounds = GMSCoordinateBounds(path: path)
            mapPadding = (mapPadding == 60.0) ? 61.0 : 60.0
            mapView.animate(with: .fit(bounds, withPadding: mapPadding))
        }
        
    }
    
    
    //MARK: CustomTabBarControllerDelegate Methods
    
    func tabBarViewControllerCenterButtonTapped(tabBarController: BaseTabBarViewController, button: UIButton, buttonState: Bool) {
    }
    
    
    @IBAction func valueChangedInTextField(_ sender: UITextField) {
        if (findDisposable != nil) {
            Utilities.hideHUD(forView: view)
            findDisposable.dispose()
        }
         mapItemPagination.paginationType = .new
        filterUsers()
    }
    
    func showCommentAlertForIndex(_ index: Int) {
        let alertController = UIAlertController(title: AppName,
                                                message: AlertMessage.newShortlistComment.description(),
                                                preferredStyle: .alert)
        
        
        let submitAction = UIAlertAction(title: AlertButton.submit.description(),
                                         style: .default) { [weak alertController] _ in
                                            guard let alertController = alertController, let textField = alertController.textFields?.first else { return }
                                            DispatchQueue.main.async {
                                                print(textField.text!)
                                                self.addUserToShortlist(comment: textField.text!, itemIndex: index)
                                            }
        }
        submitAction.isEnabled = false
        alertController.addAction(submitAction)
        
        alertController.addTextField { textField in
            textField.placeholder = "New comment".localized
            
            NotificationCenter.default.addObserver(forName: NSNotification.Name.UITextFieldTextDidChange, object: textField, queue: OperationQueue.main, using: { (notification) in
                submitAction.isEnabled = !textField.text!.isEmpty
            })
        }
        
        let cancelAction = UIAlertAction(title: AlertButton.cancel.description(), style: .cancel, handler: nil)
        alertController.addAction(cancelAction)
        present(alertController, animated: true, completion: nil)
    }
    
    func showDeleteAlertForIndex(_ index: Int) {
        let alert = UIAlertController(title: AppName, message: AlertMessage.removeShortlist.description(), preferredStyle: .alert)
        
        let confirmAction = UIAlertAction(title: AlertButton.confirm.description(), style: .default, handler: { (action) in
            DispatchQueue.main.async {
                self.removeUserFromShortlist(itemIndex: index)
            }
        })
        alert.addAction(confirmAction)
        
        let cancelAction = UIAlertAction(title: AlertButton.cancel.description(), style: .default, handler: nil)
        alert.addAction(cancelAction)
        
        self.present(alert, animated: true, completion: nil)
    }

    
    func showPinInfoViewAtIndex(_ index: Int) {
        ignoreScroll = true
        self.tableView.scrollToRow(at: IndexPath.init(row: index, section: 0), at: .top, animated: false)
        UIView.animate(withDuration: 0.2, animations: {
            self.slidingViewTopConstraint.constant = ScreenHeight/2 - 50
            self.view.layoutIfNeeded()
        }) { (completed) in
            if completed {
                self.ignoreScroll = false
            }
        }

        let selectedProfile = mapItemPagination.mapItems[index]
        if let renderer = self.clusterManager.clusterRenderedOfCluster() as? GMUDefaultClusterRenderer {
            let markers = renderer.markers as! [GMSMarker]
            
            if markers.count > 0 ,
                let testMarker = markers[safe: 0],
                let _ = testMarker.userData as? MapItem {
                let mapIndex = markers.index { (marker) -> Bool in
                    if let mapItem = marker.userData as? MapItem {
                        return mapItem.ID == selectedProfile.ID
                    } else {
                        return false
                    }
                }
                if let _mapIndex = mapIndex, _mapIndex != -1 {
                    mapItemIndex = -1
                    let marker = markers[_mapIndex]
                    var point = mapView.projection.point(for: marker.position)
                    point.y = point.y - 110
                    let camera = GMSCameraUpdate.setTarget(mapView.projection.coordinate(for: point))
                    mapView.animate(with: camera)
                    mapView.selectedMarker = marker
                } else {
                    let point = mapView.projection.point(for: selectedProfile.coordinate)
                    let camera = GMSCameraUpdate.setTarget(mapView.projection.coordinate(for: point), zoom: 15)
                    mapItemIndex = index
                    mapView.animate(with: camera)
                }
            } else {
                let point = mapView.projection.point(for: selectedProfile.coordinate)
                let camera = GMSCameraUpdate.setTarget(mapView.projection.coordinate(for: point), zoom: 15)
                mapItemIndex = index
                mapView.animate(with: camera)
            }
        }

    }
    
    func configureButtons() {
        shoutoutButton.addTarget(self, action: #selector(shoutOutHoldRelease), for: UIControlEvents.touchUpInside)
        shoutoutButton.addTarget(self, action: #selector(shoutOutHoldRelease), for: UIControlEvents.touchDragExit);
        shoutoutButton.addTarget(self, action: #selector(shoutOutHoldDown), for: UIControlEvents.touchDown)
        
        legendButton.addTarget(self, action: #selector(legendHoldRelease), for: UIControlEvents.touchUpInside)
        legendButton.addTarget(self, action: #selector(legendHoldRelease), for: UIControlEvents.touchDragExit);
        legendButton.addTarget(self, action: #selector(legendHoldDown), for: UIControlEvents.touchDown)
        
        findNowButton.addTarget(self, action: #selector(findNowHoldRelease), for: UIControlEvents.touchUpInside)
        findNowButton.addTarget(self, action: #selector(findNowHoldRelease), for: UIControlEvents.touchDragExit);
        findNowButton.addTarget(self, action: #selector(findNowHoldDown), for: UIControlEvents.touchDown)
    }
    
    @objc func shoutOutHoldDown() {
        shoutoutImageView.image = UIImage(named: "shoutout_active")
    }
    
    @objc func shoutOutHoldRelease() {
        shoutoutImageView.image = UIImage(named: "shoutout_inactive")
    }
    
    @objc func findNowHoldDown() {
        findNowImageView.image = UIImage(named: "findnow_active")
    }
    
    @objc func findNowHoldRelease() {
        findNowImageView.image = UIImage(named: "findnow_inactive")
    }
    
    @objc func legendHoldDown() {
        legendImageView.image = UIImage(named: "legends_active")
    }
    
    @objc func legendHoldRelease() {
        legendImageView.image = UIImage(named: "legends_inactive")
    }

}

extension FindViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.tableView.isHidden {
            return 0
        } else {
            return mapItemPagination.mapItems.count + 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard indexPath.row != mapItemPagination.mapItems.count else {
            if mapItemPagination.mapItems.count == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: EmptyMessageTableViewCell.cellIdentifier(), for: indexPath) as! EmptyMessageTableViewCell
                cell.messageLabel.text = "No users are present with the given data. Please try applying a different filter".localized
                return cell
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: LoadMoreTableViewCell.cellIdentifier(), for: indexPath) as! LoadMoreTableViewCell
                cell.activityIndicator.startAnimating()
                return cell
            }
        }

        let mapItem = mapItemPagination.mapItems[indexPath.row]
        if mapItem.type == "agency" {
            let cell = tableView.dequeueReusableCell(withIdentifier: FindAgencyProfileTableViewCell.cellIdentifier()) as! FindAgencyProfileTableViewCell
            cell.configureWithMapItem(mapItemPagination.mapItems[indexPath.row], atIndex: indexPath.row)
            cell.delegate = self
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: FindTutorProfileTableViewCell.cellIdentifier()) as! FindTutorProfileTableViewCell
            cell.configureWithMapItem(mapItemPagination.mapItems[indexPath.row], atIndex: indexPath.row)
            cell.delegate = self
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == mapItemPagination.mapItems.count{
            if mapItemPagination.mapItems.count == 0 {
                tableView.estimatedRowHeight = 60.0
                return UITableViewAutomaticDimension
            }
            return mapItemPagination.hasMoreToLoad ? 50.0 : 0
        }
        return FindTutorProfileTableViewCell.cellHeight()
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == mapItemPagination.mapItems.count && self.mapItemPagination.hasMoreToLoad && !isFetchingData {
            filterUsers()
        }
    }

}

extension FindViewController: GMUClusterManagerDelegate, GMSMapViewDelegate, GMUClusterRendererDelegate {
    
    func mapView(_ mapView: GMSMapView, markerInfoContents marker: GMSMarker) -> UIView? {
        marker.tracksInfoWindowChanges = true
        guard let mapItem = marker.userData as? MapItem else {
            return nil
        }
        guard let infoView = UIView.viewFromNibName("TutorMarkerInfoView") as? TutorMarkerInfoView else {
            return nil
        }
        infoView.profileImageView.sd_setShowActivityIndicatorView(true)
        infoView.profileImageView.sd_setIndicatorStyle(.gray)
        infoView.profileImageView.sd_setImage(with: URL(string: mapItem.imageURL), placeholderImage: UIImage(named: "profile_placeholder"), options: .retryFailed) { (image, error, cacheType, url) in
            if error == nil {
                //Do nothing
            }
        }
        
        infoView.nameLabel.text = mapItem.name
        infoView.distanceLabel.text = mapItem.distance
        infoView.ratePerSessionLabel.text = mapItem.pricePerSession
        infoView.ratePerMonthLabel.text = mapItem.pricePerMonth
        infoView.responseTimeLabel.text = mapItem.responseTime
        if let n = NumberFormatter().number(from: mapItem.rating) {
            infoView.ratingView.value = CGFloat(truncating: n)
        }
        infoView.ratingView.isHidden = mapItem.type != "tutor"
        infoView.responseTimeLabel.isHidden = mapItem.type != "tutor"
        infoView.clockIcon.isHidden = mapItem.type != "tutor"
        return infoView
    }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        if let _ = marker.userData as? MapItem {
            var point = mapView.projection.point(for: marker.position)
            point.y = point.y - 110
            
            let camera = GMSCameraUpdate.setTarget(mapView.projection.coordinate(for: point))
            mapView.animate(with: camera)
            mapView.selectedMarker = marker
            return true
        } else {
            mapView.selectedMarker = marker
            return true
        }
    }
    
    func mapView(_ mapView: GMSMapView, didTapInfoWindowOf marker: GMSMarker) {
        guard let mapItem = marker.userData as? MapItem else {
            return
        }
        
        if mapItem.type == "tutor" {
            let tutorProfile = TutorProfileViewController.instantiateFromAppStoryboard(appStoryboard: .Dashboard)
            tutorProfile.tutorID = mapItem.ID
            tutorProfile.delegate = self
            navigationController?.pushViewController(tutorProfile, animated: true)
        } else if mapItem.type == "student" {
            let studentProfile = StudentProfileViewController.instantiateFromAppStoryboard(appStoryboard: .Dashboard)
            studentProfile.studentID = mapItem.ID
            studentProfile.delegate = self
            navigationController?.pushViewController(studentProfile, animated: true)
        } else if mapItem.type == "agency" {
            let agencyProfile = PartnerProfileViewController.instantiateFromAppStoryboard(appStoryboard: .Dashboard)
            agencyProfile.agencyID = mapItem.ID
            agencyProfile.assetID = mapItem.assetID
            navigationController?.pushViewController(agencyProfile, animated: true)
        }
    }
    
    func didTapMyLocationButton(for mapView: GMSMapView) -> Bool {
        mapView.selectedMarker = nil
        return false
    }
    
    func clusterManager(_ clusterManager: GMUClusterManager, didTap cluster: GMUCluster) -> Bool {
        let newCamera = GMSCameraPosition.camera(withTarget: cluster.position,
                                                 zoom: mapView.camera.zoom + 1)
        let update = GMSCameraUpdate.setCamera(newCamera)
        mapView.moveCamera(update)
        return true
    }
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        guard mapItemIndex != -1 else {
            return
        }
        if let renderer = self.clusterManager.clusterRenderedOfCluster() as? GMUDefaultClusterRenderer {
            let markers = renderer.markers as! [GMSMarker]
            
            let selectedProfile = mapItemPagination.mapItems[mapItemIndex]
            if markers.count > 0 ,
            let testMarker = markers[safe: 0],
                let _ = testMarker.userData as? MapItem {
                let mapIndex = markers.index { (marker) -> Bool in
                    if let mapItem = marker.userData as? MapItem {
                        return mapItem.ID == selectedProfile.ID
                    } else {
                        return false
                    }
                }
                if let _mapIndex = mapIndex, _mapIndex != -1 {
                    mapItemIndex = -1
                    let marker = markers[_mapIndex]
                    var point = mapView.projection.point(for: marker.position)
                    point.y = point.y - 110
                    let camera = GMSCameraUpdate.setTarget(mapView.projection.coordinate(for: point))
                    mapView.animate(with: camera)
                    mapView.selectedMarker = marker
                } else {
                    mapItemIndex = -1
                    var point = mapView.projection.point(for: selectedProfile.coordinate)
                    point.y = point.y - 110
                    
                    let camera = GMSCameraUpdate.setTarget(mapView.projection.coordinate(for: point))
                    mapView.animate(with: camera)
                }
            }
        }
    }
    
    func renderer(_ renderer: GMUClusterRenderer, didRenderMarker marker: GMSMarker) {
        guard mapItemIndex != -1 else {
            return
        }
        guard let mapItem = marker.userData as? MapItem else {
            return
        }
        let selectedProfile = mapItemPagination.mapItems[mapItemIndex]
        if mapItem.ID == selectedProfile.ID {
            mapItemIndex = -1
            var point = mapView.projection.point(for: marker.position)
            point.y = point.y - 110
            mapView.selectedMarker = marker
            let camera = GMSCameraUpdate.setTarget(mapView.projection.coordinate(for: point))
            mapView.animate(with: camera)
        }
    }
    
    
}

extension FindViewController: CLLocationManagerDelegate {

    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        guard status == .authorizedWhenInUse else {
            isLocationAllowed = false
            
            return
        }
        isLocationAllowed = true
        locationManager.startUpdatingLocation()
        
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.first else {
            return
        }
        guard currentLocation == nil else {
            return
        }
        currentLocation = location.coordinate
        callAPIAccordingly()
        self.tableView.reloadData()
        locationManager.stopUpdatingLocation()
    }
}

extension FindViewController: MapTableViewCellDelegate {
    
    func showShoutoutScreen() {
        
    }
    
    func showFindNowScreen() {
    }
    
    func showLegendScreen() {
    }
    
}

extension FindViewController: FilterViewControllerDelegate {
    
    func applyButtonTapped(_ newFilterParams: FilterParams) {
        shouldCallAPI = true
        filterParams.copyValuesFromObject(newFilterParams)
        DispatchQueue.main.async {
            self.configureSearchBar()
        }
    }
    
}

extension FindViewController: LegendsViewControllerDelegate {

    func legendsSettingApplied(_ newFilterParameters: FilterParams) {
        shouldCallAPI = true
        filterParams.copyValuesFromObject(newFilterParameters)
        DispatchQueue.main.async {
            self.configureSearchBar()
        }
    }
    
}

extension FindViewController: FindTutorProfileTableViewCellDelegate {
    
    func showProfileAtIndex(_ index: Int) {
        let mapItem = mapItemPagination.mapItems[index]
        if mapItem.type == "tutor" {
            let tutorProfile = TutorProfileViewController.instantiateFromAppStoryboard(appStoryboard: .Dashboard)
            tutorProfile.tutorID = mapItem.ID
            tutorProfile.delegate = self
            navigationController?.pushViewController(tutorProfile, animated: true)
        } else if mapItem.type == "student" {
            let studentProfile = StudentProfileViewController.instantiateFromAppStoryboard(appStoryboard: .Dashboard)
            studentProfile.studentID = mapItem.ID
            studentProfile.delegate = self
            navigationController?.pushViewController(studentProfile, animated: true)
        } else if mapItem.type == "agency" {
            let agencyProfile = PartnerProfileViewController.instantiateFromAppStoryboard(appStoryboard: .Dashboard)
            agencyProfile.agencyID = mapItem.ID
            agencyProfile.assetID = mapItem.assetID
            navigationController?.pushViewController(agencyProfile, animated: true)
        }

    }
    
    func messageProfileAtIndex(_ index: Int) {
        //self.showSimpleAlertWithMessage("Yet to be developed")
        guard UserStore.shared.isLoggedIn else {
            showLoginAlert()
            return
        }
 
//        TODO
        let msgPop = self.storyboard?.instantiateViewController(withIdentifier: "MessagePopupVC") as! MessagePopupVC
        let indexPath = IndexPath(row: index, section: 0)
        let cell  = self.tableView.cellForRow(at: indexPath) as? FindTutorProfileTableViewCell
        msgPop.image = cell?.profileImageView.image
        msgPop.name =  "SEND MESSAGE TO " + (cell!.nameLabel.text!).uppercased()
        msgPop.senderID = mapItemPagination.mapItems[index].ID
        receiverID = mapItemPagination.mapItems[index].ID ?? ""
       
        let popup = PopupDialog(viewController: msgPop,
                                buttonAlignment: .horizontal,
                                transitionStyle: .bounceDown,
                                tapGestureDismissal: true,
                                panGestureDismissal: true)
        
        let buttonOne = CancelButton(title: (AlertButton.cancel.description()).uppercased(), height: 60) {
            //Do nothing
        }
        
        
        let buttonTwo = DefaultButton(title: AlertButton.sendMessage.description(), height: 60) {
            
            print("helo")
            let templateMsg = msgPop.tvMessage.text ?? ""
            self.sendMessage(templateMsg)
            
        }
        popup.addButtons([buttonOne, buttonTwo])
        present(popup, animated: true, completion: nil)
        
        
    }
    
    func shortlistProfileAtIndex(_ index: Int) {
        guard UserStore.shared.isLoggedIn else {
            
            showLoginAlert()
            return
        }
        let mapItem = mapItemPagination.mapItems[index]
        if mapItem.isFavorite {
            showDeleteAlertForIndex(index)
        } else {
            showCommentAlertForIndex(index)
        }
    }
    
    func reportProfileAtIndex(_ index: Int) {
        guard UserStore.shared.isLoggedIn else {
            showLoginAlert()
            return
        }

        let reportVC = ReportUserViewController(nibName: "ReportUserViewController", bundle: nil)
        reportVC.mapItem = mapItemPagination.mapItems[index]
        // Create the dialog
        let popup = PopupDialog(viewController: reportVC,
                                buttonAlignment: .horizontal,
                                transitionStyle: .bounceDown,
                                tapGestureDismissal: true,
                                panGestureDismissal: true)
        
        let buttonOne = CancelButton(title: AlertButton.cancel.description(), height: 60) {
            //Do nothing
        }
        
        
        let buttonTwo = DefaultButton(title: AlertButton.submit.description(), height: 60) {
            self.reportUserWithComment(reportVC.commentsTextView.text ?? "", nature: reportVC.selectedOption, mapItem: reportVC.mapItem)
        }
        
        
        popup.addButtons([buttonOne, buttonTwo])
        present(popup, animated: true, completion: nil)
    }
    
    func showAlertWithMessage(_ message: String) {
        let alert = UIAlertController(title: AppName, message: message, preferredStyle: .alert)
        let okButton = UIAlertAction(title: AlertButton.ok.description(), style: .default, handler: nil)
        alert.addAction(okButton)
        self.present(alert, animated: true, completion: nil)
    }
    
    //---- trigger send button in MessagePopVC
    @objc func sendMessage(_ message: String) {
        guard Utilities.shared.isNetworkReachable() else {
            self.showAlertWithMessage(AlertMessage.noInternetConnection.description())
            return
        }
        
        guard let userID = UserStore.shared.userID else {
            return
        }
        Utilities.showHUD(forView: self.view, excludeViews: [])
        //message , sender_id , receiver_id ,subject
        let messageObserver = ApiManager.shared.apiService.sendMessageToUserID(receiverID!, senderID: String(userID), message: message)
        let messageDisposable = messageObserver.subscribe(onNext: {(successMessage) in
            Utilities.hideHUD(forView: self.view)
            print(successMessage)
            DispatchQueue.main.async(execute: {
                self.showAlertWithMessage(successMessage.msg)
                //self.dismiss(animated: true, completion: nil)
            })
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.showAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        })
        messageDisposable.disposed(by: disposableBag)
    }
    

    func selectMarkerForProfileAtIndex(_ index: Int) {
        showPinInfoViewAtIndex(index)
    }
    
}

extension FindViewController: FindAgencyProfileTableViewCellDelegate {
    
    func selectMarkerForAgencyAtIndex(_ index: Int) {
        showPinInfoViewAtIndex(index)
    }
    
    func showAgencyAtIndex(_ index: Int) {
        
        let mapItem = mapItemPagination.mapItems[index]
        let agencyProfile = PartnerProfileViewController.instantiateFromAppStoryboard(appStoryboard: .Dashboard)
        agencyProfile.agencyID = mapItem.ID
        agencyProfile.assetID = mapItem.assetID
        navigationController?.pushViewController(agencyProfile, animated: true)
    }
    
    func enquireAgencyAtIndex(_ index: Int) {
        guard UserStore.shared.isLoggedIn else {
            showLoginAlert()
            return
        }

        let enquireVC = EnquireAgencyViewController(nibName: "EnquireAgencyViewController", bundle: nil)
        let mapItem = mapItemPagination.mapItems[index]
        enquireVC.mapItem = mapItem
        // Create the dialog
        let popup = PopupDialog(viewController: enquireVC,
                                buttonAlignment: .horizontal,
                                transitionStyle: .bounceDown,
                                tapGestureDismissal: true,
                                panGestureDismissal: true)
        
        let buttonOne = CancelButton(title: AlertButton.cancel.description(), height: 60) {
            //Do nothing
        }
        
        
        let buttonTwo = DefaultButton(title: AlertButton.submit.description(), height: 60) {
            self.enquireAgencyWithMessage(enquireVC.commentTextView.text ?? "", fullName: enquireVC.nameTextField.text ?? "", email: enquireVC.emailTextField.text ?? "", phone: enquireVC.phoneTextField.text ?? "", preferred: enquireVC.selectedOption,
                mapItem: enquireVC.mapItem)

        }
        
        popup.addButtons([buttonOne, buttonTwo])
        present(popup, animated: true, completion: nil)
    }
    
    func reportAgencyAtIndex(_ index: Int) {
        guard UserStore.shared.isLoggedIn else {
            showLoginAlert()
            return
        }

        let reportVC = ReportUserViewController(nibName: "ReportUserViewController", bundle: nil)
        let mapItem = mapItemPagination.mapItems[index]
        reportVC.mapItem = mapItem
        // Create the dialog
        let popup = PopupDialog(viewController: reportVC,
                                buttonAlignment: .horizontal,
                                transitionStyle: .bounceDown,
                                tapGestureDismissal: true,
                                panGestureDismissal: true)
        
        let buttonOne = CancelButton(title: AlertButton.cancel.description(), height: 60) {
            //Do nothing
        }
        
        
        let buttonTwo = DefaultButton(title: AlertButton.submit.description(), height: 60) {
            self.reportUserWithComment(reportVC.commentsTextView.text ?? "", nature: reportVC.selectedOption, mapItem: reportVC.mapItem)
        }
        
        popup.addButtons([buttonOne, buttonTwo])
        present(popup, animated: true, completion: nil)
    }
    
}

extension FindViewController: TutorProfileViewControllerDelegate {
    func refreshShortlistDataForTutor() {
        
    }
    
    func refreshDataForTutorWithID(_ ID: String) {
        let mapIndex = mapItemPagination.mapItems.index { (item) -> Bool in
            return item.ID == ID
        }
        if let index = mapIndex, index != -1 {
            let currentState = mapItemPagination.mapItems[index].isFavorite
            mapItemPagination.mapItems[index].isFavorite = !currentState
            tableView.reloadRows(at: [IndexPath.init(row: index + 1, section: 0)], with: .none)
        }
    }
    
}

extension FindViewController: StudentProfileViewControllerDelegate {
    
    func refreshShortlistDataForStudent() {
        
    }
    
    func refreshDataForStudentWithID(_ ID: String) {
        let mapIndex = mapItemPagination.mapItems.index { (item) -> Bool in
            return item.ID == ID
        }
        if let index = mapIndex, index != -1 {
            let currentState = mapItemPagination.mapItems[index].isFavorite
            mapItemPagination.mapItems[index].isFavorite = !currentState
            tableView.reloadRows(at: [IndexPath.init(row: index + 1, section: 0)], with: .none)
        }
    }
    
    
}

extension FindViewController: UIScrollViewDelegate {
    

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        print("Scrollview content offset: ", scrollView.contentOffset.y)
        guard !ignoreScroll else {
            return
        }
        if scrollView.contentOffset.y > 50.0, self.slidingViewTopConstraint.constant == ScreenHeight/2 - 50 {
            UIView.animate(withDuration: 0.2) {
                self.slidingViewTopConstraint.constant = 0
                self.view.layoutIfNeeded()
            }
        } else {
            if self.slidingViewTopConstraint.constant > 0 {
                self.slidingViewTopConstraint.constant -= scrollView.contentOffset.y
                scrollView.contentOffset = CGPoint.init(x: 0, y: 0)
            } else if scrollView.contentOffset.y < 0, self.slidingViewTopConstraint.constant < ScreenHeight/2 - 50 {
                self.slidingViewTopConstraint.constant += (-1 * scrollView.contentOffset.y)
                scrollView.contentOffset = CGPoint.init(x: 0, y: 0)
            }
            if self.slidingViewTopConstraint.constant < 0 {
                self.slidingViewTopConstraint.constant = 0
            }
            if self.slidingViewTopConstraint.constant > ScreenHeight/2 - 50 {
                self.slidingViewTopConstraint.constant = ScreenHeight/2 - 50
            }
            self.view.layoutIfNeeded()
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        guard !ignoreScroll else {
            return
        }

        print("scrollViewWillBeginDecelerating")
        let vel = scrollView.panGestureRecognizer.velocity(in: self.view)
        let upwardDirection = vel.y < 0
        if upwardDirection, self.slidingViewTopConstraint.constant != 0 {
            UIView.animate(withDuration: 0.2) {
                self.slidingViewTopConstraint.constant = 0
                self.view.layoutIfNeeded()
            }
        } else if !upwardDirection, self.slidingViewTopConstraint.constant > 0 {
            UIView.animate(withDuration: 0.2) {
                self.slidingViewTopConstraint.constant = ScreenHeight/2 - 50
                self.view.layoutIfNeeded()
            }
        }
    }

    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        guard !ignoreScroll else {
            return
        }

        let vel = scrollView.panGestureRecognizer.velocity(in: self.view)
        let upwardDirection = vel.y < 0
        if upwardDirection, self.slidingViewTopConstraint.constant != 0 {
            UIView.animate(withDuration: 0.2) {
                self.slidingViewTopConstraint.constant = 0
                self.view.layoutIfNeeded()
            }
        } else if !upwardDirection, self.slidingViewTopConstraint.constant > 0 {
            UIView.animate(withDuration: 0.2) {
                self.slidingViewTopConstraint.constant = ScreenHeight/2 - 50
                self.view.layoutIfNeeded()
            }
        }
    }
    
}

extension FindViewController: FilterCategorySelectionViewControllerDelegate {
    
    func multipleSubjectsSelected(_ selectedSubs: [Subject]) {
        shouldCallAPI = true
        filterParams.selectedSubjects = selectedSubs
        DispatchQueue.main.async {
            self.configureSearchBar()
        }
    }
    
    
    func subjectSelected(_ selectedSub: Subject?) {
    }
    
    func levelSelected(_ selectedLev: Level?) {
    }
    
    func qualificationSelected(_ selectedQuali: Qualification?) {
    }
    
    func experienceSelected(_ selectedExp: Experience?) {
    }
    
    func teachingSinceSelected(_ selectedYear: String) {
        
    }
}


extension FindViewController {
    
    func filterUsers() {
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }

        if mapItemPagination.paginationType != .old {
            self.mapItemPagination.currentPageNumber = 1
            if isFirstTime == true {
                Utilities.showHUD(forView: self.view, excludeViews: [])
            } else {
                UIApplication.shared.isNetworkActivityIndicatorVisible = true
            }
        } else {
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
        }
        let curencyID  = Utilities.getCurrentCountryCode()
        let filterDict = filterParams.getFilterDictForCurrentLocation(currentLocation, currentCountryID: curencyID, page: String(mapItemPagination.currentPageNumber))

        isFetchingData = true

        let filterObserver = ApiManager.shared.apiService.findUsers(filterDict as [String: AnyObject], isTutor: filterParams.isTutorButtonSelected)
        findDisposable = filterObserver.subscribe(onNext: {(paginationObject) in

            self.isFetchingData = false
            if self.mapItemPagination.paginationType == .old && self.isFirstTime == false {
                DispatchQueue.main.async(execute: {
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                })
            }

            self.mapItemPagination.appendDataFromObject(paginationObject)
            DispatchQueue.main.async(execute: {
                self.tableView.isHidden = false
                var mapItem: MapItem!
                if self.isFirstTime == false {
                    if let selMarker = self.mapView.selectedMarker {
                        mapItem = selMarker.userData as? MapItem
                    }
                    self.clusterManager.add(paginationObject.mapItems)
                    self.clusterManager.cluster()
                } else {
                    self.shouldReloadMap = true
                }
                self.populateMapWithData()
                Utilities.hideHUD(forView: self.view)
                self.isFirstTime = false
                if let item = mapItem {
                    let index = self.mapItemPagination.mapItems.index(where: { (mapitem) -> Bool in
                        return mapitem.ID == item.ID
                    })
                    if let _index = index, _index > -1 {
                        self.showPinInfoViewAtIndex(_index)
                    }
                }
                self.tableView.reloadData()
            })
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
            })
        })
        findDisposable.disposed(by: disposableBag)
    }

    func addUserToShortlist(comment: String, itemIndex: Int) {
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }
        
        guard let userID = UserStore.shared.userID else {
            return
        }
        let mapItem = mapItemPagination.mapItems[itemIndex]
        Utilities.showHUD(forView: self.view, excludeViews: [])
        
        let addToShortlistObserver = ApiManager.shared.apiService.addToShortlistForUser(String(userID), shortlistUserID: mapItem.ID, reason: comment)
        let addToShortlistDisposable = addToShortlistObserver.subscribe(onNext: {(message) in
            Utilities.hideHUD(forView: self.view)
            
            DispatchQueue.main.async {
                NotificationCenter.default.post(name: .refreshDashboardData, object: nil, userInfo:nil)
                self.showSimpleAlertWithMessage(message)
                self.mapItemPagination.mapItems[itemIndex].isFavorite = true
                self.tableView.reloadRows(at: [IndexPath.init(row: itemIndex + 1, section: 0)], with: .none)
            }
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.showSimpleAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        })
        addToShortlistDisposable.disposed(by: disposableBag)
    }
    
    func removeUserFromShortlist(itemIndex: Int) {
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }
        
        guard let userID = UserStore.shared.userID else {
            return
        }
        let mapItem = mapItemPagination.mapItems[itemIndex]
        Utilities.showHUD(forView: self.view, excludeViews: [])
        
        let deleteShortlistObserver = ApiManager.shared.apiService.deleteShortlistForUser(String(userID), shortlistUserID: mapItem.ID)
        let deleteShortlistDisposable = deleteShortlistObserver.subscribe(onNext: {(message) in
            Utilities.hideHUD(forView: self.view)
            
            DispatchQueue.main.async {
                NotificationCenter.default.post(name: .refreshDashboardData, object: nil, userInfo:nil)
                self.showSimpleAlertWithMessage(message)
                self.mapItemPagination.mapItems[itemIndex].isFavorite = false
                self.tableView.reloadRows(at: [IndexPath.init(row: itemIndex + 1, section: 0)], with: .none)
            }
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.showSimpleAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        })
        deleteShortlistDisposable.disposed(by: disposableBag)
    }
    
    func reportUserWithComment(_ comment: String, nature: Int, mapItem: MapItem) {
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }
        
        guard let userID = UserStore.shared.userID else {
            return
        }
        
        var nat = ""
        switch nature {
        case 1:
            nat = "Spam"
        case 2:
            nat = "No Reply"
        default:
            nat = "Improper Conduct"
        }
        
        Utilities.showHUD(forView: self.view, excludeViews: [])
        
        let reportUserObserver = ApiManager.shared.apiService.reportUserWithUserID(mapItem.ID, comment: comment, nature: nat, userID: String(userID))
        let reportUserDisposable = reportUserObserver.subscribe(onNext: {(message) in
            Utilities.hideHUD(forView: self.view)
            
            DispatchQueue.main.async {
                self.showSimpleAlertWithMessage(message)
            }
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.showSimpleAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        })
        reportUserDisposable.disposed(by: disposableBag)
    }
    
    func enquireAgencyWithMessage(_ message: String, fullName: String, email: String, phone: String, preferred: Int, mapItem: MapItem) {
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }
        
        guard let userID = UserStore.shared.userID else {
            return
        }
        
        var preference = ""
        switch preferred {
        case 1:
            preference = "Email"
        default:
            preference = "Phone"
        }
        
        Utilities.showHUD(forView: self.view, excludeViews: [])
        
        let enquireObserver = ApiManager.shared.apiService.enquireAgencyWithID(mapItem.ID, assetID: mapItem.assetID, name: fullName, email: email, phone: phone, preferredContact: preference, message: message, userID: String(userID), courseID: nil)
        let enquireDisposable = enquireObserver.subscribe(onNext: {(message) in
            Utilities.hideHUD(forView: self.view)
            
            DispatchQueue.main.async {
                self.showSimpleAlertWithMessage(message)
            }
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.showSimpleAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        })
        enquireDisposable.disposed(by: disposableBag)
    }


}
