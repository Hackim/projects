//
//  FilterViewController.swift
//  Tueetor
//
//  Created by Phaninder on 08/04/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit
import GooglePlaces
import RxSwift
import GooglePlaces

protocol FilterViewControllerDelegate {
    func applyButtonTapped(_ newFilterParams: FilterParams)
}
class FilterViewController: BaseViewController {

    @IBOutlet weak var tutorSelectionButton: UIButton!
    @IBOutlet weak var studentSelectionButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var showResultsButton: UIButton!
    @IBOutlet weak var bottomHolderView: UIView!
    @IBOutlet weak var applyButton: UIButton!
    @IBOutlet weak var countActivityIndicator: UIActivityIndicatorView!
    
    var filterParameters: FilterParams!
    var delegate: FilterViewControllerDelegate?
    var addressTextField: UITextField!
    var searchTextField: UITextField!
    var countries = [Country]()
    var disposableBag = DisposeBag()
    var countDisposable: Disposable!
    var currentLocation: CLLocationCoordinate2D!

    override func viewDidLoad() {
        super.viewDidLoad()
        if filterParameters == nil {
            filterParameters = FilterParams.init()
        }
        setupFilterButtonsUI()
        getCountForFilterApplied()
        let nib = UINib(nibName: "FilterHeaderView", bundle: nil)
        tableView.register(nib, forHeaderFooterViewReuseIdentifier: "FilterHeaderView")

        showResultsButton.layer.shadowOffset = CGSize.init(width: 0, height: 5)
        showResultsButton.layer.shadowRadius = 3
        showResultsButton.layer.shadowColor = themeBlueColor.cgColor
        showResultsButton.layer.shadowOpacity = 0.75
        
        bottomHolderView.layer.shadowOffset = CGSize.init(width: 0, height: -5)
        bottomHolderView.layer.shadowRadius = 3
        bottomHolderView.layer.shadowColor = themeBlackColor.cgColor
        bottomHolderView.layer.shadowOpacity = 0.4
        if let countriesArray = Utilities.getCountries() {
            countries = countriesArray
        } else {
            fetchCountries()
        }
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(filterAvailabilityTapped), name: .filterAvailabilitySelected,
                                               object: nil)
        if filterParameters.locationName == "" {
            fetchCurrentLocationAddress()
        }
    }
    
    
    @IBAction func closeButtonTapped(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func tutorFilterButtonTapped(_ sender: UIButton) {
        guard filterParameters.isTutorButtonSelected == false else {
            return
        }
        filterParameters.isTutorButtonSelected = true
        filterParameters.setLegendsForTutorSelection()
        getCountForFilterApplied()
        self.tableView.reloadSections([FilterTableViewSectionType.budget.rawValue], with: .none)
        setupFilterButtonsUI()
    }
    
    @IBAction func studentFilterButtonTapped(_ sender: UIButton) {
        guard !filterParameters.isFeatured else {
            self.showSimpleAlertWithMessage("This section is disabled.".localized)
            return
        }
        guard filterParameters.isTutorButtonSelected == true else {
            return
        }
        filterParameters.isTutorButtonSelected = false
        filterParameters.setLegendsForStudentSelection()
        getCountForFilterApplied()
        self.tableView.reloadSections([FilterTableViewSectionType.budget.rawValue], with: .none)
        setupFilterButtonsUI()
    }
    
    @IBAction func showResultsButtonTapped(_ sender: UIButton) {
        delegate?.applyButtonTapped(filterParameters)
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func clearFilterButtonTapped(_ sender: UIButton) {
        filterParameters.clearFilter()
        tableView.reloadData()
    }
    
    @objc func filterAvailabilityTapped() {
        filterParameters.isAvailableAnytimeSelected = false
        self.tableView.reloadSections([FilterTableViewSectionType.availability.rawValue], with: .none)
        DispatchQueue.main.async {
            self.getCountForFilterApplied()
        }
    }
    
    func setTitleForApplyButton(_ count: Int) {
        countActivityIndicator.stopAnimating()
        countActivityIndicator.isHidden = true
        let buttonTitle = "Show ".localized + "\(count) " + (filterParameters.isTutorButtonSelected ? "Tutors".localized : "Students".localized)
        self.applyButton.setTitle(buttonTitle, for: .normal)
    }
    
    func fetchCurrentLocationAddress() {
        let placesClient = GMSPlacesClient.init()
        
        
        placesClient.currentPlace(callback: { (placeLikelihoodList, error) -> Void in
            if let error = error {
                print("Pick Place error: \(error.localizedDescription)")
                return
            }

            if let placeLikelihoodList = placeLikelihoodList {
                for likelihood in placeLikelihoodList.likelihoods {
                    let place = likelihood.place
                    self.filterParameters.latitude = place.coordinate.latitude
                    self.filterParameters.longitude = place.coordinate.longitude
                    self.filterParameters.locationName = place.formattedAddress ?? place.name
                    if let addressLines = place.addressComponents {
                        // Populate all of the address fields we can find.
                        for field in addressLines {
                            switch field.type {
                            case kGMSPlaceTypeCountry:
                                let index = self.countries.index { (country) -> Bool in
                                    country.name == field.name
                                }
                                if let _index = index {
                                    self.filterParameters.countryID = String(self.countries[_index].id)
                                }
                            default:
                                print("ignore")
                            }
                        }
                    }
                    DispatchQueue.main.async {
                        self.tableView.reloadSections([FilterTableViewSectionType.location.rawValue, FilterTableViewSectionType.budget.rawValue], with: .none)
                        self.getCountForFilterApplied()
                    }
                }
            }
        })
    }
    
    func setupFilterButtonsUI() {
        let deSelectBackgroundColor = UIColor(red: 0.9254, green: 0.9254, blue: 0.9254, alpha: 1)
        
        tutorSelectionButton.backgroundColor = filterParameters.isTutorButtonSelected ? themeBlueColor : deSelectBackgroundColor
        tutorSelectionButton.layer.shadowOffset = CGSize.init(width: 0, height: 5)
        tutorSelectionButton.layer.shadowRadius = 3
        tutorSelectionButton.layer.shadowColor = filterParameters.isTutorButtonSelected ? themeBlueColor.cgColor : UIColor.clear.cgColor
        tutorSelectionButton.layer.shadowOpacity = filterParameters.isTutorButtonSelected ? 0.75 : 1
        tutorSelectionButton.layer.borderColor = filterParameters.isTutorButtonSelected ? themeBlueColor.cgColor : themeBlackColor.cgColor
        tutorSelectionButton.layer.borderWidth = 1.0
        tutorSelectionButton.setTitleColor(filterParameters.isTutorButtonSelected ? UIColor.white : themeBlackColor, for: .normal)
        
        studentSelectionButton.backgroundColor = filterParameters.isTutorButtonSelected ? deSelectBackgroundColor : themeBlueColor
        studentSelectionButton.layer.shadowOffset = CGSize.init(width: 0, height: 5)
        studentSelectionButton.layer.shadowRadius = 3
        studentSelectionButton.layer.shadowColor = filterParameters.isTutorButtonSelected ? UIColor.clear.cgColor : themeBlueColor.cgColor
        studentSelectionButton.layer.shadowOpacity = filterParameters.isTutorButtonSelected ? 1 : 0.75
        studentSelectionButton.layer.borderColor = filterParameters.isTutorButtonSelected ? themeBlackColor.cgColor : themeBlueColor.cgColor
        studentSelectionButton.layer.borderWidth = 1.0
        studentSelectionButton.setTitleColor(filterParameters.isTutorButtonSelected ? themeBlackColor : UIColor.white, for: .normal)
    }

}

extension FilterViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == FilterTableViewSectionType.subjects.rawValue {
            return filterParameters.selectedSubjects.count + 1
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let section = indexPath.section
        switch section {
        case FilterTableViewSectionType.subjects.rawValue:
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: FilterOptionSelectTableViewCell.cellIdentifier(), for: indexPath) as! FilterOptionSelectTableViewCell
                
                cell.titleLabel.text = "Select Subject Name".localized
                cell.titleLabel.textColor = themeBlackColor
                
                return cell
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: FilterCheckboxTableViewCell.cellIdentifier(), for: indexPath) as! FilterCheckboxTableViewCell
                cell.configureCellWithTitle(filterParameters.selectedSubjects[indexPath.row - 1].name, isChecked: true, index: indexPath)
                return cell
            }
            
        case FilterTableViewSectionType.qualifications.rawValue:
            let cell = tableView.dequeueReusableCell(withIdentifier: FilterOptionSelectTableViewCell.cellIdentifier(), for: indexPath) as! FilterOptionSelectTableViewCell
            
            if let quali = filterParameters.selectedQualification {
                cell.titleLabel.text = quali.name
                cell.titleLabel.textColor = themeBlueColor
            } else {
                cell.titleLabel.text = "No Preference".localized
                cell.titleLabel.textColor = themeBlackColor
            }
            return cell
            
        case FilterTableViewSectionType.experience.rawValue:
            let cell = tableView.dequeueReusableCell(withIdentifier: FilterOptionSelectTableViewCell.cellIdentifier(), for: indexPath) as! FilterOptionSelectTableViewCell
            
            if let exp = filterParameters.selectedExperience {
                cell.titleLabel.text = exp.name
                cell.titleLabel.textColor = themeBlueColor
            } else {
                cell.titleLabel.text = "No Preference".localized
                cell.titleLabel.textColor = themeBlackColor
            }
            return cell
        case FilterTableViewSectionType.budget.rawValue:
            let cell = tableView.dequeueReusableCell(withIdentifier: FilterBudgetSelectionTableViewCell.cellIdentifier(), for: indexPath) as! FilterBudgetSelectionTableViewCell
            cell.delegate = self
            cell.filterParams = filterParameters
            cell.configureCellWithSliderValues(filterParameters)
            cell.perSessionSlider.refresh()
            cell.perMonthSlider.refresh()
            return cell
        case FilterTableViewSectionType.rating.rawValue:
            let cell = tableView.dequeueReusableCell(withIdentifier: FilterRatingTableViewCell.cellIdentifier(), for: indexPath) as! FilterRatingTableViewCell
            cell.delegate = self
            cell.configureCellWithRating(filterParameters.rating, isAnyRatingSelected: filterParameters.anyRatingSelected)
            return cell
        case FilterTableViewSectionType.location.rawValue:
            let cell = tableView.dequeueReusableCell(withIdentifier: FilterLocationTableViewCell.cellIdentifier(), for: indexPath) as! FilterLocationTableViewCell
            addressTextField = cell.addressTextField
            cell.delegate = self
            addressTextField.delegate = self
            cell.configureTextFieldsWithLocation(filterParameters.locationName)
            return cell
        case FilterTableViewSectionType.userSearch.rawValue:
            let cell = tableView.dequeueReusableCell(withIdentifier: FilterUserSearchTableViewCell.cellIdentifier(), for: indexPath) as! FilterUserSearchTableViewCell
            searchTextField = cell.searchTextField
            cell.delegate = self
            searchTextField.delegate = self
            cell.configureTextFieldsWithText(filterParameters.searchName)
            return cell
        case FilterTableViewSectionType.distance.rawValue:
            let cell = tableView.dequeueReusableCell(withIdentifier: FilterDistanceSelectionTableViewCell.cellIdentifier(), for: indexPath) as! FilterDistanceSelectionTableViewCell
            cell.delegate = self
            cell.filterParams = self.filterParameters
            cell.configureCellWithDistance(filterParameters.distance)
            cell.distanceSlider.refresh()
            return cell
        case FilterTableViewSectionType.availability.rawValue:
            let cell = tableView.dequeueReusableCell(withIdentifier: FilterAvailabilitySelectionTableViewCell.cellIdentifier(), for: indexPath) as! FilterAvailabilitySelectionTableViewCell
            cell.filterParams = filterParameters
            cell.collectionView.reloadData()
            return cell
        default:
            //Used for levels section
            let cell = tableView.dequeueReusableCell(withIdentifier: FilterOptionSelectTableViewCell.cellIdentifier(), for: indexPath) as! FilterOptionSelectTableViewCell
            
            if let level = filterParameters.selectedLevel {
                cell.titleLabel.text = level.name
                cell.titleLabel.textColor = themeBlueColor
            } else {
                cell.titleLabel.text = "All levels".localized
                cell.titleLabel.textColor = themeBlackColor
            }
            return cell
        }

    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let section = indexPath.section
        switch section {
        case FilterTableViewSectionType.subjects.rawValue:
            if indexPath.row == 0 {
                return FilterOptionSelectTableViewCell.cellHeight()
            } else {
                return FilterOptionSelectTableViewCell.cellHeight()
            }
        case FilterTableViewSectionType.qualifications.rawValue,
             FilterTableViewSectionType.experience.rawValue:
            return FilterOptionSelectTableViewCell.cellHeight()
        case FilterTableViewSectionType.budget.rawValue:
            return FilterBudgetSelectionTableViewCell.cellHeight()
        case FilterTableViewSectionType.rating.rawValue:
            return FilterRatingTableViewCell.cellHeight()
        case FilterTableViewSectionType.location.rawValue:
            return FilterLocationTableViewCell.cellHeight()
        case FilterTableViewSectionType.userSearch.rawValue:
            return FilterUserSearchTableViewCell.cellHeight()
        case FilterTableViewSectionType.distance.rawValue:
            return FilterDistanceSelectionTableViewCell.cellHeight()
        case FilterTableViewSectionType.availability.rawValue:
            return FilterAvailabilitySelectionTableViewCell.cellHeight()
        default:
            return FilterOptionSelectTableViewCell.cellHeight()
        }
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = self.tableView.dequeueReusableHeaderFooterView(withIdentifier: "FilterHeaderView") as! FilterHeaderView
            cell.delegate = self
        if section == FilterTableViewSectionType.rating.rawValue {
            cell.configureSectionForType(section, isSelected: filterParameters.anyRatingSelected)
        } else if section == FilterTableViewSectionType.availability.rawValue {
            cell.configureSectionForType(section, isSelected: filterParameters.isAvailableAnytimeSelected)
        } else if section == FilterTableViewSectionType.distance.rawValue {
            cell.configureSectionForType(section, isSelected: filterParameters.isDistanceAnywhereSelected)
        } else if section == FilterTableViewSectionType.budget.rawValue {
            cell.configureSectionForType(section, isSelected: filterParameters.isAnyBudgetSelected)
            cell.titleLabel.text = filterParameters.isTutorButtonSelected ? "Budget".localized : "Rate".localized
        } else {
            cell.configureSectionForType(section, isSelected: false)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 69.0
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.5
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footer = UIView()
        footer.backgroundColor = UIColor.white
        return footer
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let section = indexPath.section
        switch section {
        case FilterTableViewSectionType.subjects.rawValue:
            if indexPath.row == 0 {
                let filterSelectionViewController = FilterCateogrySelectionViewController.instantiateFromAppStoryboard(appStoryboard: .Find)
                filterSelectionViewController.delegate = self
                filterSelectionViewController.screenType = .subject
                if filterParameters.selectedSubjects.count > 0 {
                    filterSelectionViewController.selectedSubjectsArray = filterParameters.selectedSubjects
                }
                filterSelectionViewController.isMultiSelect = true
                navigationController?.pushViewController(filterSelectionViewController, animated: true)
            } else {
                filterParameters.selectedSubjects.remove(at: indexPath.row - 1)
                
                tableView.deleteRows(at: [IndexPath(row: indexPath.row, section: 0)], with: .left)
                DispatchQueue.main.async {
                    self.getCountForFilterApplied()
                }
            }
            
        case FilterTableViewSectionType.levels.rawValue:
            let filterSelectionViewController = FilterCateogrySelectionViewController.instantiateFromAppStoryboard(appStoryboard: .Find)
            filterSelectionViewController.screenType = .level
            filterSelectionViewController.delegate = self
            if let level = filterParameters.selectedLevel {
                filterSelectionViewController.selectedLevel = level
            }
            navigationController?.pushViewController(filterSelectionViewController, animated: true)
            
        case FilterTableViewSectionType.qualifications.rawValue:
            let filterSelectionViewController = FilterCateogrySelectionViewController.instantiateFromAppStoryboard(appStoryboard: .Find)
            filterSelectionViewController.screenType = .qualification
            filterSelectionViewController.delegate = self
            if let qualification = filterParameters.selectedQualification {
                filterSelectionViewController.selectedQualification = qualification
            }
            navigationController?.pushViewController(filterSelectionViewController, animated: true)
            
        case FilterTableViewSectionType.experience.rawValue:
            let filterSelectionViewController = FilterCateogrySelectionViewController.instantiateFromAppStoryboard(appStoryboard: .Find)
            filterSelectionViewController.screenType = .experience
            filterSelectionViewController.delegate = self
            if let experience = filterParameters.selectedExperience {
                filterSelectionViewController.selectedExperience = experience
            }
            navigationController?.pushViewController(filterSelectionViewController, animated: true)
        default:
            break
        }
    }
    
}

extension FilterViewController: FilterHeaderViewDelegate {
    
    func checkboxButtonTappedForSection(_ section: Int) {
        if section == FilterTableViewSectionType.rating.rawValue {
            guard !filterParameters.anyRatingSelected else {
                return
            }
            filterParameters.anyRatingSelected = !filterParameters.anyRatingSelected
            filterParameters.rating = 0
            self.tableView.reloadSections([FilterTableViewSectionType.rating.rawValue], with: .none)
        } else if section == FilterTableViewSectionType.distance.rawValue {
            guard !filterParameters.isDistanceAnywhereSelected else {
                return
            }

            filterParameters.isDistanceAnywhereSelected = !filterParameters.isDistanceAnywhereSelected
            filterParameters.distance = 10000
            self.tableView.reloadSections([FilterTableViewSectionType.distance.rawValue], with: .none)
        } else if section == FilterTableViewSectionType.budget.rawValue {
            guard !filterParameters.isAnyBudgetSelected else {
                return
            }
            filterParameters.perMonthMinimum = 0
            filterParameters.perMonthMaximum = 10000
            filterParameters.perSessionMinimum = 0
            filterParameters.perSessionMaximum = 1000
            filterParameters.isAnyBudgetSelected = !filterParameters.isAnyBudgetSelected
            self.tableView.reloadSections([FilterTableViewSectionType.budget.rawValue], with: .none)
        } else if section == FilterTableViewSectionType.availability.rawValue {
            guard !filterParameters.isAvailableAnytimeSelected else {
                return
            }
            filterParameters.isAvailableAnytimeSelected = !filterParameters.isAvailableAnytimeSelected
            filterParameters.selectedTimeSlots = ["MON": ["0","0","0","0"],
                                                  "TUE": ["0","0","0","0"],
                                                  "WED": ["0","0","0","0"],
                                                  "THU": ["0","0","0","0"],
                                                  "FRI": ["0","0","0","0"],
                                                  "SAT": ["0","0","0","0"],
                                                  "SUN": ["0","0","0","0"]]
            self.tableView.reloadSections([FilterTableViewSectionType.availability.rawValue], with: .none)
        }
        DispatchQueue.main.async {
            self.getCountForFilterApplied()
        }
    }
}

extension FilterViewController: FilterDistanceSelectionTableViewCellDelegate {
    func distanceSelected(_ distance: Int) {
        if filterParameters.distance == 10000 {
            filterParameters.isDistanceAnywhereSelected = false
            filterParameters.distance = distance
            if let headerView = tableView.headerView(forSection: FilterTableViewSectionType.distance.rawValue) as? FilterHeaderView {
                headerView.checkboxButton.setImage(UIImage.init(named: "checkbox_empty"), for: .normal)
            }
        } else {
            filterParameters.distance = distance
        }
        DispatchQueue.main.async {
            self.getCountForFilterApplied()
        }
    }
}

extension FilterViewController: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.tag == 2 {
            filterParameters.searchName = textField.text!
        }
    }
    
}

extension FilterViewController: FilterLocationTableViewCellDelegate {
    
    func selectionChanged(isLocationSelected: Bool, isNameSelected: Bool) {
        
    }
    
    func showPlacePicker() {
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        present(autocompleteController, animated: true, completion: nil)
    }
    
}

extension FilterViewController: FilterUserSearchTableViewCellDelegate {
    
    func goButtonTapped() {
        self.view.endEditing(true)
        delegate?.applyButtonTapped(filterParameters)
        navigationController?.popViewController(animated: true)
    }
    
}

extension FilterViewController: GMSAutocompleteViewControllerDelegate {
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        
        filterParameters.latitude = place.coordinate.latitude
        filterParameters.longitude = place.coordinate.longitude
        filterParameters.locationName = place.formattedAddress ?? place.name
        if let addressLines = place.addressComponents {
            // Populate all of the address fields we can find.
            for field in addressLines {
                switch field.type {
                case kGMSPlaceTypeCountry:
                    let index = countries.index { (country) -> Bool in
                        country.name == field.name
                    }
                    if let _index = index {
                        filterParameters.countryID = String(countries[_index].id)
                    }
                default:
                    print("ignore")
                }
            }
        }
//        if let cell = tableView.cellForRow(at: IndexPath.init(row: 0, section: FilterTableViewSectionType.location.rawValue)) as? FilterLocationTableViewCell {
//            filterParameters.locationName = place.formattedAddress ?? place.name
//            cell.addressTextField.text = filterParameters.locationName
//        }
        DispatchQueue.main.async {
            self.tableView.reloadSections([FilterTableViewSectionType.location.rawValue, FilterTableViewSectionType.budget.rawValue], with: .none)
            self.getCountForFilterApplied()
        }
        
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}

extension FilterViewController: FilterBudgetSelectionTableViewCellDelegate {
    
    func perSessionSliderIsUpdated(lowerValue: Int, upperValue: Int) {
        if filterParameters.perSessionMinimum == 0, filterParameters.perSessionMaximum == 1000 {
            filterParameters.perSessionMinimum = lowerValue
            filterParameters.perSessionMaximum = upperValue
            filterParameters.isAnyBudgetSelected = false
            if let headerView = tableView.headerView(forSection: FilterTableViewSectionType.budget.rawValue) as? FilterHeaderView {
                headerView.checkboxButton.setImage(UIImage.init(named: "checkbox_empty"), for: .normal)
            }
        } else {
            filterParameters.perSessionMinimum = lowerValue
            filterParameters.perSessionMaximum = upperValue
        }

        DispatchQueue.main.async {
            self.getCountForFilterApplied()
        }
    }
    
    func perMonthSliderIsUpdated(lowerValue: Int, upperValue: Int) {
        if filterParameters.perMonthMinimum == 0, filterParameters.perMonthMaximum == 10000 {
            filterParameters.perMonthMinimum = lowerValue
            filterParameters.perMonthMaximum = upperValue
            filterParameters.isAnyBudgetSelected = false
            if let headerView = tableView.headerView(forSection: FilterTableViewSectionType.budget.rawValue) as? FilterHeaderView {
                headerView.checkboxButton.setImage(UIImage.init(named: "checkbox_empty"), for: .normal)
            }
        } else {
            filterParameters.perMonthMinimum = lowerValue
            filterParameters.perMonthMaximum = upperValue
        }

        DispatchQueue.main.async {
            self.getCountForFilterApplied()
        }
    }
    
}

extension FilterViewController: FilterRatingTableViewCellDelegate {
    func ratingValueChanged(_ ratingValue: CGFloat) {
        filterParameters.rating = round(10*ratingValue)/10
        filterParameters.anyRatingSelected = false
        self.tableView.reloadSections([FilterTableViewSectionType.rating.rawValue], with: .none)
        DispatchQueue.main.async {
            self.getCountForFilterApplied()
        }
    }
}


extension FilterViewController: FilterCategorySelectionViewControllerDelegate {
    func multipleSubjectsSelected(_ selectedSubs: [Subject]) {
        filterParameters.selectedSubjects = selectedSubs
        DispatchQueue.main.async {
            self.getCountForFilterApplied()
        }
        tableView.reloadData()
    }
    
    
    func subjectSelected(_ selectedSub: Subject?) {
//        filterParameters.selectedSubject = selectedSub
//        tableView.reloadData()
    }
    
    func levelSelected(_ selectedLev: Level?) {
        if let _ = selectedLev {
            filterParameters.selectedLevel = selectedLev
        } else {
            filterParameters.selectedLevel = nil
        }
        DispatchQueue.main.async {
            self.getCountForFilterApplied()
        }
        tableView.reloadData()
    }
    
    func qualificationSelected(_ selectedQuali: Qualification?) {
        if let _ = selectedQuali {
            filterParameters.selectedQualification = selectedQuali
        } else {
            filterParameters.selectedQualification = nil
        }
        DispatchQueue.main.async {
            self.getCountForFilterApplied()
        }
        tableView.reloadData()
    }
    
    func experienceSelected(_ selectedExp: Experience?) {
        if selectedExp != nil {
            filterParameters.selectedExperience = selectedExp
        } else {
            filterParameters.selectedExperience = nil
        }
        DispatchQueue.main.async {
            self.getCountForFilterApplied()
        }
        tableView.reloadData()
    }
    
    func teachingSinceSelected(_ selectedYear: String) {
        
    }
}

extension FilterViewController {
    
    func fetchCountries() {
        
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }
        
        Utilities.showHUD(forView: self.view, excludeViews: [])
        let countryObserver = ApiManager.shared.apiService.fetchCountries()
        let countryDisposable = countryObserver.subscribe(onNext: {(countries) in
            Utilities.hideHUD(forView: self.view)
            self.countries = countries
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.showSimpleAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        })
        countryDisposable.disposed(by: disposableBag)
    }

    func getCountForFilterApplied() {
        
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }
        
        if countDisposable != nil {
            countDisposable.dispose()
        }
        countActivityIndicator.isHidden = false
        countActivityIndicator.startAnimating()
        self.applyButton.setTitle("", for: .normal)

        let filterDict = filterParameters.getFilterDictForCurrentLocation(currentLocation, currentCountryID: Utilities.getCurrentCountryCode(), page: nil)
        
        let countObserver = ApiManager.shared.apiService.fetchFilterCount(filterDict as [String : AnyObject], isTutor: filterParameters.isTutorButtonSelected)
        countDisposable = countObserver.subscribe(onNext: {(count) in
            
            DispatchQueue.main.async(execute: {
                self.setTitleForApplyButton(count)
            })
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                self.setTitleForApplyButton(0)
            })
        })
        countDisposable.disposed(by: disposableBag)
    }
    

}
