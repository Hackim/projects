//
//  FilterCateogrySelectionViewController.swift
//  Tueetor
//SubjectInfoTableViewCell
//  Created by Phaninder Kumar on 24/05/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit
import RxSwift

protocol FilterCategorySelectionViewControllerDelegate {
    func subjectSelected(_ selectedSub: Subject?)
    func levelSelected(_ selectedLev: Level?)
    func qualificationSelected(_ selectedQuali: Qualification?)
    func experienceSelected(_ selectedExp: Experience?)
    func teachingSinceSelected(_ selectedYear: String)
    func multipleSubjectsSelected(_ selectedSubs: [Subject])
}

class FilterCateogrySelectionViewController: BaseViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var searchTextFieldWidthConstraint: NSLayoutConstraint!
    
    var screenType: FilterScreenCategoryType = .subject
    var disposableBag = DisposeBag()
    var isFetchingData = false
    var isSearchActive = false
    var isFirstTime = true
    var isMultiSelect = false
    var isSecondarySubjectAlertShown = false
    var searchString = ""
    var totalRows: Int = 1
    var headerText: String!
    var descriptionText: String!
    
    var subjectPagination = SubjectPagination.init()
    var selectedSubjectsArray: [Subject]!
    var filterDisposable: Disposable!

    var levelPagination = LevelPagination.init()
    var selectedLevel: Level!

    var backupQualifications = [Qualification]()
    var qualifications = [Qualification]()
    var selectedQualification: Qualification!
    
    var backupExperiences = [Experience]()
    var experiences = [Experience]()
    var selectedExperience: Experience!

    var selectedYear = ""
    var nextYearValue = 2019
    
    private let refreshControl = UIRefreshControl()
    var delegate: FilterCategorySelectionViewControllerDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 10.0, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.addSubview(refreshControl)
        }
        if selectedSubjectsArray == nil {
            selectedSubjectsArray = []
        }
        searchTextFieldWidthConstraint.constant = 0.0
        refreshControl.addTarget(self, action: #selector(refreshData(_:)), for: .valueChanged)
        switch screenType {
        case .subject:
            headerText = "Subjects".localized
            descriptionText = "Select a subject you would like to be taught".localized
            fetchSubjects()
        case .level:
            headerText = "Levels".localized
            descriptionText = "Select a level".localized
            fetchLevels()
        case .qualification:
            headerText = "Teaching Qualification".localized
            descriptionText = "Select a teaching qualification".localized
            fetchQualifications()
        case .experience:
            headerText = "Teaching Experience".localized
            descriptionText = "Select a teaching experience".localized
            fetchExperiences()
        case .teachingSince:
            headerText = "Teaching Since".localized
            descriptionText = "Select an year".localized
            let calendar = Calendar.current
            nextYearValue = calendar.component(.year, from: Date())
            nextYearValue += 1
            searchButton.isHidden = true
            fetchExperiences()
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @objc func refreshData(_ sender: Any) {
        switch screenType {
        case .subject:
            self.subjectPagination.paginationType = .new
            fetchSubjects()
        case .level:
            self.levelPagination.paginationType = .new
            fetchLevels()
        case .qualification:
            isFirstTime = true
            fetchQualifications()
        case .experience:
            isFirstTime = true
            fetchExperiences()
        case .teachingSince:
            tableView.reloadData()
        }

        if let refreshControl = sender as? UIRefreshControl {
            refreshControl.endRefreshing()
        }
    }
    
    @IBAction func searchButtonTapped(_ sender: UIButton) {
        isSearchActive = !isSearchActive
        if isSearchActive {
            searchTextField.becomeFirstResponder()
        } else {
            searchTextField.resignFirstResponder()
        }
        self.view.layoutIfNeeded()
        UIView.animate(withDuration: 0.7, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 1, options: .curveEaseIn, animations: {
            self.searchTextFieldWidthConstraint.constant = self.isSearchActive ? ScreenWidth - 132.0 : 0
            let imageName = self.isSearchActive ? "search_active" : "search_inactive"
            self.searchButton.setImage(UIImage(named: imageName), for: .normal)
            self.searchButton.backgroundColor = self.isSearchActive ? themeBlueColor : UIColor.clear
            self.view.layoutIfNeeded()
        }) { (completed) in
            
        }
    }
    
    @IBAction func valueChangedInSearchField(_ sender: Any) {
        switch screenType {
        case .subject:
            if (filterDisposable != nil) {
                Utilities.hideHUD(forView: view)
                filterDisposable.dispose()
            }
            subjectPagination.paginationType = .new
            fetchSubjects()

        case .level:
            if (filterDisposable != nil) {
                Utilities.hideHUD(forView: view)
                filterDisposable.dispose()
            }
            levelPagination.paginationType = .new
            fetchLevels()
        case .qualification:
            setQualificationsArrayFromBackup()
        case .experience:
            setExperiencesArrayFromBackup()
        case .teachingSince:
            print("Doesn't apply")
        }

    }

    @IBAction func navigateBackToFilterScreen(_ sender: UIButton) {
        switch screenType {
        case .subject:
            if isMultiSelect {
                delegate?.multipleSubjectsSelected(selectedSubjectsArray)
//                delegate?.multipleSubjectsSelected(selectedSubjectsArray.sorted(by: { (subA, subB) -> Bool in
//                    subA.name < subB.name
//                }))
            } else {
                delegate?.subjectSelected(selectedSubjectsArray[safe: 0])
            }
        case .level:
            delegate?.levelSelected(selectedLevel)
        case .qualification:
            delegate?.qualificationSelected(selectedQualification)
        case .experience:
            delegate?.experienceSelected(selectedExperience)
        case .teachingSince:
            delegate?.teachingSinceSelected(selectedYear)
        }
        self.navigateBack(sender: sender)
    }
    
    func setQualificationsArrayFromBackup() {
        searchString = self.searchTextField.text ?? ""
        guard !searchString.isEmpty else {
            self.qualifications = backupQualifications
            self.tableView.reloadData()
            return
        }
        self.qualifications = self.backupQualifications.filter({ (qualification) -> Bool in
            return qualification.name.lowercased().range(of: self.searchString.lowercased()) != nil
        })
        self.tableView.reloadData()
    }
    
    func setExperiencesArrayFromBackup() {
        searchString = self.searchTextField.text ?? ""
        guard !searchString.isEmpty else {
            self.experiences = backupExperiences
            self.tableView.reloadData()
            return
        }
        self.experiences = self.backupExperiences.filter({ (experience) -> Bool in
            return experience.name.lowercased().range(of: self.searchString.lowercased()) != nil
        })
        self.tableView.reloadData()
    }

}


extension FilterCateogrySelectionViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch screenType {
        case .subject:
            totalRows = subjectPagination.subjects.count + 2
        case .level:
            totalRows = levelPagination.levels.count + 2
        case .qualification:
            totalRows = qualifications.count + 1
        case .experience:
            totalRows = experiences.count + 1
        case .teachingSince:
            totalRows = 101
        }
        return totalRows
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard indexPath.row != 0 else {
            let cell = tableView.dequeueReusableCell(withIdentifier: FilterOptionDescriptionTableViewCell.cellIdentifier(), for: indexPath) as! FilterOptionDescriptionTableViewCell
            cell.descriptionLabel.text = descriptionText
            return cell
        }
        
        if screenType == .subject || screenType == .level {
            guard indexPath.row != totalRows - 1 else {
                let cell = tableView.dequeueReusableCell(withIdentifier: LoadMoreTableViewCell.cellIdentifier(), for: indexPath) as! LoadMoreTableViewCell
                cell.activityIndicator.startAnimating()
                return cell
            }
        }

        let cell = tableView.dequeueReusableCell(withIdentifier: FilterOptionTableViewCell.cellIdentifier(), for: indexPath) as! FilterOptionTableViewCell
        switch screenType {
        case .subject:
            let subject = subjectPagination.subjects[indexPath.row - 1]
            var isSelected = false
            let index = selectedSubjectsArray.index { (sub) -> Bool in
                return sub.id == subject.id
            }
            if let subIndex = index, subIndex > -1 {
                isSelected = true
            }
            cell.configureCellWithSubject(subject, isSelected: isSelected)
        case .level:
            let level = levelPagination.levels[indexPath.row - 1]
            var isSelected = false
            if let lev = selectedLevel {
                isSelected = level.id == lev.id
            }
            cell.configureCellWithLevel(level, isSelected: isSelected)
        case .qualification:
            let qualification = qualifications[indexPath.row - 1]
            var isSelected = false
            if let quali = selectedQualification {
                isSelected = qualification.id == quali.id
            }
            cell.configureCellWithQualification(qualification, isSelected: isSelected)
            
        case .experience:
            var isSelected = false
            let experience = experiences[indexPath.row - 1]
            if let exp = selectedExperience {
                isSelected = experience.name == exp.name
            }
            cell.configureCellWithExperience(experience, isSelected: isSelected)
        case .teachingSince:
            var isSelected = false
            let year = nextYearValue - indexPath.row
            if selectedYear == String(year) {
                isSelected = true
            }
            cell.configureCellWithYear(String(year), isSelected: isSelected)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let sectionHeader = tableView.dequeueReusableCell(withIdentifier: FilterOptionSectionHeaderTableViewCell.cellIdentifier()) as! FilterOptionSectionHeaderTableViewCell
        sectionHeader.titleLabel.text = headerText
        return sectionHeader
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        tableView.estimatedSectionHeaderHeight = 81.0
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.5
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footer = UIView()
        footer.backgroundColor = UIColor.white
        return footer
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {

        if indexPath.row == 1 &&  Utilities.isUserTypeStudent() && ( screenType == .qualification || screenType == .level || screenType == .experience ) {
            return 0
        }
        if screenType == .subject || screenType == .level {
            guard indexPath.row != totalRows - 1 else {
                switch screenType {
                case .subject:
                    return subjectPagination.hasMoreToLoad ? 50.0 : 0
                case .level:
                    return levelPagination.hasMoreToLoad ? 50.0 : 0
                case .qualification:
                    return 0
                case .experience:
                    return 0
                case .teachingSince:
                    return 0
                }
            }
        }
        tableView.estimatedRowHeight = 100.0
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        switch screenType {
        case .subject:
            if indexPath.row == subjectPagination.subjects.count + 1 && self.subjectPagination.hasMoreToLoad && !isFetchingData {
                fetchSubjects()
            }
        case .level:
            if indexPath.row == levelPagination.levels.count + 1     && self.levelPagination.hasMoreToLoad && !isFetchingData {
                fetchLevels()
            }
        case .qualification:
            break
        case .experience:
            break
        case .teachingSince:
            break
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if screenType == .subject || screenType == .level {
            guard indexPath.row != totalRows - 1 && indexPath.row != 0 else {
                return
            }
        } else if indexPath.row == 0 {
            return
        }
        
        switch screenType {
        case .subject:
            let subject = subjectPagination.subjects[indexPath.row - 1]
            let index = selectedSubjectsArray.index { (sub) -> Bool in
                return sub.id == subject.id
            }
            if isMultiSelect {
                if let subIndex = index, subIndex > -1 {
                    selectedSubjectsArray.remove(at: subIndex)
                } else {
                    if selectedSubjectsArray.count == 0 {
                        selectedSubjectsArray.append(subject)
                        tableView.reloadData()
                    } else if selectedSubjectsArray.count == 1 {
                        if isSecondarySubjectAlertShown {
                            selectedSubjectsArray.append(subject)
                            tableView.reloadData()
                        } else {
                            var messageString = "Primary subject: ".localized + selectedSubjectsArray[0].name
                            messageString += ". We will also include trainers who teaches ".localized + subject.name
                            messageString += ". Proceed?".localized
                            let alert = UIAlertController(title: AppName, message: messageString, preferredStyle: .alert)
                            let yesAction = UIAlertAction(title: AlertButton.yes.description(), style: .default) { (action) in
                                self.isSecondarySubjectAlertShown = true
                                DispatchQueue.main.async {
                                    
                                    self.selectedSubjectsArray.append(subject)
                                    self.tableView.reloadData()
                                }
                            }
                            alert.addAction(yesAction)
                            let noAction = UIAlertAction(title: AlertButton.no.description(), style: .cancel, handler: nil)
                            alert.addAction(noAction)
                            self.present(alert, animated: true, completion: nil)
                        }
                    } else if selectedSubjectsArray.count < 4 {
                        selectedSubjectsArray.append(subject)
                        tableView.reloadData()
                    } else {
                        let alert = UIAlertController(title: AppName, message: AlertMessage.subjectLimit.description(), preferredStyle: .alert)
                        let okAction = UIAlertAction(title: AlertButton.ok.description(), style: .default, handler: nil)
                        alert.addAction(okAction)
                        self.present(alert, animated: true, completion: nil)
                    }
                    
                }
            } else {
                let sub = selectedSubjectsArray[safe: 0]
                if let previousSub = sub {
                    if subject.id == previousSub.id {
                        selectedSubjectsArray.removeAll()
                    } else {
                        selectedSubjectsArray.removeAll()
                        selectedSubjectsArray.append(subject)
                    }
                } else {
                    selectedSubjectsArray.append(subject)
                }
            }
            tableView.reloadData()
        case .level:
            let currentLevel = levelPagination.levels[indexPath.row - 1]
            if let selLevel = selectedLevel, selLevel.id == currentLevel.id {
                selectedLevel = nil
            } else {
                selectedLevel = currentLevel
            }
            tableView.reloadData()
        case .qualification:
            let currentQualification = qualifications[indexPath.row - 1]
            if let selQuali = selectedQualification, selQuali.id == currentQualification.id {
                selectedQualification = nil
            } else {
                selectedQualification = currentQualification
            }
            tableView.reloadData()
        case .experience:
            let currentExp = experiences[indexPath.row - 1]
            if let selExp = selectedExperience, selExp.name == currentExp.name {
                selectedExperience = nil
            } else {
                selectedExperience = currentExp
            }
            tableView.reloadData()
        case .teachingSince:
            let currentYear = String(nextYearValue - indexPath.row)
            if selectedYear == currentYear {
                selectedYear = ""
            } else {
                selectedYear = currentYear
            }
            tableView.reloadData()
        }
    }
    
}

extension FilterCateogrySelectionViewController: UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if isFetchingData == false, searchString != textField.text {
            switch screenType {
            case .subject:
                subjectPagination.paginationType = .new
                fetchSubjects()
            case .level:
                levelPagination.paginationType = .new
                fetchLevels()
            case .qualification:
                setQualificationsArrayFromBackup()
            case .experience:
                setExperiencesArrayFromBackup()
            case .teachingSince:
                print("Doesnt apply")
            }
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (textField.text?.isEmpty)! && string == " " {
            return false
        }
        let char = string.cString(using: String.Encoding.utf8)!
        let isBackSpace = strcmp(char, "\\b")
        if (isBackSpace == -92) {
            return true
        }
        return true
    }
}

extension FilterCateogrySelectionViewController {
    
    func fetchSubjects() {
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }
        searchString = searchTextField.text ?? ""

        if subjectPagination.paginationType != .old {
            self.subjectPagination.currentPageNumber = 1
            if isFirstTime == true {
                Utilities.showHUD(forView: self.view, excludeViews: [])
            } else {
                UIApplication.shared.isNetworkActivityIndicatorVisible = true
            }
            
        } else {
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
        }
        
        let subjectObserver = ApiManager.shared.apiService.fetchSubjects(subjectPagination.currentPageNumber, searchString)
        filterDisposable = subjectObserver.subscribe(onNext: {(paginationObject) in
            self.isFetchingData = false
            
            self.subjectPagination.appendDataFromObject(paginationObject)
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if self.subjectPagination.paginationType != .old && self.isFirstTime == true {
                    self.isFirstTime = false
                } else {
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                }
                self.tableView.reloadData()
            })
        }, onError: {(error) in
            self.isFetchingData = false
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
            })
        })
        filterDisposable.disposed(by: disposableBag)
    }

    func fetchLevels() {
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }
        let countryID = Utilities.getCurrentCountryCode()
        searchString = searchTextField.text ?? ""
        
        if levelPagination.paginationType != .old {
            self.levelPagination.currentPageNumber = 1
            if isFirstTime == true {
                Utilities.showHUD(forView: self.view, excludeViews: [])
            } else {
                UIApplication.shared.isNetworkActivityIndicatorVisible = true
            }
            
        } else {
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
        }
        
        let levelObserver = ApiManager.shared.apiService.fetchLevels(levelPagination.currentPageNumber, countryID, searchString)
        filterDisposable = levelObserver.subscribe(onNext: {(paginationObject) in
            self.isFetchingData = false
            
            self.levelPagination.appendDataFromObject(paginationObject)
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if self.levelPagination.paginationType != .old && self.isFirstTime == true {
                    self.isFirstTime = false
                } else {
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                }
                self.tableView.reloadData()
            })
        }, onError: {(error) in
            self.isFetchingData = false
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
            })
        })
        filterDisposable.disposed(by: disposableBag)
    }

    func fetchQualifications() {
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }
        searchString = searchTextField.text ?? ""
        
        if isFirstTime == true {
            Utilities.showHUD(forView: self.view, excludeViews: [])
        } else {
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
        }
        
        let qualificationObserver = ApiManager.shared.apiService.fetchQualifications()
        let qualifiationDisposable = qualificationObserver.subscribe(onNext: {(qualifications) in
            self.isFetchingData = false
            self.backupQualifications = qualifications
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if self.isFirstTime == true {
                    self.isFirstTime = false
                } else {
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                }
                self.setQualificationsArrayFromBackup()
            })
        }, onError: {(error) in
            self.isFetchingData = false
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
            })
        })
        qualifiationDisposable.disposed(by: disposableBag)
    }

    func fetchExperiences() {
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }
        searchString = searchTextField.text ?? ""
        
        if isFirstTime == true {
            Utilities.showHUD(forView: self.view, excludeViews: [])
        } else {
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
        }
        
        let experienceObserver = ApiManager.shared.apiService.fetchExperiences()
        let experienceDisposable = experienceObserver.subscribe(onNext: {(experiences) in
            self.isFetchingData = false
            self.backupExperiences = experiences
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if self.isFirstTime == true {
                    self.isFirstTime = false
                } else {
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                }
                self.setExperiencesArrayFromBackup()
            })
        }, onError: {(error) in
            self.isFetchingData = false
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
            })
        })
        experienceDisposable.disposed(by: disposableBag)
    }

    
}
