//
//  FilterOptionSectionHeaderTableViewCell.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 24/05/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit

class FilterOptionSectionHeaderTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    
    class func cellIdentifier() -> String {
        return "FilterOptionSectionHeaderTableViewCell"
    }
    
}
