//
//  FilterOptionDescriptionTableViewCell.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 24/05/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit

class FilterOptionDescriptionTableViewCell: UITableViewCell {

    @IBOutlet weak var descriptionLabel: UILabel!
    
    class func cellIdentifier() -> String {
        return "FilterOptionDescriptionTableViewCell"
    }

}
