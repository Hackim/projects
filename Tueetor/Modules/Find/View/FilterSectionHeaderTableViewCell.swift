//
//  FilterSectionHeaderTableViewCell.swift
//  Tueetor
//
//  Created by Phaninder on 16/05/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit

protocol FilterSectionHeaderTableViewCellDelegate {
    func checkboxButtonTappedForSection(_ section: Int)
}

class FilterSectionHeaderTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var anywhereLabel: UILabel!
    @IBOutlet weak var separatorView: UIView!
    @IBOutlet weak var checkboxButton: UIButton!
    @IBOutlet weak var checkboxHolderView: UIView!
    @IBOutlet weak var checkboxHolderViewWidthConstraint: NSLayoutConstraint!
    
    var delegate: FilterSectionHeaderTableViewCellDelegate?
    
    var titles = ["Subjects".localized, "Levels".localized, "Teaching Qualification".localized, "Teaching Experience".localized, "Budget".localized, "Rating".localized, "Location".localized, "Search User".localized, "Distance".localized, "Availability".localized]
    
    
    class func cellIdentifier() -> String {
        return "FilterSectionHeaderTableViewCell"
    }
    
    class func sectionHeight() -> CGFloat {
        return 69.0
    }
    
    func configureSectionForType(_ section: Int, isSelected: Bool) {
        checkboxHolderView.isHidden = true
        checkboxHolderViewWidthConstraint.constant = 0
        separatorView.isHidden = false
        checkboxButton.tag = section
        titleLabel.text = titles[section]
        switch section {
        case FilterTableViewSectionType.subjects.rawValue:
            self.separatorView.isHidden = true
        case FilterTableViewSectionType.distance.rawValue,
             FilterTableViewSectionType.availability.rawValue,
            FilterTableViewSectionType.rating.rawValue:
            checkboxHolderView.isHidden = false
            checkboxHolderViewWidthConstraint.constant = 120.0
            if section == FilterTableViewSectionType.distance.rawValue {
                anywhereLabel.text = "Anywhere".localized
            } else if section == FilterTableViewSectionType.availability.rawValue {
                anywhereLabel.text = "Anytime".localized
            } else if section == FilterTableViewSectionType.rating.rawValue {
                anywhereLabel.text = "Any".localized
            }

            let imageName = isSelected ? "checkbox" : "checkbox_empty"
            checkboxButton.setImage(UIImage.init(named: imageName), for: .normal)
        default:
            break
        }
    }
    
    func configureCellWithSelection(_ isCheckBoxSelected: Bool) {
        let imageName = isCheckBoxSelected ? "checkbox" : "checkbox_empty"
        checkboxButton.setImage(UIImage.init(named: imageName), for: .normal)
    }

    @IBAction func checkboxButtonTapped(_ sender: UIButton) {
        delegate?.checkboxButtonTappedForSection(sender.tag)
    }

}
