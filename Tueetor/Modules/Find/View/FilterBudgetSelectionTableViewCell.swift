//
//  FilterBudgetSelectionTableViewCell.swift
//  Tueetor
//
//  Created by Phaninder on 17/05/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit

protocol FilterBudgetSelectionTableViewCellDelegate {
    
    func perSessionSliderIsUpdated(lowerValue: Int, upperValue: Int)
    func perMonthSliderIsUpdated(lowerValue: Int, upperValue: Int)
}

class FilterBudgetSelectionTableViewCell: UITableViewCell {

    @IBOutlet weak var perSessionDisplayLabel: UILabel!
    @IBOutlet weak var perSessionSlider: RangeSeekSlider!
    @IBOutlet weak var perMonthDisplayLabel: UILabel!
    @IBOutlet weak var perMonthSlider: RangeSeekSlider!
    
    var delegate: FilterBudgetSelectionTableViewCellDelegate?
    var filterParams: FilterParams!
    
    class func cellIdentifier() -> String {
        return "FilterBudgetSelectionTableViewCell"
    }
    
    class func cellHeight() -> CGFloat {
        return 275.0
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        perSessionSlider.hideLabels = true
        perSessionSlider.handleImage = UIImage.init(named: "slider_handle")
        perSessionSlider.lineHeight = 4
        perSessionSlider.tintColor = themeLightGrayColor
        perSessionSlider.minValue = 0
        perSessionSlider.maxValue = 1000
        perSessionSlider.selectedMinValue = CGFloat(filterParams.perSessionMinimum)
        perSessionSlider.selectedMaxValue = CGFloat(filterParams.perSessionMaximum)
        perSessionSlider.tag = 0
        perSessionSlider.enableStep = true
        perSessionSlider.step = 1
        perSessionSlider.delegate = self
        perSessionSlider.colorBetweenHandles = themeBlueColor
        
        perMonthSlider.hideLabels = true
        perMonthSlider.handleImage = UIImage.init(named: "slider_handle")
        perMonthSlider.lineHeight = 4
        perMonthSlider.tintColor = themeLightGrayColor
        perMonthSlider.minValue = 0
        perMonthSlider.maxValue = 10000
        perMonthSlider.selectedMinValue = CGFloat(filterParams.perMonthMinimum)
        perMonthSlider.selectedMaxValue = CGFloat(filterParams.perMonthMaximum)
        perMonthSlider.tag = 1
        perMonthSlider.enableStep = true
        perMonthSlider.step = 1
        perMonthSlider.delegate = self
        perMonthSlider.colorBetweenHandles = themeBlueColor
    }
    
    func configureCellWithSliderValues(_ filterParams: FilterParams) {
        self.filterParams = filterParams
        let currencyCode = Utilities.getCountryCurrencySymbol(filterParams.countryID)
        perSessionDisplayLabel.text = "\(currencyCode)\(filterParams.perSessionMinimum) - \(currencyCode)\(filterParams.perSessionMaximum)"
        perMonthDisplayLabel.text = "\(currencyCode)\(filterParams.perMonthMinimum) - \(currencyCode)\(filterParams.perMonthMaximum)"
        perSessionSlider.selectedMinValue = CGFloat(filterParams.perSessionMinimum)
        perSessionSlider.selectedMaxValue = CGFloat(filterParams.perSessionMaximum)
        perMonthSlider.selectedMinValue = CGFloat(filterParams.perMonthMinimum)
        perMonthSlider.selectedMaxValue = CGFloat(filterParams.perMonthMaximum)
    }
}

extension FilterBudgetSelectionTableViewCell: RangeSeekSliderDelegate {
    
    func rangeSeekSlider(_ slider: RangeSeekSlider, didChange minValue: CGFloat, maxValue: CGFloat) {
        
        if slider.tag == 0 {
            perSessionDisplayLabel.text = "$\(Int(minValue)) - $\(Int(maxValue))"
        } else {
            perMonthDisplayLabel.text = "$\(Int(minValue)) - $\(Int(maxValue))"
        }
    }
    
    func didEndTouches(in slider: RangeSeekSlider) {
        if slider.tag == 0 {
            delegate?.perSessionSliderIsUpdated(lowerValue: Int(slider.selectedMinValue), upperValue: Int(slider.selectedMaxValue))
        } else {
            delegate?.perMonthSliderIsUpdated(lowerValue: Int(slider.selectedMinValue), upperValue: Int(slider.selectedMaxValue))
        }
    }
    
}
