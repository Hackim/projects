//
//  FilterLocationTableViewCell.swift
//  Tueetor
//
//  Created by Phaninder on 17/05/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit

protocol FilterLocationTableViewCellDelegate {
//    func addressRadioButtonTapped()
//    func nameRadioButtonTapped()
    func selectionChanged(isLocationSelected: Bool, isNameSelected: Bool)
    func showPlacePicker()
}

class FilterLocationTableViewCell: UITableViewCell {

    @IBOutlet weak var addressTextField: UITextField!
    @IBOutlet weak var placePickerButton: UIButton!
    
    var delegate: FilterLocationTableViewCellDelegate?
    
    class func cellIdentifier() -> String {
        return "FilterLocationTableViewCell"
    }
    
    class func cellHeight() -> CGFloat {
        return 104.0
    }
    
    func configureTextFieldsWithLocation(_ location: String) {
        addressTextField.text = location
    }
    
    @IBAction func placePickerButtonTapped(_ sender: UIButton) {
        delegate?.showPlacePicker()
    }
    
}
