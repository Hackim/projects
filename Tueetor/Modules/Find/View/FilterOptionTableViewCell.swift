//
//  FilterOptionTableViewCell.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 24/05/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit

class FilterOptionTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var radioButton: UIButton!
    
    class func cellIdentifier() -> String {
        return "FilterOptionTableViewCell"
    }
    
    func configureCellWithSubject(_ subject: Subject, isSelected: Bool) {
        titleLabel.text = subject.name
        let imageName = isSelected ? "checkbox" : "checkbox_empty"
        let color = isSelected ? themeBlueColor : themeBlueColor
        radioButton.setImage(UIImage(named: imageName), for: .normal)
        titleLabel.textColor = color
    }
    
    func configureCellWithLevel(_ level: Level, isSelected: Bool) {
        titleLabel.text = level.name
        let imageName = isSelected ? "checkbox" : "checkbox_empty"
        let color = isSelected ? themeBlueColor : themeBlueColor
        radioButton.setImage(UIImage(named: imageName), for: .normal)
        titleLabel.textColor = color
    }

    func configureCellWithQualification(_ qualification: Qualification, isSelected: Bool) {
        titleLabel.text = qualification.name
        let imageName = isSelected ? "checkbox" : "checkbox_empty"
        let color = isSelected ? themeBlueColor : themeBlueColor
        radioButton.setImage(UIImage(named: imageName), for: .normal)
        titleLabel.textColor = color
    }

    func configureCellWithExperience(_ experience: Experience, isSelected: Bool) {
        titleLabel.text = experience.name
        let imageName = isSelected ? "checkbox" : "checkbox_empty"
        let color = isSelected ? themeBlueColor : themeBlueColor
        radioButton.setImage(UIImage(named: imageName), for: .normal)
        titleLabel.textColor = color
    }

    func configureCellWithYear(_ year: String, isSelected: Bool) {
        titleLabel.text = year
        let imageName = isSelected ? "checkbox" : "checkbox_empty"
        let color = isSelected ? themeBlueColor : themeBlueColor
        radioButton.setImage(UIImage(named: imageName), for: .normal)
        titleLabel.textColor = color
    }

}
