//
//  FilterRatingTableViewCell.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 01/06/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit
import SwiftyStarRatingView

protocol FilterRatingTableViewCellDelegate {
    func ratingValueChanged(_ ratingValue: CGFloat)
}


class FilterRatingTableViewCell: UITableViewCell {

    @IBOutlet weak var ratingView: SwiftyStarRatingView!

    var delegate: FilterRatingTableViewCellDelegate?
    
    class func cellIdentifier() -> String {
        return "FilterRatingTableViewCell"
    }
    
    class func cellHeight() -> CGFloat {
        return 76.0
    }
    
    func configureCellWithRating(_ rating: CGFloat, isAnyRatingSelected: Bool) {
        ratingView.value = rating
        ratingView.addTarget(self, action: #selector(ratingValueChanged), for: .valueChanged)
        if isAnyRatingSelected {
            ratingView.value = 0
        }
    }
    
    @objc func ratingValueChanged() {
        delegate?.ratingValueChanged(ratingView.value)
    }
    
}
