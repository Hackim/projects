//
//  FilterDistanceSelectionTableViewCell.swift
//  Tueetor
//
//  Created by Phaninder on 17/05/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit

protocol FilterDistanceSelectionTableViewCellDelegate {
    func distanceSelected(_ distance: Int)
}
class FilterDistanceSelectionTableViewCell: UITableViewCell {

    @IBOutlet weak var distanceDisplayLabel: UILabel!
    @IBOutlet weak var distanceSlider: RangeSeekSlider!
    var filterParams: FilterParams!

    var delegate: FilterDistanceSelectionTableViewCellDelegate?
    
    class func cellIdentifier() -> String {
        return "FilterDistanceSelectionTableViewCell"
    }
    
    class func cellHeight() -> CGFloat {
        return 115.0
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        distanceSlider.hideLabels = true
        distanceSlider.handleImage = UIImage.init(named: "slider_handle")
        distanceSlider.lineHeight = 4
        distanceSlider.tintColor = themeLightGrayColor
        distanceSlider.minValue = 0
        distanceSlider.maxValue = 10000
        distanceSlider.selectedMaxValue = CGFloat(filterParams.distance)

        distanceSlider.enableStep = true
        distanceSlider.step = 10
        distanceSlider.delegate = self
        distanceSlider.disableRange = true
        distanceSlider.colorBetweenHandles = themeBlueColor
    }
    
    func configureCellWithDistance(_ distance: Int) {
        distanceDisplayLabel.text = "\(distance) m"
        distanceSlider.selectedMaxValue = CGFloat(distance)
    }
    
}

extension FilterDistanceSelectionTableViewCell: RangeSeekSliderDelegate {
    
    func rangeSeekSlider(_ slider: RangeSeekSlider, didChange minValue: CGFloat, maxValue: CGFloat) {
        distanceDisplayLabel.text = "\(Int(maxValue)) m"
    }
    
    func didEndTouches(in slider: RangeSeekSlider) {
        delegate?.distanceSelected(Int(slider.selectedMaxValue))
    }

    
}
