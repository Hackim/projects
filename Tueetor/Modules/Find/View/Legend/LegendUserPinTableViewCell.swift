//
//  LegendUserPinTableViewCell.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 28/06/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit

class LegendUserPinTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var checkBoxButton: UIButton!
    
    class func cellIdentifier() -> String {
        return "LegendUserPinTableViewCell"
    }
    
    class func cellHeight() -> CGFloat {
        return 69.0
    }
    
    func configureCellWithType(_ type: Int, filter: FilterParams) {
        var isSelected = false
        var title = "Student Subject Pin".localized
        var imageName = "bluePin"
        switch type {
        case LegendType.studentSubject.rawValue:
            title = "Student Subject Pin".localized
            isSelected = filter.showStudentSubPin
            imageName = "bluePin"
            
        case LegendType.tutorSubject.rawValue:
            title = "Tutor Subject Pin".localized
            isSelected = filter.showTutorSubPin
            imageName = "greenPin"

        case LegendType.multiMatch.rawValue:
            title = "Matches Your Subject & Level".localized
            isSelected = filter.showMatchingSubLevelPin
            imageName = "orangePin"

        case LegendType.subjectMatch.rawValue:
            title = "Matches Your Subject Only".localized
            isSelected = filter.showMatchinSubPin
            imageName = "purplePin"

        default:
            break
        }
        nameLabel.text = title
        let checkboxImage = isSelected ? "checkbox" : "checkbox_empty"
        checkBoxButton.setImage(UIImage(named: checkboxImage), for: .normal)
        iconImageView.image = UIImage(named: imageName)
    }
    
}
