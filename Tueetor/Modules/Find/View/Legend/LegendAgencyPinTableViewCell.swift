//
//  LegendAgencyPinTableViewCell.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 28/06/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit

class LegendAgencyPinTableViewCell: UITableViewCell {

    @IBOutlet weak var checkBoxButton: UIButton!
    class func cellIdentifier() -> String {
        return "LegendAgencyPinTableViewCell"
    }
    
    class func cellHeight() -> CGFloat {
        return 79.0
    }
    
    func configureCellWithType(filter: FilterParams) {
        let imageName = filter.showAgencyPin ? "checkbox" : "checkbox_empty"
        checkBoxButton.setImage(UIImage(named: imageName), for: .normal)
    }

}
