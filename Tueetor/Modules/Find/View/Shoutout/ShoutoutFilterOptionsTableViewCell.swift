//
//  ShoutoutFilterOptionsTableViewCell.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 27/08/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit

protocol ShoutoutFilterOptionsTableViewCellDelegate {
    
    func filterOptionTappedForSection(_ section: Int)
    func sortOptionTappedForSection(_ section: Int)
    
}

class ShoutoutFilterOptionsTableViewCell: UITableViewCell {

    @IBOutlet weak var countLabel: UILabel!
    @IBOutlet weak var refineButton: UIButton!
    @IBOutlet weak var sortButton: UIButton!
    
    var delegate: ShoutoutFilterOptionsTableViewCellDelegate?
    var isRefineHighlightedState = false
    var isSortHighlightedState = false
    
    class func cellIdentifier() -> String {
        return "ShoutoutFilterOptionsTableViewCell"
    }
    
    class func cellHeight() -> CGFloat {
        return 60.0
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        refineButton.layer.cornerRadius = 20
        refineButton.layer.borderColor = isRefineHighlightedState ? themeBlueColor.cgColor : themeBlackColor.cgColor
        refineButton.backgroundColor = isRefineHighlightedState ? themeBlueColor : UIColor.white
        refineButton.setTitleColor(isRefineHighlightedState ? UIColor.white : themeBlackColor, for: .normal)
        refineButton.layer.borderWidth = 1.0
        
        // Sort Button
        sortButton.layer.cornerRadius = 20
        sortButton.layer.borderColor = isSortHighlightedState ? themeBlueColor.cgColor : themeBlackColor.cgColor
        sortButton.layer.borderWidth = 1.0
        sortButton.backgroundColor = isSortHighlightedState ? themeBlueColor : UIColor.white
        sortButton.setTitleColor(isSortHighlightedState ? UIColor.white : themeBlackColor, for: .normal)
        
        refineButton.addTarget(self, action: #selector(refineTouchUpInside), for: UIControlEvents.touchUpInside)
        refineButton.addTarget(self, action: #selector(refineDragExit), for: UIControlEvents.touchDragExit)
        refineButton.addTarget(self, action: #selector(refineHoldDown), for: UIControlEvents.touchDown)
        refineButton.layer.shadowPath = UIBezierPath(roundedRect:
            refineButton.bounds, cornerRadius: self.refineButton.layer.cornerRadius).cgPath
        refineButton.layer.shadowColor = !isRefineHighlightedState ? themeBlackColor.cgColor : themeBlueColor.cgColor
        refineButton.layer.shadowOffset = CGSize(width: 0, height: 0)
        refineButton.layer.shadowRadius = 5
        refineButton.layer.masksToBounds = false
        refineButton.layer.shadowOpacity = 0.4
        
        sortButton.addTarget(self, action: #selector(sortTouchUpInside), for: UIControlEvents.touchUpInside)
        sortButton.addTarget(self, action: #selector(sortDragExit), for: UIControlEvents.touchDragExit)
        sortButton.addTarget(self, action: #selector(sortHoldDown), for: UIControlEvents.touchDown)
        sortButton.layer.shadowPath = UIBezierPath(roundedRect:
                    sortButton.bounds, cornerRadius: self.sortButton.layer.cornerRadius).cgPath
        sortButton.layer.shadowColor = !isSortHighlightedState ? themeBlackColor.cgColor : themeBlueColor.cgColor
        sortButton.layer.shadowOffset = CGSize(width: 0, height: 0)
        sortButton.layer.shadowRadius = 5
        sortButton.layer.shadowOpacity = 0.4
        sortButton.layer.masksToBounds = false
    }
    
    @objc func refineHoldDown() {
        self.isRefineHighlightedState = true
        self.layoutSubviews()
    }
    
    @objc func refineTouchUpInside() {
        self.isRefineHighlightedState = false
        self.layoutSubviews()
        delegate?.filterOptionTappedForSection(refineButton.tag)
    }
    
    @objc func refineDragExit() {
        self.isRefineHighlightedState = false
        self.layoutSubviews()
    }
    
    @objc func sortHoldDown() {
        self.isSortHighlightedState = true
        self.layoutSubviews()
    }
    
    @objc func sortTouchUpInside() {
        self.isSortHighlightedState = false
        self.layoutSubviews()
        delegate?.sortOptionTappedForSection(refineButton.tag)
    }
    
    @objc func sortDragExit() {
        self.isSortHighlightedState = false
        self.layoutSubviews()
    }

    func configureCellWithCount(_ count: Int) {
        countLabel.text = String(count)
    }

}
