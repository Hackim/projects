//
//  ShoutoutShowAllButtonTableViewCell.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 30/08/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit

protocol ShoutoutShowAllButtonTableViewCellDelegate {
    func showAllButtonTapped(_ button: UIButton)
}

class ShoutoutShowAllButtonTableViewCell: UITableViewCell {

    @IBOutlet weak var showAllButton: UIButton!
    
    var delegate: ShoutoutShowAllButtonTableViewCellDelegate?
    
    class func cellIdentifier() -> String {
        return "ShoutoutShowAllButtonTableViewCell"
    }
    
    @IBAction func showAllButtonTapped(_ sender: UIButton) {
        delegate?.showAllButtonTapped(sender)
    }
    
}
