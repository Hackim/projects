//
//  ShoutoutMessageTableViewCell.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 30/08/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.


import UITextView_Placeholder
import UIKit

class ShoutoutMessageTableViewCell: UITableViewCell, UITextViewDelegate {

    @IBOutlet weak var messageTextView: UITextView!
    var placeholderLabel : UILabel!

    override func layoutSubviews() {
        super.layoutSubviews()
        messageTextView.delegate = self
        messageTextView.layer.cornerRadius = 5.0
        messageTextView.layer.borderWidth = 1.0
        messageTextView.layer.borderColor = themeBlueColor.cgColor
        messageTextView.clipsToBounds = true
        messageTextView.delegate = self
        placeholderLabel = UILabel()
        let placeholderText = "Enter your message here.".localized
        placeholderLabel.text = placeholderText
        
        placeholderLabel.font = UIFont(name: "Montserrat-Light", size: 18.0)!
        
        placeholderLabel.sizeToFit()
        messageTextView.placeholder = "Enter your message here.".localized
        messageTextView.placeholderColor = themeBlueColor
       // messageTextView.addSubview(placeholderLabel)
        placeholderLabel.numberOfLines = 0
        placeholderLabel.frame = CGRect(x: 5, y: (messageTextView.font?.pointSize)! / 2, width: messageTextView.frame.size.width - 5, height: 200)
        let height = placeholderLabel.heightForText()
        placeholderLabel.frame = CGRect(x: 5, y: (messageTextView.font?.pointSize)! / 2, width: messageTextView.frame.size.width - 5, height: height)
        placeholderLabel.textColor = themeBlueColor
        placeholderLabel.isHidden = !messageTextView.text.isEmpty
    }
    
    class func cellIdentifier() -> String {
        return "ShoutoutMessageTableViewCell"
    }
    
    class func cellHeight() -> CGFloat {
        return 200.0
    }

    func textViewDidChange(_ textView: UITextView) {
        //placeholderLabel.isHidden = !textView.text.isEmpty
        placeholderLabel.removeFromSuperview()
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if (textView.text?.isEmpty)! && text == " " {
            return false
        }
        let char = text.cString(using: String.Encoding.utf8)!
        let isBackSpace = strcmp(char, "\\b")
        if (isBackSpace == -92) {
            return true
        }
        return true
        
    }

}
