//
//  SectionDescriptionTableViewCell.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 22/08/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit

class SectionDescriptionTableViewCell: UITableViewCell {

    @IBOutlet weak var messageLabel: UILabel!
    
    class func cellIdentifier() -> String {
        return "SectionDescriptionTableViewCell"
    }
    
    
}
