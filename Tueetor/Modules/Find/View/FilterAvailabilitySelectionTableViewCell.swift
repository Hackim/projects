//
//  FilterAvailabilitySelectionTableViewCell.swift
//  Tueetor
//
//  Created by Phaninder on 17/05/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit

class FilterAvailabilitySelectionTableViewCell: UITableViewCell {

    @IBOutlet weak var collectionView: UICollectionView!
    var filterParams: FilterParams!
    
    class func cellIdentifier() -> String {
        return "FilterAvailabilitySelectionTableViewCell"
    }
    
    class func cellHeight() -> CGFloat {
        return 360.0
    }
    
}

extension FilterAvailabilitySelectionTableViewCell: UICollectionViewDelegateFlowLayout, UICollectionViewDelegate, UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 8
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard indexPath.item != 0 else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: FilterAvailabilityStaticCollectionViewCell.cellIdentifier(), for: indexPath) as! FilterAvailabilityStaticCollectionViewCell
            return cell
        }
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: FilterAvailabilityTimingCollectionViewCell.cellIdentifier(), for: indexPath) as! FilterAvailabilityTimingCollectionViewCell
        cell.configureCellForDay(indexPath.item, filterParams: filterParams)
        return cell

    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if indexPath.item == 0 {
            return CGSize(width: 50, height: 281)
        } else {
            return CGSize(width: (ScreenWidth - 55)/7, height: 281)
        }
    }
    
}
