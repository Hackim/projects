//
//  FilterShowAllButtonTableViewCell.swift
//  Tueetor
//
//  Created by Phaninder on 16/05/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit

class FilterShowAllButtonTableViewCell: UITableViewCell {

    class func cellIdentifier() -> String {
        return "FilterShowAllButtonTableViewCell"
    }
    
    class func cellHeight() -> CGFloat {
        return 50.0
    }

    
}
