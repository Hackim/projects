//
//  SortTableViewCell.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 31/08/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit

class SortTableViewCell: UITableViewCell {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var sortIcon: UIImageView!

    class func cellIdentifier() -> String {
        return "SortTableViewCell"
    }
    
    class func cellHeight() -> CGFloat {
        return 60.0
    }
    
    func configureCellForIndexPath(_ indexPath: IndexPath, filterParams: FilterParams) {
        let sortBy = filterParams.sortBy
        switch indexPath.row {
        case 1:
            nameLabel.text = "Qualification".localized
            nameLabel.textColor = themeBlackColor
            sortIcon.image = UIImage(named: "noSort")
            if sortBy == .qualifi_asc {
                nameLabel.textColor = themeBlueColor
                sortIcon.image = UIImage(named: "ascendingSort")
            } else if sortBy == .qualifi_desc {
                nameLabel.textColor = themeBlueColor
                sortIcon.image = UIImage(named: "descendingSort")
            }
            break
        case 2:
            nameLabel.text = "Teaching Experience".localized
            nameLabel.textColor = themeBlackColor
            sortIcon.image = UIImage(named: "noSort")
            if sortBy == .exp_asc {
                nameLabel.textColor = themeBlueColor
                sortIcon.image = UIImage(named: "ascendingSort")
            } else if sortBy == .exp_desc {
                nameLabel.textColor = themeBlueColor
                sortIcon.image = UIImage(named: "descendingSort")
            }
            break
        case 3:
            nameLabel.text = "Rate/Budget Per Session".localized
            nameLabel.textColor = themeBlackColor
            sortIcon.image = UIImage(named: "noSort")
            if sortBy == .session_asc {
                nameLabel.textColor = themeBlueColor
                sortIcon.image = UIImage(named: "ascendingSort")
            } else if sortBy == .session_desc {
                nameLabel.textColor = themeBlueColor
                sortIcon.image = UIImage(named: "descendingSort")
            }
            break
        case 4:
            nameLabel.text = "Rate/Budget Per Month".localized
            nameLabel.textColor = themeBlackColor
            sortIcon.image = UIImage(named: "noSort")
            if sortBy == .month_asc {
                nameLabel.textColor = themeBlueColor
                sortIcon.image = UIImage(named: "ascendingSort")
            } else if sortBy == .month_desc {
                nameLabel.textColor = themeBlueColor
                sortIcon.image = UIImage(named: "descendingSort")
            }
            break
        case 5:
            nameLabel.text = "Distance".localized
            nameLabel.textColor = themeBlackColor
            sortIcon.image = UIImage(named: "noSort")
            if sortBy == .dist_asc {
                nameLabel.textColor = themeBlueColor
                sortIcon.image = UIImage(named: "ascendingSort")
            } else if sortBy == .dist_desc {
                nameLabel.textColor = themeBlueColor
                sortIcon.image = UIImage(named: "descendingSort")
            }
            break
        case 6:
            nameLabel.text = "Rating".localized
            nameLabel.textColor = themeBlackColor
            sortIcon.image = UIImage(named: "noSort")
            if sortBy == .rating_asc {
                nameLabel.textColor = themeBlueColor
                sortIcon.image = UIImage(named: "ascendingSort")
            } else if sortBy == .rating_desc {
                nameLabel.textColor = themeBlueColor
                sortIcon.image = UIImage(named: "descendingSort")
            }
            break
        default:
            break
        }
    }
    
}
