//
//  FilterAvailabilityTimingCollectionViewCell.swift
//  Tueetor
//
//  Created by Phaninder on 17/05/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit

class FilterAvailabilityTimingCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var unselectedDayLabel: UILabel!
    @IBOutlet weak var selectedDayLabel: UILabel!
    @IBOutlet weak var selectedDayHolderView: UIView!
    @IBOutlet weak var selectionIndicatorImageView: UIImageView!
    @IBOutlet weak var selectDayButton: UIButton!
    
    @IBOutlet weak var slotOneSelectionButton: UIButton!
    @IBOutlet weak var slotTwoSelectionButton: UIButton!
    @IBOutlet weak var slotThreeSelectionButton: UIButton!
    @IBOutlet weak var slotFourSelectionButton: UIButton!

    var days = ["MON", "TUE", "WED", "THU", "FRI", "SAT", "SUN"]
    var day = "MON"
    var filterParams: FilterParams!
    
    class func cellIdentifier() -> String {
        return "FilterAvailabilityTimingCollectionViewCell"
    }

    func configureCellForDay(_ day: Int, filterParams: FilterParams) {
        let dayString = days[day - 1]
        self.day = dayString
        self.filterParams = filterParams
        slotOneSelectionButton.tag = 0
        slotTwoSelectionButton.tag = 1
        slotThreeSelectionButton.tag = 2
        slotFourSelectionButton.tag = 3
        unselectedDayLabel.text = dayString
        selectedDayLabel.text = dayString
        let schedule = filterParams.selectedTimeSlots[dayString]!
        for i in 0..<schedule.count {
            let isSelected = schedule[i] == "0" ? false : true
            switch i {
            case 0:
                let imageName = isSelected ? "checkbox" : "checkbox_empty"
                slotOneSelectionButton.setImage(UIImage.init(named: imageName), for: .normal)

            case 1:
                let imageName = isSelected ? "checkbox" : "checkbox_empty"
                slotTwoSelectionButton.setImage(UIImage.init(named: imageName), for: .normal)

            case 2:
                let imageName = isSelected ? "checkbox" : "checkbox_empty"
                slotThreeSelectionButton.setImage(UIImage.init(named: imageName), for: .normal)

            default:
                let imageName = isSelected ? "checkbox" : "checkbox_empty"
                slotFourSelectionButton.setImage(UIImage.init(named: imageName), for: .normal)

            }
            let hasSelected = isAnyOneSlotSelected()
            selectionIndicatorImageView.isHidden = !hasSelected
            selectedDayLabel.isHidden = !hasSelected
        }
    }
    
    @IBAction func slotOneButtonTapped(_ sender: UIButton) {
        handleTapForButton(sender)
    }
    
    @IBAction func slotTwoButtonTapped(_ sender: UIButton) {
        handleTapForButton(sender)
    }
    
    @IBAction func slowThreeButtonTapped(_ sender: UIButton) {
        handleTapForButton(sender)
    }
    
    @IBAction func slotFourButtonTapped(_ sender: UIButton) {
        handleTapForButton(sender)
    }
    
    func handleTapForButton(_ checkboxButton: UIButton) {
        let isSelected = checkboxButton.hasImage(named: "checkbox", for: .normal)
        var selectionArray = self.filterParams.selectedTimeSlots[self.day]!
        selectionArray[checkboxButton.tag] = isSelected ? "0" : "1"
        self.filterParams.selectedTimeSlots[self.day] = selectionArray
        let imageName = isSelected ? "checkbox_empty" : "checkbox"
        checkboxButton.setImage(UIImage.init(named: imageName), for: .normal)
        let hasSelected = isAnyOneSlotSelected()
        selectionIndicatorImageView.isHidden = !hasSelected
        selectedDayLabel.isHidden = !hasSelected
        NotificationCenter.default.post(name: .filterAvailabilitySelected, object: nil, userInfo:nil)
    }
    
    func isAnyOneSlotSelected() -> Bool {
        let isSlotOneSelected = slotOneSelectionButton.hasImage(named: "checkbox", for: .normal)
        let isSlotTwoSelected = slotTwoSelectionButton.hasImage(named: "checkbox", for: .normal)
        let isSlotThreeSelected = slotThreeSelectionButton.hasImage(named: "checkbox", for: .normal)
        let isSlotFourSelected = slotFourSelectionButton.hasImage(named: "checkbox", for: .normal)
        
        return isSlotOneSelected || isSlotTwoSelected || isSlotThreeSelected || isSlotFourSelected
    }
    
}
