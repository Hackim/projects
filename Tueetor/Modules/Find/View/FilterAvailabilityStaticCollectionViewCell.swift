//
//  FilterAvailabilityStaticCollectionViewCell.swift
//  Tueetor
//
//  Created by Phaninder on 17/05/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit

class FilterAvailabilityStaticCollectionViewCell: UICollectionViewCell {
    
    class func cellIdentifier() -> String {
        return "FilterAvailabilityStaticCollectionViewCell"
    }

}
