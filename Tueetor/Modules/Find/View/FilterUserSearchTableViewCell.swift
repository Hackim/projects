//
//  FilterUserSearchTableViewCell.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 26/05/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit

protocol FilterUserSearchTableViewCellDelegate {
    func goButtonTapped()
}

class FilterUserSearchTableViewCell: UITableViewCell {

    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var goButton: UIButton!
    
    var delegate: FilterUserSearchTableViewCellDelegate?
    
    class func cellIdentifier() -> String {
        return "FilterUserSearchTableViewCell"
    }
    
    class func cellHeight() -> CGFloat {
        return 104.0
    }
    
    func configureTextFieldsWithText(_ searchText: String) {
        searchTextField.text = searchText
    }
    @IBAction func goButtonTapped(_ sender: UIButton) {
        delegate?.goButtonTapped()
    }
    
}
