//
//  FilterCheckboxTableViewCell.swift
//  Tueetor
//
//  Created by Phaninder on 16/05/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit


class FilterCheckboxTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var checkboxButton: UIButton!

    var indexPath: IndexPath!
    
    class func cellIdentifier() -> String {
        return "FilterCheckboxTableViewCell"
    }
    
    class func cellHeight() -> CGFloat {
        return 50.0
    }
    
    func configureCellWithTitle(_ title: String, isChecked: Bool, index: IndexPath) {
        indexPath = index
        titleLabel.text = title
        let imageName = isChecked ? "checkbox" : "checkbox_empty"
        checkboxButton.setImage(UIImage.init(named: imageName), for: .normal)
    }

    
}
