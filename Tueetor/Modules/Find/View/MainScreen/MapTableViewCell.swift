//
//  MapTableViewCell.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 26/06/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit
import GoogleMaps

protocol MapTableViewCellDelegate {
    func showShoutoutScreen()
    func showFindNowScreen()
    func showLegendScreen()
}
class MapTableViewCell: UITableViewCell {

    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var shoutoutImageView: UIImageView!
    @IBOutlet weak var findNowImageView: UIImageView!
    @IBOutlet weak var legendImageView: UIImageView!
    @IBOutlet weak var findNowButton: UIButton!
    @IBOutlet weak var legendButton: UIButton!
    @IBOutlet weak var shoutoutButton: UIButton!

    var delegate: MapTableViewCellDelegate?
    

    class func cellIdentifier() -> String {
        return "MapTableViewCell"
    }
    
    class func cellHeight() -> CGFloat {
        return 350.0
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        shoutoutButton.addTarget(self, action: #selector(shoutOutHoldRelease), for: UIControlEvents.touchUpInside)
        shoutoutButton.addTarget(self, action: #selector(shoutOutHoldRelease), for: UIControlEvents.touchDragExit);
        shoutoutButton.addTarget(self, action: #selector(shoutOutHoldDown), for: UIControlEvents.touchDown)
        
        legendButton.addTarget(self, action: #selector(legendHoldRelease), for: UIControlEvents.touchUpInside)
        legendButton.addTarget(self, action: #selector(legendHoldRelease), for: UIControlEvents.touchDragExit);
        legendButton.addTarget(self, action: #selector(legendHoldDown), for: UIControlEvents.touchDown)

        findNowButton.addTarget(self, action: #selector(findNowHoldRelease), for: UIControlEvents.touchUpInside)
        findNowButton.addTarget(self, action: #selector(findNowHoldRelease), for: UIControlEvents.touchDragExit);
        findNowButton.addTarget(self, action: #selector(findNowHoldDown), for: UIControlEvents.touchDown)


    }

    @IBAction func shoutoutButtonTapped(_ sender: UIButton) {
        delegate?.showShoutoutScreen()
    }
    
    @IBAction func findNowButtonTapped(_ sender: UIButton) {
        delegate?.showFindNowScreen()
    }
    
    @IBAction func legendButtonTapped(_ sender: UIButton) {
        delegate?.showLegendScreen()
    }
    
    @objc func shoutOutHoldDown() {
        shoutoutImageView.image = UIImage(named: "shoutout_active")
    }
    
    @objc func shoutOutHoldRelease() {
        shoutoutImageView.image = UIImage(named: "shoutout_inactive")
    }

    @objc func findNowHoldDown() {
        findNowImageView.image = UIImage(named: "findnow_active")
    }
    
    @objc func findNowHoldRelease() {
        findNowImageView.image = UIImage(named: "findnow_inactive")
    }

    @objc func legendHoldDown() {
        legendImageView.image = UIImage(named: "legends_active")
    }
    
    @objc func legendHoldRelease() {
        legendImageView.image = UIImage(named: "legends_inactive")
    }

    
}
