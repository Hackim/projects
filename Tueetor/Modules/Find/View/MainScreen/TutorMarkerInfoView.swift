//
//  TutorMarkerInfoView.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 26/06/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//


import UIKit
import SwiftyStarRatingView
import JSSAlertView

class TutorMarkerInfoView: UIView {
  
    @IBOutlet weak var holderView: UIView!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var ratingView: SwiftyStarRatingView!
    @IBOutlet weak var responseTimeLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var ratePerSessionLabel: UILabel!
    @IBOutlet weak var ratePerMonthLabel: UILabel!
    @IBOutlet weak var clockIcon: UIImageView!
    
    override func layoutSubviews() {
        super.layoutSubviews()
        holderView.layer.borderColor = UIColor.clear.cgColor
        holderView.layer.borderWidth = 1.5
        
    }
    
}
