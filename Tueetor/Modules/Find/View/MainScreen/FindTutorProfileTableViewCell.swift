//
//  FindTutorProfileTableViewCell.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 26/06/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit
import SwiftyStarRatingView
import SDWebImage

protocol FindTutorProfileTableViewCellDelegate {
    func selectMarkerForProfileAtIndex(_ index: Int)
    func showProfileAtIndex(_ index: Int)
    func messageProfileAtIndex(_ index: Int)
    func shortlistProfileAtIndex(_ index: Int)
    func reportProfileAtIndex(_ index: Int)
}

class FindTutorProfileTableViewCell: UITableViewCell {

    @IBOutlet weak var holderView: UIView!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var legacyHolderView: UIView!
    @IBOutlet weak var legacyButton: UIButton!
    
    @IBOutlet weak var ratingView: SwiftyStarRatingView!
    @IBOutlet weak var responseTimeLabel: UILabel!
    @IBOutlet weak var profileButtonHolderView: UIView!
    @IBOutlet weak var anotherProfileButtonHolderView: UIView!

    @IBOutlet weak var messageButtonHolderView: UIView!

    @IBOutlet weak var shortlistButtonHolderView: UIView!
    @IBOutlet weak var shortlistLabel: UILabel!

    @IBOutlet weak var reportButtonHolderView: UIView!
    @IBOutlet weak var anotherReportButtonHolderView: UIView!
    
    @IBOutlet weak var viewWithShortlist: UIView!
    @IBOutlet weak var viewWithOutShortlist: UIView!

    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var ratePerSessionLabel: UILabel!
    @IBOutlet weak var ratePerMonthLabel: UILabel!
    @IBOutlet weak var clockImageView: UIImageView!
    
    var shortlistTapGesture: UITapGestureRecognizer!
    
    var reportTapGesture: UITapGestureRecognizer!
    var anotherReportTapGesture: UITapGestureRecognizer!
    
    var messageTapGesture: UITapGestureRecognizer!
    
    var profileTapGesture:UITapGestureRecognizer!
    var anotherProfileTapGesture:UITapGestureRecognizer!
    
    var isShoutoutScreen = false
    var isCellSelected = false
    var delegate: FindTutorProfileTableViewCellDelegate?
    
    class func cellIdentifier() -> String {
        return "FindTutorProfileTableViewCell"
    }
    
    class func cellHeight() -> CGFloat {
        return 201.0
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        shortlistTapGesture = UITapGestureRecognizer(target: self, action: #selector(shortlistLabelTapped))
        shortlistButtonHolderView.addGestureRecognizer(shortlistTapGesture)
        
        reportTapGesture = UITapGestureRecognizer(target: self, action: #selector(reportLabelTapped))
        reportButtonHolderView.addGestureRecognizer(reportTapGesture)
        anotherReportTapGesture = UITapGestureRecognizer(target: self, action: #selector(reportLabelTapped))
        anotherReportButtonHolderView.addGestureRecognizer(anotherReportTapGesture)

        messageTapGesture = UITapGestureRecognizer(target: self, action: #selector(messageLabelTapped))
        messageButtonHolderView.addGestureRecognizer(messageTapGesture)

        profileTapGesture = UITapGestureRecognizer(target: self, action: #selector(profileLabelTapped))
        profileButtonHolderView.addGestureRecognizer(profileTapGesture)
        anotherProfileTapGesture = UITapGestureRecognizer(target: self, action: #selector(profileLabelTapped))
        anotherProfileButtonHolderView.addGestureRecognizer(anotherProfileTapGesture)
    }

    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        holderView.layer.shadowPath = UIBezierPath(roundedRect:
            holderView.bounds, cornerRadius: self.holderView.layer.cornerRadius).cgPath
        holderView.layer.shadowColor = themeBlueColor.cgColor
        holderView.layer.shadowOpacity = isCellSelected ? 0.5 : 0
        holderView.layer.shadowOffset = CGSize(width: 0, height: 0)
        holderView.layer.shadowRadius = 5
        holderView.layer.masksToBounds = false

        holderView.layer.borderColor = isCellSelected ? themeBlueColor.cgColor : themeLightGrayColor.cgColor
        holderView.layer.borderWidth = 1.5

        
        messageButtonHolderView.layer.shadowOffset = CGSize.init(width: 0, height: 5)
        messageButtonHolderView.layer.shadowRadius = 3
        messageButtonHolderView.layer.shadowColor = themeBlackColor.cgColor
        messageButtonHolderView.layer.shadowOpacity = 0.4
        
        profileButtonHolderView.layer.shadowOffset = CGSize.init(width: 0, height: 5)
        profileButtonHolderView.layer.shadowRadius = 3
        profileButtonHolderView.layer.shadowColor = themeBlackColor.cgColor
        profileButtonHolderView.layer.shadowOpacity = 0.4
        
        shortlistButtonHolderView.layer.shadowOffset = CGSize.init(width: 0, height: 5)
        shortlistButtonHolderView.layer.shadowRadius = 3
        shortlistButtonHolderView.layer.shadowColor = themeBlackColor.cgColor
        shortlistButtonHolderView.layer.shadowOpacity = 0.4
        
        reportButtonHolderView.layer.shadowOffset = CGSize.init(width: 0, height: 5)
        reportButtonHolderView.layer.shadowRadius = 3
        reportButtonHolderView.layer.shadowColor = themeBlackColor.cgColor
        reportButtonHolderView.layer.shadowOpacity = 0.4
        
        anotherReportButtonHolderView.layer.shadowOffset = CGSize.init(width: 0, height: 5)
        anotherReportButtonHolderView.layer.shadowRadius = 3
        anotherReportButtonHolderView.layer.shadowColor = themeBlackColor.cgColor
        anotherReportButtonHolderView.layer.shadowOpacity = 0.4
        
        anotherProfileButtonHolderView.layer.shadowOffset = CGSize.init(width: 0, height: 5)
        anotherProfileButtonHolderView.layer.shadowRadius = 3
        anotherProfileButtonHolderView.layer.shadowColor = themeBlackColor.cgColor
        anotherProfileButtonHolderView.layer.shadowOpacity = 0.4
    }

    @objc func shortlistLabelTapped(sender: UITapGestureRecognizer) {
        delegate?.shortlistProfileAtIndex(legacyButton.tag)
    }
    
    @objc func reportLabelTapped(sender: UITapGestureRecognizer) {
        delegate?.reportProfileAtIndex(legacyButton.tag)
    }
    
    @objc func messageLabelTapped(sender: UITapGestureRecognizer) {
        delegate?.messageProfileAtIndex(legacyButton.tag)
    }

    @objc func profileLabelTapped(sender: UITapGestureRecognizer) {
        delegate?.showProfileAtIndex(legacyButton.tag)
    }
    
    var item:MapItem?
    func configureWithMapItem(_ mapItem: MapItem, atIndex index: Int) {
        self.item = mapItem
        self.ratingView.isHidden = false
        self.responseTimeLabel.isHidden = false
        self.clockImageView.isHidden = false
        if !UserStore.shared.isLoggedIn {
            viewWithOutShortlist.isHidden = true
            viewWithShortlist.isHidden = false
            if mapItem.type == "student" {
                self.ratingView.isHidden = true
                self.responseTimeLabel.isHidden = true
                self.clockImageView.isHidden = true
            }
        } else if mapItem.type == "student" {
            viewWithOutShortlist.isHidden = !Utilities.isUserTypeStudent()
            viewWithShortlist.isHidden = Utilities.isUserTypeStudent()
            self.ratingView.isHidden = true
            self.responseTimeLabel.isHidden = true
            self.clockImageView.isHidden = true
        } else {
            viewWithOutShortlist.isHidden = Utilities.isUserTypeStudent()
            viewWithShortlist.isHidden = !Utilities.isUserTypeStudent()
        }
        
        shortlistLabel.text = mapItem.isFavorite ? "SHORTLISTED".localized : "SHORTLIST".localized
        
        legacyButton.tag = index
        profileImageView.sd_setShowActivityIndicatorView(true)
        profileImageView.sd_setIndicatorStyle(.gray)
        profileImageView.sd_setImage(with: URL(string: mapItem.imageURL), placeholderImage: UIImage(named: "profile_placeholder"), options: .retryFailed) { (image, error, cacheType, url) in
            if error == nil {
                //Do nothing
            }
        }
        nameLabel.text = mapItem.name
        distanceLabel.text = mapItem.distance
        ratePerSessionLabel.text = mapItem.pricePerSession
        ratePerMonthLabel.text = mapItem.pricePerMonth
        legacyButton.setImage(UIImage(named: mapItem.pinImageName), for: .normal)
        if let n = NumberFormatter().number(from: mapItem.rating) {
            ratingView.value = CGFloat(truncating: n)
        }
    }
 
    func configureWithMapItemForShoutoutScreen(_ mapItem: MapItem, atIndex index: Int, isSelected: Bool) {
        self.isShoutoutScreen = true
        self.isCellSelected = isSelected
        self.legacyButton.isUserInteractionEnabled = false
        self.ratingView.isHidden = false
        self.responseTimeLabel.isHidden = false
        self.clockImageView.isHidden = false
        if !UserStore.shared.isLoggedIn {
            viewWithOutShortlist.isHidden = true
            viewWithShortlist.isHidden = false
            if mapItem.type == "student" {
                self.ratingView.isHidden = true
                self.responseTimeLabel.isHidden = true
                self.clockImageView.isHidden = true
            }
        } else if mapItem.type == "student" {
            viewWithOutShortlist.isHidden = !Utilities.isUserTypeStudent()
            viewWithShortlist.isHidden = Utilities.isUserTypeStudent()
            self.ratingView.isHidden = true
            self.responseTimeLabel.isHidden = true
            self.clockImageView.isHidden = true
        } else {
            viewWithOutShortlist.isHidden = Utilities.isUserTypeStudent()
            viewWithShortlist.isHidden = !Utilities.isUserTypeStudent()
        }
        
        shortlistLabel.text = mapItem.isFavorite ? "SHORTLISTED".localized : "SHORTLIST".localized
        
        legacyButton.tag = index
        profileImageView.sd_setShowActivityIndicatorView(true)
        profileImageView.sd_setIndicatorStyle(.gray)
        profileImageView.sd_setImage(with: URL(string: mapItem.imageURL), placeholderImage: UIImage(named: "profile_placeholder"), options: .retryFailed) { (image, error, cacheType, url) in
            if error == nil {
                //Do nothing
            }
        }
        nameLabel.text = mapItem.name
        distanceLabel.text = mapItem.distance
        ratePerSessionLabel.text = mapItem.pricePerSession
        ratePerMonthLabel.text = mapItem.pricePerMonth
        legacyButton.setImage(UIImage(named: isSelected ? "checkbox" : "checkbox_empty" ), for: .normal)
        if let n = NumberFormatter().number(from: mapItem.rating) {
            ratingView.value = CGFloat(truncating: n)
        }
        self.layoutSubviews()
    }

    @IBAction func legacyButtonTapped(_ sender: UIButton) {
        delegate?.selectMarkerForProfileAtIndex(sender.tag)
    }
}
