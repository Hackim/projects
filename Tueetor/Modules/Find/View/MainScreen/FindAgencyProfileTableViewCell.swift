//
//  FindAgencyProfileTableViewCell.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 09/07/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit
import SwiftyStarRatingView
import SDWebImage

protocol FindAgencyProfileTableViewCellDelegate {
    func selectMarkerForAgencyAtIndex(_ index: Int)
    func showAgencyAtIndex(_ index: Int)
    func enquireAgencyAtIndex(_ index: Int)
    func reportAgencyAtIndex(_ index: Int)
}

class FindAgencyProfileTableViewCell: UITableViewCell {
    
    @IBOutlet weak var holderView: UIView!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var subTitleNameLabel: UILabel!
    @IBOutlet weak var legacyHolderView: UIView!
    @IBOutlet weak var legacyButton: UIButton!
    
    @IBOutlet weak var profileButtonHolderView: UIView!
    @IBOutlet weak var enquireButtonHolderView: UIView!
    @IBOutlet weak var reportButtonHolderView: UIView!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var ratePerSessionLabel: UILabel!
    @IBOutlet weak var ratePerMonthLabel: UILabel!
    
    var reportTapGesture: UITapGestureRecognizer!
    var enquiryTapGesture: UITapGestureRecognizer!
    var profileTapGesture:UITapGestureRecognizer!
    
    var delegate: FindAgencyProfileTableViewCellDelegate?
    var isCellSelected = false

    class func cellIdentifier() -> String {
        return "FindAgencyProfileTableViewCell"
    }
    
    class func cellHeight() -> CGFloat {
        return 201.0
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        reportTapGesture = UITapGestureRecognizer(target: self, action: #selector(reportLabelTapped))
        reportButtonHolderView.addGestureRecognizer(reportTapGesture)
        
        enquiryTapGesture = UITapGestureRecognizer(target: self, action: #selector(enquireLabelTapped))
        enquireButtonHolderView.addGestureRecognizer(enquiryTapGesture)
        
        profileTapGesture = UITapGestureRecognizer(target: self, action: #selector(profileLabelTapped))
        profileButtonHolderView.addGestureRecognizer(profileTapGesture)
    }
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        holderView.layer.shadowPath = UIBezierPath(roundedRect:
            holderView.bounds, cornerRadius: self.holderView.layer.cornerRadius).cgPath
        holderView.layer.shadowColor = themeBlueColor.cgColor
        holderView.layer.shadowOpacity = isCellSelected ? 0.5 : 0
        holderView.layer.shadowOffset = CGSize(width: 0, height: 0)
        holderView.layer.shadowRadius = 5
        holderView.layer.masksToBounds = false
        
        holderView.layer.borderColor = isCellSelected ? themeBlueColor.cgColor : themeLightGrayColor.cgColor
        holderView.layer.borderWidth = 1.5

        enquireButtonHolderView.layer.shadowOffset = CGSize.init(width: 0, height: 5)
        enquireButtonHolderView.layer.shadowRadius = 3
        enquireButtonHolderView.layer.shadowColor = themeBlackColor.cgColor
        enquireButtonHolderView.layer.shadowOpacity = 0.4
        
        profileButtonHolderView.layer.shadowOffset = CGSize.init(width: 0, height: 5)
        profileButtonHolderView.layer.shadowRadius = 3
        profileButtonHolderView.layer.shadowColor = themeBlackColor.cgColor
        profileButtonHolderView.layer.shadowOpacity = 0.4
        
        reportButtonHolderView.layer.shadowOffset = CGSize.init(width: 0, height: 5)
        reportButtonHolderView.layer.shadowRadius = 3
        reportButtonHolderView.layer.shadowColor = themeBlackColor.cgColor
        reportButtonHolderView.layer.shadowOpacity = 0.4
        
    }
    
    @objc func reportLabelTapped(sender: UITapGestureRecognizer) {
        delegate?.reportAgencyAtIndex(legacyButton.tag)
    }
    
    @objc func enquireLabelTapped(sender: UITapGestureRecognizer) {
        delegate?.enquireAgencyAtIndex(legacyButton.tag)
    }
    
    @objc func profileLabelTapped(sender: UITapGestureRecognizer) {
        delegate?.showAgencyAtIndex(legacyButton.tag)
    }
    
    func configureWithMapItem(_ mapItem: MapItem, atIndex index: Int) {
        legacyButton.tag = index
        profileImageView.sd_setShowActivityIndicatorView(true)
        profileImageView.sd_setIndicatorStyle(.gray)
        profileImageView.sd_setImage(with: URL(string: mapItem.imageURL), placeholderImage: UIImage(named: "profile_placeholder"), options: .retryFailed) { (image, error, cacheType, url) in
            if error == nil {
                //Do nothing
            }
        }
        nameLabel.text = mapItem.firstName
        subTitleNameLabel.text = mapItem.lastName
        distanceLabel.text = mapItem.distance
        ratePerSessionLabel.text = mapItem.pricePerSession
        ratePerMonthLabel.text = mapItem.pricePerMonth
        legacyButton.setImage(UIImage(named: mapItem.pinImageName), for: .normal)
    }
    
    func configureWithMapItemForShoutoutScreen(_ mapItem: MapItem, atIndex index: Int, isSelected: Bool) {
        self.isCellSelected = isSelected
        legacyButton.tag = index
        self.legacyButton.isUserInteractionEnabled = false
        profileImageView.sd_setShowActivityIndicatorView(true)
        profileImageView.sd_setIndicatorStyle(.gray)
        profileImageView.sd_setImage(with: URL(string: mapItem.imageURL), placeholderImage: UIImage(named: "profile_placeholder"), options: .retryFailed) { (image, error, cacheType, url) in
            if error == nil {
                //Do nothing
            }
        }
        nameLabel.text = mapItem.firstName
        subTitleNameLabel.text = mapItem.lastName
        distanceLabel.text = mapItem.distance
        ratePerSessionLabel.text = mapItem.pricePerSession
        ratePerMonthLabel.text = mapItem.pricePerMonth
        
        legacyButton.setImage(UIImage(named: isSelected ? "checkbox" : "checkbox_empty" ), for: .normal)
        self.layoutSubviews()
    }

    
    @IBAction func legacyButtonTapped(_ sender: UIButton) {
        delegate?.selectMarkerForAgencyAtIndex(legacyButton.tag)
    }
    
}
