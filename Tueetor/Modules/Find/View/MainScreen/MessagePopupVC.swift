//
//  MessagePopup.swift
//  Tueetor
//
//  Created by Namespace  on 17/09/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//
import DGCollectionViewLeftAlignFlowLayout
import UIKit
import RxSwift

class MessagePopupVC: BaseViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout  {
    @IBOutlet weak var lblTutorName:UILabel!
    @IBOutlet weak var ivTutorImg:UIImageView!
    @IBOutlet weak var searchCollectionView:UICollectionView!
    @IBOutlet weak var templatesCollectionView:UICollectionView!
    @IBOutlet weak var tvMessage:UITextView!
    @IBOutlet weak var vwPopupBG: UIView!
//    @IBOutlet weak var btnDismiss: UIButton!
    @IBOutlet weak var btnSend: UIButton!
    var selectedCell = [IndexPath]()
    var concatedMsgs: [String] = [String]()
    var image :UIImage?
    var name:String!
    var senderID:String!
    var disposableBag = DisposeBag()
    var studentDetail: StudentDetailedInfo!

    //var messages  = ["3D Design", "3D Design:Grashhopper", "Algebra", "Animation", "Bahasa Indonesia", "Calculus","fitness:Yoga Stretch", "Mathematics"]
    var messages:[String] = [""]
    var msgList = ["Do you have other time slots available?", "Are the rates negotiable?" ,"What is your qualification?"]
    override func viewDidLoad() {
        super.viewDidLoad()
//        self.btnDismiss.layer.cornerRadius = 10
//        self.btnDismiss.layer.borderColor = UIColor.black.cgColor
//        self.btnDismiss.layer.borderWidth = 1
//        self.btnDismiss.clipsToBounds  = true
        
        fetchStudentDetails()
        
        self.ivTutorImg.layer.cornerRadius = 25
        self.ivTutorImg.clipsToBounds  = true
        
        self.vwPopupBG.layer.cornerRadius = 5
        self.vwPopupBG.clipsToBounds  = true
        
        self.tvMessage.layer.borderColor = "23A4B1".hexStringToUIColor().cgColor
        self.tvMessage.layer.borderWidth = 1

        self.tvMessage.layer.cornerRadius = 8.0
        self.btnSend.layer.cornerRadius = 8
        self.btnSend.clipsToBounds  = true
        
        if messages == [""] || messages.count == 0 {
            messages[0] = "No subject selected."
        }
        tvMessage.placeholder = "Type a message...".localized
       // tvMessage.placeholderColor = themeBlueColor
        
       // let alignedFlowLayout = searchCollectionView?.collectionViewLayout as? AlignedCollectionViewFlowLayout
       // alignedFlowLayout?.horizontalAlignment = .left
        //        let collectionViewFlowLayout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
//        let collectionViewFlowLayout = UICollectionViewFlowLayout()
//        collectionViewFlowLayout.estimatedItemSize = CGSize(width: 30  , height: 30)
//        collectionViewFlowLayout.minimumInteritemSpacing = 1
//        //        collectionViewFlowLayout.sectionInset = UIEdgeInsetsMake(0, 5, 0, 5)
//
//        collectionViewFlowLayout.minimumLineSpacing = 1
//        searchCollectionView.layer.borderColor = UIColor.red.cgColor
//        searchCollectionView.layer.borderWidth = 0.3
//        searchCollectionView.contentInset = UIEdgeInsets(top: 10, left: 10, bottom: 0, right: 0)

        searchCollectionView.collectionViewLayout = DGCollectionViewLeftAlignFlowLayout()//collectionViewFlowLayout
        
        templatesCollectionView.collectionViewLayout = DGCollectionViewLeftAlignFlowLayout()//collectionViewFlowLayout

        self.ivTutorImg.image = image
        self.lblTutorName.text = name
        fetchLimits()
        // Do any additional setup after loading the view.
    }

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    @IBAction func btnonTapDismiss(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
    @IBAction func btnOnTapSendMessage(_ sender: UIButton) {
        if !tvMessage.text.isEmpty {
            sendMessage(tvMessage.text)
        }
    }
    
    func showAlertWithMessage(_ message: String) {
        let alert = UIAlertController(title: AppName, message: message, preferredStyle: .alert)
        let okButton = UIAlertAction(title: AlertButton.ok.description(), style: .default, handler: nil)
        alert.addAction(okButton)
        self.present(alert, animated: true, completion: nil)
    }
    
    func test(){
        
    }
    
    func sendMessage(_ message: String) {
        guard Utilities.shared.isNetworkReachable() else {
            self.showAlertWithMessage(AlertMessage.noInternetConnection.description())
            return
        }
        
        guard let userID = UserStore.shared.userID else {
            return
        }
        Utilities.showHUD(forView: self.view, excludeViews: [])
        
        let messageObserver = ApiManager.shared.apiService.sendMessageToUserID(senderID!, senderID: String(userID), message: message)
        let messageDisposable = messageObserver.subscribe(onNext: {(successMessage) in
            Utilities.hideHUD(forView: self.view)
            print(successMessage)
            DispatchQueue.main.async(execute: {
                self.dismiss(animated: true, completion: nil)

            })
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.showAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        })
        messageDisposable.disposed(by: disposableBag)
    }

    func fetchLimits() {
        
        guard Utilities.shared.isNetworkReachable() else {
            self.showAlertWithMessage(AlertMessage.noInternetConnection.description())
            return
        }
        
        guard let userID = UserStore.shared.userID else {
            return
        }
        Utilities.showHUD(forView: self.view, excludeViews: [])
        let messageObserver = ApiManager.shared.apiService.fetchMessageLimits(userID)
        let messageDisposable = messageObserver.subscribe(onNext: {(limits) in
            Utilities.hideHUD(forView: self.view)
//            DispatchQueue.main.async {
//                let txt  = "\(limits.sent_today!)/ (\(limits.daily_quota!)) today, \(limits.sent_week!)/ (\(limits.week_quota!)) this week"
//                self.lblLimits.text = txt
//            }
           
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.showAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        })
        messageDisposable.disposed(by: disposableBag)
    }
}



extension MessagePopupVC {
    
     func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return collectionView == searchCollectionView ? messages.count : 3
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var text = ""
        
        if collectionView == templatesCollectionView {
           text = msgList[indexPath.row]
        }else{
           text = messages[indexPath.row]
        }
        let width = UILabel.textWidth(font: UIFont.init(name: "Montserrat-Light", size: 16)!, text: text)
        return CGSize.init(width: width + 6, height: 30) //CGSize(width: width + 6 , height: 20)
    }
    
     func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! MessagePopupCell
        if collectionView == searchCollectionView {
            cell.lblMessage.text = messages[indexPath.row]
            collectionView.allowsMultipleSelection = true

        }else {
            cell.lblMessage.text = msgList[indexPath.row]
            collectionView.allowsMultipleSelection = true

        }
//        cell.layer.borderColor = UIColor.black.cgColor
//        cell.layer.borderWidth = 1
        cell.clipsToBounds = true
//        if let annotateCell = cell as? AnnotatedPhotoCell {
//            annotateCell.photo = photos[indexPath.item]
//        }
        cell.backgroundColor = UIColor.white
        cell.lblMessage.textColor = UIColor.lightGray
        cell.layer.cornerRadius = 5
        cell.layer.borderWidth = 1
        cell.layer.borderColor = UIColor.lightGray.cgColor
        
        return cell
    }
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
//        return 2
//    }
//
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
//        return 2
//    }
//    var selectedCell : [IndexPath]?

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == templatesCollectionView{
            if !concatedMsgs.contains(msgList[indexPath.row]){
                concatedMsgs.append(msgList[indexPath.row])
            }
            self.tvMessage.text = concatedMsgs.joined(separator: ", ")
            //self.tvMessage.text = msgList[indexPath.row]
            let cell = collectionView.cellForItem(at: indexPath) as? MessagePopupCell
            selectedCell.append(indexPath)
            cell?.isSelected = true
            cell?.backgroundColor = "23A4B1".hexStringToUIColor()
            
            cell?.lblMessage.textColor = UIColor.white
            cell?.layer.cornerRadius = 5
            cell?.layer.borderWidth = 0
            
        }else{
            let cell = collectionView.cellForItem(at: indexPath) as? MessagePopupCell
            cell?.backgroundColor = "23A4B1".hexStringToUIColor()
            
            cell?.lblMessage.textColor = UIColor.white
            cell?.layer.cornerRadius = 5
            cell?.layer.borderWidth = 0
            
        }
    }
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        if collectionView == templatesCollectionView{
            
            //self.tvMessage.text = ""
            let cell = collectionView.cellForItem(at: indexPath) as? MessagePopupCell
            let msgLbl = cell?.viewWithTag(1) as! UILabel
            if concatedMsgs.contains(msgLbl.text!){
                concatedMsgs = concatedMsgs.filter { $0 != msgLbl.text! }
            }
            self.tvMessage.text = concatedMsgs.joined(separator: ", ")
            cell?.isSelected = false
            
            if selectedCell.contains(indexPath) {
                selectedCell.remove(at: selectedCell.index(of: indexPath)!)
//                cell?.contentView.backgroundColor = UIColor.lightGray
            }
            
            cell?.backgroundColor = UIColor.white
            cell?.lblMessage.textColor = UIColor.lightGray
            cell?.layer.cornerRadius = 5
            cell?.layer.borderWidth = 1
            cell?.layer.borderColor = UIColor.lightGray.cgColor
            
        }else{
            let cell = collectionView.cellForItem(at: indexPath) as? MessagePopupCell
            cell?.backgroundColor = UIColor.white
            cell?.lblMessage.textColor = UIColor.lightGray
            cell?.layer.cornerRadius = 5
            cell?.layer.borderWidth = 1
            cell?.layer.borderColor = UIColor.lightGray.cgColor
            
        }
    }
    
}

extension MessagePopupVC{
    func fetchStudentDetails() {
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }
        
        Utilities.showHUD(forView: self.view, excludeViews: [])
        
        let tutorObserver = ApiManager.shared.apiService.fetchStudentDetailsForID(senderID)
        let tutorDisposable = tutorObserver.subscribe(onNext: {(studentDetails) in
            Utilities.hideHUD(forView: self.view)
            self.studentDetail = studentDetails
            DispatchQueue.main.async {
                var subjects: [String] = []
                for subject in 0..<studentDetails.subjects.count{
                    subjects.append(studentDetails.subjects[subject].name)
                }
                self.messages = subjects
                self.searchCollectionView.reloadData()
                //self.tableView.reloadData()
                //self.messages = studentDetails.subjects
            }
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.showSimpleAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        })
        tutorDisposable.disposed(by: disposableBag)
    }
}

////MARK: - PINTEREST LAYOUT DELEGATE
//extension MessagePopup : PinterestLayoutDelegate {
//    
//    // 1. Returns the photo height
//    func collectionView(_ collectionView: UICollectionView, heightForPhotoAtIndexPath indexPath:IndexPath) -> CGFloat {
//        let cell = collectionView.cellForItem(at: indexPath) as! MessagePopupCell
//        
//        return cell.lblMessage.//photos[indexPath.item].image.size.height
//    }
//    
//}


class MessagePopupCell:UICollectionViewCell{
    @IBOutlet weak var lblMessage:UILabel!
}

extension UILabel {
    func textWidth() -> CGFloat {
        return UILabel.textWidth(label: self)
    }
    
    class func textWidth(label: UILabel) -> CGFloat {
        return textWidth(label: label, text: label.text!)
    }
    
    class func textWidth(label: UILabel, text: String) -> CGFloat {
        return textWidth(font: label.font, text: text)
    }
    
    class func textWidth(font: UIFont, text: String) -> CGFloat {
        let myText = text as NSString
        
        let rect = CGSize(width: CGFloat.greatestFiniteMagnitude, height: CGFloat.greatestFiniteMagnitude)
        let labelSize = myText.boundingRect(with: rect, options: .usesLineFragmentOrigin, attributes: [kCTFontAttributeName as NSAttributedStringKey: font], context: nil)
        return ceil(labelSize.width)
    }
}
