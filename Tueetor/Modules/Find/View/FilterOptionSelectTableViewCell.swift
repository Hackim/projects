
//
//  FilterOptionSelectTableViewCell.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 24/05/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit

class FilterOptionSelectTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var checkboxButton: UIButton!

    class func cellIdentifier() -> String {
        return "FilterOptionSelectTableViewCell"
    }
    
    class func cellHeight() -> CGFloat {
        return 50.0
    }

}
