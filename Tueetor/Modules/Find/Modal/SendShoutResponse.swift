//
//  SendShoutResponse.swift
//  Tueetor
//
//  Created by Namespace  on 05/09/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import Foundation
import Unbox

class SendShoutResponse: Unboxable {
    
    var status: String?
    var data : Dictionary<String,Any>?
    var message : String?
    
    required init(unboxer: Unboxer) throws {
        self.status = unboxer.unbox(key: "status")
        self.message = unboxer.unbox(key: "message")
        self.data = unboxer.unbox(key: "data")
    }
    
}
