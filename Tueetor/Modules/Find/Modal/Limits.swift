//
//  Limits.swift
//  Tueetor
//
//  Created by Namespace  on 18/09/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import Foundation
import Unbox

class Limits: Unboxable {
    
    var sent_today: String!
    var sent_week: String!
    var daily_quota: String!
    var week_quota: String!
    
    required init(unboxer: Unboxer) throws {
        self.sent_today = unboxer.unbox(key: "sent_today") ?? "0"
        self.sent_week = unboxer.unbox(key: "sent_week") ?? "0"
        self.daily_quota = unboxer.unbox(key: "daily_quota") ?? "0"
        self.week_quota = unboxer.unbox(key: "week_quota") ?? "0"
      }

}



