//
//  PlaceMarker.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 26/06/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import Foundation
import GoogleMaps

class PlaceMarker: GMSMarker {

    let place: MapItem
    
    init(place: MapItem) {
        self.place = place
        super.init()
        
        position = place.coordinate
        icon = UIImage(named: "purpleMapIcon")
        groundAnchor = CGPoint(x: 0.5, y: 1)
        appearAnimation = .pop
    }
}
