//
//  MapItem.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 26/06/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import Foundation
import Unbox

@objc class MapItem: NSObject, Unboxable, GMUClusterItem {
    
    
    var ID: String!
    var title: String!
    var name: String!
    var firstName: String!
    var lastName: String!
    var gender: String!
    var countryID: String!
    var latitude: String!
    var longitude: String!
    var imageURL: String!
    var rating: String!
    var ratingCount: String!
    var isFavorite: Bool = false
    var responseTime: String!
    var legend: String!
    var distance: String!
    var type: String!
    var coordinate: CLLocationCoordinate2D
    var position: CLLocationCoordinate2D
    var pinImageName = "purpleMapIcon"
    var pricePerSession: String!
    var pricePerMonth: String!
    var assetID: String!
    var isSelectedForShoutout = false
    
    required init(unboxer: Unboxer) throws {
        self.ID = unboxer.unbox(key: "id") ?? "0"
        self.title = unboxer.unbox(key: "title") ?? "Mr"
        self.name = unboxer.unbox(key: "name") ?? ""
        self.firstName = unboxer.unbox(key: "first_name") ?? ""
        self.lastName = unboxer.unbox(key: "last_name") ?? ""
        self.gender = unboxer.unbox(key: "gender") ?? "M"
        self.countryID = unboxer.unbox(key: "country_id") ?? ""
        let lat = unboxer.unbox(key: "lattitude") ?? "0"
        let long = unboxer.unbox(key: "longitude") ?? "0"
        if lat.isEmpty || lat == "null" {
            self.latitude = "0"
        } else {
            self.latitude = lat
        }
        if long.isEmpty || long == "null" {
            self.longitude = "0"
        } else {
            self.longitude = long
        }
        self.imageURL = unboxer.unbox(key: "image") ?? ""
        self.rating = unboxer.unbox(key: "average_rating") ?? ""
        self.ratingCount = unboxer.unbox(key: "rating_count") ?? "0"
        self.isFavorite = unboxer.unbox(key: "is_favorite") ?? false
        self.responseTime = unboxer.unbox(key: "responseTime") ?? ""
        self.legend = unboxer.unbox(key: "legend") ?? ""
        self.distance = unboxer.unbox(key: "distance") ?? "0"
        self.type = unboxer.unbox(key: "type") ?? ""
        self.assetID = unboxer.unbox(key: "assets_ids") ?? "0"

        self.pricePerSession = unboxer.unbox(key: "pricepersession") ?? "NA"
        self.pricePerMonth = unboxer.unbox(key: "pricepermonth") ?? "NA"
        
        self.coordinate = CLLocationCoordinate2D(latitude: Double(self.latitude)!, longitude: Double(self.longitude)!)
        self.position = CLLocationCoordinate2D(latitude: Double(self.latitude)!, longitude: Double(self.longitude)!)
        switch legend {
        case "stu":
            pinImageName = "bluePin"
        case "tut":
            pinImageName = "greenPin"
        case "sub":
            pinImageName = "purplePin"
        case "sub-lev":
            pinImageName = "orangePin"
        case "agen":
            pinImageName = "agencyPin"
        default:
            pinImageName = ""
        }
        isSelectedForShoutout = false

    }
    
}
