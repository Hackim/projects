//
//  FilterParams.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 27/06/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import Foundation

class FilterParams {
    
    var isTutorButtonSelected = true
    var selectedSubjects = [Subject]()
//    var selectedLevel: Level! = Level.init(id: 1, name: "All levels")
//    var selectedQualification: Qualification! = Qualification.init(id: "12", name: "No Preference")
//    var selectedExperience: Experience! = Experience.init(name: "No Preference")
    var selectedLevel: Level!
    var selectedQualification: Qualification!
    var selectedExperience: Experience!

    var perSessionMinimum = 0
    var perSessionMaximum = 1000
    var perMonthMinimum = 0
    var perMonthMaximum = 10000
    var anyRatingSelected = true
    var rating: CGFloat = 0
    var locationName = ""
    var latitude: CLLocationDegrees!
    var longitude: CLLocationDegrees!
    var searchName = ""
    var isDistanceAnywhereSelected = true
    var isAnyBudgetSelected = true
    var distance = 0//10000
    var isAvailableAnytimeSelected = true
    var selectedTimeSlots = ["MON": ["0","0","0","0"],
                             "TUE": ["0","0","0","0"],
                             "WED": ["0","0","0","0"],
                             "THU": ["0","0","0","0"],
                             "FRI": ["0","0","0","0"],
                             "SAT": ["0","0","0","0"],
                             "SUN": ["0","0","0","0"]]
    var showStudentSubPin = false
    var showTutorSubPin = true
    var showMatchingSubLevelPin = false
    var showMatchinSubPin = false
    var showAgencyPin = true
    var countryID = ""
    var isFeatured = false
    var isShoutout = false
    var sortBy: SortByFilter = .noSort
    
    init() {
        
    }
    
    func clearFilter() {
        selectedSubjects.removeAll()
        selectedLevel = Level.init(id: 1, name: "All levels")
        selectedQualification = Qualification.init(id: "12", name: "No Preference")
        selectedExperience = Experience.init(name: "No Preference")
        perSessionMinimum = 0
        perSessionMaximum = 1000
        perMonthMaximum = 10000
        perMonthMinimum = 0
        anyRatingSelected = true
        rating = 0
        locationName = ""
        latitude = nil
        longitude = nil
        searchName = ""
        isDistanceAnywhereSelected = false
        isAnyBudgetSelected = false
        distance = 0
        countryID = ""
        isAvailableAnytimeSelected = false
        selectedTimeSlots = ["MON": ["0","0","0","0"],
                             "TUE": ["0","0","0","0"],
                             "WED": ["0","0","0","0"],
                             "THU": ["0","0","0","0"],
                             "FRI": ["0","0","0","0"],
                             "SAT": ["0","0","0","0"],
                             "SUN": ["0","0","0","0"]]
        isFeatured = false
        isShoutout = false
        sortBy = .noSort
    }
    
    func copyValuesFromObject(_ filterParams: FilterParams) {
        isTutorButtonSelected = filterParams.isTutorButtonSelected
        selectedSubjects = filterParams.selectedSubjects
        selectedLevel = filterParams.selectedLevel
        selectedQualification = filterParams.selectedQualification
        selectedExperience = filterParams.selectedExperience
        perSessionMinimum = filterParams.perSessionMinimum
        perSessionMaximum = filterParams.perSessionMaximum
        perMonthMaximum = filterParams.perMonthMaximum
        perMonthMinimum = filterParams.perMonthMinimum
        anyRatingSelected = filterParams.anyRatingSelected
        rating = filterParams.rating
        locationName = filterParams.locationName
        latitude = filterParams.latitude
        longitude = filterParams.longitude
        searchName = filterParams.searchName
        isDistanceAnywhereSelected = filterParams.isDistanceAnywhereSelected
        isAnyBudgetSelected = filterParams.isAnyBudgetSelected
        distance = filterParams.distance
        isAvailableAnytimeSelected = filterParams.isAvailableAnytimeSelected
        selectedTimeSlots = filterParams.selectedTimeSlots
        showStudentSubPin = filterParams.showStudentSubPin
        showTutorSubPin = filterParams.showTutorSubPin
        showMatchingSubLevelPin = filterParams.showMatchingSubLevelPin
        showMatchinSubPin = filterParams.showMatchinSubPin
        showAgencyPin = filterParams.showAgencyPin
        countryID = filterParams.countryID
        isFeatured = filterParams.isFeatured
        isShoutout = filterParams.isShoutout
        sortBy = filterParams.sortBy
    }
    
    func getFilterDictForCurrentLocation(_ currentLocation: CLLocationCoordinate2D?, currentCountryID: String?, page: String?) -> [String: String] {
        var filterDict = [String: String]()
    
        filterDict = ["country_id": currentCountryID ?? "",
                      "rating": String(describing: rating),
                      "persession": "\(perSessionMinimum),\(perSessionMaximum)",
            "permonth": "\(perMonthMinimum),\(perMonthMaximum)",
            "search": searchName,
            "distance": distance > 0 ? String(distance) : ""]
        if let pageNumber = page {
            filterDict["page"] = pageNumber
        }
        var subIds = ""
        for i in 0..<selectedSubjects.count {
            if i != selectedSubjects.count - 1 {
                subIds += String(selectedSubjects[i].id)
                subIds += ","
            } else {
                subIds += String(selectedSubjects[i].id)
            }
        }
        if !subIds.isEmpty {
            filterDict["subject"] = subIds
        }

        
        if let quali = selectedQualification {
            filterDict["qualification"] = quali.id
        }

        if let exp = selectedExperience {
            filterDict["experience"] = exp.name
        }

        if let level = selectedLevel {
            filterDict["level"] = String(level.id)
        }

        if latitude != nil {
            filterDict["lattitude"] = String(latitude)
            filterDict["longitude"] = String(longitude)
        } else if let location = currentLocation {
            filterDict["lattitude"] = String(location.latitude)
            filterDict["longitude"] = String(location.longitude)
        }
     

        if let cuntID = currentCountryID, !cuntID.isEmpty {
            filterDict["country_id"] = cuntID
            countryID = cuntID
        }else{
            filterDict["country_id"] = countryID
        }
        
        if let userID = UserStore.shared.userID {
            filterDict["user_id"] = String(userID)
        }
        if isFeatured {
            filterDict["featured"] = "1"
        }
        var legends = [String]()

        if showStudentSubPin {
            legends.append("stu")
        }
        if showTutorSubPin {
            legends.append("tut")
        }
        if showMatchinSubPin {
            legends.append("sub")
        }
        if showMatchingSubLevelPin {
            legends.append("sub-lev")
        }
        if showAgencyPin {
            legends.append("agen")
        }
        filterDict["legend"] = legends.joined(separator: ",")

        if let monTimeSlot = selectedTimeSlots["MON"], monTimeSlot.contains("1") {
            filterDict["MON"] = monTimeSlot.joined(separator: ",")
        }
        
        if let tueTimeSlot = selectedTimeSlots["TUE"], tueTimeSlot.contains("1") {
            filterDict["TUE"] = tueTimeSlot.joined(separator: ",")
        }
        
        if let wedTimeSlot = selectedTimeSlots["WED"], wedTimeSlot.contains("1") {
            filterDict["WED"] = wedTimeSlot.joined(separator: ",")
        }
        
        if let thuTimeSlot = selectedTimeSlots["THU"], thuTimeSlot.contains("1") {
            filterDict["THU"] = thuTimeSlot.joined(separator: ",")
        }
        
        if let friTimeSlot = selectedTimeSlots["FRI"], friTimeSlot.contains("1") {
            filterDict["FRI"] = friTimeSlot.joined(separator: ",")
        }
        
        if let satTimeSlot = selectedTimeSlots["SAT"], satTimeSlot.contains("1") {
            filterDict["SAT"] = satTimeSlot.joined(separator: ",")
        }
        
        if let sunTimeSlot = selectedTimeSlots["SUN"], sunTimeSlot.contains("1") {
            filterDict["SUN"] = sunTimeSlot.joined(separator: ",")
        }
        if isShoutout {
            filterDict["source"] = "shoutout"
        }
        if let sortString = Utilities.getSortStringForType(sortBy) {
            filterDict["sort_by"] = sortString
        }
        return filterDict
    }
    
    
    func setLegendsForTutorSelection() {
        self.showTutorSubPin = true
        self.showAgencyPin = true
        self.showMatchinSubPin = false
        self.showMatchingSubLevelPin = false
        self.showStudentSubPin = false
    }
    
    func setLegendsForStudentSelection() {
        self.showTutorSubPin = false
        self.showAgencyPin = false
        self.showMatchinSubPin = false
        self.showMatchingSubLevelPin = false
        self.showStudentSubPin = true
    }
}
