//
//  Shoutout.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 29/08/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import Foundation
import Unbox

class Shoutout: Unboxable {
    
    var tutorCount: Int!
    var agencyCount: Int!
    var tutors = [MapItem]()
    var agencies = [MapItem]()
    
    required init(unboxer: Unboxer) throws {
        self.tutorCount = unboxer.unbox(key: "tutor_count") ?? 0
        self.agencyCount = unboxer.unbox(key: "agency_count") ?? 0
        if let tutorsArray = unboxer.dictionary["tutor_array"] as? [[String: AnyObject]] {
            for tutorDict in tutorsArray {
                let tutor: MapItem = try unbox(dictionary: tutorDict)
                self.tutors.append(tutor)
            }
        }
        if let agenciesArray = unboxer.dictionary["agency_array"] as? [[String: AnyObject]] {
            for agencyDict in agenciesArray {
                let agency: MapItem = try unbox(dictionary: agencyDict)
                self.agencies.append(agency)
            }
        }
    }
    
}
