//
//  MapItemPagination.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 02/07/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import Foundation
import Unbox

class MapItemPagination: Unboxable {
    
    var mapItems: [MapItem] = []
    private let limit = 20
    var currentPageNumber = 1
    var paginationType: PaginationType = .new
    var hasMoreToLoad: Bool
    
    init() {
        mapItems = []
        currentPageNumber = 1
        paginationType = .new
        hasMoreToLoad = false
    }
    
    required init(unboxer: Unboxer) throws {
        if let mapItemArray = unboxer.dictionary["data"] as? [[String: AnyObject]] {
            for mapItemDict in mapItemArray {
                let mapItem: MapItem = try unbox(dictionary: mapItemDict["user"] as! [String: AnyObject])
                self.mapItems.append(mapItem)
            }
        }
        hasMoreToLoad = self.mapItems.count == limit
    }
    
    func appendDataFromObject(_ newPaginationObject: MapItemPagination) {
        switch self.paginationType {
        case .new, .reload:
            self.mapItems = [];
            self.mapItems = newPaginationObject.mapItems
            self.paginationType = .old
        case .old:
            self.mapItems += newPaginationObject.mapItems
            self.paginationType = .old
        }
        self.hasMoreToLoad = newPaginationObject.hasMoreToLoad
        self.currentPageNumber += 1
    }
    
}
