//
//  MoreSectionHeaderTableViewCell.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 17/05/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit

class MoreSectionHeaderTableViewCell: UITableViewCell {

    class func cellIdentifier() -> String {
        return "MoreSectionHeaderTableViewCell"
    }
    
    class func cellHeight() -> CGFloat {
        return 125.0
    }
    
}
