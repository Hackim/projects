//
//  MoreOptionTableViewCell.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 17/05/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit

class MoreOptionTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var iconImageView: UIImageView!
    
    class func cellIdentifier() -> String {
        return "MoreOptionTableViewCell"
    }
    
    class func cellHeight() -> CGFloat {
        return 64.0
    }
    
    func configureCellWithTitle(_ title: String, andImage image: String) {
        titleLabel.text = title
        iconImageView.image = UIImage.init(named: image)
    }
    
}
