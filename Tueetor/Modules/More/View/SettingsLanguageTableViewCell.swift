//
//  SettingsLanguageTableViewCell.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 29/05/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit

class SettingsLanguageTableViewCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var languageLabel: UILabel!
    
    class func cellIdentifier() -> String {
        return "SettingsLanguageTableViewCell"
    }

    class func cellHeight() -> CGFloat {
        return 96.0
    }

}
