//
//  SettingsToggleTableViewCell.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 29/05/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit

protocol SettingsToggleTableViewCellDelegate {
    func toggleSwitched(_ sender: UISwitch)
}
class SettingsToggleTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var cellSwitch: UISwitch!
    
    var delegate: SettingsToggleTableViewCellDelegate?
    
    class func cellIdentifier() -> String {
        return "SettingsToggleTableViewCell"
    }
    
    @IBAction func toggleTapped(_ sender: UISwitch) {
        delegate?.toggleSwitched(sender)
    }
    
}
