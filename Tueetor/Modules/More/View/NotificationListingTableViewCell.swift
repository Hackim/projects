//
//  NotificationListingTableViewCell.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 30/07/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit

class NotificationListingTableViewCell: UITableViewCell {

    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    
    class func cellIdentifier() -> String {
        return "NotificationListingTableViewCell"
    }
    
    class func cellHeight() -> CGFloat {
        return 65.0
    }
    
    func configureCellForNotification(_ notify: NotificationObject) {
        titleLabel.text = notify.title
        descLabel.text = notify.message
    }
}
