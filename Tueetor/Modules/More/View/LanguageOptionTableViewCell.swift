//
//  LanguageOptionTableViewCell.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 30/05/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit

class LanguageOptionTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!

    class func cellIdentifier() -> String {
        return "LanguageOptionTableViewCell"
    }

}
