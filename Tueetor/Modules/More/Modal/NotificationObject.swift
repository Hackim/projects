//
//  Notification.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 27/07/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import Foundation
import Unbox

class NotificationObject: Unboxable {
    
    var id: Int!
    var title: String!
    var message: String!
    
    required init(unboxer: Unboxer) throws {
        self.id = unboxer.unbox(key: "id") ?? 0
        self.title = unboxer.unbox(key: "title") ?? ""
        self.message = unboxer.unbox(key: "message") ?? "0"
    }

}
