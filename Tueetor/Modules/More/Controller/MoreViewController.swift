//
//  MoreViewController.swift
//  Tueetor
//
//  Created by Phaninder on 21/04/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit

class MoreViewController: BaseViewController {

    @IBOutlet weak var tableView: UITableView!
    
   // var titles = ["About Tueetor".localized, "Tueetorial".localized, "Tueetor Experience".localized, "Tueetor Advantage".localized, "FAQ".localized, "Contact Us".localized, "Notifications".localized, "Read Our Blog".localized, "Partnership".localized, "Terms & Conditions".localized, "Privacy Policy".localized, "Settings".localized]
    var titles: [String]{
        didSet{
            ["About Tueetor".localized, "Tueetorial".localized, "Tueetor Experience".localized, "Tueetor Advantage".localized, "FAQ".localized, "Contact Us".localized, "Notifications".localized, "Read Our Blog".localized, "Partnership".localized, "Terms & Conditions".localized, "Privacy Policy".localized, "Settings".localized]
        }
    }
    var imageNames = ["about", "tutorial", "tutor_experience", "tutor_exp", "faq", "contact_us", "notification_setting", "blog", "partnership", "terms", "privacy_policy", "settings"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        (self.tabBarController as! BaseTabBarViewController).tabBar.isHidden = false
        (self.tabBarController as! BaseTabBarViewController).showCenterButton()
    }
    

}

extension MoreViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return titles.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard indexPath.row != 0 else {
            let cell = tableView.dequeueReusableCell(withIdentifier: MoreSectionHeaderTableViewCell.cellIdentifier(), for: indexPath) as! MoreSectionHeaderTableViewCell
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: MoreOptionTableViewCell.cellIdentifier(), for: indexPath) as! MoreOptionTableViewCell
        cell.configureCellWithTitle(titles[indexPath.row - 1], andImage: imageNames[indexPath.row - 1])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        guard indexPath.row != 0 else {
            return MoreSectionHeaderTableViewCell.cellHeight()
        }
        guard indexPath.row != titles.count else {
            return MoreOptionTableViewCell.cellHeight() + 40.0
        }
        if !UserStore.shared.isLoggedIn, indexPath.row == MoreTableViewRow.notifications.rawValue {
            return 0
        }
        return MoreOptionTableViewCell.cellHeight()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard indexPath.row != 0 else {
            return
        }
        (self.tabBarController as! BaseTabBarViewController).tabBar.isHidden = true
        (self.tabBarController as! BaseTabBarViewController).hideCenterButton()

        switch indexPath.row {
        case MoreTableViewRow.about.rawValue:
            if let url = aboutURL {
                pushToWebViewWithURL(url, title: titles[indexPath.row - 1])
            }
            
        case MoreTableViewRow.tueetorial.rawValue:
            if let url = tueetorialURL {
                pushToWebViewWithURL(url, title: titles[indexPath.row - 1])
            }
            
        case MoreTableViewRow.experience.rawValue:
            if let url = tueetorExperienceURL {
                pushToWebViewWithURL(url, title: titles[indexPath.row - 1])
            }

        case MoreTableViewRow.advantage.rawValue:
            if let url = tueetorAdvantageURL {
                pushToWebViewWithURL(url, title: titles[indexPath.row - 1])
            }

        case MoreTableViewRow.faq.rawValue:
            if let url = faqURL {
                pushToWebViewWithURL(url, title: titles[indexPath.row - 1])
            }

        case MoreTableViewRow.contact.rawValue:
            if let url = contactURL {
                pushToWebViewWithURL(url, title: titles[indexPath.row - 1])
            }
        case MoreTableViewRow.notifications.rawValue:
            let notificationVC = NotificationListingViewController.instantiateFromAppStoryboard(appStoryboard: .More)
            navigationController?.pushViewController(notificationVC, animated: true)
        case MoreTableViewRow.blog.rawValue:
            if let url = blogURL {
                pushToWebViewWithURL(url, title: titles[indexPath.row - 1])
            }

        case MoreTableViewRow.partnership.rawValue:
            if let url = partnerURL {
                pushToWebViewWithURL(url, title: titles[indexPath.row - 1])
            }

        case MoreTableViewRow.terms.rawValue:
            if let url = termsURL {
                pushToWebViewWithURL(url, title: titles[indexPath.row - 1])
            }

        case MoreTableViewRow.privacy.rawValue:
            if let url = privacyURL {
                pushToWebViewWithURL(url, title: titles[indexPath.row - 1])
            }

        case MoreTableViewRow.settings.rawValue:
            let settingsPage = AppSettingsViewController.instantiateFromAppStoryboard(appStoryboard: .More)
            navigationController?.pushViewController(settingsPage, animated: true)
        default:
            break
        }
    }
}
