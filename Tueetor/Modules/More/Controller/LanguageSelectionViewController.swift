//
//  LanguageSelectionViewController.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 29/05/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit
import RxSwift

protocol LanguageSelectionViewControllerDelegate {
    func languageSelected(_ language: String)
}

class LanguageSelectionViewController: BaseViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var searchTextFieldWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var backButton: UIButton!
    
    var disposableBag = DisposeBag()
    var isFetchingData = false
    var isSearchActive = false
    var isFirstTime = true
    var searchString = ""
    var headerText: String!
    var categoryID: String!
    var isFromLaunch = false
    var languagePagination = LanguagePagination.init()
    var languageDisposable: Disposable!

    var delegate:LanguageSelectionViewControllerDelegate?
    
    private let refreshControl = UIRefreshControl()

    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 10.0, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.addSubview(refreshControl)
        }
        if isFromLaunch {
            self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
            backButton.isHidden = true
        } else {
            (self.tabBarController as! BaseTabBarViewController).tabBar.isHidden = true
            (self.tabBarController as! BaseTabBarViewController).hideCenterButton()
        }
        
        searchTextFieldWidthConstraint.constant = 0.0
        refreshControl.addTarget(self, action: #selector(refreshData(_:)), for: .valueChanged)
        fetchLanguages()
    }

    @objc func refreshData(_ sender: Any) {
        self.languagePagination.paginationType = .new
        fetchLanguages()
        if let refreshControl = sender as? UIRefreshControl {
            refreshControl.endRefreshing()
        }
    }
    
    @IBAction func searchButtonTapped(_ sender: UIButton) {
        isSearchActive = !isSearchActive
        if isSearchActive {
            searchTextField.becomeFirstResponder()
        } else {
            searchTextField.resignFirstResponder()
        }
        self.view.layoutIfNeeded()
        UIView.animate(withDuration: 0.7, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 1, options: .curveEaseIn, animations: {
            self.searchTextFieldWidthConstraint.constant = self.isSearchActive ? ScreenWidth - 132.0 : 0
            let imageName = self.isSearchActive ? "search_active" : "search_inactive"
            self.searchButton.setImage(UIImage(named: imageName), for: .normal)
            self.searchButton.backgroundColor = self.isSearchActive ? themeBlueColor : UIColor.clear
            self.view.layoutIfNeeded()
        }) { (completed) in
            
        }
    }
    
    @IBAction func valueChangedInSearchField(_ sender: Any) {
        if (languageDisposable != nil) {
            Utilities.hideHUD(forView: view)
            languageDisposable.dispose()
        }
        languagePagination.paginationType = .new
        fetchLanguages()
    }

}

extension LanguageSelectionViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return languagePagination.languages.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard indexPath.row != languagePagination.languages.count else {
            let cell = tableView.dequeueReusableCell(withIdentifier: LoadMoreTableViewCell.cellIdentifier(), for: indexPath) as! LoadMoreTableViewCell
            cell.activityIndicator.startAnimating()
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: LanguageOptionTableViewCell.cellIdentifier(), for: indexPath) as! LanguageOptionTableViewCell
        let language = languagePagination.languages[indexPath.row]
        cell.nameLabel.text = language.transalationName
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let sectionHeader = tableView.dequeueReusableCell(withIdentifier: LanguageHeaderTableViewCell.cellIdentifier()) as! LanguageHeaderTableViewCell
        sectionHeader.titleLabel.text = "Select Language".localized
        return sectionHeader
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        tableView.estimatedSectionHeaderHeight = 81.0
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.5
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footer = UIView()
        footer.backgroundColor = UIColor.white
        return footer
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == languagePagination.languages.count {
            return languagePagination.hasMoreToLoad ? 50.0 : 0
        }
        tableView.estimatedRowHeight = 100.0
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == languagePagination.languages.count && self.languagePagination.hasMoreToLoad && !isFetchingData {
            fetchLanguages()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let language = languagePagination.languages[indexPath.row]
        UserStore.shared.selectedLanguageName = language.transalationName
        UserStore.shared.selectedLanguageCode = language.code
        if isFromLaunch {
            UserStore.shared.isLanguageSelected = true
            let walkThroughPageViewController = WalkThroughPageViewController.instantiateFromAppStoryboard(appStoryboard: .Main)
            self.navigationController?.pushViewController(walkThroughPageViewController, animated: true)
        } else {
            delegate?.languageSelected(language.transalationName)
            self.navigationController?.popViewController(animated: true)
        }
    }
    
}

extension LanguageSelectionViewController: UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if isFetchingData == false, searchString != textField.text {
            languagePagination.paginationType = .new
            fetchLanguages()
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (textField.text?.isEmpty)! && string == " " {
            return false
        }
        let char = string.cString(using: String.Encoding.utf8)!
        let isBackSpace = strcmp(char, "\\b")
        if (isBackSpace == -92) {
            return true
        }
        return true
    }
}

extension LanguageSelectionViewController {
    
    func fetchLanguages() {
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }
        isFetchingData = true
        searchString = searchTextField.text ?? ""
        
        if languagePagination.paginationType != .old {
            self.languagePagination.currentPageNumber = 1
            if isFirstTime == true {
                Utilities.showHUD(forView: self.view, excludeViews: [])
            } else {
                UIApplication.shared.isNetworkActivityIndicatorVisible = true
            }
            
        } else {
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
        }
        let languageObserver = ApiManager.shared.apiService.fetchLanguages(languagePagination.currentPageNumber, query: searchString)
        languageDisposable = languageObserver.subscribe(onNext: {(paginationObject) in
            self.isFetchingData = false
            
            self.languagePagination.appendDataFromObject(paginationObject)
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if self.languagePagination.paginationType != .old && self.isFirstTime == true {
                    self.isFirstTime = false
                } else {
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                }
                self.tableView.reloadData()
            })
        }, onError: {(error) in
            self.isFetchingData = false
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
            })
        })
        languageDisposable.disposed(by: disposableBag)
    }
    
}
