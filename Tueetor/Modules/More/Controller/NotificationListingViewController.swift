//
//  NotificationListingViewController.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 27/07/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit
import RxSwift

class NotificationListingViewController: BaseViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var emptyMessageLabel: UILabel!

    var disposableBag = DisposeBag()
    var isFetchingData = false
    var isFirstTime = true
    var refreshData = false
    var notificationPagination = NotificationPagination.init()

    private let refreshControl = UIRefreshControl()

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView()
        if #available(iOS 10.0, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.addSubview(refreshControl)
        }
        refreshControl.addTarget(self, action: #selector(refreshData(_:)), for: .valueChanged)
        fetchNotitifications()
    }

    @objc func refreshData(_ sender: Any) {
        self.notificationPagination.paginationType = .new

        fetchNotitifications()
        if let refreshControl = sender as? UIRefreshControl {
            refreshControl.endRefreshing()
        }
    }


}

extension NotificationListingViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if notificationPagination.notifications.count == 0 {
            return 0
        }
        return notificationPagination.notifications.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard indexPath.row != notificationPagination.notifications.count else {
            let cell = tableView.dequeueReusableCell(withIdentifier: LoadMoreTableViewCell.cellIdentifier(), for: indexPath) as! LoadMoreTableViewCell
            cell.activityIndicator.startAnimating()
            return cell
        }

        let cell = tableView.dequeueReusableCell(withIdentifier: NotificationListingTableViewCell.cellIdentifier(), for: indexPath) as! NotificationListingTableViewCell
        cell.configureCellForNotification(notificationPagination.notifications[indexPath.row])
        return cell
    }
    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//
//        if indexPath.row == notificationPagination.notifications.count {
//            return notificationPagination.hasMoreToLoad ? 50.0 : 0
//        }
//        return NotificationListingTableViewCell.cellHeight()
//    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == notificationPagination.notifications.count && self.notificationPagination.hasMoreToLoad && !isFetchingData {
            fetchNotitifications()
        }
    } 

}

extension NotificationListingViewController {
    
    func fetchNotitifications() {
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }
        
        guard let userID = UserStore.shared.userID else {
            return
        }
        
        if notificationPagination.paginationType != .old {
            self.notificationPagination.currentPageNumber = 1
            if isFirstTime == true {
                Utilities.showHUD(forView: self.view, excludeViews: [])
            } else {
                UIApplication.shared.isNetworkActivityIndicatorVisible = true
            }
            
        } else {
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
        }
        
        let notificationObserver = ApiManager.shared.apiService.fetchNotificationsForUserID("3", deviceID: "9ECD2AE8-695A-4BA8-924F-579B3AC41C9E", page: notificationPagination.currentPageNumber)
        let notificationDisposable = notificationObserver.subscribe(onNext: {(paginationObject) in
            
            self.isFetchingData = false
            self.notificationPagination.appendDataFromObject(paginationObject)
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if self.notificationPagination.paginationType != .old && self.isFirstTime == true {
                    self.isFirstTime = false
                } else {
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                }
                self.tableView.isHidden = self.notificationPagination.notifications.count == 0
                self.emptyMessageLabel.isHidden = self.notificationPagination.notifications.count != 0
                self.tableView.reloadData()
            })
        }, onError: {(error) in
            self.isFetchingData = false
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
            })
        })
        notificationDisposable.disposed(by: disposableBag)
    }

}
