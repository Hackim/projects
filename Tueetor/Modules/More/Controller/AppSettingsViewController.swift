//
//  AppSettingsViewController.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 29/05/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit
import RxSwift
import BiometricAuthentication
import UserNotifications

class AppSettingsViewController: BaseViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var disposableBag = DisposeBag()
    var biometricTitle = ""
    var showTouchID = false
    var messageText = ""
    var isPendingApproval = false
    var ignoreTheSwitchAction = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if UserStore.shared.isLoggedIn == true {
            configureTouchID()
        }
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(setNotificationsOn), name: .deviceTokenReceived,
                                               object: nil)
    }

    func configureTouchID() {
        
        if BioMetricAuthenticator.canAuthenticate() && UserStore.shared.hasLoginKey {
            self.showTouchID = true
            if BioMetricAuthenticator.shared.faceIDAvailable() {
                self.biometricTitle = "FACE ID".localized
                self.messageText = "Face ID".localized
            } else {
                self.biometricTitle = "TOUCH ID".localized
                self.messageText = "Touch ID".localized
            }
        } else {
            self.showTouchID = false
        }
    }
    
    func reverseTheToggle(_ toggleSwitch: UISwitch) {
        let isON = toggleSwitch.isOn
        ignoreTheSwitchAction = true
        toggleSwitch.setOn(!isON, animated: false)
    }
    
    func askForNotificationPermissions() {
        UserStore.shared.notificationPermissionsAsked = true
        isPendingApproval = true
        if #available(iOS 10.0, *) {
            let current = UNUserNotificationCenter.current()
            current.getNotificationSettings(completionHandler: { settings in
                
                switch settings.authorizationStatus {
                case .notDetermined:
                    DispatchQueue.main.async {
                        let delegate = UIApplication.shared.delegate as! AppDelegate
                        delegate.registerForPushNotifications()
                    }
                case .denied:
                    self.showNotificationsAlert()
                case .authorized:
                    UserStore.shared.isNotificationUnAuthorized = false
                    break
                }
            })
        } else {
            // Fallback on earlier versions
            if UIApplication.shared.isRegisteredForRemoteNotifications {
                UserStore.shared.isNotificationUnAuthorized = false
            } else {
                DispatchQueue.main.async {
                    let delegate = UIApplication.shared.delegate as! AppDelegate
                    delegate.registerForPushNotifications()
                }
            }
        }
    }
    
    func showNotificationsAlert() {
        let alert = UIAlertController(title: "Notification Permission".localized, message: "Enable Push Notification to receive timely info.".localized, preferredStyle: .alert)
        let okAction = UIAlertAction(title: AlertButton.ok.description(), style: .default) { (action) in
            self.navigateToSettingsPage()
        }
        let laterAction = UIAlertAction(title: AlertButton.later.description(), style: .cancel, handler: nil)
        alert.addAction(okAction)
        alert.addAction(laterAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc func setNotificationsOn() {
        if isPendingApproval {
            self.isPendingApproval = false
            DispatchQueue.main.async {
                let cell = self.tableView.cellForRow(at: IndexPath.init(row: 1, section: 0)) as? SettingsToggleTableViewCell
                cell?.cellSwitch.setOn(true, animated: true)
            }
        }
    }
    
    func toggleBiometrics() {
        BioMetricAuthenticator.authenticateWithBioMetrics(reason: "", success: {
            guard let username = UserStore.shared.userEmail, !username.isEmpty else {
                return
            }
            let isEnabled = UserStore.shared.isBioMetricEnabledByUser
            UserStore.shared.isBioMetricEnabledByUser = !isEnabled
            UserStore.shared.touchIDEmail = (UserStore.shared.isBioMetricEnabledByUser == true) ? UserStore.shared.userEmail : ""
        }, failure: { [weak self] (error) in
            print("Fail")
            
            // do nothing on canceled
            if error == .canceledByUser || error == .canceledBySystem {
                return
            } else if error == .biometryNotAvailable {
                // device does not support biometric (face id or touch id) authentication
                DispatchQueue.main.async(execute: {
                    let alert = UIAlertController(title: AppName, message: error.message(), preferredStyle: .alert)
                    let okAction =  UIAlertAction(title: AlertButton.ok.description(), style: .default, handler: nil)
                    alert.addAction(okAction)
                    self?.present(alert, animated: true, completion: nil)
                })
            } else if error == .fallback {
                // show alternatives on fallback button clicked
                // here we're entering username and password
                
            } else if error == .biometryNotEnrolled {
                // No biometry enrolled in this device, ask user to register fingerprint or face
                //Settings page
            } else if error == .biometryLockedout {
                // Biometry is locked out now, because there were too many failed attempts.
                // Need to enter device passcode to unlock.
                
                // show passcode authentication
            } else {
                // show error on authentication failed
                DispatchQueue.main.async(execute: {
                    let alert = UIAlertController(title: AppName, message: error.message(), preferredStyle: .alert)
                    let okAction =  UIAlertAction(title: AlertButton.ok.description(), style: .default, handler: nil)
                    alert.addAction(okAction)
                    self?.present(alert, animated: true, completion: nil)
                })
            }
        })

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        (self.tabBarController as! BaseTabBarViewController).tabBar.isHidden = true
        (self.tabBarController as! BaseTabBarViewController).hideCenterButton()
    }
    
}

extension AppSettingsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if UserStore.shared.isLoggedIn {
            return showTouchID ? 3 : 2
        } else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: SettingsToggleTableViewCell.cellIdentifier(), for: indexPath) as! SettingsToggleTableViewCell
            cell.titleLabel.text = "NOTIFICATION SETTINGS".localized
            cell.descLabel.text = "Use this button to turn ON and OFF Notifications.".localized
            cell.cellSwitch.tag = 1
            cell.delegate = self
            cell.cellSwitch.setOn(UserStore.shared.isNotificationOn, animated: false)
            return cell
        } else if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: SettingsLanguageTableViewCell.cellIdentifier(), for: indexPath) as! SettingsLanguageTableViewCell
            cell.languageLabel.text = UserStore.shared.selectedLanguageName
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: SettingsToggleTableViewCell.cellIdentifier(), for: indexPath) as! SettingsToggleTableViewCell
            cell.titleLabel.text = biometricTitle
            cell.cellSwitch.tag = 2
            cell.delegate = self
            cell.cellSwitch.setOn(UserStore.shared.isBioMetricEnabledByUser, animated: true)
            cell.descLabel.text = "Use this button to turn ON and OFF \(messageText) feature.".localized
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return SettingsLanguageTableViewCell.cellHeight()
        } else {
            tableView.estimatedRowHeight = 117.0
            return UITableViewAutomaticDimension
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard indexPath.row == 0 else {
            return
        }
        let languageVC = LanguageSelectionViewController.instantiateFromAppStoryboard(appStoryboard: .More)
        languageVC.delegate = self
        navigationController?.pushViewController(languageVC, animated: true)
    }
    
}

extension AppSettingsViewController: SettingsToggleTableViewCellDelegate {
    
    func toggleSwitched(_ sender: UISwitch) {
        guard ignoreTheSwitchAction == false else {
            ignoreTheSwitchAction = false
            return
        }
        if sender.tag == 2 {
            toggleBiometrics()
        } else {
            changeNotificationSetting(sender)
        }
    }
}

extension AppSettingsViewController: LanguageSelectionViewControllerDelegate {
    
    func languageSelected(_ language: String) {
        self.tableView.reloadData()
    }
    
}
extension AppSettingsViewController {
    
    func changeNotificationSetting(_ toggleSwitch: UISwitch) {
        if UserStore.shared.isNotificationUnAuthorized == true, toggleSwitch.isOn  {
            reverseTheToggle(toggleSwitch)
            askForNotificationPermissions()
            return
        }
        guard Utilities.shared.isNetworkReachable() else {
            reverseTheToggle(toggleSwitch)
            self.showNoInternetMessage()
            return
        }
        
        guard let userID = UserStore.shared.userID else {
            reverseTheToggle(toggleSwitch)
            return
        }
        
        let deviceName = UIDevice.current.name
        guard !UserStore.shared.deviceToken.isEmpty else {
            reverseTheToggle(toggleSwitch)
            return
        }
        guard let uniqueIdentifier = UIDevice.current.identifierForVendor?.uuidString else {
            reverseTheToggle(toggleSwitch)
            return
        }

        Utilities.showHUD(forView: self.view, excludeViews: [])
        
        let notificationObserver = ApiManager.shared.apiService.toggleNotifications(toggleSwitch.isOn, deviceName: deviceName, userId: String(userID), deviceId: uniqueIdentifier, deviceToken: UserStore.shared.deviceToken)
        let notificationDisposable = notificationObserver.subscribe(onNext: {(message) in
            Utilities.hideHUD(forView: self.view)
            DispatchQueue.main.async {
                UserStore.shared.isNotificationOn = toggleSwitch.isOn
                self.showSimpleAlertWithMessage(message)
            }
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                let isON = toggleSwitch.isOn
                toggleSwitch.setOn(!isON, animated: true)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.showSimpleAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        })
        notificationDisposable.disposed(by: disposableBag)
    }
}

