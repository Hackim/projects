//
//  Chat.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 29/08/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import Foundation
import Unbox

class Chat: Unboxable {
    
    var ID: Int!
    var displayName: String!
    var firstName: String!
    var lastName: String!
    var userType: String!
    var senderID: String!
    var unreadCount: Int!
    var readCount: Int!
    var msgSubject: String!
    var message: String!
    var type: String!
    var image: String!
    var isRead: Bool = false
    var sendedOn: String!
    var pID: String!
    
    required init(unboxer: Unboxer) throws {
        self.ID = unboxer.unbox(key: "id") ?? 0
        self.displayName = unboxer.unbox(key: "name") ?? ""
        self.firstName = unboxer.unbox(key: "first_name") ?? ""
        self.lastName = unboxer.unbox(key: "last_name") ?? ""
        self.userType = unboxer.unbox(key: "user_type") ?? "S"
        self.senderID = unboxer.unbox(key: "user_id") ?? "0"
        self.unreadCount = unboxer.unbox(key: "unread_count") ?? 0
        self.readCount = unboxer.unbox(key: "read") ?? 0
        self.msgSubject = unboxer.unbox(key: "msg_subject") ?? ""
        self.message = unboxer.unbox(key: "message") ?? ""
        self.type = unboxer.unbox(key: "type") ?? ""
        self.image = unboxer.unbox(key: "image") ?? ""
        self.isRead = unboxer.unbox(key: "read") ?? false
        self.pID = unboxer.unbox(key: "pid") ?? "0"
        
        self.sendedOn = unboxer.unbox(key: "sendedon") ?? "2018-08-29 07:20:08"
        if self.sendedOn == "0000-00-00 00:00:00" {
            self.sendedOn = "2018-08-29 07:20:08"
        }
    }
    
}
