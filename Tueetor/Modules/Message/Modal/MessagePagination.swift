//
//  MessagePagination.swift
//  Tueetor
//
//  Created by Hipster on 05/10/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import Foundation
import Unbox

class MessagePagination: Unboxable {
    
    var messageItems: [Chat] = []
    private let limit = 20
    var currentPageNumber = 1
    var paginationType: PaginationType = .new
    var hasMoreToLoad: Bool
    
    init() {
        messageItems = []
        currentPageNumber = 1
        paginationType = .new
        hasMoreToLoad = false
    }
    
    required init(unboxer: Unboxer) throws {
        if let messageDict = unboxer.dictionary["data"] as? NSDictionary {
           let detailArray =  messageDict["detail"] as? NSArray
            for messageData in detailArray!{
                let message: Chat = try unbox(dictionary: messageData as! [String: AnyObject] )
                self.messageItems.append(message)
            }
        }
        hasMoreToLoad = self.messageItems.count == limit
    }
    
    func appendDataFromObject(_ newPaginationObject: MessagePagination) {
        switch self.paginationType {
        case .new, .reload:
            self.messageItems = [];
            self.messageItems = newPaginationObject.messageItems
            self.paginationType = .old
        case .old:
            self.messageItems += newPaginationObject.messageItems
            self.paginationType = .old
        }
        self.hasMoreToLoad = newPaginationObject.hasMoreToLoad
        self.currentPageNumber += 1
    }
    
}

