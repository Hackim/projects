//
//  Message.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 29/08/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import Foundation
import MessageKit
import Unbox



class Message: MessageType,Unboxable {
    
    var messageId: String
    var sender: Sender
    var sentDate: Date
//    var data: MessageData
    var image: String
    var kind: MessageKind
    var msg: String = ""
   

    init(responseDict: NSDictionary) {
        let messageIDInt = responseDict.value(forKey: "id") as? Int ?? 0
        self.messageId = String(messageIDInt)
        let messageAttr = NSAttributedString(string: responseDict.value(forKey: "message") as? String ?? "",
                                             attributes: [.font: UIFont(name: "Montserrat-Light", size: 15.0)!,
                                                          .foregroundColor : themeBlackColor])
        
        self.kind = MessageKind.attributedText(messageAttr)
        let senderID = responseDict.value(forKey: "sender_id") as? Int ?? 0
        let senderName = responseDict.value(forKey: "sender_first_name") as? String ?? "John Doe"
        self.sender = Sender(id:  "\(senderID)", displayName: senderName)
        self.image = responseDict.value(forKey: "sender_image") as? String ?? ""
        var sentDateString = responseDict.value(forKey: "sendedon") as? String ?? "2018-08-29 09:29:06"
        if sentDateString == "0000-00-00 00:00:00" {
            sentDateString = "2018-08-29 09:29:06"
        }
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.timeZone = NSTimeZone(forSecondsFromGMT: 0) as TimeZone?
        
        let date = dateFormatter.date(from: sentDateString)
        dateFormatter.dateFormat = "E, d MMM yyyy h:mm a"
        dateFormatter.timeZone = TimeZone.current
        let datestr = dateFormatter.string(from: date!)
        
        self.sentDate = dateFormatter.date(from: datestr)!
        
        
    }
    //unboxer.unbox(key: "id") ?? 0
    required init(unboxer: Unboxer) throws {
        let messageIDInt = unboxer.unbox(key: "id") ?? 0
        self.messageId = String(messageIDInt)
        let senderId = unboxer.unbox(key: "sender_id") ?? ""
        let senderName = unboxer.unbox(key: "sender_first_name") ?? ""
        self.sender = Sender.init(id: senderId, displayName: senderName)
        let sentDateString = unboxer.unbox(key: "sendedon")  ?? ""
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.timeZone = NSTimeZone(forSecondsFromGMT: 0) as TimeZone?
        
        let date = dateFormatter.date(from: sentDateString)
        dateFormatter.dateFormat = "E, d MMM yyyy h:mm a"
        dateFormatter.timeZone = TimeZone.current
       
        let datestr = date != nil ? dateFormatter.string(from: date!) : ""
        self.sentDate = datestr != "" ? dateFormatter.date(from: datestr)! : Date()
        
        self.image = unboxer.unbox(key: "sender_image") ?? ""
        
        let str = unboxer.unbox(key: "message")  ??  ""
        let messageAttr = NSAttributedString(string: str,
                                             attributes: [.font: UIFont(name: "Montserrat-Light", size: 15.0)!,
                                                          .foregroundColor : themeBlackColor])
        self.kind = MessageKind.attributedText(messageAttr)
        self.msg = ""
    }
}
