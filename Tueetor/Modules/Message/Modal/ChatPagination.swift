//
//  ChatPagination.swift
//  Tueetor
//
//  Created by Hipster on 08/10/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import Foundation
import Foundation
import Unbox

class ChatPagination: Unboxable {
    
    //var chats: [Chat] = []
     var chats: [Message] = []
    private let limit = 20
    var currentPageNumber = 1
    var paginationType: PaginationType = .new
    var hasMoreToLoad: Bool
    
    init() {
        chats = []
        currentPageNumber = 1
        paginationType = .new
        hasMoreToLoad = false
    }
    
    required init(unboxer: Unboxer) throws {
        if let messageDict = unboxer.dictionary["data"] as? NSDictionary {
            let messagesArray = messageDict["messages"] as? NSArray
            for message in messagesArray! {
                let message: Message = try unbox(dictionary: message as! [String:AnyObject])
                self.chats.append(message)
            }
        }
        hasMoreToLoad = self.chats.count == limit
        
        /*{
         if let messageDict = unboxer.dictionary["data"] as? NSDictionary {
         let detailArray =  messageDict["detail"] as? NSArray
         for messageData in detailArray!{
         let message: Chat = try unbox(dictionary: messageData as! [String: AnyObject] )
         self.messageItems.append(message)
         }
         }
         hasMoreToLoad = self.messageItems.count == limit
         }*/
    }
    
    func appendDataFromObject(_ newPaginationObject: ChatPagination) {
        switch self.paginationType {
        case .new, .reload:
            self.chats = [];
            self.chats = newPaginationObject.chats
            self.paginationType = .old
        case .old:
            self.chats += newPaginationObject.chats
            self.paginationType = .old
        }
        self.hasMoreToLoad = newPaginationObject.hasMoreToLoad
        self.currentPageNumber += 1
    }
    
}
