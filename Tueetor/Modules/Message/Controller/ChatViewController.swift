//
//  ChatViewController.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 29/08/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit
import MessageKit
import RxSwift
import SocketIO

class ChatViewController: MessagesViewController, UIGestureRecognizerDelegate {

    var disposableBag = DisposeBag()
    var messages = [Message]()
    var senderID: String!
    var userOneImage: UIImage!
    var userTwoImage: UIImage!
    var userTwoImageURL: String!
    var userTwoName: String!
    var chatID: String!
    var isConnected = false
    var manager : SocketManager!
    var socket : SocketIOClient!
    var chatPagination = ChatPagination.init()
   private let refreshControl = UIRefreshControl()
    var isFirstTime = true
    var isFetchingData = false

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        
        messagesCollectionView.messagesDataSource = self
        messagesCollectionView.messagesLayoutDelegate = self
        messagesCollectionView.messagesDisplayDelegate = self
//        if #available(iOS 10.0, *) {
//            messagesCollectionView.refreshControl = refreshControl
//        } else {
//            messagesCollectionView.addSubview(refreshControl)
//        }
        messagesCollectionView.addSubview(refreshControl)
        refreshControl.addTarget(self, action: #selector(self.loadMoreMessages), for: .valueChanged)
        self.maintainPositionOnKeyboardFrameChanged = true
        let user = (UIApplication.shared.delegate as! AppDelegate).user
        downloadAvatarImages(imageUrl: user?.imageURL, isFirst: true)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: Notification.Name.UIKeyboardWillShow, object: nil)
        fetchChatMessages()
        configureNavBar()
        connectToSocket()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        iMessage()
    }
    
    //--- PULL TO REFRESH-----
    
//    @objc func refreshData(_ sender: Any) {
////        self.chatPagination.paginationType = .new
//        fetchChatMessages()
//        print("refresh controll called")
//        if let refreshControl = sender as? UIRefreshControl {
//            refreshControl.endRefreshing()
//        }
//    }

  func connectToSocket(){
        
        let token = "b8ae267d40563acb4c0536b864a1ff47"
        let headers = ["Authorization": "Bearer " + token,
                       "User": String(UserStore.shared.userID ?? 0)]
        let tempHeader = ["headers":headers]
        let subscribeData = ["channel": "private-chat-\(chatID!)",
            "name": "subscribe",
            "auth": tempHeader] as [String : Any]
        
        manager = SocketManager(socketURL: URL(string: "http://socket-chat.tueetor.com:6001")!, config: [.log(true), .compress, .forceNew(true)])
        socket = manager.defaultSocket
        
        socket.on(clientEvent: .connect) {data, ack in
            print("socket connected")
            
            self.socket.emitWithAck("subscribe", subscribeData).timingOut(after: 2, callback: {data in
                print("subscribed")
                
                self.socket.on("messageSent") {data, ack in
                    print("messageSent")
                    
                    let tempDict = (data[1] as? NSDictionary)?.object(forKey: "0") as? NSDictionary ?? NSDictionary()
                    if tempDict.count > 0{
                        let message = Message(responseDict:tempDict)
                        self.messages.append(message)
                        
                        DispatchQueue.main.async(execute: {
                            //                        self.messageInputBar.inputTextView.text = ""
                            self.messagesCollectionView.reloadData()
                            self.messagesCollectionView.scrollToBottom()
                        })
                        
                    }
                    
                    
                }
                
            })
            
        }
        socket.connect()
    }
    

    func downloadAvatarImages(imageUrl: String?, isFirst: Bool) {
        
        guard let downloadImageURL = imageUrl, let imgUrl = URL(string: downloadImageURL) else {
            if isFirst == true {
                self.downloadAvatarImages(imageUrl: self.userTwoImageURL, isFirst: false)
            }
            return
        }
        
        let session = URLSession.shared
        let task = session.dataTask(with:imgUrl) { (data, response, error) in
            if error == nil {
                if isFirst == true {
                    if let image = UIImage(data: data!) {
                        self.userOneImage = image
                    }
                    
                    self.downloadAvatarImages(imageUrl: self.userTwoImageURL, isFirst: false)
                } else {
                    if let image = UIImage(data: data!) {
                        self.userTwoImage = image
                    }
                }
            }
        }
        
        task.resume()
    }

    func configureNavBar() {

        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.barTintColor = UIColor.white
        self.navigationController?.navigationBar.isHidden = false
        var image = UIImage(named: "left_arrow")
        image = image?.withRenderingMode(.alwaysOriginal)
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: image, style:.plain, target: self, action: #selector(navigateBack))
        
        self.title = userTwoName
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: themeBlackColor]
        
    }
    
    @objc func navigateBack() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func defaultStyle() {
        let newMessageInputBar = MessageInputBar()
        newMessageInputBar.sendButton.tintColor = UIColor(red: 69/255, green: 193/255, blue: 89/255, alpha: 1)
        newMessageInputBar.delegate = self
        messageInputBar = newMessageInputBar
        reloadInputViews()
    }

    func iMessage() {
        defaultStyle()
        messageInputBar.isTranslucent = false
        messageInputBar.backgroundView.backgroundColor = .white
        messageInputBar.separatorLine.isHidden = true
        messageInputBar.inputTextView.backgroundColor = UIColor(red: 245/255, green: 245/255, blue: 245/255, alpha: 1)
        messageInputBar.inputTextView.placeholderTextColor = UIColor(red: 0.6, green: 0.6, blue: 0.6, alpha: 1)
        messageInputBar.inputTextView.textContainerInset = UIEdgeInsets(top: 8, left: 16, bottom: 8, right: 36)
        messageInputBar.inputTextView.placeholderLabelInsets = UIEdgeInsets(top: 8, left: 20, bottom: 8, right: 36)
        messageInputBar.inputTextView.layer.borderColor = UIColor(red: 200/255, green: 200/255, blue: 200/255, alpha: 1).cgColor
        messageInputBar.inputTextView.font = UIFont(name: "Montserrat-Light", size: 15.0)!
        messageInputBar.inputTextView.textColor = themeBlackColor
        messageInputBar.inputTextView.keyboardType = .alphabet
        messageInputBar.inputTextView.layer.borderWidth = 1.0
        messageInputBar.inputTextView.layer.cornerRadius = 5.0
        messageInputBar.inputTextView.layer.masksToBounds = true
        
        messageInputBar.inputTextView.scrollIndicatorInsets = UIEdgeInsets(top: 8, left: 0, bottom: 8, right: 0)
        messageInputBar.setRightStackViewWidthConstant(to: 36, animated: true)
        messageInputBar.setStackViewItems([messageInputBar.sendButton], forStack: .right, animated: true)
        messageInputBar.sendButton.imageView?.backgroundColor = themeColor //UIColor(red: 69/255, green: 193/255, blue: 89/255, alpha: 1)
        messageInputBar.sendButton.contentEdgeInsets = UIEdgeInsets(top: 2, left: 2, bottom: 2, right: 2)
        messageInputBar.sendButton.setSize(CGSize(width: 36, height: 36), animated: true)
        messageInputBar.sendButton.image = UIImage(named: "send")
        messageInputBar.sendButton.title = nil
        messageInputBar.sendButton.imageView?.layer.cornerRadius = 16
        messageInputBar.sendButton.backgroundColor = .clear
        messageInputBar.textViewPadding.right = -38
    }

    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailBy otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }

    func showAlertWithMessage(_ message: String) {
        let alert = UIAlertController(title: AppName, message: message, preferredStyle: .alert)
        let okButton = UIAlertAction(title: AlertButton.ok.description(), style: .default, handler: nil)
        alert.addAction(okButton)
        self.present(alert, animated: true, completion: nil)
    }
    
    
}

extension ChatViewController: MessagesDataSource {
    
    func numberOfSections(in messagesCollectionView: MessagesCollectionView) -> Int {
        return messages.count
    }
    
    
    func currentSender() -> Sender {
        let user = (UIApplication.shared.delegate as! AppDelegate).user
        let id = UserStore.shared.userID ?? 0
        let name = user!.firstName ?? "John Doe"
        return Sender(id: String(id), displayName: name)
    }
    
    func numberOfMessages(in messagesCollectionView: MessagesCollectionView) -> Int {
        return messages.count                         //----commented by me
        //return chatPagination.chats.count
        
    }
    
    func messageForItem(at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> MessageType {
        return messages[indexPath.section]
    }
    
    
    func cellTopLabelAttributedText(for message: MessageType, at indexPath: IndexPath) -> NSAttributedString? {
        let name = message.sender.displayName
        return NSAttributedString(string: name, attributes: [NSAttributedStringKey.font: UIFont(name: "Montserrat-Light", size: 10.0)!,
                                                             NSAttributedStringKey.foregroundColor: themeBlackColor])
    }
    
    func messageBottomLabelAttributedText(for message: MessageType, at indexPath: IndexPath) -> NSAttributedString? {
        
        struct ConversationDateFormatter {
            static let formatter: DateFormatter = {
                let formatter = DateFormatter()
                formatter.dateFormat = "d MMM yyyy h:mm a"
                formatter.timeZone = TimeZone.current

//                formatter.dateStyle = .medium
                return formatter
            }()
        }
        let formatter = ConversationDateFormatter.formatter
        let dateString = formatter.string(from: message.sentDate)
        return NSAttributedString(string: dateString, attributes: [NSAttributedStringKey.font: UIFont(name: "Montserrat-Light", size: 10.0)!])
    }
    
    func messageStyle(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> MessageStyle {
        let corner: MessageStyle.TailCorner = isFromCurrentSender(message: message) ? .bottomRight : .bottomLeft
        return .bubbleTail(corner, .curved)
    }

    
}


extension ChatViewController: MessagesDisplayDelegate {
    
//    func configureAvatarView(_ avatarView: AvatarView, for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) {
//        var profileImage: UIImage!
//        if let messageInfo = message as? Message {
//            if messageInfo.sender.id == String(UserStore.shared.userID!) {
//                profileImage = userOneImage
//            } else {
//                profileImage = userTwoImage
//            }
//            let avatar = Avatar(image: profileImage, initials: "\(messageInfo.sender.displayName.first!)")
//            avatarView.set(avatar: avatar)
//
//        }
//    }
    
    func textColor(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> UIColor {
        return isFromCurrentSender(message: message) ? .white : .darkText
    }
    
    func detectorAttributes(for detector: DetectorType, and message: MessageType, at indexPath: IndexPath) -> [NSAttributedStringKey: Any] {
        return MessageLabel.defaultAttributes
    }
    
    func enabledDetectors(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> [DetectorType] {
        return [.url, .address, .phoneNumber, .date, .transitInformation]
    }
    
    // MARK: - All Messages
    
    func backgroundColor(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> UIColor {
        return isFromCurrentSender(message: message) ?themeColor : UIColor(red: 230/255, green: 230/255, blue: 230/255, alpha: 1)
    }
    
//    func messageStyle(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> MessageStyle {
//        let corner: MessageStyle.TailCorner = isFromCurrentSender(message: message) ? .bottomRight : .bottomLeft
//        return .bubbleTail(corner, .curved)
//              //  let configurationClosure = { (view: MessageContainerView) in}
//        //        return .custom(configurationClosure)
//    }
    
}

extension ChatViewController: MessagesLayoutDelegate {
    
    func avatarPosition(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> AvatarPosition {
        return AvatarPosition(horizontal: .natural, vertical: .messageBottom)
    }

    func cellTopLabelHeight(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> CGFloat {
        return 0
    }
    
    func messageTopLabelHeight(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> CGFloat {
        return 2
    }
    
    func messageBottomLabelHeight(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> CGFloat {
        return 16
    }
    
}


extension ChatViewController: MessageInputBarDelegate {
    
    func messageInputBar(_ inputBar: MessageInputBar, didPressSendButtonWith text: String) {
        
        for component in inputBar.inputTextView.components {
            
            if let text = component as? String {
                self.sendMessage(text)
            }
        }
        
        
        inputBar.inputTextView.text = String()
        messagesCollectionView.scrollToBottom()
    }
    
    @objc func keyboardWillShow(notification: Notification) {
        let originalText = messageInputBar.inputTextView.text ?? ""
        messageInputBar.inputTextView.text = originalText + " "
        messageInputBar.inputTextView.text = originalText
    }
}

extension ChatViewController: MessageCellDelegate {
    
    func didTapAvatar(in cell: MessageCollectionViewCell) {
    }
    
    @objc func loadMoreMessages() {
        DispatchQueue.global(qos: .userInitiated).asyncAfter(deadline: DispatchTime.now() + 4) {
//            SampleData.shared.getMessages(count: 10) { messages in
                DispatchQueue.main.async {
//                    self.chatPagination.paginationType = .new
                    self.fetchChatMessages()
//                    self.messageList.insert(contentsOf: messages, at: 0)
//                    self.messagesCollectionView.reloadDataAndKeepOffset()
//                    self.refreshControl.endRefreshing()
                }
//            }
        }
    }
    
}


extension ChatViewController {
    
    func fetchChatMessages() {
        
        guard Utilities.shared.isNetworkReachable() else {
            self.showAlertWithMessage(AlertMessage.noInternetConnection.description())
            return
        }
        
        if chatPagination.paginationType != .old {
            self.chatPagination.currentPageNumber = 1
            if isFirstTime == true {
                Utilities.showHUD(forView: self.view, excludeViews: [])
            } else {
                UIApplication.shared.isNetworkActivityIndicatorVisible = true
            }
            
        } else {
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
        }
        
        guard let userID = UserStore.shared.userID else {
            return
        }
        
        let messageObserver = ApiManager.shared.apiService.fetchMessagesForReceiverID(String(userID), senderID: senderID, page: chatPagination.currentPageNumber)
        let messageDisposable = messageObserver.subscribe(onNext: {(paginationObject) in
            Utilities.hideHUD(forView: self.view)
            
            if self.chatPagination.paginationType == .old && self.isFirstTime == false {
                DispatchQueue.main.async(execute: {
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                })
            }
            
            self.chatPagination.appendDataFromObject(paginationObject)
//            self.messages = (paginationObject.chats as [Message]).reversed()
            
           
            self.messages = self.chatPagination.chats.reversed()
            
            if self.isFirstTime == true{
            DispatchQueue.main.async(execute: {

                self.messagesCollectionView.reloadData()
                self.messagesCollectionView.scrollToBottom()
    
            })
            }else{
                DispatchQueue.main.async(execute: {
                self.messagesCollectionView.reloadDataAndKeepOffset()
                self.refreshControl.endRefreshing()
                })
            }
             self.isFirstTime = false
            
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.showAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        })
        messageDisposable.disposed(by: disposableBag)
    }
    
    func sendMessage(_ message: String) {
        guard Utilities.shared.isNetworkReachable() else {
            self.showAlertWithMessage(AlertMessage.noInternetConnection.description())
            return
        }
        
        guard let userID = UserStore.shared.userID else {
            return
        }
        Utilities.showHUD(forView: self.view, excludeViews: [])
        
        let messageObserver = ApiManager.shared.apiService.sendMessageToUserID(senderID!, senderID: String(userID), message: message)
        let messageDisposable = messageObserver.subscribe(onNext: {(successMessage) in
            Utilities.hideHUD(forView: self.view)
            print(successMessage)
//            self.messages.append(successMessage)
//            DispatchQueue.main.async(execute: {
//                self.messageInputBar.inputTextView.text = ""
//                self.messagesCollectionView.reloadData()
//                self.messagesCollectionView.scrollToBottom()
//            })
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.showAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        })
        messageDisposable.disposed(by: disposableBag)
    }

    
}
