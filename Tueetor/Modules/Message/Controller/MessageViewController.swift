//
//  MessageViewController.swift
//  Tueetor
//
//  Created by Phaninder on 08/04/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit
import RxSwift

class MessageViewController: BaseViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var searchTextFieldWidthConstraint: NSLayoutConstraint!

    var messagePagination = MessagePagination()
    var disposableBag = DisposeBag()
    var isFetchingData = false
    var isSearchActive = false
    var isFirstTime = true
    var chats: [Chat]!
    var searchString = ""
    var messages = [SampleMessage]()
    private let refreshControl = UIRefreshControl()

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView()
        if #available(iOS 10.0, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.addSubview(refreshControl)
        }
        searchTextFieldWidthConstraint.constant = 0.0
        refreshControl.addTarget(self, action: #selector(refreshData(_:)), for: .valueChanged)
        //self.fetchUserChat()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        (self.tabBarController as! BaseTabBarViewController).tabBar.isHidden = false
        (self.tabBarController as! BaseTabBarViewController).showCenterButton()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.navigationBar.isHidden = true
        isFirstTime = true
        messagePagination.paginationType = .new
        self.fetchUserChat()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @objc func refreshData(_ sender: Any) {
        self.messagePagination.paginationType = .new
        self.fetchUserChat()
        if let refreshControl = sender as? UIRefreshControl {
            refreshControl.endRefreshing()
        }
    }
    
    
    @IBAction func searchButtonTapped(_ sender: UIButton) {
        isSearchActive = !isSearchActive
        if isSearchActive {
            searchTextField.becomeFirstResponder()
        } else {
            searchTextField.resignFirstResponder()
        }
        self.view.layoutIfNeeded()
        UIView.animate(withDuration: 0.7, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 1, options: .curveEaseIn, animations: {
            self.searchTextFieldWidthConstraint.constant = self.isSearchActive ? ScreenWidth - 96.0 : 0
            let imageName = self.isSearchActive ? "search_active" : "search_inactive"
            self.searchButton.setImage(UIImage(named: imageName), for: .normal)
            self.searchButton.backgroundColor = self.isSearchActive ? themeBlueColor : UIColor.clear
            self.view.layoutIfNeeded()
        }) { (completed) in
            
        }
    }
    
    @IBAction func valueChangedInSearchField(_ sender: Any) {

    }

}

extension MessageViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let chatsArray = chats else {
            return 0
        }
        return chatsArray.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: MessagesHeaderTableViewCell.cellIdentifier(), for: indexPath) as! MessagesHeaderTableViewCell
            cell.configureCellWithChats(chats)
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: MessageTableViewCell.cellIdentifier(), for: indexPath) as! MessageTableViewCell
            cell.configureCellWithChat(chats[indexPath.row - 1])
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return searchString.isEmpty ? MessagesHeaderTableViewCell.cellHeight() : 0.0
        } else {
            return MessageTableViewCell.cellHeight()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard indexPath.row != 0 else {
            return
        }
        (self.tabBarController as! BaseTabBarViewController).tabBar.isHidden = true
        (self.tabBarController as! BaseTabBarViewController).hideCenterButton()
//        let controller = ConversationViewController()
//        self.navigationController?.pushViewController(controller, animated: true)
        let chatVC = ChatViewController.instantiateFromAppStoryboard(appStoryboard: .Message)
        chatVC.userTwoImageURL = chats[indexPath.row - 1].image
        chatVC.senderID = chats[indexPath.row - 1].senderID
        chatVC.userTwoName = chats[indexPath.row - 1].displayName
        chatVC.chatID = "\(chats[indexPath.row - 1].pID!)"
        self.navigationController?.pushViewController(chatVC, animated: true)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == messagePagination.messageItems.count && self.messagePagination.hasMoreToLoad && !isFetchingData {
            fetchUserChat()
        }
    }
}

extension MessageViewController {
    
    func fetchUserChat() {
        
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }
        
        if messagePagination.paginationType != .old {
            self.messagePagination.currentPageNumber = 1
            if isFirstTime == true {
                Utilities.showHUD(forView: self.view, excludeViews: [])
            } else {
                UIApplication.shared.isNetworkActivityIndicatorVisible = true
            }
        } else {
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
        }
        
        guard let userID = UserStore.shared.userID else {
            return
        }
        let page = String(messagePagination.currentPageNumber)
        isFetchingData = true
        
        let chatsObserver = ApiManager.shared.apiService.fetchUserChatBox(page,userID)
        let chatsDisposable = chatsObserver.subscribe(onNext: {(paginationObject) in
            
            self.isFetchingData = false
            if self.messagePagination.paginationType == .old && self.isFirstTime == false {
                DispatchQueue.main.async(execute: {
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                })
            }
            
            self.messagePagination.appendDataFromObject(paginationObject)
            
            //self.chats = chats
            
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                self.isFirstTime = false
                self.chats = self.messagePagination.messageItems
                
                //                self.tableView.isHidden = self.notificationPagination.notifications.count == 0
                //                self.emptyMessageLabel.isHidden = self.notificationPagination.notifications.count != 0
                self.tableView.reloadData()
            })
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.showSimpleAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        })
        chatsDisposable.disposed(by: disposableBag)
    }
    
    
}
