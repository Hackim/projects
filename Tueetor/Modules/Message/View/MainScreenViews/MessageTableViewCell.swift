//
//  MessageTableViewCell.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 13/06/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit
import SDWebImage

class MessageTableViewCell: UITableViewCell {

    var shadowLayer: CAShapeLayer!
    var shadowAdded: Bool = false
    
    @IBOutlet weak var holderView: UIView!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var indicatorView: UIView!
    
    let blueColor = UIColor(red: 0.1372, green: 0.6431, blue: 0.6941, alpha: 1)
    let orangeColor = UIColor(red: 0.8823, green: 0.6941, blue: 0.0470, alpha: 1)

    class func cellIdentifier() -> String {
        return "MessageTableViewCell"
    }
    
    class func cellHeight() -> CGFloat {
        return 137.0
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.holderView.layer.shadowPath = UIBezierPath(roundedRect:
        self.holderView.bounds, cornerRadius: self.holderView.layer.cornerRadius).cgPath
        self.holderView.layer.shadowColor = themeBlackColor.cgColor
        self.holderView.layer.shadowOpacity = 0.5
        self.holderView.layer.shadowOffset = CGSize(width: 1, height: 1)
        self.holderView.layer.shadowRadius = 5
        self.holderView.layer.masksToBounds = false
    }
    
    func configureCellWithSampleMessage(_ message: SampleMessage) {
        profileImageView.sd_setShowActivityIndicatorView(true)
        profileImageView.sd_setIndicatorStyle(.gray)
        profileImageView.sd_setImage(with: URL(string: message.imageURL), placeholderImage: UIImage(named: "profile_placeholder"), options: .retryFailed) { (image, error, cacheType, url) in
            if error == nil {
                //Do nothing
            }
        }
        
        nameLabel.text = message.name
        timeLabel.text = message.time
        messageLabel.text = message.message
        dateLabel.text = message.date
        indicatorView.backgroundColor = message.indicatorColorBlue ? blueColor : orangeColor
    }
    
    func configureCellWithChat(_ chat: Chat) {
        profileImageView.sd_setShowActivityIndicatorView(true)
        profileImageView.sd_setIndicatorStyle(.gray)
        profileImageView.sd_setImage(with: URL(string: chat.image), placeholderImage: UIImage(named: "profile_placeholder"), options: .retryFailed) { (image, error, cacheType, url) in
            if error == nil {
                //Do nothing
            }
        }
        nameLabel.text = chat.displayName
        timeLabel.text = Utilities.convertTime(timeString: chat.sendedOn, outputFormat: "h:mm a", inputFormat: "yyyy-MM-dd HH:mm:ss")
        messageLabel.text = chat.message
        dateLabel.text = Utilities.convertTime(timeString: chat.sendedOn, outputFormat: "E, d MMM yyyy", inputFormat: "yyyy-MM-dd HH:mm:ss")
        indicatorView.backgroundColor = chat.isRead ? blueColor : orangeColor
    }
    
}
