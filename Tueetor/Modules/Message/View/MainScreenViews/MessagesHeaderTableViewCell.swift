//
//  MessagesHeaderTableViewCell.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 13/06/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit

class MessagesHeaderTableViewCell: UITableViewCell {

    @IBOutlet weak var countMessageLabel: UILabel!
    
    class func cellIdentifier() -> String {
        return "MessagesHeaderTableViewCell"
    }
    
    class func cellHeight() -> CGFloat {
        return 134.0
    }
    
    func configureCellWithChats(_ chats: [Chat]) {
        var count = 0
        for chat in chats {
            count += chat.unreadCount
        }
        if count ==  1 {
            countMessageLabel.text = "You have ".localized + "\(count) " + "unread message".localized
        } else {
            countMessageLabel.text = "You have ".localized + "\(count) " + "unread messages".localized
        }
    }
}
