//
//  MySubjectPagination.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 13/06/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import Foundation
import Unbox

class MySubjectPagination: Unboxable {
    
    var subjects: [MySubject] = []
    private let limit = 10
    var currentPageNumber = 1
    var paginationType: PaginationType = .new
    var hasMoreToLoad: Bool
    
    init() {
        subjects = []
        currentPageNumber = 1
        paginationType = .new
        hasMoreToLoad = false
    }
    
    required init(unboxer: Unboxer) throws {
        if let subjectsArray = unboxer.dictionary["data"] as? [[String: AnyObject]] {
            for subjectDict in subjectsArray {
                let subject: MySubject = try unbox(dictionary: subjectDict)
                self.subjects.append(subject)
            }
        }
        hasMoreToLoad = self.subjects.count == limit
    }
    
    func appendDataFromObject(_ newPaginationObject: MySubjectPagination) {
        switch self.paginationType {
        case .new, .reload:
            self.subjects = [];
            self.subjects = newPaginationObject.subjects
            self.paginationType = .old
        case .old:
            self.subjects += newPaginationObject.subjects
            self.paginationType = .old
        }
        self.hasMoreToLoad = newPaginationObject.hasMoreToLoad
        self.currentPageNumber += 1
    }
    
}
