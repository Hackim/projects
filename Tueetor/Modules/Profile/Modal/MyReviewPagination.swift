//
//  MyReviewPagination.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 01/08/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import Foundation
import Unbox

class MyReviewPagination: Unboxable {
    
    var reviews: [MyReview] = []
    private let limit = 10
    var currentPageNumber = 1
    var paginationType: PaginationType = .new
    var hasMoreToLoad: Bool
    
    init() {
        reviews = []
        currentPageNumber = 1
        paginationType = .new
        hasMoreToLoad = false
    }
    
    required init(unboxer: Unboxer) throws {
        if let reviewsArray = unboxer.dictionary["data"] as? [[String: AnyObject]] {
            for reviewDict in reviewsArray {
                let review: MyReview = try unbox(dictionary: reviewDict)
                self.reviews.append(review)
            }
        }
        hasMoreToLoad = self.reviews.count == limit
    }
    
    func appendDataFromObject(_ newPaginationObject: MyReviewPagination) {
        switch self.paginationType {
        case .new, .reload:
            self.reviews = [];
            self.reviews = newPaginationObject.reviews
            self.paginationType = .old
        case .old:
            self.reviews += newPaginationObject.reviews
            self.paginationType = .old
        }
        self.hasMoreToLoad = newPaginationObject.hasMoreToLoad
        self.currentPageNumber += 1
    }
    
}
