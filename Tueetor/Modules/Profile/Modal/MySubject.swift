//
//  MySubject.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 13/06/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import Foundation
import Unbox

class MySubject: Unboxable {
    
    var daysArray = ["SUN", "MON", "TUE", "WED", "THU", "FRI", "SAT"]
    var subject: SubjectDetailedInfo!
    var availability: [String: [String]]
    var totalAvailability = 0
    
    required init(unboxer: Unboxer) throws {
        self.subject = unboxer.unbox(key: "subject")
        availability = ["SUN": [String](),
                        "MON": [String](),
                        "TUE": [String](),
                        "WED": [String](),
                        "THU": [String](),
                        "FRI": [String](),
                        "SAT": [String]()]
        if let availabilityObjArray = unboxer.dictionary["availability"] as? [String: AnyObject] {
            for day in daysArray {
                if let scheduleArray = availabilityObjArray[day] as? [String] {
                    availability[day] = scheduleArray
                    totalAvailability += scheduleArray.count
                }
            }
        }

    }
    
}
