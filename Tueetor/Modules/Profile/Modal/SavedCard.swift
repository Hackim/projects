//
//  SavedCard.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 06/08/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import Foundation
import Unbox

class SavedCard: Unboxable {
    
    var id: String!
    var brand: String!
    var last4: String!
    var cardImageName = "1"
    
    required init(unboxer: Unboxer) throws {
        self.id = unboxer.unbox(key: "id") ?? ""
        self.brand = unboxer.unbox(key: "brand") ?? ""
        self.last4 = unboxer.unbox(key: "last4") ?? ""
        switch self.brand {
        case "Visa",
             "Visa (debit)":
            self.cardImageName = "1"
        case "MasterCard",
             "MasterCard (debit)",
             "MasterCard (prepaid)":
            self.cardImageName = "2"
        case "American Express":
            self.cardImageName = "22"
        case "Discover":
            self.cardImageName = "14"
        case "Diners Club":
            self.cardImageName = "10"
        case "JCB":
            self.cardImageName = "16"
        case "UnionPay":
            self.cardImageName = "1"
        default:
            self.cardImageName = "1"
        }
    }
    
}
