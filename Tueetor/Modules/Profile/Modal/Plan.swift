//
//  Plan.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 02/08/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import Foundation
import Unbox

class Plan: Unboxable {
    
    var id: String!
    var countryID: String!
    var currencyCode: String!
    var stripePlanID: String!
    var title: String!
    var freeMonths: String!
    var details: String!
    var totalMonths: String!
    var price: String!
    var rank: String!
    var addedOn: String!
    var updatedOn: String!
    var isPrimary: String!
    var status: String!
    
    required init(unboxer: Unboxer) throws {
        self.id = unboxer.unbox(key: "id") ?? "1"
        self.countryID = unboxer.unbox(key: "country_id") ?? ""
        self.currencyCode = unboxer.unbox(key: "currency_code") ?? ""
        self.stripePlanID = unboxer.unbox(key: "strip_plan_id") ?? ""
        self.title = unboxer.unbox(key: "title") ?? ""
        self.freeMonths = unboxer.unbox(key: "free_months") ?? ""
        self.details = unboxer.unbox(key: "description") ?? ""
        self.totalMonths = unboxer.unbox(key: "total_months") ?? "0"
        self.price = unboxer.unbox(key: "price") ?? ""
        self.rank = unboxer.unbox(key: "plan_rank") ?? ""
        self.addedOn = unboxer.unbox(key: "addedon") ?? ""
        self.updatedOn = unboxer.unbox(key: "updatedon") ?? ""
        self.isPrimary = unboxer.unbox(key: "is_primary") ?? ""
        self.status = unboxer.unbox(key: "status") ?? ""
    }
    
}
