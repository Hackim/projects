//
//  MyReview.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 01/08/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import Foundation
import Unbox

class MyReview: Unboxable {
    
    var rate: String!
    var studentName: String!
    var studentEmail: String!
    var status: Bool = false
    var sentOn: String!
    var feedbackDate: String!

    required init(unboxer: Unboxer) throws {
        self.rate = unboxer.unbox(key: "rate") ?? "0"
        self.studentName = unboxer.unbox(key: "student_name") ?? ""
        self.studentEmail = unboxer.unbox(key: "student_email") ?? ""
        self.status = unboxer.unbox(key: "status") ?? false
        self.sentOn = unboxer.unbox(key: "sent_on") ?? ""
        self.feedbackDate = unboxer.unbox(key: "feedback_date") ?? ""
    }
    
}
