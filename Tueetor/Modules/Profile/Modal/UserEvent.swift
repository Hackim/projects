//
//  UserEvent.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 08/08/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import Foundation
import Unbox

class UserEvent: Unboxable {
    
    var id: String!
    var userID: String!
    var profileType: String!
    var day: String!
    var hours: String!
    var comments: String!
    var eventIds = [String]()
    
    required init(unboxer: Unboxer) throws {
        self.id = unboxer.unbox(key: "id") ?? "0"
        self.userID = unboxer.unbox(key: "user_id") ?? ""
        self.profileType = unboxer.unbox(key: "profile_type") ?? ""
        self.day = unboxer.unbox(key: "dayname") ?? ""
        self.hours = unboxer.unbox(key: "hours") ?? ""
        self.comments = unboxer.unbox(key: "comments") ?? ""
        self.eventIds = unboxer.unbox(key: "calender_events") ?? [String]()
    }
    
}
