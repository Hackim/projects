//
//  MyReviewTableViewCell.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 01/08/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit

class MyReviewTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    
    class func cellIdentifier() -> String {
        return "MyReviewTableViewCell"
    }
    
    func configureCellWithReview(_ review: MyReview) {
        nameLabel.text = review.studentName
        emailLabel.text = review.studentEmail
        dateLabel.text = review.sentOn
        statusLabel.text = review.status ? "Rated".localized : "Pending".localized
    }
    
    
}
