//
//  MyReviewHeaderTableViewCell.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 01/08/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit
import SwiftyStarRatingView
import SDWebImage

class MyReviewHeaderTableViewCell: UITableViewCell {

    @IBOutlet weak var ratingView: SwiftyStarRatingView!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!

    class func cellIdentifier() -> String {
        return "MyReviewHeaderTableViewCell"
    }
    
    class func cellHeight() -> CGFloat {
        return 171.0
    }
    
    func configureCell() {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        guard let user = appDelegate.user else {
            return
        }
        profileImage.sd_setShowActivityIndicatorView(true)
        profileImage.sd_setIndicatorStyle(.gray)
        profileImage.sd_setImage(with: URL(string: user.imageURL), placeholderImage: UIImage(named: "profile_placeholder"), options: .retryFailed) { (image, error, cacheType, url) in
            if error == nil {
                //Do nothing
            }
        }
        nameLabel.text = user.firstName + " " + user.lastName
        if let n = NumberFormatter().number(from: "3") {
            ratingView.value = CGFloat(truncating: n)
        }

    }
}
