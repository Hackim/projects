//
//  RequestReviewInfoTableViewCell.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 01/08/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit

class RequestReviewInfoTableViewCell: UITableViewCell {

    class func cellIdentifier() -> String {
        return "RequestReviewInfoTableViewCell"
    }
    
    class func cellHeight() -> CGFloat {
        return 262.0
    }
    
}
