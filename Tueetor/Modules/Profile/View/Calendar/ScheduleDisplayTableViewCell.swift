//
//  ScheduleDisplayTableViewCell.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 07/08/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit

class ScheduleDisplayTableViewCell: UITableViewCell {

    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var holderView: UIView!
    @IBOutlet weak var displayLabel: UILabel!
    
    class func cellIdentifier() -> String {
        return "ScheduleDisplayTableViewCell"
    }
    
    class func cellHeight() -> CGFloat {
        return 49.0
    }
    
    func configureCellWithTimeObject(_ timeObject: TimeObject) {
        holderView.backgroundColor = timeObject.allNames.count == 0 ? UIColor.white : UIColor(red: 0.8398, green: 0.8398, blue: 0.8398, alpha: 0.25)
        displayLabel.text = ""
        timeLabel.text = timeObject.time > 9 ? "\(timeObject.time):00" : "0\(timeObject.time):00"
        displayLabel.text = timeObject.allNames.joined(separator: ", ")
    }

}
