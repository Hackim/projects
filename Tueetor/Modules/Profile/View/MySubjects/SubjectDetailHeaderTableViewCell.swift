//
//  SubjectDetailHeaderTableViewCell.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 14/06/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit

class SubjectDetailHeaderTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var levelLabel: UILabel!
    
    class func cellIdentifier() -> String {
        return "SubjectDetailHeaderTableViewCell"
    }
    
    func configureCellWithSubject(_ subject: SubjectDetailedInfo) {
        nameLabel.text = subject.name
        levelLabel.text = subject.level
    }
}
