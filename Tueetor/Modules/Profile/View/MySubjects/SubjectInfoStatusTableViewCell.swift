//
//  SubjectInfoStatusTableViewCell.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 14/06/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit

protocol SubjectInfoStatusTableViewCellDelegate {
    func changeStatusForSubject(_ statusSwitch: UISwitch)
}
class SubjectInfoStatusTableViewCell: UITableViewCell {

    @IBOutlet weak var cellImageView: UIImageView!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var statusSwitch: UISwitch!
    
    var delegate: SubjectInfoStatusTableViewCellDelegate?
    class func cellIdentifier() -> String {
        return "SubjectInfoStatusTableViewCell"
    }
    
    class func cellHeight() -> CGFloat {
        return 63.0
    }

    func configureCellWithStatus(_ status: String) {
        let isActive = status == "0" ? false : true
        let statusString = isActive ? "ACTIVE".localized : "INACTIVE".localized
        let statusColor = isActive ? themeBlackColor : UIColor.red
        
        let attributedString = NSMutableAttributedString()
        attributedString.append(NSAttributedString(string: statusString,
                                                   attributes: [.font: textFieldLightFont,
                                                                .foregroundColor : statusColor]))
        statusLabel.attributedText  = attributedString
        statusSwitch.setOn(isActive, animated: true)
    }

    @IBAction func switchValueChanged(_ sender: UISwitch) {
        delegate?.changeStatusForSubject(sender)
    }

}
