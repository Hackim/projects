//
//  SubjectInfoTableViewCell.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 14/06/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit

class SubjectInfoTableViewCell: UITableViewCell {

    @IBOutlet weak var customTextField: TueetorTextField!
    @IBOutlet weak var cellImageView: UIImageView!
    
    class func cellIdentifier() -> String {
        return "SubjectInfoTableViewCell"
    }
    class func cellHeight() -> CGFloat {
        return 63.0
    }
    
    func configureCellForRowAtIndex(_ index: Int, withText text: String) {
        customTextField.tag = index
        customTextField.placeholder = text
        customTextField.title = text
        customTextField.textFont = textFieldLightFont
        customTextField.titleFont = textFieldDefaultFont
        var imageName = ""
        switch index {
        case SubjectDetailRowType.qualification.rawValue:
            imageName = "minimum_qualification"
        case SubjectDetailRowType.teachingSince.rawValue:
            imageName = "minimum_experience"
        case SubjectDetailRowType.sessionRate.rawValue:
            imageName = "budget_session"
        case SubjectDetailRowType.monthlyRate.rawValue:
            imageName = "budget_month"
        case SubjectDetailRowType.location.rawValue:
            imageName = "location"
        case SubjectDetailRowType.maxDistance.rawValue:
            imageName = "location_circle"
        default:
            imageName = ""
        }
        cellImageView.image = UIImage(named: imageName)

    }

    
}
