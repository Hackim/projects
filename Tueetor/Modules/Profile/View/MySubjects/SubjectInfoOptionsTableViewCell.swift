//
//  SubjectInfoOptionsTableViewCell.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 14/06/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit

protocol SubjectInfoOptionsTableViewCellDelegate {
    func showEditScreen()
    func deleteSubject()
    func showLocationInMap()
    func showSchedules()
}
class SubjectInfoOptionsTableViewCell: UITableViewCell {

    var delegate: SubjectInfoOptionsTableViewCellDelegate?
    
    class func cellIdentifier() -> String {
        return "SubjectInfoOptionsTableViewCell"
    }
    
    class func cellHeight() -> CGFloat {
        return 70.0
    }

    @IBAction func editButtonTapped(_ sender: UIButton) {
        delegate?.showEditScreen()
    }
    
    @IBAction func deleteButtonTapped(_ sender: UIButton) {
        delegate?.deleteSubject()
    }
    
    @IBAction func locationButtonTapped(_ sender: UIButton) {
        delegate?.showLocationInMap()
    }
    
    @IBAction func schedulesButtonTapped(_ sender: UIButton) {
        delegate?.showSchedules()
    }
    
    
    
}
