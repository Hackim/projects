//
//  MySubjectTableViewCell.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 13/06/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit

protocol MySubjectTableViewCellDelegate {
    func activeSwitchTapped(_ sender: UISwitch)
}


class MySubjectTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var levelLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var sessionCostLabel: UILabel!
    @IBOutlet weak var monthlyCostLabel: UILabel!
    @IBOutlet weak var activeSwitch: UISwitch!
    
    var delegate: MySubjectTableViewCellDelegate?
    
    class func cellIdentifier() -> String {
        return "MySubjectTableViewCell"
    }
    
    class func cellHeight() -> CGFloat {
        return 112.0
    }
    
    func configureCellWithSubject(_ subjectInfo: MySubject, forIndex index: Int) {
        nameLabel.text = subjectInfo.subject.name
        levelLabel.text = subjectInfo.subject.level
        activeSwitch.tag = index
        let isActive = subjectInfo.subject.status == "0" ? false : true
        let statusString = isActive ? "   ACTIVE".localized : "  INACTIVE".localized
        let statusColor = isActive ? themeBlueColor : UIColor.red
        
        let attributedString = NSMutableAttributedString()
        attributedString.append(NSAttributedString(string: "STATUS",
                                                   attributes: [.font: UIFont(name: "Montserrat-Light", size: 13.0)!,
                                                                .foregroundColor : themeBlackColor]))
        
        attributedString.append(NSAttributedString(string: statusString,
            attributes: [.font: UIFont(name: "Montserrat-Light", size: 13.0)!,
                         .foregroundColor : statusColor]))
        statusLabel.attributedText  = attributedString
        
        sessionCostLabel.text = "Rate per Session: ".localized + subjectInfo.subject.pricePerSession
        monthlyCostLabel.text = "Rate per Month: ".localized + subjectInfo.subject.pricePerMonth
        activeSwitch.setOn(subjectInfo.subject.status == "0" ? false : true, animated: true)
    }
    
    @IBAction func switchValueChanged(_ sender: UISwitch) {
        delegate?.activeSwitchTapped(sender)
    }
    
}
