//
//  MyDocumentTableViewCell.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 19/06/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit

protocol MyDocumentTableViewCellDelegate {
    func editButtonTappedForIndex(_ index: Int)
    func deleteButtonTappedForIndex(_ index: Int)
}
class MyDocumentTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var docImage: UIImageView!
    
    var delegate: MyDocumentTableViewCellDelegate?
    
    class func cellIdentifier() -> String {
        return "MyDocumentTableViewCell"
    }
    
    func configureCellWithDocument(_ document: Document, index: Int) {
        nameLabel.text = document.name
        if document.type == "pdf" {
            docImage.image = UIImage.init(named: "pdf")
        } else if imageExtensions.contains(document.type)
            || videoExtensions.contains(document.type) {
            docImage.sd_setImage(with: URL(string: document.thumbNail.isEmpty ? document.name : document.thumbNail), placeholderImage: UIImage(named: ""), options: .retryFailed) { (image, error, cacheType, url) in
                if error == nil {
                    //Do nothing
                }
            }
        } else if document.type == "doc"
            || document.type == "msword"
            || document.type == "ms-office" {
            docImage.image = UIImage.init(named: "doc")
        } else if document.type == "xls" {
            docImage.image = UIImage.init(named: "xls")
        } else {
            docImage.image = UIImage.init(named: "txt")
        }
    }
    
}
