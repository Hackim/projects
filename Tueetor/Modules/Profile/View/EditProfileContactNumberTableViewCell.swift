//
//  EditProfileContactNumberTableViewCell.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 05/06/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit

protocol EditProfileContactNumberTableViewCellDelegate {
    func showChangeNumberScreen()
}

class EditProfileContactNumberTableViewCell: UITableViewCell {

    @IBOutlet weak var changeNumber: UIButton!
    @IBOutlet weak var placeTextField: TueetorTextField!
    @IBOutlet weak var errorLabel: UILabel!

    var delegate: EditProfileContactNumberTableViewCellDelegate?

    class func cellIdentifier() -> String {
        return "EditProfileContactNumberTableViewCell"
    }
    
    func configureCellForRowAtIndex(_ index: Int, withText text: String) {
        placeTextField.tag = index
        placeTextField.placeholder = text
        placeTextField.title = text
        placeTextField.textFont = textFieldLightFont
        placeTextField.titleFont = textFieldDefaultFont
        placeTextField.placeholderFont = textFieldDefaultFont

    }

    @IBAction func changeNumberButtonTapped(_ sender: UIButton) {
        delegate?.showChangeNumberScreen()
    }

    
}
