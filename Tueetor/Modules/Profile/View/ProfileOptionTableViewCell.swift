//
//  ProfileOptionTableViewCell.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 18/05/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit

class ProfileOptionTableViewCell: UITableViewCell {

    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
    class func cellIdentifier() -> String {
        return "ProfileOptionTableViewCell"
    }
    
    class func cellHeight() -> CGFloat {
        return 74.0
    }
    
    func configureCellWithTitle(_ title: String, andImage image: String) {
        titleLabel.text = title
        iconImageView.image = UIImage(named: image)
    }
    
}
