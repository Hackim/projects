//
//  SubjectActivationTableViewCell.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 17/06/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit

protocol SubjectActivationTableViewCellDelegate {
    func activeButtonIsSetOn(_ isOn: Bool)
    func infoButtonTappedForActivationCell(_ button: UIButton)
}
class SubjectActivationTableViewCell: UITableViewCell {

    @IBOutlet weak var activationCheckBox: UIButton!
    @IBOutlet weak var infoButton: UIButton!

    var isActiveSelected = true
    var delegate: SubjectActivationTableViewCellDelegate?
    
    class func cellIdentifier() -> String {
        return "SubjectActivationTableViewCell"
    }
    
    class func cellHeight() -> CGFloat {
        return 60.0
    }
    
    func configureCellWithSelectionState(_ isSelected: Bool) {
        isActiveSelected = isSelected
        let imageName = isSelected ? "checkbox" : "checkbox_empty"
        activationCheckBox.setImage(UIImage.init(named: imageName), for: .normal)
    }
    
    @IBAction func activeButtonTapped(_ sender: UIButton) {
        isActiveSelected = !isActiveSelected
        let imageName = isActiveSelected ? "checkbox" : "checkbox_empty"
        activationCheckBox.setImage(UIImage.init(named: imageName), for: .normal)
        delegate?.activeButtonIsSetOn(isActiveSelected)
    }
    
    @IBAction func infoButtonTapped(_ sender: UIButton) {
        delegate?.infoButtonTappedForActivationCell(sender)
    }
}
