//
//  SuggestSubjectTableViewCell.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 19/08/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit

protocol SuggestSubjectTableViewCellDelegate {
    func showSuggestSubjectAlert()
}

class SuggestSubjectTableViewCell: UITableViewCell {

    var delegate: SuggestSubjectTableViewCellDelegate?
    
    class func cellIdentifier() -> String {
        return "SuggestSubjectTableViewCell"
    }
    
    class func cellHeight() -> CGFloat {
        return 50.0
    }
    
    @IBAction func suggestSubjectButtonTapped(_ sender: UIButton) {
        delegate?.showSuggestSubjectAlert()
    }
    
}
