//
//  SubjectPreferredLocationTableViewCell.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 17/06/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit

protocol SubjectPreferredLocationTableViewCellDelegate {
    func setLocationSelected(_ isSelected: Bool)
    func locationInfoButtonTapped(_ button: UIButton)
}
class SubjectPreferredLocationTableViewCell: UITableViewCell {

    @IBOutlet weak var anyLocationCheckBoxButton: UIButton!
    @IBOutlet weak var setLocationCheckBoxButton: UIButton!
    @IBOutlet weak var infoButton: UIButton!
    
    var isLocationButtonSelected = true
    var delegate: SubjectPreferredLocationTableViewCellDelegate?
    
    class func cellIdentifier() -> String {
        return "SubjectPreferredLocationTableViewCell"
    }
    
    class func cellHeight() -> CGFloat {
        return 100.0
    }
    
    
    func configureCellWithSelectionState(_ isSelected: Bool) {
        isLocationButtonSelected = isSelected
        if isSelected {
            setLocationCheckBoxButton.setImage(UIImage.init(named: "checkbox"), for: .normal)
            anyLocationCheckBoxButton.setImage(UIImage.init(named: "checkbox_empty"), for: .normal)
        } else {
            anyLocationCheckBoxButton.setImage(UIImage.init(named: "checkbox"), for: .normal)
            setLocationCheckBoxButton.setImage(UIImage.init(named: "checkbox_empty"), for: .normal)
        }
    }
    
    @IBAction func anyLocationButtonTapped(_ sender: UIButton) {
        if isLocationButtonSelected {
            isLocationButtonSelected = !isLocationButtonSelected
            anyLocationCheckBoxButton.setImage(UIImage.init(named: "checkbox"), for: .normal)
            setLocationCheckBoxButton.setImage(UIImage.init(named: "checkbox_empty"), for: .normal)
            delegate?.setLocationSelected(false)
        }
    }
    
    @IBAction func setLocationButtonTapped(_ sender: UIButton) {
        if !isLocationButtonSelected {
            isLocationButtonSelected = !isLocationButtonSelected
            setLocationCheckBoxButton.setImage(UIImage.init(named: "checkbox"), for: .normal)
            anyLocationCheckBoxButton.setImage(UIImage.init(named: "checkbox_empty"), for: .normal)
            delegate?.setLocationSelected(true)
        }
    }
    
    @IBAction func infoButtonTapped(_ sender: UIButton) {
        delegate?.locationInfoButtonTapped(sender)
    }
    
}
