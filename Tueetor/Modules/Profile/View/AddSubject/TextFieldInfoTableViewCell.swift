//
//  TextFieldInfoTableViewCell.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 17/06/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit

protocol TextFieldInfoTableViewCellDelegate {
    func infoButtonTapped(_ button: UIButton)
    func extraInfoButtonTapped(_ tag: Int)
}


class TextFieldInfoTableViewCell: UITableViewCell {

    @IBOutlet weak var customTextField: TueetorTextField!
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var infoButton: UIButton!
    @IBOutlet weak var extraIconButton: UIButton!
    
    var delegate: TextFieldInfoTableViewCellDelegate?
    
    class func cellIdentifier() -> String {
        return "TextFieldInfoTableViewCell"
    }
    
    func configureCellForRowAtIndex(_ index: Int, withText text: String) {
        infoButton.tag = index
        customTextField.tag = index
        customTextField.placeholder = text
        customTextField.title = text
        customTextField.textFont = textFieldLightFont
        customTextField.titleFont = textFieldDefaultFont
        customTextField.placeholderFont = textFieldDefaultFont
    }
    
    func configureCellWithAttributedString(_ attrString: NSAttributedString, atIndex index: Int) {
        infoButton.tag = index
        customTextField.tag = index
        customTextField.attributedPlaceholder = attrString
        customTextField.attributedText = attrString
        customTextField.textFont = textFieldLightFont
        customTextField.titleFont = textFieldDefaultFont
        customTextField.placeholderFont = textFieldDefaultFont
    }
    
    @IBAction func infoButtonTapped(_ sender: UIButton) {
        delegate?.infoButtonTapped(sender)
    }
    @IBAction func extraInfoButtonTapped(_ sender: UIButton) {
        delegate?.extraInfoButtonTapped(customTextField.tag)
    }
    
}

extension TextFieldInfoTableViewCell {
    
    // MARK: Subject
    class func validateSubject(subject: Subject?) -> String {
        if let _ = subject {
            return ""
        } else {
            return ValidationErrorMessage.subject.description()
        }
    }
    
    // MARK: Level
    class func validateLevel(level: Level?) -> String {
        if let _ = level {
            return ""
        } else {
            return ValidationErrorMessage.level.description()
        }
    }

    // MARK: Qualification
    class func validateQualification(qualification: Qualification?) -> String {
        if let _ = qualification {
            return ""
        } else {
            return ValidationErrorMessage.qualification.description()
        }
    }

    // MARK: Teaching Since
    class func validateTeachingSince(teachingSince: String?) -> String {
        if let _ = teachingSince {
            return ""
        } else {
            return ValidationErrorMessage.teachingSince.description()
        }
    }

    // MARK: Session Rate
    class func validateSessionRate(sessionRate: String?) -> String {
        if let rate = sessionRate, !rate.isEmpty {
            return ""
        } else {
            return ValidationErrorMessage.sessionRate.description()
        }
    }

    // MARK: Month Rate
    class func validateMonthRate(monthRate: String?) -> String {
        if let rate = monthRate, !rate.isEmpty {
            return ""
        } else {
            return ValidationErrorMessage.monthRate.description()
        }
    }

    // MARK: Address
    class func validateAddress(address: String?) -> String {
        if let _address = address, !_address.isEmpty {
            return ""
        } else {
            return ValidationErrorMessage.subjectAddress.description()
        }
    }

    // MARK: Distance
    class func validateDistance(distance: String?) -> String {
        if let _distance = distance, !_distance.isEmpty {
            return ""
        } else {
            return ValidationErrorMessage.distance.description()
        }
    }
    
    // MARK: Experience
    class func validateExperience(experience: Experience?) -> String {
        if let _ = experience {
            return ""
        } else {
            return ValidationErrorMessage.experience.description()
        }
    }


}
