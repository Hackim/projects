//
//  AddSubjectButtonTableViewCell.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 17/06/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit

protocol AddSubjectButtonTableViewCellDelegate {
    func addSubjectButtonTapped()
}

class AddSubjectButtonTableViewCell: UITableViewCell {

    @IBOutlet weak var addSubjectButton: UIButton!
    
    var delegate: AddSubjectButtonTableViewCellDelegate?
    
    class func cellIdentifier() -> String {
        return "AddSubjectButtonTableViewCell"
    }
    
    class func cellHeight() -> CGFloat {
        return 86.0
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        addSubjectButton.layer.shadowOffset = CGSize.init(width: 0, height: 5)
        addSubjectButton.layer.shadowRadius = 3
        addSubjectButton.layer.shadowColor = themeBlueColor.cgColor
        addSubjectButton.layer.shadowOpacity = 0.75
    }
    
    @IBAction func addSubjectButtonTapped(_ sender: UIButton) {
        delegate?.addSubjectButtonTapped()
    }
}
