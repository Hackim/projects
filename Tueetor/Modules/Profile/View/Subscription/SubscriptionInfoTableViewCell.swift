//
//  SubscriptionInfoTableViewCell.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 02/08/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit

class SubscriptionInfoTableViewCell: UITableViewCell {

    class func cellIdentifier() -> String {
        return "SubscriptionInfoTableViewCell"
    }
    
    class func cellHeight() -> CGFloat {
        return 200.0
    }
    
}
