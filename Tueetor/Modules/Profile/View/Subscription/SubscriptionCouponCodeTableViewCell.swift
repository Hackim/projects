//
//  SubscriptionCouponCodeTableViewCell.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 02/08/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit

protocol SubscriptionCouponCodeTableViewCellDelegate {
    func applyButtonTapped()
}

class SubscriptionCouponCodeTableViewCell: UITableViewCell {

    @IBOutlet weak var couponTextField: TueetorTextField!
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var applyButton: UIButton!
    
    var delegate: SubscriptionCouponCodeTableViewCellDelegate?
    
    class func cellIdentifier() -> String {
        return "SubscriptionCouponCodeTableViewCell"
    }
        
    @IBAction func applyButtonTapped(_ sender: UIButton) {
        delegate?.applyButtonTapped()
    }
    
    func validateCouponCode(_ coupon: String) {
        errorLabel.textColor = UIColor.red
        errorLabel.text = coupon.isEmpty ? "Please enter coupon code" : ""
    }
}
