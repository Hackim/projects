//
//  NewCardButtonTableViewCell.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 06/08/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit

protocol NewCardButtonTableViewCellDelegate {
    func addCardButtonTapped()
}

class NewCardButtonTableViewCell: UITableViewCell {

    @IBOutlet weak var addCardButton: UIButton!
    
    var delegate: NewCardButtonTableViewCellDelegate?
    
    class func cellIdentifier() -> String {
        return "NewCardButtonTableViewCell"
    }
    
    class func cellHeight() -> CGFloat {
        return 76.0
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        addCardButton.layer.shadowOffset = CGSize.init(width: 0, height: 5)
        addCardButton.layer.shadowRadius = 3
        addCardButton.layer.shadowColor = themeBlueColor.cgColor
        addCardButton.layer.shadowOpacity = 0.75
    }

    @IBAction func addButtonTapped(_ sender: UIButton) {
        delegate?.addCardButtonTapped()
    }
}
