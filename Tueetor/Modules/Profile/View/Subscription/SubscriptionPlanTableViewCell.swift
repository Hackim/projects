//
//  SubscriptionPlanTableViewCell.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 02/08/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit

class SubscriptionPlanTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var durationLabel: UILabel!
    @IBOutlet weak var costLabel: UILabel!
    @IBOutlet weak var currencyLabel: UILabel!
    @IBOutlet weak var holderView: UIView!
    
    class func cellIdentifier() -> String {
        return "SubscriptionPlanTableViewCell"
    }
    
    class func cellHeight() -> CGFloat {
        return 100.0
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.holderView.layer.shadowPath = UIBezierPath(roundedRect:
            self.holderView.bounds, cornerRadius: self.holderView.layer.cornerRadius).cgPath
        self.holderView.layer.shadowColor = themeBlackColor.cgColor
        self.holderView.layer.shadowOpacity = 0.5
        self.holderView.layer.shadowOffset = CGSize(width: 1, height: 1)
        self.holderView.layer.shadowRadius = 5
        self.holderView.layer.masksToBounds = false
    }

    func configureCellWithPlan(_ plan: Plan) {
        titleLabel.text = plan.title
        descriptionLabel.text = plan.details
        durationLabel.text = plan.totalMonths + (Int(plan.totalMonths)! == 1 ? " Month".localized : " Months".localized)
        costLabel.text = plan.price
        currencyLabel.text = plan.currencyCode
    }
}
