//
//  SavedCardTableViewCell.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 06/08/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit

class SavedCardTableViewCell: UITableViewCell {

    @IBOutlet weak var cardImageView: UIImageView!
    @IBOutlet weak var descLabel: UILabel!

    class func cellIdentifier() -> String {
        return "SavedCardTableViewCell"
    }
    
    class func cellHeight() -> CGFloat {
        return 89.0
    }

    func configureCellWithCard(_ savedCard: SavedCard) {
        cardImageView.image = UIImage(named: savedCard.cardImageName)
        descLabel.text = "XXXX " + savedCard.last4
    }
}
