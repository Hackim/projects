//
//  AboutTableViewCell.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 21/06/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit

class AboutTableViewCell: UITableViewCell {

    @IBOutlet weak var dummyTextField: TueetorTextField!
    @IBOutlet weak var textView: UITextView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    class func cellIdentifier() -> String {
        return "AboutTableViewCell"
    }
    
    
    func configureCellForRowAtIndex(_ index: Int, withText text: String) {
        dummyTextField.tag = index
        textView.tag = index
        dummyTextField.placeholder = text
        dummyTextField.title = text
        dummyTextField.textFont = textFieldLightFont
        dummyTextField.titleFont = textFieldDefaultFont
        dummyTextField.placeholderFont = textFieldDefaultFont
    }
    
    @IBAction func valueChangedInTextField(_ sender: TueetorTextField) {

    }
    
    
}
