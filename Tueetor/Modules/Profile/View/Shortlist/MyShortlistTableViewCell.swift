//
//  MyShortlistTableViewCell.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 23/06/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit
import SDWebImage

class MyShortlistTableViewCell: UITableViewCell {

    @IBOutlet weak var shortlistImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var subjectsLabel: UILabel!
    @IBOutlet weak var addedAtLabel: UILabel!
    @IBOutlet weak var reasonLabel: UILabel!

    class func cellIdentifier() -> String {
        return "MyShortlistTableViewCell"
    }
    
    func configureCellWithUser(_ shortlistUser: ShortlistedUser) {
        shortlistImageView.sd_setShowActivityIndicatorView(true)
        shortlistImageView.sd_setIndicatorStyle(.gray)
        shortlistImageView.sd_setImage(with: URL(string: shortlistUser.image), placeholderImage: UIImage(named: "profile_placeholder"), options: .retryFailed) { (image, error, cacheType, url) in
            if error == nil {
                //Do nothing
            }
        }
        
        nameLabel.text = shortlistUser.name
        addedAtLabel.text = shortlistUser.createdDate
        reasonLabel.text = shortlistUser.reason
        var subjectsText = ""
        for i in 0..<shortlistUser.subjects.count {
            let sub = shortlistUser.subjects[i]
            if i != shortlistUser.subjects.count - 1 {
                subjectsText += sub.name + ", "
            } else {
                subjectsText += sub.name
            }
        }
        subjectsLabel.text = subjectsText
    }
}
