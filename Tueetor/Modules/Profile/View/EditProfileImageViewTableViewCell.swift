//
//  EditProfileImageViewTableViewCell.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 28/05/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit

protocol EditProfileImageViewTableViewCellDelegate {
    func showImageOptions()
}

class EditProfileImageViewTableViewCell: UITableViewCell {

    @IBOutlet weak var profileButton: UIButton!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var addImage: UIImageView!
    
    var delegate: EditProfileImageViewTableViewCellDelegate?

    class func cellIdentifier() -> String {
        return "EditProfileImageViewTableViewCell"
    }
    
    class func cellHeight() -> CGFloat {
        return 150.0
    }
    
    @IBAction func profileButtonTapped(_ sender: UIButton) {
        delegate?.showImageOptions()
    }
    
}
