//
//  ProfileHeaderTableViewCell.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 18/05/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit
import SDWebImage

protocol ProfileHeaderTableViewCellDelegate {
    func showEditProfileScreen()
}

class ProfileHeaderTableViewCell: UITableViewCell {
    
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var addImage: UIImageView!
    @IBOutlet weak var accountTypeLabel: UILabel!
    
    var delegate: ProfileHeaderTableViewCellDelegate?
    
    class func cellIdentifier() -> String {
        return "ProfileHeaderTableViewCell"
    }
    
    func configureCellWithUser(_ user: User) {
        nameLabel.text = user.firstName + "  " + user.lastName
        accountTypeLabel.text = user.userType == "T" ? "Tutor".localized : "Student".localized
    }
    
    @IBAction func editButtonTapped(_ sender: UIButton) {
        delegate?.showEditProfileScreen()
    }
}
