//
//  MySubjectDetailViewController.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 14/06/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit
import RxSwift

class MySubjectDetailViewController: BaseViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var mySubject: MySubject!
    var subjectID: String!
    
    var labelFields = ["QUALIFICATION".localized, "TEACHING SINCE".localized, "RATE / SESSION".localized, "RATE / MONTH".localized, "LOCATION".localized, "MAXIMUM DISTANCE".localized, "STATUS".localized]
    var disposableBag = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()
        if Utilities.isUserTypeStudent() {
            labelFields = ["MINIMUM QUALIFICATION".localized, "MINIMUM QUALIFICATION".localized, "BUDGET / SESSION".localized, "BUDGET / MONTH".localized, "LOCATION".localized, "MAXIMUM DISTANCE".localized, "STATUS".localized]
        }
        if let _subjectID = subjectID {
            fetchSubjectDetailsForSubjectID(_subjectID)
        }
    }

    func updateDataIntoTextField(_ customTextField: TueetorTextField, fromCell: Bool) {
        switch customTextField.tag {
        case SubjectDetailRowType.qualification.rawValue:
            customTextField.text = mySubject.subject.qualification
            
        case SubjectDetailRowType.teachingSince.rawValue:
            if Utilities.isUserTypeStudent() {
                customTextField.text = mySubject.subject.experience
            } else {
                customTextField.text = mySubject.subject.teachingSince
            }
            
        case SubjectDetailRowType.sessionRate.rawValue:
            customTextField.text = mySubject.subject.pricePerSession
            
        case SubjectDetailRowType.monthlyRate.rawValue:
            customTextField.text = mySubject.subject.pricePerMonth
            
        case SubjectDetailRowType.location.rawValue:
            if let address = mySubject.subject.address, !address.isEmpty {
                customTextField.text = address
            } else {
                customTextField.text = "Any Location".localized
            }
            
        case SubjectDetailRowType.maxDistance.rawValue:
            customTextField.text = mySubject.subject.abilityToTravel
            
        case SubjectDetailRowType.status.rawValue:
            customTextField.text = mySubject.subject.status
        default:
            break
        }
    }


}

extension MySubjectDetailViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let _ = mySubject {
            return 9
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case SubjectDetailRowType.header.rawValue:
            let cell = tableView.dequeueReusableCell(withIdentifier: SubjectDetailHeaderTableViewCell.cellIdentifier(), for: indexPath) as! SubjectDetailHeaderTableViewCell
            cell.configureCellWithSubject(mySubject.subject)
            return cell
        case SubjectDetailRowType.status.rawValue:
            let cell = tableView.dequeueReusableCell(withIdentifier: SubjectInfoStatusTableViewCell.cellIdentifier(), for: indexPath) as! SubjectInfoStatusTableViewCell
            cell.delegate = self
            cell.configureCellWithStatus(mySubject.subject.status)
            return cell
        case SubjectDetailRowType.options.rawValue:
            let cell = tableView.dequeueReusableCell(withIdentifier: SubjectInfoOptionsTableViewCell.cellIdentifier(), for: indexPath) as! SubjectInfoOptionsTableViewCell
            cell.delegate = self
            return cell
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: SubjectInfoTableViewCell.cellIdentifier(), for: indexPath) as! SubjectInfoTableViewCell
            cell.configureCellForRowAtIndex(indexPath.row, withText: labelFields[indexPath.row - 1])
            updateDataIntoTextField(cell.customTextField, fromCell: true)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case SubjectDetailRowType.header.rawValue:
            tableView.estimatedRowHeight = 134.0
            return UITableViewAutomaticDimension
        case SubjectDetailRowType.status.rawValue:
            return SubjectInfoStatusTableViewCell.cellHeight()
        case SubjectDetailRowType.options.rawValue:
            return SubjectInfoOptionsTableViewCell.cellHeight()
        default:
            return SubjectInfoTableViewCell.cellHeight()
        }
    }
}

extension MySubjectDetailViewController: SubjectInfoOptionsTableViewCellDelegate {
    
    func showEditScreen() {
        let editSubjectViewController = AddSubjectViewController.instantiateFromAppStoryboard(appStoryboard: .Profile)
        editSubjectViewController.mySubject = mySubject
        self.navigationController?.pushViewController(editSubjectViewController, animated: true)
    }
    
    func deleteSubject() {
        let alert = UIAlertController(title: AppName, message: AlertMessage.deleteSubject.description(), preferredStyle: .alert)
        
        let confirmAction = UIAlertAction(title: AlertButton.confirm.description(), style: .default, handler: { (action) in
            DispatchQueue.main.async {
                self.deleteSubjectWithID(String(self.mySubject.subject.id))
            }
        })
        alert.addAction(confirmAction)
        
        let cancelAction = UIAlertAction(title: AlertButton.cancel.description(), style: .default, handler: nil)
        alert.addAction(cancelAction)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func showLocationInMap() {
        let subject = mySubject.subject
        let locationViewController = LocationViewController.instantiateFromAppStoryboard(appStoryboard: .Profile)
        var latitude = ""
        var longitude = ""
        if let lat = subject?.latitude, !lat.isEmpty {
            latitude = lat
            longitude = subject!.logitude
        } else {
            let user = (UIApplication.shared.delegate as! AppDelegate).user
            latitude = user?.latitude ?? "0"
            longitude = user?.longitude ?? "0"
        }
        locationViewController.locationToShow = TueetorLocation.init(name: subject?.name ?? "", lat: latitude, long: longitude)
        navigationController?.pushViewController(locationViewController, animated: true)

    }
    
    func showSchedules() {
        let availabilityViewController = MyAvailabilityViewController.instantiateFromAppStoryboard(appStoryboard: .Profile)
        availabilityViewController.isEditable = false
        availabilityViewController.schedules = mySubject.availability
        navigationController?.pushViewController(availabilityViewController, animated: true)

    }
}

extension MySubjectDetailViewController: SubjectInfoStatusTableViewCellDelegate {

    func changeStatusForSubject(_ statusSwitch: UISwitch) {
        changeSubjectStatusForId(String(mySubject.subject.id), isOn: statusSwitch.isOn)
    }

}

extension MySubjectDetailViewController {
    
    func deleteSubjectWithID(_ subjectID: String) {
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }
        
        guard let userID = UserStore.shared.userID else {
            return
        }
        
        Utilities.showHUD(forView: self.view, excludeViews: [])
        
        let deleteSubjectObserver = ApiManager.shared.apiService.deleteSubjectWithID(subjectID, forUser: String(userID))
        let deleteSubjectDisposable = deleteSubjectObserver.subscribe(onNext: {(message) in
            Utilities.hideHUD(forView: self.view)
            NotificationCenter.default.post(name: .subjectsUpdated, object: nil, userInfo:nil)

            DispatchQueue.main.async {
                let alert = UIAlertController(title: AppName, message: message, preferredStyle: .alert)
                
                let okAction = UIAlertAction(title: AlertButton.ok.description(), style: .default, handler: { (action) in
                    DispatchQueue.main.async {
                        self.navigationController?.popViewController(animated: true)
                    }
                })
                alert.addAction(okAction)
                self.present(alert, animated: true, completion: nil)
            }
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.showSimpleAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        })
        deleteSubjectDisposable.disposed(by: disposableBag)
    }
    
    func changeSubjectStatusForId(_ subjectID: String, isOn: Bool) {
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }
        
        guard let userID = UserStore.shared.userID else {
            return
        }
        
        Utilities.showHUD(forView: self.view, excludeViews: [])
        
        let subjectStatusObserver = ApiManager.shared.apiService.changeStatusOfSubject(subjectID, forUser: String(userID))
        let subjectStatustDisposable = subjectStatusObserver.subscribe(onNext: {(message) in
            Utilities.hideHUD(forView: self.view)
            self.mySubject.subject.status = isOn ? "1" : "0"
            NotificationCenter.default.post(name: .subjectsUpdated, object: nil, userInfo:nil)
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.showSimpleAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        })
        subjectStatustDisposable.disposed(by: disposableBag)
    }

    func fetchSubjectDetailsForSubjectID(_ subID: String) {
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }
        
        guard let userID = UserStore.shared.userID else {
            return
        }
        Utilities.showHUD(forView: self.view, excludeViews: [])
        
        let subjectObserver = ApiManager.shared.apiService.fetchSubjectDetailWithID(subID, userID: String(userID))
        let subjectDisposable = subjectObserver.subscribe(onNext: {(userSubject) in
            Utilities.hideHUD(forView: self.view)
            self.mySubject = userSubject
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.showSimpleAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        })
        subjectDisposable.disposed(by: disposableBag)

    }
}
