//
//  MyDocumentsViewController.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 18/06/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit
import RxSwift
import Alamofire
import MobileCoreServices

class MyDocumentsViewController: BaseViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var emptyMessageLabel: UILabel!
    
    var disposableBag = DisposeBag()
    var isFetchingData = false
    var isFirstTime = true
    var documents = [Document]()
    var selectedDocumentData: Data!
    
    var refreshData = false

    private let refreshControl = UIRefreshControl()

    override func viewDidLoad() {
        super.viewDidLoad()
        (self.tabBarController as! BaseTabBarViewController).tabBar.isHidden = true
        (self.tabBarController as! BaseTabBarViewController).hideCenterButton()
        tableView.tableFooterView = UIView()
        if #available(iOS 10.0, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.addSubview(refreshControl)
        }
        refreshControl.addTarget(self, action: #selector(refreshData(_:)), for: .valueChanged)
        fetchMyDocuments()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    @objc func refreshData(_ sender: Any) {
        fetchMyDocuments()
        if let refreshControl = sender as? UIRefreshControl {
            refreshControl.endRefreshing()
        }
    }

    @IBAction func addButtonTapped(_ sender: UIButton) {
        showImageOptions()
    }

    func showImageOptions() {
        let alert = UIAlertController(title: "Choose File".localized, message: nil, preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Document".localized, style: .default, handler: { _ in
            self.documentsLibrary()
        }))

        alert.addAction(UIAlertAction(title: "Camera".localized, style: .default, handler: { _ in
            self.camera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery".localized, style: .default, handler: { _ in
            self.photoLibrary()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel".localized, style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }

    func showDocumentTitleAlert() {
        let alertController = UIAlertController(title: AppName,
                                                message: AlertMessage.docDesc.description(),
                                                preferredStyle: .alert)
        
        
        let confirmAction = UIAlertAction(title: AlertButton.confirm.description(),
                                          style: .default) { [weak alertController] _ in
                                            guard let alertController = alertController, let textField = alertController.textFields?.first else { return }
                                            DispatchQueue.main.async {
                                                print(textField.text!)
                                                self.uploadDocument(documentData: self.selectedDocumentData, decription: textField.text!)
                                            }
        }
        confirmAction.isEnabled = false
        alertController.addAction(confirmAction)
        
        alertController.addTextField { textField in
            textField.placeholder = "Document description"

            NotificationCenter.default.addObserver(forName: NSNotification.Name.UITextFieldTextDidChange, object: textField, queue: OperationQueue.main, using: { (notification) in
                confirmAction.isEnabled = !textField.text!.isEmpty
            })
        }
        
        
        let cancelAction = UIAlertAction(title: AlertButton.cancel.description(), style: .cancel, handler: nil)
        alertController.addAction(cancelAction)
        present(alertController, animated: true, completion: nil)
    }
    
    func showRenameDocumentTitleAlert(_ index: Int) {
        let alertController = UIAlertController(title: AppName,
                                                message: AlertMessage.docDescRename.description(),
                                                preferredStyle: .alert)
        
        
        let confirmAction = UIAlertAction(title: AlertButton.confirm.description(),
                                          style: .default) { [weak alertController] _ in
                                            guard let alertController = alertController, let textField = alertController.textFields?.first else { return }
                                            DispatchQueue.main.async {
                                                print(textField.text!)
                                                self.renameDocumentAtIndex(index, withNewDesc: textField.text!)
                                            }
        }
        confirmAction.isEnabled = false
        alertController.addAction(confirmAction)
        
        alertController.addTextField { textField in
            textField.placeholder = "New description"
            
            NotificationCenter.default.addObserver(forName: NSNotification.Name.UITextFieldTextDidChange, object: textField, queue: OperationQueue.main, using: { (notification) in
                confirmAction.isEnabled = !textField.text!.isEmpty
            })
        }
        
        
        let cancelAction = UIAlertAction(title: AlertButton.cancel.description(), style: .cancel, handler: nil)
        alertController.addAction(cancelAction)
        present(alertController, animated: true, completion: nil)
    }
    
    func showDeleteAlertForIndex(_ index: Int) {
        let alert = UIAlertController(title: AppName, message: AlertMessage.delete.description(), preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: AlertButton.cancel.description(), style: .default, handler: nil)
        alert.addAction(cancelAction)

        let confirmAction = UIAlertAction(title: AlertButton.confirm.description(), style: .default, handler: { (action) in
            DispatchQueue.main.async {
                self.deleteDocumentAtIndex(index)
            }
        })
        alert.addAction(confirmAction)
        
        
        self.present(alert, animated: true, completion: nil)
    }

    func showDataAppropritely() {
        if self.documents.count == 0 {
            tableView.isHidden = true
            emptyMessageLabel.isHidden = false
        } else {
            tableView.isHidden = false
            emptyMessageLabel.isHidden = true
        }
        tableView.reloadData()
    }


}

extension MyDocumentsViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if documents.count == 0 {
            return 0
        }
        return documents.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == documents.count {
            let cell = tableView.dequeueReusableCell(withIdentifier: EmptyMessageTableViewCell.cellIdentifier(), for: indexPath) as! EmptyMessageTableViewCell
            cell.messageLabel.text = "Swipe left to perform actions".localized
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: MyDocumentTableViewCell.cellIdentifier(), for: indexPath) as! MyDocumentTableViewCell
        cell.configureCellWithDocument(documents[indexPath.row], index: indexPath.row)
        cell.delegate = self
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == documents.count {
            return 44.0
        }
        tableView.estimatedRowHeight = 98.0
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == documents.count {
            return
        }
        let documentsPage = DocumentPageViewController.instantiateFromAppStoryboard(appStoryboard: .Profile)
        var names = [String]()
        var links = [String]()
        for document in documents {
            names.append(document.name)
            links.append(document.url)
        }
        documentsPage.titles = names
        documentsPage.links = links
        documentsPage.startIndex = indexPath.row
        self.navigationController?.pushViewController(documentsPage, animated: true)
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let rename = UITableViewRowAction(style: .normal, title: "Rename".localized) { action, index in
            DispatchQueue.main.async {
                self.showRenameDocumentTitleAlert(index.row)
            }
        }
        
        rename.backgroundColor = UIColor.init(red: 0.3843, green: 0.7882, blue: 0.4156, alpha: 1)
        
        let delete = UITableViewRowAction(style: .destructive, title: "Delete".localized) { action, index in
            DispatchQueue.main.async {
                self.showDeleteAlertForIndex(index.row)
            }
        }
        delete.backgroundColor = UIColor.init(red: 0.8941, green: 0.3764, blue: 0.3607, alpha: 1)
        
        return [delete, rename]

    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        if indexPath.row == documents.count {
            return false
        }
        return true
    }

    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        
    }

    
}

extension MyDocumentsViewController: MyDocumentTableViewCellDelegate {
    
    func editButtonTappedForIndex(_ index: Int) {
        print("Edit")
    }
    
    func deleteButtonTappedForIndex(_ index: Int) {
        print("Delete")
    }
    
}

extension MyDocumentsViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func camera() {
        if UIImagePickerController.isSourceTypeAvailable(.camera){
            let myPickerController = UIImagePickerController()
            myPickerController.delegate = self;
            myPickerController.sourceType = .camera
            myPickerController.allowsEditing = true
            self.present(myPickerController, animated: true, completion: nil)
        }
        
    }
    
    func photoLibrary() {

        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
            let myPickerController = UIImagePickerController()
            myPickerController.delegate = self;
            myPickerController.sourceType = .savedPhotosAlbum
            myPickerController.mediaTypes = ["public.image", "public.movie"]
            myPickerController.allowsEditing = true
            self.present(myPickerController, animated: true, completion: nil)
        }
    }
    
    func documentsLibrary() {
        let importMenu = UIDocumentMenuViewController(documentTypes: [String(kUTTypePDF), String(kUTTypePNG), String(kUTTypeImage), "public.image", "public.video", "public.movie"], in: .import)
        //, "com.microsoft.word.doc", "com.microsoft.excel.xls"
        importMenu.delegate = self
        importMenu.modalPresentationStyle = .formSheet
        self.present(importMenu, animated: true, completion: nil)
    }
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        dismiss(animated: true, completion: nil)
        var urlString: URL!
        
        if let myURL = info["UIImagePickerControllerMediaURL"] as? URL {
            urlString = myURL
        } else if let pickedImageURL = info["UIImagePickerControllerImageURL"] as? URL {
            urlString = pickedImageURL
        }
        if let fileURL = urlString {
            do {
            selectedDocumentData = try Data.init(contentsOf: fileURL)
            } catch {
                //Do nothing
            }
        } else {
            let image = info[UIImagePickerControllerEditedImage] as! UIImage
            selectedDocumentData = UIImageJPEGRepresentation(image, 0.5)
        }
        let bcf = ByteCountFormatter()
        bcf.allowedUnits = [.useMB] // optional: restricts the units to MB only
        bcf.countStyle = .file
        let sizeString = bcf.string(fromByteCount: Int64(selectedDocumentData.count))
        let size = CGFloat((sizeString as NSString).floatValue)
        if size > 15 {
            showSimpleAlertWithMessage("Document size cannot be greater than 15MB".localized)
            return
        }
        print("formatted result: \(sizeString)")
        showDocumentTitleAlert()
    }
    
}

extension MyDocumentsViewController: UIDocumentMenuDelegate,UIDocumentPickerDelegate {

    public func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        let myURL = url as URL
        do {
            selectedDocumentData = try Data.init(contentsOf: myURL)
            let bcf = ByteCountFormatter()
            bcf.allowedUnits = [.useMB]
            bcf.countStyle = .file
            let sizeString = bcf.string(fromByteCount: Int64(selectedDocumentData.count))
            let size = CGFloat((sizeString as NSString).floatValue)
            if size > 15 {
                showSimpleAlertWithMessage("Document size cannot be greater than 15MB".localized)
                return
            }
            print("formatted result: \(sizeString)")
            showDocumentTitleAlert()
        } catch {
            
        }
        print("import result : \(myURL)")
    }


    public func documentMenu(_ documentMenu:UIDocumentMenuViewController, didPickDocumentPicker documentPicker: UIDocumentPickerViewController) {
        documentPicker.delegate = self
        present(documentPicker, animated: true, completion: nil)
    }


    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        print("view was cancelled")
        dismiss(animated: true, completion: nil)
    }
}

extension MyDocumentsViewController {
    
    func fetchMyDocuments() {
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }
        guard let userID = UserStore.shared.userID else {
            return
        }
        
        if isFirstTime == true {
            Utilities.showHUD(forView: self.view, excludeViews: [])
        } else {
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
        }
        let documentObserver = ApiManager.shared.apiService.fetchDocumentsForUserID(userID)
        let documentDisposable = documentObserver.subscribe(onNext: {(documentsArray) in
            if self.isFirstTime == false {
                DispatchQueue.main.async(execute: {
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                })
            }
            self.isFirstTime = false
            self.documents = documentsArray
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                self.showDataAppropritely()
            })
        }, onError: {(error) in
            self.isFetchingData = false
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
            })
        })
        documentDisposable.disposed(by: disposableBag)
    }
    
    
    func uploadDocument(documentData: Data, decription: String) {

        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }
        guard let userID = UserStore.shared.userID else {
            return
        }

        Utilities.showHUD(forView: self.view, excludeViews: [])

        let documentDict : [String: String] = ["user_id": String(userID),
                                                "doc_desc": decription]
        
        print("documentDict: \n", documentDict)
        
        let url = "http://tueetoradmin.hipster-inc.com/public/post/upload_doc"
        
        let headers: HTTPHeaders = [
            "Content-type": "multipart/form-data"
        ]
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in documentDict {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            var fileName = "\(userID)"
            let mimeType = documentData.mimeType
            if mimeType == "image/jpeg" {
                fileName += ".jpeg"
            } else if mimeType == "application/pdf" {
                fileName += ".pdf"
            } else if mimeType == "application/octet-stream" {
                fileName += ".mp4"
            } else {
                fileName += ".png"
            }
            
            multipartFormData.append(documentData, withName: "doc", fileName: fileName, mimeType: mimeType)
            
        }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (result) in
            switch result{
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    print("Succesfully uploaded")
                    DispatchQueue.main.async(execute: {
                        Utilities.hideHUD(forView: self.view)
                    })
                    if let _ = response.error{
                        self.showSimpleAlertWithMessage("Failed to edit account.".localized)
                        return
                    }
                    let json = try? JSONSerialization.jsonObject(with: response.data!, options: .mutableContainers)
                    print(json)
                    if let responseDict = json as? [String: AnyObject], let message = responseDict["message"] as? String {
                        
                        DispatchQueue.main.async(execute: {
                            self.showSimpleAlertWithMessage(message)
                            self.fetchMyDocuments()
                        })
                    }
                }
            case .failure(let error):
                DispatchQueue.main.async(execute: {
                    Utilities.hideHUD(forView: self.view)
                    if let error_ = error as? ResponseError {
                        self.showSimpleAlertWithMessage(error_.description())
                    } else {
                        if !error.localizedDescription.isEmpty {
                            self.showSimpleAlertWithMessage(error.localizedDescription)
                        }
                    }
                })
            }
        }
    }
  
    
    func deleteDocumentAtIndex(_ index: Int) {
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }
        
        guard let userID = UserStore.shared.userID else {
            return
        }
        
        Utilities.showHUD(forView: self.view, excludeViews: [])
        
        let deleteDocumentObserver = ApiManager.shared.apiService.deleteDocumentForUserID(userID, documentID: documents[index].id)
        let deleteDocumentDisposable = deleteDocumentObserver.subscribe(onNext: {(message) in
            Utilities.hideHUD(forView: self.view)
            
            DispatchQueue.main.async {
                self.showSimpleAlertWithMessage(message)
                self.fetchMyDocuments()
            }
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.showSimpleAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        })
        deleteDocumentDisposable.disposed(by: disposableBag)
    }
    
    func renameDocumentAtIndex(_ index: Int, withNewDesc newText: String) {
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }
        
        guard let userID = UserStore.shared.userID else {
            return
        }
        
        Utilities.showHUD(forView: self.view, excludeViews: [])
        
        let renameDocumentObserver = ApiManager.shared.apiService.renameDocumentForUserID(userID, documentID: documents[index].id, desc: newText)
        let renameDocumentDisposable = renameDocumentObserver.subscribe(onNext: {(message) in
            Utilities.hideHUD(forView: self.view)
            
            DispatchQueue.main.async {
                self.showSimpleAlertWithMessage(message)
                self.fetchMyDocuments()
            }
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.showSimpleAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        })
        renameDocumentDisposable.disposed(by: disposableBag)
    }

    
}

