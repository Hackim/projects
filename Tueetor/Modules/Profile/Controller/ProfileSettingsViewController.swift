//
//  ProfileSettingsViewController.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 28/05/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit

class ProfileSettingsViewController: BaseViewController {

    var shouldShowMobileScreen = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        (self.tabBarController as! BaseTabBarViewController).tabBar.isHidden = true
        (self.tabBarController as! BaseTabBarViewController).hideCenterButton()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "ProfilePagerView") {
            let secondViewController = segue.destination  as! ProfileSettingsPagerViewController
            secondViewController.shouldShowMobileScreen = self.shouldShowMobileScreen
            // Pass data to secondViewController before the transition
        }

    }

}
