//
//  CalendarOverviewViewController.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 07/08/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit
import SpreadsheetView
import RxSwift

class CalendarOverviewViewController: BaseViewController {

    @IBOutlet weak var calendarSpreadSheet: SpreadsheetView!
    
    var headers = ["".localized, "MON".localized, "TUE".localized, "WED".localized, "THU".localized, "FRI".localized, "SAT".localized, "SUN".localized]
    var data = [[String]]()
    var schedules: [String : [Schedule]]!
    var userEvents: [String: [UserEvent]]!
    var disposableBag = DisposeBag()
    var shouldReloadDataOnViewDidAppear = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fetchUserSchedules()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        (self.tabBarController as! BaseTabBarViewController).tabBar.isHidden = true
        (self.tabBarController as! BaseTabBarViewController).hideCenterButton()
        if shouldReloadDataOnViewDidAppear {
            calendarSpreadSheet.reloadData()
            shouldReloadDataOnViewDidAppear = false
        }
    }
    
    func refreshData() {
        data = getDataArrayFromSchedules(schedules, events: userEvents)
        calendarSpreadSheet.delegate = self
        calendarSpreadSheet.dataSource = self
        calendarSpreadSheet.isScrollEnabled = true
        calendarSpreadSheet.register(HeaderCell.self, forCellWithReuseIdentifier: String(describing: HeaderCell.self))
        calendarSpreadSheet.register(TextCell.self, forCellWithReuseIdentifier: String(describing: TextCell.self))
        calendarSpreadSheet.register(CheckBoxCell.self, forCellWithReuseIdentifier: String(describing: CheckBoxCell.self))
    }
    
    func getDataArrayFromSchedules(_ schedules: [String: [Schedule]], events: [String: [UserEvent]]) -> [[String]] {
        var completeData = [[String]]()
        let weeksArray = ["MON", "TUE", "WED", "THU", "FRI", "SAT", "SUN"]
        for i in 0...23 {
            var hourSchedule = [String]()
            if i > 9 {
                hourSchedule = ["\(i):00"]
            } else {
                hourSchedule = ["0\(i):00"]
            }
            for day in weeksArray {
                
                let daysSchedule = schedules[day]
                let personalEvents = userEvents[day]
                let filteredSchedules = daysSchedule?.filter({ (schedule) -> Bool in
                    return schedule.hours == String(i)
                })
                let filteredEvents = personalEvents?.filter({ (e) -> Bool in
                    return e.hours == String(i)
                })
                
                if let schedulesArray = filteredSchedules, schedulesArray.count > 0 {
                    hourSchedule.append("2")
                } else if let _filteredEvents = filteredEvents, _filteredEvents.count > 0 {
                    hourSchedule.append("1")
                } else {
                    hourSchedule.append("0")
                }
            }
            completeData.append(hourSchedule)
        }
        return completeData
    }

}

extension CalendarOverviewViewController: SpreadsheetViewDataSource, SpreadsheetViewDelegate {
    
    func numberOfColumns(in spreadsheetView: SpreadsheetView) -> Int {
        return headers.count
    }
    
    func numberOfRows(in spreadsheetView: SpreadsheetView) -> Int {
        return 1 + data.count
    }
    
    func spreadsheetView(_ spreadsheetView: SpreadsheetView, widthForColumn column: Int) -> CGFloat {
        if column == 0 {
            return 50
        } else {
            return (ScreenWidth - 60)/7
        }
    }
    
    func spreadsheetView(_ spreadsheetView: SpreadsheetView, heightForRow row: Int) -> CGFloat {
        if case 0 = row {
            return 50
        } else {
            return 35
        }
    }
    
    func frozenRows(in spreadsheetView: SpreadsheetView) -> Int {
        return 1
    }

    func spreadsheetView(_ spreadsheetView: SpreadsheetView, cellForItemAt indexPath: IndexPath) -> Cell? {
        if case 0 = indexPath.row {
            let cell = calendarSpreadSheet.dequeueReusableCell(withReuseIdentifier: String(describing: HeaderCell.self), for: indexPath) as! HeaderCell
            cell.contentView.backgroundColor = themeLightGrayColor

            cell.gridlines.left = .solid(width: 1, color: themeLightGrayColor)
            cell.gridlines.top = .none
            cell.gridlines.right = .solid(width: 1, color: themeLightGrayColor)
            cell.gridlines.bottom = .none
            cell.label.text = headers[indexPath.column]
            
            return cell
        } else {
            let dataString = data[indexPath.row - 1][indexPath.column]
            
            if indexPath.column == 0 {
                let cell = calendarSpreadSheet.dequeueReusableCell(withReuseIdentifier: String(describing: TextCell.self), for: indexPath) as! TextCell
                cell.label.text = dataString
                cell.label.backgroundColor = UIColor.clear
                cell.gridlines.left = .none
                cell.gridlines.top = .none
                cell.gridlines.right = .none
                cell.gridlines.bottom = .none

                return cell
            } else {
                let cell = calendarSpreadSheet.dequeueReusableCell(withReuseIdentifier: String(describing: CheckBoxCell.self), for: indexPath) as! CheckBoxCell
                cell.imageView.image = UIImage(named: dataString == "0" ? "calendar_empty" : (dataString == "1" ? "calendar_userEntered" : "calendar_active"))
                cell.gridlines.left = .none
                cell.gridlines.top = .none
                cell.gridlines.right = .none
                cell.gridlines.bottom = .none

                return cell
            }
        }
    }
    
    func spreadsheetView(_ spreadsheetView: SpreadsheetView, didSelectItemAt indexPath: IndexPath) {
        guard indexPath.column != 0 else {
            return
        }
        guard indexPath.row != 0 else {
            return
        }
        let calendarDetailVC = CalendarDetailViewController.instantiateFromAppStoryboard(appStoryboard: .Profile)
        calendarDetailVC.currentWeekDaySelection = indexPath.column - 1
        calendarDetailVC.schedules = self.schedules
        calendarDetailVC.userCalendarEvents = self.userEvents
        calendarDetailVC.showRow = indexPath.row - 1
        calendarDetailVC.delegate = self
        self.navigationController?.pushViewController(calendarDetailVC, animated: true)
        
    }

}

extension CalendarOverviewViewController: CalendarDetailViewControllerDelegate {
    
    func calendarDidUpdate(schedules: [String : [Schedule]], userCalendarEvents: [String : [UserEvent]]) {
        self.schedules = schedules
        self.userEvents = userCalendarEvents
        data = getDataArrayFromSchedules(self.schedules, events: self.userEvents)
        shouldReloadDataOnViewDidAppear = true
    }
}

extension CalendarOverviewViewController {
    
    func fetchUserSchedules() {
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }
        
        guard let userID = UserStore.shared.userID else {
            return
        }
        
        Utilities.showHUD(forView: self.view, excludeViews: [])
        
        let calendarObserver = ApiManager.shared.apiService.fetchUserSchedules(userID)
        let calendarDisposable = calendarObserver.subscribe(onNext: {(userCalendar) in
            Utilities.hideHUD(forView: self.view)
            self.schedules = userCalendar.calendar
            self.userEvents = userCalendar.userEvent
            DispatchQueue.main.async {
                self.refreshData()
            }
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.showSimpleAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        })
        calendarDisposable.disposed(by: disposableBag)
    }
}
