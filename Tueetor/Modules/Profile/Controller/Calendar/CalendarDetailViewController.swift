//
//  CalendarDetailViewController.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 07/08/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit
import EventKit
import RxSwift

struct TimeObject {
    var time = 0
    var allNames = [String]()
    var subjectsDetails = [SubjectDetailedInfo]()
    var userEvents = [UserEvent]()
}

protocol CalendarDetailViewControllerDelegate {
    func calendarDidUpdate(schedules: [String: [Schedule]], userCalendarEvents: [String: [UserEvent]])
}

class CalendarDetailViewController: BaseViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var weekSelectionHeader: CalendarHeaderTableViewCell!
    var currentWeekDaySelection = 0
    var currentHourSelection = 0
    var showRow = -1
    var schedules: [String: [Schedule]]!
    var userCalendarEvents: [String: [UserEvent]]!
    var sortedData: [[TimeObject]]!
    var eventDialogue: PopupDialog!
    var subjectListingDialogue: PopupDialog!

    var eventStore : EKEventStore!
    var canAccessCalendar = false
    var disposableBag = DisposeBag()
    var delegate: CalendarDetailViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        createdTableData()
        eventStore = (UIApplication.shared.delegate as! AppDelegate).eventStore
        canAccessCalendar = EKEventStore.authorizationStatus(for: .event) == .authorized
        if canAccessCalendar == false {
            eventStore.requestAccess(to: .event) { (success, error) in
                
                if error == nil && success {
                    self.canAccessCalendar = true
                } else {
                    let alert = UIAlertController(title: "Calendar Permission".localized, message: "Please grant calendar permission to fetch and add Tueetor events.".localized, preferredStyle: .alert)
                    let okAction = UIAlertAction(title: AlertButton.ok.description(), style: .default) { (action) in
                        self.navigateToSettingsPage()
                    }
                    let laterAction = UIAlertAction(title: AlertButton.later.description(), style: .cancel, handler: nil)
                    alert.addAction(okAction)
                    alert.addAction(laterAction)
                    self.present(alert, animated: true, completion: nil)
                    print("error = \(String(describing: error?.localizedDescription))")
                }
            }
        }

        self.tableView.reloadData()
        if showRow != -1 {
            self.tableView.scrollToRow(at: IndexPath.init(row: showRow, section: 0), at: .middle, animated: true)
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    func createdTableData() {
        sortedData = [[TimeObject]]()
        guard let weekSchedule = schedules else {
            return
        }
        guard let calendarEvents = userCalendarEvents else {
            return
        }
        for day in daysArray {
            var timeObjArray = [TimeObject]()
            let daySchedules = weekSchedule[day]!
            let dayEvents = calendarEvents[day]!
            for i in 0..<24 {
                var stringArray = [String]()
                var subjectsArray = [SubjectDetailedInfo]()
                var eventsArray = [UserEvent]()
                
                for schedule in daySchedules {
                    if schedule.hours == String(i) {
                        stringArray.append(schedule.subject.name)
                        subjectsArray.append(schedule.subject)
                    }
                }
                for individualEvent in dayEvents {
                    if individualEvent.hours == String(i) {
                        stringArray.append(individualEvent.comments)
                        eventsArray.append(individualEvent)
                    }
                }
                
                timeObjArray.append(TimeObject.init(time: i,
                                                    allNames: stringArray,
                                                    subjectsDetails: subjectsArray,
                                                    userEvents: eventsArray))
            }
            sortedData.append(timeObjArray)
        }
    }
    

    func showSubjectsPopup(subjects: [SubjectDetailedInfo]) {
        let subPopupVC = SubjectsPopupViewController(nibName: "SubjectsPopupViewController", bundle: nil)
        subPopupVC.subjects = subjects
        subPopupVC.delegate = self
        // Create the dialog
        subjectListingDialogue = PopupDialog(viewController: subPopupVC,
                                buttonAlignment: .horizontal,
                                transitionStyle: .bounceDown,
                                tapGestureDismissal: true,
                                panGestureDismissal: true)
        
        present(subjectListingDialogue, animated: true, completion: nil)

    }
    
    func showAddEventPopup(event: UserEvent?) {
        let eventPopupVC = EventsPopupViewController(nibName: "EventsPopupViewController", bundle: nil)
        if let _event = event {
            eventPopupVC.eventDescription = _event.comments
            eventPopupVC.userEvent = _event
        }
        eventPopupVC.canAccessCalendar = self.canAccessCalendar
        eventPopupVC.eventStore = self.eventStore
        eventPopupVC.delegate = self
        eventDialogue = PopupDialog(viewController: eventPopupVC,
                                buttonAlignment: .horizontal,
                                transitionStyle: .bounceDown,
                                tapGestureDismissal: true,
                                panGestureDismissal: true)
        
        present(eventDialogue, animated: true, completion: nil)
    }

    func showEventsPopup(events: [UserEvent]) {
        let eventListingPopupVC = EventListingPopupViewController(nibName: "EventListingPopupViewController", bundle: nil)
        eventListingPopupVC.userEvents = events
        let eventListingDialogue = PopupDialog(viewController: eventListingPopupVC,
                                    buttonAlignment: .horizontal,
                                    transitionStyle: .bounceDown,
                                    tapGestureDismissal: true,
                                    panGestureDismissal: true)
        
        present(eventListingDialogue, animated: true, completion: nil)
    }

    func saveEventToCalendar(desc: String, recurring: Int, alarms: [Int], userEvent: UserEvent?) {
        guard canAccessCalendar else {
            var message = ""
            if let _ = userEvent {
                message = "You won't be able to update the calendar. Proceed to update the event or change the settings".localized
            } else {
                message = "You won't be able to add event to calendar. Proceed to add the event or change the settings".localized
            }
            let alert = UIAlertController(title: "Calendar Permission".localized, message: message, preferredStyle: .alert)
            let proceedAction = UIAlertAction(title: AlertButton.proceed.description(), style: .default) { (action) in
                if let existingEvent = userEvent {
                    self.updateUserEvent(eventID: existingEvent.id, comment: desc, eventIDs: existingEvent.eventIds)
                } else {
                    self.addEventToCalendar(comment: desc, eventIDs: [String]())
                }
            }
            let settingsAction = UIAlertAction(title: AlertButton.settings.description(), style: .default) { (action) in
                self.navigateToSettingsPage()
            }
            alert.addAction(settingsAction)
            alert.addAction(proceedAction)
            self.present(alert, animated: true, completion: nil)

            return
        }
        do {
            let event = EKEvent.init(eventStore: eventStore)
            event.title = "Tueetor - \(desc)"
            let selectedDate = Utilities.getDateBasedOnWeekday(currentWeekDaySelection, andHours: currentHourSelection)
            event.startDate = selectedDate
            event.endDate = Date.init(timeInterval: 3600, since: selectedDate)
            print(event.eventIdentifier)
            for alarmOption in alarms {
                var timeDifference: Double = 0
                switch alarmOption {
                case 0:
                    timeDifference = -15*60
                case 1:
                    timeDifference = -30*60
                default:
                    timeDifference = -60*60
                }
                let alarm = EKAlarm.init(absoluteDate: Date.init(timeInterval: timeDifference, since: event.startDate))
                event.addAlarm(alarm)
            }
            
            if let recurrenceRule = Utilities.getRepeatValue(recurring) {
                event.recurrenceRules = [recurrenceRule]
            }
            
            event.calendar = eventStore.defaultCalendarForNewEvents
            
            try self.eventStore.save(event, span: .thisEvent, commit: true)
            if let existingEvent = userEvent {
                var arrayOfEventIds = existingEvent.eventIds
                arrayOfEventIds.append(event.eventIdentifier)
                self.updateUserEvent(eventID: existingEvent.id, comment: desc, eventIDs: arrayOfEventIds)
            } else {
                self.addEventToCalendar(comment: desc, eventIDs: [event.eventIdentifier])
            }
        } catch let error as NSError {
            print("failed to save event with error : \(error)")
        }
        print("Saved Event")
    }
    
    func editEventInCalendar(event: EKEvent, desc: String, recurring: Int, alarms: [Int], userEvent: UserEvent) {
        guard canAccessCalendar else {
            return
        }
        do {
            event.title = "Tueetor - \(desc)"
            
            print(event.eventIdentifier)
            if let alarms = event.alarms {
                for alarm in alarms {
                    event.removeAlarm(alarm)
                }
            }
            for alarmOption in alarms {
                var timeDifference: Double = 0
                switch alarmOption {
                case 0:
                    timeDifference = -15*60
                case 1:
                    timeDifference = -30*60
                default:
                    timeDifference = -60*60
                }
                let alarm = EKAlarm.init(absoluteDate: Date.init(timeInterval: timeDifference, since: event.startDate))
                event.addAlarm(alarm)
            }

            if let recurrenceRulesArray = event.recurrenceRules {
                for recurrenceRule in recurrenceRulesArray {
                    event.removeRecurrenceRule(recurrenceRule)
                }
            }
            
            
            if let recurrenceRule = Utilities.getRepeatValue(recurring) {
                event.recurrenceRules = [recurrenceRule]
            }
            
            try self.eventStore.save(event, span: .futureEvents, commit: true)
            print(event.eventIdentifier)
            print("Edited Event")
            self.updateUserEvent(eventID: userEvent.id, comment: desc, eventIDs: userEvent.eventIds)
        } catch let error as NSError {
            print("failed to save event with error : \(error)")
        }
    }
    
    func deleteEvent(_ event: EKEvent, userEvent: UserEvent) {
        guard canAccessCalendar else {
            return
        }
        do {
            try self.eventStore.remove(event, span: .futureEvents, commit: true)
            removeUserEvent(eventID: userEvent.id)
        } catch let error as NSError {
            print("failed to save event with error : \(error)")
        }

    }
    
}


extension CalendarDetailViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 24
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ScheduleDisplayTableViewCell.cellIdentifier(), for: indexPath) as! ScheduleDisplayTableViewCell
        let daySchedule = sortedData[currentWeekDaySelection]
        cell.configureCellWithTimeObject(daySchedule[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50.0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let sectionHeader = tableView.dequeueReusableCell(withIdentifier: CalendarHeaderTableViewCell.cellIdentifier()) as! CalendarHeaderTableViewCell
        weekSelectionHeader = sectionHeader
        sectionHeader.selectedIndex = currentWeekDaySelection
        sectionHeader.delegate = self
        return sectionHeader
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CalendarHeaderTableViewCell.sectionHeight()
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.5
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footer = UIView()
        footer.backgroundColor = UIColor.white
        return footer
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let actionSheet = UIAlertController(title: AppName, message: indexPath.row > 9 ? "\(indexPath.row):00" : "0\(indexPath.row):00", preferredStyle: .actionSheet)
        let daySchedule = sortedData[currentWeekDaySelection]
        let timeObj = daySchedule[indexPath.row]
        currentHourSelection = indexPath.row
        if !timeObj.subjectsDetails.isEmpty {
            actionSheet.addAction(UIAlertAction(title: "View Subjects".localized, style: .default, handler: { _ in
                self.showSubjectsPopup(subjects: timeObj.subjectsDetails)
            }))
        }
        
        if !timeObj.userEvents.isEmpty {
            
            for event in timeObj.userEvents {
                actionSheet.addAction(UIAlertAction(title: "Event - \(event.comments ?? "")", style: .default, handler: { _ in
                    self.showAddEventPopup(event: event)
                }))
            }
        }
        actionSheet.addAction(UIAlertAction(title: "Add Event", style: .default, handler: { _ in
            self.showAddEventPopup(event: nil)
        }))
        
        actionSheet.addAction(UIAlertAction.init(title: "Cancel".localized, style: .cancel, handler: nil))
        
        self.present(actionSheet, animated: true, completion: nil)
    }
}

extension CalendarDetailViewController: EventsPopupViewControllerDelegate {

    func cancelButtonTapped() {
        guard let dialogue = eventDialogue else {
            return
        }
        dialogue.dismiss()
    }
    
    func okButtonTapped(desc: String, recurring: Int, alarm: [Int]) {
        guard let dialogue = eventDialogue else {
            return
        }
        dialogue.dismiss()
        self.saveEventToCalendar(desc: desc, recurring: recurring, alarms: alarm, userEvent: nil)
    }
    
    func editButtonTapped(event: EKEvent?, desc: String, recurring: Int, alarm: [Int], userEvent: UserEvent) {
        guard let dialogue = eventDialogue else {
            return
        }
        dialogue.dismiss()
        if let calendarEvent = event {
            editEventInCalendar(event: calendarEvent, desc: desc, recurring: recurring, alarms: alarm, userEvent: userEvent)
        } else {
            saveEventToCalendar(desc: desc, recurring: recurring, alarms: alarm, userEvent: userEvent)
        }
    }
    
    func deleteButtonTapped(calendarEvent: EKEvent?, userEvent: UserEvent) {
        guard let dialogue = eventDialogue else {
            return
        }
        dialogue.dismiss()
        if let calEvent = calendarEvent {
            deleteEvent(calEvent, userEvent: userEvent)
        } else {
            removeUserEvent(eventID: userEvent.id)
        }
    }
}

extension CalendarDetailViewController: CalendarHeaderTableViewCellDelegate {
    
    func indexSelectedForCalendarHeader(_ index: Int) {
        currentWeekDaySelection = index
        self.tableView.reloadData()
    }
    
}

extension CalendarDetailViewController: SubjectsPopupViewControllerDelegate {
    
    func showSubjectDetailsForSubject(_ subject: SubjectDetailedInfo) {
        guard let dialogue = subjectListingDialogue else {
            return
        }
        dialogue.dismiss()
        let subjectDetailPage = MySubjectDetailViewController.instantiateFromAppStoryboard(appStoryboard: .Profile)
        subjectDetailPage.subjectID = subject.subjectID
        self.navigationController?.pushViewController(subjectDetailPage, animated: true)
    }
}

extension CalendarDetailViewController {
    
    func addEventToCalendar(comment: String, eventIDs: [String]) {
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }
        
        guard let userID = UserStore.shared.userID else {
            return
        }
        var currentDay = "MON"
        switch currentWeekDaySelection {
        case 0:
            currentDay = "MON"
        case 1:
            currentDay = "TUE"
        case 2:
            currentDay = "WED"
        case 3:
            currentDay = "THU"
        case 4:
            currentDay = "FRI"
        case 5:
            currentDay = "SAT"
        case 6:
            currentDay = "SUN"
        default:
            break
        }
        Utilities.showHUD(forView: self.view, excludeViews: [])
        
        let calendarObserver = ApiManager.shared.apiService.addEventWithComment(comment, dayName: currentDay, hours: String(currentHourSelection), eventIDs: eventIDs, userID: userID)
        let calendarDisposable = calendarObserver.subscribe(onNext: {(message) in

            DispatchQueue.main.async {
                self.fetchUserSchedules()
            }
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.showSimpleAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        })
        calendarDisposable.disposed(by: disposableBag)
    }
    
    func updateUserEvent(eventID: String, comment: String, eventIDs: [String]) {
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }
        
        Utilities.showHUD(forView: self.view, excludeViews: [])
        
        let calendarObserver = ApiManager.shared.apiService.editEventWithID(eventID, comment: comment, eventIDs: eventIDs)
        let calendarDisposable = calendarObserver.subscribe(onNext: {(message) in
            
            DispatchQueue.main.async {
                self.fetchUserSchedules()
            }
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.showSimpleAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        })
        calendarDisposable.disposed(by: disposableBag)
    }
    
    func removeUserEvent(eventID: String) {
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }
        
        Utilities.showHUD(forView: self.view, excludeViews: [])
        
        let calendarObserver = ApiManager.shared.apiService.deleteEventWithID(eventID)
        let calendarDisposable = calendarObserver.subscribe(onNext: {(message) in
            
            DispatchQueue.main.async {
                self.fetchUserSchedules()
            }
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.showSimpleAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        })
        calendarDisposable.disposed(by: disposableBag)
    }

    func fetchUserSchedules() {
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }
        
        guard let userID = UserStore.shared.userID else {
            return
        }
        
        
        let calendarObserver = ApiManager.shared.apiService.fetchUserSchedules(userID)
        let calendarDisposable = calendarObserver.subscribe(onNext: {(userCalendar) in
            Utilities.hideHUD(forView: self.view)
            self.schedules = userCalendar.calendar
            self.userCalendarEvents = userCalendar.userEvent
            self.delegate?.calendarDidUpdate(schedules: userCalendar.calendar, userCalendarEvents: userCalendar.userEvent)
            DispatchQueue.main.async {
                self.createdTableData()
                self.tableView.reloadData()
            }
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.showSimpleAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        })
        calendarDisposable.disposed(by: disposableBag)
    }

}




