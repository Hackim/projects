//
//  ProfileViewController.swift
//  Tueetor
//
//  Created by Phaninder on 08/04/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit
import BiometricAuthentication
import RxSwift

class ProfileViewController: BaseViewController {

    @IBOutlet weak var touchIDButton: UIButton!
    @IBOutlet weak var logoutButton: UIButton!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    var titles = ["My Subjects".localized, "My Documents".localized, "My Calendar".localized, "My Shortlist".localized, "My Reviews".localized, "My Subscription".localized, "Privacy".localized, "Logout".localized]
    var images = ["my_subjects", "assignment", "my_calender", "my_shortlist", "star_blue", "budget_session", "my_settings", "my_logout"]
    var disposableBag = DisposeBag()
    var isFirstTime = true
    var profileImage: UIImage!
    private let refreshControl = UIRefreshControl()

    override func viewDidLoad() {
        super.viewDidLoad()

        loginButton.layer.shadowOffset = CGSize.init(width: 0, height: 5)
        loginButton.layer.shadowRadius = 3
        loginButton.layer.shadowColor = themeBlueColor.cgColor
        loginButton.layer.shadowOpacity = 0.75

        if #available(iOS 10.0, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.addSubview(refreshControl)
        }
        refreshControl.addTarget(self, action: #selector(refreshData(_:)), for: .valueChanged)

        if UserStore.shared.isLoggedIn == true {
            DispatchQueue.main.async {
                self.fetchUserDetails()
            }
        } else {
            tableView.isHidden = true
        }
        tableView.tableFooterView = UIView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        profileImage = nil
        tableView.reloadData()
        (self.tabBarController as! BaseTabBarViewController).tabBar.isHidden = false
        (self.tabBarController as! BaseTabBarViewController).showCenterButton()
    }
    
    @objc func refreshData(_ sender: Any) {
        if UserStore.shared.isLoggedIn == true {
            DispatchQueue.main.async {
                self.fetchUserDetails()
            }
        }
        if let refreshControl = sender as? UIRefreshControl {
            refreshControl.endRefreshing()
        }
    }

    @IBAction func loginButtonTapped(_ sender: UIButton) {
        logout()
    }
    
    func logout() {
        guard let window = UIApplication.shared.keyWindow else {
            return
        }
        
        guard let rootViewController = window.rootViewController else {
            return
        }
        
        let landingViewController = LandingViewController.instantiateFromAppStoryboard(appStoryboard: .Login)
        landingViewController.view.frame = rootViewController.view.frame
        landingViewController.view.layoutIfNeeded()
        let navController = BaseNavigationController(rootViewController: landingViewController)
        UIView.transition(with: window, duration: 0.3, options: .transitionCrossDissolve, animations: {
            window.rootViewController = navController
        }, completion: { completed in
            Utilities.logoutUser()
            // maybe do something here
        })

    }
    
    @IBAction func editProfileButtonTapped(_ sender: UIButton) {
        let userViewController = EditProfileViewController.instantiateFromAppStoryboard(appStoryboard: .Profile)
       userViewController.user = (UIApplication.shared.delegate as! AppDelegate).user
        navigationController?.pushViewController(userViewController, animated: true)
    }
    
    @IBAction func touchIDButtonTapped(_ sender: UIButton) {
        BioMetricAuthenticator.authenticateWithBioMetrics(reason: "", success: {
            guard let username = UserStore.shared.userEmail, !username.isEmpty else {
                return
            }
            let isEnabled = UserStore.shared.isBioMetricEnabledByUser
            UserStore.shared.isBioMetricEnabledByUser = !isEnabled
            UserStore.shared.touchIDEmail = (UserStore.shared.isBioMetricEnabledByUser == true) ? UserStore.shared.userEmail : ""
        }, failure: { [weak self] (error) in
            print("Fail")
            
            // do nothing on canceled
            if error == .canceledByUser || error == .canceledBySystem {
                return
            } else if error == .biometryNotAvailable {
                // device does not support biometric (face id or touch id) authentication
                DispatchQueue.main.async(execute: {
                    let alert = UIAlertController(title: AppName, message: error.message(), preferredStyle: .alert)
                    let okAction =  UIAlertAction(title: AlertButton.ok.description(), style: .default, handler: nil)
                    alert.addAction(okAction)
                    self?.present(alert, animated: true, completion: nil)
                })
            } else if error == .fallback {
                // show alternatives on fallback button clicked
                // here we're entering username and password
                
            } else if error == .biometryNotEnrolled {
                // No biometry enrolled in this device, ask user to register fingerprint or face
                //Settings page
            } else if error == .biometryLockedout {
                // Biometry is locked out now, because there were too many failed attempts.
                // Need to enter device passcode to unlock.
                
                // show passcode authentication
            } else {
                // show error on authentication failed
                DispatchQueue.main.async(execute: {
                    let alert = UIAlertController(title: AppName, message: error.message(), preferredStyle: .alert)
                    let okAction =  UIAlertAction(title: AlertButton.ok.description(), style: .default, handler: nil)
                    alert.addAction(okAction)
                    self?.present(alert, animated: true, completion: nil)
                })
            }
        })
    }
    
    //MARK: Helper Methods -
    func configureTouchIDButton() {
        if BioMetricAuthenticator.canAuthenticate() {
            if BioMetricAuthenticator.shared.faceIDAvailable() {
                let title =  UserStore.shared.isBioMetricEnabledByUser ? "Disable FaceID" : "Enable FaceID"
                touchIDButton.setTitle(title, for: .normal)
            } else {
                let title =  UserStore.shared.isBioMetricEnabledByUser ? "Disable TouchID" : "Enable TouchID"
                touchIDButton.setTitle(title, for: .normal)
            }
        } else {
            touchIDButton.isHidden = true
        }
    }
}

extension ProfileViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if UserStore.shared.isLoggedIn == true, let _ = (UIApplication.shared.delegate as! AppDelegate).user  {
            return 1
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return titles.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard indexPath.row != 0 else {
            let cell = tableView.dequeueReusableCell(withIdentifier: ProfileHeaderTableViewCell.cellIdentifier(), for: indexPath) as! ProfileHeaderTableViewCell
            let user = (UIApplication.shared.delegate as! AppDelegate).user!
            if let userImage = profileImage {
                cell.profileImageView.image = userImage
                cell.addImage.image = UIImage(named: "editButton")
            } else {
                cell.profileImageView.sd_setShowActivityIndicatorView(true)
                cell.profileImageView.sd_setIndicatorStyle(.gray)
                cell.addImage.image = UIImage(named: "addIcon")
                cell.profileImageView.sd_setImage(with: URL(string: user.imageURL), placeholderImage: UIImage(named: "profile_placeholder"), options: .retryFailed) { (image, error, cacheType, url) in
                    if error == nil {
                        self.profileImage = image
                        cell.addImage.image = UIImage(named: "editButton")
                    }
                }
            }

            cell.configureCellWithUser((UIApplication.shared.delegate as! AppDelegate).user!)
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: ProfileOptionTableViewCell.cellIdentifier(), for: indexPath) as! ProfileOptionTableViewCell
        cell.configureCellWithTitle(titles[indexPath.row - 1], andImage: images[indexPath.row - 1])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        guard indexPath.row != 0 else {
            tableView.estimatedRowHeight = 302
            return UITableViewAutomaticDimension
        }
        if (indexPath.row == 2 || indexPath.row == 5 || indexPath.row == 6) , Utilities.isUserTypeStudent() {
            return 0
        }
        return ProfileOptionTableViewCell.cellHeight()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            break
        case 1:
            let subjectsViewController = MySubjectListingViewController.instantiateFromAppStoryboard(appStoryboard: .Profile)
            navigationController?.pushViewController(subjectsViewController, animated: true)
            break
        case 2:
            let documentsViewController = MyDocumentsViewController.instantiateFromAppStoryboard(appStoryboard: .Profile)
            navigationController?.pushViewController(documentsViewController, animated: true)
            break
        case 3:
            let calendarViewController = CalendarOverviewViewController.instantiateFromAppStoryboard(appStoryboard: .Profile)
            navigationController?.pushViewController(calendarViewController, animated: true)
            break
        case 4:
            let shortlistViewController = ShortlistedUserListingViewController.instantiateFromAppStoryboard(appStoryboard: .Profile)
            navigationController?.pushViewController(shortlistViewController, animated: true)
            break
        case 5:
            let myReviewsViewController = MyReviewsViewController.instantiateFromAppStoryboard(appStoryboard: .Profile)
            navigationController?.pushViewController(myReviewsViewController, animated: true)
        case 6:
            let mySubscriptionViewController = MySubscriptionViewController.instantiateFromAppStoryboard(appStoryboard: .Profile)
            navigationController?.pushViewController(mySubscriptionViewController, animated: true)
        case 7:
            let settingsViewController = ProfileSettingsViewController.instantiateFromAppStoryboard(appStoryboard: .Profile)
            navigationController?.pushViewController(settingsViewController, animated: true)
        case 8:
            logout()
        default:
            break
        }
    }
}

extension ProfileViewController {
    
    func fetchUserDetails() {
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }
        Utilities.showHUD(forView: self.view, excludeViews: [])

        let userObserver = ApiManager.shared.apiService.fetchUserDetails()
        let userDisposable = userObserver.subscribe(onNext: {(user) in
            DispatchQueue.main.async {
                self.tableView.isHidden = false
                Utilities.hideHUD(forView: self.view)

//                if self.isFirstTime == false {
//                    Utilities.hideHUD(forView: self.view)
//                } else {
//                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
//                }
                
//                self.isFirstTime = false
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.user = user
                self.tableView.reloadData()
            }
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.showSimpleAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        })
        userDisposable.disposed(by: disposableBag)
    }

}
