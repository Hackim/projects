//
//  EditProfileViewController.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 28/05/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit
import SDWebImage
import RxSwift
import GooglePlaces
import Alamofire
import Unbox
import IQKeyboardManagerSwift

class EditProfileViewController: BaseViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var user: User!
    var profileImage: UIImage!
    var disposableBag = DisposeBag()
    var countries = [Country]()
    var editedUser: User!
    var activeTextField: UITextField!
    var isFirstTimeRendering = true
    var aboutCellHeight:CGFloat = 150.0
    var errorMessagesDict = [Int: String]()
    //Temp. remove this later
    var name = ""
    var labelFields = [["EMAIL".localized], ["TITLE".localized, "GENDER".localized], ["FIRST NAME".localized], ["LAST NAME".localized], ["DISPLAY NAME".localized], [""], ["BIRTHDAY".localized], ["ADDRESS LINE 1".localized], ["ADDRESS LINE 2".localized], ["CITY".localized, "ZIP CODE".localized], ["COUNTRY".localized], ["CONTACT NUMBER".localized], ["ABOUT".localized]]
    
    //Pickers
    let titlePickerView = UIPickerView()
    let genderPickerView = UIPickerView()
    let datePicker = UIDatePicker()
    let countryPickerView = UIPickerView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView()
        editedUser = User.init(user)
        
        titlePickerView.delegate = self
        titlePickerView.dataSource = self
        titlePickerView.tag = 1
        
        genderPickerView.delegate = self
        genderPickerView.dataSource = self
        genderPickerView.tag = 2
        
        datePicker.datePickerMode = .date
        datePicker.maximumDate = Date()
        let inputDateFormatter = DateFormatter()
        inputDateFormatter.dateFormat = "dd-MMM-yyyy"
        if let date = inputDateFormatter.date(from: editedUser.dob!) {
            datePicker.date = date
        }
        datePicker.addTarget(self, action: #selector(datePickerValueChanged(sender:)), for: .valueChanged)

        countryPickerView.delegate = self
        countryPickerView.dataSource = self
        countryPickerView.tag = 3
        if let countriesArray = Utilities.getCountries() {
            countries = countriesArray
        } else {
            fetchCountries()
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        (self.tabBarController as! BaseTabBarViewController).tabBar.isHidden = true
        (self.tabBarController as! BaseTabBarViewController).hideCenterButton()
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func saveButtonTapped(_ sender: UIButton) {
        self.view.endEditing(true)
        let validationInformation = isDataValid()
        guard validationInformation.isValid else {
            self.showSimpleAlertWithMessage(validationInformation.message)
            return
        }
        guard let _ = profileImage else {
            self.showSimpleAlertWithMessage("Please select profile picture to continue.".localized)
            return
        }
        editAccountWithImage(profileImage)
    }
    
    @objc func datePickerValueChanged(sender:UIDatePicker) {
        activeTextField.text = Utilities.getFormattedDate(sender.date, dateFormat: "dd-MMM-yyyy")
    }

    
    func configureTextField(_ customTextField: TueetorTextField) {
        customTextField.delegate = self
        customTextField.keyboardType = .asciiCapable
        customTextField.isUserInteractionEnabled = true
        customTextField.autocapitalizationType = .none
        customTextField.inputView = nil
        let imageName = "drop-down-arrow"
        let originalImage = UIImage(named: imageName)
        let templateImage = originalImage!.withRenderingMode(UIImageRenderingMode.alwaysTemplate)

        switch customTextField.tag {
        case EditProfileScreenRowType.email.rawValue:
            customTextField.isUserInteractionEnabled = false
            customTextField.keyboardType = .emailAddress
            
        case EditProfileScreenRowType.title.rawValue:
            if customTextField.title == "TITLE".localized {
                customTextField.inputView = titlePickerView
            } else {
                customTextField.inputView = genderPickerView
            }
            customTextField.iconImage.image = templateImage
            customTextField.iconImage.tintColor = themeBlackColor
            customTextField.showImage()
            
        case EditProfileScreenRowType.birthday.rawValue:
            customTextField.inputView = datePicker
            customTextField.iconImage.image = templateImage
            customTextField.iconImage.tintColor = themeBlackColor
            customTextField.showImage()

        case EditProfileScreenRowType.address1.rawValue:
            customTextField.isUserInteractionEnabled = false

        case EditProfileScreenRowType.city.rawValue:
            customTextField.icon = "tick"
            if customTextField.title == "CITY".localized {
                customTextField.keyboardType = .asciiCapable
            } else {
                customTextField.keyboardType = .numberPad
            }

        case EditProfileScreenRowType.country.rawValue:
            customTextField.inputView = countryPickerView
            customTextField.icon = "drop-down-arrow"
            customTextField.showImage()

        case EditProfileScreenRowType.contact.rawValue:
            customTextField.keyboardType = .phonePad
            customTextField.isUserInteractionEnabled = false

        default:
            customTextField.keyboardType = .asciiCapable
            customTextField.autocapitalizationType = .words
        }
        
    }
    
    func updateDataIntoTextField(_ customTextField: TueetorTextField, fromCell: Bool) {
        switch customTextField.tag {
        case EditProfileScreenRowType.email.rawValue:
            fromCell ? (customTextField.text = editedUser.email) : (editedUser.email = customTextField.text!)
            
        case EditProfileScreenRowType.title.rawValue:
            if customTextField.title == "TITLE".localized {
                fromCell ? (customTextField.text = editedUser.title) : (editedUser.title = customTextField.text!)
            } else {
                fromCell ? (customTextField.text = editedUser.gender) : (editedUser.gender = customTextField.text!)
            }
            
        case EditProfileScreenRowType.firstName.rawValue:
            fromCell ? (customTextField.text = editedUser.firstName) : (editedUser.firstName = customTextField.text!)

        case EditProfileScreenRowType.lastName.rawValue:
            fromCell ? (customTextField.text = editedUser.lastName) : (editedUser.lastName = customTextField.text!)

        case EditProfileScreenRowType.display.rawValue:
            fromCell ? (customTextField.text = editedUser.displayName) : (editedUser.displayName = customTextField.text!)
            
            if let displayName = customTextField.text, !displayName.isEmpty, fromCell == false {
                getProfileLinkURLForName(displayName)
            }

            
        case EditProfileScreenRowType.birthday.rawValue:
            fromCell ? (customTextField.text = editedUser.dob) : (editedUser.dob = customTextField.text!)

        case EditProfileScreenRowType.address1.rawValue:
            fromCell ? (customTextField.text = editedUser.address1) : (editedUser.address1 = customTextField.text!)

        case EditProfileScreenRowType.address2.rawValue:
            fromCell ? (customTextField.text = editedUser.address2) : (editedUser.address2 = customTextField.text!)
            
        case EditProfileScreenRowType.city.rawValue:
            if customTextField.title == "CITY".localized {
                fromCell ? (customTextField.text = editedUser.city) : (editedUser.city = customTextField.text!)
            } else {
                fromCell ? (customTextField.text = editedUser.zipCode) : (editedUser.zipCode = customTextField.text!)
            }
            
        case EditProfileScreenRowType.country.rawValue:
            fromCell ? (customTextField.text = editedUser.country) : (editedUser.country = customTextField.text!)

        case EditProfileScreenRowType.contact.rawValue:
            fromCell ? (customTextField.text = editedUser.phone) : (editedUser.phone = customTextField.text!)
            
        default:
            break
        }
    }

    func updateDataIntoTextView(_ customTextView: UITextView, fromCell: Bool) {
        
        fromCell ? (customTextView.text = editedUser.about) : (editedUser.about = customTextView.text!)
    }
    
    func isDataValid() -> (isValid: Bool, message: String) {
        var errorMessages: [String] = []
        for index in 1...labelFields.count {
            let currentIndexPath = IndexPath.init(row: index, section: 0)
            switch index {
            case EditProfileScreenRowType.email.rawValue:
                let cell = tableView.cellForRow(at: currentIndexPath) as? TueetorTextFieldTableViewCell
                cell?.validateEmail(text: editedUser.email)
                errorMessages.append(cell?.errorLabel.text ?? "")
                
            case EditProfileScreenRowType.title.rawValue:
                let dualCell = tableView.cellForRow(at: currentIndexPath) as? TueetorDualTextFieldTableViewCell
                dualCell?.validateTitle(text: editedUser.title ?? "")
                dualCell?.validateGender(text: editedUser.gender ?? "")
                errorMessages.append(dualCell?.errorMessageLabelOne.text ?? "")
                errorMessages.append(dualCell?.errorMessageLabelTwo.text ?? "")
                
            case EditProfileScreenRowType.firstName.rawValue:
                let cell = tableView.cellForRow(at: currentIndexPath) as? TueetorTextFieldTableViewCell
                cell?.validateFirstName(text: editedUser.firstName)
                cell?.customTextField.hideImage()
                errorMessages.append(cell?.errorLabel.text ?? "")
                
            case EditProfileScreenRowType.lastName.rawValue:
                let cell = tableView.cellForRow(at: currentIndexPath) as? TueetorTextFieldTableViewCell
                cell?.validateLastName(text: editedUser.lastName)
                cell?.customTextField.hideImage()
                errorMessages.append(cell?.errorLabel.text ?? "")
                
            case EditProfileScreenRowType.display.rawValue:
                let cell = tableView.cellForRow(at: currentIndexPath) as? TueetorTextFieldTableViewCell
                cell?.validateDisplayName(text: editedUser.displayName)
                cell?.customTextField.hideImage()
                errorMessages.append(cell?.errorLabel.text ?? "")
                
            case EditProfileScreenRowType.birthday.rawValue:
                let cell = tableView.cellForRow(at: currentIndexPath) as? TueetorTextFieldTableViewCell
                cell?.validateBirthday(text: editedUser.dob ?? "")
                errorMessages.append(cell?.errorLabel.text ?? "")
                
            case EditProfileScreenRowType.city.rawValue:
                let dualCell = tableView.cellForRow(at: currentIndexPath) as? TueetorDualTextFieldTableViewCell
                dualCell?.validateCity(text: editedUser.city ?? "")
                dualCell?.validateZip(text: editedUser.zipCode ?? "")
                errorMessages.append(dualCell?.errorMessageLabelOne.text ?? "")
                errorMessages.append(dualCell?.errorMessageLabelTwo.text ?? "")
                
            case EditProfileScreenRowType.address1.rawValue:
                let cell = tableView.cellForRow(at: currentIndexPath) as? AddressPlacePickerTableViewCell
                cell?.validateAddress(text: editedUser.address1 ?? "")
                errorMessages.append(cell?.errorLabel.text ?? "")
                
            case EditProfileScreenRowType.country.rawValue:
                let cell = tableView.cellForRow(at: currentIndexPath) as? TueetorTextFieldTableViewCell
                cell?.validateCountry(text: editedUser.country)
                errorMessages.append(cell?.errorLabel.text ?? "")
                
            case EditProfileScreenRowType.contact.rawValue:
                let cell = tableView.cellForRow(at: currentIndexPath) as? TueetorTextFieldTableViewCell
                cell?.validatePhone(text: editedUser.phone ?? "")
                errorMessages.append(cell?.errorLabel.text ?? "")

            default:
                break
            }
        }
        let filteredErrorMessages = errorMessages.filter { (message) -> Bool in
            message.count > 0
        }
        tableView.beginUpdates()
        tableView.endUpdates()
        let isValidData = filteredErrorMessages.count == 0
        return (isValid: isValidData, message: isValidData ? "" : filteredErrorMessages[0])
    }

}

extension EditProfileViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 14
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case EditProfileScreenRowType.email.rawValue,
             EditProfileScreenRowType.firstName.rawValue,
             EditProfileScreenRowType.lastName.rawValue,
             EditProfileScreenRowType.display.rawValue,
             EditProfileScreenRowType.birthday.rawValue,
             EditProfileScreenRowType.address2.rawValue,
             EditProfileScreenRowType.country.rawValue:
            let cell = tableView.dequeueReusableCell(withIdentifier: TueetorTextFieldTableViewCell.cellIdentifier(), for: indexPath) as! TueetorTextFieldTableViewCell
            cell.configureCellForRowAtIndex(indexPath.row, withText: labelFields[indexPath.row - 1][0])
            updateDataIntoTextField(cell.customTextField, fromCell: true)
            configureTextField(cell.customTextField)
            return cell

        case EditProfileScreenRowType.title.rawValue,
            EditProfileScreenRowType.city.rawValue:
            let cell = tableView.dequeueReusableCell(withIdentifier: TueetorDualTextFieldTableViewCell.cellIdentifier(), for: indexPath) as! TueetorDualTextFieldTableViewCell
            cell.configureCellWithTitles(title: labelFields[indexPath.row - 1], index: indexPath.row)
            configureTextField(cell.textFieldOne)
            configureTextField(cell.textFieldTwo)
            updateDataIntoTextField(cell.textFieldOne, fromCell: true)
            updateDataIntoTextField(cell.textFieldTwo, fromCell: true)

            if indexPath.row == EditProfileScreenRowType.title.rawValue {
                cell.textFieldOne.showImage()
                cell.textFieldTwo.showImage()
            }
            return cell
            
        case EditProfileScreenRowType.profileURL.rawValue:
            let cell = tableView.dequeueReusableCell(withIdentifier: ProfileDisplayLinkTableViewCell.cellIdentifier(), for: indexPath) as! ProfileDisplayLinkTableViewCell
            cell.delegate = self
            cell.configureLinkLabelWithLink(editedUser.profileUrl, color: themeBlackColor)
            return cell

        case EditProfileScreenRowType.address1.rawValue:
            let cell = tableView.dequeueReusableCell(withIdentifier: AddressPlacePickerTableViewCell.cellIdentifier(), for: indexPath) as! AddressPlacePickerTableViewCell
            cell.configureCellForRowAtIndex(indexPath.row, withText: labelFields[indexPath.row - 1][0])
            updateDataIntoTextField(cell.placeTextField, fromCell: true)
            cell.delegate = self
            configureTextField(cell.placeTextField)
            return cell
            
        case EditProfileScreenRowType.contact.rawValue:
            let cell = tableView.dequeueReusableCell(withIdentifier: EditProfileContactNumberTableViewCell.cellIdentifier(), for: indexPath) as! EditProfileContactNumberTableViewCell
            cell.configureCellForRowAtIndex(indexPath.row, withText: labelFields[indexPath.row - 1][0])
            updateDataIntoTextField(cell.placeTextField, fromCell: true)
            cell.delegate = self
            configureTextField(cell.placeTextField)
            return cell
        case EditProfileScreenRowType.about.rawValue:
            let cell = tableView.dequeueReusableCell(withIdentifier: AboutTableViewCell.cellIdentifier(), for: indexPath) as! AboutTableViewCell
            cell.configureCellForRowAtIndex(indexPath.row, withText: labelFields[indexPath.row - 1][0])
            cell.textView.delegate = self
            cell.dummyTextField.text = editedUser.about ?? ""
            updateDataIntoTextField(cell.dummyTextField, fromCell: true)
            updateDataIntoTextView(cell.textView, fromCell: true)
            configureTextField(cell.dummyTextField)
            aboutCellHeight = cell.textView.contentSize.height + 24.0
            return cell
        default:
            //Profile image
            let cell = tableView.dequeueReusableCell(withIdentifier: EditProfileImageViewTableViewCell.cellIdentifier(), for: indexPath) as! EditProfileImageViewTableViewCell
            cell.delegate = self
            if let userImage = profileImage {
                cell.profileImageView.image = userImage
                cell.addImage.image = UIImage(named: "editButton")
            } else {
                cell.profileImageView.sd_setShowActivityIndicatorView(true)
                cell.profileImageView.sd_setIndicatorStyle(.gray)
                cell.addImage.image = UIImage(named: "addIcon")
                cell.profileImageView.sd_setImage(with: URL(string: user.imageURL), placeholderImage: UIImage(named: "profile_placeholder"), options: .retryFailed) { (image, error, cacheType, url) in
                    if error == nil {
                        self.profileImage = image
                        cell.addImage.image = UIImage(named: "editButton")
                    }
                }
            }
            
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case EditProfileScreenRowType.profileImage.rawValue:
            return EditProfileImageViewTableViewCell.cellHeight()
        case EditProfileScreenRowType.about.rawValue:
            return aboutCellHeight
        case EditProfileScreenRowType.profileURL.rawValue:
            tableView.estimatedRowHeight = 84.0
            return UITableViewAutomaticDimension
        default:
            tableView.estimatedRowHeight = 63.0
            return UITableViewAutomaticDimension
        }
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case EditProfileScreenRowType.profileImage.rawValue:
            return EditProfileImageViewTableViewCell.cellHeight()
        case EditProfileScreenRowType.about.rawValue:
            return aboutCellHeight
        case EditProfileScreenRowType.profileURL.rawValue:
            return 84.0
        default:
            return 63.0
        }
    }
}

extension EditProfileViewController: EditProfileImageViewTableViewCellDelegate {
    
    func showImageOptions() {
        let alert = UIAlertController(title: "Choose Image".localized, message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera".localized, style: .default, handler: { _ in
            self.camera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery".localized, style: .default, handler: { _ in
            self.photoLibrary()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel".localized, style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
}

extension EditProfileViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func camera() {
        if UIImagePickerController.isSourceTypeAvailable(.camera){
            let myPickerController = UIImagePickerController()
            myPickerController.delegate = self;
            myPickerController.sourceType = .camera
            myPickerController.allowsEditing = true
            self.present(myPickerController, animated: true, completion: nil)
        }
        
    }
    
    func photoLibrary() {
        
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
            let myPickerController = UIImagePickerController()
            myPickerController.delegate = self;
            myPickerController.sourceType = .photoLibrary
            myPickerController.allowsEditing = true
            self.present(myPickerController, animated: true, completion: nil)
        }
    }
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        let image = info[UIImagePickerControllerEditedImage] as! UIImage
        self.profileImage = image
        tableView.reloadData()
        dismiss(animated:true, completion: nil)
    }
    
}

extension EditProfileViewController: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        activeTextField = textField as! TueetorTextField
        switch activeTextField.tag {
        case EditProfileScreenRowType.address1.rawValue:
            view.endEditing(true)
        case EditProfileScreenRowType.country.rawValue,
            EditProfileScreenRowType.title.rawValue:
            managePickerView()
        default:
            break
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        updateDataIntoTextField(textField as! TueetorTextField, fromCell: false)
        let currentIndexPath = IndexPath.init(row: textField.tag, section: 0)
        switch textField.tag {
        case EditProfileScreenRowType.email.rawValue:
            let cell = tableView.cellForRow(at: currentIndexPath) as! TueetorTextFieldTableViewCell
            cell.validateEmail(text: textField.text ?? "")

        case EditProfileScreenRowType.title.rawValue:
            let dualCell = tableView.cellForRow(at: currentIndexPath) as! TueetorDualTextFieldTableViewCell
            let customField = textField as! TueetorTextField
            if customField.title == "TITLE".localized {
                dualCell.validateTitle(text: customField.text ?? "")
            } else {
                dualCell.validateGender(text: customField.text ?? "")
            }

        case EditProfileScreenRowType.firstName.rawValue:
            let cell = tableView.cellForRow(at: currentIndexPath) as! TueetorTextFieldTableViewCell
            cell.validateFirstName(text: textField.text ?? "")
            cell.customTextField.hideImage()
            
        case EditProfileScreenRowType.lastName.rawValue:
            let cell = tableView.cellForRow(at: currentIndexPath) as! TueetorTextFieldTableViewCell
            cell.validateLastName(text: textField.text ?? "")
            cell.customTextField.hideImage()
            
        case EditProfileScreenRowType.display.rawValue:
            let cell = tableView.cellForRow(at: currentIndexPath) as! TueetorTextFieldTableViewCell
            cell.validateDisplayName(text: textField.text ?? "")
            cell.customTextField.hideImage()
            
        case EditProfileScreenRowType.birthday.rawValue:
            let cell = tableView.cellForRow(at: currentIndexPath) as! TueetorTextFieldTableViewCell
            cell.validateBirthday(text: textField.text ?? "")

        case EditProfileScreenRowType.city.rawValue:
            let dualCell = tableView.cellForRow(at: currentIndexPath) as! TueetorDualTextFieldTableViewCell
            let customField = textField as! TueetorTextField
            if customField.title == "CITY".localized {
                dualCell.validateCity(text: customField.text ?? "")
            } else {
                dualCell.validateZip(text: customField.text ?? "")
            }
            
        case EditProfileScreenRowType.address1.rawValue:
            let cell = tableView.cellForRow(at: currentIndexPath) as! AddressPlacePickerTableViewCell
            cell.validateAddress(text: textField.text ?? "")
            
        case EditProfileScreenRowType.country.rawValue:
            let cell = tableView.cellForRow(at: currentIndexPath) as! TueetorTextFieldTableViewCell
            cell.validateCountry(text: textField.text ?? "")
            
        case EditProfileScreenRowType.contact.rawValue:
            let cell = tableView.cellForRow(at: currentIndexPath) as! TueetorTextFieldTableViewCell
            cell.validatePhone(text: textField.text ?? "")

        default:
            break
        }
        tableView.beginUpdates()
        tableView.endUpdates()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (textField.text?.isEmpty)! && string == " " {
            return false
        }
        let char = string.cString(using: String.Encoding.utf8)!
        let isBackSpace = strcmp(char, "\\b")
        if (isBackSpace == -92) {
            return true
        }
        switch textField.tag {
        case EditProfileScreenRowType.email.rawValue:
            return (textField.text?.count)! < EmailMaxLimit

        case EditProfileScreenRowType.firstName.rawValue:
            return (textField.text?.count)! < NameMaxLimit
            
        case EditProfileScreenRowType.lastName.rawValue:
            return (textField.text?.count)! < NameMaxLimit
            
        case EditProfileScreenRowType.display.rawValue:
            return (textField.text?.count)! < DisplayNameMaxLimit

        case EditProfileScreenRowType.address2.rawValue:
            return (textField.text?.count)! < AddressMaxLimit
            
        case EditProfileScreenRowType.city.rawValue:
            let customTF = textField as! TueetorTextField
            if customTF.title == "CITY".localized {
                return (customTF.text?.count)! < CityMaxLimit
            } else {
                return (customTF.text?.count)! < PostalCodeMaxLimit
            }
            
        case EditProfileScreenRowType.contact.rawValue:
            return (textField.text?.count)! < PhoneNumberMaxLimit

        case EditProfileScreenRowType.address2.rawValue:
            return (textField.text?.count)! < AddressMaxLimit
            
        case EditProfileScreenRowType.city.rawValue:
            let customTF = textField as! TueetorTextField
            if customTF.title == "CITY".localized {
                return (customTF.text?.count)! < CityMaxLimit
            } else {
                return (customTF.text?.count)! < PostalCodeMaxLimit
            }
            
        case EditProfileScreenRowType.contact.rawValue:
            return (textField.text?.count)! < PhoneNumberMaxLimit
            
        default:
            return true
        }
    }
    
}


extension EditProfileViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func managePickerView() {
        let textField = activeTextField as! TueetorTextField

        titlePickerView.reloadAllComponents()
        titlePickerView.reloadInputViews()
        genderPickerView.reloadAllComponents()
        genderPickerView.reloadInputViews()
        countryPickerView.reloadAllComponents()
        countryPickerView.reloadInputViews()

        if textField.title == "TITLE".localized {
            textField.inputView = titlePickerView
            let index = titleOptions.index(of: editedUser.title ?? "")
            if let _index = index {
                self.titlePickerView.selectRow(_index, inComponent: 0, animated: true)
            } else {
                titlePickerView.selectRow(0, inComponent: 0, animated: true)
                activeTextField.text = titleOptions[0]
            }
        } else if textField.title == "GENDER".localized {
            textField.inputView = genderPickerView
            let index = genderOptions.index(of: editedUser.gender ?? "")
            if let _index = index {
                self.genderPickerView.selectRow(_index, inComponent: 0, animated: true)
            } else {
                genderPickerView.selectRow(0, inComponent: 0, animated: true)
                activeTextField.text = genderOptions[0]
            }
        } else {
            let index = countries.index { (country) -> Bool in
                country.name == editedUser.country
            }
            
            if let _index = index {
                self.countryPickerView.selectRow(_index, inComponent: 0, animated: true)
            } else {
                countryPickerView.selectRow(0, inComponent: 0, animated: true)
                activeTextField.text = countries[0].name
            }
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == titlePickerView {
            return titleOptions.count
        } else if pickerView == genderPickerView {
            return genderOptions.count
        } else {
            return countries.count
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == titlePickerView {
            return titleOptions[row]
        } else if pickerView == genderPickerView {
            return genderOptions[row]
        } else {
            return countries[row].name
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == titlePickerView {
            activeTextField.text = titleOptions[row]
        } else if pickerView == genderPickerView {
            activeTextField.text = genderOptions[row]
        } else {
            activeTextField.text = countries[row].name
            editedUser.country = countries[row].name
            editedUser.countryID = String(countries[row].id)
        }

    }
    
}

extension EditProfileViewController: AddressPlacePickerTableViewCellDelegate {
    
    func showPlacePicker() {
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        present(autocompleteController, animated: true, completion: nil)
    }
    
}

extension EditProfileViewController: GMSAutocompleteViewControllerDelegate {
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        
        editedUser.latitude = String(place.coordinate.latitude)
        editedUser.longitude = String(place.coordinate.longitude)
        editedUser.address1 = place.formattedAddress ?? place.name
        
        // Get the address components.
        if let addressLines = place.addressComponents {
            // Populate all of the address fields we can find.
            for field in addressLines {
                switch field.type {
                case kGMSPlaceTypeCountry:
                    let index = countries.index { (country) -> Bool in
                        country.name == field.name
                    }
                    if let _index = index {
                        editedUser.country = countries[_index].name
                        editedUser.countryID = String(countries[_index].id)
                    }
                case kGMSPlaceTypePostalCode:
                    editedUser.zipCode = field.name
                case kGMSPlaceTypeLocality:
                    editedUser.city = field.name
                default:
                    print("ignore")
                }
            }
        }
        
        tableView.reloadData()
        let cell = tableView.cellForRow(at: IndexPath.init(row: EditProfileScreenRowType.address1.rawValue, section: 0)) as! AddressPlacePickerTableViewCell
        cell.placeTextField.text = place.formattedAddress ?? place.name
        cell.validateAddress(text: cell.placeTextField.text ?? "")
        let cityCell = tableView.cellForRow(at: IndexPath.init(row: EditProfileScreenRowType.city.rawValue, section: 0)) as! TueetorDualTextFieldTableViewCell
        cityCell.textFieldOne.text = editedUser.city
        cityCell.textFieldTwo.text = editedUser.zipCode
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}

extension EditProfileViewController: ProfileDisplayLinkTableViewCellDelegate {

    func showShareOptions() {
        let myURL = NSURL(string: user.profileUrl)

        
        guard let url = myURL else {
            return
        }
        
        let shareItems: Array = [url]
        let activityViewController:UIActivityViewController = UIActivityViewController(activityItems: shareItems, applicationActivities: nil)
        activityViewController.excludedActivityTypes = [UIActivityType.print, UIActivityType.postToWeibo, UIActivityType.copyToPasteboard, UIActivityType.addToReadingList, UIActivityType.postToVimeo]
        self.present(activityViewController, animated: true, completion: nil)
    }
    
}

extension EditProfileViewController: EditProfileContactNumberTableViewCellDelegate {
    
    func showChangeNumberScreen() {
        let settingsViewController = ProfileSettingsViewController.instantiateFromAppStoryboard(appStoryboard: .Profile)
        settingsViewController.shouldShowMobileScreen = true
        navigationController?.pushViewController(settingsViewController, animated: true)
    }
    
}

extension EditProfileViewController: UITextViewDelegate {

    func textViewDidChange(_ textView: UITextView) {
        if let cell = tableView.cellForRow(at: IndexPath.init(row: textView.tag, section: 0)) as? AboutTableViewCell {
            cell.dummyTextField.text = textView.text
            let size = textView.bounds.size
            let newSize = textView.sizeThatFits(CGSize(width: size.width, height: CGFloat.greatestFiniteMagnitude))
            
            if aboutCellHeight != newSize.height + 32 {
                aboutCellHeight = newSize.height + 32
                UIView.setAnimationsEnabled(false)
                tableView?.beginUpdates()
                IQKeyboardManager.sharedManager().reloadLayoutIfNeeded()
                tableView?.endUpdates()
                UIView.setAnimationsEnabled(true)
            }
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        editedUser.about = textView.text
    }
    
}

extension EditProfileViewController {
    
    func fetchCountries() {
        
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }
        
        Utilities.showHUD(forView: self.view, excludeViews: [])
        let countryObserver = ApiManager.shared.apiService.fetchCountries()
        let countryDisposable = countryObserver.subscribe(onNext: {(countries) in
            Utilities.hideHUD(forView: self.view)
            self.countries = countries
            let index = self.countries.index { (country) -> Bool in
                return String(country.id) == self.editedUser.countryID!
            }
            if let _index = index {
                self.user.country = self.countries[_index].name
                self.editedUser.country = self.countries[_index].name
            }
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
            
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.showSimpleAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        })
        countryDisposable.disposed(by: disposableBag)
    }

    func editAccountWithImage(_ image: UIImage) {
        let imageData = UIImageJPEGRepresentation(image, 0.5)
        
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }
        
        Utilities.showHUD(forView: self.view, excludeViews: [])
        let dob = editedUser.dob!
        let inputDateFormatter = DateFormatter()
        inputDateFormatter.dateFormat = "dd-MMM-yyyy"
        let date = inputDateFormatter.date(from: dob)!
        let formattedDateString = Utilities.getFormattedDate(date, dateFormat: "yyyy-MM-dd")
        let editedUserDict : [String: String] = ["title": editedUser.title!,
                                                 "gender": editedUser.gender == "Male".localized ? "M" : "F",
                                                 "first_name": editedUser.firstName,
                                                 "last_name": editedUser.lastName,
                                                 "name": editedUser.displayName,
                                                 "dob": formattedDateString,
                                                 "about": editedUser.about ?? "",
                                                 "address1": editedUser.address1!,
                                                 "address2": editedUser.address2 ?? "",
                                                 "lattitude": editedUser.latitude!,
                                                 "longitude": editedUser.longitude!,
                                                 "city": editedUser.city ?? "",
                                                 "postcode": editedUser.zipCode ?? "",
                                                 "country_id": editedUser.countryID ?? ""]
        
        print("editedUserDict: \n", editedUserDict)
        
        let url = "http://tueetoradmin.hipster-inc.com/public/post/profile/\(editedUser.ID!)"
        
        let headers: HTTPHeaders = [
            "Content-type": "multipart/form-data"
        ]
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in editedUserDict {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            
            if let data = imageData {
                multipartFormData.append(data, withName: "image", fileName: "image.png", mimeType: "image/png")
            }
            
        }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (result) in
            switch result{
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    print("Succesfully uploaded")
                    DispatchQueue.main.async(execute: {
                        Utilities.hideHUD(forView: self.view)
                    })
                    if let _ = response.error{
                        self.showSimpleAlertWithMessage("Failed to edit account.".localized)
                        return
                    }
                    let json = try? JSONSerialization.jsonObject(with: response.data!, options: .mutableContainers)
                    print(json)
                    if let userDict = json as? [String: AnyObject], let data = userDict["data"] as? [String: AnyObject], let userObject = data["user"] as? [String: AnyObject] {
                        do {
                            let user: User = try unbox(dictionary: userObject)
                            (UIApplication.shared.delegate as! AppDelegate).user = user
                            DispatchQueue.main.async(execute: {
                                self.showSimpleAlertWithMessage("Updated user successfully.".localized)
                            })

                        } catch {
                            //Error
                            DispatchQueue.main.async(execute: {
                                self.showSimpleAlertWithMessage("Error Occured updating the profile.".localized)
                            })

                        }
                    }
                }
            case .failure(let error):
                DispatchQueue.main.async(execute: {
                    Utilities.hideHUD(forView: self.view)
                    if let error_ = error as? ResponseError {
                        self.showSimpleAlertWithMessage(error_.description())
                    } else {
                        if !error.localizedDescription.isEmpty {
                            self.showSimpleAlertWithMessage(error.localizedDescription)
                        }
                    }
                })
            }
        }
    }
    
    func getProfileLinkURLForName(_ displayName: String) {
        
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }
        
        Utilities.showHUD(forView: self.view, excludeViews: [])
        let linkObserver = ApiManager.shared.apiService.profileLinkForName(displayName)
        let linkDisposable = linkObserver.subscribe(onNext: {(validName) in
            Utilities.hideHUD(forView: self.view)
            let trimmedString = validName.components(separatedBy: .whitespacesAndNewlines).joined(separator: "")
            
            self.name = trimmedString.lowercased()
            self.editedUser.profileUrl = "\(baseUrl)/\(self.name)"
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.showSimpleAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        })
        linkDisposable.disposed(by: disposableBag)
    }


}
