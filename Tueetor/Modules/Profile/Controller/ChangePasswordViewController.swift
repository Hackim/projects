//
//  ChangePasswordViewController.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 28/05/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit
import XLPagerTabStrip
import RxSwift

class ChangePasswordViewController: BaseViewController, IndicatorInfoProvider {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var changePasswordButton: UIButton!
    
    var activeTextField: UITextField!
    let labelFields = ["OLD PASSWORD".localized, "NEW PASSWORD".localized, "CONFIRM PASSWORD".localized]
    var oldPassword = ""
    var newPassword = ""
    var confirmPassword = ""
    var disposableBag = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView()
        changePasswordButton.layer.shadowOffset = CGSize.init(width: 0, height: 5)
        changePasswordButton.layer.shadowRadius = 3
        changePasswordButton.layer.shadowColor = themeBlueColor.cgColor
        changePasswordButton.layer.shadowOpacity = 0.75

    }

    @IBAction func changePasswordButtonTapped(_ sender: UIButton) {
        guard isDataValid() else {
            showInvalidInputsMessage()
            return
        }
        changePassword(newPassword, oldPassword)
    }
    
    func resetData() {
        self.oldPassword = ""
        self.newPassword = ""
        self.confirmPassword = ""
        for index in 0...labelFields.count - 1 {
            let currentIndexPath = IndexPath.init(row: index, section: 0)
            let cell = tableView.cellForRow(at: currentIndexPath) as! TueetorTextFieldTableViewCell
            cell.customTextField.hideImage()
        }
    }

    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: "Password".localized)
    }
    
    func configureTextField(_ customTextField: TueetorTextField) {
        customTextField.delegate = self
        customTextField.isSecureTextEntry = true
        customTextField.keyboardType = .asciiCapable
    }

    func updateDataIntoTextField(_ customTextField: TueetorTextField, fromCell: Bool) {
        switch customTextField.tag {
        case ChangePasswordTextFieldType.oldPassword.rawValue:
            fromCell ? (customTextField.text = oldPassword) : (oldPassword = customTextField.text!)
            break
        case ChangePasswordTextFieldType.newPassword.rawValue:
            fromCell ? (customTextField.text = newPassword) : (newPassword = customTextField.text!)
            break
        case ChangePasswordTextFieldType.confirmPassword.rawValue:
            fromCell ? (customTextField.text = confirmPassword) : (confirmPassword = customTextField.text!)
            break
        default:
            break
        }
    }

    func isDataValid() -> Bool {
        var errorMessages: [String] = []
        
        for index in 0...labelFields.count - 1 {
            let currentIndexPath = IndexPath.init(row: index, section: 0)
            let cell = tableView.cellForRow(at: currentIndexPath) as! TueetorTextFieldTableViewCell
            switch index {
            case ChangePasswordTextFieldType.oldPassword.rawValue:
                cell.validatePassword(text: oldPassword)
            case ChangePasswordTextFieldType.newPassword.rawValue:
                cell.validatePassword(text: newPassword)
            case ChangePasswordTextFieldType.confirmPassword.rawValue:
                cell.validateConfirmPassword(text: confirmPassword, password: newPassword)
            default:
                break
            }
            let message = cell.errorLabel.text ?? ""
            if !message.isEmptyString() {
                errorMessages.append(message)
            }
        }
        tableView.beginUpdates()
        tableView.endUpdates()
        return errorMessages.count == 0
    }
    
}

extension ChangePasswordViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return labelFields.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: TueetorTextFieldTableViewCell.cellIdentifier(), for: indexPath) as! TueetorTextFieldTableViewCell
        cell.configureCellForRowAtIndex(indexPath.row, withText: labelFields[indexPath.row])
        updateDataIntoTextField(cell.customTextField, fromCell: true)
        configureTextField(cell.customTextField)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        tableView.estimatedRowHeight = 63.0
        return UITableViewAutomaticDimension
    }
    
}

//MARK: - TextField Delegate -

extension ChangePasswordViewController: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        activeTextField = textField as! TueetorTextField
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        updateDataIntoTextField(textField  as! TueetorTextField, fromCell: false)
        let currentIndexPath = IndexPath.init(row: textField.tag, section: 0)
        let cell = tableView.cellForRow(at: currentIndexPath) as! TueetorTextFieldTableViewCell
        switch textField.tag {
        case ChangePasswordTextFieldType.oldPassword.rawValue:
            cell.validatePassword(text: textField.text ?? "")
        case ChangePasswordTextFieldType.newPassword.rawValue:
            cell.validatePassword(text: textField.text ?? "")
        case ChangePasswordTextFieldType.confirmPassword.rawValue:
            cell.validateConfirmPassword(text: textField.text ?? "", password: newPassword)
        default:
            break
        }
        tableView.beginUpdates()
        tableView.endUpdates()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (textField.text?.isEmpty)! && string == " " {
            return false
        }
        let char = string.cString(using: String.Encoding.utf8)!
        let isBackSpace = strcmp(char, "\\b")
        if (isBackSpace == -92) {
            return true
        }
        return (textField.text?.count)! < PasswordMaxLimit
    }
    
}

extension ChangePasswordViewController {
    
    func changePassword(_ password: String, _ oldPassword: String) {
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }
        
        guard let userID = UserStore.shared.userID else {
            return
        }
        
        Utilities.showHUD(forView: self.view, excludeViews: [])
        
        let changePasswordObserver = ApiManager.shared.apiService.changePasswordForID(String(userID), oldPassword: oldPassword, newPassword: password)
        let changePasswordDisposable = changePasswordObserver.subscribe(onNext: {(user) in
            Utilities.hideHUD(forView: self.view)
            
            DispatchQueue.main.async {
                let alert = UIAlertController(title: AppName, message: AlertMessage.changePasswordSuccess.description(), preferredStyle: .alert)
                let okAction = UIAlertAction(title: AlertButton.ok.description(), style: .default, handler: { (action) in
                    self.resetData()
                    self.tableView.reloadData()
                })
                alert.addAction(okAction)
                self.present(alert, animated: true, completion: nil)
            }
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.showSimpleAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        })
        changePasswordDisposable.disposed(by: disposableBag)
    }

}
