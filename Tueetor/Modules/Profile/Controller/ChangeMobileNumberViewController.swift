//
//  ChangeMobileNumberViewController.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 28/05/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit
import XLPagerTabStrip
import RxSwift

class ChangeMobileNumberViewController: BaseViewController, IndicatorInfoProvider {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var changeMobileButton: UIButton!
    
    var activeTextField: UITextField!
    let labelFields = ["OLD CONTACT NUMBER".localized, "NEW CONTACT NUMBER".localized]
    var oldMobile = ""
    var newMobile = ""
    var disposableBag = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView()
        changeMobileButton.layer.shadowOffset = CGSize.init(width: 0, height: 5)
        changeMobileButton.layer.shadowRadius = 3
        changeMobileButton.layer.shadowColor = themeBlueColor.cgColor
        changeMobileButton.layer.shadowOpacity = 0.75
    }
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: "Contact".localized)
    }

    func resetData() {
        self.oldMobile = ""
        self.newMobile = ""
        for index in 0...labelFields.count - 1 {
            let currentIndexPath = IndexPath.init(row: index, section: 0)
            let cell = tableView.cellForRow(at: currentIndexPath) as! TueetorTextFieldTableViewCell
            cell.customTextField.hideImage()
        }
    }

    @IBAction func changeMobileButtonTapped(_ sender: UIButton) {
        guard isDataValid() else {
            showInvalidInputsMessage()
            return
        }
        self.changeMobileNumber(newMobile, oldMobile)
    }

    func configureTextField(_ customTextField: TueetorTextField) {
        customTextField.delegate = self
        customTextField.isSecureTextEntry = false
        customTextField.keyboardType = .phonePad
    }
    
    func updateDataIntoTextField(_ customTextField: TueetorTextField, fromCell: Bool) {
        switch customTextField.tag {
        case ChangeMobileTextFieldType.oldMobile.rawValue:
            fromCell ? (customTextField.text = oldMobile) : (oldMobile = customTextField.text!)
            break
        case ChangeMobileTextFieldType.newMobile.rawValue:
            fromCell ? (customTextField.text = newMobile) : (newMobile = customTextField.text!)
            break
        default:
            break
        }
    }
    
    func isDataValid() -> Bool {
        var errorMessages: [String] = []
        
        for index in 0...labelFields.count - 1 {
            let currentIndexPath = IndexPath.init(row: index, section: 0)
            let cell = tableView.cellForRow(at: currentIndexPath) as! TueetorTextFieldTableViewCell
            switch index {
            case ChangeMobileTextFieldType.oldMobile.rawValue:
                cell.validatePhone(text: oldMobile)
            case ChangeMobileTextFieldType.newMobile.rawValue:
                cell.validatePhone(text: newMobile)
            default:
                break
            }
            let message = cell.errorLabel.text ?? ""
            if !message.isEmptyString() {
                errorMessages.append(message)
            }
        }
        tableView.beginUpdates()
        tableView.endUpdates()
        return errorMessages.count == 0
    }

}

extension ChangeMobileNumberViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return labelFields.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: TueetorTextFieldTableViewCell.cellIdentifier(), for: indexPath) as! TueetorTextFieldTableViewCell
        cell.configureCellForRowAtIndex(indexPath.row, withText: labelFields[indexPath.row])
        updateDataIntoTextField(cell.customTextField, fromCell: true)
        configureTextField(cell.customTextField)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        tableView.estimatedRowHeight = 63.0
        return UITableViewAutomaticDimension
    }
    
}

//MARK: - TextField Delegate -

extension ChangeMobileNumberViewController: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        activeTextField = textField as! TueetorTextField
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        updateDataIntoTextField(textField  as! TueetorTextField, fromCell: false)
        let currentIndexPath = IndexPath.init(row: textField.tag, section: 0)
        let cell = tableView.cellForRow(at: currentIndexPath) as! TueetorTextFieldTableViewCell
        switch textField.tag {
        case ChangeMobileTextFieldType.oldMobile.rawValue:
            cell.validatePhone(text: textField.text ?? "")
        case ChangeMobileTextFieldType.newMobile.rawValue:
            cell.validatePhone(text: textField.text ?? "")
        default:
            break
        }
        tableView.beginUpdates()
        tableView.endUpdates()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (textField.text?.isEmpty)! && string == " " {
            return false
        }
        let char = string.cString(using: String.Encoding.utf8)!
        let isBackSpace = strcmp(char, "\\b")
        if (isBackSpace == -92) {
            return true
        }
        return (textField.text?.count)! < PhoneNumberMaxLimit
    }
    
}

extension ChangeMobileNumberViewController {
    
    func changeMobileNumber(_ newNumber: String, _ oldNumber: String) {
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }
        
        guard let userID = UserStore.shared.userID else {
            return
        }
        
        Utilities.showHUD(forView: self.view, excludeViews: [])
        
        let changeMobileObserver = ApiManager.shared.apiService.changePhoneNumberForID(String(userID), oldNumber: oldNumber, newNumber: newNumber)
        let changeMobileDisposable = changeMobileObserver.subscribe(onNext: {(id) in
            Utilities.hideHUD(forView: self.view)
            
            DispatchQueue.main.async {
                self.resetData()
                let otpViewController = ProfileOTPViewController.instantiateFromAppStoryboard(appStoryboard: .Profile)
                otpViewController.phoneNumber = newNumber
                otpViewController.userID = userID
                self.navigationController?.pushViewController(otpViewController, animated: true)
            }
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.showSimpleAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        })
        changeMobileDisposable.disposed(by: disposableBag)
    }
    
}

