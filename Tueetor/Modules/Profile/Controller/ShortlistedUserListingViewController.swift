//
//  ShortlistedUserListingViewController.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 23/06/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit
import RxSwift

class ShortlistedUserListingViewController: BaseViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var searchTextFieldWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var emptyMessageLabel: UILabel!

    
    var disposableBag = DisposeBag()
    var isFetchingData = false
    var isSearchActive = false
    var isFirstTime = true
    var searchString = ""

    var shortlistPagination = ShortlistedUserPagination.init()
    var shortlistDisposable: Disposable!

    private let refreshControl = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 10.0, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.addSubview(refreshControl)
        }
        
        searchTextFieldWidthConstraint.constant = 0.0
        refreshControl.addTarget(self, action: #selector(refreshData(_:)), for: .valueChanged)
        fetchShortlistedUsers()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        (self.tabBarController as! BaseTabBarViewController).tabBar.isHidden = true
        (self.tabBarController as! BaseTabBarViewController).hideCenterButton()
    }
    
    @objc func refreshData(_ sender: Any) {
        self.shortlistPagination.paginationType = .new
        fetchShortlistedUsers()
        if let refreshControl = sender as? UIRefreshControl {
            refreshControl.endRefreshing()
        }
    }
    
    @IBAction func searchButtonTapped(_ sender: UIButton) {
        isSearchActive = !isSearchActive
        if isSearchActive {
            searchTextField.becomeFirstResponder()
        } else {
            searchTextField.resignFirstResponder()
        }
        self.view.layoutIfNeeded()
        UIView.animate(withDuration: 0.7, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 1, options: .curveEaseIn, animations: {
            self.searchTextFieldWidthConstraint.constant = self.isSearchActive ? ScreenWidth - 132.0 : 0
            let imageName = self.isSearchActive ? "search_active" : "search_inactive"
            self.searchButton.setImage(UIImage(named: imageName), for: .normal)
            self.searchButton.backgroundColor = self.isSearchActive ? themeBlueColor : UIColor.clear
            self.view.layoutIfNeeded()
        }) { (completed) in
            
        }
    }
    
    @IBAction func valueChangedInSearchField(_ sender: Any) {
        if (shortlistDisposable != nil) {
            Utilities.hideHUD(forView: view)
            shortlistDisposable.dispose()
        }
        shortlistPagination.paginationType = .new
        fetchShortlistedUsers()
    }
    
    
    func showEditCommentAlert(_ index: Int) {
        let alertController = UIAlertController(title: AppName,
                                                message: AlertMessage.editShortlistComment.description(),
                                                preferredStyle: .alert)
        
        
        let confirmAction = UIAlertAction(title: AlertButton.confirm.description(),
                                          style: .default) { [weak alertController] _ in
                                            guard let alertController = alertController, let textField = alertController.textFields?.first else { return }
                                            DispatchQueue.main.async {
                                                print(textField.text!)
                                                self.editShortlistAtIndex(index, newComment: textField.text!)
                                            }
        }
        confirmAction.isEnabled = false
        alertController.addAction(confirmAction)
        
        alertController.addTextField { textField in
            textField.placeholder = "Note".localized
            
            NotificationCenter.default.addObserver(forName: NSNotification.Name.UITextFieldTextDidChange, object: textField, queue: OperationQueue.main, using: { (notification) in
                confirmAction.isEnabled = !textField.text!.isEmpty
            })
        }
        
        
        let cancelAction = UIAlertAction(title: AlertButton.cancel.description(), style: .cancel, handler: nil)
        alertController.addAction(cancelAction)
        present(alertController, animated: true, completion: nil)
    }
    
    func showDeleteAlertForIndex(_ index: Int) {
        let alert = UIAlertController(title: AppName, message: AlertMessage.delete.description(), preferredStyle: .alert)
        
        let confirmAction = UIAlertAction(title: AlertButton.confirm.description(), style: .default, handler: { (action) in
            DispatchQueue.main.async {
                self.deleteShortlistAtIndex(index)
            }
        })
        alert.addAction(confirmAction)
        
        let cancelAction = UIAlertAction(title: AlertButton.cancel.description(), style: .default, handler: nil)
        alert.addAction(cancelAction)
        
        self.present(alert, animated: true, completion: nil)
    }
    
}


extension ShortlistedUserListingViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if shortlistPagination.shortlistedUsers.count == 0 {
            return 0
        }
        return shortlistPagination.shortlistedUsers.count + 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard indexPath.row != shortlistPagination.shortlistedUsers.count + 1 else {
            let cell = tableView.dequeueReusableCell(withIdentifier: LoadMoreTableViewCell.cellIdentifier(), for: indexPath) as! LoadMoreTableViewCell
            cell.activityIndicator.startAnimating()
            return cell
        }
        
        guard indexPath.row != shortlistPagination.shortlistedUsers.count else {
            let cell = tableView.dequeueReusableCell(withIdentifier: EmptyMessageTableViewCell.cellIdentifier(), for: indexPath) as! EmptyMessageTableViewCell
            cell.messageLabel.text = "Swipe left to perform actions".localized
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: MyShortlistTableViewCell.cellIdentifier(), for: indexPath) as! MyShortlistTableViewCell
        cell.configureCellWithUser(shortlistPagination.shortlistedUsers[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == shortlistPagination.shortlistedUsers.count {
            return 44.0
        }
        
        if indexPath.row == shortlistPagination.shortlistedUsers.count + 1 {
            return shortlistPagination.hasMoreToLoad ? 50.0 : 0
        }
        tableView.estimatedRowHeight = 189.0
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == shortlistPagination.shortlistedUsers.count + 1 && self.shortlistPagination.hasMoreToLoad && !isFetchingData {
            fetchShortlistedUsers()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard indexPath.row < shortlistPagination.shortlistedUsers.count else {
            return
        }
        let shortlistedUser = shortlistPagination.shortlistedUsers[indexPath.row]
        if shortlistedUser.userType == "T" {
            let tutorProfile = TutorProfileViewController.instantiateFromAppStoryboard(appStoryboard: .Dashboard)
            tutorProfile.tutorID = shortlistedUser.userID
            tutorProfile.delegate = self
            navigationController?.pushViewController(tutorProfile, animated: true)
        } else {
            let studentProfile = StudentProfileViewController.instantiateFromAppStoryboard(appStoryboard: .Dashboard)
            studentProfile.studentID = shortlistedUser.userID
            studentProfile.delegate = self
            navigationController?.pushViewController(studentProfile, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let edit = UITableViewRowAction(style: .normal, title: "Edit".localized) { action, index in
            DispatchQueue.main.async {
                self.showEditCommentAlert(index.row)
            }
        }
        
        edit.backgroundColor = UIColor.init(red: 0.3843, green: 0.7882, blue: 0.4156, alpha: 1)
        
        let delete = UITableViewRowAction(style: .destructive, title: "Delete".localized) { action, index in
            
            DispatchQueue.main.async {
                self.showDeleteAlertForIndex(index.row)
            }
        }
        delete.backgroundColor = UIColor.init(red: 0.8941, green: 0.3764, blue: 0.3607, alpha: 1)
        
        return [delete, edit]
        
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        if indexPath.row >= shortlistPagination.shortlistedUsers.count {
            return false
        }
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
    }
    

}

extension ShortlistedUserListingViewController: UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if isFetchingData == false, searchString != textField.text {
            shortlistPagination.paginationType = .new
            fetchShortlistedUsers()
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (textField.text?.isEmpty)! && string == " " {
            return false
        }
        let char = string.cString(using: String.Encoding.utf8)!
        let isBackSpace = strcmp(char, "\\b")
        if (isBackSpace == -92) {
            return true
        }
        return true
    }
}
extension ShortlistedUserListingViewController: TutorProfileViewControllerDelegate {
    func refreshShortlistDataForTutor() {
        self.shortlistPagination.paginationType = .new
        fetchShortlistedUsers()
    }
    
    func refreshDataForTutorWithID(_ ID: String) {
        
    }
    
    
    
}
extension ShortlistedUserListingViewController: StudentProfileViewControllerDelegate {
    func refreshShortlistDataForStudent() {
        self.shortlistPagination.paginationType = .new
        fetchShortlistedUsers()
    }
    
    func refreshDataForStudentWithID(_ ID: String) {
        
    }
    
    
    
}

extension ShortlistedUserListingViewController {
    
    func fetchShortlistedUsers() {
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }
        searchString = searchTextField.text ?? ""
        guard let userID = UserStore.shared.userID else {
            return
        }

        if shortlistPagination.paginationType != .old {
            self.shortlistPagination.currentPageNumber = 1
            if isFirstTime == true {
                Utilities.showHUD(forView: self.view, excludeViews: [])
            } else {
                UIApplication.shared.isNetworkActivityIndicatorVisible = true
            }
            
        } else {
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
        }
        
        let shortlistObserver = ApiManager.shared.apiService.fetchShortlistUsersForUserID(userID, page: shortlistPagination.currentPageNumber, query: searchString)
        shortlistDisposable = shortlistObserver.subscribe(onNext: {(paginationObject) in
            self.isFetchingData = false
            
            self.shortlistPagination.appendDataFromObject(paginationObject)
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if self.shortlistPagination.paginationType != .old && self.isFirstTime == true {
                    self.isFirstTime = false
                } else {
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                }
                self.tableView.isHidden = self.shortlistPagination.shortlistedUsers.count == 0
                self.emptyMessageLabel.isHidden = self.shortlistPagination.shortlistedUsers.count != 0
                self.emptyMessageLabel.text = self.searchString.isEmpty ? "You haven't shortlisted anyone yet.".localized : "No users found with \(self.searchString)".localized
                self.tableView.reloadData()
            })
        }, onError: {(error) in
            self.isFetchingData = false
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
            })
        })
        shortlistDisposable.disposed(by: disposableBag)
    }
    
    func editShortlistAtIndex(_ index: Int, newComment: String) {
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }
        
        guard let userID = UserStore.shared.userID else {
            return
        }
        
        Utilities.showHUD(forView: self.view, excludeViews: [])
        
        let editShortlistObserver = ApiManager.shared.apiService.editShortlistForUser(String(userID), shortlistUserID: shortlistPagination.shortlistedUsers[index].userID, reason: newComment)
        let editShortlistDisposable = editShortlistObserver.subscribe(onNext: {(message) in
            Utilities.hideHUD(forView: self.view)
            
            DispatchQueue.main.async {
                self.showSimpleAlertWithMessage(message)
                self.shortlistPagination.paginationType = .new
                self.fetchShortlistedUsers()
            }
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.showSimpleAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        })
        editShortlistDisposable.disposed(by: disposableBag)
    }

    func deleteShortlistAtIndex(_ index: Int) {
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }
        
        guard let userID = UserStore.shared.userID else {
            return
        }
        
        Utilities.showHUD(forView: self.view, excludeViews: [])
        
        let deleteShortlistObserver = ApiManager.shared.apiService.deleteShortlistForUser(String(userID), shortlistUserID: shortlistPagination.shortlistedUsers[index].userID)
        let deleteShortlistDisposable = deleteShortlistObserver.subscribe(onNext: {(message) in
            Utilities.hideHUD(forView: self.view)
            NotificationCenter.default.post(name: .refreshDashboardData, object: nil, userInfo:nil)

            DispatchQueue.main.async {
                self.showSimpleAlertWithMessage(message)
                self.shortlistPagination.paginationType = .new
                self.fetchShortlistedUsers()
            }
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.showSimpleAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        })
        deleteShortlistDisposable.disposed(by: disposableBag)
    }

}


