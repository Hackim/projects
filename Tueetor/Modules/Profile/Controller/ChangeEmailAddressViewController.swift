//
//  ChangeEmailAddressViewController.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 28/05/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit
import XLPagerTabStrip
import RxSwift

class ChangeEmailAddressViewController: BaseViewController, IndicatorInfoProvider {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var changeEmailButton: UIButton!

    var activeTextField: UITextField!
    let labelFields = ["OLD EMAIL".localized, "NEW EMAIL".localized, "CONFIRM EMAIL".localized]
    var oldEmail = ""
    var newEmail = ""
    var confirmEmail = ""
    var disposableBag = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView()
        changeEmailButton.layer.shadowOffset = CGSize.init(width: 0, height: 5)
        changeEmailButton.layer.shadowRadius = 3
        changeEmailButton.layer.shadowColor = themeBlueColor.cgColor
        changeEmailButton.layer.shadowOpacity = 0.75
    }

    @IBAction func changeEmailButtonTapped(_ sender: UIButton) {
        guard isDataValid() else {
            showInvalidInputsMessage()
            return
        }
        changeEmail(newEmail, oldEmail)
    }

    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: "Email".localized)
    }
    
    func configureTextField(_ customTextField: TueetorTextField) {
        customTextField.delegate = self
        customTextField.isSecureTextEntry = false
        customTextField.keyboardType = .emailAddress
    }
    
    func resetData() {
        self.oldEmail = ""
        self.newEmail = ""
        self.confirmEmail = ""
        for index in 0...labelFields.count - 1 {
            let currentIndexPath = IndexPath.init(row: index, section: 0)
            let cell = tableView.cellForRow(at: currentIndexPath) as! TueetorTextFieldTableViewCell
            cell.customTextField.hideImage()
        }
    }
    
    func updateDataIntoTextField(_ customTextField: TueetorTextField, fromCell: Bool) {
        switch customTextField.tag {
        case ChangeEmailTextFieldType.oldEmail.rawValue:
            fromCell ? (customTextField.text = oldEmail) : (oldEmail = customTextField.text!)
            break
        case ChangeEmailTextFieldType.newEmail.rawValue:
            fromCell ? (customTextField.text = newEmail) : (newEmail = customTextField.text!)
            break
        case ChangeEmailTextFieldType.confirmEmail.rawValue:
            fromCell ? (customTextField.text = confirmEmail) : (confirmEmail = customTextField.text!)
            break
        default:
            break
        }
    }
    
    func isDataValid() -> Bool {
        var errorMessages: [String] = []
        
        for index in 0...labelFields.count - 1 {
            let currentIndexPath = IndexPath.init(row: index, section: 0)
            let cell = tableView.cellForRow(at: currentIndexPath) as! TueetorTextFieldTableViewCell
            switch index {
            case ChangeEmailTextFieldType.oldEmail.rawValue:
                cell.validateEmail(text: oldEmail)
            case ChangeEmailTextFieldType.newEmail.rawValue:
                cell.validateEmail(text: newEmail)
            case ChangeEmailTextFieldType.confirmEmail.rawValue:
                cell.validateConfirmEmail(text: confirmEmail, email: newEmail)
            default:
                break
            }
            let message = cell.errorLabel.text ?? ""
            if !message.isEmptyString() {
                errorMessages.append(message)
            }
        }
        tableView.beginUpdates()
        tableView.endUpdates()
        return errorMessages.count == 0
    }

}

extension ChangeEmailAddressViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return labelFields.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: TueetorTextFieldTableViewCell.cellIdentifier(), for: indexPath) as! TueetorTextFieldTableViewCell
        cell.configureCellForRowAtIndex(indexPath.row, withText: labelFields[indexPath.row])
        updateDataIntoTextField(cell.customTextField, fromCell: true)
        configureTextField(cell.customTextField)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        tableView.estimatedRowHeight = 63.0
        return UITableViewAutomaticDimension
    }
    
}

//MARK: - TextField Delegate -

extension ChangeEmailAddressViewController: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        activeTextField = textField as! TueetorTextField
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        updateDataIntoTextField(textField  as! TueetorTextField, fromCell: false)
        let currentIndexPath = IndexPath.init(row: textField.tag, section: 0)
        let cell = tableView.cellForRow(at: currentIndexPath) as! TueetorTextFieldTableViewCell
        switch textField.tag {
        case ChangeEmailTextFieldType.oldEmail.rawValue:
            cell.validateEmail(text: textField.text ?? "")
        case ChangeEmailTextFieldType.newEmail.rawValue:
            cell.validateEmail(text: textField.text ?? "")
        case ChangeEmailTextFieldType.confirmEmail.rawValue:
            cell.validateConfirmEmail(text: textField.text ?? "", email: newEmail)
        default:
            break
        }
        tableView.beginUpdates()
        tableView.endUpdates()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (textField.text?.isEmpty)! && string == " " {
            return false
        }
        let char = string.cString(using: String.Encoding.utf8)!
        let isBackSpace = strcmp(char, "\\b")
        if (isBackSpace == -92) {
            return true
        }
        return (textField.text?.count)! < EmailMaxLimit
    }
    
}

extension ChangeEmailAddressViewController {
    
    func changeEmail(_ newEmail: String, _ oldEmail: String) {
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }
        
        guard let userID = UserStore.shared.userID else {
            return
        }
        
        Utilities.showHUD(forView: self.view, excludeViews: [])
        
        let changeEmailObserver = ApiManager.shared.apiService.changeEmailForID(String(userID), oldEmail: oldEmail, newEmail: newEmail)
        let changeEmailDisposable = changeEmailObserver.subscribe(onNext: {(user) in
            Utilities.hideHUD(forView: self.view)
            
            DispatchQueue.main.async {
                let alert = UIAlertController(title: AppName, message: AlertMessage.changeEmailSuccess.description(), preferredStyle: .alert)
                let okAction = UIAlertAction(title: AlertButton.ok.description(), style: .default, handler: { (action) in
                    self.resetData()
                    self.tableView.reloadData()
                })
                alert.addAction(okAction)
                self.present(alert, animated: true, completion: nil)
            }
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.showSimpleAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        })
        changeEmailDisposable.disposed(by: disposableBag)
    }
    
}
