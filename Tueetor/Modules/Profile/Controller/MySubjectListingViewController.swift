//
//  MySubjectListingViewController.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 13/06/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit
import RxSwift

class MySubjectListingViewController: BaseViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var disposableBag = DisposeBag()
    var isFetchingData = false
    var isFirstTime = true
    var subjectPagination = MySubjectPagination.init()
    var subjectObserver: Observable<MySubjectPagination>!
    var refreshData = false
    @IBOutlet weak var emptyMessageLabel: UILabel!
    
    private let refreshControl = UIRefreshControl()

    override func viewDidLoad() {
        super.viewDidLoad()
        (self.tabBarController as! BaseTabBarViewController).tabBar.isHidden = true
        (self.tabBarController as! BaseTabBarViewController).hideCenterButton()
        tableView.tableFooterView = UIView()
        if #available(iOS 10.0, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.addSubview(refreshControl)
        }
        refreshControl.addTarget(self, action: #selector(refreshData(_:)), for: .valueChanged)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(refreshSubjectList), name: .subjectsUpdated,
                                               object: nil)

        fetchMySubjects()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if refreshData {
            refreshData = false
            self.subjectPagination.paginationType = .new
            fetchMySubjects()
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: .subjectsUpdated, object: nil)
    }
    
    @objc func refreshData(_ sender: Any) {
        self.subjectPagination.paginationType = .new
        fetchMySubjects()
        if let refreshControl = sender as? UIRefreshControl {
            refreshControl.endRefreshing()
        }
    }
    
    @objc func refreshSubjectList() {
        refreshData = true
    }
    
    func showDataAppropritely() {
        if self.subjectPagination.subjects.count == 0 {
            tableView.isHidden = true
            emptyMessageLabel.isHidden = false
        } else {
            tableView.isHidden = false
            emptyMessageLabel.isHidden = true
        }
        tableView.reloadData()
    }

    @IBAction func addButtonTapped(_ sender: UIButton) {
        let newSubjectViewController =  AddSubjectViewController.instantiateFromAppStoryboard(appStoryboard: .Profile)
        navigationController?.pushViewController(newSubjectViewController, animated: true)
    }
    
    func showLocationScreenForIndex(_ index: Int) {
        let subject = subjectPagination.subjects[index].subject
        let locationViewController = LocationViewController.instantiateFromAppStoryboard(appStoryboard: .Profile)
        var latitude = ""
        var longitude = ""
        if let lat = subject?.latitude, !lat.isEmpty {
            latitude = lat
            longitude = subject!.logitude
        } else {
            let user = (UIApplication.shared.delegate as! AppDelegate).user
            latitude = user?.latitude ?? "0"
            longitude = user?.longitude ?? "0"
        }
        locationViewController.locationToShow = TueetorLocation.init(name: subject?.name ?? "", lat: latitude, long: longitude)
        navigationController?.pushViewController(locationViewController, animated: true)
    }
    
    func showSchedulesScreenForIndex(_ index: Int) {
        let subject = subjectPagination.subjects[index]
        let availabilityViewController = MyAvailabilityViewController.instantiateFromAppStoryboard(appStoryboard: .Profile)
        availabilityViewController.isEditable = false
        availabilityViewController.schedules = subject.availability
        navigationController?.pushViewController(availabilityViewController, animated: true)
    }
    
    func showEditScreenForIndex(_ index: Int) {
        let subject = subjectPagination.subjects[index]
        let editSubjectViewController = AddSubjectViewController.instantiateFromAppStoryboard(appStoryboard: .Profile)
        editSubjectViewController.mySubject = subject
        self.navigationController?.pushViewController(editSubjectViewController, animated: true)

    }
    
    func deleteSubjectAtIndex(_ index: Int) {
        let subject = subjectPagination.subjects[index]
        let alert = UIAlertController(title: AppName, message: AlertMessage.deleteSubject.description(), preferredStyle: .alert)
        
        let confirmAction = UIAlertAction(title: AlertButton.confirm.description(), style: .default, handler: { (action) in
            DispatchQueue.main.async {
                self.deleteSubjectWithID(String(subject.subject.id))
            }
        })
        alert.addAction(confirmAction)
        
        let cancelAction = UIAlertAction(title: AlertButton.cancel.description(), style: .default, handler: nil)
        alert.addAction(cancelAction)
        
        self.present(alert, animated: true, completion: nil)

    }
    
}


extension MySubjectListingViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if subjectPagination.subjects.count == 0 {
            return 0
        }

        return subjectPagination.subjects.count + 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard indexPath.row != subjectPagination.subjects.count + 1 else {
            let cell = tableView.dequeueReusableCell(withIdentifier: LoadMoreTableViewCell.cellIdentifier(), for: indexPath) as! LoadMoreTableViewCell
            cell.activityIndicator.startAnimating()
            return cell
        }

        guard indexPath.row != subjectPagination.subjects.count else {
            let cell = tableView.dequeueReusableCell(withIdentifier: EmptyMessageTableViewCell.cellIdentifier(), for: indexPath) as! EmptyMessageTableViewCell
            cell.messageLabel.text = "Swipe left to perform actions".localized
            return cell
        }

        let cell = tableView.dequeueReusableCell(withIdentifier: MySubjectTableViewCell.cellIdentifier(), for: indexPath) as! MySubjectTableViewCell
        cell.configureCellWithSubject(subjectPagination.subjects[indexPath.row], forIndex: indexPath.row)
        cell.delegate = self
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == subjectPagination.subjects.count {
            return 44.0
        }

        if indexPath.row == subjectPagination.subjects.count + 1 {
            return subjectPagination.hasMoreToLoad ? 50.0 : 0
        }
        return MySubjectTableViewCell.cellHeight()
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == subjectPagination.subjects.count + 1 && self.subjectPagination.hasMoreToLoad && !isFetchingData {
            fetchMySubjects()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row < subjectPagination.subjects.count {
            let subjectDetailViewController = MySubjectDetailViewController.instantiateFromAppStoryboard(appStoryboard: .Profile)
            subjectDetailViewController.mySubject = subjectPagination.subjects[indexPath.row]
            navigationController?.pushViewController(subjectDetailViewController, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let location = UITableViewRowAction(style: .normal, title: "Location".localized) { action, index in
            DispatchQueue.main.async {
                self.showLocationScreenForIndex(index.row)
            }
        }
        
        location.backgroundColor = UIColor.init(red: 0.97647, green: 0.5098, blue: 0, alpha: 1)

        let schedule = UITableViewRowAction(style: .normal, title: "Schedule".localized) { action, index in
            DispatchQueue.main.async {
                self.showSchedulesScreenForIndex(index.row)
            }
        }
        
        schedule.backgroundColor = UIColor.lightGray

        let edit = UITableViewRowAction(style: .normal, title: "Edit".localized) { action, index in
            DispatchQueue.main.async {
                self.showEditScreenForIndex(index.row)
            }
        }
        
        edit.backgroundColor = UIColor.init(red: 0.3843, green: 0.7882, blue: 0.4156, alpha: 1)
        
        let delete = UITableViewRowAction(style: .destructive, title: "Delete".localized) { action, index in
            DispatchQueue.main.async {
                self.deleteSubjectAtIndex(index.row)
            }
        }
        delete.backgroundColor = UIColor.init(red: 0.8941, green: 0.3764, blue: 0.3607, alpha: 1)
        
        return [delete, edit, schedule, location]
        
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        if indexPath.row >= subjectPagination.subjects.count {
            return false
        }
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
    }

}

extension MySubjectListingViewController {
    
    func fetchMySubjects() {
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }
        guard let userID = UserStore.shared.userID else {
            return
        }
        isFetchingData = true
        if subjectPagination.paginationType != .old {
            self.subjectPagination.currentPageNumber = 1
            if isFirstTime == true {
                Utilities.showHUD(forView: self.view, excludeViews: [])
            } else {
                UIApplication.shared.isNetworkActivityIndicatorVisible = true
            }
            
        } else {
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
        }
        subjectObserver = ApiManager.shared.apiService.fetchMySubjectListingAtPage(subjectPagination.currentPageNumber, forUserID: userID)
        let subjectDisposable = subjectObserver.subscribe(onNext: {(paginationObject) in
            self.isFetchingData = false
            
            self.subjectPagination.appendDataFromObject(paginationObject)
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if self.subjectPagination.paginationType != .old && self.isFirstTime == true {
                    self.isFirstTime = false
                } else {
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                }
                self.showDataAppropritely()
            })
        }, onError: {(error) in
            self.isFetchingData = false
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
            })
        })
        subjectDisposable.disposed(by: disposableBag)
    }
    
    func changeSubjectStatusForId(_ subjectID: String, atIndex index: Int, isOn: Bool) {
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }
        
        guard let userID = UserStore.shared.userID else {
            return
        }
        
        Utilities.showHUD(forView: self.view, excludeViews: [])
        
        let subjectStatusObserver = ApiManager.shared.apiService.changeStatusOfSubject(subjectID, forUser: String(userID))
        let subjectStatustDisposable = subjectStatusObserver.subscribe(onNext: {(message) in
            Utilities.hideHUD(forView: self.view)
            self.subjectPagination.subjects[index].subject.status = isOn ? "1" : "0"
            DispatchQueue.main.async {
                self.tableView.reloadRows(at: [IndexPath.init(row: index, section: 0)], with: .none)
            }
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.showSimpleAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        })
        subjectStatustDisposable.disposed(by: disposableBag)
    }
    
    func deleteSubjectWithID(_ subjectID: String) {
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }
        
        guard let userID = UserStore.shared.userID else {
            return
        }
        
        Utilities.showHUD(forView: self.view, excludeViews: [])
        
        let deleteSubjectObserver = ApiManager.shared.apiService.deleteSubjectWithID(subjectID, forUser: String(userID))
        let deleteSubjectDisposable = deleteSubjectObserver.subscribe(onNext: {(message) in
            Utilities.hideHUD(forView: self.view)
            NotificationCenter.default.post(name: .subjectsUpdated, object: nil, userInfo:nil)
            
            DispatchQueue.main.async {
                self.subjectPagination.paginationType = .new
                self.fetchMySubjects()

                let alert = UIAlertController(title: AppName, message: message, preferredStyle: .alert)
                
                let okAction = UIAlertAction(title: AlertButton.ok.description(), style: .default, handler: nil)
                alert.addAction(okAction)
                self.present(alert, animated: true, completion: nil)
            }
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.showSimpleAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        })
        deleteSubjectDisposable.disposed(by: disposableBag)
    }

    
}

extension MySubjectListingViewController: MySubjectTableViewCellDelegate {
    
    func activeSwitchTapped(_ sender: UISwitch) {
        let subjectDetail = subjectPagination.subjects[sender.tag]
        changeSubjectStatusForId(String(subjectDetail.subject.id), atIndex: sender.tag, isOn: sender.isOn)
    }
}
