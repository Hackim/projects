//
//  AddSubjectViewController.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 17/06/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit
import RxSwift
import GooglePlaces
import Unbox
import AMPopTip

class AddSubjectViewController: BaseViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var backButton: UIButton!
    
    var subject: SubjectDetailedInfo!
    var mySubject: MySubject!
    var disposableBag = DisposeBag()
    var shouldHideBackButton = false
    var activeTextField: UITextField!
    var selectedSubject: Subject!
    var selectedLevel: Level!
    var selectedQualification: Qualification!
    var selectedExperience: Experience!
    var selectedTeachingSince: String!
    var isSetLocationSelected = false
    var errorMessagesDict = [Int: String]()
    var isSubjectActive = true
    var shedules = ["MON": [String](), "TUE": [String](), "WED": [String](), "THU": [String](), "FRI": [String](), "SAT": [String](), "SUN": [String]()]
    var schedulesCount = 0
    var preferredTimeSlot: String!
    var isAddSubject = true
    var popTip: PopTip!
    var countryCurrency = ""
    var sessionCurrencyString: NSMutableAttributedString!
    var monthCurrencyString: NSMutableAttributedString!
    var labelFields = ["SUBJECT".localized, "", "LEVEL".localized, "QUALIFICATION".localized, "TEACHING SINCE".localized, "RATE PER SESSION (IN YOUR CURRENCY)".localized, "RATE PER MONTH (IN YOUR CURRENCY)".localized, "PREFERRED LOCATION".localized, "ADDRESS".localized, "DISTANCE (IN METRES)".localized, "PREFERRED TIME".localized, ""]
    
    var helperTexts: [String]!
    var countries: [Country]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView()
        countries = Utilities.getCountries() ?? [Country]()
        countryCurrency = Utilities.getCurrentCountryCurrency(nil).uppercased()
        configureCountryCurrencyStrings()
        if let userSubject = mySubject {
            subject = userSubject.subject
            selectedSubject = Subject.init(id: Int(subject.subjectID)!, name: subject.name)
            selectedLevel = Level.init(id: Int(subject.levelID)!, name: subject.level)
            selectedQualification = Qualification.init(id: subject.qualificationID, name: subject.qualification)
            selectedTeachingSince = subject.teachingSince
            selectedExperience = Experience.init(name: subject.experience)
            isSetLocationSelected = false
            if let abilityToTravel = subject.abilityToTravel {
                isSetLocationSelected = abilityToTravel != "ANYWHERE"
            }
            isSubjectActive = subject.status == "1" ? true : false
            shedules = userSubject.availability
            schedulesCount = userSubject.totalAvailability
            isAddSubject = false
            titleLabel.text = "Edit Subject".localized
        }
        helperTexts = tutorSubjectTexts
        if Utilities.isUserTypeStudent() {
            helperTexts = studentSubjectTexts
            labelFields = ["SUBJECT".localized, "", "LEVEL".localized, "MINIMUM QUALIFICATION".localized, "MINIMUM EXPERIENCE".localized, "BUDGET PER SESSION (IN YOUR CURRENCY)".localized, "BUDGET PER MONTH (IN YOUR CURRENCY)".localized, "PREFERRED LOCATION".localized, "ADDRESS".localized, "DISTANCE (IN METRES)".localized, "PREFERRED TIME".localized, ""]
        }
        
        if schedulesCount == 1 {
            preferredTimeSlot = "\(schedulesCount)" + " slot selected".localized
        } else {
            preferredTimeSlot = "\(schedulesCount)" + " slots selected".localized
        }
        
        if subject == nil {
            subject = SubjectDetailedInfo()
            titleLabel.text = "Add Subject".localized
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        if shouldHideBackButton {
//            self.backButton.isHidden = true
        }
    }

    func configureCountryCurrencyStrings() {
        sessionCurrencyString = NSMutableAttributedString()
        sessionCurrencyString.append(NSAttributedString(string: "RATE PER SESSION".localized,
                                                   attributes: [.font: UIFont(name: "Montserrat-Regular", size: 15.0)!,
                                                                .foregroundColor : themeBlueColor]))
        sessionCurrencyString.append(NSAttributedString(string: " (\(countryCurrency))",
            attributes: [.font: UIFont(name: "Montserrat-Regular", size: 15.0)!,
                         .foregroundColor : themeBlueColor]))

        monthCurrencyString = NSMutableAttributedString()
        monthCurrencyString.append(NSAttributedString(string: "RATE PER MONTH".localized,
                                                   attributes: [.font: UIFont(name: "Montserrat-Regular", size: 15.0)!,
                                                                .foregroundColor : themeBlueColor]))
        monthCurrencyString.append(NSAttributedString(string: " (\(countryCurrency))",
            attributes: [.font: UIFont(name: "Montserrat-Regular", size: 15.0)!,
                         .foregroundColor : themeBlueColor]))

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if let existingPopTip = popTip {
            existingPopTip.hide()
        }
    }
    
    func configureTextField(_ customTextField: TueetorTextField) {
        customTextField.delegate = self
        customTextField.keyboardType = .asciiCapable
        customTextField.isUserInteractionEnabled = true
        customTextField.autocapitalizationType = .none
        customTextField.inputView = nil
        switch customTextField.tag {
        case AddSubjectRowType.subject.rawValue,
             AddSubjectRowType.level.rawValue,
             AddSubjectRowType.qualification.rawValue,
             AddSubjectRowType.teachingSince.rawValue,
             AddSubjectRowType.address.rawValue:
            customTextField.isUserInteractionEnabled = false
        case AddSubjectRowType.sessionRate.rawValue,
             AddSubjectRowType.monthlyRate.rawValue,
             AddSubjectRowType.distance.rawValue:
            customTextField.keyboardType = .numberPad
        default:
            customTextField.isUserInteractionEnabled = true
        }
    }

    func updateDataIntoTextField(_ customTextField: TueetorTextField, fromCell: Bool) {
        switch customTextField.tag {
        case AddSubjectRowType.subject.rawValue:
            fromCell ? (customTextField.text = subject.name) : (subject.name = customTextField.text!)
        case AddSubjectRowType.level.rawValue:
            fromCell ? (customTextField.text = subject.level) : (subject.level = customTextField.text!)
        case AddSubjectRowType.qualification.rawValue:
            fromCell ? (customTextField.text = subject.qualification) : (subject.qualification = customTextField.text!)
        case AddSubjectRowType.teachingSince.rawValue:
            if Utilities.isUserTypeStudent() {
                fromCell ? (customTextField.text = subject.experience) : (subject.experience = customTextField.text!)
            } else {
                fromCell ? (customTextField.text = subject.teachingSince) : (subject.teachingSince = customTextField.text!)
            }
        case AddSubjectRowType.sessionRate.rawValue:
            fromCell ? (customTextField.text = subject.pricePerSession) : (subject.pricePerSession = customTextField.text!)
        case AddSubjectRowType.monthlyRate.rawValue:
            fromCell ? (customTextField.text = subject.pricePerMonth) : (subject.pricePerMonth = customTextField.text!)
        case AddSubjectRowType.distance.rawValue:
            fromCell ? (customTextField.text = subject.radius) : (subject.radius = customTextField.text!)
        case AddSubjectRowType.address.rawValue:
            fromCell ? (customTextField.text = subject.address) : (subject.address = customTextField.text!)
        case AddSubjectRowType.preferredTime.rawValue:
            fromCell ? (customTextField.text = preferredTimeSlot) : print("ignore")
        default:
            break
        }
    }
    
    func isDataValid() -> (isValid: Bool, message: String) {
        var errorMessages: [String] = []
        errorMessagesDict = [Int: String]()
        for index in 0...labelFields.count {
            switch index {
            case AddSubjectRowType.subject.rawValue:
                let message = TextFieldInfoTableViewCell.validateSubject(subject: selectedSubject)
                errorMessagesDict[index] = message
                errorMessages.append(message)
            case AddSubjectRowType.level.rawValue:
                let message = TextFieldInfoTableViewCell.validateLevel(level: selectedLevel)
                errorMessagesDict[index] = message
                errorMessages.append(message)
            case AddSubjectRowType.qualification.rawValue:
                let message = TextFieldInfoTableViewCell.validateQualification(qualification: selectedQualification)
                errorMessagesDict[index] = message
                errorMessages.append(message)
            case AddSubjectRowType.teachingSince.rawValue:
                var message = ""
                if Utilities.isUserTypeStudent() {
                    message = TextFieldInfoTableViewCell.validateExperience(experience: selectedExperience)
                } else {
                    message = TextFieldInfoTableViewCell.validateTeachingSince(teachingSince: selectedTeachingSince)

                }
                errorMessagesDict[index] = message
                errorMessages.append(message)
            case AddSubjectRowType.sessionRate.rawValue:
                let message = TextFieldInfoTableViewCell.validateSessionRate(sessionRate: subject.pricePerSession)
                errorMessagesDict[index] = message
                errorMessages.append(message)
            case AddSubjectRowType.monthlyRate.rawValue:
                let message = TextFieldInfoTableViewCell.validateMonthRate(monthRate: subject.pricePerMonth)
                errorMessagesDict[index] = message
                errorMessages.append(message)
            case AddSubjectRowType.address.rawValue:
                if isSetLocationSelected {
                    let message = TextFieldInfoTableViewCell.validateAddress(address: subject.address)
                    errorMessagesDict[index] = message
                    errorMessages.append(message)
                }
            case AddSubjectRowType.distance.rawValue:
                if isSetLocationSelected {
                    let message = TextFieldInfoTableViewCell.validateDistance(distance: subject.radius)
                    errorMessagesDict[index] = message
                    errorMessages.append(message)
                }
            case AddSubjectRowType.preferredTime.rawValue:
                if schedulesCount == 0 {
                    let message = ValidationErrorMessage.schedule.description()
                    errorMessagesDict[index] = message
                    errorMessages.append(message)
                }
            default:
                break
            }
        }
        let filteredErrorMessages = errorMessages.filter { (message) -> Bool in
            message.count > 0
        }
        tableView.reloadData()
        let isValidData = filteredErrorMessages.count == 0
        return (isValid: isValidData, message: isValidData ? "" : filteredErrorMessages[0])
    }


    func showPopTipForButton(_ button: UIButton) {
        
        if let existingPopTip = self.popTip {
            existingPopTip.hide(forced: true)
            guard popTip.tag != button.tag else {
                self.popTip = nil
                return
            }
            self.popTip = PopTip()
            self.popTip.tag = button.tag
            self.popTip.bubbleColor = themeBlueColor
            self.popTip.shouldDismissOnTapOutside = true
            self.popTip.shouldDismissOnTap = true
            self.popTip.textColor = UIColor.white
            self.popTip.font = UIFont(name: "Montserrat-Light", size: 13.0)!
            let fromRect = button.convert(button.frame, to: self.view)
            self.popTip.show(text: helperTexts[button.tag], direction: .down, maxWidth: 250, in: self.view, from: fromRect)
        } else {
            self.popTip = PopTip()
            self.popTip.bubbleColor = themeBlueColor
            self.popTip.shouldDismissOnTapOutside = true
            self.popTip.shouldDismissOnTap = true
            self.popTip.textColor = UIColor.white
            self.popTip.font = UIFont(name: "Montserrat-Light", size: 13.0)!
            let fromRect = button.convert(button.frame, to: self.view)
            self.popTip.show(text: helperTexts[button.tag], direction: .down, maxWidth: 250, in: self.view, from: fromRect)
        }

    }
    
}

extension AddSubjectViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return labelFields.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == labelFields.count {
            let cell = tableView.dequeueReusableCell(withIdentifier: AddSubjectButtonTableViewCell.cellIdentifier(), for: indexPath) as! AddSubjectButtonTableViewCell
            
            cell.addSubjectButton.setTitle(isAddSubject ? "Add Subject".localized : "Update".localized, for: .normal)
            cell.delegate = self
            return cell
            
        } else if indexPath.row == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: SuggestSubjectTableViewCell.cellIdentifier(), for: indexPath) as! SuggestSubjectTableViewCell
            cell.delegate = self
            return cell
        } else if indexPath.row == AddSubjectRowType.location.rawValue {
            let cell = tableView.dequeueReusableCell(withIdentifier: SubjectPreferredLocationTableViewCell.cellIdentifier(), for: indexPath) as! SubjectPreferredLocationTableViewCell
            cell.configureCellWithSelectionState(isSetLocationSelected)
            cell.infoButton.tag = indexPath.row
            cell.delegate = self
            return cell
        } else if indexPath.row == AddSubjectRowType.active.rawValue {
            let cell = tableView.dequeueReusableCell(withIdentifier: SubjectActivationTableViewCell.cellIdentifier(), for: indexPath) as! SubjectActivationTableViewCell
            cell.configureCellWithSelectionState(isSubjectActive)
            cell.delegate = self
            cell.infoButton.tag = indexPath.row
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: TextFieldInfoTableViewCell.cellIdentifier(), for: indexPath) as! TextFieldInfoTableViewCell
        cell.delegate = self
        let errorMessage = errorMessagesDict[indexPath.row] ?? ""
        cell.errorLabel.text = errorMessage
        if indexPath.row == 5 {
            cell.configureCellWithAttributedString(sessionCurrencyString, atIndex: indexPath.row)
        } else if indexPath.row == 6 {
            cell.configureCellWithAttributedString(monthCurrencyString, atIndex: indexPath.row)
        } else if indexPath.row != 1 && indexPath.row != 12 {
            cell.configureCellForRowAtIndex(indexPath.row, withText: labelFields[indexPath.row])
        }
        configureTextField(cell.customTextField)
        updateDataIntoTextField(cell.customTextField, fromCell: true)
        cell.extraIconButton.isHidden = false
        cell.infoButton.isHidden = false
        cell.extraIconButton.setImage(UIImage(named: "right_arrow"), for: .normal)
        cell.extraIconButton.isUserInteractionEnabled = false
        switch indexPath.row {
            case AddSubjectRowType.subject.rawValue,
                 AddSubjectRowType.level.rawValue,
                 AddSubjectRowType.qualification.rawValue,
                 AddSubjectRowType.teachingSince.rawValue:
            cell.customTextField.isUserInteractionEnabled = false
        case AddSubjectRowType.address.rawValue:
            cell.customTextField.isUserInteractionEnabled = false
            cell.extraIconButton.isUserInteractionEnabled = true
            cell.infoButton.isHidden = true
            cell.extraIconButton.setImage(UIImage(named: "location_circle"), for: .normal)
        case AddSubjectRowType.preferredTime.rawValue:
            cell.customTextField.isUserInteractionEnabled = false
            cell.extraIconButton.setImage(UIImage(named: "my_calender"), for: .normal)
        case AddSubjectRowType.monthlyRate.rawValue,
             AddSubjectRowType.sessionRate.rawValue:
            cell.extraIconButton.isHidden = true
        case AddSubjectRowType.distance.rawValue:
            cell.extraIconButton.isHidden = true
            cell.infoButton.isHidden = true
        default:
            cell.customTextField.isUserInteractionEnabled = true
        }
        return cell

    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == labelFields.count {
            return AddSubjectButtonTableViewCell.cellHeight()
        } else if indexPath.row == 1 {
            return SuggestSubjectTableViewCell.cellHeight()
        }else if indexPath.row == AddSubjectRowType.location.rawValue {
            return SubjectPreferredLocationTableViewCell.cellHeight()
        } else if indexPath.row == AddSubjectRowType.active.rawValue {
            return SubjectActivationTableViewCell.cellHeight()
        } else {
            if !isSetLocationSelected, (indexPath.row == AddSubjectRowType.address.rawValue || indexPath.row == AddSubjectRowType.distance.rawValue) {
                return 0
            }
            
        
            tableView.estimatedRowHeight = 63.0
            return UITableViewAutomaticDimension
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case AddSubjectRowType.subject.rawValue:
            let filterSelectionViewController = FilterCateogrySelectionViewController.instantiateFromAppStoryboard(appStoryboard: .Find)
            filterSelectionViewController.delegate = self
            filterSelectionViewController.screenType = .subject
            if let subject = selectedSubject {
                filterSelectionViewController.selectedSubjectsArray = [subject]
            }
            navigationController?.pushViewController(filterSelectionViewController, animated: true)
        case AddSubjectRowType.level.rawValue:
            let filterSelectionViewController = FilterCateogrySelectionViewController.instantiateFromAppStoryboard(appStoryboard: .Find)
            filterSelectionViewController.screenType = .level
            filterSelectionViewController.delegate = self
            if let level = selectedLevel {
                filterSelectionViewController.selectedLevel = level
            }
            navigationController?.pushViewController(filterSelectionViewController, animated: true)
        case AddSubjectRowType.qualification.rawValue:
            let filterSelectionViewController = FilterCateogrySelectionViewController.instantiateFromAppStoryboard(appStoryboard: .Find)
            filterSelectionViewController.delegate = self
            filterSelectionViewController.screenType = .qualification
            if let qualification = selectedQualification {
                filterSelectionViewController.selectedQualification = qualification
            }
            navigationController?.pushViewController(filterSelectionViewController, animated: true)

        case AddSubjectRowType.teachingSince.rawValue:
            let filterSelectionViewController = FilterCateogrySelectionViewController.instantiateFromAppStoryboard(appStoryboard: .Find)
            filterSelectionViewController.delegate = self
            if Utilities.isUserTypeStudent() {
                filterSelectionViewController.screenType = .experience
                if let experience = selectedExperience {
                    filterSelectionViewController.selectedExperience = experience
                }
            } else {
                filterSelectionViewController.screenType = .teachingSince
                if let year = selectedTeachingSince {
                    filterSelectionViewController.selectedYear = year
                }
            }
            navigationController?.pushViewController(filterSelectionViewController, animated: true)
        case AddSubjectRowType.address.rawValue:
            let autocompleteController = GMSAutocompleteViewController()
            autocompleteController.delegate = self
            present(autocompleteController, animated: true, completion: nil)
        case AddSubjectRowType.preferredTime.rawValue:
            let availabilityViewController = MyAvailabilityViewController.instantiateFromAppStoryboard(appStoryboard: .Profile)
            availabilityViewController.delegate = self
            availabilityViewController.schedules = self.shedules
            navigationController?.pushViewController(availabilityViewController, animated: true)
        default:
            print("Nothing")
        }
    }

    
}

extension AddSubjectViewController: FilterCategorySelectionViewControllerDelegate {
    
    func multipleSubjectsSelected(_ selectedSubs: [Subject]) {
        
    }
    
    func subjectSelected(_ selectedSub: Subject?) {
        if selectedSub == nil {
            selectedSubject = nil
            subject.name = ""
        } else {
            selectedSubject = selectedSub
            subject.name = selectedSubject.name
        }
        let cell = tableView.cellForRow(at: IndexPath.init(row: AddSubjectRowType.subject.rawValue, section: 0)) as? TextFieldInfoTableViewCell
        let message = TextFieldInfoTableViewCell.validateSubject(subject: selectedSubject)
        errorMessagesDict[AddSubjectRowType.subject.rawValue] = message
        cell?.errorLabel.text = message
        tableView.reloadData()
    }
    
    func levelSelected(_ selectedLev: Level?) {
        if let _ = selectedLev {
            selectedLevel = selectedLev
            subject.level = selectedLevel.name
        } else {
            selectedLevel = nil
            subject.level = ""
        }
        let cell = tableView.cellForRow(at: IndexPath.init(row: AddSubjectRowType.level.rawValue, section: 0)) as? TextFieldInfoTableViewCell
        let message = TextFieldInfoTableViewCell.validateLevel(level: selectedLevel)
        errorMessagesDict[AddSubjectRowType.level.rawValue] = message
        cell?.errorLabel.text = message

        tableView.reloadData()
    }
    
    func qualificationSelected(_ selectedQuali: Qualification?) {
        if let _ = selectedQuali {
            selectedQualification = selectedQuali
            subject.qualification = selectedQualification.name
        } else {
            selectedQualification = nil
            subject.qualification = ""
        }
        let cell = tableView.cellForRow(at: IndexPath.init(row: AddSubjectRowType.qualification.rawValue, section: 0)) as? TextFieldInfoTableViewCell
        let message = TextFieldInfoTableViewCell.validateQualification(qualification: selectedQualification)
        errorMessagesDict[AddSubjectRowType.qualification.rawValue] = message
        cell?.errorLabel.text = message

        tableView.reloadData()
    }
    
    func teachingSinceSelected(_ selectedYear: String) {
        selectedTeachingSince = selectedYear
        subject.teachingSince = selectedTeachingSince
        let cell = tableView.cellForRow(at: IndexPath.init(row: AddSubjectRowType.teachingSince.rawValue, section: 0)) as? TextFieldInfoTableViewCell
        let message = TextFieldInfoTableViewCell.validateTeachingSince(teachingSince: selectedTeachingSince)
        errorMessagesDict[AddSubjectRowType.teachingSince.rawValue] = message
        cell?.errorLabel.text = message

        tableView.reloadData()
    }

    func experienceSelected(_ selectedExp: Experience?) {
        if let selectedEp = selectedExp {
            if selectedEp.name == "None".localized {
                selectedExperience = nil
                subject.experience = ""
            } else {
                selectedExperience = selectedEp
                subject.experience = selectedEp.name
            }
        } else {
            selectedExperience = nil
            subject.experience = ""
        }

        let cell = tableView.cellForRow(at: IndexPath.init(row: AddSubjectRowType.teachingSince.rawValue, section: 0)) as? TextFieldInfoTableViewCell
        let message = TextFieldInfoTableViewCell.validateExperience(experience: selectedExperience)
        errorMessagesDict[AddSubjectRowType.teachingSince.rawValue] = message
        cell?.errorLabel.text = message
        tableView.reloadData()
    }
}

extension AddSubjectViewController: SubjectPreferredLocationTableViewCellDelegate {
    
    func setLocationSelected(_ isSelected: Bool) {
        isSetLocationSelected = isSelected
        tableView.beginUpdates()
        tableView.endUpdates()
    }
    
    func locationInfoButtonTapped(_ button: UIButton) {
        showPopTipForButton(button)
    }

}

extension AddSubjectViewController: SubjectActivationTableViewCellDelegate {

    func activeButtonIsSetOn(_ isOn: Bool) {
        isSubjectActive = isOn
    }
    
    func infoButtonTappedForActivationCell(_ button: UIButton) {
        showPopTipForButton(button)
    }
    
}

extension AddSubjectViewController: TextFieldInfoTableViewCellDelegate {
    
    func infoButtonTapped(_ button: UIButton) {
        showPopTipForButton(button)
    }
    
    func extraInfoButtonTapped(_ tag: Int) {
        if tag == AddSubjectRowType.address.rawValue {
            let placesClient = GMSPlacesClient.init()
            
            
            placesClient.currentPlace(callback: { (placeLikelihoodList, error) -> Void in
                if let error = error {
                    print("Pick Place error: \(error.localizedDescription)")
                    return
                }
                
                if let placeLikelihoodList = placeLikelihoodList {
                    for likelihood in placeLikelihoodList.likelihoods {
                        let place = likelihood.place
                        self.subject.latitude = String(place.coordinate.latitude)
                        self.subject.logitude = String(place.coordinate.longitude)
                        self.subject.address = place.formattedAddress ?? place.name
                        if let addressLines = place.addressComponents {
                            // Populate all of the address fields we can find.
                            for field in addressLines {
                                switch field.type {
                                case kGMSPlaceTypeCountry:
                                    let index = self.countries.index { (country) -> Bool in
                                        country.name == field.name
                                    }
                                    if let _index = index {
                                        self.countryCurrency = Utilities.getCurrentCountryCurrency(String(self.countries[_index].id)).uppercased()
                                        self.configureCountryCurrencyStrings()
                                    }
                                default:
                                    print("ignore")
                                }
                            }
                        }

                        DispatchQueue.main.async {
                            let _ = self.isDataValid()
                        }
                    }
                }
            })
            

            print("Correct")
        }
    }
}

extension AddSubjectViewController: GMSAutocompleteViewControllerDelegate {
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        self.subject.latitude = String(place.coordinate.latitude)
        self.subject.logitude = String(place.coordinate.longitude)
        self.subject.address = place.formattedAddress ?? place.name
        if let addressLines = place.addressComponents {
            // Populate all of the address fields we can find.
            for field in addressLines {
                switch field.type {
                case kGMSPlaceTypeCountry:
                    let index = countries.index { (country) -> Bool in
                        country.name == field.name
                    }
                    if let _index = index {
                        countryCurrency = Utilities.getCurrentCountryCurrency(String(countries[_index].id))
                        configureCountryCurrencyStrings()
                    }
                default:
                    print("ignore")
                }
            }
        }
        let _ = isDataValid()
//        tableView.reloadData()
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}

extension AddSubjectViewController: MyAvailabilityViewControllerDelegate {
    
    func slotsSelected(slots: [String : [String]], count: Int) {
        shedules = slots
        schedulesCount = count
        
        if schedulesCount == 1 {
            preferredTimeSlot = "\(schedulesCount)" + " slot selected".localized
        } else {
            preferredTimeSlot = "\(schedulesCount)" + " slots selected".localized
        }
        
        let cell = tableView.cellForRow(at: IndexPath.init(row: AddSubjectRowType.preferredTime.rawValue, section: 0)) as? TextFieldInfoTableViewCell
        let message = schedulesCount == 0 ? ValidationErrorMessage.schedule.description() : ""
        errorMessagesDict[AddSubjectRowType.preferredTime.rawValue] = message
        cell?.errorLabel.text = message
        
        self.tableView.reloadData()
    }
}

extension AddSubjectViewController: AddSubjectButtonTableViewCellDelegate {
    
    func addSubjectButtonTapped() {
        self.view.endEditing(true)
        let validationInformation = isDataValid()
        guard validationInformation.isValid else {
            self.showSimpleAlertWithMessage(validationInformation.message)
            return
        }
        if isAddSubject {
            addSubject()
        } else {
            editSubject()
        }
    }
    
}

extension AddSubjectViewController: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        activeTextField = textField as! TueetorTextField
        if let existingPopTip = popTip {
            existingPopTip.hide()
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        updateDataIntoTextField(textField as! TueetorTextField, fromCell: false)
        let currentIndexPath = IndexPath.init(row: textField.tag, section: 0)
        switch textField.tag {
        case AddSubjectRowType.sessionRate.rawValue:
            let cell = tableView.cellForRow(at: currentIndexPath) as? TextFieldInfoTableViewCell
            let message = TextFieldInfoTableViewCell.validateSessionRate(sessionRate: textField.text ?? "")
            errorMessagesDict[textField.tag] = message
            cell?.errorLabel.text = message

        case AddSubjectRowType.monthlyRate.rawValue:
            let cell = tableView.cellForRow(at: currentIndexPath) as? TextFieldInfoTableViewCell
            let message = TextFieldInfoTableViewCell.validateMonthRate(monthRate: textField.text ?? "")
            errorMessagesDict[textField.tag] = message
            cell?.errorLabel.text = message

        case AddSubjectRowType.distance.rawValue:
            let cell = tableView.cellForRow(at: currentIndexPath) as? TextFieldInfoTableViewCell
            let message = TextFieldInfoTableViewCell.validateDistance(distance: textField.text ?? "")
            errorMessagesDict[textField.tag] = message
            cell?.errorLabel.text = message
        default:
            break
        }
        tableView.beginUpdates()
        tableView.endUpdates()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (textField.text?.isEmpty)! && string == " " {
            return false
        }
        let char = string.cString(using: String.Encoding.utf8)!
        let isBackSpace = strcmp(char, "\\b")
        if (isBackSpace == -92) {
            return true
        }
        return true
    }

}

extension AddSubjectViewController: UIScrollViewDelegate {
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        if let existingPopTip = popTip {
            existingPopTip.hide()
        }
    }
}


extension AddSubjectViewController: SuggestSubjectTableViewCellDelegate {
    
    func showSuggestSubjectAlert() {
        let alertController = UIAlertController(title: AppName,
                                                message: AlertMessage.suggestSubject.description(),
                                                preferredStyle: .alert)
        
        
        let submitAction = UIAlertAction(title: AlertButton.submit.description(),
                                         style: .default) { [weak alertController] _ in
                                            guard let alertController = alertController, let textField = alertController.textFields?.first else { return }
                                            DispatchQueue.main.async {
                                                print(textField.text!)
                                                self.suggestSubjectWithName(textField.text!)
                                            }
        }
        submitAction.isEnabled = false
        alertController.addAction(submitAction)
        
        alertController.addTextField { textField in
            textField.placeholder = "Subject Name".localized
            textField.tag = 40
            
            NotificationCenter.default.addObserver(forName: NSNotification.Name.UITextFieldTextDidChange, object: textField, queue: OperationQueue.main, using: { (notification) in
                guard textField.tag == 40 else {
                    return
                }
                submitAction.isEnabled = !textField.text!.isEmpty
            })
        }
        
        let cancelAction = UIAlertAction(title: AlertButton.cancel.description(), style: .cancel, handler: nil)
        alertController.addAction(cancelAction)
        present(alertController, animated: true, completion: nil)

    }
    
}
extension AddSubjectViewController {
    
    func addSubject() {
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }
        
        guard let userID = UserStore.shared.userID else {
            return
        }
        
        var subjectDict = ["user_id": String(userID),
                           "subject_id": String(selectedSubject.id),
                           "level_id": String(selectedLevel.id),
                           "qualification_id": selectedQualification.id,
                           "persession": subject.pricePerSession,
                           "permonth": subject.pricePerMonth,
                           "status": isSubjectActive ? "1" : "0",
                           "address": isSetLocationSelected ? subject.address : "",
                           "lattitude": isSetLocationSelected ? subject.latitude : "",
                           "longitude": isSetLocationSelected ? subject.logitude : "",
                           "will_to_teach": isSetLocationSelected ? "L" : "A",
                           "distance": isSetLocationSelected ? subject.radius : "",
                           "MON": shedules["MON"]!,
                           "TUE": shedules["TUE"]!,
                           "WED": shedules["WED"]!,
                           "THU": shedules["THU"]!,
                           "FRI": shedules["FRI"]!,
                           "SAT": shedules["SAT"]!,
                           "SUN": shedules["SUN"]!
            ] as [String : AnyObject]
        if Utilities.isUserTypeStudent() {
            subjectDict["experience"] = subject.experience as AnyObject
        } else {
            subjectDict["teachingsince"] = subject.teachingSince as AnyObject
        }
        print(subjectDict)
        Utilities.showHUD(forView: self.view, excludeViews: [])
        
        let addSubjectObserver = ApiManager.shared.apiService.addSubject(subjectDict)
        let addSubjectDisposable = addSubjectObserver.subscribe(onNext: {(message) in
            Utilities.hideHUD(forView: self.view)
            NotificationCenter.default.post(name: .subjectsUpdated, object: nil, userInfo:nil)

            DispatchQueue.main.async {
                if self.shouldHideBackButton {
                    self.navigateToDashboardViewController()
                } else {
                    let alert = UIAlertController(title: AppName, message: AlertMessage.addSubjectSuccess.description(), preferredStyle: .alert)
                    
                    let okAction = UIAlertAction(title: AlertButton.ok.description(), style: .default, handler: { (action) in
                        DispatchQueue.main.async {
                            self.navigationController?.popViewController(animated: true)
                        }
                    })
                    alert.addAction(okAction)
                    
                    let findAction = UIAlertAction(title: AlertButton.findNow.description(), style: .default, handler: { (action) in
                        DispatchQueue.main.async {
                            self.navigateToFindScreen()
                        }
                    })
                    alert.addAction(findAction)

                    self.present(alert, animated: true, completion: nil)
                }
            }
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.showSimpleAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        })
        addSubjectDisposable.disposed(by: disposableBag)
    }
    
    func editSubject() {
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }
        
        guard let userID = UserStore.shared.userID else {
            return
        }
        
        var subjectDict = ["id": mySubject.subject.id,
                           "user_id": String(userID),
                           "subject_id": String(selectedSubject.id),
                           "level_id": String(selectedLevel.id),
                           "qualification_id": selectedQualification.id,
                           "persession": subject.pricePerSession,
                           "permonth": subject.pricePerMonth,
                           "status": isSubjectActive ? "1" : "0",
                           "address": isSetLocationSelected ? subject.address : "",
                           "lattitude": isSetLocationSelected ? subject.latitude : "",
                           "longitude": isSetLocationSelected ? subject.logitude : "",
                           "will_to_teach": isSetLocationSelected ? "L" : "A",
                           "distance": isSetLocationSelected ? subject.radius : "",
                           "MON": shedules["MON"]!,
                           "TUE": shedules["TUE"]!,
                           "WED": shedules["WED"]!,
                           "THU": shedules["THU"]!,
                           "FRI": shedules["FRI"]!,
                           "SAT": shedules["SAT"]!,
                           "SUN": shedules["SUN"]!
            ] as [String : AnyObject]
        if Utilities.isUserTypeStudent() {
            subjectDict["experience"] = subject.experience as AnyObject
        } else {
            subjectDict["teachingsince"] = subject.teachingSince as AnyObject
        }

        print(subjectDict)
        Utilities.showHUD(forView: self.view, excludeViews: [])
        
        let editSubjectObserver = ApiManager.shared.apiService.editSubject(subjectDict)
        let editSubjectDisposable = editSubjectObserver.subscribe(onNext: {(message) in
            Utilities.hideHUD(forView: self.view)
            NotificationCenter.default.post(name: .subjectsUpdated, object: nil, userInfo:nil)
            
            DispatchQueue.main.async {
                let alert = UIAlertController(title: AppName, message: message, preferredStyle: .alert)
                
                let okAction = UIAlertAction(title: AlertButton.ok.description(), style: .default, handler: { (action) in
                    DispatchQueue.main.async {
                        let viewControllers: [UIViewController] = self.navigationController!.viewControllers
                        for aViewController in viewControllers {
                            if aViewController is MySubjectListingViewController {                                self.navigationController!.popToViewController(aViewController, animated: true)
                            }
                        }
                    }
                })
                alert.addAction(okAction)
                self.present(alert, animated: true, completion: nil)
            }
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.showSimpleAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        })
        editSubjectDisposable.disposed(by: disposableBag)
    }

    func suggestSubjectWithName(_ name: String) {
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }
        
        guard let userID = UserStore.shared.userID else {
            return
        }
        let countryID = Utilities.getCurrentCountryCode()

        Utilities.showHUD(forView: self.view, excludeViews: [])
        
        let suggestSubjectObserver = ApiManager.shared.apiService.suggestSubject(name: name, userID: String(userID), countryID: countryID)
        let suggestSubjectDisposable = suggestSubjectObserver.subscribe(onNext: {(message) in
            Utilities.hideHUD(forView: self.view)
            DispatchQueue.main.async {
                self.showSimpleAlertWithMessage(message)
            }
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.showSimpleAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        })
        suggestSubjectDisposable.disposed(by: disposableBag)
    }
    
}
