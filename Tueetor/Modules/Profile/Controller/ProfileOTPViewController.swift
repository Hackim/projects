//
//  ProfileOTPViewController.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 30/05/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit
import RxSwift

class ProfileOTPViewController: BaseViewController {

    @IBOutlet weak var lockImageView: UIImageView!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var confirmationTextField: UITextField!
    @IBOutlet weak var firstLabel: UILabel!
    @IBOutlet weak var secondLabel: UILabel!
    @IBOutlet weak var thirdLabel: UILabel!
    @IBOutlet weak var fourthLabel: UILabel!
    @IBOutlet weak var firstView: UIView!
    @IBOutlet weak var secondView: UIView!
    @IBOutlet weak var thirdView: UIView!
    @IBOutlet weak var fourthView: UIView!
    @IBOutlet weak var verifyButton: UIButton!
    
    var phoneNumber: String = ""
    var inactiveColor: UIColor = UIColor.init(red: 0.820, green: 0.820, blue: 0.820, alpha: 1)
    var disposableBag = DisposeBag()
    var userID: Int!

    override func viewDidLoad() {
        super.viewDidLoad()
        resetFields()
        
        confirmationTextField.addTarget(self, action: #selector(codeChanged(textField:)), for: .editingChanged)
        
        let imageName = "lock"
        let originalImage = UIImage(named: imageName)
        let templateImage = originalImage!.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
        lockImageView.image = templateImage
        lockImageView.tintColor = themeColor

        verifyButton.layer.shadowOffset = CGSize.init(width: 0, height: 5)
        verifyButton.layer.shadowRadius = 3
        verifyButton.layer.shadowColor = themeBlueColor.cgColor
        verifyButton.layer.shadowOpacity = 0.75

        let messageText = "Enter 4 digit code which has been sent to your registered mobile number \(phoneNumber) "
        let attributedString = NSMutableAttributedString(string: messageText, attributes: [
            .font: UIFont(name: "Montserrat-Regular", size: 13.0)!,
            .foregroundColor: themeBlackColor
            ])
        
        attributedString.addAttribute(.font, value: UIFont(name: "Montserrat-SemiBold", size: 13.0)!, range: NSRange(location: 71, length: phoneNumber.count + 1))
        messageLabel.attributedText = attributedString

    }

    @IBAction func activateTextField(_ sender: UIButton) {
        confirmationTextField.becomeFirstResponder()
    }
    
    @IBAction func verifyButtonTapped(_ sender: UIButton) {
        guard confirmationTextField.text!.count == 4 else {
            self.showSimpleAlertWithMessage("Please enter the confirmation code sent to your number")
            return
        }
        verifyOTP(confirmationTextField.text!.trimmingCharacters(in: .whitespaces))
        
    }

    @objc func codeChanged(textField: UITextField) {
        resetFields()
        let code = textField.text ?? ""
        for (index, character) in code.enumerated() {
            switch index + 1 {
            case 1:
                firstView.backgroundColor = themeBlackColor
                firstLabel.text = "\(character)"
            case 2:
                secondView.backgroundColor = themeBlackColor
                secondLabel.text = "\(character)"
            case 3:
                thirdView.backgroundColor = themeBlackColor
                thirdLabel.text = "\(character)"
            case 4:
                fourthView.backgroundColor = themeBlackColor
                fourthLabel.text = "\(character)"
            default:
                break
            }
        }
    }
    
    func resetFields() {
        firstLabel.text = ""
        secondLabel.text = ""
        thirdLabel.text = ""
        fourthLabel.text = ""
        firstView.backgroundColor = inactiveColor
        secondView.backgroundColor = inactiveColor
        thirdView.backgroundColor = inactiveColor
        fourthView.backgroundColor = inactiveColor
    }


}

extension ProfileOTPViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (textField.text?.isEmpty)! && string == " " {
            return false
        }
        let char = string.cString(using: String.Encoding.utf8)!
        let isBackSpace = strcmp(char, "\\b")
        if (isBackSpace == -92) {
            return true
        }
        return (textField.text?.count)! < 4
    }
    
}

extension ProfileOTPViewController {
    
    func verifyOTP(_ otp: String) {
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }
        
        Utilities.showHUD(forView: self.view, excludeViews: [])
        
        let verifyObserver = ApiManager.shared.apiService.verifyOTPForNewMobile(otp, forUserID: userID, tempContact: phoneNumber)
        let verifyDisposable = verifyObserver.subscribe(onNext: {(user) in
            Utilities.hideHUD(forView: self.view)
            
            DispatchQueue.main.async {
                let appDelegate = (UIApplication.shared.delegate as! AppDelegate)
                appDelegate.user = user
                let alert = UIAlertController(title: AppName, message: AlertMessage.changeMobileSuccess.description(), preferredStyle: .alert)
                let okAction = UIAlertAction(title: AlertButton.ok.description(), style: .default, handler: { (action) in
                    self.navigationController?.popViewController(animated: true)
                })
                alert.addAction(okAction)
                self.present(alert, animated: true, completion: nil)
            }
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.showSimpleAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        })
        verifyDisposable.disposed(by: disposableBag)
    }
    
    
}
