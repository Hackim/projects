//
//  MySubscriptionViewController.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 01/08/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit
import RxSwift

class MySubscriptionViewController: BaseViewController {

    @IBOutlet weak var memberSinceLabel: UILabel!
    @IBOutlet weak var tPakExpiryLabel: UILabel!
    @IBOutlet weak var changePlanButton: UIButton!
    @IBOutlet weak var cancelPlanButton: UIButton!
    @IBOutlet weak var viewSavedCardsButton: UIButton!
    @IBOutlet var infoViews: [UIView]!
    
    var disposableBag = DisposeBag()
    var isFirstTime = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        (self.tabBarController as! BaseTabBarViewController).tabBar.isHidden = true
        (self.tabBarController as! BaseTabBarViewController).hideCenterButton()
        changePlanButton.isHidden = true
        viewSavedCardsButton.isHidden = true
        cancelPlanButton.isHidden = true
        for view in infoViews {
            view.isHidden = true
        }
        DispatchQueue.main.async {
            self.fetchUserDetails()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        guard !isFirstTime else {
            return
        }
        self.showAppropriateData()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        changePlanButton.layer.shadowOffset = CGSize.init(width: 0, height: 5)
        changePlanButton.layer.shadowRadius = 3
        changePlanButton.layer.shadowColor = themeBlueColor.cgColor
        changePlanButton.layer.shadowOpacity = 0.75
        
        cancelPlanButton.layer.shadowOffset = CGSize.init(width: 0, height: 5)
        cancelPlanButton.layer.shadowRadius = 3
        cancelPlanButton.layer.shadowColor = themeBlueColor.cgColor
        cancelPlanButton.layer.shadowOpacity = 0.75

        viewSavedCardsButton.layer.shadowOffset = CGSize.init(width: 0, height: 5)
        viewSavedCardsButton.layer.shadowRadius = 3
        viewSavedCardsButton.layer.shadowColor = themeBlueColor.cgColor
        viewSavedCardsButton.layer.shadowOpacity = 0.75

    }

    @IBAction func updatePlanButtonTapped(_ sender: UIButton) {
        let subscriptionListingVC = SubscriptionListingViewController.instantiateFromAppStoryboard(appStoryboard: .Profile)
        self.navigationController?.pushViewController(subscriptionListingVC, animated: true)
    }
    
    @IBAction func cancelSubscriptionButtonTapped(_ sender: UIButton) {
        unsubscribePlan()
    }
    
    @IBAction func viewSavedCardsButtonTapped(_ sender: UIButton) {
        
    }
    
    func showAppropriateData() {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let user = appDelegate.user!
        let userPaid = !user.validTillDateFormat.isEmpty
        self.changePlanButton.setTitle(userPaid ? "Renew/Change Plan".localized : "Subscribe".localized, for: .normal)

        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let date = dateFormatter.date(from: user.createdOn)!
        self.memberSinceLabel.text = Utilities.getFormattedDate(date, dateFormat: "dd-MMM-yyyy")
        self.tPakExpiryLabel.text = userPaid ? user.validTillDateFormat : "---"
        for view in self.infoViews {
            view.isHidden = false
        }
        self.cancelPlanButton.isHidden = !userPaid
        self.changePlanButton.isHidden = false
        self.viewSavedCardsButton.isHidden = false
    }
    
}

extension MySubscriptionViewController {
    
    func fetchUserDetails() {
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }
        Utilities.showHUD(forView: self.view, excludeViews: [])
        
        let userObserver = ApiManager.shared.apiService.fetchUserDetails()
        let userDisposable = userObserver.subscribe(onNext: {(user) in
            self.isFirstTime = false
            DispatchQueue.main.async {
                Utilities.hideHUD(forView: self.view)
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.user = user
                self.showAppropriateData()
            }
        }, onError: {(error) in
            self.isFirstTime = false
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.showSimpleAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        })
        userDisposable.disposed(by: disposableBag)
    }

    func unsubscribePlan() {
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }
        Utilities.showHUD(forView: self.view, excludeViews: [])
        
        let cancelSubObserver = ApiManager.shared.apiService.unsubscribePlanForUserID(String(UserStore.shared.userID!))
        let cancelSubDisposable = cancelSubObserver.subscribe(onNext: {(message) in
            Utilities.hideHUD(forView: self.view)
            DispatchQueue.main.async {
                self.showAppropriateData()
                self.showSimpleAlertWithMessage(message)
            }
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.showSimpleAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        })
        cancelSubDisposable.disposed(by: disposableBag)
    }

}
