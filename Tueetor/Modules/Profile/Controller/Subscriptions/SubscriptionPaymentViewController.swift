//
//  SubscriptionPaymentViewController.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 02/08/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit
import RxSwift
import Stripe

class SubscriptionPaymentViewController: BaseViewController {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var durationLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var couponTextField: TueetorTextField!
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var applyButton: UIButton!
    @IBOutlet weak var paymentButton: UIButton!
    
    var disposableBag = DisposeBag()
    var selectedPlan: Plan!
    var discountDict: [String: String]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        couponTextField.placeholder = "Enter Campaign Code".localized
        couponTextField.title = "CAMPAIGN CODE".localized
        couponTextField.textFont = textFieldLightFont
        couponTextField.placeholderFont = textFieldDefaultFont
        couponTextField.titleFont = textFieldDefaultFont
        couponTextField.clearButtonMode = .always
        couponTextField.autocapitalizationType = .allCharacters

        configureViewWithPlan()

    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        paymentButton.layer.shadowOffset = CGSize.init(width: 0, height: 5)
        paymentButton.layer.shadowRadius = 3
        paymentButton.layer.shadowColor = themeBlueColor.cgColor
        paymentButton.layer.shadowOpacity = 0.75
    }

    func configureViewWithPlan() {
        titleLabel.text = selectedPlan.title
        descriptionLabel.text = selectedPlan.details
        let totatMonths = Int(selectedPlan.totalMonths)! + Int(selectedPlan.freeMonths)!
        durationLabel.text = String(totatMonths) + (totatMonths == 1 ? " Month".localized : " Months".localized)
        priceLabel.text = selectedPlan.price + " " + selectedPlan.currencyCode
        errorLabel.text = ""
    }
    
    func configureViewWithPlanAndDiscount(_ discountDict: [String: String]) {
        titleLabel.text = selectedPlan.title
        descriptionLabel.text = selectedPlan.details
        
        let totatMonths = Int(selectedPlan.totalMonths)! + Int(selectedPlan.freeMonths)!
        let beforeDuration = String(totatMonths) + (totatMonths == 1 ? " Month".localized : " Months".localized)
        if let duration = discountDict["extraMonth"], let durationInt = Int(duration) {
            let afterDuration = String(totatMonths + durationInt) + ((totatMonths + durationInt) == 1 ? " Month".localized : " Months".localized)
            let durationText = NSMutableAttributedString()
            durationText.append(NSAttributedString(string: beforeDuration,
                                                       attributes: [.font: UIFont(name: "Montserrat-Light", size: 17.0)!,
                                                                    .foregroundColor : themeBlackColor]))
            durationText.addAttribute(NSAttributedStringKey.strikethroughStyle,
                                         value: NSUnderlineStyle.styleSingle.rawValue,
                                         range: NSMakeRange(0, durationText.length))
            durationText.addAttribute(NSAttributedStringKey.strikethroughColor,
                                      value: themeRedColor,
                                      range: NSMakeRange(0, durationText.length))
            durationText.append(NSAttributedString(string: "  \(afterDuration)",
                                                       attributes: [.font: UIFont(name: "Montserrat-Light", size: 17.0)!,
                                                                    .foregroundColor : themeBlackColor]))
            durationLabel.attributedText = durationText

        } else {
            durationLabel.text = beforeDuration
        }

        let beforePrice = selectedPlan.price + " " + selectedPlan.currencyCode
        if let discount = discountDict["discount"] {
            let discountNumber = atof(discount)
            let originalPrice = atof(selectedPlan.price)
            let finalPrice = originalPrice - discountNumber
            let finalPriceString = "\(finalPrice)" + " " + selectedPlan.currencyCode

            let priceText = NSMutableAttributedString()
            priceText.append(NSAttributedString(string: beforePrice,
                                                   attributes: [.font: UIFont(name: "Montserrat-Light", size: 17.0)!,
                                                                .foregroundColor : themeBlackColor]))
            priceText.addAttribute(NSAttributedStringKey.strikethroughStyle,
                                      value: NSUnderlineStyle.styleSingle.rawValue,
                                      range: NSMakeRange(0, priceText.length))
            priceText.addAttribute(NSAttributedStringKey.strikethroughColor,
                                      value: themeRedColor,
                                      range: NSMakeRange(0, priceText.length))
            priceText.append(NSAttributedString(string: "  \(finalPriceString)",
                attributes: [.font: UIFont(name: "Montserrat-Light", size: 17.0)!,
                             .foregroundColor : themeBlackColor]))
            priceLabel.attributedText = priceText
            
        } else {
            priceLabel.text = beforePrice
        }
        errorLabel.text = "Campaign Code has been applied successfully".localized
        errorLabel.textColor = themeGreenColor
    }
    
    @IBAction func applyButtonTapped(_ sender: UIButton) {
        self.view.endEditing(true)
        guard !couponTextField.text!.isEmpty else {
            errorLabel.text = "Please enter a campaign code".localized
            errorLabel.textColor = themeRedColor
            return
        }
        applyCoupon(couponTextField.text ?? "")
    }
    
    @IBAction func paymentButtonTapped(_ sender: UIButton) {
        fetchSavedCards()
    }
    
    
}

extension SubscriptionPaymentViewController: STPAddCardViewControllerDelegate {
    
    func addCardViewControllerDidCancel(_ addCardViewController: STPAddCardViewController) {
        // Dismiss add card view controller
        dismiss(animated: true)
    }
    
    func addCardViewController(_ addCardViewController: STPAddCardViewController, didCreateToken token: STPToken, completion: @escaping STPErrorBlock) {
        self.subscribeToPlanWithStripeToken(token.tokenId)
        dismiss(animated: true)
    }
}

extension SubscriptionPaymentViewController: SelectPaymentViewControllerDelegate {
    
    func purchaseWithStripeToken(_ token: String) {
        self.subscribeToPlanWithStripeToken(token)
    }
    
    func purchaseWithCard(_ cardId: String) {
        self.subscribeToPlanWithCardID(cardId)
    }

}

extension SubscriptionPaymentViewController: UITextFieldDelegate {

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard discountDict != nil else {
            return true
        }
        discountDict = nil
        self.configureViewWithPlan()
        errorLabel.text = ""
        return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        guard discountDict != nil else {
            return true
        }

        discountDict = nil
        self.configureViewWithPlan()
        errorLabel.text = ""
        return true
    }
}

extension SubscriptionPaymentViewController {
    
    func fetchEphemeralKey() {
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        guard let user = appDelegate.user else {
            return
        }
        
        Utilities.showHUD(forView: self.view, excludeViews: [])
        
        let ephemeralObserver = ApiManager.shared.apiService.fetchEphemeralKeyForUserID(String(user.ID))
        let ephemeralDisposable = ephemeralObserver.subscribe(onNext: {(key) in
            Utilities.hideHUD(forView: self.view)

        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.showSimpleAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        })
        ephemeralDisposable.disposed(by: disposableBag)
    }
    
    func subscribeToPlanWithStripeToken(_ token: String) {
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        guard let user = appDelegate.user else {
            return
        }
        var couponCode: String = ""
        if let disDict = discountDict, let coupon = disDict["coupon"] {
            couponCode = coupon
        }
        Utilities.showHUD(forView: self.view, excludeViews: [])
        
        let subscribeObserver = ApiManager.shared.apiService.subscribeToPlanWithStripeToken(token, userID: String(user.ID), planID: selectedPlan.id, couponCode: couponCode.isEmpty ? nil : couponCode)
        let subscribeDisposable = subscribeObserver.subscribe(onNext: {(message) in
            Utilities.hideHUD(forView: self.view)
            DispatchQueue.main.async {
                let alert = UIAlertController(title: AppName, message: message, preferredStyle: .alert)
                let okAction = UIAlertAction(title: AlertButton.ok.description(), style: .default, handler: { (action) in
                    let viewControllers: [UIViewController] = self.navigationController!.viewControllers
                    for aViewController in viewControllers {
                        if aViewController is MyReviewsViewController {
                            self.navigationController!.popToViewController(aViewController, animated: true)
                        } else if aViewController is MySubscriptionViewController {
                            self.navigationController!.popToViewController(aViewController, animated: true)
                        }
                    }
                })
                alert.addAction(okAction)
                self.present(alert, animated: true, completion: nil)
            }
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.showSimpleAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        })
        subscribeDisposable.disposed(by: disposableBag)
    }
    
    func subscribeToPlanWithCardID(_ cardID: String) {
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        guard let user = appDelegate.user else {
            return
        }
        var couponCode: String = ""
        if let disDict = discountDict, let coupon = disDict["coupon"] {
            couponCode = coupon
        }
        Utilities.showHUD(forView: self.view, excludeViews: [])
        
        let subscribeObserver = ApiManager.shared.apiService.subscribeToPlanWithCardID(cardID, userID: String(user.ID), planID: selectedPlan.id, couponCode: couponCode.isEmpty ? nil : couponCode)
        let subscribeDisposable = subscribeObserver.subscribe(onNext: {(message) in
            Utilities.hideHUD(forView: self.view)
            DispatchQueue.main.async {
                let alert = UIAlertController(title: AppName, message: message, preferredStyle: .alert)
                let okAction = UIAlertAction(title: AlertButton.ok.description(), style: .default, handler: { (action) in
                    let viewControllers: [UIViewController] = self.navigationController!.viewControllers
                    for aViewController in viewControllers {
                        if aViewController is MyReviewsViewController {
                            self.navigationController!.popToViewController(aViewController, animated: true)
                        } else if aViewController is MySubscriptionViewController {
                            self.navigationController!.popToViewController(aViewController, animated: true)
                        }
                    }
                })
                alert.addAction(okAction)
                self.present(alert, animated: true, completion: nil)
            }
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.showSimpleAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        })
        subscribeDisposable.disposed(by: disposableBag)
    }

    func fetchSavedCards() {
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }
        guard let userID = UserStore.shared.userID else {
            return
        }
        
        Utilities.showHUD(forView: self.view, excludeViews: [])
        
        let savedCardObserver = ApiManager.shared.apiService.fetchSavedCardsForUserID(String(userID))
        let savedCardDisposable = savedCardObserver.subscribe(onNext: {(cardsArray) in
            
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if cardsArray.count == 0 {
                    let addCardViewController = STPAddCardViewController()
                    addCardViewController.delegate = self
                    let navigationController = UINavigationController(rootViewController: addCardViewController)
                    self.present(navigationController, animated: true)
                } else {
                    let paymentMethodController = SelectPaymentViewController.instantiateFromAppStoryboard(appStoryboard: .Profile)
                    paymentMethodController.savedCards = cardsArray
                    paymentMethodController.delegate = self
                    self.navigationController?.pushViewController(paymentMethodController, animated: true)
                }
            })
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
            })
        })
        savedCardDisposable.disposed(by: disposableBag)
    }
    
    func applyCoupon(_ coupon: String) {
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }
        
        Utilities.showHUD(forView: self.view, excludeViews: [])
        
        let couponObserver = ApiManager.shared.apiService.applyCouponCode(coupon, toPlanID: selectedPlan.id, countryID: "178")
        let couponDisposable = couponObserver.subscribe(onNext: {(dataDict) in
            
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                self.discountDict = dataDict
                self.configureViewWithPlanAndDiscount(self.discountDict)
            })
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.showSimpleAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        })
        couponDisposable.disposed(by: disposableBag)
    }
    
}
