//
//  SavedCardsViewController.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 06/08/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit
import RxSwift
import Stripe

class SavedCardsViewController: BaseViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var emptyMessageLabel: UILabel!
    
    var disposableBag = DisposeBag()
    var savedCards: [SavedCard]!
    
    private let refreshControl = UIRefreshControl()

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView()
        if #available(iOS 10.0, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.addSubview(refreshControl)
        }
        refreshControl.addTarget(self, action: #selector(refreshData(_:)), for: .valueChanged)
        fetchSavedCards()
    }


    @objc func refreshData(_ sender: Any) {
        fetchSavedCards()
        if let refreshControl = sender as? UIRefreshControl {
            refreshControl.endRefreshing()
        }
    }

    func showDataAppropritely() {
        if self.savedCards.count == 0 {
            tableView.isHidden = true
            emptyMessageLabel.isHidden = false
        } else {
            tableView.isHidden = false
            emptyMessageLabel.isHidden = true
        }
        tableView.reloadData()
    }

    @IBAction func addCardButtonTapped(_ sender: UIButton) {
        let addCardViewController = STPAddCardViewController()
        addCardViewController.delegate = self
        
        // Present add card view controller
        
        let navigationController = UINavigationController(rootViewController: addCardViewController)
        present(navigationController, animated: true)

    }
    
}

extension SavedCardsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let cards = savedCards else {
            return 0
        }
        return cards.count == 0 ? 0 : savedCards.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard indexPath.row != savedCards.count else {
            let cell = tableView.dequeueReusableCell(withIdentifier: EmptyMessageTableViewCell.cellIdentifier(), for: indexPath) as! EmptyMessageTableViewCell
            cell.messageLabel.text = "Swipe left to perform actions".localized
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: SavedCardTableViewCell.cellIdentifier(), for: indexPath) as! SavedCardTableViewCell
        cell.configureCellWithCard(savedCards[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == savedCards.count {
            return 44.0
        }
        
        return SavedCardTableViewCell.cellHeight()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let delete = UITableViewRowAction(style: .normal, title: "Delete".localized) { action, index in
            DispatchQueue.main.async {
                self.deleteCardWithID(self.savedCards[indexPath.row].id)
            }
        }
        
        delete.backgroundColor = themeRedColor
        
        return [delete]
        
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        if indexPath.row >= savedCards.count {
            return false
        }
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
    }

    
}

extension SavedCardsViewController: STPAddCardViewControllerDelegate {
    
    func addCardViewControllerDidCancel(_ addCardViewController: STPAddCardViewController) {
        // Dismiss add card view controller
        dismiss(animated: true)
    }
    
    func addCardViewController(_ addCardViewController: STPAddCardViewController, didCreateToken token: STPToken, completion: @escaping STPErrorBlock) {
        DispatchQueue.main.async {
            self.addCardWithToken(token.tokenId)
        }
        dismiss(animated: true)
    }
}

extension SavedCardsViewController {
    
    func fetchSavedCards() {
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }
        guard let userID = UserStore.shared.userID else {
            return
        }
        
        Utilities.showHUD(forView: self.view, excludeViews: [])

        let savedCardObserver = ApiManager.shared.apiService.fetchSavedCardsForUserID(String(userID))
        let savedCardDisposable = savedCardObserver.subscribe(onNext: {(cardsArray) in

            self.savedCards = cardsArray
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                self.showDataAppropritely()
            })
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
            })
        })
        savedCardDisposable.disposed(by: disposableBag)
    }
    
    func deleteCardWithID(_ cardID: String) {
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }
        guard let userID = UserStore.shared.userID else {
            return
        }
        
        Utilities.showHUD(forView: self.view, excludeViews: [])
        
        let deleteCardObserver = ApiManager.shared.apiService.deleteCardForUserID(String(userID), cardID: cardID)
        let deleteCardDisposable = deleteCardObserver.subscribe(onNext: {(cardsArray) in
            
            self.savedCards = cardsArray
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                self.showDataAppropritely()
            })
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
            })
        })
        deleteCardDisposable.disposed(by: disposableBag)
    }

    func addCardWithToken(_ stripeToken: String) {
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }
        guard let userID = UserStore.shared.userID else {
            return
        }
        
        Utilities.showHUD(forView: self.view, excludeViews: [])
        
        let addCardObserver = ApiManager.shared.apiService.addCardToUserID(String(userID), stripeToken: stripeToken)
        let addCardDisposable = addCardObserver.subscribe(onNext: {(cardsArray) in
            
            self.savedCards = cardsArray
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                self.showDataAppropritely()
            })
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
            })
        })
        addCardDisposable.disposed(by: disposableBag)
    }

}
