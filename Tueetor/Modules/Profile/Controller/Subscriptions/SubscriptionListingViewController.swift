//
//  SubscriptionListingViewController.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 01/08/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit
import RxSwift

class SubscriptionListingViewController: BaseViewController {

    @IBOutlet weak var tableView: UITableView!
    var plans: [Plan]!
    var disposableBag = DisposeBag()
    var couponTextField: TueetorTextField!
    var selectedPlan: Plan!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView()
        fetchPlans()
    }

    override func viewWillAppear(_ animated: Bool) {
        (self.tabBarController as! BaseTabBarViewController).tabBar.isHidden = true
        (self.tabBarController as! BaseTabBarViewController).hideCenterButton()
    }
    
}

extension SubscriptionListingViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let plansArray = plans else {
            return 0
        }
        return plansArray.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: SubscriptionInfoTableViewCell.cellIdentifier(), for: indexPath) as! SubscriptionInfoTableViewCell
            return cell
//        } else if indexPath.row == plans.count + 1  {
//            let cell = tableView.dequeueReusableCell(withIdentifier: SubscriptionCouponCodeTableViewCell.cellIdentifier(), for: indexPath) as! SubscriptionCouponCodeTableViewCell
//            cell.delegate = self
//            couponTextField = cell.couponTextField
//            couponTextField.placeholder = "Enter Campaign Code".localized
//            couponTextField.title = "CAMPAIGN CODE".localized
//            couponTextField.textFont = textFieldLightFont
//            couponTextField.placeholderFont = textFieldDefaultFont
//            couponTextField.titleFont = textFieldDefaultFont
//            couponTextField.clearButtonMode = .always
//            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: SubscriptionPlanTableViewCell.cellIdentifier(), for: indexPath) as! SubscriptionPlanTableViewCell
            cell.configureCellWithPlan(plans[indexPath.row - 1])
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return SubscriptionInfoTableViewCell.cellHeight()
        } else {
            return SubscriptionPlanTableViewCell.cellHeight()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard indexPath.row != 0 else {
            return
        }
        print(indexPath.row)
        let summaryVC = SubscriptionPaymentViewController.instantiateFromAppStoryboard(appStoryboard: .Profile)
        selectedPlan = plans[indexPath.row - 1]
        summaryVC.selectedPlan = selectedPlan
//
//
//        self.navigationController?.pushViewController(summaryVC, animated: true)
//        self.navigationController?.navigationBar.isTranslucent = true
//        self.navigationController?.navigationBar.isHidden = false
//        selectedPlan = plans[indexPath.row - 1]
//        let addCardViewController = STPAddCardViewController()
//        addCardViewController.delegate = self
//        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.navigationController?.pushViewController(summaryVC, animated: true)

    }
    
}

extension SubscriptionListingViewController: SubscriptionCouponCodeTableViewCellDelegate {

    func applyButtonTapped() {
        self.view.endEditing(true)
        let couponCode = couponTextField.text ?? ""
        let cell = tableView.cellForRow(at: IndexPath.init(row: plans.count + 1, section: 0)) as? SubscriptionCouponCodeTableViewCell
        cell?.validateCouponCode(couponCode)
        tableView.beginUpdates()
        tableView.endUpdates()
        let message = cell?.errorLabel.text ?? ""
        if !message.isEmpty {
            self.showSimpleAlertWithMessage(message)
        } else {
            validateCouponCode(couponCode)
        }
    }
}

//extension SubscriptionListingViewController: STPAddCardViewControllerDelegate {
//    
//    func addCardViewControllerDidCancel(_ addCardViewController: STPAddCardViewController) {
//        self.navigationController?.setNavigationBarHidden(true, animated: true)
//
//        navigationController?.popViewController(animated: true)
//    }
//    
//    func addCardViewController(_ addCardViewController: STPAddCardViewController,
//                               didCreateToken token: STPToken,
//                               completion: @escaping STPErrorBlock) {
//        self.navigationController?.setNavigationBarHidden(true, animated: true)
//        navigationController?.popViewController(animated: true)
//    }
//}



extension SubscriptionListingViewController {
    
    
    func fetchPlans() {
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        guard let user = appDelegate.user else {
            return
        }
        
        Utilities.showHUD(forView: self.view, excludeViews: [])
        
        let plansObserver = ApiManager.shared.apiService.fetchPlansForCountryID("178")
        let plansDisposable = plansObserver.subscribe(onNext: {(plans) in
            self.plans = plans
            DispatchQueue.main.async {
                self.tableView.isHidden = false
                Utilities.hideHUD(forView: self.view)
                self.tableView.reloadData()
            }
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.showSimpleAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        })
        plansDisposable.disposed(by: disposableBag)
    }
    
    func validateCouponCode(_ code: String) {
        let cell = tableView.cellForRow(at: IndexPath.init(row: plans.count + 1, section: 0)) as? SubscriptionCouponCodeTableViewCell
        
        if code.lowercased() == "phani" {
            
            cell?.errorLabel.textColor = UIColor.green
            cell?.errorLabel.text = "Successfully applied"
        } else {
            cell?.errorLabel.textColor = UIColor.red
            cell?.errorLabel.text = "Invalid coupon code"
        }
        self.showSimpleAlertWithMessage(cell?.errorLabel.text ?? "")
        tableView.beginUpdates()
        tableView.endUpdates()
    }
    
}

