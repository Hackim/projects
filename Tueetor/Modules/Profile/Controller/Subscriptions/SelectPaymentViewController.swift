//
//  SelectPaymentViewController.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 06/08/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit
import Stripe

protocol SelectPaymentViewControllerDelegate {
    func purchaseWithStripeToken(_ token: String)
    func purchaseWithCard(_ cardId: String)
}

class SelectPaymentViewController: BaseViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var savedCards: [SavedCard]!
    var delegate: SelectPaymentViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView()
    }

}

extension SelectPaymentViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return savedCards.count + 1
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard indexPath.row != savedCards.count else {
            let cell = tableView.dequeueReusableCell(withIdentifier: NewCardButtonTableViewCell.cellIdentifier(), for: indexPath) as! NewCardButtonTableViewCell
            cell.delegate = self
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: SavedCardTableViewCell.cellIdentifier(), for: indexPath) as! SavedCardTableViewCell
        cell.configureCellWithCard(savedCards[indexPath.row])
        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        guard indexPath.row != savedCards.count else {
            return NewCardButtonTableViewCell.cellHeight()
        }
        return SavedCardTableViewCell.cellHeight()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard indexPath.row != savedCards.count else {
            return
        }
        delegate?.purchaseWithCard(savedCards[indexPath.row].id)
        self.navigationController?.popViewController(animated: true)
    }
    
}

extension SelectPaymentViewController: NewCardButtonTableViewCellDelegate {
    
    func addCardButtonTapped() {
        let addCardViewController = STPAddCardViewController()
        addCardViewController.delegate = self
        
        let navigationController = UINavigationController(rootViewController: addCardViewController)
        present(navigationController, animated: true)
    }
}

extension SelectPaymentViewController: STPAddCardViewControllerDelegate {
    
    func addCardViewControllerDidCancel(_ addCardViewController: STPAddCardViewController) {
        // Dismiss add card view controller
        dismiss(animated: true)
    }
    
    func addCardViewController(_ addCardViewController: STPAddCardViewController, didCreateToken token: STPToken, completion: @escaping STPErrorBlock) {
        delegate?.purchaseWithStripeToken(token.tokenId)
        dismiss(animated: true)
        self.navigationController?.popViewController(animated: true)
    }
}

