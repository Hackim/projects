//
//  DocumentChildPageViewController.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 04/07/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit
import WebKit

class DocumentChildPageViewController: BaseViewController {

    @IBOutlet weak var holderView: UIView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var webView: WKWebView!
    var link: String!

    override func loadView() {
        super.loadView()
        let webConfiguration = WKWebViewConfiguration()
        webConfiguration.allowsInlineMediaPlayback = true
        webConfiguration.requiresUserActionForMediaPlayback = true
        webView = WKWebView(frame: CGRect.init(x: 0, y: 0, width: ScreenWidth, height: ScreenHeight - 20.0), configuration: webConfiguration)
        webView.uiDelegate = self
        webView.navigationDelegate = self
        holderView.backgroundColor = UIColor.orange
        holderView.addSubview(webView)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        if let urlString = link, let url = URL.init(string: urlString) {
            let myRequest = URLRequest(url: url)
            webView.load(myRequest)
        }
    }
    
}

extension DocumentChildPageViewController: WKUIDelegate, WKNavigationDelegate {
    
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        activityIndicator.stopAnimating()
    }
    
}
