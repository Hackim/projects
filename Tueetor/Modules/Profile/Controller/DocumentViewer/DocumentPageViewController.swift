//
//  DocumentPageViewController.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 04/07/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit

class DocumentPageViewController: UIPageViewController {

    var pageControl = UIPageControl()
    var pages: [DocumentChildPageViewController]!
    var links: [String]!
    var titles: [String]!
    var titleLabel: UILabel!
    var backButton: UIButton!
    var startIndex = 0
    var isPushed = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        (self.tabBarController as! BaseTabBarViewController).tabBar.isHidden = true
        (self.tabBarController as! BaseTabBarViewController).hideCenterButton()

        UIApplication.shared.statusBarStyle = .default
        pages = []
        for link in links {
            let documentVC = DocumentChildPageViewController.instantiateFromAppStoryboard(appStoryboard: .Profile)
            documentVC.link = link
            pages.append(documentVC)
        }
        self.dataSource = self
        self.delegate = self
        configurePageControl()
        let bgView = UIView.init(frame: UIScreen.main.bounds)
        bgView.backgroundColor = UIColor.white
        view.insertSubview(bgView, at: 0)
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        
        titleLabel = UILabel(frame: CGRect(x: 46, y: 29, width: ScreenWidth - 92, height: 30))
        titleLabel.text = titles[startIndex]
        titleLabel.font = UIFont(name: "Montserrat-Regular", size: 17.0)
        titleLabel.clipsToBounds = true
        titleLabel.numberOfLines = 2
        titleLabel.textAlignment = .center
        titleLabel.textColor = themeBlackColor
        view.addSubview(titleLabel)
        view.bringSubview(toFront: titleLabel)
        
        let borderView = UIView(frame: CGRect(x: 0, y: 65, width: ScreenWidth, height: 1))
        borderView.backgroundColor = themeLightGrayColor
        view.addSubview(borderView)
        view.bringSubview(toFront: borderView)
        
        backButton = UIButton(frame: CGRect(x: isPushed ? 18 : ScreenWidth - 40, y: 29, width: 30, height: 30))
        backButton.setImage(UIImage.init(named: isPushed ? "left_arrow" : "close"), for: .normal)
        backButton.addTarget(self, action: #selector(backButtonTapped), for: .touchUpInside)
        view.addSubview(backButton)
        view.bringSubview(toFront: backButton)
        
        setViewControllers([pages[startIndex]], direction: .forward, animated: false, completion: nil)

    }
    
    func configurePageControl() {
        // The total number of pages that are available is based on how many available colors we have.
        pageControl = UIPageControl(frame: CGRect(x: 0,y: UIScreen.main.bounds.maxY - 50,width: UIScreen.main.bounds.width,height: 50))
        pageControl.numberOfPages = pages.count
        pageControl.currentPage = startIndex
        pageControl.tintColor = UIColor.white
        pageControl.pageIndicatorTintColor = UIColor.init(red: 0.8431, green: 0.8431, blue: 0.8431, alpha: 1)
        pageControl.currentPageIndicatorTintColor = UIColor.init(red: 0.1372, green: 0.6431, blue: 0.6941, alpha: 1)
        pageControl.layer.position.y = ScreenHeight - 50
        view.addSubview(pageControl)
    }
    
    @objc func backButtonTapped() {
        if isPushed {
            self.navigationController?.popViewController(animated: true)
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @objc func doneButtonTapped() {
        let landingViewController = LandingViewController.instantiateFromAppStoryboard(appStoryboard: .Login)
        UserStore.shared.isWalkthroughCompleted = true
        self.navigationController?.pushViewController(landingViewController, animated: true)
    }
    
    func changeValues() {
        let pageContentViewController = self.viewControllers![0]
        self.pageControl.currentPage = pages.index(of: pageContentViewController as! DocumentChildPageViewController)!
        titleLabel.text = titles[self.pageControl.currentPage]
        if self.pageControl.currentPage == pages.count - 1 {
//            nextButton.setTitle("Done", for: .normal)
//            nextButton.addTarget(self, action: #selector(doneButtonTapped), for: .touchUpInside)
        } else {
//            nextButton.setTitle("Next", for: .normal)
//            nextButton.addTarget(self, action: #selector(nextButtonTapped), for: .touchUpInside)
        }
        
    }
}

extension DocumentPageViewController: UIPageViewControllerDataSource {
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        guard let viewControllerIndex = pages.index(of: viewController as! DocumentChildPageViewController) else { return nil }
        
        let previousIndex = viewControllerIndex - 1
        
        guard previousIndex >= 0          else { return nil }
        
        guard pages.count > previousIndex else { return nil }

        return pages[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController?
    {
        guard let viewControllerIndex = pages.index(of: viewController as! DocumentChildPageViewController) else { return nil }
        
        let nextIndex = viewControllerIndex + 1
        
        guard nextIndex < pages.count else { return nil }
        
        guard pages.count > nextIndex else { return nil }

        return pages[nextIndex]
    }
    
    func presentationIndex(for pageViewController: UIPageViewController) -> Int {
        return startIndex
    }
    
    func presentationCount(for pageViewController: UIPageViewController) -> Int {
        return pages.count
    }
}

extension DocumentPageViewController: UIPageViewControllerDelegate {
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        changeValues()
    }
    
}
