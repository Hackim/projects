//
//  MyAvailabilityViewController.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 17/06/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit
import SpreadsheetView

protocol MyAvailabilityViewControllerDelegate {
    func slotsSelected(slots: [String: [String]], count: Int)
}
class MyAvailabilityViewController: BaseViewController, SpreadsheetViewDataSource, SpreadsheetViewDelegate {

    @IBOutlet weak var spreadSheet: SpreadsheetView!

    var headers = ["TIME".localized, "MON".localized, "TUE".localized, "WED".localized, "THU".localized, "FRI".localized, "SAT".localized, "SUN".localized]
    var data = [[String]]()
    var schedules: [String: [String]]!
    var delegate: MyAvailabilityViewControllerDelegate?
    var isEditable = true
    
    override func viewDidLoad() {
        super.viewDidLoad()

        getDataArrayFromSchedule()
        
        spreadSheet.delegate = self
        spreadSheet.dataSource = self
        spreadSheet.isDirectionalLockEnabled = true
        spreadSheet.register(HeaderCell.self, forCellWithReuseIdentifier: String(describing: HeaderCell.self))
        spreadSheet.register(TextCell.self, forCellWithReuseIdentifier: String(describing: TextCell.self))
        spreadSheet.register(CheckBoxCell.self, forCellWithReuseIdentifier: String(describing: CheckBoxCell.self))
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        spreadSheet.scrollToItem(at: IndexPath.init(row: 6, column: 0), at: .top, animated: true)
    }
    func getDataArrayFromSchedule() {
        var completeData = [[String]]()
        let weeksArray = ["MON", "TUE", "WED", "THU", "FRI", "SAT", "SUN"]
        for i in 0...23 {
            var hourSchedule = [String]()
            if i < 10 {
                hourSchedule.append("0\(i):00")
            } else {
                hourSchedule.append("\(i):00")
            }
            for day in weeksArray {
                let daysSchedule = schedules[day]
                if daysSchedule!.contains("\(i)") {
                    hourSchedule.append("1")
                } else {
                    hourSchedule.append("0")
                }
            }
            completeData.append(hourSchedule)
        }
        data = completeData
    }
    
    @IBAction func backButtonTapped(_ sender: UIButton) {
        var schedulesDict = ["MON": [String](), "TUE": [String](), "WED": [String](), "THU": [String](), "FRI": [String](), "SAT": [String](), "SUN": [String]()]
        var count = 0
        for i in 0...23 {
            for j in 1...7 {
                if data[i][j] == "1" {
                    let day = getDayBasedOnIndex(index: j)
                    var currentData = schedulesDict[day]
                    currentData!.append("\(i)")
                    schedulesDict[day] = currentData
                    count += 1
                }
            }
        }
        delegate?.slotsSelected(slots: schedulesDict, count: count)
        self.navigateBack(sender: sender)
    }
    
    func getDayBasedOnIndex(index: Int) -> String {
        switch index {
        case 1:
            return "MON"
        case 2:
            return "TUE"
        case 3:
            return "WED"
        case 4:
            return "THU"
        case 5:
            return "FRI"
        case 6:
            return "SAT"
        case 7:
            return "SUN"
        default:
            return ""
        }
    }
    
    func numberOfColumns(in spreadsheetView: SpreadsheetView) -> Int {
        return headers.count
    }
    
    func numberOfRows(in spreadsheetView: SpreadsheetView) -> Int {
        return 25
    }
    
    func spreadsheetView(_ spreadsheetView: SpreadsheetView, widthForColumn column: Int) -> CGFloat {
        if column == 0 {
            return 60
        } else {
            return 50
        }
    }
    
    func spreadsheetView(_ spreadsheetView: SpreadsheetView, heightForRow row: Int) -> CGFloat {
        if case 0 = row {
            return 40
        } else {
            return 50
        }
    }
    
    func frozenRows(in spreadsheetView: SpreadsheetView) -> Int {
        return 1
    }
    
    func spreadsheetView(_ spreadsheetView: SpreadsheetView, cellForItemAt indexPath: IndexPath) -> Cell? {
        if indexPath.row == 0 {
            let cell = spreadSheet.dequeueReusableCell(withReuseIdentifier: String(describing: HeaderCell.self), for: indexPath) as! HeaderCell
            cell.label.text = headers[indexPath.column]
            cell.gridlines.left = .none
            cell.gridlines.right = .none
            cell.gridlines.top = .none
            cell.gridlines.left = .none
            return cell
        } else {
            if indexPath.column == 0 {
                let cell = spreadSheet.dequeueReusableCell(withReuseIdentifier: String(describing: TextCell.self), for: indexPath) as! TextCell
                cell.label.text = data[indexPath.row - 1][0]
                cell.label.font = UIFont(name: "Montserrat-SemiBold", size: 14.0)!
                cell.gridlines.left = .none
                cell.gridlines.right = .none
                cell.gridlines.top = .none
                cell.gridlines.left = .none
                return cell
            } else {
                let cell = spreadSheet.dequeueReusableCell(withReuseIdentifier: String(describing: CheckBoxCell.self), for: indexPath) as! CheckBoxCell
                let cellValue = data[indexPath.row - 1][indexPath.column]
                if cellValue == "0" {
                    cell.imageView.image = UIImage(named: "checkbox_empty")
                } else {
                    cell.imageView.image = UIImage(named: "checkbox")
                }
                cell.gridlines.left = .none
                cell.gridlines.right = .none
                cell.gridlines.top = .none
                cell.gridlines.left = .none
                return cell
            }
        }
    }
    
    func spreadsheetView(_ spreadsheetView: SpreadsheetView, didSelectItemAt indexPath: IndexPath) {
        guard isEditable else {
            return
        }
        if indexPath.row != 0 || indexPath.column != 0 {
            let cellValue = data[indexPath.row - 1][indexPath.column]
            let cell = spreadsheetView.cellForItem(at: indexPath) as! CheckBoxCell
            if cellValue == "0" {
                data[indexPath.row - 1][indexPath.column] = "1"
                cell.imageView.image = UIImage(named: "checkbox")
            } else {
                data[indexPath.row - 1][indexPath.column] = "0"
                cell.imageView.image = UIImage(named: "checkbox_empty")
            }
        }
    }
    
}

