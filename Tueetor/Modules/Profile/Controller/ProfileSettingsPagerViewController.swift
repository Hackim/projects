//
//  ProfileSettingsPagerViewController.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 28/05/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit
import XLPagerTabStrip

class ProfileSettingsPagerViewController: ButtonBarPagerTabStripViewController {

    var shouldShowMobileScreen = false
    
    override func viewDidLoad() {
        self.settings.style.selectedBarBackgroundColor = themeBlueColor
        self.settings.style.buttonBarItemBackgroundColor = UIColor.clear
        self.settings.style.buttonBarItemTitleColor = themeBlackColor
        self.settings.style.buttonBarItemsShouldFillAvailiableWidth = true
        self.settings.style.buttonBarItemFont = textFieldDefaultFont
//        if shouldShowMobileScreen {
//            DispatchQueue.main.async {
//                self.moveToViewController(at: 1, animated: false)
//            }
//        }

        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        
        let changePasswordVC = ChangePasswordViewController.instantiateFromAppStoryboard(appStoryboard: .Profile)
        let changeMobileVC = ChangeMobileNumberViewController.instantiateFromAppStoryboard(appStoryboard: .Profile)
        if shouldShowMobileScreen {
            return [changeMobileVC]
        } else {
            return [changePasswordVC, changeMobileVC]
        }
    }
    


}
