//
//  RequestReviewViewController.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 01/08/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit
import RxSwift

class RequestReviewViewController: BaseViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var submitButton: UIButton!
    
    var disposableBag = DisposeBag()
    var studentName = ""
    var studentEmail = ""
    var activeTextField: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView()
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        submitButton.layer.shadowOffset = CGSize.init(width: 0, height: 5)
        submitButton.layer.shadowRadius = 3
        submitButton.layer.shadowColor = themeBlueColor.cgColor
        submitButton.layer.shadowOpacity = 0.75
    }

    @IBAction func submitButtonTapped(_ sender: UIButton) {
        self.view.endEditing(true)
        let validationInformation = isDataValid()
        guard validationInformation.isValid else {
            if !validationInformation.message.isEmpty {
                self.showSimpleAlertWithMessage(validationInformation.message)
            }
            return
        }

        requestReviewFromUser()
    }

    func configureTextField(_ customTextField: TueetorTextField) {
        customTextField.delegate = self

        switch customTextField.tag {
        case 1:
            customTextField.autocapitalizationType = .words
            customTextField.keyboardType = .asciiCapable

        default:
            customTextField.autocapitalizationType = .none
            customTextField.keyboardType = .emailAddress
        }
        
    }
    
    func updateDataIntoTextField(_ customTextField: TueetorTextField, fromCell: Bool) {
        switch customTextField.tag {
        case 1:
            fromCell ? (customTextField.text = studentName) : (studentName = customTextField.text!)
        default:
            fromCell ? (customTextField.text = studentEmail) : (studentEmail = customTextField.text!)
        }
    }
    
    func isDataValid() -> (isValid: Bool, message: String) {
        var errorMessages: [String] = []
        
        let cell1 = tableView.cellForRow(at: IndexPath.init(row: 1, section: 0)) as? TueetorTextFieldTableViewCell
        cell1?.validateStudentName(text: studentName)
        errorMessages.append(cell1?.errorLabel.text ?? "")
        
        let cell2 = tableView.cellForRow(at: IndexPath.init(row: 2, section: 0)) as? TueetorTextFieldTableViewCell
        cell2?.validateEmail(text: studentEmail)
        errorMessages.append(cell2?.errorLabel.text ?? "")

        tableView.beginUpdates()
        tableView.endUpdates()
        let filteredErrorMessages = errorMessages.filter { (message) -> Bool in
            message.count > 0
        }

        let isValidData = filteredErrorMessages.count == 0
        return (isValid: isValidData, message: isValidData ? "" : filteredErrorMessages[0])
    }
    

}

extension RequestReviewViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard indexPath.row != 0 else {
            let cell = tableView.dequeueReusableCell(withIdentifier: RequestReviewInfoTableViewCell.cellIdentifier(), for: indexPath) as! RequestReviewInfoTableViewCell
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: TueetorTextFieldTableViewCell.cellIdentifier(), for: indexPath) as! TueetorTextFieldTableViewCell
        cell.configureCellForRowAtIndex(indexPath.row, withText: indexPath.row == 1 ? "STUDENT NAME".localized : "STUDENT EMAIL".localized)
        updateDataIntoTextField(cell.customTextField, fromCell: true)
        configureTextField(cell.customTextField)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        guard indexPath.row != 0 else {
            return RequestReviewInfoTableViewCell.cellHeight()
        }
        tableView.estimatedRowHeight = 63.0
        return UITableViewAutomaticDimension
    }
    
}

extension RequestReviewViewController: UITextFieldDelegate {
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        updateDataIntoTextField(textField as! TueetorTextField, fromCell: false)
        let currentIndexPath = IndexPath.init(row: textField.tag, section: 0)
        if textField.tag == 1 {
            let cell = tableView.cellForRow(at: currentIndexPath) as! TueetorTextFieldTableViewCell
            cell.validateStudentName(text: textField.text ?? "")
        } else {
            let cell = tableView.cellForRow(at: currentIndexPath) as! TueetorTextFieldTableViewCell
            cell.validateEmail(text: textField.text ?? "")
        }
        tableView.beginUpdates()
        tableView.endUpdates()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (textField.text?.isEmpty)! && string == " " {
            return false
        }
        let char = string.cString(using: String.Encoding.utf8)!
        let isBackSpace = strcmp(char, "\\b")
        if (isBackSpace == -92) {
            return true
        }
        if textField.tag == 1 {
            return (textField.text?.count)! < EmailMaxLimit
        } else {
            return (textField.text?.count)! < NameMaxLimit
        }
    }
    
}

extension RequestReviewViewController {
    
    func requestReviewFromUser() {
        
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }
        guard let userID = UserStore.shared.userID else {
            return
        }

        Utilities.showHUD(forView: self.view, excludeViews: [])
        let reviewObserver = ApiManager.shared.apiService.requestReviewForUserID(userID, studentName: studentName, studentEmail: studentEmail)
        let reviewDisposable = reviewObserver.subscribe(onNext: {(message) in
            Utilities.hideHUD(forView: self.view)

            DispatchQueue.main.async(execute: {
                let alert = UIAlertController(title: AppName, message: message, preferredStyle: .alert)
                let okAction = UIAlertAction(title: AlertButton.ok.description(), style: .default, handler: { (action) in
                  self.navigationController?.popViewController(animated: true)
                })
                alert.addAction(okAction)
                self.present(alert, animated: true, completion: nil)
            })
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.showSimpleAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        })
        reviewDisposable.disposed(by: disposableBag)
    }
    
}
