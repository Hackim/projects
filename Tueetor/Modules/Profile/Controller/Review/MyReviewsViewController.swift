//
//  MyReviewsViewController.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 01/08/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit
import RxSwift

class MyReviewsViewController: BaseViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var searchTextFieldWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var emptyMessageLabel: UILabel!
    @IBOutlet weak var addReviewButton: UIButton!
    
    
    var disposableBag = DisposeBag()
    var isFetchingData = false
    var isSearchActive = false
    var isFirstTime = true
    var searchString = ""
    
    var reviewsPagination = MyReviewPagination.init()
    var reviewDisposable: Disposable!
    
    private let refreshControl = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 10.0, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.addSubview(refreshControl)
        }
        
        searchTextFieldWidthConstraint.constant = 0.0
        refreshControl.addTarget(self, action: #selector(refreshData(_:)), for: .valueChanged)
        fetchReviews()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        (self.tabBarController as! BaseTabBarViewController).tabBar.isHidden = true
        (self.tabBarController as! BaseTabBarViewController).hideCenterButton()
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        addReviewButton.layer.shadowOffset = CGSize.init(width: 0, height: 5)
        addReviewButton.layer.shadowRadius = 3
        addReviewButton.layer.shadowColor = themeBlueColor.cgColor
        addReviewButton.layer.shadowOpacity = 0.75
    }
    
    @objc func refreshData(_ sender: Any) {
        self.reviewsPagination.paginationType = .new
        fetchReviews()
        if let refreshControl = sender as? UIRefreshControl {
            refreshControl.endRefreshing()
        }
    }
    
    @IBAction func searchButtonTapped(_ sender: UIButton) {
        isSearchActive = !isSearchActive
        if isSearchActive {
            searchTextField.becomeFirstResponder()
        } else {
            searchTextField.resignFirstResponder()
        }
        self.view.layoutIfNeeded()
        UIView.animate(withDuration: 0.7, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 1, options: .curveEaseIn, animations: {
            self.searchTextFieldWidthConstraint.constant = self.isSearchActive ? ScreenWidth - 132.0 : 0
            let imageName = self.isSearchActive ? "search_active" : "search_inactive"
            self.searchButton.setImage(UIImage(named: imageName), for: .normal)
            self.searchButton.backgroundColor = self.isSearchActive ? themeBlueColor : UIColor.clear
            self.view.layoutIfNeeded()
        }) { (completed) in
            
        }
    }
    
    @IBAction func valueChangedInSearchField(_ sender: Any) {
        if (reviewDisposable != nil) {
            Utilities.hideHUD(forView: view)
            reviewDisposable.dispose()
        }
        reviewsPagination.paginationType = .new
        fetchReviews()
    }
    
    @IBAction func addReviewButtonTapped(_ sender: UIButton) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        guard let user = appDelegate.user, user.paymentStatus == true else {
            let alert = UIAlertController(title: AppName, message: AlertMessage.tPakRenewal.description(), preferredStyle: .alert)
            let laterAction = UIAlertAction(title: AlertButton.later.description(), style: .default, handler: nil)
            alert.addAction(laterAction)

            let renewNowAction = UIAlertAction(title: AlertButton.renewNow.description(), style: .default, handler: { (action) in
                let subscriptionListingVC = SubscriptionListingViewController.instantiateFromAppStoryboard(appStoryboard: .Profile)
                self.navigationController?.pushViewController(subscriptionListingVC, animated: true)
            })
            alert.addAction(renewNowAction)
            self.present(alert, animated: true, completion: nil)
            return
        }
        let reqReviewController = RequestReviewViewController.instantiateFromAppStoryboard(appStoryboard: .Profile)
        navigationController?.pushViewController(reqReviewController, animated: true)
    }
}

extension MyReviewsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if reviewsPagination.reviews.count == 0 {
            return 2
        }

        return reviewsPagination.reviews.count + 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard indexPath.row != 0 else {
            let cell = tableView.dequeueReusableCell(withIdentifier: MyReviewHeaderTableViewCell.cellIdentifier(), for: indexPath) as! MyReviewHeaderTableViewCell
            cell.configureCell()
            return cell
        }
        
        guard reviewsPagination.reviews.count != 0 else {
            let cell = tableView.dequeueReusableCell(withIdentifier: EmptyMessageTableViewCell.cellIdentifier(), for: indexPath) as! EmptyMessageTableViewCell
            cell.messageLabel.text = self.searchString.isEmpty ? "You have received no reviews so far.".localized : "No reviews found with \(self.searchString)".localized
            return cell
        }
        
        guard indexPath.row != reviewsPagination.reviews.count + 1 else {
            let cell = tableView.dequeueReusableCell(withIdentifier: LoadMoreTableViewCell.cellIdentifier(), for: indexPath) as! LoadMoreTableViewCell
            cell.activityIndicator.startAnimating()
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: MyReviewTableViewCell.cellIdentifier(), for: indexPath) as! MyReviewTableViewCell
        cell.configureCellWithReview(reviewsPagination.reviews[indexPath.row - 1])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        guard indexPath.row != 0 else {
            return MyReviewHeaderTableViewCell.cellHeight()
        }
        guard reviewsPagination.reviews.count != 0 else {
            tableView.estimatedRowHeight = 44.0
            return UITableViewAutomaticDimension
        }
        if indexPath.row == reviewsPagination.reviews.count + 1 {
            return reviewsPagination.hasMoreToLoad ? 50.0 : 0
        }
        tableView.estimatedRowHeight = 156.0
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == reviewsPagination.reviews.count  && self.reviewsPagination.hasMoreToLoad && !isFetchingData {
            fetchReviews()
        }
    }
    
    
}

extension MyReviewsViewController: UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if isFetchingData == false, searchString != textField.text {
            reviewsPagination.paginationType = .new
            fetchReviews()
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (textField.text?.isEmpty)! && string == " " {
            return false
        }
        let char = string.cString(using: String.Encoding.utf8)!
        let isBackSpace = strcmp(char, "\\b")
        if (isBackSpace == -92) {
            return true
        }
        return true
    }
}

extension MyReviewsViewController {
    
    func fetchReviews() {
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }
        searchString = searchTextField.text ?? ""
        guard let userID = UserStore.shared.userID else {
            return
        }
        
        if reviewsPagination.paginationType != .old {
            self.reviewsPagination.currentPageNumber = 1
            if isFirstTime == true {
                Utilities.showHUD(forView: self.view, excludeViews: [])
            } else {
                UIApplication.shared.isNetworkActivityIndicatorVisible = true
            }
            
        } else {
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
        }
        
        let reviewObserver = ApiManager.shared.apiService.fetchReviewsForUserID(userID, page: reviewsPagination.currentPageNumber, search: searchString)
        reviewDisposable = reviewObserver.subscribe(onNext: {(paginationObject) in
            self.isFetchingData = false
            
            self.reviewsPagination.appendDataFromObject(paginationObject)
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if self.reviewsPagination.paginationType != .old && self.isFirstTime == true {
                    self.isFirstTime = false
                } else {
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                }
                self.tableView.reloadData()
            })
        }, onError: {(error) in
            self.isFetchingData = false
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
            })
        })
        reviewDisposable.disposed(by: disposableBag)
    }
    

}
