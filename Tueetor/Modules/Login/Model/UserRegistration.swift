//
//  UserRegistration.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 10/04/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import Foundation

struct UserRegistration {
    
    var email = ""
    var confirmEmail = ""
    var password = ""
    var confirmPassword = ""
    var title = ""
    var gender = ""
    var firstName = ""
    var lastName = ""
    var displayName = ""
    var birthday = ""
    var addressLine1 = ""
    var addressLine2 = ""
    var latitude = ""
    var longitude = ""
    var city = ""
    var zipCode = ""
    var country = ""
    var countryID = 0
    var phone = ""
    var isStudent = true
    var fbID = ""
    var imageURL = ""
    
    init() {
        
    }

    mutating func copyValuesFromFBDict(_ dict: [String: AnyObject]) {
        self.email = dict["email"] as? String ?? ""
        self.confirmEmail = dict["email"] as? String ?? ""
        self.lastName = dict["last_name"] as? String ?? ""
        self.fbID = dict["fb_id"] as? String ?? ""
        self.firstName = dict["first_name"] as? String ?? ""
        self.imageURL = dict["url"] as? String ?? ""
    }

}
