//
//  Country.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 24/04/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import Foundation
import Unbox

class Country: Unboxable {
    
    var id: Int!
    var code: String!
    var name: String!
    var currencyCode: String!
    var currencySymbol: String?
    var currencyDigitalCode: String!
    var currencyName: String!
    
    
    
    required init(unboxer: Unboxer) throws {
        self.id = unboxer.unbox(key: "country_id")
        self.code = unboxer.unbox(key: "code")
        self.name = unboxer.unbox(key: "name")
        self.currencyCode = unboxer.unbox(key: "currency_code")
        self.currencySymbol = unboxer.unbox(key: "currency_symbol")
        self.currencyDigitalCode = unboxer.unbox(key: "currency_digital_code")
        self.currencyName = unboxer.unbox(key: "currency_name")
    }
    
}

