//
//  User.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 24/04/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import Foundation
import Unbox

class User: Unboxable {
    
    var ID: Int!
    var fbID: String!
    var title: String?
    var displayName: String!
    var firstName: String!
    var lastName: String!
    var email: String!
    var address1: String?
    var address2: String?
    var city: String?
    var countryID: String?
    var phone: String?
    var zipCode: String?
    var latitude: String?
    var longitude: String?
    var dob: String?
    var about: String?
    var imageFile: String!
    var imageURL: String!
    var status: String?
    var userType: String?
    var gender: String?
    var userToken: String!
    var redirectToVerify: String!
    var country: String = ""
    var profileUrl: String = ""
    var averageRating: String = "0"
    var paymentStatus = false
    var createdOn: String!
    var validTill: Int!
    var validTillDateFormat: String!
    
    required init(unboxer: Unboxer) throws {
        self.ID = unboxer.unbox(key: "id") ?? 0
        UserStore.shared.userID = self.ID
        self.fbID = unboxer.unbox(key: "fb_id") ?? ""
        self.title = unboxer.unbox(key: "title")
        self.displayName = unboxer.unbox(key: "name") ?? ""
        self.firstName = unboxer.unbox(key: "first_name") ?? ""
        self.lastName = unboxer.unbox(key: "last_name") ?? ""
        self.email = unboxer.unbox(key: "email") ?? ""
        self.address1 = unboxer.unbox(key: "address1")
        self.address2 = unboxer.unbox(key: "address2")
        self.city = unboxer.unbox(key: "city")
        self.countryID = unboxer.unbox(key: "country_id")
        self.phone = unboxer.unbox(key: "contact")
        self.zipCode = unboxer.unbox(key: "postcode")
        self.latitude = unboxer.unbox(key: "lattitude")
        self.longitude = unboxer.unbox(key: "longitude")
        self.dob = unboxer.unbox(key: "dob") ?? ""
        self.about = unboxer.unbox(key: "about") ?? ""
        self.profileUrl = unboxer.unbox(key: "profile_url") ?? ""
        self.redirectToVerify = unboxer.unbox(key: "redirectToVerify") ?? ""
        self.imageFile = unboxer.unbox(key: "image") ?? ""
        self.imageURL = self.imageFile
        self.country = Utilities.getCountryNameForID(self.countryID)
        self.status = unboxer.unbox(key: "status")
        self.userType = unboxer.unbox(key: "user_type")
        self.gender = unboxer.unbox(key: "gender") ?? ""
        if self.gender == "M".localized {
            self.gender = "Male".localized
        } else {
            self.gender = "Female".localized
        }
        self.userToken = unboxer.unbox(key: "user_token")
        let inputDateFormatter = DateFormatter()
        inputDateFormatter.dateFormat = "yyyy-MM-dd"
        if let date = inputDateFormatter.date(from: self.dob!) {
            self.dob = Utilities.getFormattedDate(date, dateFormat: "dd-MMM-yyyy")
        }
        self.averageRating = unboxer.unbox(key: "average_rating") ?? "0"
        self.paymentStatus = unboxer.unbox(keyPath: "payment_status.status") ?? false
        self.createdOn = unboxer.unbox(key: "addedon") ?? "2018-06-13 09:46:52"
        self.validTill = 0
        if let paymentStatusDict = unboxer.dictionary["payment_status"] as? [String: AnyObject],
            let planDict = paymentStatusDict["plan"] as? [String: AnyObject], let planEndTime = planDict["current_period_end"] as? Int {
            self.validTill = planEndTime
        }
        self.validTillDateFormat = self.getDate(unixdate: self.validTill)
    }
    
    init(_ user: User) {
        self.ID = user.ID
        self.fbID = user.fbID
        self.title = user.title
        self.displayName = user.displayName
        self.firstName = user.firstName
        self.lastName = user.lastName
        self.email = user.email
        self.address1 = user.address1 ?? ""
        self.address2 = user.address2 ?? ""
        self.city = user.city ?? ""
        self.profileUrl = user.profileUrl
        self.countryID = user.countryID ?? ""
        self.phone = user.phone ?? ""
        self.zipCode = user.zipCode ?? ""
        self.latitude = user.latitude ?? ""
        self.longitude = user.longitude ?? ""
        self.dob = user.dob ?? ""
        self.about = user.about ?? ""
        self.redirectToVerify = user.redirectToVerify ?? ""
        self.imageFile = user.imageFile
        self.imageURL = user.imageURL
        self.status = user.status
        self.userType = user.userType
        self.gender = user.gender ?? ""
        self.userToken = user.userToken ?? ""
        self.country = user.country
        self.averageRating = "0"
        self.paymentStatus = false
        self.createdOn = "2018-06-13 09:46:52"
        self.validTill = 0
        self.validTillDateFormat = ""
    }
 
    func getDate(unixdate: Int) -> String {
        if unixdate == 0 {return ""}
        let date = NSDate(timeIntervalSince1970: TimeInterval(unixdate))
        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.dateFormat = "dd-MMM-yyyy"
        dayTimePeriodFormatter.timeZone = TimeZone.current
        let dateString = dayTimePeriodFormatter.string(from: date as Date)
        return "\(dateString)"
    }
}
