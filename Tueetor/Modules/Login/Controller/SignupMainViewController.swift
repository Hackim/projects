//
//  SignupMainViewController.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 19/04/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit
import RxSwift

class SignupMainViewController: BaseViewController {
    
    @IBOutlet weak var studentButton: UIButton!
    @IBOutlet weak var tutorButton: UIButton!
    
    @IBOutlet weak var signupEmailButtonView: UIView!
    @IBOutlet weak var facebookButtonView: UIView!
    
    @IBOutlet weak var facebookButtonHeight: NSLayoutConstraint!
    @IBOutlet weak var termsTextView: UITextView!

    @IBOutlet weak var signinButton: UIButton!
    @IBOutlet weak var signinTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var signinBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var verticalSpacingConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var emailImageView: UIImageView!
    
    @IBOutlet weak var facebookLabel: UILabel!
    @IBOutlet weak var facebookImageView: UIImageView!
    
    @IBOutlet weak var emailSignupButton: UIButton!
    @IBOutlet weak var emailSignupImageView: UIImageView!
    
    @IBOutlet weak var facebookSignupButton: UIButton!
    
    let unselectedColor = UIColor(white: 1, alpha: 0.4)
    var selectedOption = 0
    let fontStyle = UIFont(name: "Montserrat-Regular", size: 15.0)!
    let showTermsURL = URL(string: "action://show-terms")!
    var disposableBag = DisposeBag()
    var isOptionButtonTapped = false
    var increaseInDistance: CGFloat = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        setupUIComponents()
        if #available(iOS 11.0, *) {
            scrollView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentBehavior.never
        } else {
            // Fallback on earlier versions
        }
        increaseInDistance = (ScreenHeight / 6)
        verticalSpacingConstraint.constant += increaseInDistance
        signinTopConstraint.constant -= increaseInDistance
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        resetSelection()
    }
    
    func setupUIComponents() {
        studentButton.layer.cornerRadius = 25
        studentButton.layer.borderColor = UIColor.white.cgColor
        studentButton.layer.borderWidth = 1.0

        tutorButton.layer.cornerRadius = 25
        tutorButton.layer.borderColor = UIColor.white.cgColor
        tutorButton.layer.borderWidth = 1.0
        configureButtons()
        
        //Signin Button
        let attributedString = NSMutableAttributedString()
        attributedString.append(NSAttributedString(string: "Already have an account ",
                                                   attributes: [.font: UIFont(name: "Montserrat-Light", size: 19.0)!,
                                                                .foregroundColor : UIColor.white]))
        attributedString.append(NSAttributedString(string: "SIGN IN",
                                                   attributes: [.font: UIFont(name: "Montserrat-Regular", size: 19.0)!,
                                                                .foregroundColor : UIColor.white,
                                                                .underlineStyle: NSUnderlineStyle.styleSingle.rawValue,
                                                                .underlineColor: UIColor.white]))
        
        signinButton.titleLabel?.attributedText = attributedString

        setupTextView()
    }
    
    func configureButtons() {
        
        // Email Button
        signupEmailButtonView.layer.cornerRadius = 28
        signupEmailButtonView.layer.borderColor = UIColor.white.cgColor
        signupEmailButtonView.layer.borderWidth = 1.0
        signupEmailButtonView.isHidden = true
        
        // Facebook Button
        facebookButtonView.layer.cornerRadius = 28
        facebookButtonView.layer.borderColor = UIColor.white.cgColor
        facebookButtonView.layer.borderWidth = 1.0
        facebookButtonView.isHidden = true
        
        emailSignupButton.addTarget(self, action: #selector(signupTouchUpInside), for: UIControlEvents.touchUpInside)
        emailSignupButton.addTarget(self, action: #selector(signupDragExit), for: UIControlEvents.touchDragExit);
        emailSignupButton.addTarget(self, action: #selector(signupHoldDown), for: UIControlEvents.touchDown)
        
        facebookSignupButton.addTarget(self, action: #selector(facebookTouchUpInside), for: UIControlEvents.touchUpInside)
        facebookSignupButton.addTarget(self, action: #selector(facebookDragExit), for: UIControlEvents.touchDragExit);
        facebookSignupButton.addTarget(self, action: #selector(facebookHoldDown), for: UIControlEvents.touchDown)
    }
    
    @objc func signupHoldDown() {
        signupEmailButtonView.backgroundColor = UIColor.white
        emailLabel.textColor = themeBlueColor
        emailImageView.image = UIImage(named: "email_blue")
    }
    
    @objc func signupTouchUpInside() {
        signupEmailButtonView.backgroundColor = UIColor.clear
        emailLabel.textColor = UIColor.white
        emailImageView.image = UIImage(named: "mail")
        let signupAccountViewController = SignupAccountViewController.instantiateFromAppStoryboard(appStoryboard: .Login)
        signupAccountViewController.isStudent = selectedOption == 1
        self.navigationController?.pushViewController(signupAccountViewController, animated: true)

    }
    
    @objc func signupDragExit() {
        signupEmailButtonView.backgroundColor = UIColor.clear
        emailLabel.textColor = UIColor.white
        emailImageView.image = UIImage(named: "mail")
    }
    
    @objc func facebookHoldDown() {
        facebookButtonView.backgroundColor = UIColor.white
        facebookLabel.textColor = themeBlueColor
        facebookImageView.image = UIImage(named: "facebook")
    }
    
    @objc func facebookTouchUpInside() {
        facebookButtonView.backgroundColor = UIColor.clear
        facebookLabel.textColor = UIColor.white
        facebookImageView.image = UIImage(named: "facebook_white")
        let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
        fbLoginManager.logIn(withReadPermissions: ["public_profile", "email"], from: self) { (result, error) -> Void in
            if (error == nil){
                let fbloginresult : FBSDKLoginManagerLoginResult = result!
                if (result?.isCancelled)!{
                    return
                }
                if(fbloginresult.grantedPermissions.contains("email")) {
                    self.getFBUserData()
                }
            }
        }
    }
    
    @objc func facebookDragExit() {
        facebookButtonView.backgroundColor = UIColor.clear
        facebookLabel.textColor = UIColor.white
        facebookImageView.image = UIImage(named: "facebook_white")
    }

    func setupTextView() {
        let attributedString = NSMutableAttributedString()
        attributedString.append(NSAttributedString(string: "By tapping Create Account or Facebook option, I agree to Tueetor's ",
                                                   attributes: [.font: fontStyle,
                                                                .foregroundColor : UIColor.white]))
        attributedString.append(NSAttributedString(string: "Terms of Services and Privacy Policy",
                                                   attributes: [.link: showTermsURL,
                                                                .underlineStyle: NSUnderlineStyle.styleSingle.rawValue,
                                                                .underlineColor: UIColor.white,
                                                                .foregroundColor : UIColor.white,
                                                                .font: fontStyle]))
        termsTextView.delegate = self
        termsTextView.attributedText = attributedString
        termsTextView.linkTextAttributes = [NSAttributedStringKey.foregroundColor.rawValue: UIColor.white]
        
    }

    @IBAction func laterButtonTapped(_ sender: UIButton) {
        guard let window = UIApplication.shared.keyWindow else {
            return
        }
        
        guard let rootViewController = window.rootViewController else {
            return
        }
        
        let tabBarController = BaseTabBarViewController()
        tabBarController.selectedIndex = 2
        tabBarController.view.frame = rootViewController.view.frame
        tabBarController.view.layoutIfNeeded()
        
        UIView.transition(with: window, duration: 0.3, options: .transitionCrossDissolve, animations: {
            window.rootViewController = tabBarController
        }, completion: { completed in
            UserStore.shared.isSkipSelected = true
            // maybe do something here
        })
    }

    @IBAction func studentButtonTapped(_ sender: UIButton) {
        guard selectedOption != 1 else {
            return
        }
        selectedOption = 1
        resetSelection()
        studentButton.backgroundColor = UIColor.white
        studentButton.setTitleColor(themeColor, for: .normal)
        
        tutorButton.backgroundColor = UIColor.clear
        tutorButton.setTitleColor(UIColor.white, for: .normal)
        
        UIView.animate(withDuration: 0.2) {
            if self.isOptionButtonTapped == false {
                self.isOptionButtonTapped = true
                self.verticalSpacingConstraint.constant = 18.0
                self.signinTopConstraint.constant = 38.0
            }

            self.signupEmailButtonView.isHidden = false
            self.termsTextView.isHidden = false
            self.facebookButtonView.isHidden = false
            
            self.facebookButtonHeight.constant = 56.0
            self.view.layoutIfNeeded()
        }

    }
    
    @IBAction func tutorButtonTapped(_ sender: UIButton) {
        guard selectedOption != 2 else {
            return
        }
        selectedOption = 2
        resetSelection()
        tutorButton.backgroundColor = UIColor.white
        tutorButton.setTitleColor(themeColor, for: .normal)
        
        studentButton.backgroundColor = UIColor.clear
        studentButton.setTitleColor(UIColor.white, for: .normal)

        self.signupEmailButtonView.isHidden = false
        self.termsTextView.isHidden = false
        UIView.animate(withDuration: 0.2) {
            if self.isOptionButtonTapped == false {
                self.isOptionButtonTapped = true
                self.verticalSpacingConstraint.constant = 18.0
                self.signinTopConstraint.constant = 38.0
            }
            
            self.facebookButtonHeight.constant = 0
            self.view.layoutIfNeeded()
        }
    }
    
    func resetSelection() {
     
        facebookLabel.textColor = UIColor.white
        facebookImageView.image = UIImage(named: "facebook_white")
        facebookButtonView.backgroundColor = UIColor.clear

        emailLabel.textColor = UIColor.white
        emailImageView.image = UIImage(named: "mail")
        signupEmailButtonView.backgroundColor = UIColor.clear

    }
    
    @IBAction func emailButtonTapped(_ sender: UIButton) {
        emailLabel.textColor = themeColor
        emailImageView.image = UIImage(named: "email_blue")
        signupEmailButtonView.backgroundColor = UIColor.white

        facebookLabel.textColor = UIColor.white
        facebookImageView.image = UIImage(named: "facebook_white")
        facebookButtonView.backgroundColor = UIColor.clear

    }
    
    @IBAction func facebookButtonTapped(_ sender: UIButton) {
        facebookLabel.textColor = themeColor
        facebookImageView.image = UIImage(named: "facebook")
        facebookButtonView.backgroundColor = UIColor.white
        
        emailLabel.textColor = UIColor.white
        emailImageView.image = UIImage(named: "mail")
        signupEmailButtonView.backgroundColor = UIColor.clear

    }
    
    func getFBUserData(){
        if((FBSDKAccessToken.current()) != nil){
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email"]).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil){
                    print(result as! [String: Any])
                    if let fbObject = result as? [String: Any] {
                        if let email = fbObject["email"] as? String {
                            var loginObject = [
                                "email": email,
                                "first_name": fbObject["first_name"] as! String,
                                "last_name": fbObject["last_name"] as! String,
                                "fb_id": fbObject["id"] as! String
                                ] as [String : AnyObject]
                            if let picutureDict = fbObject["picture"] as? [String: AnyObject],
                                let dataDict = picutureDict["data"] as? [String: AnyObject],
                                let isSilhouetter = dataDict["is_silhouette"] as? Int, isSilhouetter == 0,
                                let url = dataDict["url"] as? String {
                                loginObject["url"] = url as AnyObject
                            }
                            print(loginObject)
                            //Social login happens here
                            DispatchQueue.main.async {
                                self.checkEmailID(email, fbDict: loginObject)
                            }
                        } else {
                            self.showSimpleAlertWithMessage("An email ID is not associated with this account.".localized)
                        }
                    } else {
                        self.showSimpleAlertWithMessage("Failed while trying to login with Facebook".localized)
                    }
                }
            })
        }
    }

    
}

extension SignupMainViewController: UITextViewDelegate {
    
    @available(iOS, deprecated: 10.0)
    func textView(_ textView: UITextView, shouldInteractWith url: URL, in characterRange: NSRange) -> Bool {
        if url == showTermsURL, let terms = termsURL {
            pushToWebViewWithURL(terms, title: "Terms".localized)
            return true
        }
        
        return false
    }
    
    @available(iOS 10.0, *)
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        if URL == showTermsURL, let terms = termsURL {
            pushToWebViewWithURL(terms, title: "Terms".localized)
            return true
        }
        return false
    }
}

extension SignupMainViewController {
    
    func socialLoginWithDict(_ dict: [String: AnyObject]) {
        
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }
        
        Utilities.showHUD(forView: self.view, excludeViews: [])
        let loginObserver = ApiManager.shared.apiService.facebookLoginWithDict(dict)
        let loginDisposable = loginObserver.subscribe(onNext: {(user) in
            Utilities.hideHUD(forView: self.view)
            
            UserStore.shared.userEmail = user.email
            UserStore.shared.isLoggedIn = true
            
            DispatchQueue.main.async {
                Utilities.sendDeviceToken(nil)

                guard let window = UIApplication.shared.keyWindow else {
                    return
                }
                
                guard let rootViewController = window.rootViewController else {
                    return
                }
                
                let tabBarController = BaseTabBarViewController()
                tabBarController.selectedIndex = 2
                tabBarController.view.frame = rootViewController.view.frame
                tabBarController.view.layoutIfNeeded()
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.user = user
                UIView.transition(with: window, duration: 0.3, options: .transitionCrossDissolve, animations: {
                    window.rootViewController = tabBarController
                }, completion: nil)
            }
            print(user)
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.showSimpleAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        })
        loginDisposable.disposed(by: disposableBag)
    }
    
    func checkEmailID(_ email: String, fbDict: [String: AnyObject]) {
        
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }
        
        Utilities.showHUD(forView: self.view, excludeViews: [])
        let emailObserver = ApiManager.shared.apiService.checkAccountAvailability(email, phoneNumber: "")
        let emailDisposable = emailObserver.subscribe(onNext: {(isAvailable) in
            Utilities.hideHUD(forView: self.view)
            DispatchQueue.main.async {
                let signupAccountViewController = SignupAccountViewController.instantiateFromAppStoryboard(appStoryboard: .Login)
                signupAccountViewController.isStudent = true
                signupAccountViewController.facebookDict = fbDict
                self.navigationController?.pushViewController(signupAccountViewController, animated: true)
            }
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.socialLoginWithDict(fbDict)
                    }
                }
            })
        })
        emailDisposable.disposed(by: disposableBag)
    }

}
