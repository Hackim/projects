//
//  SignupAccountViewController.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 10/04/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit
import RxSwift

// MARK: - Class -

class SignupAccountViewController: BaseViewController {

    //MARK: - IBOutlets
    
    @IBOutlet weak var tableView: UITableView!
    
    //MARK: - Variables
    
    var activeTextField: UITextField!
    let labelFields = ["EMAIL ADDRESS".localized, "CONFIRM EMAIL ADDRESS".localized, "PASSWORD".localized, "CONFIRM PASSWORD".localized]
    var userRegistration = UserRegistration()
    var isStudent: Bool!
    var facebookDict: [String: AnyObject]!
    var isFirstTime = true
    var isEmailInvalid = true
    var isCheckUnderProcess = false
    var disposableBag = DisposeBag()

    //MARK: - Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        userRegistration.isStudent = isStudent
        tableView.tableFooterView = UIView()
        if let fbDict = facebookDict {
            self.userRegistration.copyValuesFromFBDict(fbDict)
            self.tableView.reloadData()
        } else {
            isFirstTime = false
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }
    
    //MARK: - IBActions

    @IBAction func nextButtonTapped(_ sender: TueetorButton) {
        self.view.endEditing(true)
        let validationInformation = isDataValid()
        guard validationInformation.isValid else {
            if !validationInformation.message.isEmpty {
                self.showSimpleAlertWithMessage(validationInformation.message)
            }
            return
        }
        let signUpProfileViewController = SignupProfileViewController.instantiateFromAppStoryboard(appStoryboard: .Login)
        signUpProfileViewController.userRegistration = self.userRegistration
        self.navigationController?.pushViewController(signUpProfileViewController, animated: true)
    }
 
    @IBAction func backButtonTapped(_ sender: UIButton) {
        self.view.endEditing(true)
        if userRegistration.email != "" ||
            userRegistration.confirmEmail != "" ||
            userRegistration.password != "" ||
            userRegistration.confirmPassword != "" {
            let alert = UIAlertController(title: AppName, message: AlertMessage.removeFieldData.description(), preferredStyle: .alert)
            let yesAction = UIAlertAction(title: AlertButton.yes.description(), style: .default) { (action) in
                self.navigateBack(sender: sender)
            }
            
            let noAction =  UIAlertAction(title: AlertButton.no.description(), style: .default, handler: nil)
            alert.addAction(yesAction)
            alert.addAction(noAction)
            self.present(alert, animated: true, completion: nil)
        } else {
            navigateBack(sender: sender)
        }
    }
    
    @IBAction func loginButtonTapped(_ sender: UIButton) {
        let alert = UIAlertController(title: AppName, message: AlertMessage.exitSignupProcess.description(), preferredStyle: .alert)
        let yesAction = UIAlertAction(title: AlertButton.yes.description(), style: .default) { (action) in
            self.navigationController?.popToRootViewController(animated: true)
        }
        
        let noAction =  UIAlertAction(title: AlertButton.no.description(), style: .default, handler: nil)
        alert.addAction(yesAction)
        alert.addAction(noAction)
        self.present(alert, animated: true, completion: nil)
    }

    //MARK: - Helper Methods

    func configureTextField(_ customTextField: TueetorTextField) {
        customTextField.delegate = self
        customTextField.isSecureTextEntry = false
        customTextField.keyboardType = .asciiCapable

        switch customTextField.tag {
        case SignupAccountTextFieldType.email.rawValue,
             SignupAccountTextFieldType.confirmEmail.rawValue:
            if isFirstTime == true {
                customTextField.showImage()
                isFirstTime = false
            }
            customTextField.keyboardType = .emailAddress
        case SignupAccountTextFieldType.password.rawValue,
             SignupAccountTextFieldType.confirmPassword.rawValue:
            customTextField.keyboardType = .asciiCapable
            customTextField.isSecureTextEntry = true
        default:
            customTextField.keyboardType = .asciiCapable
        }
    }

    func updateDataIntoTextField(_ customTextField: TueetorTextField, fromCell: Bool) {
        switch customTextField.tag {
        case SignupAccountTextFieldType.email.rawValue:
            fromCell ? (customTextField.text = userRegistration.email) : (userRegistration.email = customTextField.text!)
            break
        case SignupAccountTextFieldType.confirmEmail.rawValue:
            fromCell ? (customTextField.text = userRegistration.confirmEmail) : (userRegistration.confirmEmail = customTextField.text!)
            break
        case SignupAccountTextFieldType.password.rawValue:
            fromCell ? (customTextField.text = userRegistration.password) : (userRegistration.password = customTextField.text!)
            break
        case SignupAccountTextFieldType.confirmPassword.rawValue:
            fromCell ? (customTextField.text = userRegistration.confirmPassword) : (userRegistration.confirmPassword = customTextField.text!)
            break
        default:
            break
        }
    }

    func isDataValid() -> (isValid: Bool, message: String) {
        
        var errorMessages: [String] = []
        if isEmailInvalid == true, !userRegistration.email.isEmpty {
            checkEmailID()
            isCheckUnderProcess = true
            return (isValid: false, message: "")
        } else {
            for index in 1...labelFields.count {
                let currentIndexPath = IndexPath.init(row: index, section: 0)
                let cell = tableView.cellForRow(at: currentIndexPath) as! TueetorTextFieldTableViewCell
                switch index {
                case SignupAccountTextFieldType.email.rawValue:
                    cell.validateEmail(text: userRegistration.email)
                case SignupAccountTextFieldType.confirmEmail.rawValue:
                    cell.validateConfirmEmail(text: userRegistration.confirmEmail, email: userRegistration.email)
                case SignupAccountTextFieldType.password.rawValue:
                    cell.validatePassword(text: userRegistration.password)
                case SignupAccountTextFieldType.confirmPassword.rawValue:
                    cell.validateConfirmPassword(text: userRegistration.confirmPassword, password: userRegistration.password)
                default:
                    break
                }
                let message = cell.errorLabel.text ?? ""
                if !message.isEmptyString() {
                    errorMessages.append(message)
                }
            }
            tableView.beginUpdates()
            tableView.endUpdates()
            let isValidData = errorMessages.count == 0
            return (isValid: isValidData, message: isValidData ? "" : errorMessages[0])
        }
    }
    
}

//MARK: - TableView Delegate and Datasource -

extension SignupAccountViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return labelFields.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard indexPath.row != 0 else {
            let cell = tableView.dequeueReusableCell(withIdentifier: SignupAccountHeaderTableViewCell.cellIdentifier(), for: indexPath) as! SignupAccountHeaderTableViewCell
            cell.messageLabel.text = isStudent ? "Let's check if your Student ID is available".localized : "Let's check if your Tutor ID is available".localized
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: TueetorTextFieldTableViewCell.cellIdentifier(), for: indexPath) as! TueetorTextFieldTableViewCell
        cell.configureCellForRowAtIndex(indexPath.row, withText: labelFields[indexPath.row - 1])
        updateDataIntoTextField(cell.customTextField, fromCell: true)
        configureTextField(cell.customTextField)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        guard indexPath.row != 0 else {
            return 219
        }
        tableView.estimatedRowHeight = 63.0
        return UITableViewAutomaticDimension
    }
    
}

//MARK: - TextField Delegate -

extension SignupAccountViewController: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        activeTextField = textField as! TueetorTextField
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        updateDataIntoTextField(textField  as! TueetorTextField, fromCell: false)
        let currentIndexPath = IndexPath.init(row: textField.tag, section: 0)
        let cell = tableView.cellForRow(at: currentIndexPath) as! TueetorTextFieldTableViewCell
        switch textField.tag {
        case SignupAccountTextFieldType.email.rawValue:
            cell.validateEmail(text: textField.text ?? "")
            if cell.errorLabel.text?.count == 0, !userRegistration.email.isEmpty {
                isEmailInvalid = true
                checkEmailID()
            }
        case SignupAccountTextFieldType.confirmEmail.rawValue:
            cell.validateConfirmEmail(text: textField.text ?? "", email: userRegistration.email)
        case SignupAccountTextFieldType.password.rawValue:
            cell.validatePassword(text: textField.text ?? "")
        case SignupAccountTextFieldType.confirmPassword.rawValue:
            cell.validateConfirmPassword(text: textField.text ?? "", password: userRegistration.password)
        default:
            break
        }
        tableView.beginUpdates()
        tableView.endUpdates()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (textField.text?.isEmpty)! && string == " " {
            return false
        }
        let char = string.cString(using: String.Encoding.utf8)!
        let isBackSpace = strcmp(char, "\\b")
        if (isBackSpace == -92) {
            return true
        }
        switch textField.tag {
        case SignupAccountTextFieldType.email.rawValue,
             SignupAccountTextFieldType.confirmEmail.rawValue:
            return (textField.text?.count)! < EmailMaxLimit
        case SignupAccountTextFieldType.password.rawValue,
             SignupAccountTextFieldType.confirmPassword.rawValue:
            return (textField.text?.count)! < PasswordMaxLimit
        default:
            return true
        }
    }
    
}

extension SignupAccountViewController {
    
    func checkEmailID() {
        
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }
        
        Utilities.showHUD(forView: self.view, excludeViews: [])
        let emailObserver = ApiManager.shared.apiService.checkAccountAvailability(userRegistration.email, phoneNumber: "")
        let emailDisposable = emailObserver.subscribe(onNext: {(isAvailable) in
            Utilities.hideHUD(forView: self.view)
            self.isEmailInvalid = false
            DispatchQueue.main.async(execute: {
                if self.isCheckUnderProcess == true {
                    self.isCheckUnderProcess = false
                    self.nextButtonTapped(TueetorButton())
                }
            })
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                self.isEmailInvalid = true
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        let cell = self.tableView.cellForRow(at: IndexPath.init(row: SignupAccountTextFieldType.email.rawValue, section: 0) ) as! TueetorTextFieldTableViewCell
                        cell.errorLabel.text = error.localizedDescription
                        cell.customTextField.hideImage()
                        self.tableView.beginUpdates()
                        self.tableView.endUpdates()
                        self.showSimpleAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        })
        emailDisposable.disposed(by: disposableBag)
    }
    
}
