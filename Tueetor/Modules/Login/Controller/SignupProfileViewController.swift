//
//  SignupProfileViewController.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 10/04/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit
import AVFoundation
import RxSwift

class SignupProfileViewController: BaseViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var activeTextField: UITextField!
    let labelFields = [["TITLE".localized, "GENDER".localized], ["FIRST NAME".localized], ["LAST NAME".localized], ["DISPLAY NAME".localized], [""], ["BIRTHDAY".localized]]
    var userRegistration: UserRegistration!
    let titlePickerView = UIPickerView()
    let genderPickerView = UIPickerView()
    let datePicker = UIDatePicker()
    var profileImage: UIImage!
    var isImageSelected = false
    var isDisplayNameEdited = false
    var name = ""
    var disposableBag = DisposeBag()
    var firstNameTick = true
    var lastNameTick = true
    var displayNameTick = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.tableFooterView = UIView()
        titlePickerView.delegate = self
        titlePickerView.dataSource = self
        titlePickerView.tag = 1
        
        genderPickerView.delegate = self
        genderPickerView.dataSource = self
        genderPickerView.tag = 2
        
        datePicker.datePickerMode = .date
        datePicker.maximumDate = Date()
        datePicker.addTarget(self, action: #selector(datePickerValueChanged(sender:)), for: .valueChanged)
        if userRegistration.fbID != "" {
            self.userRegistration.displayName = self.userRegistration.firstName.lowercased().trimmingCharacters(in: .whitespaces) + userRegistration.lastName.lowercased().trimmingCharacters(in: .whitespaces)
            getProfileLinkURLForName(self.userRegistration.displayName)
        } else {
            firstNameTick = false
            lastNameTick = false
            displayNameTick = false
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }


    @IBAction func nextButtonTapped(_ sender: TueetorButton) {
        let validationInformation = isDataValid()
        guard validationInformation.isValid else {
            self.showSimpleAlertWithMessage(validationInformation.message)
            return
        }
        guard isImageSelected else {
            self.showSimpleAlertWithMessage("Please select profile picture to continue.".localized)
            return
        }
        let signUpAddressViewController = SignupAddressViewController.instantiateFromAppStoryboard(appStoryboard: .Login)
        signUpAddressViewController.userRegistration = userRegistration
        if let image = profileImage {
            signUpAddressViewController.profilePicture = image
        }
        self.navigationController?.pushViewController(signUpAddressViewController, animated: true)
    }

    @IBAction func backButtonTapped(_ sender: UIButton) {
        self.view.endEditing(true)
        if userRegistration.firstName != "" ||
            userRegistration.lastName != "" ||
            userRegistration.displayName != "" ||
            userRegistration.title != "" ||
            userRegistration.gender != "" ||
            userRegistration.birthday != "" ||
            isImageSelected {
            let alert = UIAlertController(title: AppName, message: AlertMessage.removeFieldData.description(), preferredStyle: .alert)
            let yesAction = UIAlertAction(title: AlertButton.yes.description(), style: .default) { (action) in
                self.navigateBack(sender: sender)
            }
            
            let noAction =  UIAlertAction(title: AlertButton.no.description(), style: .default, handler: nil)
            alert.addAction(yesAction)
            alert.addAction(noAction)
            self.present(alert, animated: true, completion: nil)
        } else {
            navigateBack(sender: sender)
        }
    }
    
    @IBAction func loginButtonTapped(_ sender: UIButton) {
        let alert = UIAlertController(title: AppName, message: AlertMessage.exitSignupProcess.description(), preferredStyle: .alert)
        let yesAction = UIAlertAction(title: AlertButton.yes.description(), style: .default) { (action) in
            self.navigationController?.popToRootViewController(animated: true)
        }
        
        let noAction =  UIAlertAction(title: AlertButton.no.description(), style: .default, handler: nil)
        alert.addAction(yesAction)
        alert.addAction(noAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    func configureTextField(_ customTextField: TueetorTextField) {
        customTextField.delegate = self
        customTextField.isSecureTextEntry = false
        customTextField.keyboardType = .asciiCapable
        customTextField.inputView = nil
        switch customTextField.tag {
        case SignupProfileTextFieldType.dual.rawValue:
            if customTextField.title == "TITLE".localized {
                customTextField.inputView = titlePickerView
            } else {
                customTextField.inputView = genderPickerView
            }
            customTextField.icon = "drop-down-arrow"
            customTextField.showImage()
        case SignupProfileTextFieldType.birthday.rawValue:
            customTextField.inputView = datePicker
            customTextField.icon = "drop-down-arrow"
            customTextField.showImage()
        case SignupProfileTextFieldType.firstName.rawValue:
            customTextField.keyboardType = .asciiCapable
            if firstNameTick == true {
                customTextField.showImage()
                firstNameTick = false
            }
        case SignupProfileTextFieldType.displayName.rawValue:
            customTextField.keyboardType = .asciiCapable
            if displayNameTick == true {
                customTextField.showImage()
                displayNameTick = false
            }
        case SignupProfileTextFieldType.lastName.rawValue:
            customTextField.keyboardType = .asciiCapable
            if lastNameTick == true {
                customTextField.showImage()
                lastNameTick = false
            }
        default:
            customTextField.keyboardType = .asciiCapable
        }

    }
    
    func updateDataIntoTextField(_ customTextField: TueetorTextField, fromCell: Bool) {
        switch customTextField.tag {
        case SignupProfileTextFieldType.dual.rawValue:
            if customTextField.title == "TITLE".localized {
                fromCell ? (customTextField.text = userRegistration.title) : (userRegistration.title = customTextField.text!)
            } else {
                fromCell ? (customTextField.text = userRegistration.gender) : (userRegistration.gender = customTextField.text!)
            }
            break
        case SignupProfileTextFieldType.firstName.rawValue:
            fromCell ? (customTextField.text = userRegistration.firstName) : (userRegistration.firstName = customTextField.text!)
            if isDisplayNameEdited == false && fromCell == false {
                let displayName = userRegistration.firstName + " " + userRegistration.lastName
                userRegistration.displayName = displayName
                let cell = tableView.cellForRow(at: IndexPath.init(row: SignupProfileTextFieldType.displayName.rawValue, section: 0)) as! TueetorTextFieldTableViewCell
                cell.customTextField.text = displayName
                if !displayName.isEmpty, fromCell == false {
                    getProfileLinkURLForName(displayName)
                }
            }
            break
        case SignupProfileTextFieldType.lastName.rawValue:
            fromCell ? (customTextField.text = userRegistration.lastName) : (userRegistration.lastName = customTextField.text!)
            if isDisplayNameEdited == false && fromCell == false {
                let displayName = userRegistration.firstName + " " + userRegistration.lastName
                userRegistration.displayName = displayName
                let cell = tableView.cellForRow(at: IndexPath.init(row: SignupProfileTextFieldType.displayName.rawValue, section: 0)) as! TueetorTextFieldTableViewCell
                cell.customTextField.text = displayName
                if !displayName.isEmpty, fromCell == false {
                    getProfileLinkURLForName(displayName)
                }
            }
            break
        case SignupProfileTextFieldType.displayName.rawValue:
            fromCell ? (customTextField.text = userRegistration.displayName) : (userRegistration.displayName = customTextField.text!)
            if let displayName = customTextField.text, !displayName.isEmpty, fromCell == false {
                getProfileLinkURLForName(displayName)
            }
            break
        case SignupProfileTextFieldType.birthday.rawValue:
            fromCell ? (customTextField.text = userRegistration.birthday) : (userRegistration.birthday = customTextField.text!)
            break
        default:
            break
        }
    }
    
    @objc func datePickerValueChanged(sender:UIDatePicker) {
        activeTextField.text = Utilities.getFormattedDate(sender.date, dateFormat: "dd-MMM-yyyy")
    }

    func isDataValid() -> (isValid: Bool, message: String) {
        var errorMessages: [String] = []
        for index in 1...labelFields.count {
            let currentIndexPath = IndexPath.init(row: index, section: 0)
            switch index {
            case SignupProfileTextFieldType.dual.rawValue:
                let dualCell = tableView.cellForRow(at: currentIndexPath) as! TueetorDualTextFieldTableViewCell
                dualCell.validateTitle(text: userRegistration.title)
                dualCell.validateGender(text: userRegistration.gender)
                errorMessages.append(dualCell.errorMessageLabelOne.text ?? "")
                errorMessages.append(dualCell.errorMessageLabelTwo.text ?? "")
            case SignupProfileTextFieldType.firstName.rawValue:
                let cell = tableView.cellForRow(at: currentIndexPath) as! TueetorTextFieldTableViewCell
                cell.validateFirstName(text: userRegistration.firstName)
                errorMessages.append(cell.errorLabel.text ?? "")
            case SignupProfileTextFieldType.lastName.rawValue:
                let cell = tableView.cellForRow(at: currentIndexPath) as! TueetorTextFieldTableViewCell
                cell.validateLastName(text: userRegistration.lastName)
                errorMessages.append(cell.errorLabel.text ?? "")
            case SignupProfileTextFieldType.displayName.rawValue:
                let cell = tableView.cellForRow(at: currentIndexPath) as! TueetorTextFieldTableViewCell
                cell.validateDisplayName(text: userRegistration.displayName)
                errorMessages.append(cell.errorLabel.text ?? "")
            case SignupProfileTextFieldType.birthday.rawValue:
                let cell = tableView.cellForRow(at: currentIndexPath) as? TueetorTextFieldTableViewCell
                cell?.validateBirthday(text: userRegistration.birthday)
                
                
                errorMessages.append(cell?.errorLabel.text ?? "")
            default:
                break
            }
        }
        let filteredErrorMessages = errorMessages.filter { (message) -> Bool in
            message.count > 0
        }
        tableView.reloadData()
        let isValidData = filteredErrorMessages.count == 0
        return (isValid: isValidData, message: isValidData ? "" : filteredErrorMessages[0])
    }

}


extension SignupProfileViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return labelFields.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard indexPath.row != 0 else {
            let cell = tableView.dequeueReusableCell(withIdentifier: SignupProfileHeaderTableViewCell.cellIdentifier(), for: indexPath) as! SignupProfileHeaderTableViewCell
            cell.signupProfileDelegate = self
            if let userImage = profileImage {
                cell.profileImageView.image = userImage
                cell.profileImageView.layer.cornerRadius = cell.profileImageView.bounds.height / 2
                cell.profileImageView.layer.borderColor = UIColor.white.cgColor
                cell.profileImageView.layer.borderWidth = 2
                cell.profileImageView.contentMode = .scaleAspectFill
                cell.editImageView.isHidden = false
                cell.editImageView.layer.cornerRadius = 15
                cell.editImageView.layer.borderColor = themeColor.cgColor
                cell.editImageView.layer.borderWidth = 2
                cell.editImageView.backgroundColor = UIColor.white
            } else {
                cell.profileImageView.sd_setShowActivityIndicatorView(true)
                cell.profileImageView.sd_setIndicatorStyle(.gray)
                cell.profileImageView.sd_setImage(with: URL(string: userRegistration.imageURL), placeholderImage: UIImage(named: "profile_icon"), options: .retryFailed) { (image, error, cacheType, url) in
                    if error == nil {
                        self.isImageSelected = true
                        self.profileImage = image
                        cell.profileImageView.layer.cornerRadius = cell.profileImageView.bounds.height / 2
                        cell.profileImageView.layer.borderColor = UIColor.white.cgColor
                        cell.profileImageView.layer.borderWidth = 2
                        cell.profileImageView.contentMode = .scaleAspectFill
                        cell.editImageView.isHidden = false
                        cell.editImageView.layer.cornerRadius = 15
                        cell.editImageView.layer.borderColor = themeColor.cgColor
                        cell.editImageView.layer.borderWidth = 2
                        cell.editImageView.backgroundColor = UIColor.white
                    }
                }
            }
            return cell
        }
        
        guard indexPath.row != 1 else {
            let cell = tableView.dequeueReusableCell(withIdentifier: TueetorDualTextFieldTableViewCell.cellIdentifier(), for: indexPath) as! TueetorDualTextFieldTableViewCell
            cell.configureCellWithTitles(title: labelFields[indexPath.row - 1], index: indexPath.row)
            configureTextField(cell.textFieldOne)
            configureTextField(cell.textFieldTwo)
            cell.textFieldOne.showImage()
            cell.textFieldTwo.showImage()
            return cell
        }
        
        guard indexPath.row != 5 else {
            let cell = tableView.dequeueReusableCell(withIdentifier: ProfileDisplayLinkTableViewCell.cellIdentifier(), for: indexPath) as! ProfileDisplayLinkTableViewCell
            cell.configureLinkLabel(name)
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: TueetorTextFieldTableViewCell.cellIdentifier(), for: indexPath) as! TueetorTextFieldTableViewCell
        cell.configureCellForRowAtIndex(indexPath.row, withText: labelFields[indexPath.row - 1][0])
        cell.delegate = self
        updateDataIntoTextField(cell.customTextField, fromCell: true)
        configureTextField(cell.customTextField)
        return cell

    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        guard indexPath.row != 0 else {
            return 219
        }
        guard indexPath.row != 5 else {
            tableView.estimatedRowHeight = 84.0
            return name.isEmpty ? 0 : UITableViewAutomaticDimension
        }
        tableView.estimatedRowHeight = 63.0
        return UITableViewAutomaticDimension
    }

}

extension SignupProfileViewController: SignupProfileHeaderTableViewCellDelegate {
    
    func showImageOptions() {
        let alert = UIAlertController(title: "Choose Image".localized, message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera".localized, style: .default, handler: { _ in
            self.camera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery".localized, style: .default, handler: { _ in
            self.photoLibrary()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel".localized, style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
}

extension SignupProfileViewController: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        activeTextField = textField as! TueetorTextField
        if activeTextField.tag == SignupProfileTextFieldType.dual.rawValue {
            managePickerView()
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        updateDataIntoTextField(textField as! TueetorTextField, fromCell: false)
        
        let currentIndexPath = IndexPath.init(row: textField.tag, section: 0)
        switch textField.tag {
        case SignupProfileTextFieldType.dual.rawValue:
            let dualCell = tableView.cellForRow(at: currentIndexPath) as! TueetorDualTextFieldTableViewCell
            let customField = textField as! TueetorTextField
            if customField.title == "TITLE".localized {
                dualCell.validateTitle(text: customField.text ?? "")
            } else {
                dualCell.validateGender(text: customField.text ?? "")
            }
        case SignupProfileTextFieldType.firstName.rawValue:
            let cell = tableView.cellForRow(at: currentIndexPath) as! TueetorTextFieldTableViewCell
            cell.validateFirstName(text: textField.text ?? "")
        case SignupProfileTextFieldType.lastName.rawValue:
            let cell = tableView.cellForRow(at: currentIndexPath) as! TueetorTextFieldTableViewCell
            cell.validateLastName(text: textField.text ?? "")
        case SignupProfileTextFieldType.displayName.rawValue:
            let cell = tableView.cellForRow(at: currentIndexPath) as! TueetorTextFieldTableViewCell
            cell.validateDisplayName(text: textField.text ?? "")
        case SignupProfileTextFieldType.birthday.rawValue:
            let cell = tableView.cellForRow(at: currentIndexPath) as! TueetorTextFieldTableViewCell
            cell.validateBirthday(text: textField.text ?? "")
        default:
            break
        }
        tableView.beginUpdates()
        tableView.endUpdates()

    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (textField.text?.isEmpty)! && string == " " {
            return false
        }
        let char = string.cString(using: String.Encoding.utf8)!
        let isBackSpace = strcmp(char, "\\b")
        if (isBackSpace == -92) {
            if textField.tag == SignupProfileTextFieldType.displayName.rawValue {
                isDisplayNameEdited = true
            }
            return true
        }
        
        if textField.tag == SignupProfileTextFieldType.displayName.rawValue {
            isDisplayNameEdited = true
        }
        switch textField.tag {
        case SignupProfileTextFieldType.firstName.rawValue:
            return (textField.text?.count)! < NameMaxLimit
        case SignupProfileTextFieldType.lastName.rawValue:
            return (textField.text?.count)! < NameMaxLimit
        case SignupProfileTextFieldType.displayName.rawValue:
            return (textField.text?.count)! < DisplayNameMaxLimit
        default:
            return true
        }
    }
    
}

extension SignupProfileViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func managePickerView() {
        let textField = activeTextField as! TueetorTextField
        titlePickerView.reloadAllComponents()
        titlePickerView.reloadInputViews()
        genderPickerView.reloadAllComponents()
        genderPickerView.reloadInputViews()
        if textField.title == "TITLE".localized {
            textField.inputView = titlePickerView
            let index = titleOptions.index(of: userRegistration.title)
            if let _index = index {
                self.titlePickerView.selectRow(_index, inComponent: 0, animated: true)
            } else {
                titlePickerView.selectRow(0, inComponent: 0, animated: true)
                activeTextField.text = titleOptions[0]
            }
        } else {
            textField.inputView = genderPickerView
            let index = genderOptions.index(of: userRegistration.gender)
            if let _index = index {
                self.genderPickerView.selectRow(_index, inComponent: 0, animated: true)
            } else {
                genderPickerView.selectRow(0, inComponent: 0, animated: true)
                activeTextField.text = genderOptions[0]
            }
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == titlePickerView {
            return titleOptions.count
        } else {
            return genderOptions.count
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == titlePickerView {
            return titleOptions[row]
        } else {
            return genderOptions[row]
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == titlePickerView {
            activeTextField.text = titleOptions[row]
        } else {
            activeTextField.text = genderOptions[row]
        }
    }
    
}

extension SignupProfileViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func camera() {
        if UIImagePickerController.isSourceTypeAvailable(.camera){
            let myPickerController = UIImagePickerController()
            myPickerController.delegate = self;
            myPickerController.sourceType = .camera
            myPickerController.allowsEditing = true
            self.present(myPickerController, animated: true, completion: nil)
        }
        
    }
    
    func photoLibrary() {
        
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
            let myPickerController = UIImagePickerController()
            myPickerController.delegate = self;
            myPickerController.sourceType = .photoLibrary
            myPickerController.allowsEditing = true
            self.present(myPickerController, animated: true, completion: nil)
        }
    }
    

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        let image = info[UIImagePickerControllerEditedImage] as! UIImage
        self.profileImage = image
        tableView.reloadData()
        isImageSelected = true
        dismiss(animated:true, completion: nil)
    }
    
}

extension SignupProfileViewController: TueetorTextFieldTableViewCellDelegate {

    func valueChangedForTextField(_ textField: TueetorTextField) {
    }
    
}

extension SignupProfileViewController {
    
    func getProfileLinkURLForName(_ displayName: String) {
        
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }
        
        Utilities.showHUD(forView: self.view, excludeViews: [])
        let linkObserver = ApiManager.shared.apiService.profileLinkForName(displayName)
        let linkDisposable = linkObserver.subscribe(onNext: {(validName) in
            Utilities.hideHUD(forView: self.view)
            let trimmedString = validName.components(separatedBy: .whitespacesAndNewlines).joined(separator: "")

            self.name = trimmedString.lowercased()
            
            DispatchQueue.main.async {
                let _ = self.isDataValid()
            }
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.showSimpleAlertWithMessage(error.localizedDescription) 
                    }
                }
            })
        })
        linkDisposable.disposed(by: disposableBag)
    }
}
