//
//  ForgotPasswordMobileViewController.swift
//  Tueetor
//
//  Created by Phaninder on 21/04/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit
import RxSwift

class ForgotPasswordMobileViewController: BaseViewController {

    @IBOutlet weak var mobileNumberTextField: TueetorTextField!
    @IBOutlet weak var errorMessageLabel: UILabel!
    @IBOutlet weak var errorLabelHeightConstraint: NSLayoutConstraint!
    
    var disposableBag = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()
        errorMessageLabel.text = ""
        mobileNumberTextField.placeholder = "MOBILE NUMBER"
        mobileNumberTextField.title = "MOBILE NUMBER"
        mobileNumberTextField.delegate = self
        mobileNumberTextField.keyboardType = .phonePad
        mobileNumberTextField.textFont = textFieldDefaultFont

    }

    @IBAction func sendCodeButtonTapped(_ sender: TueetorButton) {
        view.endEditing(true)
        guard isDataValid() else {
            self.showSimpleAlertWithMessage("Please enter valid data.")
            return
        }
        sendOTPToMobileNumber(self.mobileNumberTextField.text!)
//        let forgotPasswordOTPViewController = ForgotPasswordOTPViewController.instantiateFromAppStoryboard(appStoryboard: .Login)
//        forgotPasswordOTPViewController.phoneNumber = self.mobileNumberTextField.text!
//        forgotPasswordOTPViewController.userID = 123123
//        self.navigationController?.pushViewController(forgotPasswordOTPViewController, animated: true)

    }
    
    func isDataValid() -> Bool {
        let phoneNumber = mobileNumberTextField.text ?? ""
        if phoneNumber.isEmptyString() {
            mobileNumberTextField.hideImage()
            errorMessageLabel.text = "Please enter your registered Mobile Number."
        } else if !phoneNumber.isValidPhoneNumber() {
            mobileNumberTextField.hideImage()
            errorMessageLabel.text = "Please enter a valid Mobile Number."
        } else {
            mobileNumberTextField.showImage()
            errorMessageLabel.text = ""
        }
        animateMobileError()
        return errorMessageLabel.text?.count == 0
    }

    func animateMobileError() {
        let text = errorMessageLabel.text ?? ""
        if errorLabelHeightConstraint.constant == 0, !text.isEmptyString() {
            UIView.animate(withDuration: 0.2) {
                self.errorLabelHeightConstraint.constant = self.errorMessageLabel.heightForText()
                self.view.layoutIfNeeded()
            }
        } else if errorLabelHeightConstraint.constant > 0, text.isEmptyString() {
            UIView.animate(withDuration: 0.2) {
                self.errorLabelHeightConstraint.constant = 0
                self.view.layoutIfNeeded()
            }
        }
    }

}

extension ForgotPasswordMobileViewController: UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        _ = isDataValid()
    }
}

extension ForgotPasswordMobileViewController {
    
    func sendOTPToMobileNumber(_ number: String) {
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }

        Utilities.showHUD(forView: self.view, excludeViews: [])
        
        let forgotObserver = ApiManager.shared.apiService.sentOTPToMobileNumber(number)
        let forgotDisposable = forgotObserver.subscribe(onNext: {(userID) in
            Utilities.hideHUD(forView: self.view)
            
            DispatchQueue.main.async {
                let forgotPasswordOTPViewController = ForgotPasswordOTPViewController.instantiateFromAppStoryboard(appStoryboard: .Login)
                forgotPasswordOTPViewController.phoneNumber = self.mobileNumberTextField.text!
                forgotPasswordOTPViewController.userID = userID
                self.navigationController?.pushViewController(forgotPasswordOTPViewController, animated: true)
            }
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.showSimpleAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        })
        forgotDisposable.disposed(by: disposableBag)
    }

    
}
