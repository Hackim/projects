//
//  SignupAddressViewController.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 10/04/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit
import RxSwift
import GooglePlaces
import Alamofire
import Unbox

class SignupAddressViewController: BaseViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var activeTextField: UITextField!
    let labelFields = [["ADDRESS LINE 1".localized], ["ADDRESS LINE 2".localized], ["CITY".localized, "ZIP CODE".localized], ["COUNTRY".localized], ["CONTACT NUMBER".localized]]
    var userRegistration: UserRegistration!
    var disposableBag = DisposeBag()
    var countries = [Country]()
    let countryPickerView = UIPickerView()
    var profilePicture: UIImage!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let countriesArray = Utilities.getCountries() {
            countries = countriesArray
        } else {
            fetchCountries()
        }
        countryPickerView.delegate = self
        countryPickerView.dataSource = self
        tableView.tableFooterView = UIView()
        
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }

    @IBAction func submitButtonTapped(_ sender: TueetorButton) {
        let validationInformation = isDataValid()
        guard validationInformation.isValid else {
            self.showSimpleAlertWithMessage(validationInformation.message)
            return
        }
        checkPhoneNumber()
    }
    
    @IBAction func backButtonTapped(_ sender: UIButton) {
        self.view.endEditing(true)
        if userRegistration.addressLine1 != "" ||
            userRegistration.addressLine2 != "" ||
            userRegistration.city != "" ||
            userRegistration.zipCode != "" ||
            userRegistration.country != "" ||
            userRegistration.phone != "" {
            let alert = UIAlertController(title: AppName, message: AlertMessage.removeFieldData.description(), preferredStyle: .alert)
            let yesAction = UIAlertAction(title: AlertButton.yes.description(), style: .default) { (action) in
                self.navigateBack(sender: sender)
            }
            
            let noAction =  UIAlertAction(title: AlertButton.no.description(), style: .default, handler: nil)
            alert.addAction(yesAction)
            alert.addAction(noAction)
            self.present(alert, animated: true, completion: nil)
        } else {
            navigateBack(sender: sender)
        }

    }
    
    @IBAction func loginButtonTapped(_ sender: UIButton) {
        let alert = UIAlertController(title: AppName, message: AlertMessage.exitSignupProcess.description(), preferredStyle: .alert)
        let yesAction = UIAlertAction(title: AlertButton.yes.description(), style: .default) { (action) in
            self.navigationController?.popToRootViewController(animated: true)
        }
        
        let noAction =  UIAlertAction(title: AlertButton.no.description(), style: .default, handler: nil)
        alert.addAction(yesAction)
        alert.addAction(noAction)
        self.present(alert, animated: true, completion: nil)
    }

    func configureTextField(_ customTextField: TueetorTextField) {
        customTextField.delegate = self
        customTextField.isSecureTextEntry = false
        customTextField.keyboardType = .asciiCapable
        switch customTextField.tag {
        case SignupAddressTextFieldType.dual.rawValue:
            customTextField.icon = "tick"
            if customTextField.title == "CITY".localized {
                customTextField.keyboardType = .asciiCapable
            } else {
                customTextField.keyboardType = .numberPad
            }
        case SignupAddressTextFieldType.phone.rawValue:
            customTextField.keyboardType = .phonePad
        case SignupAddressTextFieldType.country.rawValue:
            customTextField.inputView = countryPickerView
            customTextField.icon = "drop-down-arrow"
            customTextField.showImage()
        case SignupAddressTextFieldType.address1.rawValue:
            customTextField.isUserInteractionEnabled = false
        default:
            customTextField.keyboardType = .asciiCapable
        }
        
    }
    
    func updateDataIntoTextField(_ customTextField: TueetorTextField, fromCell: Bool) {
        switch customTextField.tag {
        case SignupAddressTextFieldType.address1.rawValue:
            fromCell ? (customTextField.text = userRegistration.addressLine1) : (userRegistration.addressLine1 = customTextField.text!)
            break
        case SignupAddressTextFieldType.address2.rawValue:
            fromCell ? (customTextField.text = userRegistration.addressLine2) : (userRegistration.addressLine2 = customTextField.text!)
            break
        case SignupAddressTextFieldType.dual.rawValue:
            if customTextField.title == "CITY".localized {
                fromCell ? (customTextField.text = userRegistration.city) : (userRegistration.city = customTextField.text!)
            } else {
                fromCell ? (customTextField.text = userRegistration.zipCode) : (userRegistration.zipCode = customTextField.text!)
            }
            break
        case SignupAddressTextFieldType.country.rawValue:
            fromCell ? (customTextField.text = userRegistration.country) : (userRegistration.country = customTextField.text!)
            break
        case SignupAddressTextFieldType.phone.rawValue:
            fromCell ? (customTextField.text = userRegistration.phone) : (userRegistration.phone = customTextField.text!)
            break
        default:
            break
        }
    }

    func isDataValid() -> (isValid: Bool, message: String) {
        var errorMessages: [String] = []
        
        for index in 1...labelFields.count {
            let currentIndexPath = IndexPath.init(row: index, section: 0)
            switch index {
            case SignupAddressTextFieldType.dual.rawValue:
                let dualCell = tableView.cellForRow(at: currentIndexPath) as! TueetorDualTextFieldTableViewCell
                dualCell.validateCity(text: userRegistration.city)
                dualCell.validateZip(text: userRegistration.zipCode)
                errorMessages.append(dualCell.errorMessageLabelOne.text ?? "")
                errorMessages.append(dualCell.errorMessageLabelTwo.text ?? "")
            case SignupAddressTextFieldType.address1.rawValue:
                let cell = tableView.cellForRow(at: currentIndexPath) as! AddressPlacePickerTableViewCell
                cell.validateAddress(text: userRegistration.addressLine1)
                errorMessages.append(cell.errorLabel.text ?? "")
            case SignupAddressTextFieldType.country.rawValue:
                let cell = tableView.cellForRow(at: currentIndexPath) as! TueetorTextFieldTableViewCell
                cell.validateCountry(text: userRegistration.country)
                errorMessages.append(cell.errorLabel.text ?? "")
            case SignupAddressTextFieldType.phone.rawValue:
                let cell = tableView.cellForRow(at: currentIndexPath) as! TueetorTextFieldTableViewCell
                cell.validatePhone(text: userRegistration.phone)
                errorMessages.append(cell.errorLabel.text ?? "")
            default:
                break
            }
        }
        let filteredErrorMessages = errorMessages.filter { (message) -> Bool in
            message.count > 0
        }
        tableView.beginUpdates()
        tableView.endUpdates()
        let isValidData = filteredErrorMessages.count == 0
        return (isValid: isValidData, message: isValidData ? "" : filteredErrorMessages[0])
    }

}

extension SignupAddressViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return labelFields.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard indexPath.row != 0 else {
            let cell = tableView.dequeueReusableCell(withIdentifier: SignupAddressHeaderTableViewCell.cellIdentifier(), for: indexPath) as! SignupAddressHeaderTableViewCell
            return cell
        }
        guard indexPath.row != 1 else {
            let cell = tableView.dequeueReusableCell(withIdentifier: AddressPlacePickerTableViewCell.cellIdentifier(), for: indexPath) as! AddressPlacePickerTableViewCell
            cell.configureCellForRowAtIndex(indexPath.row, withText: labelFields[indexPath.row - 1][0])
            updateDataIntoTextField(cell.placeTextField, fromCell: true)
            cell.delegate = self
            configureTextField(cell.placeTextField)
            return cell

        }
        guard indexPath.row != 3 else {
            let cell = tableView.dequeueReusableCell(withIdentifier: TueetorDualTextFieldTableViewCell.cellIdentifier(), for: indexPath) as! TueetorDualTextFieldTableViewCell
            cell.configureCellWithTitles(title: labelFields[indexPath.row - 1], index: indexPath.row)
            configureTextField(cell.textFieldOne)
            configureTextField(cell.textFieldTwo)
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: TueetorTextFieldTableViewCell.cellIdentifier(), for: indexPath) as! TueetorTextFieldTableViewCell
        cell.configureCellForRowAtIndex(indexPath.row, withText: labelFields[indexPath.row - 1][0])
        updateDataIntoTextField(cell.customTextField, fromCell: true)
        configureTextField(cell.customTextField)
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        guard indexPath.row != 0 else {
            return 219
        }
        tableView.estimatedRowHeight = 63.0
        return UITableViewAutomaticDimension
    }
    
}

extension SignupAddressViewController: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        activeTextField = textField as! TueetorTextField
        if activeTextField.tag == SignupAddressTextFieldType.country.rawValue {
            managePickerView()
        } else if activeTextField.tag == SignupAddressTextFieldType.address1.rawValue {
            view.endEditing(true)
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        updateDataIntoTextField(textField as! TueetorTextField, fromCell: false)
        let currentIndexPath = IndexPath.init(row: textField.tag, section: 0)
        switch textField.tag {
        case SignupAddressTextFieldType.dual.rawValue:
            let dualCell = tableView.cellForRow(at: currentIndexPath) as! TueetorDualTextFieldTableViewCell
            let customField = textField as! TueetorTextField
            if customField.title == "CITY".localized {
                dualCell.validateCity(text: customField.text ?? "")
            } else {
                dualCell.validateZip(text: customField.text ?? "")
            }
        case SignupAddressTextFieldType.address1.rawValue:
            let cell = tableView.cellForRow(at: currentIndexPath) as! AddressPlacePickerTableViewCell
            cell.validateAddress(text: textField.text ?? "")
        case SignupAddressTextFieldType.country.rawValue:
            let cell = tableView.cellForRow(at: currentIndexPath) as! TueetorTextFieldTableViewCell
            cell.validateCountry(text: textField.text ?? "")
        case SignupAddressTextFieldType.phone.rawValue:
            let cell = tableView.cellForRow(at: currentIndexPath) as! TueetorTextFieldTableViewCell
            cell.validatePhone(text: textField.text ?? "")
        default:
            break
        }
        tableView.beginUpdates()
        tableView.endUpdates()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (textField.text?.isEmpty)! && string == " " {
            return false
        }
        let char = string.cString(using: String.Encoding.utf8)!
        let isBackSpace = strcmp(char, "\\b")
        if (isBackSpace == -92) {
            return true
        }
        switch textField.tag {
        case SignupAddressTextFieldType.address2.rawValue:
            return (textField.text?.count)! < AddressMaxLimit
        case SignupAddressTextFieldType.dual.rawValue:
            let customTF = textField as! TueetorTextField
            if customTF.title == "CITY".localized {
                return (customTF.text?.count)! < CityMaxLimit
            } else {
                return (customTF.text?.count)! < PostalCodeMaxLimit
            }
        case SignupAddressTextFieldType.phone.rawValue:
            return (textField.text?.count)! < PhoneNumberMaxLimit
        default:
            return true
        }
    }
    
}


extension SignupAddressViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func managePickerView() {
        let textField = activeTextField as! TueetorTextField
        countryPickerView.reloadAllComponents()
        countryPickerView.reloadInputViews()
        textField.inputView = countryPickerView
        let index = countries.index { (country) -> Bool in
            country.name == userRegistration.country
        }
        
        if let _index = index {
            self.countryPickerView.selectRow(_index, inComponent: 0, animated: true)
        } else {
            countryPickerView.selectRow(0, inComponent: 0, animated: true)
            activeTextField.text = countries[0].name
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return countries.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return countries[row].name
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        activeTextField.text = countries[row].name
        userRegistration.countryID = countries[row].id
    }
    
}

extension SignupAddressViewController: AddressPlacePickerTableViewCellDelegate {

    func showPlacePicker() {
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        present(autocompleteController, animated: true, completion: nil)
    }
    
}

extension SignupAddressViewController: GMSAutocompleteViewControllerDelegate {
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        
        userRegistration.latitude = String(place.coordinate.latitude)
        userRegistration.longitude = String(place.coordinate.longitude)
        userRegistration.addressLine1 = place.formattedAddress ?? place.name
        
        // Get the address components.
        if let addressLines = place.addressComponents {
            // Populate all of the address fields we can find.
            for field in addressLines {
                switch field.type {
                case kGMSPlaceTypeCountry:
                    let index = countries.index { (country) -> Bool in
                        country.name == field.name
                    }
                    if let _index = index {
                        userRegistration.country = countries[_index].name
                        userRegistration.countryID = countries[_index].id
                    }
                case kGMSPlaceTypePostalCode:
                    userRegistration.zipCode = field.name
                case kGMSPlaceTypeLocality:
                    userRegistration.city = field.name
                default:
                    print("ignore")
                }
            }
        }

        tableView.reloadData()
        let cell = tableView.cellForRow(at: IndexPath.init(row: 1, section: 0)) as! AddressPlacePickerTableViewCell
        cell.placeTextField.text = place.formattedAddress ?? place.name
        cell.validateAddress(text: cell.placeTextField.text ?? "")
        let cityCell = tableView.cellForRow(at: IndexPath.init(row: 3, section: 0)) as! TueetorDualTextFieldTableViewCell
        cityCell.textFieldOne.text = userRegistration.city
        cityCell.textFieldTwo.text = userRegistration.zipCode

//        cell.placeTextField.text = place.formattedAddress ?? place.name
//        cell.validateAddress(text: cell.placeTextField.text ?? "")
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}

extension SignupAddressViewController {
    
    func fetchCountries() {
        
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }
        
        Utilities.showHUD(forView: self.view, excludeViews: [])
        let countryObserver = ApiManager.shared.apiService.fetchCountries()
        let countryDisposable = countryObserver.subscribe(onNext: {(countries) in
            Utilities.hideHUD(forView: self.view)
            self.countries = countries
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.showSimpleAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        })
        countryDisposable.disposed(by: disposableBag)
    }

    func createAccount() {
        
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }
        
        Utilities.showHUD(forView: self.view, excludeViews: [])
        let dob = userRegistration.birthday
        let inputDateFormatter = DateFormatter()
        inputDateFormatter.dateFormat = "dd-MMM-yyyy"
        let date = inputDateFormatter.date(from: dob)!
        let formattedDateString = Utilities.getFormattedDate(date, dateFormat: "yyyy-MM-dd")
        let signupDict = ["email": userRegistration.email,
                          "password": userRegistration.password,
                          "title": userRegistration.title,
                          "gender": userRegistration.gender == "Male" ? "M" : "F",
                          "first_name": userRegistration.firstName,
                          "last_name": userRegistration.lastName,
                          "name": userRegistration.displayName,
                          "dob": formattedDateString,
                          "address1": userRegistration.addressLine1,
                          "address2": userRegistration.addressLine2,
                          "lattitude": userRegistration.latitude,
                          "longitude": userRegistration.longitude,
                          "city": userRegistration.city,
                          "postcode": userRegistration.zipCode,
                          "country_id": userRegistration.countryID,
                          "contact": userRegistration.phone,
                          "user_type": userRegistration.isStudent ? "S" : "T"] as [String : AnyObject]
        print("signupDict: \n", signupDict)
        let regObserver = ApiManager.shared.apiService.createAccountWithDetails(signupDict)
        let regDisposable = regObserver.subscribe(onNext: {(userID) in
            Utilities.hideHUD(forView: self.view)
            
            DispatchQueue.main.async {
                let signupConfirmMobileViewController = SignupConfirmMobileViewController.instantiateFromAppStoryboard(appStoryboard: .Login)
                signupConfirmMobileViewController.mobileNumber = self.userRegistration.phone
                signupConfirmMobileViewController.userID = userID
                self.navigationController?.pushViewController(signupConfirmMobileViewController, animated: true)
            }
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.showSimpleAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        })
        regDisposable.disposed(by: disposableBag)
    }
    
    func createAccountWithImage(_ image: UIImage) {
        let imageData = UIImageJPEGRepresentation(image, 0.5)
        
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }

        Utilities.showHUD(forView: self.view, excludeViews: [])
        let dob = userRegistration.birthday
        let inputDateFormatter = DateFormatter()
        inputDateFormatter.dateFormat = "dd-MMM-yyyy"
        let date = inputDateFormatter.date(from: dob)!
        let formattedDateString = Utilities.getFormattedDate(date, dateFormat: "yyyy-MM-dd")
        var signupDict = ["email": userRegistration.email,
                          "password": userRegistration.password,
                          "title": userRegistration.title,
                          "gender": userRegistration.gender == "Male" ? "M" : "F",
                          "first_name": userRegistration.firstName,
                          "last_name": userRegistration.lastName,
                          "name": userRegistration.displayName,
                          "dob": formattedDateString,
                          "address1": userRegistration.addressLine1,
                          "address2": userRegistration.addressLine2,
                          "lattitude": userRegistration.latitude,
                          "longitude": userRegistration.longitude,
                          "city": userRegistration.city,
                          "postcode": userRegistration.zipCode,
                          "country_id": userRegistration.countryID,
                          "contact": userRegistration.phone,
                          "user_type": userRegistration.isStudent ? "S" : "T"] as [String : AnyObject]
        if !userRegistration.fbID.isEmpty {
            signupDict["fb_id"] = userRegistration.fbID as AnyObject
        }
        print("signupDict: \n", signupDict)
        
        let url = "http://socket-chat.tueetor.com/post/register" //"http://tueetoradmin.hipster-inc.com/public/post/register"
        let headers: HTTPHeaders = [
            "Content-type": "multipart/form-data"
        ]
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in signupDict {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            
            if let data = imageData {
                multipartFormData.append(data, withName: "image", fileName: "image.png", mimeType: "image/png")
            }
            
        }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (result) in
            switch result{
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    print("Succesfully uploaded")
                    DispatchQueue.main.async(execute: {
                        Utilities.hideHUD(forView: self.view)
                    })
                    if let _ = response.error{
                        self.showSimpleAlertWithMessage("Failed to create account.".localized)
                        return
                    }
                    let json = try? JSONSerialization.jsonObject(with: response.data!, options: .mutableContainers)

                    if let userDict = json as? [String: AnyObject], let data = userDict["data"] as? [String: AnyObject], let id = data["id"] as? Int {
                        DispatchQueue.main.async(execute: {
                            let signupConfirmMobileViewController = SignupConfirmMobileViewController.instantiateFromAppStoryboard(appStoryboard: .Login)
                            signupConfirmMobileViewController.userID = id
                            signupConfirmMobileViewController.password = self.userRegistration.password
                            signupConfirmMobileViewController.mobileNumber = self.userRegistration.phone
                            self.navigationController?.pushViewController(signupConfirmMobileViewController, animated: true)
                        })
                    } else if let userDict = json as? [String: AnyObject], let message = userDict["message"] as? String {
                        self.showSimpleAlertWithMessage(message)
                    } else {
                        self.showSimpleAlertWithMessage("There seems to be a problem during registration. Please try again after sometime.")
                    }
                }
            case .failure(let error):
                DispatchQueue.main.async(execute: {
                    Utilities.hideHUD(forView: self.view)
                    if let error_ = error as? ResponseError {
                        self.showSimpleAlertWithMessage(error_.description())
                    } else {
                        if !error.localizedDescription.isEmpty {
                            self.showSimpleAlertWithMessage(error.localizedDescription)
                        }
                    }
                })
            }
        }
    }
    
    func checkPhoneNumber() {
        
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }
        
        Utilities.showHUD(forView: self.view, excludeViews: [])
        let verifyObserver = ApiManager.shared.apiService.checkAccountAvailability("", phoneNumber: userRegistration.phone)
        let verifyDisposable = verifyObserver.subscribe(onNext: {(isAvailable) in
            Utilities.hideHUD(forView: self.view)
            DispatchQueue.main.async {
                if let image = self.profilePicture {
                    self.createAccountWithImage(image)
                } else {
                    self.createAccount()
                }
            }
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.showSimpleAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        })
        verifyDisposable.disposed(by: disposableBag)
    }

    
}

