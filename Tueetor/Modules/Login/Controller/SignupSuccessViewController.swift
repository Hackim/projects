//
//  SignupSuccessViewController.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 21/04/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit

class SignupSuccessViewController: BaseViewController {

    var user: User!
    var password: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let email = user.email!
        UserStore.shared.isLoggedIn = true
        UserStore.shared.userEmail = email
        do {
            // This is a new account, create a new keychain item with the account name.
            let passwordItem = KeychainPasswordItem(service: KeychainConfiguration.serviceName,
                                                    account: email,
                                                    accessGroup: KeychainConfiguration.accessGroup)
            
            // Save the password for the new item.
            try passwordItem.savePassword(password)
            UserStore.shared.hasLoginKey = true
        } catch {
            UserStore.shared.hasLoginKey = false
            fatalError("Error updating keychain - \(error)")
        }

        Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(navigateToOnboardingFlow), userInfo: nil, repeats: false)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }

    @objc func navigateToOnboardingFlow() {
        if user.userType! == "T" {
            let onboard = OnBoardingMainViewController.instantiateFromAppStoryboard(appStoryboard: .Main)
            self.navigationController?.pushViewController(onboard, animated: true)
        } else {
            let onboard = StudentOnboardingSubjectViewController.instantiateFromAppStoryboard(appStoryboard: .Main)
            self.navigationController?.pushViewController(onboard, animated: true)
        }
    }

}
