//
//  ForgotPasswordOTPViewController.swift
//  Tueetor
//
//  Created by Phaninder on 21/04/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit
import RxSwift

class ForgotPasswordOTPViewController: BaseViewController {

    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var confirmationTextField: UITextField!
    @IBOutlet weak var firstLabel: UILabel!
    @IBOutlet weak var secondLabel: UILabel!
    @IBOutlet weak var thirdLabel: UILabel!
    @IBOutlet weak var fourthLabel: UILabel!
    @IBOutlet weak var firstView: UIView!
    @IBOutlet weak var secondView: UIView!
    @IBOutlet weak var thirdView: UIView!
    @IBOutlet weak var fourthView: UIView!

    var phoneNumber: String = ""
    var inactiveColor: UIColor = UIColor(white: 1, alpha: 0.5)
    var disposableBag = DisposeBag()
    var userID: Int!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        resetFields()
        
        confirmationTextField.addTarget(self, action: #selector(codeChanged(textField:)), for: .editingChanged)

        let messageText = "Enter 4 digit code which has been sent to your registered mobile number \(phoneNumber) "
        let attributedString = NSMutableAttributedString(string: messageText, attributes: [
            .font: UIFont(name: "Montserrat-Regular", size: 13.0)!,
            .foregroundColor: UIColor(white: 1.0, alpha: 1.0)
            ])
        
        attributedString.addAttribute(.font, value: UIFont(name: "Montserrat-SemiBold", size: 13.0)!, range: NSRange(location: 71, length: phoneNumber.count + 1))
        messageLabel.attributedText = attributedString
        
    }

    @IBAction func activateTextField(_ sender: UIButton) {
        confirmationTextField.becomeFirstResponder()
    }
    
    @IBAction func verifyButtonTapped(_ sender: TueetorButton) {
        guard confirmationTextField.text!.count == 4 else {
            self.showSimpleAlertWithMessage("Please enter the confirmation code sent to your number")
            return
        }
        verifyOTP(confirmationTextField.text!.trimmingCharacters(in: .whitespaces))

    }
    
    @objc func codeChanged(textField: UITextField) {
        resetFields()
        let code = textField.text ?? ""
        for (index, character) in code.enumerated() {
            switch index + 1 {
            case 1:
                firstView.backgroundColor = UIColor.white
                firstLabel.text = "\(character)"
            case 2:
                secondView.backgroundColor = UIColor.white
                secondLabel.text = "\(character)"
            case 3:
                thirdView.backgroundColor = UIColor.white
                thirdLabel.text = "\(character)"
            case 4:
                fourthView.backgroundColor = UIColor.white
                fourthLabel.text = "\(character)"
            default:
                break
            }
        }
    }
    
    func resetFields() {
        firstLabel.text = ""
        secondLabel.text = ""
        thirdLabel.text = ""
        fourthLabel.text = ""
        firstView.backgroundColor = inactiveColor
        secondView.backgroundColor = inactiveColor
        thirdView.backgroundColor = inactiveColor
        fourthView.backgroundColor = inactiveColor
    }


}

extension ForgotPasswordOTPViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (textField.text?.isEmpty)! && string == " " {
            return false
        }
        let char = string.cString(using: String.Encoding.utf8)!
        let isBackSpace = strcmp(char, "\\b")
        if (isBackSpace == -92) {
            return true
        }
        return (textField.text?.count)! < 4
    }
    
}

extension ForgotPasswordOTPViewController {
    
    func verifyOTP(_ otp: String) {
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }
        
        Utilities.showHUD(forView: self.view, excludeViews: [])
        
        let verifyObserver = ApiManager.shared.apiService.verifyOTP(otp, forUserID: userID)
        let verifyDisposable = verifyObserver.subscribe(onNext: {(user) in
            Utilities.hideHUD(forView: self.view)
            
            DispatchQueue.main.async {
                let resetPasswordViewController = ResetPasswordViewController.instantiateFromAppStoryboard(appStoryboard: .Login)
                resetPasswordViewController.userID = self.userID
                self.navigationController?.pushViewController(resetPasswordViewController, animated: true)
            }
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.showSimpleAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        })
        verifyDisposable.disposed(by: disposableBag)
    }

    
}
