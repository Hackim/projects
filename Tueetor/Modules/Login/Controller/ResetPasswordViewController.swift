//
//  ResetPasswordViewController.swift
//  Tueetor
//
//  Created by Phaninder on 21/04/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit
import RxSwift

class ResetPasswordViewController: BaseViewController {

    @IBOutlet weak var passwordTextField: TueetorTextField!
    @IBOutlet weak var confirmPasswordTextField: TueetorTextField!
    @IBOutlet weak var passwordErrorLabel: UILabel!
    @IBOutlet weak var confirmPasswordErrorLabel: UILabel!
    @IBOutlet weak var passwordErrorHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var confirmPasswordHeightConstraint: NSLayoutConstraint!
    
    var userID: Int!
    var disposableBag = DisposeBag()


    override func viewDidLoad() {
        super.viewDidLoad()
        passwordTextField.isSecureTextEntry = true
        passwordTextField.tag = 1
        confirmPasswordTextField.isSecureTextEntry = true
        confirmPasswordTextField.tag = 2
        
        passwordErrorLabel.text = ""
        confirmPasswordErrorLabel.text = ""
        passwordTextField.placeholder = "PASSWORD"
        passwordTextField.title = "PASSWORD"
        passwordTextField.textFont = textFieldDefaultFont

        confirmPasswordTextField.placeholder = "CONFIRM PASSWORD"
        confirmPasswordTextField.title = "CONFIRM PASSWORD"
        confirmPasswordTextField.textFont = textFieldDefaultFont

    }

    @IBAction func submitButtonTapped(_ sender: TueetorButton) {
        guard isDataValid() else {
            showInvalidInputsMessage()
            return
        }
        changePassword(passwordTextField.text!)
    }

    func isDataValid() -> Bool {
        let password = passwordTextField.text ?? ""
        let confirmPassword = confirmPasswordTextField.text ?? ""
        
        if password.isEmptyString() {
            passwordTextField.hideImage()
            passwordErrorLabel.text = "Please enter password."
        } else if !password.isValidPassword() {
            passwordTextField.hideImage()
            passwordErrorLabel.text = "Please enter a valid password."
        } else {
            passwordTextField.showImage()
            passwordErrorLabel.text = ""
        }
        animatePasswordError()
        
        if confirmPassword.isEmptyString() {
            confirmPasswordTextField.hideImage()
            confirmPasswordErrorLabel.text = "Please confirm password."
        } else if !confirmPassword.isValidPassword() {
            confirmPasswordTextField.hideImage()
            confirmPasswordErrorLabel.text = "Please enter a valid password."
        } else if confirmPassword != password {
            confirmPasswordTextField.hideImage()
            confirmPasswordErrorLabel.text = "Password and confirm password should be same."
        } else {
            confirmPasswordTextField.showImage()
            confirmPasswordErrorLabel.text = ""
        }
        animateConfirmPasswordError()
        
        return passwordErrorLabel.text!.isEmpty && confirmPasswordErrorLabel.text!.isEmpty
    }

    func animatePasswordError() {
        let text = passwordErrorLabel.text ?? ""
        if passwordErrorHeightConstraint.constant == 0, !text.isEmptyString() {
            UIView.animate(withDuration: 0.2) {
                self.passwordErrorHeightConstraint.constant = self.passwordErrorLabel.heightForText()
                self.view.layoutIfNeeded()
            }
        } else if passwordErrorHeightConstraint.constant > 0, text.isEmptyString() {
            UIView.animate(withDuration: 0.2) {
                self.passwordErrorHeightConstraint.constant = 0
                self.view.layoutIfNeeded()
            }
        }
    }
    
    func animateConfirmPasswordError() {
        let text = confirmPasswordErrorLabel.text ?? ""
        if confirmPasswordHeightConstraint.constant == 0, !text.isEmptyString() {
            UIView.animate(withDuration: 0.2) {
                self.confirmPasswordHeightConstraint.constant = self.confirmPasswordErrorLabel.heightForText()
                self.view.layoutIfNeeded()
            }
        } else if confirmPasswordHeightConstraint.constant > 0, text.isEmptyString() {
            UIView.animate(withDuration: 0.2) {
                self.confirmPasswordHeightConstraint.constant = 0
                self.view.layoutIfNeeded()
            }
        }
    }

    
}

extension ResetPasswordViewController: UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        let customTextField = textField as! TueetorTextField
        if textField.tag == 1 {
            if textField.text!.isEmptyString() {
                customTextField.hideImage()
                passwordErrorLabel.text = "Please enter password."
            } else if !textField.text!.isValidPassword() {
                customTextField.hideImage()
                passwordErrorLabel.text = "Please enter a valid password."
            } else {
                customTextField.showImage()
                passwordErrorLabel.text = ""
            }
            animatePasswordError()
        } else {
            if textField.text!.isEmptyString() {
                customTextField.hideImage()
                confirmPasswordErrorLabel.text = "Please confirm password."
            } else if !textField.text!.isValidPassword() {
                customTextField.hideImage()
                confirmPasswordErrorLabel.text = "Please enter a valid password."
            } else if textField.text! != passwordTextField.text! {
                customTextField.hideImage()
                confirmPasswordErrorLabel.text = "Password and confirm password should be same."
            } else {
                customTextField.showImage()
                confirmPasswordErrorLabel.text = ""
            }
            animateConfirmPasswordError()
        }
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (textField.text?.isEmpty)! && string == " " {
            return false
        }
        let char = string.cString(using: String.Encoding.utf8)!
        let isBackSpace = strcmp(char, "\\b")
        if (isBackSpace == -92) {
            return true
        }
        return true
    }

    
}

extension ResetPasswordViewController {
    
    func changePassword(_ password: String) {
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }

        Utilities.showHUD(forView: self.view, excludeViews: [])
        
        let changePasswordObserver = ApiManager.shared.apiService.resetPasswordForID(userID, withPassword: password)
        let changePasswordDisposable = changePasswordObserver.subscribe(onNext: {(user) in
            Utilities.hideHUD(forView: self.view)
            
            DispatchQueue.main.async {
                let alert = UIAlertController(title: AppName, message: AlertMessage.resetPasswordSuccess.description(), preferredStyle: .alert)
                let yesAction = UIAlertAction(title: AlertButton.yes.description(), style: .default) { (action) in
                    DispatchQueue.main.async {
                        self.navigationController?.popToRootViewController(animated: true)
                    }
                }
                
                let noAction =  UIAlertAction(title: AlertButton.no.description(), style: .default, handler: nil)
                alert.addAction(yesAction)
                alert.addAction(noAction)
                self.present(alert, animated: true, completion: nil)
            }
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.showSimpleAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        })
        changePasswordDisposable.disposed(by: disposableBag)
    }
    
}
