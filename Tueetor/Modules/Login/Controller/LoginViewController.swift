//
//  LoginViewController.swift
//  Tueetor
//
//  Created by Phaninder on 08/04/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class LoginViewController: BaseViewController {

    @IBOutlet weak var emailTextField: TueetorTextField!
    @IBOutlet weak var passwordTextField: TueetorButtonTextField!
    @IBOutlet weak var loginButton: TueetorButton!
    
    let fontStyle = UIFont(name: "AvenirNext-Regular", size: 17.0)!
    var showPassword = false

    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
    }
    
    // MARK:-  Helper Methods
    
    func configureUI() {
        emailTextField.placeholder = "Email"
        emailTextField.title = "Email"
        passwordTextField.placeholder = "Password"
        passwordTextField.title = "Password"
        emailTextField.textFont = textFieldDefaultFont
        passwordTextField.textFieldButtonDelegate = self
        passwordTextField.setButtonTitle(title: "SHOW")
        passwordTextField.textFont = textFieldDefaultFont
        passwordTextField.isSecureTextEntry = true
    }
    
    func setUpPasswordFieldType() {
        passwordTextField.isSecureTextEntry = !showPassword
        passwordTextField.keyboardType = .asciiCapable
        let tmpString = passwordTextField.text
        passwordTextField.text = " "
        passwordTextField.text = tmpString
    }


}

extension LoginViewController: TueetorButtonTextFieldDelegate {
    
    func actionButtonTapped() {
        showPassword = !showPassword
        setUpPasswordFieldType()
        passwordTextField.setButtonTitle(title: showPassword ? "HIDE" : "SHOW")
    }
    
    
}
