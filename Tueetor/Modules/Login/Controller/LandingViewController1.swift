//
//  LandingViewController.swift
//  Tueetor
//
//  Created by Phaninder on 08/04/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit

class LandingViewControllerBackUp: BaseViewController {

    @IBOutlet weak var bottomHalfHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var facebookButton: UIButton!
    @IBOutlet weak var createAccountButton: UIButton!
    @IBOutlet weak var termsTextView: UITextView!
    
    
    let fontStyle = UIFont(name: "AvenirNext-Regular", size: 15.0)!
    let showTermsURL = URL(string: "action://show-terms")!

    override func viewDidLoad() {
        super.viewDidLoad()
        setupUIComponents()
    }

    func setupUIComponents() {
        UIApplication.shared.statusBarStyle = .lightContent
        bottomHalfHeightConstraint.constant = termsTextView.frame.maxY + 8
        facebookButton.layer.borderColor = UIColor.white.cgColor
        facebookButton.layer.borderWidth = 1.0
        facebookButton.layer.cornerRadius = 20
        createAccountButton.layer.borderColor = UIColor.white.cgColor
        createAccountButton.layer.borderWidth = 1.0
        createAccountButton.layer.cornerRadius = 20
        setupTextView()
    }
    
    func setupTextView() {
        let attributedString = NSMutableAttributedString()
        attributedString.append(NSAttributedString(string: "By tapping Create Account or Facebook option, I agree to Tueetor's ",
                                                   attributes: [.font: fontStyle,
                                                                .foregroundColor : UIColor.white]))
        attributedString.append(NSAttributedString(string: "Terms of Services and Privacy Policy",
                                                   attributes: [.link: showTermsURL,
                                                                .underlineStyle: NSUnderlineStyle.styleSingle.rawValue,
                                                                .underlineColor: UIColor.white,
                                                                .foregroundColor : UIColor.white,
                                                                .font: fontStyle]))
        termsTextView.delegate = self
        termsTextView.attributedText = attributedString
        termsTextView.linkTextAttributes = [NSAttributedStringKey.foregroundColor.rawValue: UIColor.white]

    }
    
    // MARK:- IBActions
    @IBAction func loginButtonTapped(_ sender: UIButton) {
        let loginViewController = LoginViewController.instantiateFromAppStoryboard(appStoryboard: .Login)
        navigationController?.pushViewController(loginViewController, animated: true)
//        guard let window = UIApplication.shared.keyWindow else {
//            return
//        }
//
//        guard let rootViewController = window.rootViewController else {
//            return
//        }
//
//        let tabBarController = BaseTabBarViewController()
//        tabBarController.selectedIndex = 0
//        tabBarController.view.frame = rootViewController.view.frame
//        tabBarController.view.layoutIfNeeded()
//
//        UIView.transition(with: window, duration: 0.3, options: .transitionCrossDissolve, animations: {
//            window.rootViewController = tabBarController
//        }, completion: { completed in
//            UserStore.shared.isLoggedIn = true
//            // maybe do something here
//        })
    }
    
    @IBAction func createAccountButtonTapped(_ sender: UIButton) {
        let signupViewController = SignupAccountViewController.instantiateFromAppStoryboard(appStoryboard: .Login)
        self.navigationController?.pushViewController(signupViewController, animated: true)
    }
    
}

extension LandingViewControllerBackUp: UITextViewDelegate {
    
    @available(iOS, deprecated: 10.0)
    func textView(_ textView: UITextView, shouldInteractWith url: URL, in characterRange: NSRange) -> Bool {
        if url == showTermsURL, let term = termsURL {
            pushToWebViewWithURL(term, title: "Terms".localized)
            return true
        }

        return false
    }
    
    @available(iOS 10.0, *)
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        if URL == showTermsURL, let term = termsURL {
            pushToWebViewWithURL(term, title: "Terms".localized)
            return true
        }
        return false
    }
}

