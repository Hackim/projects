//
//  LandingViewController.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 13/04/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit
import FacebookLogin
import RxSwift
import BiometricAuthentication

struct KeychainConfiguration {
    static let serviceName = "TouchMeIn"
    static let accessGroup: String? = nil
}

class LandingViewController: BaseViewController {

    @IBOutlet weak var signupButtonTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var emailTextField: TueetorTextField!
    @IBOutlet weak var passwordTextField: TueetorButtonTextField!
    @IBOutlet weak var emailErrorLabel: UILabel!
    @IBOutlet weak var emailErrorLabelHeight: NSLayoutConstraint!
    @IBOutlet weak var passwordErrorLabel: UILabel!
    @IBOutlet weak var passwordErrorLabelHeight: NSLayoutConstraint!
    @IBOutlet weak var signInEmailButtonView: UIView!
    @IBOutlet weak var facebookButtonView: UIView!
    @IBOutlet weak var signupButton: UIButton!
    @IBOutlet weak var touchIDButton: UIButton!
    @IBOutlet weak var touchIDYConstraint: NSLayoutConstraint!
    @IBOutlet weak var signUpEmailButton: UIButton!
    @IBOutlet weak var signUpFacebookButton: UIButton!
    @IBOutlet weak var mailIconImageView: UIImageView!
    @IBOutlet weak var facebookIconImageView: UIImageView!
    @IBOutlet weak var signupEmailLabel: UILabel!
    @IBOutlet weak var facebookLabel: UILabel!
    
    var showPassword = false
    var disposableBag = DisposeBag()
    var passwordItems: [KeychainPasswordItem] = []
    var errorMessagesArray: [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UserStore.shared.isSkipSelected = false
        UserStore.shared.isLoggedIn = false
        configureUI()
        emailErrorLabelHeight.constant = 0
        passwordErrorLabelHeight.constant = 0

    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }
    
    func configureButtons() {
        
        // Email Button
        signInEmailButtonView.layer.cornerRadius = 28
        signInEmailButtonView.layer.borderColor = UIColor.white.cgColor
        signInEmailButtonView.layer.borderWidth = 1.0
        
        // Facebook Button
        facebookButtonView.layer.cornerRadius = 28
        facebookButtonView.layer.borderColor = UIColor.white.cgColor
        facebookButtonView.layer.borderWidth = 1.0

        signUpEmailButton.addTarget(self, action: #selector(signupTouchUpInside), for: UIControlEvents.touchUpInside)
        signUpEmailButton.addTarget(self, action: #selector(signupDragExit), for: UIControlEvents.touchDragExit);
        signUpEmailButton.addTarget(self, action: #selector(signupHoldDown), for: UIControlEvents.touchDown)
        
        signUpFacebookButton.addTarget(self, action: #selector(facebookTouchUpInside), for: UIControlEvents.touchUpInside)
        signUpFacebookButton.addTarget(self, action: #selector(facebookDragExit), for: UIControlEvents.touchDragExit);
        signUpFacebookButton.addTarget(self, action: #selector(facebookHoldDown), for: UIControlEvents.touchDown)
    }

    @objc func signupHoldDown() {
        signInEmailButtonView.backgroundColor = UIColor.white
        signupEmailLabel.textColor = themeBlueColor
        mailIconImageView.image = UIImage(named: "email_blue")
    }
    
    @objc func signupTouchUpInside() {
        signInEmailButtonView.backgroundColor = UIColor.clear
        signupEmailLabel.textColor = UIColor.white
        mailIconImageView.image = UIImage(named: "mail")
        self.view.endEditing(true)
        guard isDataValid() else {
            self.showSimpleAlertWithMessage(errorMessagesArray[0])
            return
        }
        loginUserWithEmail(emailTextField.text!, password: passwordTextField.text!)
    }

    @objc func signupDragExit() {
        signInEmailButtonView.backgroundColor = UIColor.clear
        signupEmailLabel.textColor = UIColor.white
        mailIconImageView.image = UIImage(named: "mail")
    }

    @objc func facebookHoldDown() {
        facebookButtonView.backgroundColor = UIColor.white
        facebookLabel.textColor = themeBlueColor
        facebookIconImageView.image = UIImage(named: "facebook")
    }
    
    @objc func facebookTouchUpInside() {
        facebookButtonView.backgroundColor = UIColor.clear
        facebookLabel.textColor = UIColor.white
        facebookIconImageView.image = UIImage(named: "facebook_white")
        let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
        fbLoginManager.logIn(withReadPermissions: ["public_profile", "email"], from: self) { (result, error) -> Void in
            if (error == nil){
                let fbloginresult : FBSDKLoginManagerLoginResult = result!
                if (result?.isCancelled)!{
                    return
                }
                if(fbloginresult.grantedPermissions.contains("email")) {
                    self.getFBUserData()
                }
            }
        }
    }
    
    @objc func facebookDragExit() {
        facebookButtonView.backgroundColor = UIColor.clear
        facebookLabel.textColor = UIColor.white
        facebookIconImageView.image = UIImage(named: "facebook_white")
    }

    // MARK: - IBActions
    
    @IBAction func signInWithEmailButtonTapped(_ sender: UIButton) {
    }
    
    @IBAction func signInWithFacebookButtonTapped(_ sender: UIButton) {
    }
    
    @IBAction func forgotPasswordButtonTapped(_ sender: UIButton) {
        let forgotPasswordMobileViewController = ForgotPasswordMobileViewController.instantiateFromAppStoryboard(appStoryboard: .Login)
        navigationController?.pushViewController(forgotPasswordMobileViewController, animated: true)
    }
    
    @IBAction func signupButtonTapped(_ sender: UIButton) {
        let signupViewController = SignupMainViewController.instantiateFromAppStoryboard(appStoryboard: .Login)
        self.navigationController?.pushViewController(signupViewController, animated: true)
    }
    
    @IBAction func laterButtonTapped(_ sender: UIButton) {
        guard let window = UIApplication.shared.keyWindow else {
            return
        }
        
        guard let rootViewController = window.rootViewController else {
            return
        }
        
        let tabBarController = BaseTabBarViewController()
        tabBarController.selectedIndex = 2
        tabBarController.view.frame = rootViewController.view.frame
        tabBarController.view.layoutIfNeeded()
        
        UIView.transition(with: window, duration: 0.3, options: .transitionCrossDissolve, animations: {
            window.rootViewController = tabBarController
        }, completion: { completed in
            UserStore.shared.isSkipSelected = true
            // maybe do something here
        })

    }
    
    @IBAction func touchIDLoginAction(_ sender: UIButton) {
        BioMetricAuthenticator.authenticateWithBioMetrics(reason: "", success: {
            guard let email = UserStore.shared.touchIDEmail, !email.isEmpty else {
                return
            }
            
            do {
                let passwordItem = KeychainPasswordItem(service: KeychainConfiguration.serviceName,
                                                        account: email,
                                                        accessGroup: KeychainConfiguration.accessGroup)
                let keychainPassword = try passwordItem.readPassword()
                DispatchQueue.main.async {
                    self.loginUserWithEmail(email, password: keychainPassword)
                }
            } catch {
                //Error
                fatalError("Error reading password from keychain - \(error)")
            }
        }, failure: { [weak self] (error) in
            print("Fail")
            
            // do nothing on canceled
            if error == .canceledByUser || error == .canceledBySystem {
                return
            } else if error == .biometryNotAvailable {
                // device does not support biometric (face id or touch id) authentication
                self?.showSimpleAlertWithMessage(error.message())
            } else if error == .fallback {
                // show alternatives on fallback button clicked
                // here we're entering username and password

            } else if error == .biometryNotEnrolled {
                // No biometry enrolled in this device, ask user to register fingerprint or face
                //Settings page
            } else if error == .biometryLockedout {
                // Biometry is locked out now, because there were too many failed attempts.
                // Need to enter device passcode to unlock.

                // show passcode authentication
            } else {
                // show error on authentication failed
                self?.showSimpleAlertWithMessage(error.message())
            }
        })
    }
    
    
    // MARK: - Helper Methods
    
    func configureUI() {
        UIApplication.shared.statusBarStyle = .lightContent

        //Email and Password TF
        emailTextField.placeholder = "EMAIL"
        emailTextField.title = "EMAIL"
        emailTextField.delegate = self
        emailTextField.textFont = textFieldDefaultFont
        emailTextField.keyboardType = UIKeyboardType.emailAddress
        
        passwordTextField.placeholder = "PASSWORD"
        passwordTextField.title = "PASSWORD"
        passwordTextField.delegate = self
        passwordTextField.textFieldButtonDelegate = self
        passwordTextField.textFont = textFieldDefaultFont
        passwordTextField.setButtonTitle(title: "Show")
        passwordTextField.isSecureTextEntry = true
        
        configureButtons()
        
        // TouchID Button
        touchIDButton.isHidden = !UserStore.shared.isBioMetricEnabledByUser
        if UserStore.shared.isBioMetricEnabledByUser && BioMetricAuthenticator.canAuthenticate() {
            touchIDButton.isHidden = false
            if BioMetricAuthenticator.shared.faceIDAvailable() {
                touchIDButton.setImage(UIImage(named: "FaceIcon"), for: .normal)
            } else {
                touchIDButton.setImage(UIImage(named: "Touch-icon-lg"), for: .normal)
            }
        } else {
            touchIDButton.isHidden = true
        }


        //Signup Button
        let attributedString = NSMutableAttributedString()
        attributedString.append(NSAttributedString(string: "Don't have an account ",
                                                   attributes: [.font: UIFont(name: "Montserrat-Light", size: 19.0)!,
                                                                .foregroundColor : UIColor.white]))
        attributedString.append(NSAttributedString(string: "SIGN UP",
                                                   attributes: [.font: UIFont(name: "Montserrat-Regular", size: 19.0)!,
                                                                .foregroundColor : UIColor.white,
                                                                .underlineStyle: NSUnderlineStyle.styleSingle.rawValue,
                                                                .underlineColor: UIColor.white]))

        signupButton.titleLabel?.attributedText = attributedString
    }
    
    func getFBUserData(){
        if((FBSDKAccessToken.current()) != nil){
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email"]).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil){
                    print(result as! [String: Any])
                    if let fbObject = result as? [String: Any] {
                        if let email = fbObject["email"] as? String {
                            var loginObject = [
                                "email": email,
                                "first_name": fbObject["first_name"] as! String,
                                "last_name": fbObject["last_name"] as! String,
                                "fb_id": fbObject["id"] as! String
                                ] as [String : AnyObject]
                            if let picutureDict = fbObject["picture"] as? [String: AnyObject],
                            let dataDict = picutureDict["data"] as? [String: AnyObject],
                                let isSilhouetter = dataDict["is_silhouette"] as? Int, isSilhouetter == 0,
                                let url = dataDict["url"] as? String {
                                loginObject["url"] = url as AnyObject
                            }
                            print(loginObject)
                            DispatchQueue.main.async {
                                self.checkEmailID(email, fbDict: loginObject)
                            }
                        } else {
                            self.showSimpleAlertWithMessage("An email ID is not associated with this account.".localized)
                        }
                    } else {
                        self.showSimpleAlertWithMessage("Failed while trying to login with Facebook".localized)
                    }
                }
            })
        }
    }

    func setUpPasswordFieldType() {
        passwordTextField.isSecureTextEntry = !showPassword
        passwordTextField.keyboardType = .asciiCapable
        let tmpString = passwordTextField.text
        passwordTextField.text = " "
        passwordTextField.text = tmpString
    }
    
    func isDataValid() -> Bool {
        let password = passwordTextField.text ?? ""
        let email = emailTextField.text ?? ""
        errorMessagesArray.removeAll()
        if email.isEmptyString() {
            emailTextField.hideImage()
            emailErrorLabel.text = ValidationErrorMessage.emailEmpty.description()
            errorMessagesArray.append(emailErrorLabel.text ?? "")
        } else if !email.isValidEmailID() {
            emailTextField.hideImage()
            emailErrorLabel.text = ValidationErrorMessage.emailInvalid.description()
            errorMessagesArray.append(emailErrorLabel.text ?? "")
        } else {
            emailTextField.showImage()
            emailErrorLabel.text = ""
        }
        animateEmailError()
        
        if password.isEmptyString() {
            passwordErrorLabel.text = ValidationErrorMessage.passwordEmpty.description()
            errorMessagesArray.append(passwordErrorLabel.text ?? "")
//        } else if !password.isValidPassword() {
//            passwordErrorLabel.text = ValidationErrorMessage.passwordInvalid.description()
//            errorMessagesArray.append(passwordErrorLabel.text ?? "")
        } else {
            passwordErrorLabel.text = ""
        }
        animatePasswordError()

        return errorMessagesArray.isEmpty
    }

    
    func animateEmailError() {
        let text = emailErrorLabel.text ?? ""
        UIView.animate(withDuration: 0.2) {
            self.emailErrorLabelHeight.constant = text.isEmpty ? 0 : self.emailErrorLabel.heightForText()
            self.view.layoutIfNeeded()
        }
    }
    
    func animatePasswordError() {
        let text = passwordErrorLabel.text ?? ""
        UIView.animate(withDuration: 0.2) {
            self.passwordErrorLabelHeight.constant = text.isEmpty ? 0 : self.passwordErrorLabel.heightForText()
            self.view.layoutIfNeeded()
        }
    }

    
}

// MARK: - TueetorButtonTextFieldDelegate

extension LandingViewController: TueetorButtonTextFieldDelegate {
    
    func actionButtonTapped() {
        showPassword = !showPassword
        setUpPasswordFieldType()
        passwordTextField.setButtonTitle(title: showPassword ? "Hide" : "Show")
    }
    
    
}

// MARK: - TextField Delegate

extension LandingViewController: UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.tag == 1 {
            let customTextField = textField as! TueetorTextField
            if textField.text!.isEmptyString() {
                customTextField.hideImage()
                emailErrorLabel.text = ValidationErrorMessage.emailEmpty.description()
            } else if !textField.text!.isValidEmailID() {
                customTextField.hideImage()
                emailErrorLabel.text = ValidationErrorMessage.emailInvalid.description()
            } else {
                customTextField.showImage()
                emailErrorLabel.text = ""
            }
            animateEmailError()
        } else {
            if textField.text!.isEmptyString() {
                passwordErrorLabel.text = ValidationErrorMessage.passwordEmpty.description()
//            } else if !textField.text!.isValidPassword() {
//                passwordErrorLabel.text = ValidationErrorMessage.passwordInvalid.description()
            } else {
                passwordErrorLabel.text = ""
            }
            animatePasswordError()
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if let _textField = textField as? TueetorTextField {
            _textField.errorMessage = ""
        } else if let _textField = textField as? TueetorButtonTextField {
            _textField.errorMessage = ""
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (textField.text?.isEmpty)! && string == " " {
            return false
        }
        let char = string.cString(using: String.Encoding.utf8)!
        let isBackSpace = strcmp(char, "\\b")
        if (isBackSpace == -92) {
            return true
        }
        
        switch textField.tag {
        case 1:
            return (textField.text?.count)! < EmailMaxLimit
        case 2:
            return (textField.text?.count)! < PasswordMaxLimit
        default:
            return true
        }
    }

}


extension LandingViewController {
    
    func loginUserWithEmail(_ email: String, password: String) {

        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }
        
        Utilities.showHUD(forView: self.view, excludeViews: [])
        let loginObserver = ApiManager.shared.apiService.loginUserWithEmail(email, password: password)
        let loginDisposable = loginObserver.subscribe(onNext: {(user) in
            
            if let stat = user.status, stat == "E"{
                DispatchQueue.main.async {
                    Utilities.hideHUD(forView: self.view)
                let popupDialog = PopupDialog.init(title: "Alert", message: "A verification link has been sent to your registered email.Please verify it")
                let buttonOne = CancelButton(title: "Ok") {
                   // self.navigationController?.popToRootViewController(animated: true)
                }
                popupDialog.addButtons([buttonOne])
                self.present(popupDialog, animated: true, completion: nil)
                }
                return
            }
            
            guard let status = user.status, status != "P" else {
                DispatchQueue.main.async {
                    Utilities.hideHUD(forView: self.view)
                    let signupConfirmMobileViewController = SignupConfirmMobileViewController.instantiateFromAppStoryboard(appStoryboard: .Login)
                    let components = user.redirectToVerify.components(separatedBy: "/")
                    signupConfirmMobileViewController.reConfirmMobile = true
                    signupConfirmMobileViewController.password = password
                    //                signupConfirmMobileViewController.mobileNumber = self.userRegistration.phone
                    signupConfirmMobileViewController.userID = Int(components.last!)!
                    self.navigationController?.pushViewController(signupConfirmMobileViewController, animated: true)
                }
                return
            }
            Utilities.hideHUD(forView: self.view)

            UserStore.shared.userEmail = email
            do {
                // This is a new account, create a new keychain item with the account name.
                let passwordItem = KeychainPasswordItem(service: KeychainConfiguration.serviceName,
                                                        account: email,
                                                        accessGroup: KeychainConfiguration.accessGroup)
                
                // Save the password for the new item.
                try passwordItem.savePassword(password)
                UserStore.shared.hasLoginKey = true
            } catch {
                UserStore.shared.hasLoginKey = false
                fatalError("Error updating keychain - \(error)")
            }
            
            DispatchQueue.main.async {
                Utilities.sendDeviceToken(nil)

                guard let window = UIApplication.shared.keyWindow else {
                    return
                }
                
                guard let rootViewController = window.rootViewController else {
                    return
                }
                
                let tabBarController = BaseTabBarViewController()
                tabBarController.selectedIndex = 2
                tabBarController.view.frame = rootViewController.view.frame
                tabBarController.view.layoutIfNeeded()
                UserStore.shared.isLoggedIn = true
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.user = user
                UIView.transition(with: window, duration: 0.3, options: .transitionCrossDissolve, animations: {
                    window.rootViewController = tabBarController
                }, completion: nil)
            }
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.showSimpleAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        })
        loginDisposable.disposed(by: disposableBag)
    }
    
    
    func socialLoginWithDict(_ dict: [String: AnyObject]) {
        
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }
        
        Utilities.showHUD(forView: self.view, excludeViews: [])
        let loginObserver = ApiManager.shared.apiService.facebookLoginWithDict(dict)
        let loginDisposable = loginObserver.subscribe(onNext: {(user) in
            Utilities.hideHUD(forView: self.view)
            
            UserStore.shared.userEmail = user.email
            UserStore.shared.isLoggedIn = true

            DispatchQueue.main.async {
                Utilities.sendDeviceToken(nil)

                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.user = user

                guard let window = UIApplication.shared.keyWindow else {
                    return
                }
                
                guard let rootViewController = window.rootViewController else {
                    return
                }
                
                let tabBarController = BaseTabBarViewController()
                tabBarController.selectedIndex = 2
                tabBarController.view.frame = rootViewController.view.frame
                tabBarController.view.layoutIfNeeded()
                UIView.transition(with: window, duration: 0.3, options: .transitionCrossDissolve, animations: {
                    window.rootViewController = tabBarController
                }, completion: nil)
            }
            print(user)
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.showSimpleAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        })
        loginDisposable.disposed(by: disposableBag)
    }
    
    func checkEmailID(_ email: String, fbDict: [String: AnyObject]) {
        
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }
        
        Utilities.showHUD(forView: self.view, excludeViews: [])
        let emailObserver = ApiManager.shared.apiService.checkAccountAvailability(email, phoneNumber: "")
        let emailDisposable = emailObserver.subscribe(onNext: {(isAvailable) in
            Utilities.hideHUD(forView: self.view)
            
            DispatchQueue.main.async {
                let alert = UIAlertController(title: AppName,
                                              message: AlertMessage.fbAccountDoesntExist.description(),
                                              preferredStyle: .alert)
                let yesAction = UIAlertAction(title: AlertButton.yes.description(),
                                              style: UIAlertActionStyle.default,
                                              handler: { (action) in
                                                DispatchQueue.main.async {
                                                    let signupAccountViewController = SignupAccountViewController.instantiateFromAppStoryboard(appStoryboard: .Login)
                                                    signupAccountViewController.isStudent = true
                                                    signupAccountViewController.facebookDict = fbDict
                                                    self.navigationController?.pushViewController(signupAccountViewController, animated: true)
                                                }
                })
                let noAction = UIAlertAction(title: AlertButton.no.description(),
                                             style: .cancel,
                                             handler: nil)
                alert.addAction(yesAction)
                alert.addAction(noAction)
                self.present(alert, animated: true, completion: nil)
            }
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.socialLoginWithDict(fbDict)
                    }
                }
            })
        })
        emailDisposable.disposed(by: disposableBag)
    }


}
