//
//  SignupConfirmMobileViewController.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 21/04/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit
import RxSwift

class SignupConfirmMobileViewController: BaseViewController {

    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var confirmationTextField: UITextField!
    @IBOutlet weak var firstLabel: UILabel!
    @IBOutlet weak var secondLabel: UILabel!
    @IBOutlet weak var thirdLabel: UILabel!
    @IBOutlet weak var fourthLabel: UILabel!
    @IBOutlet weak var firstView: UIView!
    @IBOutlet weak var secondView: UIView!
    @IBOutlet weak var thirdView: UIView!
    @IBOutlet weak var fourthView: UIView!
    
    var mobileNumber: String = ""
    var inactiveColor: UIColor = UIColor(white: 1, alpha: 0.5)
    var userID: Int!
    var disposableBag = DisposeBag()
    var reConfirmMobile = false
    var password: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        resetFields()
        if mobileNumber.isEmpty {
            messageLabel.text = "WE SENT A CODE TO YOUR REGISTERED NUMBER"
            resendOTP()
        } else {
            messageLabel.text = "WE SENT A CODE TO \(mobileNumber)"
        }
        confirmationTextField.addTarget(self, action: #selector(codeChanged(textField:)), for: .editingChanged)

    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }

    @IBAction func activateTextField(_ sender: UIButton) {
        confirmationTextField.becomeFirstResponder()
    }
    
    @IBAction func verifyButtonTapped(_ sender: TueetorButton) {
        guard confirmationTextField.text!.count == 4 else {
            self.showSimpleAlertWithMessage("Please enter the confirmation code sent to your number".localized)
            return
        }
        verifyOTP(confirmationTextField.text!)
    }
    
    
    @IBAction func changeNumberButtonTapped(_ sender: TueetorButton) {
        let alertController = UIAlertController(title: AppName,
                                                message: AlertMessage.changePhoneNumber.description(),
                                                preferredStyle: .alert)

        
        let confirmAction = UIAlertAction(title: AlertButton.confirm.description(),
                                          style: .default) { [weak alertController] _ in
            guard let alertController = alertController, let textField = alertController.textFields?.first else { return }
                                            DispatchQueue.main.async {
                                                print(textField.text!)
                                                self.overrideNumber(textField.text!)
                                            }
        }
        confirmAction.isEnabled = false
        alertController.addAction(confirmAction)
        
        alertController.addTextField { textField in
            textField.placeholder = "New Phone Number"
            textField.keyboardType = .numberPad
            NotificationCenter.default.addObserver(forName: NSNotification.Name.UITextFieldTextDidChange, object: textField, queue: OperationQueue.main, using: { (notification) in
                let phone = textField.text ?? ""
                confirmAction.isEnabled = phone.isValidPhoneNumber()
            })
        }

        
        let cancelAction = UIAlertAction(title: AlertButton.cancel.description(), style: .cancel, handler: nil)
        alertController.addAction(cancelAction)
        present(alertController, animated: true, completion: nil)
    }
    
    
    @IBAction func backButtonTapped(_ sender: UIButton) {
        self.view.endEditing(true)
        let alert = UIAlertController(title: AppName, message: "Do you want to exit the verification process?".localized, preferredStyle: .alert)
        let yesAction = UIAlertAction(title: AlertButton.yes.description(), style: .default) { (action) in
            let viewControllers: [UIViewController] = self.navigationController!.viewControllers
            for aViewController in viewControllers {
                if aViewController is LandingViewController {                                self.navigationController!.popToViewController(aViewController, animated: true)
                }
            }
        }
        
        let noAction =  UIAlertAction(title: AlertButton.no.description(), style: .default, handler: nil)
        alert.addAction(yesAction)
        alert.addAction(noAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func resendOTPButtonTapped(_ sender: UIButton) {
        resendOTP()
    }
    
    @objc func codeChanged(textField: UITextField) {
        resetFields()
        let code = textField.text ?? ""
        for (index, character) in code.enumerated() {
            switch index + 1 {
            case 1:
                firstView.backgroundColor = UIColor.white
                firstLabel.text = "\(character)"
            case 2:
                secondView.backgroundColor = UIColor.white
                secondLabel.text = "\(character)"
            case 3:
                thirdView.backgroundColor = UIColor.white
                thirdLabel.text = "\(character)"
            case 4:
                fourthView.backgroundColor = UIColor.white
                fourthLabel.text = "\(character)"
            default:
                break
            }
        }
    }

    func resetFields() {
        firstLabel.text = ""
        secondLabel.text = ""
        thirdLabel.text = ""
        fourthLabel.text = ""
        firstView.backgroundColor = inactiveColor
        secondView.backgroundColor = inactiveColor
        thirdView.backgroundColor = inactiveColor
        fourthView.backgroundColor = inactiveColor
    }
    
}

extension SignupConfirmMobileViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (textField.text?.isEmpty)! && string == " " {
            return false
        }
        let char = string.cString(using: String.Encoding.utf8)!
        let isBackSpace = strcmp(char, "\\b")
        if (isBackSpace == -92) {
            return true
        }
        return (textField.text?.count)! < 4
    }
    
}

// MARK:- APIs

extension SignupConfirmMobileViewController {
    
    func verifyOTP(_ otp: String) {
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }
        
        Utilities.showHUD(forView: self.view, excludeViews: [])

        let verifyObserver = ApiManager.shared.apiService.verifyOTP(otp, forUserID: userID)
        let verifyDisposable = verifyObserver.subscribe(onNext: {(user) in
            Utilities.hideHUD(forView: self.view)
            
            DispatchQueue.main.async {
                let popupDialog = PopupDialog.init(title: "Alert", message: "A verification link has been sent to your registered email.Please verify it")
                let buttonOne = CancelButton(title: "Ok") {
                    self.navigationController?.popToRootViewController(animated: true)
                }
                popupDialog.addButtons([buttonOne])
                self.present(popupDialog, animated: true, completion: nil)

//                let signupSuccessViewController = SignupSuccessViewController.instantiateFromAppStoryboard(appStoryboard: .Login)
//                (UIApplication.shared.delegate as! AppDelegate).user = user
//                signupSuccessViewController.user = user
//                signupSuccessViewController.password = self.password
//                self.navigationController?.pushViewController(signupSuccessViewController, animated: true)
            }
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.showSimpleAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        })
        verifyDisposable.disposed(by: disposableBag)
    }
    
    func resendOTP() {
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }
        
        Utilities.showHUD(forView: self.view, excludeViews: [])
        
        let resendObserver = ApiManager.shared.apiService.resendOTPForID(userID)
        let resendDisposable = resendObserver.subscribe(onNext: {(successDictionary) in
            Utilities.hideHUD(forView: self.view)
            self.showSimpleAlertWithMessage("OTP has been resent".localized)
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.showSimpleAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        })
        resendDisposable.disposed(by: disposableBag)
    }
    
    func overrideNumber(_ phone: String) {
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }
        
        Utilities.showHUD(forView: self.view, excludeViews: [])
        
        let overrideObserver = ApiManager.shared.apiService.overridePhoneNumber(phoneNumber: phone, userid: String(userID))
        let overrideDisposable = overrideObserver.subscribe(onNext: {(successDictionary) in
            Utilities.hideHUD(forView: self.view)
            DispatchQueue.main.async {
                if phone.isEmpty {
                    self.messageLabel.text = "WE SENT A CODE TO YOUR REGISTERED NUMBER"
                } else {
                    self.messageLabel.text = "WE SENT A CODE TO \(phone)"
                }
            }
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.showSimpleAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        })
        overrideDisposable.disposed(by: disposableBag)
    }
    
}
