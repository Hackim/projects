//
//  AddressPlacePickerTableViewCell.swift
//  Tueetor
//
//  Created by Phaninder on 26/04/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit

protocol AddressPlacePickerTableViewCellDelegate {
    func showPlacePicker()
}

class AddressPlacePickerTableViewCell: UITableViewCell {

    @IBOutlet weak var placePickerButton: UIButton!
    @IBOutlet weak var placeTextField: TueetorTextField!
    @IBOutlet weak var errorLabel: UILabel!
    
    var delegate: AddressPlacePickerTableViewCellDelegate?
    
    class func cellIdentifier() -> String {
        return "AddressPlacePickerTableViewCell"
    }
    
    func configureCellForRowAtIndex(_ index: Int, withText text: String) {
        placeTextField.tag = index
        placeTextField.placeholder = text
        placeTextField.title = text
        placeTextField.textFont = textFieldLightFont
        placeTextField.placeholderFont = textFieldDefaultFont
        placeTextField.titleFont = textFieldDefaultFont

    }


    @IBAction func placePickerButtonTapped(_ sender: UIButton) {
        delegate?.showPlacePicker()
    }
    
    func validateAddress(text: String) {
        if text.isEmptyString() {
            placeTextField.hideImage()
            errorLabel.text = ValidationErrorMessage.address.description()
        } else {
            placeTextField.showImage()
            errorLabel.text = ""
        }
    }

}
