//
//  SignupProfileHeaderTableViewCell.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 10/04/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit

protocol SignupProfileHeaderTableViewCellDelegate {
    func showImageOptions()
}

class SignupProfileHeaderTableViewCell: UITableViewCell {

    @IBOutlet weak var profileButtonTapped: UIButton!
    @IBOutlet weak var profileImageView: UIImageView!
    
    @IBOutlet weak var editImageView: UIImageView!
    var signupProfileDelegate: SignupProfileHeaderTableViewCellDelegate?
    
    class func cellIdentifier() -> String {
        return "SignupProfileHeaderTableViewCell"
    }
    
    @IBAction func profileButtonTapped(_ sender: UIButton) {
        signupProfileDelegate?.showImageOptions()
    }
    
}
