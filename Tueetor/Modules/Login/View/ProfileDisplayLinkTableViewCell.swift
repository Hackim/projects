//
//  ProfileDisplayLinkTableViewCell.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 26/05/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit

protocol ProfileDisplayLinkTableViewCellDelegate {
    func showShareOptions()
}

class ProfileDisplayLinkTableViewCell: UITableViewCell {

    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var linkLabel: UILabel!
    
    var delegate: ProfileDisplayLinkTableViewCellDelegate?
    
    class func cellIdentifier() -> String {
        return "ProfileDisplayLinkTableViewCell"
    }
        
    func configureLinkLabel(_ name: String, color: UIColor = UIColor.white) {
        let attributedString = NSMutableAttributedString()

        attributedString.append(NSAttributedString(string: "Your Profile Link ".localized,
                                                   attributes: [.font: UIFont(name: "Montserrat-Regular", size: 15.0)!,
                                                                .foregroundColor : color]))
        attributedString.append(NSAttributedString(string: "(to change it, enter a new Display Name)".localized,
                                                   attributes: [.font: UIFont(name: "Montserrat-Light", size: 15.0)!,
                                                                .foregroundColor : color]))
        
        descriptionLabel?.attributedText = attributedString
        linkLabel.text = "\(baseUrl)/\(name)"
    }
    
    @IBAction func shareButtonTapped(_ sender: UIButton) {
        delegate?.showShareOptions()
    }
    
    func configureLinkLabelWithLink(_ link: String, color: UIColor = UIColor.white) {
        let attributedString = NSMutableAttributedString()
        
        attributedString.append(NSAttributedString(string: "Your Profile Link ".localized,
                                                   attributes: [.font: UIFont(name: "Montserrat-Regular", size: 15.0)!,
                                                                .foregroundColor : themeBlueColor]))
        attributedString.append(NSAttributedString(string: "(to change it, enter a new Display Name)".localized,
                                                   attributes: [.font: UIFont(name: "Montserrat-Light", size: 15.0)!,
                                                                .foregroundColor : themeBlueColor]))
        
        descriptionLabel?.attributedText = attributedString
        linkLabel.text = link
    }

}
