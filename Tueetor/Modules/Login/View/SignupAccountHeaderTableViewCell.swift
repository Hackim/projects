//
//  SignupAccountHeaderTableViewCell.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 10/04/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit

class SignupAccountHeaderTableViewCell: UITableViewCell {

    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var emailImageView: UIImageView!
    
    class func cellIdentifier() -> String {
        return "SignupAccountHeaderTableViewCell"
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
//        let imageName = "email"
//        let originalImage = UIImage(named: imageName)
//        let templateImage = originalImage!.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
//        emailImageView.image = templateImage
//        emailImageView.tintColor = UIColor.lightGray

    }
}
