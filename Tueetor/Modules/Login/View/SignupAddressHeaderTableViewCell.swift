//
//  SignupAddressHeaderTableViewCell.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 12/04/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit

class SignupAddressHeaderTableViewCell: UITableViewCell {

    class func cellIdentifier() -> String {
        return "SignupAddressHeaderTableViewCell"
    }

}
