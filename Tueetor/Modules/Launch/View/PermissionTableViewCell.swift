//
//  PermissionTableViewCell.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 03/07/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit

class PermissionTableViewCell: UITableViewCell {

    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    class func cellIdentifier() -> String {
        return "PermissionTableViewCell"
    }
    
    class func cellHeight() -> CGFloat {
        return 226
    }
    
    func configureCellForIndex(_ index: Int) {
        iconImageView.image = UIImage(named: index == 1 ? "location1" : "push")
        descriptionLabel.text = index == 1 ? "Enable Location\nto find resources nearby".localized : "Enable Push Notification\nto receive timely info".localized
    }
}
