//
//  OnBoardingMainViewController.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 20/06/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit

class OnBoardingMainViewController: BaseViewController {

    @IBOutlet weak var continueButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        continueButton.layer.shadowOffset = CGSize.init(width: 0, height: 5)
        continueButton.layer.shadowRadius = 3
        continueButton.layer.shadowColor = themeBlueColor.cgColor
        continueButton.layer.shadowOpacity = 0.75
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }


    @IBAction func continueButtonTapped(_ sender: UIButton) {
        let onboard = OnBoardingAboutMeViewController.instantiateFromAppStoryboard(appStoryboard: .Main)
        self.navigationController?.pushViewController(onboard, animated: true)

    }
    
}
