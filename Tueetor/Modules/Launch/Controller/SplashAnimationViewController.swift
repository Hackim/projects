//
//  SplashAnimationViewController.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 24/04/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit
import RxSwift

class SplashAnimationViewController: BaseViewController {

    @IBOutlet weak var imageViewVerticalConstraint: NSLayoutConstraint!
    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var labelOne: UILabel!
    @IBOutlet weak var likeLabel: UILabel!
    @IBOutlet weak var neverLabel: UILabel!
    @IBOutlet weak var beforeLabel: UILabel!
    @IBOutlet weak var noInternetMessageLabel: UILabel!
    @IBOutlet weak var retryButton: UIButton!
    
    var isNetAvailable = true
    var isAnimationDone = false
    var disposableBag = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.shared.statusBarStyle = .lightContent

        isNetAvailable = Utilities.shared.isNetworkReachable()
        if UserStore.shared.isLoggedIn, isNetAvailable {
            fetchUserDetails()
        }
        retryButton.layer.borderColor = UIColor.white.cgColor
        animateTextLabels()
    }
    
    @IBAction func retryButtonTapped(_ sender: UIButton) {
        isNetAvailable = Utilities.shared.isNetworkReachable()
        if isNetAvailable {
            noInternetMessageLabel.isHidden = true
            retryButton.isHidden = true
            fetchUserDetails()
            Utilities.showHUD(forView: self.view, excludeViews: [])
        }
    }
    
    func animateTextLabels() {
        UIView.animate(withDuration: 1) {
            self.logoImageView.center.y = -(ScreenHeight / 2) + (ScreenHeight * 0.5)
        }
        
        labelOne.transform = CGAffineTransform(scaleX: 1.5,y: 1.5)
        
        UIView.animate(withDuration: 1, delay: 0.5, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveLinear, animations: {
            self.labelOne.transform = CGAffineTransform.identity
            self.labelOne.alpha = 1
            
        }) { (completed) in
            UIView.animate(withDuration: 0.4, animations: {
                self.likeLabel.alpha = 1
            }, completion: { (completed) in
                UIView.animate(withDuration: 0.4, animations: {
                    self.neverLabel.alpha = 1
                }, completion: { (completed) in
                    UIView.animate(withDuration: 0.4, animations: {
                        self.beforeLabel.alpha = 1
                    }, completion: { (completed) in
                        self.isAnimationDone = true
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.4, execute: {
                            self.navigateAccordinglyToScreen()
                        })
                    })
                })
            })
        }
    }
    
    
    func navigateAccordinglyToScreen() {

        if UserStore.shared.isLoggedIn {
            if self.isNetAvailable {
                if let _  = (UIApplication.shared.delegate as! AppDelegate).user {
                    self.navigateToDashboardViewController()
                } else {
                    Utilities.showHUD(forView: self.view, excludeViews: [])
                }
            } else {
                self.noInternetMessageLabel.isHidden = false
                self.retryButton.isHidden = false
            }
        } else {
            notLoggedInFlow()
        }
    }

    func notLoggedInFlow() {
        guard UserStore.shared.isSkipSelected == false else {
            self.navigateToDashboardViewController()
            return
        }
        guard let window = UIApplication.shared.keyWindow else {
            return
        }
        
        guard let rootViewController = window.rootViewController else {
            return
        }
        if UserStore.shared.isLanguageSelected == true {
            if UserStore.shared.isWalkthroughCompleted == true {
                let landingViewController = LandingViewController.instantiateFromAppStoryboard(appStoryboard: .Login)
                landingViewController.view.frame = rootViewController.view.frame
                landingViewController.view.layoutIfNeeded()
                let navController = BaseNavigationController(rootViewController: landingViewController)
                
                UIView.transition(with: window, duration: 0.3, options: .transitionCrossDissolve, animations: {
                    window.rootViewController = navController
                }, completion: nil)
            } else {
                let walkThroughPageViewController = WalkThroughPageViewController.instantiateFromAppStoryboard(appStoryboard: .Main)
                walkThroughPageViewController.view.frame = rootViewController.view.frame
                walkThroughPageViewController.view.layoutIfNeeded()
                let navController = BaseNavigationController(rootViewController: walkThroughPageViewController)
                
                UIView.transition(with: window, duration: 0.3, options: .transitionCrossDissolve, animations: {
                    window.rootViewController = navController
                }, completion: nil)
            }
        } else {
            let languageSelectionScreen = InitialLanguageSelectionViewController.instantiateFromAppStoryboard(appStoryboard: .Main)
            self.navigationController?.pushViewController(languageSelectionScreen, animated: true)
        }
    }
    
}

extension SplashAnimationViewController {
    
    func fetchUserDetails() {
        guard Utilities.shared.isNetworkReachable() else {
            return
        }
        
        let userObserver = ApiManager.shared.apiService.fetchUserDetails()
        let userDisposable = userObserver.subscribe(onNext: {(user) in
            DispatchQueue.main.async {
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.user = user
                if self.isAnimationDone {
                    Utilities.hideHUD(forView: self.view)
                    self.navigateToDashboardViewController()
                }
            }
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                self.navigateToDashboardViewController()
            })
        })
        userDisposable.disposed(by: disposableBag)
    }

}
