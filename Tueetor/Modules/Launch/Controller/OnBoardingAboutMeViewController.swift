//
//  OnBoardingAboutMeViewController.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 20/06/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit
import RxSwift

class OnBoardingAboutMeViewController: BaseViewController, UITextViewDelegate {

    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var countLabel: UILabel!
    
    var placeholderLabel : UILabel!
    var disposableBag = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()
        textView.delegate = self
        textView.layer.cornerRadius = 5.0
        textView.layer.borderWidth = 1.0
        textView.layer.borderColor = themeLightGrayColor.cgColor
        textView.clipsToBounds = true
        
        placeholderLabel = UILabel()
        let placeholderText = "Provide a good introduction about yourself: what makes you stand out, your expectation and aspiration, your achievements to date - anything that makes you highly employable. Be sincere, be earnest, be result driven, You can't go wrong. Minimum: 50 characters.".localized
        placeholderLabel.text = placeholderText
        
        placeholderLabel.font = UIFont(name: "Montserrat-Light", size: 15.0)!

        placeholderLabel.sizeToFit()
        textView.addSubview(placeholderLabel)
        placeholderLabel.numberOfLines = 0
        placeholderLabel.frame = CGRect(x: 5, y: (textView.font?.pointSize)! / 2, width: textView.frame.size.width - 5, height: 200)
        let height = placeholderLabel.heightForText()
        placeholderLabel.frame = CGRect(x: 5, y: (textView.font?.pointSize)! / 2, width: textView.frame.size.width - 5, height: height)
        placeholderLabel.textColor = UIColor.lightGray
        placeholderLabel.isHidden = !textView.text.isEmpty
        
        nextButton.layer.shadowOffset = CGSize.init(width: 0, height: 5)
        nextButton.layer.shadowRadius = 3
        nextButton.layer.shadowColor = themeBlueColor.cgColor
        nextButton.layer.shadowOpacity = 0.75
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }

    @IBAction func nextButtonTapped(_ sender: UIButton) {
        if let aboutText = textView.text {
            if aboutText.isEmpty {
                showSimpleAlertWithMessage("About cannot be empty".localized)
            } else if aboutText.count < 50 {
                showSimpleAlertWithMessage("About should be of minimum 50 characters".localized)
            } else {
                editUserDetails()
            }
        }
    }
    
    func textViewDidChange(_ textView: UITextView) {
        countLabel.text = String(textView.text.count)
        placeholderLabel.isHidden = !textView.text.isEmpty
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if (textView.text?.isEmpty)! && text == " " {
            return false
        }
        let char = text.cString(using: String.Encoding.utf8)!
        let isBackSpace = strcmp(char, "\\b")
        if (isBackSpace == -92) {
            return true
        }
        return true

    }
}

extension OnBoardingAboutMeViewController {
    
    func editUserDetails() {
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }

        guard let userID = (UIApplication.shared.delegate as! AppDelegate).user?.ID else {
            return
        }

        let dict = ["user_id": String(userID),
                    "about": textView.text!] as [String : AnyObject]
        Utilities.showHUD(forView: self.view, excludeViews: [])

        let userObserver = ApiManager.shared.apiService.editProfileWithDict(dict)
        let userDisposable = userObserver.subscribe(onNext: {(user) in
            Utilities.hideHUD(forView: self.view)
            DispatchQueue.main.async {
                let onboardingDocumentVC = OnboardingDocumentViewController.instantiateFromAppStoryboard(appStoryboard: .Main)
                self.navigationController?.pushViewController(onboardingDocumentVC, animated: true)
            }
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                self.navigateToDashboardViewController()
            })
        })
        userDisposable.disposed(by: disposableBag)
    }
    
}
