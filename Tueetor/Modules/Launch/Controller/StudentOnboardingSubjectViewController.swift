//
//  StudentOnboardingSubjectViewController.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 20/06/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit

class StudentOnboardingSubjectViewController: BaseViewController {

    @IBOutlet weak var okButton: UIButton!
    @IBOutlet weak var laterButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        okButton.layer.shadowOffset = CGSize.init(width: 0, height: 5)
        okButton.layer.shadowRadius = 3
        okButton.layer.shadowColor = themeBlueColor.cgColor
        okButton.layer.shadowOpacity = 0.75
        //Signup Button
        let attributedString = NSMutableAttributedString()
        attributedString.append(NSAttributedString(string: "Later ",
                                                   attributes: [.font: UIFont(name: "Montserrat", size: 19.0)!,
                                                                .foregroundColor : themeBlueColor,
                                                                .underlineStyle: NSUnderlineStyle.styleSingle.rawValue,
                                                                .underlineColor: UIColor.black]))
//        laterButton.titleLabel?.attributedText = attributedString

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }
    
    @IBAction func okButtonTapped(_ sender: UIButton) {
        let newSubjectViewController =  AddSubjectViewController.instantiateFromAppStoryboard(appStoryboard: .Profile)
        newSubjectViewController.shouldHideBackButton = true
        navigationController?.pushViewController(newSubjectViewController, animated: true)
    }
    
    @IBAction func laterButtonTapped(_ sender: UIButton) {
        self.navigateToDashboardViewController()
    }

}
