//
//  WalkThroughPageViewController.swift
//  Tueetor
//
//  Created by Phaninder on 23/04/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import Foundation
import UIKit

class WalkThroughPageViewController: UIPageViewController {

    var pageControl = UIPageControl()
    fileprivate lazy var pages: [UIViewController] = {
        return [
            WalkThroughScreenOneViewController.instantiateFromAppStoryboard(appStoryboard: .Main),
            WalkThroughScreenTwoViewController.instantiateFromAppStoryboard(appStoryboard: .Main),
            WalkThroughScreenThreeViewController.instantiateFromAppStoryboard(appStoryboard: .Main)
        ]
    }()
    
    var nextButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        UIApplication.shared.statusBarStyle = .default

        self.dataSource = self
        self.delegate = self
        configurePageControl()
        let bgView = UIView.init(frame: UIScreen.main.bounds)
        bgView.backgroundColor = UIColor.white
        view.insertSubview(bgView, at: 0)
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
 
        
//        let bgImageView = UIImageView(frame: UIScreen.main.bounds)
//        bgImageView.image = UIImage(named: "background")
//        view.insertSubview(bgImageView, at: 0)
        
        nextButton = UIButton(frame: CGRect(x: ScreenWidth - 75, y: ScreenHeight - 70, width: 50, height: 40))
        nextButton.setTitle("Next", for: .normal)
        nextButton.titleLabel?.font = UIFont(name: "Montserrat-Regular", size: 17.0)
        nextButton.setTitleColor(UIColor.init(red: 0.1372, green: 0.6431, blue: 0.6941, alpha: 1), for: .normal)
        nextButton.titleLabel?.textColor = UIColor.init(red: 0.1372, green: 0.6431, blue: 0.6941, alpha: 1)
        nextButton.addTarget(self, action: #selector(nextButtonTapped), for: .touchUpInside)
        view.addSubview(nextButton)
        view.bringSubview(toFront: nextButton)
        if let firstVC = pages.first {
            setViewControllers([firstVC], direction: .forward, animated: true, completion: nil)
        }
    }

    func configurePageControl() {
        // The total number of pages that are available is based on how many available colors we have.
        pageControl = UIPageControl(frame: CGRect(x: 0,y: UIScreen.main.bounds.maxY - 50,width: UIScreen.main.bounds.width,height: 50))
        pageControl.numberOfPages = pages.count
        pageControl.currentPage = 0
        pageControl.tintColor = UIColor.white
        pageControl.pageIndicatorTintColor = UIColor.init(red: 0.8431, green: 0.8431, blue: 0.8431, alpha: 1)
        pageControl.currentPageIndicatorTintColor = UIColor.init(red: 0.1372, green: 0.6431, blue: 0.6941, alpha: 1)
        pageControl.layer.position.y = ScreenHeight - 50
        view.addSubview(pageControl)
    }
    
    @objc func nextButtonTapped() {
        let currentViewController = pages[pageControl.currentPage]
        guard let nextViewController = dataSource?.pageViewController(self, viewControllerAfter: currentViewController) else { return }
        setViewControllers([nextViewController], direction: .forward, animated: true) { (completed) in
            DispatchQueue.main.async {
                self.changeValues()
            }
        }
    }
    
    @objc func doneButtonTapped() {
        let landingViewController = LandingViewController.instantiateFromAppStoryboard(appStoryboard: .Login)
        UserStore.shared.isWalkthroughCompleted = true
        self.navigationController?.pushViewController(landingViewController, animated: true)
    }

    func changeValues() {
        let pageContentViewController = self.viewControllers![0]
        self.pageControl.currentPage = pages.index(of: pageContentViewController)!
        if self.pageControl.currentPage == pages.count - 1 {
            nextButton.setTitle("Done", for: .normal)
            nextButton.addTarget(self, action: #selector(doneButtonTapped), for: .touchUpInside)
        } else {
            nextButton.setTitle("Next", for: .normal)
            nextButton.addTarget(self, action: #selector(nextButtonTapped), for: .touchUpInside)
        }
        
    }
}

extension WalkThroughPageViewController: UIPageViewControllerDataSource {
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        guard let viewControllerIndex = pages.index(of: viewController) else { return nil }
        
        let previousIndex = viewControllerIndex - 1
        
        guard previousIndex >= 0          else { return nil }
        
        guard pages.count > previousIndex else { return nil }
        
        return pages[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController?
    {
        guard let viewControllerIndex = pages.index(of: viewController) else { return nil }
        
        let nextIndex = viewControllerIndex + 1
        
        guard nextIndex < pages.count else { return nil }
        
        guard pages.count > nextIndex else { return nil }
        
        return pages[nextIndex]
    }
}

extension WalkThroughPageViewController: UIPageViewControllerDelegate {
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        changeValues()
    }
    
}
