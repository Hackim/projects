//
//  OnBoardingSubjectViewController.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 20/06/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit

class OnBoardingSubjectViewController: BaseViewController {

    @IBOutlet weak var okButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        okButton.layer.shadowOffset = CGSize.init(width: 0, height: 5)
        okButton.layer.shadowRadius = 3
        okButton.layer.shadowColor = themeBlueColor.cgColor
        okButton.layer.shadowOpacity = 0.75
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }

    @IBAction func okButtonTapped(_ sender: UIButton) {
        let newSubjectViewController =  AddSubjectViewController.instantiateFromAppStoryboard(appStoryboard: .Profile)
        newSubjectViewController.shouldHideBackButton = true
        navigationController?.pushViewController(newSubjectViewController, animated: true)
    }
    
}
