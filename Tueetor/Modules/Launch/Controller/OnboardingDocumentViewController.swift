//
//  OnboardingDocumentViewController.swift
//  Tueetor
//
//  Created by Phaninder Kumar on 20/06/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit
import RxSwift
import Alamofire
import MobileCoreServices

class OnboardingDocumentViewController: BaseViewController {

    @IBOutlet weak var uploadButton: UIButton!
    
    var selectedDocumentData: Data!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        uploadButton.layer.shadowOffset = CGSize.init(width: 0, height: 5)
        uploadButton.layer.shadowRadius = 3
        uploadButton.layer.shadowColor = themeBlueColor.cgColor
        uploadButton.layer.shadowOpacity = 0.75
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }
    
    @IBAction func uploadButtonTapped(_ sender: UIButton) {
        showImageOptions()
    }
    
    @IBAction func skipButtonTapped(_ sender: UIButton) {
        let onboardSubject = OnBoardingSubjectViewController.instantiateFromAppStoryboard(appStoryboard: .Main)
        self.navigationController?.pushViewController(onboardSubject, animated: true)
    }
    
    
    func showImageOptions() {
        let alert = UIAlertController(title: "Choose File".localized, message: nil, preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Document".localized, style: .default, handler: { _ in
            self.documentsLibrary()
        }))
        
        alert.addAction(UIAlertAction(title: "Camera".localized, style: .default, handler: { _ in
            self.camera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery".localized, style: .default, handler: { _ in
            self.photoLibrary()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel".localized, style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func showDocumentTitleAlert() {
        let alertController = UIAlertController(title: AppName,
                                                message: AlertMessage.docDesc.description(),
                                                preferredStyle: .alert)
        
        
        let confirmAction = UIAlertAction(title: AlertButton.confirm.description(),
                                          style: .default) { [weak alertController] _ in
                                            guard let alertController = alertController, let textField = alertController.textFields?.first else { return }
                                            DispatchQueue.main.async {
                                                print(textField.text!)
                                                self.uploadDocument(documentData: self.selectedDocumentData, decription: textField.text!)
                                            }
        }
        confirmAction.isEnabled = false
        alertController.addAction(confirmAction)
        
        alertController.addTextField { textField in
            textField.placeholder = "Document description"
            
            NotificationCenter.default.addObserver(forName: NSNotification.Name.UITextFieldTextDidChange, object: textField, queue: OperationQueue.main, using: { (notification) in
                confirmAction.isEnabled = !textField.text!.isEmpty
            })
        }
        
        
        let cancelAction = UIAlertAction(title: AlertButton.cancel.description(), style: .cancel, handler: nil)
        alertController.addAction(cancelAction)
        present(alertController, animated: true, completion: nil)
    }

}

extension OnboardingDocumentViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func camera() {
        if UIImagePickerController.isSourceTypeAvailable(.camera){
            let myPickerController = UIImagePickerController()
            myPickerController.delegate = self;
            myPickerController.sourceType = .camera
            myPickerController.allowsEditing = true
            self.present(myPickerController, animated: true, completion: nil)
        }
        
    }
    
    func photoLibrary() {
        
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
            let myPickerController = UIImagePickerController()
            myPickerController.delegate = self;
            myPickerController.sourceType = .savedPhotosAlbum
            myPickerController.mediaTypes = ["public.image", "public.movie"]
            myPickerController.allowsEditing = true
            self.present(myPickerController, animated: true, completion: nil)
        }
    }
    
    func documentsLibrary() {
        let importMenu = UIDocumentMenuViewController(documentTypes: [String(kUTTypePDF), String(kUTTypePNG), String(kUTTypeImage), "public.image", "public.video", "public.movie"], in: .import)
        //, "com.microsoft.word.doc", "com.microsoft.excel.xls"
        importMenu.delegate = self
        importMenu.modalPresentationStyle = .formSheet
        self.present(importMenu, animated: true, completion: nil)
    }

    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        dismiss(animated: true, completion: nil)
        var urlString: URL!
        
        if let myURL = info["UIImagePickerControllerMediaURL"] as? URL {
            urlString = myURL
        } else if let pickedImageURL = info["UIImagePickerControllerImageURL"] as? URL {
            urlString = pickedImageURL
        }
        if let fileURL = urlString {
            do {
                selectedDocumentData = try Data.init(contentsOf: fileURL)
            } catch {
                //Do nothing
            }
        } else {
            let image = info[UIImagePickerControllerEditedImage] as! UIImage
            selectedDocumentData = UIImageJPEGRepresentation(image, 0.5)
        }
        let bcf = ByteCountFormatter()
        bcf.allowedUnits = [.useMB] // optional: restricts the units to MB only
        bcf.countStyle = .file
        let sizeString = bcf.string(fromByteCount: Int64(selectedDocumentData.count))
        let size = CGFloat((sizeString as NSString).floatValue)
        if size > 15 {
            showSimpleAlertWithMessage("Document size cannot be greater than 15MB".localized)
            return
        }
        print("formatted result: \(sizeString)")
        showDocumentTitleAlert()
    }

}

extension OnboardingDocumentViewController: UIDocumentMenuDelegate,UIDocumentPickerDelegate {
    
    public func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        let myURL = url as URL
        do {
            selectedDocumentData = try Data.init(contentsOf: myURL)
            let bcf = ByteCountFormatter()
            bcf.allowedUnits = [.useMB]
            bcf.countStyle = .file
            let sizeString = bcf.string(fromByteCount: Int64(selectedDocumentData.count))
            let size = CGFloat((sizeString as NSString).floatValue)
            if size > 15 {
                showSimpleAlertWithMessage("Document size cannot be greater than 15MB".localized)
                return
            }
            print("formatted result: \(sizeString)")
            showDocumentTitleAlert()
        } catch {
            
        }
        print("import result : \(myURL)")
    }
    
    
    public func documentMenu(_ documentMenu:UIDocumentMenuViewController, didPickDocumentPicker documentPicker: UIDocumentPickerViewController) {
        documentPicker.delegate = self
        present(documentPicker, animated: true, completion: nil)
    }
    
    
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        print("view was cancelled")
        dismiss(animated: true, completion: nil)
    }
}

extension OnboardingDocumentViewController {
    
    func uploadDocument(documentData: Data, decription: String) {
        
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }
        guard let userID = UserStore.shared.userID else {
            return
        }
        
        Utilities.showHUD(forView: self.view, excludeViews: [])
        
        let documentDict : [String: String] = ["user_id": String(userID),
                                               "doc_desc": decription]
        
        print("documentDict: \n", documentDict)
        
        let url = "http://tueetoradmin.hipster-inc.com/public/post/upload_doc"
        
        let headers: HTTPHeaders = [
            "Content-type": "multipart/form-data"
        ]
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in documentDict {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            var fileName = "\(userID)"
            let mimeType = documentData.mimeType
            if mimeType == "image/jpeg" {
                fileName += ".jpeg"
            } else if mimeType == "application/pdf" {
                fileName += ".pdf"
            } else if mimeType == "application/octet-stream" {
                fileName += ".mp4"
            } else {
                fileName += ".png"
            }
            
            multipartFormData.append(documentData, withName: "doc", fileName: fileName, mimeType: mimeType)
            
        }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (result) in
            switch result{
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    print("Succesfully uploaded")
                    DispatchQueue.main.async(execute: {
                        Utilities.hideHUD(forView: self.view)
                    })
                    if let _ = response.error{
                        self.showSimpleAlertWithMessage("Failed to edit account.".localized)
                        return
                    }
                    let json = try? JSONSerialization.jsonObject(with: response.data!, options: .mutableContainers)
                    print(json)
                    if let responseDict = json as? [String: AnyObject], let message = responseDict["message"] as? String {
                        
                        DispatchQueue.main.async(execute: {
                            let onboardSubject = OnBoardingSubjectViewController.instantiateFromAppStoryboard(appStoryboard: .Main)
                            self.navigationController?.pushViewController(onboardSubject, animated: true)
                        })
                    }
                }
            case .failure(let error):
                DispatchQueue.main.async(execute: {
                    Utilities.hideHUD(forView: self.view)
                    if let error_ = error as? ResponseError {
                        self.showSimpleAlertWithMessage(error_.description())
                    } else {
                        if !error.localizedDescription.isEmpty {
                            self.showSimpleAlertWithMessage(error.localizedDescription)
                        }
                    }
                })
            }
        }
    }

}
