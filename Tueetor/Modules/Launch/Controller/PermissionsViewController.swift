//
//  PermissionsViewController.swift
//  Tueetor
//
//  Created by Phaninder on 22/04/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit
import CoreLocation
import UserNotifications

class PermissionsViewController: BaseViewController {

    @IBOutlet weak var tableView: UITableView!
    
    let locationManager = CLLocationManager()

    override func viewDidLoad() {
        super.viewDidLoad()
        locationManager.delegate = self
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(setNotificationsOn), name: .deviceTokenReceived,
                                               object: nil)

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        (self.tabBarController as! BaseTabBarViewController).tabBar.isHidden = true
        (self.tabBarController as! BaseTabBarViewController).hideCenterButton()
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false

    }

    @IBAction func OkbuttonTapped(_ sender: TueetorButton) {
        if UserStore.shared.isNotificationUnAuthorized  {
            askForNotificationPermissions()
        } else if Utilities.isLocationPermissionDenied() {
            checkLocationAuthorizationStatus()
        }
    }
    
    @IBAction func closeButtonTapped(_ sender: UIButton) {
        self.navigateBack(sender: sender)
    }
    
    func checkLocationAuthorizationStatus() {
        if CLLocationManager.authorizationStatus() == .denied {
            let alert = UIAlertController(title: "Location Permission".localized, message: "Please grant location permission to find resources nearby.".localized, preferredStyle: .alert)
            let okAction = UIAlertAction(title: AlertButton.ok.description(), style: .default) { (action) in
                self.navigateToSettingsPage()
            }
            let laterAction = UIAlertAction(title: AlertButton.later.description(), style: .cancel, handler: nil)
            alert.addAction(okAction)
            alert.addAction(laterAction)
            self.present(alert, animated: true, completion: nil)
        } else {
            (UIApplication.shared.delegate as! AppDelegate).ignoreRegisteringNotification = true
            locationManager.requestWhenInUseAuthorization()
        }
    }
    
    func askForNotificationPermissions() {
        UserStore.shared.notificationPermissionsAsked = true
        if #available(iOS 10.0, *) {
            let current = UNUserNotificationCenter.current()
            current.getNotificationSettings(completionHandler: { settings in
                
                switch settings.authorizationStatus {
                case .notDetermined:
                    DispatchQueue.main.async {
                        let delegate = UIApplication.shared.delegate as! AppDelegate
                        delegate.registerForPushNotifications()
                    }
                case .denied:
                    self.showNotificationsAlert()
                case .authorized:
                    UserStore.shared.isNotificationUnAuthorized = false
                    break
                }
            })
        } else {
            // Fallback on earlier versions
            if UIApplication.shared.isRegisteredForRemoteNotifications {
                UserStore.shared.isNotificationUnAuthorized = false
            } else {
                DispatchQueue.main.async {
                    let delegate = UIApplication.shared.delegate as! AppDelegate
                    delegate.registerForPushNotifications()
                }
            }
        }
    }
    
    func showNotificationsAlert() {
        let alert = UIAlertController(title: "Notification Permission".localized, message: "Enable Push Notification to receive timely info.".localized, preferredStyle: .alert)
        let okAction = UIAlertAction(title: AlertButton.ok.description(), style: .default) { (action) in
            self.navigateToSettingsPage()
        }
        let laterAction = UIAlertAction(title: AlertButton.later.description(), style: .cancel, handler: nil)
        alert.addAction(okAction)
        alert.addAction(laterAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc func setNotificationsOn() {
        self.tableView.reloadData()
        if Utilities.isLocationPermissionDenied() {
            self.checkLocationAuthorizationStatus()
        }
    }

    
}

extension PermissionsViewController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {

    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            self.navigationController?.popViewController(animated: true)
        }
    }
}

extension PermissionsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: PermissionTableViewCell.cellIdentifier(), for: indexPath) as! PermissionTableViewCell
        cell.configureCellForIndex(indexPath.row)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 1 {
            return Utilities.isLocationPermissionDenied() ? PermissionTableViewCell.cellHeight() : 0
        } else {
            return UserStore.shared.isNotificationUnAuthorized ? PermissionTableViewCell.cellHeight() : 0
        }
    }
}
